// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });

    $(".selectActividad").select2().change(function (e) {

        var usuario = $(".selectActividad").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onchangeActividad(usuario, nombreUsuario);

    });

    $(".selectActividad2").change(function (e) {

        var usuario = $(".selectActividad2").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            codigo: "",
            descripcion: "",
            equivalente: "",

            valorActividad: "",
            selectActividades: [],

            busqueda: "",

            codigo_actualizar: "",
            cod_grupo_modal: "",
            grupo_modal: "",
            codigo_modal: "",
            desc_modal: "",
            equivalente_modal: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadAnio: function () {


                var app = this;
                $.post('./request/getAno.php', {
                    opcion: '1',
                    buscar: '',
                    codDep: ''
                }, function (data) {
                    var datos = jQuery.parseJSON(data);
                    app.tablaDepartamentos = datos;
                    app.busqueda = "";
                });
            },

            loadClaseActividades: function () {
                var app = this;
                $.get('./request/getClaseAcividades.php', function (data) {
                    app.selectActividades = JSON.parse(data);
                });
            },

            onchangeActividad: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorActividad = cod;

                $.post('./request/getAno.php', {
                    opcion: '3',
                    buscar: '',
                    codDep: cod
                }, function (data) {
                    //console.log(data)
                    var datos = jQuery.parseJSON(data);
                    app.tablaDepartamentos = datos;
                    app.busqueda = "";
                });

            },

            verDepartamento: function (dato) {

                this.grupo_modal = dato.DESC_GRUPO;
                this.cod_grupo_modal = dato.GRUPO;
                this.codigo_modal = dato.CODIGO;
                this.desc_modal = dato.DESCRIPCION;
                this.equivalente_modal = dato.EQUIVALENTE;

                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno.php', {
                        opcion: '2',
                        buscar: app.busqueda.toUpperCase(),
                        codDep: ''
                    }, function (data) {
                        //console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;
                        app.busqueda = "";
                    });
                } else {

                    app.loadAnio()

                }

            },

            actualizar: function () {

                if (this.grupo_modal == "" ||
                    this.codigo_modal == "" ||
                    this.equivalente_modal == "" ||
                    this.desc_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo_modal,
                            descripcion: this.desc_modal,
                            equivalente: this.equivalente_modal,
                            codigo_clas: this.cod_grupo_modal,
                        },

                    }).done(function (data) {


                        //console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        //console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.DESCRIPCION + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codigo: dato.CODIGO,
                            grupo: dato.GRUPO,
                        }, function (data) {

                            //console.log(data)
                            app.valorActividad = "";

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {

                //console.log($(".selectActividad").val())

                if (this.codigo == "" ||
                    this.equivalente == "" ||
                    this.descripcion == "" ||
                    $(".selectActividad").val() == null) {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo,
                            descripcion: this.descripcion,
                            equivalente: this.equivalente,
                            codigo_clas: $(".selectActividad").val(),
                        },

                    }).done(function (data) {

                        //console.log(data)


                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadAnio();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        //console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.descripcion = "";
                app.equivalente = "";
                app.valorActividad = "";
                $("#selectActividad").val("").trigger('change')

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadAnio();
            this.loadClaseActividades();


        }
        ,

    })
;
