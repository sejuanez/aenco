<?php
    include("gestion.php");


    $ID = $_POST["serie"];


    $sql = "
    select  b.marca,
            b.codigomat,
            b.nombre_mat,
            b.proveedor,
            b.nombre_prov,
            b.documento_e,
            b.concepto_e,
            b.fecha_e,
            b.documento_s,
            b.concepto_s,
            b.fecha_s,
            b.super,
            b.nombre_super,
            b.actalega,
            b.fechalega, 
            b.serie, 
            B.grupo, 
            d.descripcion 
  from bodega_series b 
    left join descripcion_series d on d.grupo=b.grupo and d.codigo=b.marca 
   where  b.serie  LIKE '%" . $ID . "%'
  union 
    select  a.as_marca, 
            a.as_codmater, 
            a.as_desmater, 
            a.as_codprove,
            a.as_nomprove, 
            a.as_documento_e,
            a.as_concepto_e,
            a.as_fecha_e,
            a.as_documento_s,
            a.as_concepto_s,
            a.as_fecha_s,
            a.as_codtecnico,
            a.as_nomtecnico, 
            a.as_actalega,
            a.as_fechalega, 
            a.as_serie, 
            a.as_grupo, 
            d.descripcion 
    from actas_series a 
      left join descripcion_series d on d.grupo=a.as_grupo and d.codigo=a.as_marca 
     where a.as_serie  LIKE '%" . $ID . "%'
    ";

    $return_arr = array();

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $row_array['marca'] = utf8_encode($fila[0]);
        $row_array['codigo_material'] = utf8_encode($fila[1]);
        $row_array['nombre_material'] = utf8_encode($fila[2]);
        $row_array['proveedor'] = utf8_encode($fila[3]);
        $row_array['nombre_proveedor'] = utf8_encode($fila[4]);
        $row_array['documento_e'] = utf8_encode($fila[5]);
        $row_array['concepto_e'] = utf8_encode($fila[6]);
        $row_array['fecha_e'] = utf8_encode($fila[7]);
        $row_array['documento_s'] = utf8_encode($fila[8]);
        $row_array['concepto_s'] = utf8_encode($fila[9]);
        $row_array['fecha_s'] = utf8_encode($fila[10]);
        $row_array['super'] = utf8_encode($fila[11]);
        $row_array['nombre_super'] = utf8_encode($fila[12]);
        $row_array['acta_lega'] = utf8_encode($fila[13]);
        $row_array['fecha_lega'] = utf8_encode($fila[14]);
        $row_array['serie'] = utf8_encode($fila[15]);
        $row_array['grupo'] = utf8_encode($fila[16]);
        $row_array['descripcion'] = utf8_encode($fila[17]);
        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);
    
