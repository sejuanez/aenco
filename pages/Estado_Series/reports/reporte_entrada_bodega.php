<?php
    include("../request/gestion.php");
    require('fpdf.php');


    $server = json_decode($_POST['server']);
    $data = json_decode($_POST['datos']);
    $empresa = $_POST['empresa'];
    $nit = $_POST['nit'];
    $direccion = $_POST['direccion'];


    //echo($empresa);

    class PDF extends FPDF
    {
        function Header()
        {
            global $title;
            global $city;
            global $fecha;
            global $fechaIng;
            global $nDoc, $numero, $concepto, $idProveedor, $proveedor;

            global $nombreEmpresa, $nitEmpresa, $direccionEmpresa;

            $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
            $this->SetFont('Lato-Regular', '', 12);

            // Ancho de pagina 190 en X
            // $this->setDrawColor(80,80,80);
            // $this->setFillColor(0,80,170);
            // $this->setTextColor(255,255,255);
            $this->setTextColor(80, 80, 80);
            $this->Image('../../../img/empresa1.jpg', 10, 10, 40, 12, 'jpg');


            $this->SetFont('Lato-Regular', '', 14);
            $this->Cell(190, 10, $nombreEmpresa, 0, 0, 'C', false);
            $this->Ln(8);
            $this->SetFont('Lato-Regular', '', 8);
            $this->Cell(190, 5, 'NIT:  ' . $nitEmpresa, 0, 0, 'C', false);
            $this->Ln(5);
            $this->Cell(190, 5, $direccionEmpresa, 0, 0, 'C', false);

            $this->SetFont('Lato-Regular', '', 14);
            $this->Ln(10);
            $this->Cell(190, 5, $title, 0, 0, 'C', false);


            // Line break
            $this->Ln(10);

            $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
            $this->SetFont('Lato-Regular', '', 10);

            $f = explode("-", $fecha);
            $this->setTextColor(0, 80, 170);
            $this->Cell(31, 7, 'Fecha Movimiento :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(26, 7, $f[2] . "/" . $f[1] . "/" . $f[0], 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(26, 7, 'Fecha Registro :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(26, 7, str_replace('-', '/', $fechaIng), 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(20, 7, utf8_decode('N° Interno :'), 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(20, 7, $nDoc, 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(26, 7, utf8_decode('N° Movimiento :'), 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(22, 7, $numero, 0, 0, 'L');
            $this->Ln(7);

            $this->setTextColor(0, 80, 170);
            $this->Cell(20, 7, 'Concepto :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(170, 7, $concepto, 0, 0, 'L');
            $this->Ln(7);

            $this->setTextColor(0, 80, 170);
            $this->Cell(20, 7, 'Proveedor :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(110, 7, $idProveedor . ' - ' . $proveedor, 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(30, 7, 'Orden de Compra :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(30, 7, '  ', 0, 0, 'L');
            $this->Ln(10);

        }

        function Footer()
        {
            // Go to 1.5 cm from bottom
            $this->SetY(-15);
            $this->AddFont('LatoL', '', 'Lato-Light.php');
            $this->SetFont('LatoL', '', 8);
            // Print centered page number
            $this->setFillColor(0, 80, 170);
            $this->setTextColor(80, 80, 80);
            $this->Cell(95, 10, 'Fecha y hora de impresion: ' . date('d/m/Y  -H:i:s', time() - 21600), 'T', 0, 'L');
            $this->Cell(95, 10, utf8_decode('Página ' . $this->PageNo()) . '/{nb}', 'T', 0, 'R');
        }
    }

    $title = 'ENTRADAS A ALMACEN';
    $city = 'Bucaramanga';

    $fecha = $data[0]->fecha;
    $fechaIng = $server->fechaIng;
    $nDoc = $data[0]->nDoc;
    $numero = $server->numero;
    $concepto = $data[0]->concepto;
    $idProveedor = $data[0]->idProveedor;
    $proveedor = $data[0]->proveedor;
    $nombreEmpresa = $empresa;
    $nitEmpresa = $nit;
    $direccionEmpresa = $direccion;


    $pdf = new PDF();
    $pdf->SetTitle($title);
    $pdf->AliasNbPages();
    $pdf->AddPage();
    // Header table
    $pdf->AddFont('LatoL', '', 'Lato-Light.php');
    $pdf->SetFont('LatoL', '', 9);
    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(0, 80, 170);
    $pdf->setTextColor(255, 255, 255);

    $pdf->Cell(20, 7, 'CODIGO', 1, 0, 'C', true);
    $pdf->Cell(100, 7, 'DESCRIPCION DE MATERIAL', 1, 0, 'C', true);
    $pdf->Cell(30, 7, 'MARCA', 1, 0, 'C', true);
    $pdf->Cell(20, 7, 'UNIDAD', 1, 0, 'C', true);
    $pdf->Cell(20, 7, 'CANTIDAD', 1, 0, 'C', true);
    $pdf->Ln();

    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(255, 255, 255);
    $pdf->setTextColor(80, 80, 80);
    $pdf->AddFont('Lato', '', 'Lato-Regular.php');
    $pdf->SetFont('Lato', '', 9);


    // DETALLE
    // var_dump($data);
    $n = count($data);
    for ($i = 0; $i < $n; $i++) {

        $n2 = count($data[$i]->arraySeries);

        if ($n2 > 0) {

            $pdf->Cell(20, 7, $data[$i]->codigo, 'B', 0, 'C', true);
            $pdf->Cell(100, 7, $data[$i]->descripcion, 'B', 0, 'C', true);
            $pdf->Cell(30, 7, $data[$i]->arraySeries[$i]->marca, 'B', 0, 'C', true);
            $pdf->Cell(20, 7, 'UNIDAD', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, $data[$i]->cantidad, 'B', 0, 'C', true);
            $pdf->Ln(8);

        } else {

            $pdf->Cell(20, 7, $data[$i]->codigo, 'B', 0, 'C', true);
            $pdf->Cell(100, 7, $data[$i]->descripcion, 'B', 0, 'C', true);
            $pdf->Cell(30, 7, '', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, 'UNIDAD', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, $data[$i]->cantidad, 'B', 0, 'C', true);
            $pdf->Ln(8);

        }
    }

    $pdf->Ln(2);
    for ($i = 0; $i < $n; $i++) {

        $n2 = count($data[$i]->arraySeries);
        if ($n2 > 0) {
            $text = '>> SERIES DE ' . $data[$i]->descripcion . ' <<';
            $text2 = '';
            for ($j = 0; $j < $n2; $j++) {
                $text2 .= $data[$i]->arraySeries[$j]->serie . ' - ';
            }
            $pdf->SetFont('Lato', '', 7);
            $pdf->Cell(190, 7, $text);
            $pdf->Ln(5);
            $pdf->MultiCell(0, 3, $text2);
            $pdf->Ln(1);
        }
    }


    $pdf->Ln();
    $pdf->SetFont('Lato', '', 9);
    $pdf->Cell(60, 7, 'Registro : ' . $server->user);
    $pdf->Ln(20);

    $pdf->setTextColor(0, 80, 170);
    $pdf->Cell(90, 7, 'Entrego', 'T', 0, 'C', true);
    $pdf->Cell(10, 7, ' ');
    $pdf->Cell(90, 7, 'Recibio', 'T', 0, 'C', true);
    $pdf->Ln(10);

    $pdf->Ln(10);
    $pdf->Output();


?>
