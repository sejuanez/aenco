<!DOCTYPE html>
<html>
<head>
    <title>Entradas a Bodega</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">


    <style type="text/css">
        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .btn-app {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 50px;
            margin-bottom: 10px;
            color: #555;
            font-size: 0.8em;
        }

        .btn-app:hover {
            background: #007bff;
            color: #FFF;
        }

        .btn-app:active {
            background: #009dff;
            color: #FFF;
        }

        label {
            margin-bottom: 0;
        }

        thead {
            font-size: .8em;
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }

        td {
            font-size: .8em;
            cursor: pointer;
        }

        .gj-datepicker input, .gj-datepicker span {
            padding: 5px;
            font-size: .9em;

        }

        input[type="text"] {
            text-transform: uppercase;
        }

        .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
            line-height: initial;
        }

    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Estado de Series

        </p>
    </header>


    <div class="container">


        <div style="display:flex;justify-content:center;align-items:center;">
            <div>
                <form action="" id="form_encabezado">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="proveedor"> N° de Serie</label>

                                <div class="input-group float-right input-group-sm"><input type="text"
                                                                                           v-model="inputSerie"

                                                                                           class="form-control">
                                    <span
                                            class="input-group-btn"><button type="button" @click="buscarSeries()"
                                                                            class="btn btn-secondary">Buscar</button></span>
                                </div>

                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        <!--        <br>-->


    </div>

    <div class="container">

        <div class="col-12">

            <div class="row" style="margin-bottom: 8px;">


                <div class="col-4">
                    <div class="form-group">
                        <label for="">Descripcion</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control " aria-label=""
                                   disabled v-model="descripcion" min="1">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group">
                        <label for="">Marca</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control " aria-label=""
                                   disabled v-model="marca" min="1">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group">
                        <label for="">Material:</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control " aria-label=""
                                   disabled v-model="material" min="1">
                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>

    <div class="container">

        <div class="row">

            <div class="col-6">
                <fieldset>
                    <legend>Entradas</legend>
                    <div class="row">


                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Documento Ent:</label>
                                <div class="input-group input-group-sm">
                                    <input type="number" id="" class="form-control " aria-label=""
                                           disabled v-model="documento_e" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Concepto:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="concepto_e" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Fecha:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="fecha_e" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Proveedor:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="proveedor" min="1">
                                </div>
                            </div>
                        </div>


                    </div>
                    <br>

                </fieldset>

            </div>

            <div class="col-6">
                <fieldset>
                    <legend>Salidas</legend>
                    <div class="row">


                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Documento Sal:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="documento_s" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Concepto:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="concepto_s" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Fecha:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="fecha_s" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="">Tecnico:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="tecnico" min="1">
                                </div>
                            </div>
                        </div>


                    </div>

                    <br>

                </fieldset>
            </div>

            <div class="col-12">
                <fieldset style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">N° Acta:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="acta_lega" min="1">
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Fecha:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" class="form-control " aria-label=""
                                           disabled v-model="fecha_lega" min="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </fieldset>
            </div>

        </div>


        <br>

        <div style="display:flex;justify-content:center;align-items:center;">
            <div>
                <p>
                    <button id="anterior" class="btn btn-secondary" @click="anterior">Anterior</button>

                    <span v-text="primero"></span>
                    de
                    <span v-text="ultimo"></span>

                    <button id="siguiente" class="btn btn-secondary" @click="siguiente">Siguiente</button>
                </p>
            </div>
        </div>


    </div>

</div>

</div>


<script src="js/jquery/jquery-3.2.1.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/datepicker.js"></script>
<script src="js/pace/pace.min.js"></script>


<!-- <script src="js/vue/vue.min.js"></script> -->
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>
</html>
