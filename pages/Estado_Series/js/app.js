// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });
// $('#fecha').val(app.fechaEntrada);


});


let app = new Vue({
    el: '#app',
    data: {

        series: [],

        inputSerie: "",

        primero: 0,
        ultimo: 0,
        descripcion: "",
        marca: "",
        material: "",
        proveedor: "",
        documento_e: "",
        concepto_e: "",
        fecha_e: "",
        documento_s: "",
        concepto_s: "",
        fecha_s: "",
        tecnico: "",
        fecha_lega: "",
        acta_lega: "",

    },


    methods: {

        buscarSeries: function () {
            var app = this;
            $.post('./request/getSeries.php', {serie: app.inputSerie}, function (data) {
                var data = JSON.parse(data);
                app.series = data;
                console.log(data)
                if (app.series.length == 0) {

                    alertify.error('No se encontraron registros');

                } else {

                    app.mostrarResultados()

                }


            });
        },

        mostrarResultados: function () {
            var app = this;
            var series = app.series;
            app.primero = 1;
            app.ultimo = series.length;

            app.listar(app.primero);

        },

        anterior: function () {

            if (app.primero > 1) {
                app.primero--;
                app.listar(app.primero)
            }

        },

        siguiente: function () {

            if (app.primero < app.series.length) {
                app.primero++;
                app.listar(app.primero)
            }

        },

        listar: function (i) {

            app.descripcion = app.series[i - 1].serie;

            app.marca = app.series[i - 1].marca + " - " + app.series[i - 1].descripcion;
            app.material = app.series[i - 1].codigo_material + " - " + app.series[i - 1].nombre_material;
            app.proveedor = app.series[i - 1].proveedor + " - " + app.series[i - 1].nombre_proveedor;
            app.documento_e = app.series[i - 1].documento_e;
            app.concepto_e = app.series[i - 1].concepto_e;
            app.fecha_e = app.series[i - 1].fecha_e;
            app.documento_s = app.series[i - 1].documento_s;
            app.concepto_s = app.series[i - 1].concepto_s;
            app.fecha_s = app.series[i - 1].fecha_s;
            app.tecnico = app.series[i - 1].nombre_super;
            app.fecha_lega = app.series[i - 1].fecha_lega;
            app.acta_lega = app.series[i - 1].acta_lega;

        }


    },
    watch: {},
    computed: {},
    created() {

    },
    mounted() {

    },

    updated() {


    },


});

