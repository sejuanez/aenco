<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$orden=$_POST["orden"];
		
	$consulta = "SELECT O.ot_orden, O.ot_fecha_generacion, O.ot_cod_cliente, o.ot_nombre_cliente,
						o.ot_telefono_cliente, o.ot_direccion_cliente, o.ot_cod_dpto, d.de_nombre,
						o.ot_cod_mpio, m.mu_nombre, o.ot_cod_localidad, c.co_nombre, o.ot_cod_barrio,
						b.ba_nombarrio, o.ot_tecnico, t.te_nombres, t.te_cuadrilla,
						coalesce(o.ot_placa, v.ve_placa) placa, o.ot_proceso_campana, ca.c_nombre,
						o.ot_fecha_asigna_contrata, o.ot_fechaej, o.ot_fechaprogramada, o.ot_fecha_envio_contratante,
						coalesce(fa.fa_longitud, 0) fa_longitud, coalesce(fa.fa_latitud, 0) fa_latitud,
						dur.ui_codigo codResultado, dur.ui_nombre Resultado, DUA.ui_codigo CodAnomalia, dua.ui_nombre Anomalia,
						o.ot_solicitud,
						o.ot_solicitud_externa
					from ot o left join departamentos D ON D.de_codigo = O.ot_cod_dpto
						left join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio
						left join corregimientos c on c.co_depto=o.ot_cod_dpto and c.co_municipio=o.ot_cod_mpio
						and c.co_codcorregimiento=o.ot_cod_localidad
						left join barrios b on b.ba_depto=o.ot_cod_dpto and b.ba_mpio=o.ot_cod_mpio
						and b.ba_sector=o.ot_cod_localidad and b.ba_codbarrio=o.ot_cod_barrio
						left join tecnicos t on t.te_codigo=o.ot_tecnico
						left join campanas ca on ca.c_codigo=o.ot_proceso_campana
						left join cuadrillas cu on cu.cu_ctecnico=o.ot_tecnico
						left join vehiculos v on v.ve_cuadrilla=cu.cu_codigo
						left join fac_solicitud fa on fa.fa_solicitud=o.ot_solicitud
						left join dato_unirre dur on dur.ui_acta=o.ot_solicitud and dur.ui_tipo='R'
						left join dato_unirre dua on dua.ui_acta=o.ot_solicitud and dua.ui_tipo='A'
					where o.ot_solicitud = '".$orden."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['orden'] = utf8_encode($fila[0]);
		$row_array['fechaGen'] = utf8_encode($fila[1]);
		$row_array['codCliente'] = utf8_encode($fila[2]);
		$row_array['nombreCliente'] = utf8_encode($fila[3]);
		$row_array['telefono'] = utf8_encode($fila[4]);
		$row_array['direccion'] = utf8_encode($fila[5]);
		$row_array['codDpto'] = utf8_encode($fila[6]);
		$row_array['dpto'] = utf8_encode($fila[7]);
		$row_array['codMun'] = utf8_encode($fila[8]);
		$row_array['mun'] = utf8_encode($fila[9]);
		$row_array['codLocalidad'] = utf8_encode($fila[10]);
		$row_array['localidad'] = utf8_encode($fila[11]);
		$row_array['codBarrio'] = utf8_encode($fila[12]);		
		$row_array['barrio'] = utf8_encode($fila[13]);
		$row_array['codTecnico'] = utf8_encode($fila[14]);
		$row_array['nombreTecnico'] = utf8_encode($fila[15]);
		$row_array['cuadrilla'] = utf8_encode($fila[16]);
		$row_array['placa'] = utf8_encode($fila[17]);
		$row_array['codProceso'] = utf8_encode($fila[18]);
		$row_array['nombreProceso'] = utf8_encode($fila[19]);
		$row_array['fechaAsignaContrata'] = utf8_encode($fila[20]);
		$row_array['fechaEje'] = utf8_encode($fila[21]);
		$row_array['fechaProgramada'] = utf8_encode($fila[22]);
		$row_array['fechaEnvioContratante'] = utf8_encode($fila[23]);
		$row_array['lon'] = utf8_encode($fila[24]);
		$row_array['lat'] = utf8_encode($fila[25]);
		$row_array['codResultado'] = utf8_encode($fila[26]);
		$row_array['resultado'] = utf8_encode($fila[27]);
		$row_array['codAnomalia'] = utf8_encode($fila[28]);
		$row_array['anomalia'] = utf8_encode($fila[29]);
		
		
								
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>