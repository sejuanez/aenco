<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">

	<title>Consultar soicitud</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  
	<link rel="stylesheet" type="text/css" href="css/estilo.css">

  	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

  	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  
    
  	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
  	<script type="text/javascript" src="../../js/gmaps.js"></script>

	<script type="text/javascript">

		$(function(){
			var map;
			var marker;

			$( "#alertEscogerSolicitud" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				width:700,
				buttons: {
					Ok: function() {

						//ajax getSolicitud
						$( this ).dialog( "close" );
						getSolicitud($("input[name=radioOrdenInterna]:checked").val());
						//$("#alertEscogerSolicitud table>tbody").html("");
						//$( this ).dialog( "close" );
					},
					cancelar:function(){

						$( this ).dialog( "close" );
						$("#txtSolicitud").attr("disabled",false);
						$("#consultando").hide();
						$("#consultar").show();
						//$("#contenido").show();
					}
				}
			});

			$("#consultando").hide();
			//$("#contenido").hide();

			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:1
			});
			
			function getSolicitud(ordenInterna){

				var doneAjax1 = doneAjax2 = doneAjax3 = false;


				/* Ajax getCabeceraSolicitanteReporte.php*/
				$.ajax({
		              url:'request/getSolicitud.php',
		              type:'POST',
		              dataType:'json',
		              data:{orden:ordenInterna}
		          	}).done(function(repuesta){

		              if (repuesta.length>0) {
		                  
		                  /*cabecera*/
		                  $("#txtOrden").val(repuesta[0].orden);
		                  $("#txtCliente").val(repuesta[0].codCliente+" - "+repuesta[0].nombreCliente);
		                  $("#txtDepartamento").val(repuesta[0].codDpto+" - "+repuesta[0].dpto);
		                  $("#txtMunicipio").val(repuesta[0].codMun+" - "+repuesta[0].mun);
		                  $("#txtLocal").val(repuesta[0].codLocalidad+" - "+repuesta[0].localidad);
		                  $("#txtBarrio").val(repuesta[0].codBarrio+"-"+repuesta[0].barrio);
		                  $("#txtDireccion").val(repuesta[0].direccion);
		                  $("#txtTelefono").val(repuesta[0].telefono);
		                  $("#txtTecnico").val(repuesta[0].codTecnico+"-"+repuesta[0].nombreTecnico);
		                  $("#txtPlaca").val(repuesta[0].placa);
		                  $("#txtCuadrilla").val(repuesta[0].cuadrilla);
		                  $("#txtProceso").val(repuesta[0].codProceso+"-"+repuesta[0].nombreProceso);
		                  $("#txtResultado").val(repuesta[0].codResultado+"-"+repuesta[0].resultado);
		                  $("#txtAnomalia").val(repuesta[0].codAnomalia+"-"+repuesta[0].anomalia);


		                  marker.setPosition({lat:+repuesta[0].lat, lng:+repuesta[0].lon});
		                  map.setCenter({lat:+repuesta[0].lat, lng:+repuesta[0].lon});
		                 
		              }else
		              {
		                    //alert("No se encontraron datos para la cabecera, solicitante y reporte con este número de acta");
		                    
		              }
		              
		                doneAjax1=true;

		                if(doneAjax1 && doneAjax2 && doneAjax3 ){
		                	$("#txtSolicitud").attr("disabled",false);
							$("#consultando").hide();
							$("#consultar").show();
							$("#contenido").show();
		                }
		                
		                
		          	});
				/*Fin Ajax getCabeceraSolicitanteReporte.php*/

				/*Ajax getTabla.php*/
				$.ajax({
		              url:'request/getTabla.php',
		              type:'POST',
		              dataType:'json',
		              data:{orden:ordenInterna}
		          	}).done(function(repuesta){

		              if (repuesta.length>0) {
		                  
		                  //var filaActiva=0;
		                  
		                  /*tabla*/
		                  var filas = "<tr class='cabecera'>"+
										"<td>Tipo</td>"+
										"<td>Fecha</td>"+
										"<td>Observaciones</td>"+
										"</tr>";

							for(var i=0; i<repuesta.length;i++)
	                        {                        
	                            //filas+="<tr name='"+i+"' class='fila'>"+
	                            filas+="<tr class='fila'>"+
	                            "<td>"+repuesta[i].tipo+"</td><td>"+repuesta[i].fecha+"</td><td>"+repuesta[i].observa+"</td>"+
	                            "</tr>";	                            
	                        }			
		                  	
		                  	$("#tabla-detalles").html(filas);

		                  	
		              }else
		              {
		                    //alert("No se encontraron datos para los materiales con este número de acta");
		                    
		              }
		              
		                
		                doneAjax2=true;

		                if(doneAjax1 && doneAjax2 && doneAjax3){
		                	$("#txtSolicitud").attr("disabled",false);
							$("#consultando").hide();
							$("#consultar").show();
							$("#contenido").show();
		                }
		          	});
				/*fin Ajax materiales.php*/



				/*ajax listaEvidencias*/
				$.ajax({
	                url:'request/listarEvidencias.php',
	                type:'POST',
	                dataType:'json',
	                data:{orden:ordenInterna}

	             	}).done(function(repuesta){
	             

		                if(repuesta.length>0){

		                 $("#list-img").html("");//resetamos la lista de imagenes
		                 $("#list-img").animate({left:0});//resetamos la lista de imagenes
		                 $("#img-max").html("");//reseteamos a la imagen maximizada
		                 $("#title-img").html("");//reseteamos el titulo de la imagen maximizada

		                 var arr_img={};//vector q almacena las imagenes de cada nodo
		                  
		                  for(var j=0 ;j<repuesta.length; j++){//for que carga las imagenes del acta
		                    arr_img[j]=new Image();
		                    arr_img[j].onload=function(){};
		                    arr_img[j].src=repuesta[j].url_img;
		                    arr_img[j].name=j;
		                    arr_img[j].alt="imagen "+(j+1);
		                    //arr_img[j].width=600;
		                    //arr_img[j].height=400;
		                    arr_img[j].title="Imagen "+(j+1)+" de "+repuesta.length;
		                    $("#list-img").append(arr_img[j]);
		                                        
		                  }//fin del for que carga las imagenes del acta

		                 
		                  var zoom = 50;
		                  var rotacion=0;
		                  var imgSeleccionada=null;
		                  $("#galeria").hide();

		                  $("#menubar-cerrar").click(function(e){                     
		                      e.preventDefault();

		                      rotacion=0;
		                  	  $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

		                      $("#galeria").hide();
		                      $("#img-max img").css("width",50+"%");
		                  });

		                  $("#menubar-aumentar").click(function(e){    
		                  	  e.preventDefault();                  
		                      if(zoom<100){
		                        zoom+=10;
		                        $("#img-max img").css("width",zoom+"%");
		                      }                        
		                  });

		                  $("#menubar-disminuir").click(function(e){  
		                  	  e.preventDefault();                    
		                      if(zoom>50){
		                        zoom-=10;
		                        $("#img-max img").css("width",zoom+"%");
		                      }                        
		                  });

		                  $("#menubar-anterior").click(function(e){  
		                  	  e.preventDefault();

		                  	  rotacion=0;
		                  	  $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

		                      if(imgSeleccionada>0){
		                        imgSeleccionada--;
		                        $("#img-max img").animate({opacity:0},200,function(){
		                        	var img=$("#list-img img[name='"+imgSeleccionada+"']").clone();                   
			                        $("#img-max").html(img);
			                        $("#title-img").html("Imagen "+(imgSeleccionada+1)+" de "+repuesta.length);
			                        zoom=50;
			                        $("#img-max img").css("width",zoom+"%");
			                        $("#img-max img").animate({opacity:1},200,function(){console.log("fin animacion");});
		                        });
		                        
		                      }                        
		                  });

		                  $("#menubar-siguiente").click(function(e){
		                  	  e.preventDefault();

		                  	  rotacion=0;
		                  	  $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

		                      if(imgSeleccionada<repuesta.length-1){
		                        imgSeleccionada++;
		                        $("#img-max img").animate({opacity:0},200,function(){
		                        	var img=$("#list-img img[name='"+imgSeleccionada+"']").clone();
		                        $("#title-img").html("Imagen "+(imgSeleccionada+1)+" de "+repuesta.length);                   
		                        $("#img-max").html(img);
		                        zoom=50;
		                        $("#img-max img").css("width",zoom+"%");
		                        $("#img-max img").animate({opacity:1},200,function(){console.log("fin animacion");});
		                        });
		                        
		                      }                        
		                  });

		                  $("#menubar-rotarIzq").click(function(e){
		                  	  e.preventDefault();

		                  	  if(rotacion>-360){
		                  	  	rotacion=rotacion-90;
		                  	  	 $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
		                  	  	
		                  	  }else{
		                  	  	rotacion=0;
		                  	  	$("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
		                  	  }  
		                  	  //console.log(rotacion);                
		                  });

		                  $("#menubar-rotarDer").click(function(e){
		                  	  e.preventDefault();

		                  	  if(rotacion<360){
		                  	  	rotacion=rotacion+90;
		                  	  	 $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
		                  	  	 
		                  	  }else{
		                  	  	rotacion=0;
		                  	  	$("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
		                  	  } 
		                  	  //console.log(rotacion);                
		                  });

		                  //EVENTO CLIC PARA CADA IMAGEN DEL CARRUSEL
		                  $("#list-img img").click(function(e){
		                  	e.preventDefault();
		                    //$("#list-img img").removeClass("img-selected");
		                    var img=$(this).clone();                   
		                    $("#img-max").html(img);
		                    //$(this).addClass("img-selected");
		                    $("#title-img").html("Imagen "+(+$(this).attr("name")+1)+" de "+repuesta.length);
		                    imgSeleccionada=+$(this).attr("name");
		                    $("#galeria").show();
		                                        
		                  });//fin del evento clic de las imagenes del carrusel

		                  
		                  $("#carrusel").show();
		                  $("#contenido").show();
		                  $("#modal").animate({right:'-100%'},500); 
		                }//fin del if si hay resultado
		                else{
		                  $("#carrusel").hide();
		                   //alert("No se encontraron datos para las evidencias con este número de acta");       
		                  //$("#contenido").hide();
		                  //$("#modal h1").html("Lo sentimos!");
		                  //$("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
		                }

		                doneAjax3=true;

		                if(doneAjax1 && doneAjax2 && doneAjax3){
		                	$("#txtSolicitud").attr("disabled",false);
							$("#consultando").hide();
							$("#consultar").show();
							$("#contenido").show();
		                }
	           		});//fin del done
			};


			function resetearTodo(){
				$(".reset").val("");

				$("#tabla-luminaria").html("<tr class='cabecera'>"+
										"<td>Luminaria</td>"+
										"<td>CodigoMat</td>"+
										"<td>Marca</td>"+
										"<td>Serie</td>"+
										"<td>Nodo</td>"+
										"<td>Punto</td>"+
										"<td>Lámpara</td>"+
										/*"<td>Potencia</td>"+*/
										"<td>Encendido</td>"+
									"</tr>");

				$("#tabla-materiales").html("<tr class='cabecera'>"+
										"<td>CodMater</td>"+
										"<td>Desmater</td>"+
										"<td>CanMater</td>"+
										"<td>ValorMat</td>"+
										"<td>TipoMat</td>"+
										"<td>RI</td>"+
										"<td>Obs</td>"+
									"</tr>");
				$("#tabla-adecuaciones").html("<tr class='cabecera'>"+
										"<td>Código</td>"+
										"<td>Nombre</td>"+
									"</tr>");

			};

			function cargarMapa(){
              /*GMaps.geolocate({
                  success: function(position){*/
                        
                         map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                            el: '#mapa',
                            lat: 8.749349,/*position.coords.latitude,*/
                            lng: -75.879568,/*position.coords.longitude,*/
                            zoom:10
                          });
                         

                          marker = new google.maps.Marker({
                          	position: {lat: 8.749349/*position.coords.latitude*/, lng: -75.879568/*position.coords.longitude*/},
                          	//map: map
                          	//title: 'Hello World!'
                          });

		                  map.addMarker(marker);
             
                  /*},
                  error: function(error){
                     alert('Fallo en la geolocalizacion:'+error.message);
                     return false;
                  },
                  not_supported:function(){
                    alert('Geolocalizacion no soportada');
                    return false;
                  }
              });*/
        	};
        	cargarMapa();

			//========funcion buscar=================================
			function buscar(evt){

				evt.preventDefault();

				$("#txtSolicitud").attr("disabled",true);
				$("#buscar").button("disable");
				$("#consultar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				var orden = $("#txtSolicitud").val();

				resetearTodo();

				/* Ajax getCabeceraSolicitanteReporte.php*/
				$.ajax({
		              url:'request/getSolicitudes.php',
		              type:'POST',
		              dataType:'json',
		              data:{orden:orden}
		          	}).done(function(repuesta){

		              if (repuesta.length>0) {

		              		if(repuesta.length>1){

		              			var filas="<tr class='cabecera'><td>Sel.</td><td>Orden Interna</td><td>Proceso</td><td>Fecha</td><td>Cliente</td><td>Departamento</td><td>Municipio</td><td>Ejecutada</td></tr>";
		              			filas+="<tr class='fila'><td><input type='radio' name='radioOrdenInterna' value='"+repuesta[0].ordenInterna+"' checked></td><td>"+repuesta[0].ordenInterna+"</td><td>"+repuesta[0].proceso+"</td><td>"+repuesta[0].fecha+"</td><td>"+repuesta[0].cliente+"</td><td>"+repuesta[0].dpto+"</td><td>"+repuesta[0].mun+"</td><td>"+repuesta[0].ejecutada+"</td></tr>";
		              			for(var i=1; i<repuesta.length;i++){

		              				filas+="<tr class='fila'><td><input type='radio' name='radioOrdenInterna' value='"+repuesta[i].ordenInterna+"'></td><td>"+repuesta[i].ordenInterna+"</td><td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].fecha+"</td><td>"+repuesta[i].cliente+"</td><td>"+repuesta[i].dpto+"</td><td>"+repuesta[i].mun+"</td><td>"+repuesta[i].ejecutada+"</td></tr>";
		              			}

		              			$("#alertEscogerSolicitud table>tbody").html(filas);
		              			$("#alertEscogerSolicitud").dialog("open");
		              		}
		              		else
		              		{
		              			getSolicitud(repuesta[0].ordenInterna);
		              		}
		                  
		                  
		                 
		              }else
		              {
		                    //alert("No se encontraron datos para la cabecera, solicitante y reporte con este número de acta");
		                    $("#txtSolicitud").attr("disabled",false);
							$("#consultando").hide();
							$("#consultar").show();
		                    
		              }
		              	                
		                
		          	});
				/*Fin Ajax getCabeceraSolicitanteReporte.php*/

			};
			//========fin de la funcion buscar========================

			document.getElementById("form").addEventListener("submit",buscar);

			$("#consultar").click(buscar);

					
			document.getElementById("txtSolicitud").focus();

			//$("#tabs").tabs({show: { effect: "slide", duration: 200 },active:1});

			

			$("body").show();
			$("#txtSolicitud").focus();


		});//fin JQuery

	</script>

</head>
<body style="display:none">

	<div id="alertEscogerSolicitud" title="Escoje una solicitud">
		<!--<p>Se han encontrado varias solicitudes para la misma orden. </p>-->
		<table>
			<tbody></tbody>
		</table>
	</div>

	<header>
	    <h3>Consultar solicitud</h3>
	    <nav>
	      <ul id="menu">
	        
	        <!--<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>-->
	        <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
	        <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
	          
	      </ul>
	    </nav>
    </header>
  
  


  <div id='subheader'>

	    <div id="div-form">

	      <form id="form">

	      		<div><label>Solicitud No.</label><input type="text" id="txtSolicitud" autofocus></div>

	      </form>

  		</div>

  </div>   




	<div id="contenido">
		
		
		<div id="div_2">

				<div>

					<div class="wrapper">
					
						<div><label>Orden No.</label><input type="text" id="txtOrden" class="reset" readOnly></div>
						<div><label>Cliente</label><input type="text" id="txtCliente" class="reset" readOnly></div>
						<div><label>Departamento</label><input type="text" id="txtDepartamento" class="reset" readOnly></div>
						<div><label>Municipio</label><input type="text" id="txtMunicipio" class="reset" readOnly></div>
						<div><label>Local/Vda</label><input type="text" id="txtLocal" class="reset" readOnly></div>
						<div><label>Barrio</label><input type="text" id="txtBarrio" class="reset" readOnly></div>					
						<div><label>Dirección</label><input type="text" id="txtDireccion" class="reset" readOnly></div>

						<div><label>Teléfono</label><input type="text" id="txtTelefono" class="reset" readOnly></div>
						<div><label>Técnico</label><input type="text" id="txtTecnico" class="reset" readOnly></div>
						<div><label>Placa</label><input type="text" id="txtPlaca" class="reset" readOnly></div>
						<div><label>Cuadrilla</label><input type="text" id="txtCuadrilla" class="reset" readOnly></div>
						<div><label>Proceso</label><input type="text" id="txtProceso" class="reset" readOnly></div>
						<div><label>Resultado</label><input type="text" id="txtResultado" class="reset" readOnly></div>					
						<div><label>Anomalía</label><input type="text" id="txtAnomalia" class="reset" readOnly></div>
						
						
					</div>

				</div>
				
		</div>
		<!-- fin div_2-->




		<!-- Tabs -->

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Estados</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab' >Ubicación</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab' >Evidencias</span></a></li>
				
			
			</ul>


			<div id='tab1'>
									
				<table id="tabla-detalles">
					<tr class="cabecera">
						<td>Tipo</td>
						<td>Fecha</td>
						<td>Observaciones</td>
					</tr>
				</table>
						
				
			</div>




			<div id='tab2'>
				
				<div id="div-map"><div id="mapa"></div></div>

			</div>




			<div id='tab3'>
				
				<div id="div-galeria">
				
					<div class="wrapper">
						
						<div id='carrusel'>
					      <!--<a href='#' id='carrusel-anterior'></a>
					      <a href='#' id='carrusel-siguiente'></a>-->
					      <div id="list-img"></div>
					    </div>
					      
					    
					    <div id="galeria">
					        
					        <div id="menubar">
					          <span id="title-img"></span>
					          <a href="#" title="anterior" id="menubar-anterior" class="ion-ios-arrow-thin-left"></a>
					          <a href="#" title="siguiente" id="menubar-siguiente" class="ion-ios-arrow-thin-right"></a>
					          <a href="#" title="disminuir" id="menubar-disminuir" class="ion-ios-minus-empty"></a>
					          <a href="#" title="aumentar" id="menubar-aumentar" class="ion-ios-plus-empty"></a>
					          <a href="#" title="Rotar -45º" id="menubar-rotarIzq" class="ion-ios-undo-outline"></a>
					          <a href="#" title="Rotar 45º" id="menubar-rotarDer" class="ion-ios-redo-outline"></a>
					          <a href="#" title="cerrar" id="menubar-cerrar" class="ion-ios-close-empty"></a>
					      </div>

					      <div id="img-max"></div>

					    </div>
					    
					</div>
					
				</div>
				
			</div>


		</div>

		<!-- fin tabs -->

	</div><!-- fin div contenido -->



</body>
</html>