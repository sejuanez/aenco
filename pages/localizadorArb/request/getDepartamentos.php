<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	
	
	$consulta = "SELECT d.de_codigo, d.de_nombre from departamentos d";



	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['value'] = $fila[0];
		$row_array['label'] = $fila[1];
		
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>