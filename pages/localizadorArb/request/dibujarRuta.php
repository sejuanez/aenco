<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	
	
	$dpto = $_POST["dpto"];
	$mun = $_POST["mun"];
	//$trafo = $_POST["trafo"];

	
	function getFotosActa($cnx, $acta){

		$sql = "SELECT ea.ea_no_foto from evidencias_alumbrado ea where ea.ea_acta='".$acta."'";
		$divImg="<div>";

		$result = ibase_query($cnx,$sql);
		while($fila = ibase_fetch_row($result)){
			$divImg.="<img src='request/imagenEvidencias.php?acta=".$acta."&codfoto=".$fila[0]."'></img>";
		}

		$divImg.="</div>";

		return $divImg;
	};
	
	
	
	//$consulta = "SELECT DA.dla_longitud LON_INICIAL, DA.dla_latitud LAT_INICIAL, DA.dla_nodo NOD_INICIAL, DA.dla_nodo_anterior NODO_ANTES, (DA.dla_longitud) LON_ANTES, (DA.dla_latitud) LAT_ANTES, da.dla_pto_fisico, da.dla_tipo_lampara, cast(da.dla_potencia as integer) potencia, da.dla_tipo_encendido, da.dla_direccion, da.dla_luminaria, MAT.ma_descripcion, ds.descripcion, da.dla_serielum, d.de_nombre dpto, m.mu_nombre mpio, c.co_nombre corregimiento, b.ba_nombarrio, da.dla_fecha FECHA_CENSO,da.dla_acta FROM busca_serielum ('".$dpto."', '".$mun."') bs inner join dato_lev_alumbrado da on da.dla_serielum=bs.dla_serielum and da.dla_fecha=bs.fechaej and da.dla_dpto = '".$dpto."' and da.dla_mpio= '".$mun."' and da.dla_longitud<>0 LEFT JOIN departamentos d on d.de_codigo=da.dla_dpto LEFT JOIN municipios m on m.mu_depto = da.dla_dpto and m.mu_codigomun=da.dla_mpio LEFT JOIN corregimientos c on c.co_depto=da.dla_dpto and c.co_municipio=da.dla_mpio and c.co_codcorregimiento=da.dla_corregimiento LEFT JOIN barrios b on b.ba_depto=da.dla_dpto and b.ba_mpio=da.dla_mpio and b.ba_sector=da.dla_corregimiento and b.ba_codbarrio=da.dla_barrio LEFT JOIN materiales MAT ON MAT.ma_codigo=DA.dla_codigomat LEFT JOIN descripcion_series DS on ds.grupo=MAT.ma_grupo and ds.codigo=da.dla_codmarca";
	
	$consulta = "SELECT ca.ca_placa placa, p.pr_nombres nombre_comun, p.pr_razonsocial nombre_cientifico, ca.ca_cap CAP_cm, ca.ca_alturatotal altura_total_m, ca.ca_altura_fuste altura_fuste_m, ca.ca_diametro_copa diametro_copa_m, ca.ca_estadofitosanitario estado_fitosanitario, ca.ca_enrgizado energizado, ca.ca_erradicacion erradicacion, t.te_nombres funcionario, ca.ca_longitud longitud, ca.ca_latutud latitud, ca.ca_cod_ncomun codarbol from censo_arboles ca left join proveedores p on p.pr_codigo=ca.ca_cod_ncomun left join tecnicos t on t.te_codigo=ca.ca_usuario_ejecuta where ca.ca_dpto='".$dpto."' and ca.ca_mpio='".$mun."'";

	$return_arr = array();
	

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		
		
		$row_array['latIni'] = $fila[12];
		$row_array['lonIni'] = $fila[11];
		$row_array['nodoIni'] = $fila[2];
		$row_array['nodoAnt'] = $fila[3];
		$row_array['latAnt'] = $fila[5];
		$row_array['lonAnt'] = $fila[4];
		$row_array['tipoLamp'] = $fila[7];
		$row_array['potencia'] = $fila[8];
		$row_array['acta'] = $fila[0];
		$row_array['nombre_comun'] = $fila[1];
		$row_array['codArbol'] = $fila[13];
		$row_array['infoWin'] = "<div id='infoWindow'><table><tr class='cabecera'><td>Placa ".$fila[0]."</td><td>Nombre común</td><td>".$fila[1]."</td></tr>".
															"<tr class='fila'><td>Nombre científico</td><td>:</td><td>".utf8_encode($fila[2])."</td></tr>".
															"<tr class='fila'><td>CAP (cm)</td><td>:</td><td>".utf8_encode($fila[3])."</td></tr>".
															"<tr class='fila'><td>Altura total (m)</td><td>:</td><td>".utf8_encode($fila[4])."</td></tr>".
															"<tr class='fila'><td>Altura fuste (m)</td><td>:</td><td>".utf8_encode($fila[5])."</td></tr>".
															"<tr class='fila'><td>Diámetro copa (m)</td><td>:</td><td>".utf8_encode($fila[6])."</td></tr>".
															"<tr class='fila'><td>Estado fitosanitario</td><td>:</td><td>".utf8_encode($fila[7])."</td></tr>".
															"<tr class='fila'><td>Energizado</td><td>:</td><td>".utf8_encode($fila[8])."</td></tr>".
															"<tr class='fila'><td>Erradicación</td><td>:</td><td>".utf8_encode($fila[9])."</td></tr>".
															"<tr class='fila'><td>Funcionario</td><td>:</td><td>".utf8_encode($fila[10])."</td></tr>".
															"<tr class='fila'><td>Longitud</td><td>:</td><td>".utf8_encode($fila[11])."</td></tr>".
															"<tr class='fila'><td>Latitud</td><td>:</td><td>".utf8_encode($fila[12])."</td></tr></table></div>";
															//"<tr class='fila'><td>Serie</td><td>:</td><td>".utf8_encode($fila[14])."</td></tr></table>".getFotosActa($conexion, $fila[20])."</div>";
		//$row_array['title'] = "Trafo ".$fila[0];
		array_push($return_arr, $row_array);
		
	}

	echo json_encode($return_arr);
	//print_r($return_arr);
?>