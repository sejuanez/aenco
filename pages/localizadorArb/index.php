<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  } 
?>

<!DOCTYPE html5>

<html>

<head>
	
  <meta charset="utf-8">

	<title>Arboles por municipio</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  
  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">


  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	
  <!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry"></script>
  <script type="text/javascript" src="js/gmaps.js"></script>-->

  <script type="text/javascript" src="../../js/accounting.js"></script>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
  <script type="text/javascript" src="../../js/gmaps.js"></script>
  

  	<script type="text/javascript">
  		
      $(function(){

        //$("#exportar").hide();
        $("#consultar").hide();
        $("#consultando").hide();
        $('#departamento').attr('disabled',true);
        $('#municipio').attr('disabled',true);

        
        var map;
        var ptsMedir = [];
        var ptsArea = [];
        var poligonoArea;
        var markerRetornados=0;
        //var polylinesRetornadas=0;

        function listarEvidencias(acta){
            /*ajax listaEvidencias*/
            $("#list-img").html("Buscando fotos del árbol "+acta+" ...");//resetamos la lista de imagenes
            $("#div-btnGaleria").hide();
            $.ajax({
                  url:'request/listarEvidencias.php',
                  type:'POST',
                  dataType:'json',
                  data:{acta:acta}

                }).done(function(repuesta){
               

                    if(repuesta.length>0){

                     $("#list-img").html("");//resetamos la lista de imagenes
                     //$("#list-img").animate({left:0});//resetamos la lista de imagenes
                     $("#img-max").html("");//reseteamos a la imagen maximizada
                     $("#title-img").html("");//reseteamos el titulo de la imagen maximizada

                     var arr_img={};//vector q almacena las imagenes de cada nodo
                      
                        


                      for(var j=0 ;j<repuesta.length; j++){//for que carga las imagenes del acta
                        arr_img[j]=new Image();
                        arr_img[j].onload=function(){};
                        arr_img[j].src=repuesta[j].url_img;
                        arr_img[j].name=j;
                        arr_img[j].alt="imagen "+(j+1);
                        arr_img[j].title="Imagen "+(j+1)+" de "+repuesta.length;
                        $("#list-img").append(arr_img[j]);
                                            
                      }//fin del for que carga las imagenes del acta

                     
                      var zoom = 50;
                      var rotacion=0;
                      var imgSeleccionada=null;
                      $("#galeria").hide();
                      $("#div-cantidadFotos").html(repuesta.length);
                      $("#div-btnGaleria").show();

                      $("#menubar-cerrar").click(function(e){                     
                          e.preventDefault();

                          rotacion=0;
                          $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

                          $("#galeria").hide();
                          $("#img-max img").css("width",50+"%");
                      });

                      $("#menubar-aumentar").click(function(e){    
                          e.preventDefault();                  
                          if(zoom<100){
                            zoom+=10;
                            $("#img-max img").css("width",zoom+"%");
                          }                        
                      });

                      $("#menubar-disminuir").click(function(e){  
                          e.preventDefault();                    
                          if(zoom>50){
                            zoom-=10;
                            $("#img-max img").css("width",zoom+"%");
                          }                        
                      });

                      $("#menubar-anterior").click(function(e){  
                          e.preventDefault();

                          rotacion=0;
                          $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

                          if(imgSeleccionada>0){
                            imgSeleccionada--;
                            $("#img-max img").animate({opacity:0},200,function(){
                              var img=$("#list-img img[name='"+imgSeleccionada+"']").clone();                   
                              $("#img-max").html(img);
                              $("#title-img").html("Imagen "+(imgSeleccionada+1)+" de "+repuesta.length);
                              zoom=50;
                              $("#img-max img").css("width",zoom+"%");
                              $("#img-max img").animate({opacity:1},200,function(){console.log("fin animacion");});
                            });
                            
                          }                        
                      });

                      $("#menubar-siguiente").click(function(e){
                          e.preventDefault();

                          rotacion=0;
                          $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});

                          if(imgSeleccionada<repuesta.length-1){
                            imgSeleccionada++;
                            $("#img-max img").animate({opacity:0},200,function(){
                              var img=$("#list-img img[name='"+imgSeleccionada+"']").clone();
                            $("#title-img").html("Imagen "+(imgSeleccionada+1)+" de "+repuesta.length);                   
                            $("#img-max").html(img);
                            zoom=50;
                            $("#img-max img").css("width",zoom+"%");
                            $("#img-max img").animate({opacity:1},200,function(){console.log("fin animacion");});
                            });
                            
                          }                        
                      });

                      $("#menubar-rotarIzq").click(function(e){
                          e.preventDefault();

                          if(rotacion>-360){
                            rotacion=rotacion-90;
                             $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
                            
                          }else{
                            rotacion=0;
                            $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
                          }  
                          //console.log(rotacion);                
                      });

                      $("#menubar-rotarDer").click(function(e){
                          e.preventDefault();

                          if(rotacion<360){
                            rotacion=rotacion+90;
                             $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
                             
                          }else{
                            rotacion=0;
                            $("#img-max img").css({"transform":"rotate ("+rotacion+"deg)","-moz-transform":"rotate("+rotacion+"deg)","-webkit-transform":"rotate("+rotacion+"deg)","-o-transform":"rotate("+rotacion+"deg)","-ms-transform":"rotate("+rotacion+"deg)"});
                          } 
                          //console.log(rotacion);                
                      });

                      //EVENTO CLIC PARA CADA IMAGEN DEL CARRUSEL
                      $("#list-img img").click(function(e){
                        e.preventDefault();
                        //$("#list-img img").removeClass("img-selected");
                        var img=$(this).clone();                   
                        $("#img-max").html(img);
                        //$(this).addClass("img-selected");
                        $("#title-img").html("Imagen "+(+$(this).attr("name")+1)+" de "+repuesta.length);
                        imgSeleccionada=+$(this).attr("name");
                        $("#galeria").show();
                                            
                      });//fin del evento clic de las imagenes del carrusel

                      
                      $("#carrusel").show();
                      $("#contenido").show();
                      $("#modal").animate({right:'-100%'},500); 
                    }//fin del if si hay resultado
                    else{
                      $("#list-img").html("No hay fotos para el acta "+acta);
                       
                    }
                    
                });//fin del done
        };

        function dibujarDistancia(latini,lonini,latfin,lonfin){
              
              var distancia = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(latini, lonini), new google.maps.LatLng(latfin, lonfin));

              
              

              var lon1 = lonini*Math.PI / 180;
              var lon2 = lonfin*Math.PI / 180;

              var lat1 = latini* Math.PI / 180;
              var lat2 = latfin* Math.PI / 180;

              var dLon = lon2 - lon1;

              var x = Math.cos(lat2) * Math.cos(dLon);
              var y = Math.cos(lat2) * Math.sin(dLon);

              var lat3 = Math.atan2( Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + x) * (Math.cos(lat1) + x) + y * y) );
              var lon3 = lon1 + Math.atan2(y, Math.cos(lat1) + x);

              center_lat  = lat3 * 180 / Math.PI;
              center_lon = lon3 * 180 / Math.PI;

              map.drawOverlay({
                lat:center_lat,
                lng:center_lon,
                content:"<div class='overlay'>"+Math.round(distancia)+" mts.</div>"

             });
        };// fin de dibujarDistancia()

        function borrarMediciones(){
                
                //if(!($("#chk-medir").prop("checked"))){// si des seleccionamos la casilla de medir
                  
                  //recorrer el array map.markers desde la posicion markerRetornados(desde donde se empezaron a agregar marker para medir)
                  for(var i=markerRetornados ; i< map.markers.length; i++ ){
                      map.markers[i].setMap(null);
                  }//fin del for map.markers
                  
                  //reseteamos el array ptsMedir
                  ptsMedir=[];
                  ptsArea=[];


                  //recorrer todas la polylines de mapa
                  for(var i=0; i<map.polylines.length; i++){
                    map.polylines[i].setMap(null);
                  }//fin del for map.markers

                  map.removeOverlays();

                  if(poligonoArea)
                    poligonoArea.setMap(null);


                  $("#txtArea").val("");
                  if($("#chk-medirArea").prop("checked"))
                      $("#divArea").show();
                  else
                    $("#divArea").hide();

                //} //fin de if !checked                 
        }; //fin de borrarMediciones()

        function remedirDistancias(){

          if($("#chk-medirLineal").prop("checked")){

            //recorrer todas la polylines de mapa para borrarlas
              for(var i=0; i<map.polylines.length; i++){
                map.polylines[i].setMap(null);
              }//fin del for map.markers


              //removeemos todos las overlays(donde aparecen las distancias)
              map.removeOverlays();


              if(ptsMedir.length>1){//si hay mas de 1 marker en el mapa
                  
                  for(var i=0;i<ptsMedir.length - 1;i++){
                    //trazar polylinea entre el el marker anterior y el actual
                    map.drawPolyline({

                      path: [[ptsMedir[i].lat,ptsMedir[i].lng],[ptsMedir[i+1].lat,ptsMedir[i+1].lng]],
                       strokeColor: '#ff0000',//color[c],
                       strokeOpacity: 0.6,
                       strokeWeight: 3,
                       name:"medir"

                    });//fin de dibujar polyline

                    //dibujamos la distancia entre los marker anterior y actual
                    dibujarDistancia(ptsMedir[i].lat,ptsMedir[i].lng,ptsMedir[i+1].lat,ptsMedir[i+1].lng);

                    //calcular area entre los puntos
                    /*if(ptsArea.length > 2){
                      var area = google.maps.geometry.spherical.computeArea(ptsArea);
                      console.log(area);
                      console.log(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','})+" m2");  

                      if(poligonoArea){
                        poligonoArea.setMap(null);
                      }
                      
                      poligonoArea = new google.maps.Polygon({
                        paths: ptsMedir,
                        strokeColor: '#ff0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#ff0000',
                        fillOpacity: 0.35
                      });
                      poligonoArea.setMap(map.map);
                    }*/

                  }//fin del for
               
              }//fin del if

          }
          else if($("#chk-medirArea").prop("checked")){

            if(poligonoArea)
              poligonoArea.setMap(null);


            //calcular area entre los puntos
              if(ptsArea.length > 2){
                
                var area = google.maps.geometry.spherical.computeArea(ptsArea);
                //console.log(area);
                //console.log(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','})+" m2");

                $("#txtArea").val(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','}));  

                if(poligonoArea){
                  poligonoArea.setMap(null);
                }
                
                poligonoArea = new google.maps.Polygon({
                  paths: ptsMedir,
                  strokeColor: '#ff0000',
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: '#ff0000',
                  fillOpacity: 0.35
                });
                poligonoArea.setMap(map.map);
              }
          }
          else if($("#chk-dibujarPuntos").prop("checked")){

          }
          else if($("#chk-dibujarLineas").prop("checked")){

            //recorrer todas la polylines de mapa para borrarlas
              for(var i=0; i<map.polylines.length; i++){
                map.polylines[i].setMap(null);
              }//fin del for map.markers


              //removeemos todos las overlays(donde aparecen las distancias)
              map.removeOverlays();


              if(ptsMedir.length>1){//si hay mas de 1 marker en el mapa
                  
                  for(var i=0;i<ptsMedir.length - 1;i++){
                    //trazar polylinea entre el el marker anterior y el actual
                    map.drawPolyline({

                      path: [[ptsMedir[i].lat,ptsMedir[i].lng],[ptsMedir[i+1].lat,ptsMedir[i+1].lng]],
                       strokeColor: '#ff0000',//color[c],
                       strokeOpacity: 0.6,
                       strokeWeight: 3,
                       name:"medir"

                    });//fin de dibujar polyline

                    //dibujamos la distancia entre los marker anterior y actual
                    //dibujarDistancia(ptsMedir[i].lat,ptsMedir[i].lng,ptsMedir[i+1].lat,ptsMedir[i+1].lng);

                    

                  }//fin del for
               
              }//fin del if
          }
          else if($("#chk-dibujarPoligono").prop("checked")){

            if(poligonoArea)
              poligonoArea.setMap(null);


            //calcular area entre los puntos
              if(ptsArea.length > 2){
                
                if(poligonoArea){
                  poligonoArea.setMap(null);
                }
                
                poligonoArea = new google.maps.Polygon({
                  paths: ptsMedir,
                  strokeColor: '#ff0000',
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: '#ff0000',
                  fillOpacity: 0.35
                });
                poligonoArea.setMap(map.map);
              }
          }

      
        };//fin de remedirDistancias()
        
        function setMunicipio(Dpto){
            $.ajax({
                  url:'request/getMunicipios.php',
                  type:'POST',
                  dataType:'json',
                  data:{dpto:Dpto}
                      
            }).done(function(repuesta){
                  
                  if(repuesta.length>0){
                      var options="";
                      
                      for(var i=0;i<repuesta.length;i++){
                        options+="<option value='"+repuesta[i].value+"'>"+repuesta[i].label+"</option>";
                      }
                      
                      $("#municipio").html(options);
                      $("#municipio").attr("disabled",false);
                      $("#consultar").show();
                      //setCorregimiento(Dpto,$("#municipio").val());
                  }
              
            });

        };// fin de la funcion setMunicipio
        //=================== inicializar componentes ============================

        function iniciarComponentes(){

            $("#divArea").hide();
            
            //ajax que carga la lista de departamentos
            $.ajax({
                    url:'request/getDepartamentos.php',
                    type:'POST',
                    dataType:'json'
                        
              }).done(function(repuesta){
                
                if(repuesta.length>0){
                    var optionsDpto="";
                    var optionsMun="";
                    
                    for(var i=0;i<repuesta.length;i++){
                        optionsDpto+="<option value='"+repuesta[i].value+"'>"+repuesta[i].label+"</option>";
                      
                    }
                    
                    $("#departamento").html(optionsDpto);
                    $('#departamento').attr('disabled',false);
                    
                    setMunicipio($("#departamento").val());
                  
                }
                
                
            });// fin del ajax departamentos


            //Listeners de el evento Change de las listas departamentos
            $("#departamento").on("change",function(){

                $("#consultar").hide();

                $("#municipio").html("");
                $("#municipio").attr("disabled",true);

                
                setMunicipio($("#departamento").val());
              }); 
            //fin de Listeners de el evento Change de la lista departamentos

            
            
            //Evento change de chk- show-hide-polylines
            /*$("#show-hide-polylines").on("change",function(){
                if($("#show-hide-polylines").prop("checked")){
                  for(var i in map.polylines){
                    map.polylines[i].setVisible(true);
                  }
                }                   
                else{
                  for(var i in map.polylines){
                    map.polylines[i].setVisible(false);
                  }
                }

            });*///fin del Evento change de chk- show-hide-polylines


            //Evento change de chk-medir
            //$("#chk-medir").on("change",borrarMediciones);// fin del evento change de chk-medir
            
            $("#chk-medirLineal").on("change",borrarMediciones);// fin del evento change de chk-medirLineal
            $("#chk-medirArea").on("change",borrarMediciones);// fin del evento change de chk-medirArea
            $("#chk-dibujarPuntos").on("change",borrarMediciones);// fin del evento change de chk-dibujarPuntos
            $("#chk-dibujarLineas").on("change",borrarMediciones);// fin del evento change de chk-dibujarLineas
            $("#chk-dibujarPoligono").on("change",borrarMediciones);// fin del evento change de chk-dibujarPoligono

                     
              //consultar
             $("#consultar").click(function(event){
                  
                  event.preventDefault();
                  
                  //$("#exportar").hide();
                  $("#consultar").hide();
                  $("#consultando").show();

                  var doneAjax1=doneAjax2= hayRutas=false;

                  
                  $("#detalles table").html("<tr class='title'><td colspan='4'>Resumen</td></tr><tr class='cabecera'><td>Icono</td><td>Nombre Común</td><td>Cantidad</td><td>Ver</td></tr><tr class='fila'><td>Total</td><td colspan='2' id='totalCantidad' style='text-align: left;'>0</td><td id='todas'>Todas</td><td><input type='checkbox' id='chk-show-hide-all-lamparas' checked /></td></tr>");
                  
                  $("#detalles").hide();
                  $("#container-map").hide();
                  $("#list-img").html("");//resetamos la lista de imagenes
                  $("#div-galeria").hide();
                  
                  $("#chk-medir").prop("checked",false);
                  
                  borrarMediciones();

                  map.removeMarkers();
                  map.removePolylines();
                  map.removeOverlays();

                  
                  //$("#show-hide-polylines").attr("disabled",true);
                  //----------------------------------------------------------------------------
                    
                  
                  //------------------------------------------------------------------------

                  
                  //----------------------------------------------------------------------------
                    $.ajax({//ajax tabla detalles
                        url:'request/detalles.php',
                        type:'POST',
                        dataType:'json',
                        data:{mun:$("#municipio").val(),dpto:$("#departamento").val()}

                    }).done(function(repuesta){
                        
                        var filas="<tr class='title'><td colspan='4'>Resumen</td></tr><tr class='cabecera'><td>Icono</td><td>Nombre Común</td><td>Cantidad</td><td>Ver</td></tr><tr class='fila'><td>Total</td><td id='totalCantidad' style='text-align: left;'>0</td><td id='todas'>Todas</td><td><input type='checkbox' id='chk-show-hide-all-lamparas' checked /></td></tr>";
                        
                        if(repuesta.length>0){
            
                            var cantidad=0;

                            for(var i=0; i<repuesta.length;i++){
                              cantidad= cantidad + (+repuesta[i].cantidad);
                              
                              filas=filas+"<tr class='fila'><td><img src='img/"+repuesta[i].codArbol+".png'/></td><td>"+repuesta[i].nombre_comun+"</td><td>"+repuesta[i].cantidad+"</td><td><input type='checkbox' class='show-hide-lamparas' data-cantidad='"+repuesta[i].cantidad+"' checked name='"+repuesta[i].codArbol+"' /></td></tr>";
                            }

                            //console.log(cantidad);


                            $("#detalles table").html(filas);

                            //estableccemos el totalCantidad
                            $("#totalCantidad").html(""+cantidad);



                             //Evento change de chk-show-hide-all-lamparas
                              $("#chk-show-hide-all-lamparas").on("change", function(){
                                  
                                    //capturamos los checkbox de la tabla detalles
                                    var allCheckbox= $(".show-hide-lamparas");
                                    //console.log(allCheckbox);
                                    
                                    if($(this).prop("checked")){// si se selecciono chk-show-hide-all-lamparas
                                                            
                                       //chequeamos todos los checkbox de la tabla y sumamos todas las cantidades
                                       var cantidad=0;

                                        for(var i=0;i<allCheckbox.length;i++){
                                           
                                           allCheckbox[i].checked=true;
                                           cantidad= cantidad + (+allCheckbox[i].dataset['cantidad']);
                                           
                                        }                                 
                                        //console.log(cantidad);
                                        $("#totalCantidad").html(""+cantidad);

                                        //mostramos todos los markers de lamparas
                                        for(var i=0; i < markerRetornados; i++){
                                            map.markers[i].setVisible(true);
                                        }
                                        
                                     }//fin del si                       
                                    else{// si se deselecciono chk-show-hide-all-lamparas

                                       //reseteamos el totalCantidad a cero (0)
                                       $("#totalCantidad").html("0");

                                       //deschequeamos todos los checkbox de la tabla
                                        for(var i in allCheckbox){
                                           allCheckbox[i].checked=false;
                                        }                                 

                                        //mostramos todos los markers de lamparas
                                        for(var i=0; i < markerRetornados; i++){
                                            map.markers[i].setVisible(false);
                                        }
                                    }// fin del else
                                       
                                  
                              });//fin del evento change de chk-show-hide-all-lamparas

                              //evento change de cada chk-show-hide-lamparas
                              $(".show-hide-lamparas").on("change",function(){
                                  
                                    if($(this).prop("checked")){

                                      var totalCantidad = +($("#totalCantidad").html());
                                      var cantidad = +($(this).attr("data-cantidad"));

                                      totalCantidad = totalCantidad + cantidad;

                                      $("#totalCantidad").html(""+totalCantidad);

                                      for(var i in map.markers){

                                        if(map.markers[i].name==$(this).attr("name"))
                                              map.markers[i].setVisible(true);
                                      }

                                      
                                      //console.log($(this).attr("data-cantidad"));

                                    }                   
                                    else{

                                      var totalCantidad = +($("#totalCantidad").html());
                                      var cantidad = +($(this).attr("data-cantidad"));

                                      totalCantidad = totalCantidad - cantidad;

                                      $("#totalCantidad").html(""+totalCantidad);

                                      for(var i in map.markers){
                                        if(map.markers[i].name==$(this).attr("name"))
                                              map.markers[i].setVisible(false);
                                      }//fin del for
                                      
                                    }//fin del else

                              });// fin de la funcion callback

                            
                        }
                        else{

                            filas=filas+"<tr class='fila'><td colspan='4'>Vacío</td></tr>";
                            $("#detalles table").html(filas);
                        }

                        $("#detalles").show();

                        doneAjax1=true;

                        if(doneAjax1 && doneAjax2){

                            if(hayRutas){

                              //$("#exportar").show();

                            }else{
                              
                              //$("#exportar").hide();
                            }
                            
                            $("#consultando").hide();

                            $("#consultar").show();
                             $("#div-galeria").show();
                        }

                    });


                   //------------------------------------------------------------------------

                  $.ajax({//ajax que dibuja la ruta
                        url:'request/dibujarRuta.php',
                        type:'POST',
                        dataType:'json',
                        data:{mun:$("#municipio").val(),dpto:$("#departamento").val()}

                    }).done(function(repuesta){
                      
                        if(repuesta.length>0){// si hay cordenadas que mostrar

                            hayRutas = true;

                            //$("#exportar a").attr("href","request/exportarExcel.php?dpto="+$('#departamento').val()+"&mun="+$('#municipio').val()+"&departamento="+$("#departamento option:selected").text()+"&municipio="+$("#municipio option:selected").text());
            
                            
                            
                            //guardamos la cantidad de markers retornados por la consulta
                            //para saber desde que posicion del array map.markers remover los marker
                            //agregados con la opcion de medir distancia
                            markerRetornados = repuesta.length;
                            //polylinesRetornadas = repuesta.length - 1;

                            map.setCenter(repuesta[0].latIni, repuesta[0].lonIni);
                            
                            
                            //var color=new Array('red','yellow','blue','pink','orange','green','black','Fuchsia','GreenYellow','Lime','Magenta','Purple');
                            
                            map.addMarker({lat:repuesta[0].latIni, lng:repuesta[0].lonIni, title: repuesta[0].acta, icon:'img/'+repuesta[0].codArbol+'.png', infoWindow:{content:repuesta[0].infoWin}, name:repuesta[0].codArbol, 'data-acta':repuesta[0].acta, click:function(e){
                                
                                listarEvidencias($(this).attr("data-acta"));
                              }
                            });

                            google.maps.event.addListener(map.markers[0].infoWindow,'closeclick',function(){
                                $("#list-img").html("");
                                
                              });
                            
                            for(var i=1; i<repuesta.length;i++){
                              //var c=Math.floor(Math.random()*12);
                                                            
                              map.addMarker({lat:repuesta[i].latIni, lng:repuesta[i].lonIni, title:repuesta[i].acta, icon:'img/'+repuesta[i].codArbol+'.png', infoWindow:{content:repuesta[i].infoWin}, name:repuesta[i].codArbol, 'data-acta':repuesta[i].acta, click:function(e){
                                
                                  listarEvidencias($(this).attr("data-acta"));
                                  
                                }
                              });

                              google.maps.event.addListener(map.markers[i].infoWindow,'closeclick',function(){
                                $("#list-img").html("");

                              });

                               /*map.drawPolyline({
                                 path: [[repuesta[i].latIni,repuesta[i].lonIni],[repuesta[i].latAnt,repuesta[i].lonAnt]],
                                 strokeColor: '#000080',
                                 strokeOpacity: 0.6,
                                 strokeWeight: 6
                               });

                               map.polylines[i-1].setVisible(false);*/
                               
                            }//fin del for
                            
                            //console.log(map.markers[0].name);
                            $("#aceptar").button({disabled:false});
                            $("#show-hide-polylines").attr("disabled",false);
                          
                        }//fin del si hay coordenadas para mostrar
                        else
                        {//si no hay coordenadas que mostrar
                          //$("#imprimir").button({disabled:true});
                          hayRutas=false;

                          //$("#exportar a").attr("href","");

                          alert("En este municipio no hay rutas para dibujar!");
                        }//fin del si no hay cordenadas para mostrar

                        $("#container-map").show();
                        doneAjax2=true;

                        if(doneAjax1 && doneAjax2){
                            
                            if(hayRutas){

                              //$("#exportar").show();

                            }else{
                              
                              //$("#exportar").hide();
                            }


                            $("#consultando").hide();
                            $("#consultar").show();
                             $("#div-galeria").show();
                            //$("#contenido").show();
                        }
                    });//fin done Ajax que dibuja las rutas
                    //----------------------------------------------------------------------------

              });//fin del boton aceptar
                

              $("#btnGaleria").click(function(e){

                e.preventDefault();

                $("#galeria").show();
              });
              //$("#consultar").show();
              
              //$("#imprimir").button({disabled:true});
              
              //============ boton Aceptar fin =============================
              $(".ui-autocomplete").addClass("ui-autocomplete");

        };//fin initiComponents()
        //=================== inicializar componentes FIN ========================


        //===================================================================================

          function cargarMapa(){
              /*GMaps.geolocate({
                  success: function(position){*/
                      
                         map = new GMaps({  // muestra mapa centrado en monteria coords [lat, lng]
                            el: '#container-map',
                            lat: 8.749349,/*position.coords.latitude,*/
                            lng: -75.879568,/*position.coords.longitude,*/
                            zoom:16,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                position: google.maps.ControlPosition.TOP_RIGHT
                            },
                            zoomControlOptions:{position: google.maps.ControlPosition.TOP_RIGHT},
                            streetView:null,                          
                                                        
                            rightclick:function(e){
                              
                              if($("#chk-medirLineal").prop("checked")){
                                   //agregamos un marker en el mapa
                                    map.addMarker({
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng(),
                                      name: ptsMedir.length,
                                      label:{text:""+ptsMedir.length, color:"white",fontSize:"6px"},
                                      icon:'img/2.png',
                                      draggable:true,
                                      drag:function(e){
                                          //console.log(this.getLabel().text);
                                          //console.log(ptsMedir[this.getLabel()]);
                                          //actualizamos la coordenada almacenada en la posicion i (marker arrastrado) por la que arroja el evento dragend


                                          ptsMedir[+this.getLabel().text]={
                                            lat:e.latLng.lat(),
                                            lng:e.latLng.lng()
                                          };

                                          ptsArea[+this.getLabel().text]=new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});
                                          
                                          remedirDistancias();
                                      }//fin de funcion dragend()
                                    });//fiin de map.addMarker()

                                    //agregamos al array de puntos el marker actual
                                    ptsMedir[ptsMedir.length]={
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng()
                                    };

                                    //agregamos al array de puntos el marker actual
                                    ptsArea.push(new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()}));



                                    if(ptsMedir.length>1){//si hay mas de 1 marker en el mapa
                                      map.drawPolyline({

                                        path: [[ptsMedir[ptsMedir.length-2].lat,ptsMedir[ptsMedir.length-2].lng],[ptsMedir[ptsMedir.length-1].lat,ptsMedir[ptsMedir.length-1].lng]],
                                         strokeColor: '#ff0000',//color[c],
                                         strokeOpacity: 0.6,
                                         strokeWeight: 3,
                                         name:"medir"

                                      });//fin de dibujar polyline

                                      //dibujamos la distancia entre los marker anterior y actual
                                      dibujarDistancia(ptsMedir[ptsMedir.length-2].lat,ptsMedir[ptsMedir.length-2].lng,ptsMedir[ptsMedir.length-1].lat,ptsMedir[ptsMedir.length-1].lng);

                                    }//fin del if

                                    //calcular area entre los puntos
                                    /*if(ptsArea.length > 2){
                                      var area = google.maps.geometry.spherical.computeArea(ptsArea);
                                      console.log(area);  
                                      console.log(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','})+" m2");  

                                      if(poligonoArea){
                                        poligonoArea.setMap(null);
                                      }
                                      
                                      poligonoArea = new google.maps.Polygon({
                                        paths: ptsMedir,
                                        strokeColor: '#ff0000',
                                        strokeOpacity: 1,
                                        strokeWeight: 2,
                                        fillColor: '#ff0000',
                                        fillOpacity: 0.35
                                      });
                                      poligonoArea.setMap(map.map);
                                    }*/
                                    
                              }//fin del if que comprueba que el chk-medir este seleccionado
                              else if($("#chk-medirArea").prop("checked")){

                                //agregamos un marker en el mapa
                                    map.addMarker({
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng(),
                                      name: ptsMedir.length,
                                      label:{text:""+ptsMedir.length, color:"white",fontSize:"6px"},
                                      icon:'img/2.png',
                                      draggable:true,
                                      drag:function(e){
                                          //console.log(this.getLabel().text);
                                          //console.log(ptsMedir[this.getLabel()]);
                                          //actualizamos la coordenada almacenada en la posicion i (marker arrastrado) por la que arroja el evento dragend


                                          ptsMedir[+this.getLabel().text]={
                                            lat:e.latLng.lat(),
                                            lng:e.latLng.lng()
                                          };

                                          ptsArea[+this.getLabel().text]=new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});
                                          
                                          remedirDistancias();
                                      }//fin de funcion dragend()
                                    });//fiin de map.addMarker()

                                    //agregamos al array de puntos el marker actual
                                    ptsMedir[ptsMedir.length]={
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng()
                                    };

                                    //agregamos al array de puntos el marker actual
                                    ptsArea.push(new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()}));

                                //calcular area entre los puntos
                                    if(ptsArea.length > 2){
                                      
                                      var area = google.maps.geometry.spherical.computeArea(ptsArea);
                                      //console.log(area);  
                                      //console.log(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','})+" m2");  
                                      
                                      $("#txtArea").val(accounting.formatMoney(area, {symbol : '', precision : 2,thousand : ','}));
                                      
                                      if(poligonoArea){
                                        poligonoArea.setMap(null);
                                      }
                                      
                                      poligonoArea = new google.maps.Polygon({
                                        paths: ptsMedir,
                                        strokeColor: '#ff0000',
                                        strokeOpacity: 1,
                                        strokeWeight: 2,
                                        fillColor: '#ff0000',
                                        fillOpacity: 0.35
                                      });
                                      poligonoArea.setMap(map.map);
                                    }
                              }
                              else if($("#chk-dibujarPuntos").prop("checked")){

                                //agregamos un marker en el mapa
                                    map.addMarker({
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng(),
                                      name: ptsMedir.length,
                                      label:{text:""+ptsMedir.length, color:"white",fontSize:"6px"},
                                      icon:'img/2.png',
                                      draggable:true,
                                      drag:function(e){
                                          //console.log(this.getLabel().text);
                                          //console.log(ptsMedir[this.getLabel()]);
                                          //actualizamos la coordenada almacenada en la posicion i (marker arrastrado) por la que arroja el evento dragend


                                          ptsMedir[+this.getLabel().text]={
                                            lat:e.latLng.lat(),
                                            lng:e.latLng.lng()
                                          };

                                          ptsArea[+this.getLabel().text]=new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});
                                          
                                          remedirDistancias();
                                      }//fin de funcion dragend()
                                    });//fiin de map.addMarker()

                                    //agregamos al array de puntos el marker actual
                                    ptsMedir[ptsMedir.length]={
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng()
                                    };

                                    //agregamos al array de puntos el marker actual
                                    ptsArea.push(new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()}));
                              }
                              else if($("#chk-dibujarLineas").prop("checked")){

                                map.addMarker({
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng(),
                                      name: ptsMedir.length,
                                      label:{text:""+ptsMedir.length, color:"white",fontSize:"6px"},
                                      icon:'img/2.png',
                                      draggable:true,
                                      drag:function(e){
                                          //console.log(this.getLabel().text);
                                          //console.log(ptsMedir[this.getLabel()]);
                                          //actualizamos la coordenada almacenada en la posicion i (marker arrastrado) por la que arroja el evento dragend


                                          ptsMedir[+this.getLabel().text]={
                                            lat:e.latLng.lat(),
                                            lng:e.latLng.lng()
                                          };

                                          ptsArea[+this.getLabel().text]=new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});
                                          
                                          remedirDistancias();
                                      }//fin de funcion dragend()
                                    });//fiin de map.addMarker()

                                    //agregamos al array de puntos el marker actual
                                    ptsMedir[ptsMedir.length]={
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng()
                                    };

                                    //agregamos al array de puntos el marker actual
                                    ptsArea.push(new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()}));



                                    if(ptsMedir.length>1){//si hay mas de 1 marker en el mapa
                                      map.drawPolyline({

                                        path: [[ptsMedir[ptsMedir.length-2].lat,ptsMedir[ptsMedir.length-2].lng],[ptsMedir[ptsMedir.length-1].lat,ptsMedir[ptsMedir.length-1].lng]],
                                         strokeColor: '#ff0000',//color[c],
                                         strokeOpacity: 0.6,
                                         strokeWeight: 3,
                                         name:"medir"

                                      });//fin de dibujar polyline

                                      //dibujamos la distancia entre los marker anterior y actual
                                      //dibujarDistancia(ptsMedir[ptsMedir.length-2].lat,ptsMedir[ptsMedir.length-2].lng,ptsMedir[ptsMedir.length-1].lat,ptsMedir[ptsMedir.length-1].lng);

                                    }//fin del if
                              }
                              else if($("#chk-dibujarPoligono").prop("checked")){

                                //agregamos un marker en el mapa
                                    map.addMarker({
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng(),
                                      name: ptsMedir.length,
                                      label:{text:""+ptsMedir.length, color:"white",fontSize:"6px"},
                                      icon:'img/2.png',
                                      draggable:true,
                                      drag:function(e){
                                          //console.log(this.getLabel().text);
                                          //console.log(ptsMedir[this.getLabel()]);
                                          //actualizamos la coordenada almacenada en la posicion i (marker arrastrado) por la que arroja el evento dragend


                                          ptsMedir[+this.getLabel().text]={
                                            lat:e.latLng.lat(),
                                            lng:e.latLng.lng()
                                          };

                                          ptsArea[+this.getLabel().text]=new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});
                                          
                                          remedirDistancias();
                                      }//fin de funcion dragend()
                                    });//fiin de map.addMarker()

                                    //agregamos al array de puntos el marker actual
                                    ptsMedir[ptsMedir.length]={
                                      lat:e.latLng.lat(),
                                      lng:e.latLng.lng()
                                    };

                                    //agregamos al array de puntos el marker actual
                                    ptsArea.push(new google.maps.LatLng({lat: e.latLng.lat(), lng: e.latLng.lng()}));

                                //calcular area entre los puntos
                                    if(ptsArea.length > 2){
                                      
                                      if(poligonoArea){
                                        poligonoArea.setMap(null);
                                      }
                                      
                                      poligonoArea = new google.maps.Polygon({
                                        paths: ptsMedir,
                                        strokeColor: '#ff0000',
                                        strokeOpacity: 1,
                                        strokeWeight: 2,
                                        fillColor: '#ff0000',
                                        fillOpacity: 0.35
                                      });
                                      poligonoArea.setMap(map.map);
                                    }
                              }  
                            }//fin del evento clickRight del mapa
                          });

                         map.getStreetView().setOptions({addressControlOptions: {position: google.maps.ControlPosition.TOP_RIGHT}});
                         
                            
                          iniciarComponentes();                                      
                  /*},
                  error: function(error){
                     alert('Fallo en la geolocalizacion:'+error.message);
                     return false;
                  },
                  not_supported:function(){
                    alert('Geolocalizacion no soportada');
                    return false;
                  }
              });*/
          };
        //======================================================================================



        //=========== Run ======================================================================
        cargarMapa();


        
  		});//fin del onready
  	</script>
</head>

  <body>

      <header>
        <h3>Árboles por municipio</h3>
        <nav>
          <ul id="menu">
            
            <!--<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>-->
            <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
            <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
              
          </ul>
        </nav>
      </header>



      <div id='subheader'>

          <div id="div-form">

              <form id="form">

                  <div><label>Departamento</label><select type="text" id="departamento" autofocus></select></div>

                  <div><label>Municipio</label><select type="text" id="municipio"></select></div>
                  
                  <!--<div><input type='checkbox' id='show-hide-polylines' disabled /><span>Ver líneas</span></div>-->
                  <!--<div><input type='checkbox' id='chk-medir' /><span>Medir</span></div>-->
                  
                  <div>
                    <fieldset>
                      <legend>Medir</legend>
                      <label><input type='radio' id='chk-medirLineal' name='chk-opcion' checked="true" /> Lineal</label>
                      <label><input type='radio' id='chk-medirArea' name='chk-opcion' /> Area</label>
                    </fieldset>
                  </div>

                  <div>
                    <fieldset>
                      <legend>Dibujar</legend>
                      <label><input type='radio' id='chk-dibujarPuntos' name='chk-opcion' /> Puntos</label>
                      <label><input type='radio' id='chk-dibujarLineas' name='chk-opcion' /> Líneas</label>
                      <label><input type='radio' id='chk-dibujarPoligono' name='chk-opcion' /> Polígono</label>
                    </fieldset>
                  </div>

                  <div id="divArea"><label>Área (m<sup>2</sup>)</label><input type="text" id="txtArea" readonly="true"></div>                  

              </form>

          </div>
      </div>



      <div id="div-galeria">
        
          <div class="wrapper">
            
            <div id='carrusel'>
                <!--<a href='#' id='carrusel-anterior'></a>
                <a href='#' id='carrusel-siguiente'></a>-->
                <div id="list-img"></div>
              </div>
                
              
              <div id="galeria">
                  
                  <div id="menubar">
                    <span id="title-img"></span>
                    <a href="#" title="anterior" id="menubar-anterior" class="ion-ios-arrow-thin-left"></a>
                    <a href="#" title="siguiente" id="menubar-siguiente" class="ion-ios-arrow-thin-right"></a>
                    <a href="#" title="disminuir" id="menubar-disminuir" class="ion-ios-minus-empty"></a>
                    <a href="#" title="aumentar" id="menubar-aumentar" class="ion-ios-plus-empty"></a>
                    <a href="#" title="Rotar -45º" id="menubar-rotarIzq" class="ion-ios-undo-outline"></a>
                    <a href="#" title="Rotar 45º" id="menubar-rotarDer" class="ion-ios-redo-outline"></a>
                    <a href="#" title="cerrar" id="menubar-cerrar" class="ion-ios-close-empty"></a>
                </div>

                <div id="img-max"></div>

              </div>
              
          </div>
      </div>


      <div id="detalles">
        <table>
          <tr class='title'>
            <td colspan='4'>Resumen</td>
          </tr>
          <tr class='cabecera'><td>Icono</td><td>Nombre Común</td><td>Cantidad</td><td>Ver</td></tr>
          
        </table>
      </div>


      <div id="container-map"></div>

  </body>
</html>