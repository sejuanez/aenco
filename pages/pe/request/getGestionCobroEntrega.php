<?php
    // session_start();
    // if(!$_SESSION['user']){
    //   echo"<script>window.location.href='../../inicio/index_.php';</script>";
    //   exit();
    // }
    include('../../../init/gestion.php');

    $fechai = $_POST['fechai'];
    $fechaf = $_POST['fechaf'];
    $delegacion = $_POST["delegacion"];

	$param = "";

	if (!empty($delegacion)) {
		$param = "AND delegacion = '$delegacion' ";
	}

    $query = "SELECT co.t_area_contratacion Delegacion, co.t_cod_contrata Contratista, gc.campana Campana,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.f_entrega) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.f_entrega) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.f_entrega) AS INTEGER) + 10000 FROM 2 FOR 4) F_Entrega,
    gc.nic Nic, gc.nis NisRad,
    SUBSTRING(CAST (EXTRACT(DAY FROM lc.ca_fechaej) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM lc.ca_fechaej) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM lc.ca_fechaej) AS INTEGER) + 10000 FROM 2 FOR 4) F_Gestion,
    lc.ca_codtipoordens T_Gestion, du.ui_codigo Resultado, dua.ui_codigo Anomalia, pp.pp_nombre Entidad_Recaudo,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.fecha_pago) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.fecha_pago) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.fecha_pago) AS INTEGER) + 10000 FROM 2 FOR 4) F_Pago,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.fecha_proxima_visita) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.fecha_proxima_visita) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.fecha_proxima_visita) AS INTEGER) + 10000 FROM 2 FOR 4) F_Com_Pago,
    UPPER(dc.dc_observa) Pers_Contacto, COALESCE(gc.cedula, 0) Cedula, COALESCE(gc.cliente,'0') Tit_Pago, gc.telefono_actual Telefono,
     CASE
      WHEN gc.correoe = '' THEN '0' ELSE COALESCE(gc.correoe, '0')
     END
     Email,
    (COALESCE(
    ( COALESCE(r.r_descripcion,'')||' '||
    CASE
        WHEN
            ((gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio) > 0) THEN gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio
        WHEN
            ((gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio) = 0) THEN UPPER(lc.ca_observa)
    END
    ||' '|| COALESCE( EXTRACT(DAY FROM gc.fecha_pago ), EXTRACT (DAY FROM gc.fecha_proxima_visita)  )||'/'||
    COALESCE( EXTRACT(month FROM gc.fecha_pago ),EXTRACT (MONTH FROM gc.fecha_proxima_visita))||'/'||
    COALESCE( EXTRACT(year FROM gc.fecha_pago ),EXTRACT (YEAR FROM gc.fecha_proxima_visita))||
    ' '||COALESCE(pp.pp_nombre,'')  ), UPPER(lc.ca_observa))) Observaciones,
    gc.lectura Lectura_Firma, t.te_nombres Gestor_Cobro, lc.ca_horaej Hora_Duraccion,
    'Lon: '||COALESCE(gc.longitud,'0')||' * '||'Lat: '||COALESCE(gc.latitud,'0') Punto_GPS
    FROM lega_cabecera lc
    INNER JOIN gestion_cobro_entrega gc ON gc.id = CAST(lc.ca_orden AS NUMERIC(15,2))
    LEFT JOIN dato_unirre du ON du.ui_acta = lc.ca_acta AND du.ui_tipo = 'M'
    LEFT JOIN dato_unirre dua ON dua.ui_acta = lc.ca_acta AND dua.ui_tipo = 'R'
    LEFT JOIN dato_clientes dc ON dc.dc_acta = lc.ca_acta
    LEFT JOIN resultado_scr r ON r.r_codigo = du.ui_codigo AND r.r_tiporden = lc.ca_codtipoordens
    LEFT JOIN acciones a ON a.ac_tiporden = lc.ca_codtipoordens AND a.ac_motivo = du.ui_codigo
    AND a.ac_codigo = dua.ui_codigo
    LEFT JOIN tecnicos t ON t.te_codigo = gc.tecnico
    LEFT JOIN puntos_pago pp ON pp.pp_codigo = gc.punto_pago
    LEFT JOIN tipo_orden_servicio tos ON tos.to_codigo = gc.tipo_orden
    LEFT JOIN contratistas co ON co.t_nomcontrata = gc.contratista AND co.t_delegacion = gc.delegacion
    WHERE lc.ca_fechaej BETWEEN '$fechai' AND '$fechaf' $param";
    $data = ibase_query($conexion, $query);

    $return_arr = array();
    while ($row = ibase_fetch_row($data)) {
        $row_array['delegacion']      = utf8_encode($row[0]);
        $row_array['contratista']     = utf8_encode($row[1]);
        $row_array['campana']         = utf8_encode($row[2]);
        $row_array['fecha_entrega']   = utf8_encode($row[3]);
        $row_array['nic']             = utf8_encode($row[4]);
        $row_array['nis_rad']         = utf8_encode($row[5]);
        $row_array['fecha_gestion']   = utf8_encode($row[6]);
        $row_array['t_gestion']       = utf8_encode($row[7]);
        $row_array['resultado']       = utf8_encode($row[8]);
        $row_array['anomalia']        = utf8_encode($row[9]);
        $row_array['entidad_recaudo'] = utf8_encode($row[10]);
        $row_array['forma_pago']      = utf8_encode($row[11]);
        $row_array['fecha_com_pago']  = utf8_encode($row[12]);
        $row_array['pers_contrato']   = utf8_encode($row[13]);
        $row_array['cedula']          = utf8_encode($row[14]);
        $row_array['tit_pago']        = utf8_encode($row[15]);
        $row_array['telefono']        = utf8_encode($row[16]);
        $row_array['email']           = utf8_encode($row[17]);
        $row_array['observaciones']   = utf8_encode($row[18]);
        $row_array['lectura_firma']   = utf8_encode($row[19]);
        $row_array['gestor_cobro']    = utf8_encode($row[20]);
        $row_array['hora_duracion']   = utf8_encode($row[21]);
        $row_array['punto_gps']       = utf8_encode($row[22]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>