<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT DISTINCT t_delegacion FROM contratistas";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['t_delegacion'] = utf8_encode($row[0]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>