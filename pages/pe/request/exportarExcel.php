<?php

	// session_start();
  	// if(!$_SESSION['user']){
    //     echo"<script>window.location.href='../../inicio/index.php';</script>";
    //     exit();
    // }
    
    // header("Content-Type: application/force-download");
    // header("Content-Transfer-Encoding: binary");
 	// header("Content-disposition: attachment; filename=pe_desde_".str_replace('-',"",$_GET["fechai"])."_hasta_".str_replace('-',"",$_GET["fechaf"]).".xls");
	// header("Pragma: no-cache");
    // header("Expires: 0");
    

    $name_archivo = "pe_desde_".str_replace('/',"_",$_GET["fechai"])."_hasta_".str_replace('/',"_",$_GET["fechaf"])."_".(trim(time())).".txt";
    $filecontent = "";
    header("Content-Type: plain/text"); 
    
    header("Content-disposition: attachment; filename=".$name_archivo);
    //header("Content-Type: application/force-download");
    
   // header("Content-Transfer-Encoding: binary");
    //header("Content-Length: ".strlen($filecontent));
    header("Pragma: no-cache");
   // header("Expires: 0");


	include("../../../init/gestion.php");

	$fechai = $_GET["fechai"];
	$fechaf = $_GET['fechaf'];
	$delegacion = $_GET["delegacion"];

	$param = "";

	if (!empty($delegacion)) {
		$param = "AND delegacion = '$delegacion' ";
	}

	$query = "SELECT co.t_area_contratacion Delegacion, co.t_cod_contrata Contratista, gc.campana Campana,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.f_entrega) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.f_entrega) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.f_entrega) AS INTEGER) + 10000 FROM 2 FOR 4) F_Entrega,
    gc.nic Nic, gc.nis NisRad,
    SUBSTRING(CAST (EXTRACT(DAY FROM lc.ca_fechaej) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM lc.ca_fechaej) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM lc.ca_fechaej) AS INTEGER) + 10000 FROM 2 FOR 4) F_Gestion,
    lc.ca_codtipoordens T_Gestion, du.ui_codigo Resultado, dua.ui_codigo Anomalia, pp.pp_nombre Entidad_Recaudo,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.fecha_pago) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.fecha_pago) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.fecha_pago) AS INTEGER) + 10000 FROM 2 FOR 4) F_Pago,
    SUBSTRING(CAST (EXTRACT(DAY FROM gc.fecha_proxima_visita) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(MONTH FROM gc.fecha_proxima_visita) AS INTEGER) + 100 FROM 2 FOR 2)||'/'||
    SUBSTRING(CAST (EXTRACT(YEAR FROM gc.fecha_proxima_visita) AS INTEGER) + 10000 FROM 2 FOR 4) F_Com_Pago,
    UPPER(dc.dc_observa) Pers_Contacto, COALESCE(gc.cedula, 0) Cedula, COALESCE(gc.cliente,'0') Tit_Pago, gc.telefono_actual Telefono,
     CASE
      WHEN gc.correoe = '' THEN '0' ELSE COALESCE(gc.correoe, '0')
     END
     Email,
    (COALESCE(
    ( COALESCE(r.r_descripcion,'')||' '||
    CASE
        WHEN
            ((gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio) > 0) THEN gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio
        WHEN
            ((gc.valor_pago + gc.vr_c_atrasada + gc.vr_c_convenio) = 0) THEN UPPER(lc.ca_observa)
    END
    ||' '|| COALESCE( EXTRACT(DAY FROM gc.fecha_pago ), EXTRACT (DAY FROM gc.fecha_proxima_visita)  )||'/'||
    COALESCE( EXTRACT(month FROM gc.fecha_pago ),EXTRACT (MONTH FROM gc.fecha_proxima_visita))||'/'||
    COALESCE( EXTRACT(year FROM gc.fecha_pago ),EXTRACT (YEAR FROM gc.fecha_proxima_visita))||
    ' '||COALESCE(pp.pp_nombre,'')  ), UPPER(lc.ca_observa))) Observaciones,
    gc.lectura Lectura_Firma, t.te_nombres Gestor_Cobro, lc.ca_horaej Hora_Duraccion,
    'Lon: '||COALESCE(gc.longitud,'0')||' * '||'Lat: '||COALESCE(gc.latitud,'0') Punto_GPS
    FROM lega_cabecera lc
    INNER JOIN gestion_cobro_entrega gc ON gc.id = CAST(lc.ca_orden AS NUMERIC(15,2))
    LEFT JOIN dato_unirre du ON du.ui_acta = lc.ca_acta AND du.ui_tipo = 'M'
    LEFT JOIN dato_unirre dua ON dua.ui_acta = lc.ca_acta AND dua.ui_tipo = 'R'
    LEFT JOIN dato_clientes dc ON dc.dc_acta = lc.ca_acta
    LEFT JOIN resultado_scr r ON r.r_codigo = du.ui_codigo AND r.r_tiporden = lc.ca_codtipoordens
    LEFT JOIN acciones a ON a.ac_tiporden = lc.ca_codtipoordens AND a.ac_motivo = du.ui_codigo
    AND a.ac_codigo = dua.ui_codigo
    LEFT JOIN tecnicos t ON t.te_codigo = gc.tecnico
    LEFT JOIN puntos_pago pp ON pp.pp_codigo = gc.punto_pago
    LEFT JOIN tipo_orden_servicio tos ON tos.to_codigo = gc.tipo_orden
    LEFT JOIN contratistas co ON co.t_nomcontrata = gc.contratista AND co.t_delegacion = gc.delegacion
    WHERE lc.ca_fechaej BETWEEN '$fechai' AND '$fechaf' $param";
	
	$return_arr = array();

	$data = ibase_query($conexion, $query);

    $filecontent .= "Delegacion|Contratista|#Campaña|F.Entrega|Nic|NisRad|F.Gestion|T.Gestion|Resultado|Anomalia|Entidad Recaudo|F.Pago|F.Com.Pago|Pers.Contacto|Cedula|Tit.Pago|Telefono|Email|Observaciones|Lectura Firma|Gestor Cobro|Hora Duraccion|Punto GPS\n";
   
    // $row = ibase_fetch_row($data);
	while($row = ibase_fetch_row($data)) {
        // $filecontent .=  chr(13).chr(10);
        $filecontent .= "".utf8_encode($row[0]);
        $filecontent .= "|".utf8_encode($row[1]);
        $filecontent .= "|".utf8_encode($row[2]);
        $filecontent .= "|".utf8_encode($row[3]);
        $filecontent .= "|".utf8_encode($row[4]);
        $filecontent .= "|".utf8_encode($row[5]);
        $filecontent .= "|".utf8_encode($row[6]);
        $filecontent .= "|".utf8_encode($row[7]);
        $filecontent .= "|".utf8_encode($row[8]);
        $filecontent .= "|".utf8_encode($row[9]);
        $filecontent .= "|".utf8_encode($row[10]);
        $filecontent .= "|".utf8_encode($row[11]);
        $filecontent .= "|".utf8_encode($row[12]);
        $filecontent .= "|".utf8_encode($row[13]);
        $filecontent .= "|".utf8_encode($row[14]);
        $filecontent .= "|".utf8_encode($row[15]);
        $filecontent .= "|".utf8_encode($row[16]);
        $filecontent .= "|".utf8_encode($row[17]);
        $filecontent .= "|".utf8_encode($row[18]);
        $filecontent .= "|".utf8_encode($row[19]);
        $filecontent .= "|".utf8_encode($row[20]);
        $filecontent .= "|".utf8_encode($row[21]);
        $filecontent .= "|".utf8_encode($row[22]);
        $filecontent .= "\n";


  
  

        /*
        $filecontent['delegacion']      = utf8_encode($row[0]);
        $filecontent['contratista']     = utf8_encode($row[1]);
        $filecontent['campana']         = utf8_encode($row[2]);
        $filecontent['fecha_entrega']   = utf8_encode($row[3]);
        $filecontent['nic']             = utf8_encode($row[4]);
        $filecontent['nis_rad']         = utf8_encode($row[5]);
        $filecontent['fecha_gestion']   = utf8_encode($row[6]);
        $filecontent['t_gestion']       = utf8_encode($row[7]);
        $filecontent['resultado']       = utf8_encode($row[8]);
        $filecontent['anomalia']        = utf8_encode($row[9]);
        $filecontent['entidad_recaudo'] = utf8_encode($row[10]);
        $filecontent['forma_pago']      = utf8_encode($row[11]);
        $filecontent['fecha_com_pago']  = utf8_encode($row[12]);
        $filecontent['pers_contrato']   = utf8_encode($row[13]);
        $filecontent['cedula']          = utf8_encode($row[14]);
        $filecontent['tit_pago']        = utf8_encode($row[15]);
        $filecontent['telefono']        = utf8_encode($row[16]);
        $filecontent['email']           = utf8_encode($row[17]);
        $filecontent['observaciones']   = utf8_encode($row[18]);
        $filecontent['lectura_firma']   = utf8_encode($row[19]);
        $filecontent['gestor_cobro']    = utf8_encode($row[20]);
        $filecontent['hora_duracion']   = utf8_encode($row[21]);
        $filecontent['punto_gps']       = utf8_encode($row[22]);

        array_push($return_arr, $filecontent);
        */
        
    }
    $path = $_SERVER['DOCUMENT_ROOT']."/app/";
    //echo $path;
    //file_put_contents($path.$name_archivo,$filecontent);
    echo $filecontent;
	//echo json_encode($return_arr);
?>