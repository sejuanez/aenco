<!DOCTYPE html>
<html>
<head>
    <title>Traslado entre Bodegas Principales</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .btn-app {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 50px;
            margin-bottom: 10px;
            color: #555;
            font-size: 0.8em;
        }

        .btn-app:hover {
            background: #007bff;
            color: #FFF;
        }

        .btn-app:active {
            background: #009dff;
            color: #FFF;
        }

        label {
            margin-bottom: 0;
        }

        thead {
            font-size: .8em;
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }

        td {
            font-size: .8em;
            cursor: pointer;
        }

        .gj-datepicker input, .gj-datepicker span {
            padding: 5px;
            font-size: .9em;

        }

        input[type="text"] {
            text-transform: uppercase;
        }

        .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
            line-height: initial;
        }

        input[disabled], input[readonly] {
            cursor: not-allowed;
        }

    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Traslado entre Bodegas Principales

        </p>
    </header>

    <div class="container-fluid">

        <form action="" id="form_encabezado">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label for="tecnico">Bodega Origen</label>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control col-3" required name="idtecnOrigen" readonly
                                   v-model="codTecnOri">
                            <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button"
                                            @click="openModalTecnicos('ORIGEN')">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                            <input type="text" class="form-control" required name="nomTecnOri" disabled
                                   v-model="nomTecnOri">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero">Cuadrilla Origen</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="numero" class="form-control" disabled v-model="cuadrillaOri">
                        </div>
                    </div>
                </div>


                <div class="col-4">
                    <div class="form-group ">
                        <label for="">Supervisor Origen</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control col-3 " disabled v-model="codSuperOri">
                            <input type="text" id="" class="form-control col" disabled v-model="superOri">
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group ">
                        <label for="">Tenicos Origen</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control" disabled aria-label="" v-model="tecnsOri">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label for="tecnico">Bodega Destino</label>
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control col-3" required name="idtecnico" readonly
                                   v-model="codTecnDes">
                            <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button"
                                            @click="openModalTecnicos('DESTINO')">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                            <input type="text" class="form-control" required name="tecnico" disabled
                                   v-model="nomTecnDes">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero">Cuadrilla Destino</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="numero" class="form-control" disabled v-model="cuadrillaDes">
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group ">
                        <label for="">Supervisor Destino</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control col-3 " disabled v-model="codSuperDes">
                            <input type="text" id="" class="form-control col" disabled v-model="superDes">
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group ">
                        <label for="">Tenicos Destino</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control" disabled aria-label="" v-model="tecnsDes">
                        </div>
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group ">
                        <label for="">Doc. Referencia</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control" required aria-label=""
                                   v-model="numeroEntrega">
                        </div>
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group">
                        <label for="fecha">Fecha Entrega</label>
                        <div class="input-group input-group-sm">
                            <input type="date" id="fecha" class="form-control" required v-model="fechaEntrega">
                        </div>
                    </div>
                </div>

                <div class="col-8">
                    <div class="form-group ">
                        <label for="">Concepto</label>
                        <div class="input-group input-group-sm">
                            <input type="text" id="" class="form-control" required aria-label="" v-model="concepto">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form action="" id="formMateriales" class="" v-show="!hasSerie">
            <div class="row">
                <div class="col-6">
                    <label for="proveedor">Material/Herramienta</label>
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control col-3" name="idproveedor" readonly required
                               v-model="idMaterial">
                        <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" data-toggle="modal"
                                                @click="openModalMateriales()"><i class="fa fa-search"></i></button>
								      </span>
                        <input type="text" class="form-control" name="proveedor" required aria-label="" disabled
                               v-model="material">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Cantidad</label>
                        <div class="input-group input-group-sm">
                            <input type="number" class="form-control noSigno" min="1" :required="serie!='S'"
                                   :disabled="serie=='S'" aria-label="" v-model="cantidad">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="">Existencia</label>
                        <div class="input-group input-group-sm">
                            <input type="number" disabled required class="form-control" v-model="existencia">
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <!-- Detalle Producto seriado -->
        <form id="form_serie" class="" v-show="hasSerie">
            <div class="row">
                <div class="col-12" style="margin-top: 10px">
                    <h6>Detalles de Producto Seriado</h6>
                </div>
                <!-- 						<div class="col-sm-3">
                                            <label for="proveedor">Propiedad</label>
                                            <div class="input-group input-group-sm">
                                                  <input type="text" class="form-control col-sm-3" readonly name="idproveedor" placeholder="" aria-label="" v-model="idPropiedad" required>
                                                  <span class="input-group-btn">
                                                    <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#modalPropiedades" ><i class="fa fa-search" data-toggle="modal" data-target="#modalPropiedades"></i></button>
                                                  </span>
                                                  <input type="text" class="form-control" name="proveedor" placeholder="" aria-label="" disabled v-model="propiedad" required>
                                            </div>
                                        </div> -->
                <!-- 						<div class="col-sm-3">
                                            <label for="">Paquete</label>
                                            <div class="form-group">
                                                <div class="input-group input-group-sm">
                                                  <input type="text" id="" class="form-control" aria-label="" v-model="paquete" required>
                                                </div>
                                            </div>
                                        </div> -->
                <div class="col-sm-6">
                    <label for="">Marca</label>
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control col-sm-3" readonly name="id" placeholder="" aria-label=""
                               v-model="idMarca" required>
                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button" data-toggle="modal"
                                            data-target="#modalMarcas"><i class="fa fa-search"></i></button>
							      </span>
                        <input type="text" class="form-control" name="" placeholder="" aria-label="" disabled
                               v-model="marca" required>
                    </div>
                </div>
                <!-- 	<div class="col-sm-3">
                        <label for="">Tipo Material</label>
                        <div class="input-group input-group-sm">
                              <input type="text" class="form-control col-sm-3" readonly name="id" placeholder="" aria-label="" v-model="idTipoMaterial" required>
                              <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button" data-toggle="modal" data-target='#modalTipoMateriales'><i class="fa fa-search" ></i></button>
                              </span>
                              <input type="text" class="form-control" name="" placeholder="" aria-label="" disabled v-model="tipoMaterial" required>
                        </div>
                    </div> -->
            </div>

            <div class="row">

                <div class="col-12">
                    <fieldset>
                        <legend>Configuracion / Rango de Serie</legend>
                        <div class="row" style="margin-bottom: 2px;">

                            <div class="col-2">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="modo" id="Manual"
                                               value="Manual" v-model="picked"> Manual
                                    </label><br>
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="modo" id="Auto" value="Auto"
                                               v-model="picked"> Automatico en rango
                                    </label>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">Serie</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="" class="form-control" aria-label=""
                                               :disabled="picked==='Auto'" :required="picked!='Auto'"
                                               v-model="serieManual">
                                    </div>
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">Activo Nro.</label>
                                    <div class="input-group input-group-sm">
                                        <input type="number" id="" class="form-control" aria-label=""
                                               :disabled="picked==='Auto'" v-model="nactivo">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label for="">Prefijo</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="" class="form-control" :disabled="picked==='Manual'"
                                               v-model="prefijo">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="">Valor Inicial</label>
                                    <div class="input-group input-group-sm">
                                        <input type="number" id="" class="form-control" aria-label=""
                                               :disabled="picked==='Manual'" v-model="inicioSerie"
                                               :required="picked!='Manual'">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label for="">Sufijo</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="" class="form-control" aria-label=""
                                               :disabled="picked==='Manual'" v-model="sufijo">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="">Cant. a Ingresar</label>
                                    <div class="input-group input-group-sm">
                                        <input type="number" id="" class="form-control" aria-label=""
                                               :disabled="picked==='Manual'" v-model="finSerie"
                                               :required="picked!='Manual'">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </div>
            </div>
        </form>
        <!--  Tabla Detalle  -->
        <div class="row" v-show="!hasSerie" style="margin-top: 8px">
            <div class="col-11">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr>
                        <th width="100">Codigo</th>
                        <th>Descripción</th>
                        <th width="80" class="text-right">Cantidad</th>
                        <th width="200" class="text-right">Existencia</th>
                        <th width="40px" class="text-right">+</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-for="(dato, index) in tablaDetalle">
                        <td v-text="dato.codigo"></td>
                        <td v-text="dato.descripcion"></td>
                        <td class="text-right" v-text="dato.cantidad"></td>
                        <td class="text-right" v-text="dato.existencia"></td>
                        <th class="text-right"><i class="fa fa-times" title="Eliminar item" data-toggle="tooltip"
                                                  data-placement="top"
                                                  style="cursor:pointer;"
                                                  @click="deleteItemDetalle(index);"></i class="fa fa-times"></th>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-1 text-center">

                <div style="font-size: .8em">Cant. Items <br><b v-text="tablaDetalle.length"></b></div>
                <br>
                <div class="btn border btn-app" @click="addItem()"><i class="fa fa-check"></i><br><span>Añadir</span>
                </div>
                <br>
                <div v-if="loadingGuadar" class="btn border btn-app bg-success text-white" @click="insertProductos()"
                     v-if="tablaDetalle.length>0"><i class="fa fa-save"></i><br><span>Guardar</span></div>
                <div v-else class="btn border btn-app"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"
                                                          style="font-size: 2em; padding: 5px"></i></div>

            </div>
        </div>
        <!-- TABLA SERIES  -->
        <div class="row" v-show="hasSerie" style="margin-top: 8px">
            <div class="col-11" style="height: 230px; overflow-y: scroll;">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr>
                        <th>Material</th>
                        <th>Grupo</th>
                        <th>Marca</th>
                        <th>Serie</th>
                        <th width="30px" class="text-center">+</th>
                    </tr>
                    </thead>
                    <tbody id="">
                    <tr v-for="(dato, index) in tablaSeries">
                        <td v-text="dato.MATERIAL"></td>
                        <td v-text="dato.CODMARCA"></td>
                        <td v-text="dato.NOMMARCA"></td>
                        <td v-text="dato.SERIE"></td>
                        <th><i class="fa fa-times" title="Quitar item" style="cursor:pointer;"
                               @click="quitarSerie(index);"></i class="fa fa-times"></th>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-1 text-center">

                <div style="font-size: .8em">Num. Series <br><b v-text="tablaSeries.length"></b></div>
                <br>
                <div v-if="loadingSerie" class="btn border btn-app" @click="addRangoSerie()"><i class="fa fa-list-ol"
                                                                                                style="font-size: 2em; padding: 5px"></i>
                </div>
                <div v-else class="btn border btn-app"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"
                                                          style="font-size: 2em; padding: 5px"></i></div>
                <br>
                <div class="btn border btn-app bg-success text-white" v-if="tablaSeries.length>0"
                     @click="addRangoSerieDetalle()">
                    <i class="fa fa-check"></i><br><span>Agregar</span>
                </div>

                <div class="btn border btn-app bg-danger text-white" v-if="tablaSeries.length===0"
                     @click="cancelarSerie()">
                    <i class="fa fa-ban"></i><br><span>Cancelar</span>
                </div>
                <br>
                <div class="btn border btn-app bg-danger text-white" v-if="tablaSeries.length>0"
                     @click="eliminarRangoSerie()">
                    <i class="fa fa-trash-o"></i><br><span>Eliminar</span>
                </div>
                <br>
                <div class="btn border btn-app bg-danger text-white" data-toggle="modal"
                     data-target='#modalSeriesExiste' v-show="serieExiste.length>0">
                    <i class="fa fa-eye"></i><br><span>Series</span>
                </div>

            </div>
        </div>

    </div>


    <!-- Modals -->
    <div id="modalTecnico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Tenicos</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6" id="buscarTecnico"
                                       @keyup="getTecnicos()" v-model="buscarTecnico">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getTecnicos()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th>Cod.</th>
                                    <th>Tecnico</th>
                                    <th>Cuadrilla</th>
                                    <th>Supervisor</th>
                                    <th>Tecnicos</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="tecn in tablaTecnicos" @click="seleccionarTecnico(tecn)">
                                    <td v-text="tecn.CODTECNICO"></td>
                                    <td v-text="tecn.NOMTECNICO"></td>
                                    <td v-text="tecn.CUADRILLA"></td>
                                    <td v-text="tecn.NOMSUPERVISOR"></td>
                                    <td v-text="tecn.TECNICOS"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalMateriales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Materiales / Herramientas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6" @keyup="getMateriales()"
                                       name="idproveedor" placeholder="Buscar..." aria-label=""
                                       v-model="buscarMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getMateriales()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="70">Codigo</th>
                                    <th>Descripcion</th>
                                    <th>Existencia</th>
                                    <th width="50">Seriado</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="material in tablaMateriales" @click="seleccionarMaterial(material)">
                                    <td v-text="material.CODIGO"></td>
                                    <td v-text="material.DESCRIPCION"></td>
                                    <td v-text="material.EXISTENCIA"></td>
                                    <td v-text="material.SERIADO"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalPropiedades" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Propiedades</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6" @keyup="getPropiedades()"
                                       name="idproveedor" placeholder="Buscar..." aria-label=""
                                       v-model="buscarPropiedad">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getPropiedades()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Codigo</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="prop in tablaPropiedades" @click="seleccionarPropiedad(prop)">
                                    <td v-text="prop.CODIGO"></td>
                                    <td v-text="prop.DESCRIPCION"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalMarcas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Marcas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6" @keyup="getMarcas()" name="idproveedor"
                                       placeholder="Buscar..." aria-label="" v-model="buscarMarca">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getMarcas()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Codigo</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="marca in tablaMarcas" @click="seleccionarMarca(marca)">
                                    <td v-text="marca.CODIGO"></td>
                                    <td v-text="marca.DESCRIPCION"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalTipoMateriales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Tipo Materiales</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6" @keyup="getTipoMateriales()"
                                       name="idproveedor" placeholder="Buscar..." aria-label=""
                                       v-model="buscarTipoMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getTipoMateriales()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Codigo</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="prop in tablaTipoMateriales" @click="seleccionarTipoMaterial(prop)">
                                    <td v-text="prop.CODIGO"></td>
                                    <td v-text="prop.DESCRIPCION"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalSeriesExiste" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Series Existes
                        <small>({{ serieExiste.length }} series existen)</small>
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th>Material</th>
                                    <th>Marca</th>
                                    <th>Serie</th>
                                    <th>Legalizado</th>
                                    <th>Propietario</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="serie in serieExiste">
                                    <td v-text="serie.MATERIAL"></td>
                                    <td v-text="serie.NOMMARCA"></td>
                                    <td v-text="serie.SERIE"></td>
                                    <td v-text="serie.LEGALIZADO"></td>
                                    <td v-text="serie.ASIGNADO"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/datepicker.js"></script>
<script src="js/pace/pace.min.js"></script>


<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>
</html>
