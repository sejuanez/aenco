<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <title>Legalizacion de ordenes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>


    <style type="text/css">


        ::-webkit-input-placeholder { /* WebKit browsers */
            text-transform: none;
        }

        .container {
            max-width: 95%;
        }

        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .btn-app {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 50px;
            margin-bottom: 10px;
            color: #555;
            font-size: 0.8em;
        }

        .btn-app:hover {
            background: #007bff;
            color: #FFF;
        }

        .btn-app:active {
            background: #009dff;
            color: #FFF;
        }

        label {
            margin-bottom: 0;
        }

        thead {
            font-size: .8em;
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }

        td {
            font-size: .8em;
            cursor: pointer;
        }

        .gj-datepicker input, .gj-datepicker span {
            padding: 5px;
            font-size: .9em;

        }

        input[type="text"] {
            text-transform: uppercase;
        }

        .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
            line-height: initial;
        }

    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Legalización de Ordenes


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  v-if="response.length != 0"
                  @click="legalizar()">
					<i class="fa fa-check" aria-hidden="true"></i>
                Legalizar
            </span>
            <!--
         <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
               @click="buscarDocumento()">
                 <i class="fa fa-search" aria-hidden="true"></i>
             Consultar
         </span>
         -->


        </p>
    </header>

    <div v-if="ajax" class="loading">Loading&#8230;</div>

    <div class="container-fluid">

        <form action="" id="form_encabezado">

            <div class="container">

                <div class="row">

                    <div class="col-1"></div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Orden</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm " id="inputEmail3"
                                       autocomplete="nope"
                                       autofocus
                                       v-model="orden"
                                       v-on:blur="consultaOrden()"
                                       v-on:enter="consultaOrden()"
                                       placeholder="Orden">
                            </div>
                        </div>
                    </div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Contrato</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="contrato"
                                       placeholder="Contrato">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-1"></div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Solicitud</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="solicitud"
                                       placeholder="Solicitud">
                            </div>
                        </div>
                    </div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Telefono</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="telefono"
                                       placeholder="Telefono">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-1"></div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">T. Orden</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="tipoOrden"
                                       placeholder="Tipo de Orden">
                            </div>
                        </div>
                    </div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Plan</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="plan"
                                       placeholder="Plan">
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-1"></div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Direccion</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="direccion"
                                       placeholder="Direccion">
                            </div>
                        </div>
                    </div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Ciudad</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="ciudad"
                                       placeholder="Ciudad">
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-1"></div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Instalador</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="instalador"
                                       placeholder="Instalador">
                            </div>
                        </div>
                    </div>

                    <div class="col-5 ">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-3 col-form-label">Cliente</label>
                            <div class="col-9">
                                <input type="text" class="form-control form-control-sm desabilitado" id="inputEmail3"
                                       autocomplete="nope"
                                       v-model="cliente"
                                       placeholder="Cliente">
                            </div>
                        </div>
                    </div>


                </div>

            </div>


        </form>

        <br>

        <div class="container">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#datos" role="tab"
                       aria-controls="noSerie"
                       aria-selected="true">Datos Ejecucion</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#materiales" role="tab"
                       aria-controls="serie"
                       aria-selected="false">Materiales/Item</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#seriados" role="tab"
                       aria-controls="serie"
                       aria-selected="false">Materiales Seriados</a>
                </li>


            </ul>

            <div class="tab-content" id="myTabContent">

                <br>
                <div class="tab-pane fade show active" id="datos" role="tabpanel" aria-labelledby="home-tab">
                    <div class="container">

                        <div class="row">

                            <div class="col-1"></div>

                            <div class="col-7 ">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-4 col-form-label">Fecha Ejecucion</label>
                                    <div class="col-8">
                                        <input type="text" class="form-control form-control-sm desabilitado"
                                               id="inputEmail3"
                                               autocomplete="nope"
                                               v-model="fechaEjecucion"
                                               placeholder="Fecha EjecucionS">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-1"></div>

                            <div class="col-7">
                                <div class="form-group row">
                                    <label for="selectTraslado" class="col-4 col-form-label">Traslado en cambio</label>
                                    <div class="col-8">
                                        <select class="browser-default custom-select form-control-sm"
                                                id="selectTraslado"
                                                name="selectTraslado"
                                                v-model="trasladoSelected">
                                            <option value="">Seleccionar...</option>
                                            <option value="S">Si</option>
                                            <option value="N">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-1"></div>

                            <div class="col-7 ">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-4 col-form-label">Observacion</label>
                                    <div class="col-8">
                                        <textarea type="email" class="form-control form-control" id="inputEmail3"
                                                  autocomplete="nope"
                                                  rows="3"
                                                  v-model="observacion"
                                                  placeholder="Observacion">
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="tab-pane fade" id="materiales" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="container">


                        <div class="row">


                            <div class="col-4">
                                <div class="form-group">
                                    <label for="selectTraslado">Tipo de Trabajo</label>
                                    <div class="input-group input-group-sm">
                                        <select class="browser-default custom-select form-control-sm col-12"
                                                id="selectTraslado"
                                                name="selectTraslado"
                                                v-model="tipoTrabajo">
                                            <option value="I">Instalacion</option>
                                            <option value="R">Retiro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label for="proveedor">Material</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control col-12" required name="idproveedor"
                                               readonly
                                               placeholder="" aria-label="">
                                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button" data-toggle="modal"
                                            @click="modalMaterial()">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label for="proveedor">Item de pago</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control col-12" required name="idproveedor"
                                               readonly
                                               placeholder="" aria-label="">
                                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button" data-toggle="modal"
                                            @click="modalItemPago()">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="row">

                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label for="numero">Codigo</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="numero" class="form-control"
                                               required v-model="codigoItem" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-5">
                                <div class="form-group">
                                    <label for="numero">Descripcion</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="numero" class="form-control"
                                               required v-model="descItem" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group">
                                    <label for="numero">Existencia</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" id="numero" class="form-control"
                                               required v-model="existenciaItem" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group">
                                    <label for="numero">Cantidad</label>
                                    <div class="input-group input-group-sm">
                                        <input type="number" id="numero" class="form-control no-signo" class="noSigno"
                                               required v-model="cantidadItem">
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="row" v-show="true" style="margin-top: 8px">
                            <div class="col-11">
                                <table class="table table-sm">
                                    <thead class="fondoGris">
                                    <tr>
                                        <th width="100">Codigo</th>
                                        <th>Descripción</th>
                                        <th width="80" class="text-right">Cantidad</th>
                                        <th width="200" class="text-right">Tipo</th>
                                        <th width="200" class="text-right">IR</th>
                                        <th width="40px" class="text-right">+</th>
                                    </tr>
                                    </thead>
                                    <tbody id="detalle_gastos">
                                    <tr v-for="(dato, index) in tablaItems"
                                        :class="dato.IR == 'R' ? 'table-danger' : ''">
                                        <td v-text="dato.codigo"></td>
                                        <td v-text="dato.descripcion"></td>
                                        <td class="text-right" v-text="dato.cantidad"></td>
                                        <td class="text-right" v-text="dato.tipo"></td>
                                        <td class="text-right" v-text="dato.IR"></td>
                                        <th class="text-right"><i class="fa fa-times" title="Eliminar item"
                                                                  data-toggle="tooltip"
                                                                  data-placement="top"
                                                                  style="cursor:pointer;"
                                                                  @click="deleteItems(index);"></i class="fa fa-times">
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-1 text-center">

                                <div class="btn border btn-app" @click="addItem()"><i class="fa fa-check">
                                    </i><br><span>Añadir</span>
                                </div>
                                <br>


                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-pane fade" id="seriados" role="tabpanel" aria-labelledby="profile-tab">

                    <div class="container">


                        <div class="row">

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="selectTraslado">Tipo de Trabajo</label>
                                    <div class="input-group input-group-sm">
                                        <select class="browser-default custom-select form-control-sm col-12"
                                                @change="changeTipo()"
                                                name="selectTraslado"
                                                v-model="tipoTrabajo">
                                            <option value="I">Instalacion</option>
                                            <option value="R">Retiro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <label for="tecnico">Material Seriado</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control col-3" required name="idtecnico" readonly
                                               placeholder="" aria-label="" v-model="codMatSer">
                                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button"
                                            data-toggle="modal" @click="modalMaterialSeriado()">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                                        <input type="text" class="form-control" required name="tecnico" disabled
                                               v-model="nomMatSer">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label for="tecnico">Marca</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control col-3" required name="idtecnico" readonly
                                               placeholder="" aria-label="" v-model="codMarca">
                                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button"
                                            data-toggle="modal" @click="modalMarca()">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>
                                        <input type="text" class="form-control" required name="tecnico" disabled
                                               v-model="nomMarca">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group">
                                    <label for="tecnico">Serie</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control col-12" required id="idCodSerie"
                                               placeholder="" aria-label="" v-model="codSerie">
                                        <span class="input-group-btn">
							        <button class="btn btn-secondary" type="button"
                                            id="btnCodSerie"
                                            data-toggle="modal" @click="modalSerie()">
							        	<i class="fa fa-search"></i>
							        </button>
							      </span>

                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="row" v-show="true" style="margin-top: 8px">
                            <div class="col-11">
                                <table class="table table-sm">
                                    <thead class="fondoGris">
                                    <tr>
                                        <th width="200">Serie</th>
                                        <th>Descripción Material</th>
                                        <th width="200">Marca</th>
                                        <th width="80" class="text-right">Cantidad</th>
                                        <th width="40px" class="text-right">+</th>
                                    </tr>
                                    </thead>
                                    <tbody id="detalle_gastos">
                                    <tr v-for="(dato, index) in tablaItemSeriado"
                                        :class="dato.IR == 'R' ? 'table-danger' : ''">
                                        <td v-text="dato.codigoSerie"></td>
                                        <td v-text="dato.descripcion"></td>
                                        <td v-text="dato.marca"></td>
                                        <td class="text-right" v-text="dato.cantidad"></td>
                                        <th class="text-right"><i class="fa fa-times" title="Eliminar item"
                                                                  data-toggle="tooltip"
                                                                  data-placement="top"
                                                                  style="cursor:pointer;"
                                                                  @click="deleteItemSeriado(index);"></i class="fa fa-times">
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-1 text-center">

                                <div class="btn border btn-app" @click="addItemSeriado()"><i class="fa fa-check">
                                    </i><br><span>Añadir</span>
                                </div>
                                <br>


                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>


    </div>

    <div id="modalMaterial" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Materiales</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       v-model="buscarMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button"
                                                @click="getMateriales(codInstalador,buscarMaterial,2,tipoTrabajo)"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Id</th>
                                    <th>Material</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaMateriales" @click="seleccionarMaterial(item)">
                                    <td v-text="item.codigo"></td>
                                    <td v-text="item.material"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalMaterialSeriado" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Materiales Seriado</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       v-model="buscarMaterialSeriado">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button"
                                                @click="getMaterialSeriado(codInstalador,buscarMaterialSeriado,4)"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Id</th>
                                    <th>Material</th>
                                    <th>Cantidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaMaterialSeriado" @click="seleccionarMaterialSeriado(item)">
                                    <td v-text="item.codigo"></td>
                                    <td v-text="item.material"></td>
                                    <td v-text="item.existencia"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalMarca" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Marca</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       v-model="buscarMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button"
                                                @click="getMaterialSeriado(codInstalador,buscarMaterial,4)"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Id</th>
                                    <th>MarcaS</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaMarcas" @click="seleccionarMarca(item)">
                                    <td v-text="item.codigo"></td>
                                    <td v-text="item.marca"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalSerie" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Serie</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       v-model="buscarMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button"
                                                @click="getMaterialSeriado(codInstalador,buscarMaterial,4)"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th>Material</th>
                                    <th>Serie</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaSeries" @click="seleccionarSerie(item)">
                                    <td v-text="item.material"></td>
                                    <td v-text="item.serie"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalItemPago" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Items de Pago</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       v-model="buscarItemPago">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button"
                                                @click="getItemPago()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="200">Id</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaItemPago" @click="seleccionarItemPago(item)">
                                    <td v-text="item.codigo"></td>
                                    <td v-text="item.descripcion"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/datepicker.js"></script>

<script src="js/pace/pace.min.js"></script>


<script src="js/vue/vue.js"></script>
<script src="js/select2.full.min.js"></script>
<script src="js/app.js"></script>


</body>
</html>
