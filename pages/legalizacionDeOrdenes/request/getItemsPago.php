<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 05/11/2018
 * Time: 1.01
 */


include("../../../init/gestion.php");

$buscar = $_POST["buscar"];

if ($buscar == "") {

    $sql = "SELECT 
              mo_codigod,
              mo_descripciond
              FROM MOBRAS_BUSCA (1, null)";

} else {

    $sql = "SELECT 
              mo_codigod,
              mo_descripciond
              FROM MOBRAS_BUSCA (2, '" . $buscar . "')";

}


$return_arr = array();

$result = ibase_query($conexion, $sql);

while ($fila = ibase_fetch_row($result)) {
    $row_array['codigo'] = utf8_encode($fila[0]);
    $row_array['descripcion'] = utf8_encode($fila[1]);

    array_push($return_arr, $row_array);
}

echo json_encode($return_arr);
