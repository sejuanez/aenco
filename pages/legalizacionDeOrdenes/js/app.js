// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);
    });
// $('#fecha').val(app.fechaEntrada);

    $('.noSigno').keydown(function (e) {
        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });


    $(".desabilitado").prop('disabled', true)

});


let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        response: [],

        orden: "219552863",

        contrato: "",
        solicitud: "",
        telefono: "",
        tipoOrden: "",
        direccion: "",
        plan: "",
        cliente: "",
        ciudad: "",
        instalador: "",
        codInstalador: "",

        tipoTrabajo: "I",
        fechaEjecucion: "",

        selectTraslado: [],
        trasladoSelected: "",

        observacion: "",

        tablaNoSeriados: [],

        buscarMaterial: "",
        tablaMateriales: [],

        codigoItem: "",
        descItem: "",
        cantidadItem: "",
        existenciaItem: "",
        tipoItem: "",

        tablaItems: [],

        tablaItemPago: [],
        buscarItemPago: "",

        tablaItemSeriado: [],

        tablaMaterialSeriado: [],

        codMatSer: "",
        nomMatSer: "",
        codGrupo: "",
        buscarMarca: "",

        tablaMarcas: [],
        codMarca: "",
        nomMarca: "",

        codSerie: "",

        tablaSeries: [],

        buscarMaterialSeriado: "",

    },


    methods: {

        buscarDocumento: function () {

            var app = this;

            if (app.idDocumento == "" || app.tipoDocumento == "") {

                alertify.warning("Seleccione los criterios de busqueda")

            } else {

                $.ajax({
                    url: './request/getDocumento.php',
                    type: 'GET',
                    data: {
                        codigo: app.idDocumento,
                        tipo: app.tipoDocumento,
                    },

                }).done(function (response) {

                    try {
                        var data = JSON.parse(response);

                        if (data.length > 0) {
                            app.flangImprimir = true;
                        }

                        console.log(data)

                        app.codigo = data[0].codTecnico;
                        app.concepto = data[0].concepto;
                        app.nombre = data[0].nomTecnico;
                        app.fecha = data[0].fecha;

                        console.log(data[0].serie)

                        app.tablaNoSeriados = [];

                        app.tablaTodos = data;


                        for (var i = 0; i < data.length; i++) {


                            app.tablaNoSeriados.push(
                                {
                                    codigo: data[i].codigo,
                                    descripcion: data[i].descripcion,
                                    marca: "",
                                    unidad: data[i].nomUnidad,
                                    cantidad: data[i].cantidad
                                }
                            )


                        }

                        console.log(app.tablaNoSeriados)

                    } catch (e) {

                        console.log('doc no encontrado')

                    }


                }).fail(function (error) {

                    console.log(error)

                });


                $.ajax({
                    url: './request/getSeriados.php',
                    type: 'GET',
                    data: {
                        codigo: app.idDocumento,
                        tipo: app.tipoDocumento,
                    },

                }).done(function (response) {

                    try {

                        var data = JSON.parse(response);

                        app.tablaSeriados = [];

                        for (var i = 0; i < data.length; i++) {

                            app.tablaSeriados.push(
                                {
                                    codigo: data[i].serie,
                                    descripcion: data[i].material,
                                    marca: data[i].marca,
                                    unidad: data[i].codigo,
                                    cantidad: 1
                                }
                            )

                        }

                        console.log(app.tablaSeriados)

                    } catch (e) {

                        console.log('no tiene serie')

                    }


                }).fail(function (error) {

                    console.log(error)

                });


            }
        },

        consultaOrden: function () {

            let app = this;

            console.log(app.orden)

            $.ajax({
                url: './request/ordenBusca.php',
                type: 'POST',
                data: {
                    orden: app.orden,
                },

            }).done(function (response) {

                app.response = JSON.parse(response)

                if (app.response.length <= 0) {
                    //alertify.alert('Resultados', 'No se encontro resultado para este numero de orden')
                } else {
                    app.llenarCampos();
                }

            }).fail(function (error) {

                console.log(error)

            });

        },

        llenarCampos: function () {

            let app = this;

            app.contrato = app.response[0]['contrato'];
            app.solicitud = app.response[0]['solicitud'];
            app.telefono = app.response[0]['telefono'];
            app.tipoOrden = app.response[0]['tipoOrden'];
            app.direccion = app.response[0]['direccion'];
            app.plan = app.response[0]['plan'];
            app.cliente = app.response[0]['cliente'];
            app.ciudad = app.response[0]['nomciudad'];
            app.instalador = app.response[0]['instalador'];
            app.observacion = app.response[0]['observaciones'];
            app.fechaEjecucion = app.response[0]['fechaEjecucion'];
            app.codInstalador = app.response[0]['codInstalador'];
            app.trasladoSelected = app.response[0]['trasladoCambio'];

            console.log(app.contrato)

        },

        modalMaterial: function () {

            let app = this;

            app.buscarMaterial = "";
            app.tablaMateriales = [];

            if (app.codInstalador == "") {

                alertify.warning('Escribe una orden primero')

            } else {

                if (app.tipoTrabajo == 'I') {
                    app.getMateriales(app.codInstalador, "", 1, app.tipoItem);
                }
                $("#modalMaterial").modal("show");
            }
        },

        modalMaterialSeriado: function () {

            let app = this;

            app.buscarMaterialSeriado = "";

            app.tablaMaterialSeriado = [];

            if (app.codInstalador == "") {

                alertify.warning('Escribe una orden primero')

            } else {

                if (app.tipoTrabajo == 'I') {
                    app.getMaterialSeriado(app.codInstalador, "", 3, app.tipoItem);
                }
                $("#modalMaterialSeriado").modal("show");

            }
        },

        modalItemPago: function () {

            let app = this;

            app.buscarMaterial = "";

            if (app.codInstalador == "") {

                alertify.warning('Escribe una orden primero')

            } else {

                $("#modalItemPago").modal("show");

                app.getItemPago('');
            }
        },

        modalMarca: function () {

            let app = this;

            app.buscarMarca = "";

            if (app.codMatSer == "") {

                alertify.warning('Selecciona un material seriado')

            } else {

                $("#modalMarca").modal("show");

                app.getMarca();
            }
        },

        modalSerie: function () {

            let app = this;

            app.buscarMarca = "";

            if (app.codMarca == "") {

                alertify.warning('Selecciona una marca')

            } else {

                $("#modalSerie").modal("show");

                app.getSerie();
            }
        },

        getMateriales: function (codTecnico, codMaterial, opcion, tipo) {

            let app = this;

            app.tablaMateriales = [];

            $.ajax({
                url: './request/getMateriales.php',
                type: 'POST',
                async: false,
                data: {
                    tecnico: codTecnico,
                    material: codMaterial,
                    opcion: opcion,
                    tipo: app.tipoTrabajo,
                },

            }).done(function (response) {

                app.tablaMateriales = JSON.parse(response)

            }).fail(function (error) {

                console.log(error)

            });

        },

        getMaterialSeriado: function (codTecnico, codMaterial, opcion, tipoTrabajo) {

            let app = this;

            $.ajax({
                url: './request/getMateriales.php',
                type: 'POST',
                data: {
                    tecnico: codTecnico,
                    material: codMaterial,
                    opcion: opcion,
                    tipo: app.tipoTrabajo,
                },

            }).done(function (response) {
                console.log(response);

                app.tablaMaterialSeriado = JSON.parse(response)

                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        },

        getMarca: function () {


            let app = this;

            $.ajax({
                url: './request/getMarcas.php',
                type: 'POST',
                data: {
                    grupo: app.codGrupo,
                },

            }).done(function (response) {
                console.log(response);

                app.tablaMarcas = JSON.parse(response)

                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        },

        getSerie: function () {


            let app = this;

            $.ajax({
                url: './request/getSerie.php',
                type: 'POST',
                data: {
                    material: app.codMatSer,
                    grupo: app.codGrupo,
                    marca: app.codMarca,
                    super: app.codInstalador,
                },

            }).done(function (response) {
                console.log(response);

                app.tablaSeries = JSON.parse(response)

                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        },

        seleccionarMaterial: function (material) {

            let app = this;

            app.codigoItem = material.codigo;
            app.descItem = material.material;
            app.cantidadItem = 0;
            app.existenciaItem = parseInt(material.existencia);
            app.tipoItem = 'M';
            $("#modalMaterial").modal("hide");

        },

        seleccionarMaterialSeriado: function (material) {

            let app = this;

            app.codMatSer = material.codigo;
            app.nomMatSer = material.material;
            app.codGrupo = material.grupo;
            app.tipoItem = 'M';
            $("#modalMaterialSeriado").modal("hide");

            app.codMarca = "";
            app.nomMarca = "";
            app.codSerie = "";

        },

        seleccionarMarca: function (marca) {

            let app = this;

            app.codMarca = marca.codigo;
            app.nomMarca = marca.marca;
            //app.codGrupo = material.grupo;
            $("#modalMarca").modal("hide");

            app.codSerie = "";

        },

        seleccionarSerie: function (marca) {

            let app = this;

            app.codSerie = marca.serie;
            //app.codGrupo = material.grupo;
            $("#modalSerie").modal("hide");

        },

        addItem: function () {

            let app = this;

            if (app.codigoItem == "") {
                alertify.alert('Alerta', 'Seleccione un material o un item de pago')
            } else {
                if (app.cantidadItem > app.existenciaItem || app.cantidadItem <= 0) {

                    if (app.cantidadItem <= 0) {
                        alertify.alert("Aleta!", "No se pueden ingresar cantidades menores que 0");
                    } else {
                        if (app.existenciaItem != "") {
                            alertify.alert("Aleta!", "No hay suficiente material");
                        } else {

                            var arrayItemPago = []
                            for (var j = 0; j < app.tablaItems.length; j++) {
                                arrayItemPago.push(app.tablaItems[j].codigo);
                            }
                            if (arrayItemPago.includes(app.codigoItem)) {
                                alertify.alert("Aleta!", "El Item de pago ya fue ingresado", function () {
                                    app.codigoItem = "";
                                    app.descItem = "";
                                    app.cantidadItem = "";
                                    app.existenciaItem = "";
                                    app.tipoItem = "";
                                });
                            } else {


                                app.tablaItems.push({
                                    'codigo': app.codigoItem,
                                    'descripcion': app.descItem,
                                    'cantidad': app.cantidadItem,
                                    'tipo': app.tipoItem,
                                    'serie': 'N',
                                    'IR': 'I',
                                });
                                app.codigoItem = "";
                                app.descItem = "";
                                app.cantidadItem = "";
                                app.existenciaItem = "";
                                app.tipoItem = "";
                            }


                        }
                    }

                } else {


                    var arrDetalleExiste = []
                    for (var i = 0; i < app.tablaItems.length; i++) {
                        arrDetalleExiste.push(app.tablaItems[i].codigo);
                    }
                    if (arrDetalleExiste.includes(app.codigoItem)) {
                        alertify.alert("Aleta!", "El material ya fue ingresado", function () {
                            app.codigoItem = "";
                            app.descItem = "";
                            app.cantidadItem = "";
                            app.existenciaItem = "";
                            app.tipoItem = "";
                        });
                    } else {


                        if (app.tipoTrabajo == 'R') {
                            app.cantidadItem *= -1;
                        }

                        app.tablaItems.push({
                            'codigo': app.codigoItem,
                            'descripcion': app.descItem,
                            'cantidad': app.cantidadItem,
                            'tipo': app.tipoItem,
                            'serie': 'N',
                            'IR': app.tipoTrabajo,
                        });
                        app.codigoItem = "";
                        app.descItem = "";
                        app.cantidadItem = "";
                        app.existenciaItem = "";
                        app.tipoItem = "";
                    }


                }
            }

        },

        addItemSeriado: function () {

            let app = this;

            if (app.codSerie == "" || app.codMatSer == "") {
                alertify.alert('Alerta', 'Seleccione un material seriado')
            } else {

                let arrDetalleExiste = []
                for (var i = 0; i < app.tablaItemSeriado.length; i++) {
                    arrDetalleExiste.push(app.tablaItemSeriado[i].codigoSerie);
                }
                if (arrDetalleExiste.includes(app.codSerie)) {
                    alertify.alert("Aleta!", "El material ya fue ingresado", function () {
                        app.codMatSer = "";
                        app.nomMatSer = "";
                        app.codSerie = "";
                        app.codMarca = "";
                        app.nomMarca = "";
                    });
                } else {


                    let cantidadItem = 1;

                    if (app.tipoTrabajo == 'R') {
                        cantidadItem *= -1;
                    }

                    app.tablaItemSeriado.push({
                        'codigo': app.codMatSer,
                        'codigoSerie': app.codSerie,
                        'descripcion': app.nomMatSer,
                        'cantidad': cantidadItem,
                        'marca': app.nomMarca,
                        'tipo': 'M',
                        'serie': 'S',
                        'IR': app.tipoTrabajo,
                    });

                    let arrDetalleExiste = []
                    for (let i = 0; i < app.tablaItems.length; i++) {
                        arrDetalleExiste.push(app.tablaItems[i].codigo);
                    }
                    if (arrDetalleExiste.includes(app.codMatSer)) {

                        for (let i = 0; i < app.tablaItems.length; i++) {

                            if (app.tablaItems[i].codigo == app.codMatSer) {
                                app.tablaItems[i].cantidad += cantidadItem;
                            }
                        }

                    } else {

                        app.tablaItems.push({
                            'codigo': app.codMatSer,
                            'descripcion': app.nomMatSer,
                            'cantidad': cantidadItem,
                            'tipo': 'M',
                            'serie': 'S',
                            'IR': app.tipoTrabajo,
                        });

                    }

                    app.codMatSer = "";
                    app.nomMatSer = "";
                    app.codSerie = "";
                    app.codMarca = "";
                    app.nomMarca = "";
                }


            }


        },

        deleteItems: function (index) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaItems.splice(index, 1);
                },
                function () {
                });
        },

        deleteItemSeriado: function (index) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaItemSeriado.splice(index, 1);
                },
                function () {
                });
        },

        getItemPago: function (codItem) {

            let app = this;

            $.ajax({
                url: './request/getItemsPago.php',
                type: 'POST',
                data: {
                    buscar: codItem
                },

            }).done(function (response) {
                console.log(response);

                app.tablaItemPago = JSON.parse(response)

                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        },

        seleccionarItemPago: function (material) {

            let app = this;

            app.codigoItem = material.codigo;
            app.descItem = material.descripcion;
            app.tipoItem = 'I';
            $("#modalItemPago").modal("hide");

        },

        changeTipo: function () {

            let app = this;

            console.log(app.tipoTrabajo)

            if (app.tipoTrabajo == 'I') {

                $("#idCodSerie").prop('disabled', true)
                $("#btnCodSerie").prop('disabled', false)

            } else {

                $("#idCodSerie").prop('disabled', false)
                $("#btnCodSerie").prop('disabled', true)

            }
        },

        legalizar: function () {

            let app = this;

            $.ajax({
                url: './request/legalizar.php',
                type: 'POST',
                data: {
                    orden: app.orden,
                    observaciones: app.observacion,
                    traslado: app.trasladoSelected,
                    materiales: app.tablaItems,
                    materialesSeries: app.tablaItemSeriado
                },

            }).done(function (response) {
                console.log(response);
            }).fail(function (error) {

                console.log(error)

            });

        }

    },

    watch:
        {},


    mounted() {

        this.changeTipo();

    },


});

