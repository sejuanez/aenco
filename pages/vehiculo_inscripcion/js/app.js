// ----------------------------------------------
// Mostrar foto preview
// ----------------------------------------------
// (function() {
//   'use strict';

//   window.addEventListener('load', function() {
//     var form = document.getElementById('formImage');
//     form.addEventListener('submit', function(event) {
//       if (form.checkValidity() === false) {
//         event.preventDefault();
//         event.stopPropagation();
//       }
//       form.classList.add('was-validated');
//     }, false);
//   }, false);
// })();


jQuery(document).ready(function($) {
    $("#foto").change(function() {
        readURL(this);
    });
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imgFuncionario').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


let app = new Vue({
    el: '#app',
    data: {

        valid: true,

        inputSearch: "",

        placa: "",
        t_vehiculo: "",
        color: "",
        marca: "",
        cuadrilla: "",
        modelo: "",
        cc: "",
        propietario_cc: "",
        propietario_nombre: "",
        propietario_direccion: "",
        propietario_telefono: "",
        conductor_cc: "",
        conductor_nombre: "",
        conductor_direccion: "",
        conductor_telefono: "",
        conductor_tlicencia: "",




        listFuncionarios: [],

        btnGuardar: true,
        btnActualizar: false,
        btnEliminar: false,
        btnEditar: false,
        btnFile: true,
        btnCancelar: true,

    },


    methods: {

        nuevo: function() {
            this.resetModal();
            $('#addFucionario').modal('show');
        },

        verFuncionario: function(dato) {
            this.placa = dato.placa;
            this.t_vehiculo = dato.t_vehiculo;
            this.color = dato.color;
            this.cuadrilla = dato.cuadrilla;
            this.modelo = dato.modelo;
            this.marca = dato.marca;
            this.cc = dato.cc;
            this.propietario_cc = dato.propietario_cc;
            this.propietario_nombre = dato.propietario_nombre;
            this.propietario_direccion = dato.propietario_direccion;
            this.propietario_telefono = dato.propietario_telefono;
            this.conductor_cc = dato.conductor_cc;
            this.conductor_nombre = dato.conductor_nombre;
            this.conductor_direccion = dato.conductor_direccion;
            this.conductor_telefono = dato.conductor_telefono;
            this.conductor_telefono = dato.conductor_telefono;
            this.conductor_tlicencia = dato.conductor_tlicencia;




            $('#imgFuncionario').attr('src', "./request/getFoto.php?placa=" + dato.placa);

            $('.formGuardar input').attr('disabled', 'disabled');
            $('.formGuardar select').attr('disabled', 'disabled');


            this.btnGuardar = false;
            this.btnActualizar = false;
            this.btnEliminar = true;
            this.btnEditar = true;

            this.btnCancelar = false;
            this.btnFile = false;

            $('#addFucionario').modal('show');
        },

        search: function() {
            var app = this;

            $.post('./request/getFuncionario.php', { funcionario: app.inputSearch }, function(data) {
                var datos = jQuery.parseJSON(data);
                app.listFuncionarios = datos;

            });
        },

        resetModal: function() {
            $('#addFucionario').modal('hide');
            this.placa = "";
            this.t_vehiculo = "";
            this.color = "";
            this.cuadrilla = "";
            this.modelo = "";
            this.cc = "";
            this.marca = "";
            this.propietario_cc = "";
            this.propietario_nombre = "";
            this.propietario_direccion = "";
            this.propietario_telefono = "";
            this.conductor_cc = "";
            this.conductor_nombre = "";
            this.conductor_direccion = "";
            this.conductor_telefono = "";
            this.conductor_tlicencia = "";

            $('input[type=file]').val(null);
            $('#imgFuncionario').attr('src', 'img/user.png');
            $('.formGuardar').removeClass('was-validated');
            this.btnGuardar = true;
            this.btnActualizar = false;
            this.btnEliminar = false;
            this.btnEditar = false;
            this.btnFile = true;
            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar select').removeAttr('disabled');
        },

        guardar: function() {

            if (this.placa == "" ||
                this.t_vehiculo == "" ||
                this.color == "" ||
                this.cuadrilla == "" ||
                this.modelo == "" ||
                this.cc == "" ||
                this.marca == "" ||
                this.propietario_cc == "" ||
                this.propietario_nombre == "" ||
                this.propietario_direccion == "" ||
                this.propietario_telefono == "" ||
                this.conductor_cc == "" ||
                this.conductor_nombre == "" ||
                this.conductor_direccion == "" ||
                this.conductor_telefono == "" ||
                this.conductor_tlicencia == ""

            ) {
                $('.formGuardar').addClass('was-validated');
            } else {

                var app = this;
                var formData = new FormData($('.formGuardar')[0]);

                //hacemos la petición ajax
                $.ajax({
                    url: './uploadFile.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function() {},
                    success: function(data) {

                        app.formDatos(data, "./request/insertFuncionario.php");
                    },
                    //si ha ocurrido un error
                    error: function(FormData) {
                        console.log(data)
                    }
                });
            }
        },

        editar: function() {
            this.btnFile = true;
            this.btnCancelar = true;
            this.btnEliminar = false;
            this.btnEditar = false;
            this.btnActualizar = true;
            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar #placa').attr('disabled', 'disabled');
            $('.formGuardar select').removeAttr('disabled');

            jQuery(document).ready(function($) {
                $("#foto").change(function() {
                    readURL(this);
                });
            });


            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#imgFuncionario').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        },

        actualizar: function() {
            if (this.placa == "") {
                $('.formGuardar').addClass('was-validated');
            } else {

                var app = this;
                var formData = new FormData($('.formGuardar')[0]);


                //hacemos la petición ajax
                $.ajax({
                    url: './uploadFile.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function() {},
                    success: function(data) {
                        //console.log(data);
                        app.formDatos(data, "./request/updateFuncionario.php");
                    },
                    //si ha ocurrido un error
                    error: function(FormData) {
                        console.log(data)
                    }
                });
            }
        },

        eliminar: function() {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function() {
                    $.post('./request/eliminarFuncionario.php', { placa: app.placa }, function(data) {
                        // console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.search();

                        }
                    });

                },
                function() {
                    // alertify.error('Cancel');
                });
        },

        formDatos: function(data, url) {

            var app = this;
            var formData = new FormData($('.formGuardar')[0]);
            formData.append('url', data);
            formData.append('placa1', app.placa);



            //hacemos la petición ajax
            $.ajax({
                url: url,
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function() {},
                success: function(data) {


                    if (data.indexOf("UNIQUE KEY constraint") > -1) {

                        alertify.warning("Ya se encuentra Registrado");

                    }

                    app.search();


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");
                        app.resetModal();
                        if (url == "./request/updateFuncionario.php") {
                            app.search();
                        }

                    } else {
                        alertify.error("Ha ocurrido un error");
                    }

                },
                //si ha ocurrido un error
                error: function(FormData) {
                    console.log(data)
                }
            });
        },

    },
    watch: {

    },
    mounted() {
        // this.loadSelectPlaca();
        // this.loadSelectGastos();
        // this.loadSelectProveedores();


        jQuery(document).ready(function($) {
            $("#foto").change(function() {
                readURL(this);
            });
        });


    },

});