<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  } 
?>


    <!DOCTYPE html>

    <html>

    <head>
        <title>Incripcion</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="css/select2.min.css" />
        <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
        <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous"> -->

        <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

        <script type="text/javascript">







            $(function() {

                $('#t_vehiculo').attr('disabled', true);

                $.ajax({
                    url: 'request/getDepartamentos.php',
                    type: 'POST',
                    dataType: 'json'

                }).done(function(repuesta) {

                    if (repuesta.length > 0) {
                        var optionsDpto = "";

                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                optionsDpto += "<option value='" + repuesta[i].codigoDpto + "'>" + repuesta[i].nombreDpto + "</option>";

                        }

                        $("#t_vehiculo").html(optionsDpto);
                        
                    }


                }); // fin del ajax departamentos//ajax que carga la lista de departamentos

                $('#conductor_tlicencia').attr('disabled', true);

                $.ajax({
                    url: 'request/getTipoLicencia.php',
                    type: 'POST',
                    dataType: 'json'

                }).done(function(repuesta) {

                    if (repuesta.length > 0) {
                        var optionsDpto = "";

                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                optionsDpto += "<option value='" + repuesta[i].codigoDpto + "'>" + repuesta[i].nombreDpto + "</option>";

                        }

                        $("#conductor_tlicencia").html(optionsDpto);
                        $('#conductor_tlicencia').attr('disabled', false);






                    }


                }); // fin del ajax departamentos//ajax que carga la lista de departamentos



            });
        </script>


        <style type="text/css">

            table tr.cabecera {
            background-color: #00034A;
            }

            table tr.cabecera td {
            color: #fff;
            text-align: center;
            
            }

  
            .modal-lg {
                max-width: 1000px
            }
            
            label {
                font-size: 0.8em;
            }
            
            .fondoGris {
                background: #EFEFEF;
            }
            
            .form-group {
                margin-bottom: 0.2rem;
            }
            
            .nav-tabs .nav-link {
                background: #EFEFEF;
                color: #999;
            }
            
            .form-control:disabled,
            .form-control[readonly] {
                background-color: #eee;
                opacity: 0.8;
            }
            
            #btnBuscar:hover {
                color: #333;
                text-shadow: 1px 0 #CCC;
            }
            
            select {
                padding: 3px;
                width: 100%;
            }
            
            .danger label {
                color: red;
            }
            
            .danger input {
                border: 1px solid red
            }
            
            .danger select {
                border: 1px solid red
            }
            
            */
        </style>

    </head>

    <body>

        <div id="app">

            <header>
                <p class="text-center fondoGris" style="padding: 10px;">

                    Inscripcion de Vehiculos

                </p>
            </header>


            <div class="container-fluid">
                <div class="row" style="margin-bottom: 10px;">

                    <div class="col-6">
                        <button class="btn btn-primary btn-sm" @click="nuevo()">Nuevo</button>
                    </div>
                    <div class="col-6">
                        <div class="input-group float-right input-group-sm">
                            <input type="text" class="form-control" v-model="inputSearch" placeholder="Buscar por placa del Vehiculo">
                            <span class="input-group-btn">
			                      <button type="button" class="btn btn-primary" @click="search()">Buscar</button>
			                    </span>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-12">
                        <table class="table table-sm">
                            <thead class="fondoGris">
                                <tr class="cabecera">
                                    <td>Placa</td>
                                    <td>T. Vehiculo</td>
                                    <td>Marca</td>
                                    <td>Modelo</td>
                                    <td>Propietario</td>
                                    <td>Conductor</td>
                                    <td>Cuadrilla</td>
                                    <td>T. Licencia</td>
                                    <td style="text-align: center;" width="100">+</td>
                                </tr>
                            </thead>
                            <tbody id="detalle_gastos" style="font-size: .8em">

                                <tr v-for="(dato, index) in listFuncionarios">
                                    <td v-text="dato.placa"></td>
                                    <td v-text="dato.t_vehiculo"></td>
                                    <td v-text="dato.marca"></td>
                                    <td v-text="dato.modelo"></td>
                                    <td v-text="dato.propietario_nombre"></td>
                                    <td v-text="dato.conductor_nombre"></td>
                                    <td v-text="dato.cuadrilla"></td>
                                    <td v-text="dato.conductor_tlicencia"></td>
                                    <td width="100" style="text-align: center;">
                                        <i class="fa fa-eye" title="" style="cursor:pointer; margin: 0 0 10px 10px;" @click="verFuncionario(dato)"></i>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLabel">Nueva Inscripcion
                                <small>( Los campos con <code>*</code> son obligatorios )</small></h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
                        </div>

                        <form enctype="multipart/form-data" class="formGuardar">

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-7">

                                        <div class="row">

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="placa">Placa <code>*</code></label>
                                                    <input type="text" id="placa" name="placa" class="form-control" required aria-label="placa" v-model="placa" maxlength="10">
                                                    <div class="invalid-feedback">Este campo es requerido</div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <div><label>T. Vehiculo</label><select id='t_vehiculo' name="t_vehiculo" class="form-control" required aria-label="t_vehiculo" v-model="t_vehiculo"></select></div>
                                                    <div class="invalid-feedback">Este campo es requerido</div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="color">Color </label>
                                                    <input type="text" id="color" name="color" class="form-control" required aria-label="color" v-model="color" maxlength="20">
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="marca">Marca </label>
                                                    <input type="text" id="marca" name="marca" class="form-control" required aria-label="marca" v-model="marca" maxlength="20">
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="cuadrilla">Cuadrilla </label>
                                                    <input type="text" id="cuadrilla" name="cuadrilla" class="form-control" required aria-label="cuadrilla" v-model="cuadrilla" maxlength="10">
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="modelo">Modelo </label>
                                                    <input type="text" id="modelo" name="modelo" class="form-control" required aria-label="modelo" v-model="modelo" maxlength="4">
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="cc">CC </label>
                                                    <input type="text" id="cc" name="cc" class="form-control"  aria-label="cc" v-model="cc" maxlength="10">
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                    <div class="col-5">

                                        <div class="row">

                                            <div class="col-10">
                                                <img id="imgFuncionario" src="img/user.png" alt="" width="100%">
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-6" v-if="btnFile">
                                                <div class="form-group">
                                                    <label for="foto">Foto <code>*</code></label>
                                                    <input type="file" class="form-control"  id="foto" accept="image/*" name="foto" required />
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>


































                                <div class="row">

                                    <div class="container">

                                        <br>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home">Datos del propietario</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#menu1">Datos del conductor</a>
                                            </li>

                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div id="home" class="container tab-pane active"><br>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="propietario_cc">Cedula<code>*</code></label>
                                                        <input type="text" id="propietario_cc" name="propietario_cc" required class="form-control" aria-label="propietario_cc" v-model="propietario_cc" maxlength="10">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="propietario_nombre">Nombres<code>*</code></label>
                                                        <input type="text" id="propietario_nombre" name="propietario_nombre" class="form-control" required aria-label="propietario_nombre" v-model="propietario_nombre" maxlength="40">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="propietario_direccion">Direccion<code>*</code></label>
                                                        <input type="text" id="propietario_direccion" name="propietario_direccion" class="form-control" required aria-label="propietario_direccion" v-model="propietario_direccion" maxlength="40">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="propietario_telefono">Telefono<code>*</code></label>
                                                        <input type="text" id="propietario_telefono" name="propietario_telefono" class="form-control" required aria-label="propietario_telefono" v-model="propietario_telefono" maxlength="15">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>

                                            </div>


                                            <!-- Datos conductor -->

                                            <div id="menu1" class="container tab-pane fade"><br>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="conductor_cc">Cedula <code>*</code></label>
                                                        <input type="text" id="conductor_cc" name="conductor_cc" class="form-control" required aria-label="conductor_cc" v-model="conductor_cc" maxlength="10">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="conductor_nombre">Nombre <code>*</code></label>
                                                        <input type="text" id="conductor_nombre" name="conductor_nombre" class="form-control" required aria-label="conductor_nombre" v-model="conductor_nombre" maxlength="40">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="conductor_direccion">Direccion <code>*</code></label>
                                                        <input type="text" id="conductor_direccion" name="conductor_direccion" class="form-control" required aria-label="conductor_direccion" v-model="conductor_direccion" maxlength="40">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <label for="conductor_telefono">Telefono <code>*</code></label>
                                                        <input type="text" id="conductor_telefono" name="conductor_telefono" class="form-control" required aria-label="conductor_telefono" v-model="conductor_telefono" maxlength="15">
                                                        <div class="invalid-feedback">Este campo es requerido</div>

                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group input-group-sm">
                                                        <div><label>T. Licencia</label><select id='conductor_tlicencia' name="conductor_tlicencia" class="form-control" required required aria-label="conductor_tlicencia" v-model="conductor_tlicencia"></select></div>
                                                        <div class="invalid-feedback">Este campo es requerido</div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" v-if="btnCancelar" class="btn btn-secondary btn-sm" @click="resetModal()">Cancelar</button>

                                <button type="button" v-if="btnActualizar" class="btn btn-primary btn-sm" @click="actualizar()">Actualizar</button>
                                <button type="button" v-if="btnEditar" class="btn btn-info btn-sm" @click="editar()">Editar</button>
                                <button type="button" v-if="btnEliminar" class="btn btn-danger btn-sm" @click="eliminar()">Eliminar</button>

                                <button type="button" v-if="btnGuardar" class="btn btn-primary btn-sm" @click="guardar()">Guardar</button>

                            </div>

                        </form>

                    </div>


                </div>
            </div>
        </div>


        </div>



        <script src="js/jquery/jquery-3.2.1.min.js"></script>
        <script src="js/select2.min.js"></script>
        <script src="js/bootstrap/popper.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/alertify/alertify.min.js"></script>
        <script src="js/vue/vue.js"></script>
        <script src="js/app.js"></script>



    </body>

    </html>