<?php
  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

  include("../../../init/gestion.php");
  // include("gestion.php");


  $ID = $_POST['funcionario'];

  


  $sql = "SELECT
                ve_placa,
                ve_tipove,
                ve_cuadrilla,
                ve_color,
                ve_marca,
                ve_modelo,                
                ve_dp_cedula,
                ve_dp_nombre,
                ve_dp_direccion,
                ve_dp_telefono,
                ve_dc_cedula,
                ve_dc_nombre,
                ve_dc_direccion,
                ve_dc_telefono,
                ve_ccosto,
                ve_tipo_licencia_conductor                         
          FROM VEHICULOS where ve_placa like '%".$ID."%'
          ORDER BY ve_placa";

  $return_arr = array();

  $result = ibase_query($conexion, $sql);

  while($fila = ibase_fetch_row($result)){
    $row_array['placa'] = utf8_encode($fila[0]);
    $row_array['t_vehiculo'] = utf8_encode($fila[1]);
    $row_array['cuadrilla'] = utf8_encode($fila[2]);
    $row_array['color'] = utf8_encode($fila[3]);
    $row_array['marca'] = utf8_encode($fila[4]);
    $row_array['modelo'] = utf8_encode($fila[5]);
    $row_array['propietario_cc'] = utf8_encode($fila[6]);
    $row_array['propietario_nombre'] = utf8_encode($fila[7]);
    $row_array['propietario_direccion'] = utf8_encode($fila[8]);
    $row_array['propietario_telefono'] = utf8_encode($fila[9]);
    $row_array['conductor_cc'] = utf8_encode($fila[10]);
    $row_array['conductor_nombre'] = utf8_encode($fila[11]);
    $row_array['conductor_direccion'] = utf8_encode($fila[12]);
    $row_array['conductor_telefono'] = utf8_encode($fila[13]);
    $row_array['cc'] = utf8_encode($fila[14]);
    $row_array['conductor_tlicencia'] = utf8_encode($fila[15]);
    array_push($return_arr, $row_array);
  }
  echo json_encode($return_arr);

?>