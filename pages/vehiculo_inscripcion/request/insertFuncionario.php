<?php

  define('MAX_SEGMENT_SIZE', 65535);

  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

include("../../../init/gestion.php"); 



function blob_create($data) {
    if (strlen($data) == 0)
        return false;
    $handle = ibase_blob_create();
    $len = strlen($data);
    for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
        $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
        $buf = substr($data, $pos, $buflen);
        ibase_blob_add($handle, $buf);
    }
    return ibase_blob_close($handle);
}


  $placa = $_POST['placa'];
  $t_vehiculo = $_POST['t_vehiculo'];
  $color = $_POST['color'];
  $marca = $_POST['marca'];
  $cuadrilla = $_POST['cuadrilla'];
  $modelo = $_POST['modelo'];
  $cc = $_POST['cc'];
  $propietario_cc = $_POST['propietario_cc'];
  $propietario_nombre = $_POST['propietario_nombre'];
  $propietario_direccion = $_POST['propietario_direccion'];
  $propietario_telefono = $_POST['propietario_telefono'];
  $conductor_cc = $_POST['conductor_cc'];
  $conductor_nombre = $_POST['conductor_nombre'];
  $conductor_direccion = $_POST['conductor_direccion'];
  $conductor_telefono = $_POST['conductor_telefono'];
  $conductor_tlicencia = $_POST['conductor_tlicencia'];

  

  $url = $_POST['url'];

    



if ($url == '0') {
        $foto = blob_create(file_get_contents("../img/user.png"));
    }else{
        $foto = blob_create(file_get_contents("../temp/".$url));
}


  $sql = "INSERT into vehiculos
            ( 
                ve_placa,
                ve_tipove,
                ve_cuadrilla,
                ve_color,
                ve_marca,
                ve_modelo,                
                ve_dp_cedula,
                ve_dp_nombre,
                ve_dp_direccion,
                ve_dp_telefono,
                ve_dc_cedula,
                ve_dc_nombre,
                ve_dc_direccion,
                ve_dc_telefono,
                ve_ccosto,
                ve_tipo_licencia_conductor,
                ve_foto
            )
            
            
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  // $result = ibase_query($conexion, $sql);

    $consulta = ibase_prepare($conexion , $sql);
    $result = ibase_execute($consulta,
                                        utf8_decode($placa),
                                        utf8_decode($t_vehiculo),
                                        utf8_decode($cuadrilla),
                                        utf8_decode($color),
                                        utf8_decode($marca),
                                        utf8_decode($modelo),
                                        utf8_decode($propietario_cc),
                                        utf8_decode($propietario_nombre),
                                        utf8_decode($propietario_direccion),
                                        utf8_decode($propietario_telefono),
                                        utf8_decode($conductor_cc),
                                        utf8_decode($conductor_nombre),
                                        utf8_decode($conductor_direccion),
                                        utf8_decode($conductor_telefono),
                                        utf8_decode($cc),
                                        utf8_decode($conductor_tlicencia),
                                        $foto
            );

if ($url != '0') {
       unlink("../temp/".$url);//acá le damos la direccion exacta del archivo
}

echo $result;

?>