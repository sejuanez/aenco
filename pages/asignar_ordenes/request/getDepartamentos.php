<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];
	
	$consulta = "SELECT o.ot_dpto, d.de_nombre, COUNT(*) Cantidad FROM ot_creditos o LEFT JOIN departamentos d on d.de_codigo=o.ot_dpto WHERE (o.ot_ejecutada<>'S' or (o.ot_ejecutada is null)) and (o.ot_tecnico='' or (o.ot_tecnico is null)) GROUP BY o.ot_dpto,d.de_nombre";
	

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$row_array['codigoDpto'] = null;
	$row_array['nombreDpto'] = '(Todos)';
	
	array_push($return_arr, $row_array);

	while($fila = ibase_fetch_row($result)){
		
		/*$row_array['codigo'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]);*/
		$row_array['codigoDpto'] = utf8_encode($fila[0]);
		$row_array['nombreDpto'] = utf8_encode($fila[1]." (".$fila[2].")");
		
		//$row_array['codigoMun'] = utf8_encode($fila[2]);
		//$row_array['nombreMun'] = utf8_encode($fila[3]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>