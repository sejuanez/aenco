<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");
	
	$dpto = $_POST["dpto"];
	
			
	$consulta = "SELECT o.ot_mpio, m.mu_nombre, COUNT(*) Cantidad FROM ot_creditos o LEFT JOIN municipios m on m.mu_depto=o.ot_dpto AND m.mu_codigomun=o.ot_mpio WHERE (o.ot_dpto='".$dpto."' AND o.ot_ejecutada<>'S' OR (o.ot_ejecutada is null)) AND (o.ot_tecnico='' OR (o.ot_tecnico is null)) GROUP BY o.ot_mpio, m.mu_nombre";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$row_array['codigo'] = null;
	$row_array['nombre'] = '(Todos)';
	array_push($return_arr, $row_array);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['codigo'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]." (".$fila[2].")");
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>