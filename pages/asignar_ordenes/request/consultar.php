<?php
	session_start();
  	if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	
	include("../../../init/gestion.php");
	
	$dpto = $_POST["dpto"];
	$mun = $_POST["mun"];
	$cgto = $_POST["cgto"];
	$barrio = $_POST["barrio"];

		
	$consulta = "SELECT CAST('N' AS VARCHAR(2)) SELECCIONADO, OT_NUMERO OT_NUMERO, OT_CEDULA CEDULA, OT_APELLIDO1 APELLIDO1, OT_APELLIDO2 APELLIDO2, OT_NOMBRE1 NOMBRE1, OT_NOMBRE2 NOMBRE2, d.de_nombre DPTO, m.mu_nombre MUNICIPIO, C.co_nombre CTO, b.ba_nombarrio BARRIO, OT_DIRECCION DIRECCION, OT_PTO_REF REFERENCIA, OT_TELEFONOS TELEFONOS, OT_FECHA_PROGRAMACION FECHA_VISITA, OT_HORA_PROGRAMACION HORA_VISITA, t.to_descripcion TIPO_ORDEN, OT_OBSERVACION, OT_FECHA_CREA FECHA_GENERA, OT_HORA_CREA HORA_GENERA, OT_USU_CREA USUARIO_SISEMA, OT_TECNICO TECNICO from ot_creditos o left join departamentos d on d.de_codigo=o.ot_dpto left join municipios m on m.mu_depto=o.ot_dpto and m.mu_codigomun=o.ot_mpio left join corregimientos c on c.co_depto=o.ot_dpto and c.co_municipio = o.ot_mpio and c.co_codcorregimiento=o.ot_corregimiento left join barrios b on b.ba_codbarrio=o.ot_barrio and b.ba_depto=o.ot_dpto and b.ba_mpio=o.ot_mpio and b.ba_sector=o.ot_corregimiento left join tipo_orden_servicio t on t.to_codigo=o.ot_tipo_visita where (O.ot_tecnico is null or (O.ot_tecnico='') and (O.ot_ejecutada is null or (O.ot_ejecutada='')))";

	if($dpto!='null'&&$dpto!=''){
		$consulta.=" and o.ot_dpto='".$dpto."'";
		
	}

	if($mun!='null'&&$mun!=''){
		$consulta.=" and o.ot_mpio='".$mun."'";
		
	}
	if($cgto!='null'&&$cgto!=''){
		$consulta.=" and o.ot_corregimiento='".$cgto."'";
		
	}
	if($barrio!='null'&&$barrio!=''){
		$consulta.=" and o.ot_barrio='".$barrio."'";
		
	}

	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

		
	while($fila = ibase_fetch_row($result)){
		
		$row_array['seleccionado'] = utf8_encode($fila[0]);
		$row_array['ot_numero'] = utf8_encode($fila[1]);
		$row_array['cedula'] = utf8_encode($fila[2]);
		$row_array['apellido1'] = utf8_encode($fila[3]);
		$row_array['apellido2'] = utf8_encode($fila[4]);
		$row_array['nombre1'] = utf8_encode($fila[5]);
		$row_array['nombre2'] = utf8_encode($fila[6]);
		$row_array['dpto'] = utf8_encode($fila[7]);
		$row_array['mun'] = utf8_encode($fila[8]);
		$row_array['cgto'] = utf8_encode($fila[9]);
		$row_array['barrio'] = utf8_encode($fila[10]);
		$row_array['direccion'] = utf8_encode($fila[11]);
		$row_array['ptoref'] = utf8_encode($fila[12]);
		$row_array['tels'] = utf8_encode($fila[13]);
		$row_array['fechavisita'] = utf8_encode($fila[14]);
		$row_array['horavisita'] = utf8_encode($fila[15]);
		$row_array['tipoorden'] = utf8_encode($fila[16]);
		$row_array['obs'] = utf8_encode($fila[17]);
		$row_array['fechagen'] = utf8_encode($fila[18]);
		$row_array['horagen'] = utf8_encode($fila[19]);
		$row_array['usuario'] = utf8_encode($fila[20]);
		$row_array['tecnico'] = utf8_encode($fila[21]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>