<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

    //header('Content-Encoding: UTF-8');//nueva

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=compras_proveedores_por_mes_desde_".$_GET["mesini"]."-".$_GET["anioini"]."_hasta_".$_GET["mesfin"]."-".$_GET["aniofin"]."xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	$consulta = "SELECT PR_RAZONSOCIAL, extract(month from COM_FECHARECIB) mes, extract(year from COM_FECHARECIB) ano, round(sum(DCOM_CANTIDAD*DCOM_VRNETO)) vrneto, round(sum(DCOM_CANTIDAD*DCOM_VRIVA)) vriva from TEM_DETALLECOMPRAS left join PROVEEDORES on PR_NIT=DCOM_PROVE left join ARTICULOS on AR_CODIGO=DCOM_CODIGOAR left join TEM_COMPRAS on COM_PROVEEDOR=DCOM_PROVE and  COM_INGRESO=DCOM_INGRESO where extract(month from COM_FECHARECIB) between '".$mesini."' and '".$mesfin."' and extract(year from COM_FECHARECIB) between '".$anioini."' and '".$aniofin."' and DCOM_PROVE ='".$nit."' group by PR_RAZONSOCIAL, extract(month from COM_FECHARECIB), extract(year from COM_FECHARECIB)";

	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Razon social</td>".
						"<td>Mes-Año</td>".
						"<td>Valor neto</td>".
						"<td>Valor IVA</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."-".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>