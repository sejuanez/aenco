<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Compras proveedor  por mes</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico-tecnico").width();
					var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);
				}
			});
			

			$(window).resize(function() {
			    	var ancho = +$("#div-grafico-tecnico").width();
			    	var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);
			});
		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores ($)'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            //valueSuffix: ' Pesos'
			            
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};





			$('#proveedor').attr('disabled',true);

			$("#consultar").hide();
			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();

			$("#div-total").hide();
			$("#div-neto").hide();
			$("#div-iva").hide();

			
			//ajax que carga la lista de vendedoress
	        $.ajax({
	            url:'request/getProveedores.php',
	            type:'POST',
	            dataType:'json'
	                    
	          }).done(function(repuesta){
	            
	            if(repuesta.length>0){
	                var options="";
	                          
	                for(var i=0;i<repuesta.length;i++){
	                  if(repuesta[i]!=null)
	                    options+="<option value='"+repuesta[i].codProveedor+"'>"+repuesta[i].nombreProveedor+"</option>";
	                  
	                }
	              
	                $("#proveedor").html(options);           
	                $('#proveedor').attr('disabled',false);

	                $("#consultar").show();
	            }
	            
	            
	        });// fin del ajax vendedores




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				$("#div-neto").hide();
				$("#div-iva").hide();
				$("#div-total").hide();
				
				


				/*var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;*/

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				/*var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;*/

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;		        
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



		        // ajax GENERAL

				$.ajax({
					url:'request/general.php',
	                type:'POST',
	                dataType:'json',
	                //data:{fechaini:fechaIni , fechafin:fechaFin}
	                data:{nit:$("#proveedor").val(), mesini:mesIni , mesfin:mesFin, anioini:$('#txtFechaIni').datepicker('getDate').getFullYear(), aniofin:$('#txtFechaFin').datepicker('getDate').getFullYear()}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

											
											
						
						var filas="";
						var raz=[];						
						var periodos=[];						
						var vlrneto=[];
						var vlriva=[];
						var total = totalNeto = totalIva=0;
						//var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var monthNamesShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

						for (var i=0; i<repuesta.length; i++) {

							//raz[i] = repuesta[i].raz;
							periodos[i] = monthNamesShort[(+repuesta[i].mes)-1]+"-"+repuesta[i].anio;
							vlrneto[i] = +repuesta[i].vlrneto;
							vlriva[i] = +repuesta[i].vlriva;

							totalNeto+= +repuesta[i].vlrneto;
							totalIva+= +repuesta[i].vlriva;
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].raz+"</td><td>"+monthNamesShort[(+repuesta[i].mes)-1]+"-"+repuesta[i].anio+"</td><td>"+accounting.formatMoney(+repuesta[i].vlrneto, {symbol : '$', precision : 0,thousand : ','})+"</td>"+"<td>"+accounting.formatMoney(+repuesta[i].vlriva, {symbol : '$', precision : 0,thousand : ','})+"</td></tr>";
						}

						total = totalNeto + totalIva;


						$("#total-neto").html(accounting.formatMoney(totalNeto, {symbol : '$', precision : 0,thousand : ','}));		
						$("#div-neto").show();

						$("#total-iva").html(accounting.formatMoney(totalIva, {symbol : '$', precision : 0,thousand : ','}));		
						$("#div-iva").show();

						$("#total").html(accounting.formatMoney(total, {symbol : '$', precision : 0,thousand : ','}));		
						$("#div-total").show();
						
						$("#tabla-general tbody").html(filas);

						var series=[
										{
											name:'Valor ($)', 
											data:vlrneto,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				//format: '${point.y}', // one decimal
				                              	formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						$("#contenido").show();
						dibujarGraficoBarras("grafico-tecnico",periodos,series,colors);

						var series=[
										{
											name:'Valor ($)', 
											data:vlriva,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];
						dibujarGraficoBarras("grafico-municipio",periodos,series,colors);


						$("#exportar a").attr("href","request/exportarExcel.php?nit="+$("#proveedor").val()+"&mesini="+mesIni+"&mesfin="+mesFin+"&anioini="+$('#txtFechaIni').datepicker('getDate').getFullYear()+"&aniofin="+$('#txtFechaFin').datepicker('getDate').getFullYear());
						$("#exportar").show();
	
					}
					else{												
						$("#total-neto").html("");		
						$("#div-neto").hide();
						$("#total-iva").html("");		
						$("#div-iva").hide();	
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");
						$("#exportar").hide();								
					}

					$("#consultando").hide();
					$("#consultar").show();
				});

			});//fin clic Consultar
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Compras proveedor  por mes</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<div><label>Proveedor</label><select id='proveedor'></select></div>

				<div id="div-subtotales">
	              	<div id="div-neto"> 
	              		<h5>Neto</h5>       
	                	<h4 id="total-neto">0</h4>                	
	              	</div>

	              	<div id="div-iva"> 
	              		<h5>IVA</h5>       
	                	<h4 id="total-iva">0</h4>                	
	              	</div>

	              	<div id="div-total"> 
						<h5>Total</h5>       
	                	<h4 id="total">0</h4>                	
              		</div>
              	</div><!--fin div-subtotales -->
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Datos</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Valor neto</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Valor IVA</span></a></li>
			
			</ul>


			<div id='tab1'>
				<table id='tabla-general'>
					<thead>
						<tr class='cabecera'><td>Razon social</td><td>Mes-Año</td><td>Valor neto</td><td>Valor IVA</td></tr>
					</thead>	

					<tbody>
						
					</tbody>
				</table>
			</div>


			<div id='tab2'>
				<div id='div-grafico-tecnico'>
					<div id='grafico-tecnico'></div>
				</div>
			</div>


			<div id='tab3'>
				<div id='div-grafico-municipio'>
					<div id='grafico-municipio'></div>
				</div>
			</div>


		</div>

	</div>

	

</body>
</html>