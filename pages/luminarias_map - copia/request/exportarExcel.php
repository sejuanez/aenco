<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	// header("Content-type: application/vnd.ms-excel; name='excel'");
 	// header("Content-Disposition: filename=luminarias_".$_GET["municipio"]."_".$_GET["departamento"].".xls");
	// header("Pragma: no-cache");
	// header("Expires: 0");

  echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$dpto = $_GET["dpto"];
  $mun = $_GET["mun"];
		
	$consulta = "SELECT DA.dla_longitud LON_INICIAL, DA.dla_latitud LAT_INICIAL, DA.dla_nodo NOD_INICIAL, DA.dla_nodo_anterior NODO_ANTES, (DA.dla_longitud) LON_ANTES, (DA.dla_latitud) LAT_ANTES, da.dla_pto_fisico, da.dla_tipo_lampara, cast(da.dla_potencia as integer) potencia, da.dla_tipo_encendido, da.dla_direccion, da.dla_luminaria, MAT.ma_descripcion, ds.descripcion, da.dla_serielum, d.de_nombre dpto, m.mu_nombre mpio, c.co_nombre corregimiento, b.ba_nombarrio, da.dla_fecha FECHA_CENSO,da.dla_acta FROM busca_serielum ('".$dpto."', '".$mun."') bs inner join dato_lev_alumbrado da on da.dla_serielum=bs.dla_serielum and da.dla_fecha=bs.fechaej and da.dla_dpto = '".$dpto."' and da.dla_mpio= '".$mun."' and da.dla_longitud<>0 LEFT JOIN departamentos d on d.de_codigo=da.dla_dpto LEFT JOIN municipios m on m.mu_depto = da.dla_dpto and m.mu_codigomun=da.dla_mpio LEFT JOIN corregimientos c on c.co_depto=da.dla_dpto and c.co_municipio=da.dla_mpio and c.co_codcorregimiento=da.dla_corregimiento LEFT JOIN barrios b on b.ba_depto=da.dla_dpto and b.ba_mpio=da.dla_mpio and b.ba_sector=da.dla_corregimiento and b.ba_codbarrio=da.dla_barrio LEFT JOIN materiales MAT ON MAT.ma_codigo=DA.dla_codigomat LEFT JOIN descripcion_series DS on ds.grupo=MAT.ma_grupo and ds.codigo=da.dla_codmarca";

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					//"<tr class='cabecera' style='background-color:yellow'><td>LON_INICIAL</td><td>LAT_INICIAL</td><td>NOD_INICIAL</td><td>DLA_PTO_FISICO</td><td>DLA_TIPO_LAMPARA</td><td>POTENCIA</td><td>DLA_TIPO_ENCENDIDO</td><td>DLA_DIRECCION</td><td>MA_DESCRIPCION</td><td>DESCRIPCION</td><td>DLA_SERIELUM</td><td>DPTO</td><td>MPIO</td><td>CORREGIMIENTO</td><td>BA_NOMBARRIO</td></tr>".

          //"<tr class='cabecera'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>".

          "<tr class='cabecera'><td>LONGITUD</td><td>LATITUD</td><td>NODO</td><td>PTO_FISICO</td><td>TIPO_LAMPARA</td><td>POTENCIA</td><td>TIPO_ENCENDIDO</td><td>DIRECCION</td><td>DESCRIPCION</td><td>MARCA</td><td>SERIE</td><td>DPTO</td><td>MPIO</td><td>CORREGIMIENTO</td><td>BARRIO</td><td>ULTIMA VISITA</td></tr>";

	while($fila = ibase_fetch_row($result)){

		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0]).//lon inicial
          "</td><td>".utf8_encode($fila[1]).//lat inicial
          "</td>"."<td>".utf8_encode($fila[2]).//nodo inical
          "</td><td>".utf8_encode($fila[6]).//punto fisico
          "</td><td>".utf8_encode($fila[7]).//tipo lampara
          "</td><td>".utf8_encode($fila[8]).//potencia
          "</td><td>".utf8_encode($fila[9]).//tipo encendido
          "</td><td>".utf8_encode($fila[10]).//dla direccion
          "</td><td>".utf8_encode($fila[12]).//material descripcion
          "</td><td>".utf8_encode($fila[13]).//descripcion
          "</td><td>".utf8_encode($fila[14]).//serie luminaria
          "</td><td>".utf8_encode($fila[15]).//dpto
          "</td><td>".utf8_encode($fila[16]).//mpio
          "</td><td>".utf8_encode($fila[17]).//cgto
          "</td><td>".utf8_encode($fila[18]).//barrio
          "</td><td>".utf8_encode($fila[19]).//ultima visit
          "</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>