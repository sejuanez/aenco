
//=================================================
//=================================================

jQuery(document).ready(function ($) {

    $("#tabValorizado").click(function () {

		
        app.graficar( app.obtenerTotalesTipos(app.tablaGastos), app.obtenerTotalesProveedor(app.tablaGastos));

    });

    $("#tabCantidades").click(function () {

        app.graficar( app.obtenerTotalesTipos(app.tablaGastos), app.obtenerTotalesProveedor(app.tablaGastos));

	});
	
});

//=================================================
//=================================================


let app = new Vue({
		el:'#app',
		data : {
			btnConsultar : false,

			fechaInicial:"",
			fechaFinal:"",
			tipoGasto:"",
			placa:"",

			filter : {
				fechaInicial:"",
				fechaFinal:"",
				tipoGasto:"",
				placa:""
				},

			selectPlacas: [],
			selectTipoGastos: [],

			tablaGastos: []
		},

		methods:{
			loadSelectGastos: function (){
				var app = this;
				$.get('./request/getSelectGastos.php', function(data) {
					var data = JSON.parse(data);
					app.selectTipoGastos = data ;
				});
			},

			loadSelectPlaca: function () {
				var app = this;
				$.get('./request/getSelectPlacas.php', function(data) {
					app.selectPlacas = JSON.parse(data);
					// console.log(data);
				});
			},

			showSoporte: function(data){
					$('#imgSoporte').attr('src','./request/getSoporte.php?placa='+data.PLACA+'&factura='+data.FACTURA+'&fecha='+data.FECHA+'&proveedor='+data.IDPROVEEDOR);
					$('#modalSoporte').modal('show');

			},

			exportar : function(){
				
				if(app.tablaGastos.length > 0){

					

						var elt = document.getElementById('datos');
						var wb = XLSX.utils.table_to_book(elt, {
							sheet: "Sheet JS"
						});
						return XLSX.writeFile(wb, 'reporete.xlsx');
					

				}else{
					alertify.error('No hay registros que exportar');
				}

			},

			consultar: function(){
				var app = this;

				this.filter.fechaInicial = this.fechaInicial;
				this.filter.fechaFinal = this.fechaFinal;
				this.filter.tipoGasto = this.tipoGasto;
				this.filter.placa = this.placa;

				if (this.fechaInicial != "" && this.fechaFinal != "" && this.tipoGasto != "" && this.placa != "") {

					$.post('./request/getGastos.php',{ data : this.filter }, function(data) {
						// console.log(data);
						app.tablaGastos = [];
	
						var datos = jQuery.parseJSON(data);
                        app.tablaGastos = [];
                        app.tablaGastos = datos;							

						

						var arrayTipos = app.obtenerTotalesTipos(datos);
						var arrayProveedor = app.obtenerTotalesProveedor(datos);

						app.graficar(arrayTipos,arrayProveedor);

						//console.log(arrayProveedor);

						alertify.success("Registros encontrados: "+app.tablaGastos.length);
					});

				}else{

					alertify.error("Todos los campos son obligatorios.");

				}

			},

			graficar : function (arrayDatos, arrayProveedor){

				var colors=['#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
							  
							  
				var hc_column=[];// vendedores vendidos
				var hc_column_clientesImpactados=[];// vendedores cantidad 
				
				var totalrecaudo = 0;
				var totalClientesImpactados = 0;
	
				for(var i=0; i< arrayDatos.length ;i++)
				{                           
					
					hc_column.push([arrayDatos[i].tipo,arrayDatos[i].valor]);
					
					
					totalrecaudo+= (arrayDatos[i].valor);//total preventas
					
				}

				for(var i=0; i< arrayProveedor.length ;i++)
				{                           
					
					
					hc_column_clientesImpactados.push([arrayProveedor[i].proveedor,arrayProveedor[i].valor]);
					
					totalClientesImpactados+= (arrayProveedor[i].valor);//total preventas
					
				}

				
	
										  
				totalrecaudo=accounting.formatMoney(totalrecaudo, {symbol : '$', precision : 0,thousand : ','});
				$("#total-preventas").html(totalrecaudo);
	

				totalClientesImpactados=accounting.formatMoney(totalClientesImpactados, {symbol : '$', precision : 0,thousand : ','});
				$("#total-clientesImpactados").html(totalClientesImpactados);
				
	
				app.drawColumnChart(hc_column,"grafico-preventas",colors);//DIBUJAR EL GRAFICO preventas
				app.drawColumnChartSinFormato(hc_column_clientesImpactados,"grafico-clientesImpactados",colors);//DIBUJAR EL GRAFICO preventas
				$("g.highcharts-data-labels>g[opacity=0]").attr("opacity",1);
	
				$("#div-total-preventas").fadeIn(200);
				$("#div-grafico-preventas").fadeIn(200);
	
				$("#div-total-clientesImpactados").fadeIn(200);
				$("#div-grafico-clientesImpactados").fadeIn(200);
			},

			obtenerTotalesTipos : function  (arrayDatos) {

				let result = [];
	
				const elementExist = (array, value) => {
					let i = 0;
					while (i < array.length) {
						if (array[i].tipo == value) return i;
						i++;
					}
					return false;
				}
	
				arrayDatos.forEach((e) => {
					let i = elementExist(result, e.TIPO_GASTO);
					if (i === false) {
						// Si no existe, creo agrego un nuevo objeto.
						var ValorPedido_float = parseFloat(e.VALOR);
						result.push({
							"tipo": e.TIPO_GASTO,
							"valor": ValorPedido_float
						});
					} else {
						// Si el ya existe agrego el nuevo elemento a el array valor.
						
						var ValorPedido_float = parseFloat(e.VALOR);
						result[i].valor += ValorPedido_float;
					}
				});
	
				return result;
			},

			obtenerTotalesProveedor : function  (arrayDatos) {

				let result = [];
	
				const elementExist = (array, value) => {
					let i = 0;
					while (i < array.length) {
						if (array[i].proveedor == value) return i;
						i++;
					}
					return false;
				}
	
				arrayDatos.forEach((e) => {
					let i = elementExist(result, e.PROVEEDOR);
					if (i === false) {
						// Si no existe, creo agrego un nuevo objeto.
						var ValorPedido_float = parseFloat(e.VALOR);
						result.push({
							"proveedor": e.PROVEEDOR,
							"valor": ValorPedido_float
						});
					} else {
						// Si el ya existe agrego el nuevo elemento a el array valor.
						
						var ValorPedido_float = parseFloat(e.VALOR);
						result[i].valor += ValorPedido_float;
					}
				});
	
				return result;
			},
	
			drawColumnChart: function (rows, element, colors) {
	
			 
				$('#'+element).highcharts({
									   chart: {
										   type: 'column',
										   backgroundColor:'#ffffff'
									   },
									   colors:colors,
									   credits:{enabled:false},
									   title:null /*{
										   text: 'Recaudo diario'
									   }*/,
									   /*subtitle: {
										   //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
										   text:dia+'/'+mes+'/'+anio
									   },*/
									   xAxis: {
										   type: 'category',
										   labels: {
											   //rotation: -45,
											   style: {
												   fontSize: '10px',
												   fontFamily: 'Verdana, sans-serif'
											   }
										   }
									   },
									   yAxis: {
										   min: 0,
										   title:null /*{
											   text: 'Recaudo diario ($)'
										   }*/
									   },
									   legend: {
										   enabled: false
									   },
									   tooltip: {
										   pointFormat: '<b>${point.y}</b>',
										   pointFormatter: function(){return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});}
									   },
									   series: [{
										   name: 'recaudo',
										   data:rows,
										   colorByPoint: true,
										   dataLabels: {
											   enabled: true,
											   //rotation: -90,
											   color: '#000',
											   align: 'center',
											  // format: '${point.y}', // one decimal
											   formatter:function(){
												   return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
											   },
											   y: 10, // 10 pixels down from the top
											   padding:20,
											   style: {
												   fontSize: '10px',
												   fontFamily: 'Verdana, sans-serif'
											   }
										   }
									   }]
								   });
			},
	 
			drawColumnChartSinFormato: function(rows, element, colors) {
	 
			  
				$('#'+element).highcharts({
									   chart: {
										   type: 'column',
										   backgroundColor:'#ffffff'
									   },
									   colors:colors,
									   credits:{enabled:false},
									   title:null /*{
										   text: 'Recaudo diario'
									   }*/,
									   /*subtitle: {
										   //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
										   text:dia+'/'+mes+'/'+anio
									   },*/
									   xAxis: {
										   type: 'category',
										   labels: {
											   //rotation: -45,
											   style: {
												   fontSize: '10px',
												   fontFamily: 'Verdana, sans-serif'
											   }
										   }
									   },
									   yAxis: {
										   min: 0,
										   title:null /*{
											   text: 'Recaudo diario ($)'
										   }*/
									   },
									   legend: {
										   enabled: false
									   },
									   tooltip: {
										   pointFormat: '<b>{point.y}</b>',
										   pointFormatter: function(){return accounting.formatMoney(this.y, {symbol : '', precision : 0,thousand : ','});}
									   },
									   series: [{
										   name: 'recaudo',
										   data:rows,
										   colorByPoint: true,
										   dataLabels: {
											   enabled: true,
											   //rotation: -90,
											   color: '#000',
											   align: 'center',
											  // format: '${point.y}', // one decimal
											   formatter:function(){
												   return accounting.formatMoney(this.y, {symbol : '', precision : 0,thousand : ','});
											   },
											   y: 10, // 10 pixels down from the top
											   padding:20,
											   style: {
												   fontSize: '10px',
												   fontFamily: 'Verdana, sans-serif'
											   }
										   }
									   }]
								   });
			}


		},
		watch: {
	        fechaFinal: function(){

 				var f1= this.fechaInicial.split("-");
 				var fi = new Date(parseInt(f1[0]), parseInt(f1[1]-1), parseInt(f1[2]));
 				fi.setHours(0,0,0,0);

 				var f2= this.fechaFinal.split("-");
 				var ff = new Date(parseInt(f2[0]), parseInt(f2[1]-1), parseInt(f2[2]));
 				ff.setHours(0,0,0,0);


	        	if (fi.getTime() > ff.getTime()) {
	        		alertify.error("No puede ingresar una fecha mayor a la inicial");
	        		this.fechaFinal = "";
	        	}

	        }

	    },
		mounted() {
			this.loadSelectPlaca();
			this.loadSelectGastos();

		},

});