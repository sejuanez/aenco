<?php
include("../../../init/gestion.php");

$datos = $_POST['data'];
// $datos = $_GET['data'];

$data = json_decode($datos);

// var_dump($data);


   require_once 'PHPExcel.php';
   $objPHPExcel = new PHPExcel();

   //Informacion del excel
   $objPHPExcel->
    getProperties()
        ->setCreator("CONCORPLANET MED")
        ->setLastModifiedBy("CONCORPLANET MED")
        ->setTitle("Gasto de Vehiculos")
        ->setSubject("Gasto de Vehiculos")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("CONCORPLANET MED")
        ->setCategory("Gastos");

	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'FECHA')
        ->setCellValue('B1', 'PLACA')
        ->setCellValue('C1', 'PROPIETARIO')
        ->setCellValue('D1', 'DESCUENTO')
        ->setCellValue('E1', 'TIPO_VEHICULO')
        ->setCellValue('F1', 'TIPO_DE_GASTO')
        ->setCellValue('G1', 'PROVEEDOR')
        ->setCellValue('H1', 'VALOR')
        ->setCellValue('I1', 'DOCUMENTO');



   $i = 2;
   foreach ($data as $value) {
   		// echo $value->PLACA;
      $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $value->FECHA)
            ->setCellValue('B'.$i, $value->PLACA)
            ->setCellValue('C'.$i, $value->PROPIETARIO)
            ->setCellValue('D'.$i, $value->DESCUENTO)
            ->setCellValue('E'.$i, $value->TIPO_VEHICULO)
            ->setCellValue('F'.$i, $value->TIPO_GASTO)
            ->setCellValue('G'.$i, $value->PROVEEDOR)
            ->setCellValue('H'.$i, $value->VALOR)
            ->setCellValue('I'.$i, $value->FACTURA);
      $i++;
	}


function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill('')
    ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
    'startcolor' => array('rgb' => $color)
    ));
 }

cellColor('A1', 'c5c7c7');
cellColor('B1', 'c5c7c7');
cellColor('C1', 'c5c7c7');
cellColor('D1', 'c5c7c7');
cellColor('E1', 'c5c7c7');
cellColor('F1', 'c5c7c7');
cellColor('G1', 'c5c7c7');
cellColor('H1', 'c5c7c7');
cellColor('I1', 'c5c7c7');


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="gastosVehiculos.xls"');
header('Cache-Control: max-age=0');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
// $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
// $objWriter->save('php://output');
exit;

?>