<?php

include("../../../init/gestion.php");

$data = $_POST['data'];


// var_dump($data);

$hasTipoGasto = "";
if ($data['tipoGasto']<>"TODOS") {
  $hasTipoGasto = " AND  vm.vm_cod_tipomtto='".$data['tipoGasto']."'";
 }

$hasPlaca = "";
if ($data['placa']<>"TODOS") {
  $hasPlaca = " AND vm.vm_placa='".$data['placa']."'";
 }



$sql = "SELECT vm.vm_fecha_mtto FECHA,
               vm.vm_placa PLACA,
               v.ve_dp_nombre PROPIETARIO,
               coalesce(vm.vm_descuento,'NO') DESCUENTO,
               v.ve_tipove TIPO_VEHICULO,
               tv.tvdescripcion TIPO_GASTO,
               p.pr_razonsocial PROVEEDOR,
               vm.vm_valor VALOR,
               vm.vm_km KM,
               vm.vm_factura FACTURA,
               vm.vm_soporte SOPORTE,
               p.pr_codigo IDPROVEEDOR
        FROM vehiculos_mtto vm
         left join vehiculos v on v.ve_placa=vm.vm_placa
         left join tipovencimiento tv on tv.tvmodulo='VEHICULOSMTTO' and vm.vm_cod_tipomtto=tv.tvcodigo
         left join proveedores p on p.pr_codigo=vm.vm_proveedor
         where (vm.vm_fecha_mtto between '".$data['fechaInicial']."' and '".$data['fechaFinal']."')
          ".$hasTipoGasto." ".$hasPlaca."
         order by vm.vm_fecha_mtto";
        // donde vm.vm_fecha_mtto entre: fi y: ff"

$return_arr = array();

$result = ibase_query($conexion, $sql);

while($fila = ibase_fetch_row($result)){
  $row_array['FECHA'] = utf8_encode($fila[0]);
  $row_array['PLACA'] = utf8_encode($fila[1]);
  $row_array['PROPIETARIO'] = utf8_encode($fila[2]);
  $row_array['DESCUENTO'] = utf8_encode($fila[3]);
  $row_array['TIPO_VEHICULO'] = utf8_encode($fila[4]);
  $row_array['TIPO_GASTO'] = utf8_encode($fila[5]);
  $row_array['PROVEEDOR'] = utf8_encode($fila[6]);
  $row_array['VALOR'] = utf8_encode($fila[7]);
  $row_array['KM'] = utf8_encode($fila[8]);
  $row_array['FACTURA'] = utf8_encode($fila[9]);
  $row_array['SOPORTE'] = utf8_encode($fila[10]);
  $row_array['IDPROVEEDOR'] = utf8_encode($fila[11]);
  array_push($return_arr, $row_array);
}

$array = array("result"=>$return_arr);

echo json_encode($return_arr);



?>