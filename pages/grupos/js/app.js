//=================================================
//=================================================

jQuery(document).ready(function ($) {

    $(".selectGrupo").select2().change(function (e) {
        var codGrupo = $(this).val();
        var selectedIndex = document.getElementById("selectGrupo").selectedIndex
        app.onChangeMaterial(codGrupo, selectedIndex);
    });


});

//=================================================
//=================================================


let app = new Vue({
        el: '#app',
        data: {

            grupos: [],

            selectedGrupo: "",
            codigo: "",
            descripcion: "",
            serie: false,


        },

        methods: {


            loadGrupos: function (condigoGrupo) {
                var app = this;

                if (condigoGrupo != null) {

                    $.get('./request/getGrupos.php?opcion=1&codigo=' + condigoGrupo + "", function (data) {

                        var data = JSON.parse(data);

                        try {

                            if (data[0].SERIE == "S") {

                                app.serie = true;

                            } else {

                                app.serie = false;

                            }


                            app.descripcion = data[0].NOMBRE;

                            $('#selectUnidadMedida').select2().val(data[0].unidad).trigger('change')
                        } catch (e) {

                        }

                    });

                } else {

                    $.get('./request/getGrupos.php?opcion=todos', function (data) {

                        var data = JSON.parse(data);
                        app.grupos = data;


                    });
                }
            },

            onChangeMaterial: function (cod, index) {

                var app = this;


                if (index == 0) {

                    app.selectedGrupo = "";

                } else {

                    if (app.grupos[index - 1].SERIE == "S") {

                        app.serie = true;

                    } else {

                        app.serie = false;

                    }

                    app.codigo = app.grupos[index - 1].CODIGO;
                    app.descripcion = app.grupos[index - 1].NOMBRE;

                }

            },


            guardar: function () {

                var app = this;

                if (app.codigo == "" ||
                    app.descripcion == ""
                ) {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;

                    var serieInsert

                    if (app.serie) {
                        serieInsert = "S"
                    } else {
                        serieInsert = "N"
                    }

                    var formData = new FormData($('.formGuardar')[0]);
                    formData.append("serie", serieInsert)

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertGrupo.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: formData,

                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
                        //mientras enviamos el archivo
                        beforeSend: function () {
                        },
                        success: function (data) {

                            if (data.indexOf("UNIQUE KEY constraint") > -1) {

                                alertify.warning("Existe un material con este codigo");


                            }


                            //app.search();


                            if (data == 1) {

                                alertify.success("Registro guardado Exitosamente");
                                app.nuevo();
                                app.loadGrupos()

                            }

                        },
                        //si ha ocurrido un error
                        error: function (FormData) {
                            console.log(data)
                        }
                    });


                }

            },

            actualizar: function () {

                var app = this;

                if (app.codigo == "" ||
                    app.descripcion == ""
                ) {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;

                    var serieInsert

                    if (app.serie) {
                        serieInsert = "S"
                    } else {
                        serieInsert = "N"
                    }

                    var formData = new FormData($('.formGuardar')[0]);
                    formData.append("serie", serieInsert)

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateGrupo.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: formData,

                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
                        //mientras enviamos el archivo
                        beforeSend: function () {
                        },
                        success: function (data) {


                            if (data == 1) {

                                alertify.success("Registro Actualizado Exitosamente");
                                app.nuevo();
                                app.loadGrupos()

                            }

                        },
                        //si ha ocurrido un error
                        error: function (FormData) {
                            console.log(formData)
                        }
                    });


                }


            }
            ,

            nuevo: function () {

                var app = this;

                $('.formGuardar').removeClass('was-validated');

                app.codigo = "";
                app.descripcion = "";
                app.serie = false;
                $('#selectGrupo').select2().val("").trigger('change')

                app.loadGrupos()


            },

            exportar: function () {

                var app = this

                if (app.grupos.length > 0) {


                    var elt = document.getElementById('datos');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "Grupos"
                    });
                    return XLSX.writeFile(wb, 'lista_grupos.xlsx');


                } else {
                    alertify.error('No hay registros que exportar');
                }

            },

            eliminar: function () {


                var app = this;

                if (app.codigo == "") {

                    alertify.warning("No hay ningun Grupo seleccionado")

                } else {

                    alertify.confirm("Eliminar", ".. Desea eliminar este Grupo?",
                        function () {
                            $.post('./request/deleteGrupo.php', {grupo: app.codigo}, function (data) {
                                // console.log(data);
                                if (data == 1) {
                                    alertify.success("Registro Eliminado.");

                                    app.nuevo()

                                }
                            });

                        },
                        function () {
                            // alertify.error('Cancel');
                        });
                }
            },


        },


        watch:
            {}
        ,
        mounted() {


            this.loadGrupos();


        }
        ,

    })
;
