<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador


        echo
        "<script>
            window.location.href='../inicio/index.php';
            
        </script>";
        exit();
    } else {


    }
?>

<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>

    <style type="text/css">

        .contenido {
            /* 25% of viewport */
            height: 90vh;

        }

        .table-wrapper-scroll-y {
            display: block;
            height: 75vh;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .headerTabla {
            background: #00034A;
            color: #fff;
        }

        .fondoGris {
            background-color: #ededed;
        }

        .table td, .table th {
            padding: 0.25rem;
            vertical-align: top;
            border-top: 1px solid #e9ecef;
        }

        input[type="checkbox"] {
            vertical-align: middle;
        }

        label {
            font-size: 0.8em;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }


    </style>

</head>
<body>

<div id="app" class="">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Grupos

            <span id="btnExportar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="exportar()">
            	<i class="fa fa-download" aria-hidden="true"></i> Exportar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="eliminar()">
            	<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
            </span>


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="nuevo()">
            	<i class="fa fa-edit" aria-hidden="true"></i> Nuevo
            </span>

            <span id="btnActualizar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="actualizar()">
            	<i class="fa fa-pencil" aria-hidden="true"></i> Actualizar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="guardar()">
            	<i class="fa fa-save" aria-hidden="true"></i> Guardar
            </span>


        </p>
    </header>


    <div class="container contenido "
         style="max-width: 100%;">

        <form class="formGuardar">

            <div class="row align-items-center">

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="codigo">Codigo</label>
                        <input type="text" id="codigo" name="codigo" class="form-control" required
                               aria-label="codigo" v-model="codigo" maxlength="40"
                               v-on:keyup.13="loadGrupos(codigo)"
                               @blur="loadGrupos(codigo)">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" id="descripcion" name="descripcion" class="form-control" required
                               aria-label="descripcion" v-model="descripcion"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-1 ">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value=""
                                   id="serie"
                                   name="serie"
                                   v-model="serie">
                            Serie
                        </label>
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectGrupo">Grupos Registrados</label>

                        <select class="selectGrupo" id="selectGrupo" name="selectGrupo" required
                                v-model="selectedGrupo">
                            <option value="">Seleccione...</option>
                            <option v-for="selectGrupo in grupos" :value="selectGrupo.codigo">
                                {{selectGrupo.CODIGO}} - {{selectGrupo.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>


            </div>

            <hr>


        </form>
        <div id="datos" style="">
            <div class="row">

                <div class="col-12 ">
                    <div class="table-wrapper-scroll-y">
                        <table class="table table-hover table-striped ">
                            <thead class="headerTabla" style="font-size: 0.9rem;">
                            <tr class="cabecera">
                                <td>Codigo</td>
                                <td>Descripcion</td>
                                <td>Serie</td>
                            </tr>
                            </thead>
                            <tbody id="detalle_gastos" style="font-size: .8em">

                            <tr v-for="(dato, index) in grupos">
                                <td v-text="dato.CODIGO"></td>
                                <td v-text="dato.NOMBRE"></td>
                                <td v-text="dato.SERIE"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script lang="javascript" src="js/xlsx.full.min.js"></script>
<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>
</html>
