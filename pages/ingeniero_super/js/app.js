// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        codigo: "",
        tipo: "1",
        nombre: "",
        cedula: "",

        busqueda: "",

        btn_update: false,
        btn_save: false,

        divCod: true,

        tablaIngenieros: [],

        noResultados:
            false,

    },


    methods: {

        loadAnio: function () {


            var app = this;
            $.get('./request/getAno.php', function (data) {
                console.log(data)
                app.tablaIngenieros = JSON.parse(data);
            });
        }
        ,

        modal() {

            let app = this;

            app.resetCampos()

            $('#addFucionario').modal('show');
            $("#codigo").prop('disabled', false);
            $("#tipo").prop('disabled', false);

            app.btn_save = true;
            app.btn_update = false;
            app.divCod = false;
        },


        verDepartamento: function (dato) {
            let app = this;

            app.btn_save = false;
            app.btn_update = true;
            app.divCod = true;

            dato.tipo == 'Ingeniero' ? app.tipo = 1 : app.tipo = 2;

            app.codigo = dato.codigo;
            app.cedula = dato.cedula;
            app.nombre = dato.nombre;

            $("#codigo").prop('disabled', true);
            $("#tipo").prop('disabled', true);
            $('#addFucionario').modal('show');
        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        buscar: function () {
            var app = this;
            $.post('./request/getAno.php', {
                buscar: app.busqueda.toUpperCase()
            }, function (data) {
                console.log(data)
                app.tablaIngenieros = jQuery.parseJSON(data);
                app.busqueda = "";

            });
        },

        actualizar: function () {

            let app = this;

            if (app.cedula == "" ||
                app.nombre == "") {
                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");
            } else {

                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: app.codigo,
                        tipo: app.tipo,
                        cedula: app.cedula,
                        nombre: app.nombre,
                    },

                }).done(function (data) {


                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.resetModal();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {
                    console.log(data)
                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            console.log(dato)
            alertify.confirm("Eliminar", ".. Desea eliminar a " + dato.nombre + " ?",
                function () {
                    $.post('./request/deleteMes.php', {
                        codigo: dato.codigo,
                        tipo: dato.tipo,
                    }, function (data) {
                        console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {


                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }

                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {

            let app = this;

            if (app.cedula == "" ||
                app.nombre == "") {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertIng.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        tipo: app.tipo,
                        nombre: app.nombre,
                        cedula: app.cedula
                    },

                }).done(function (data) {

                    console.log(data)

                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {
                    console.log(data)
                });


            }
        },

        resetCampos: function () {

            let app = this;

            app.codigo = "";
            app.nombre = "";
            app.cedula = "";
            app.tipo = 1;
            $('#addFucionario').modal('hide');

        }
    },


    watch: {},

    mounted() {
        this.buscar();
    },

});
