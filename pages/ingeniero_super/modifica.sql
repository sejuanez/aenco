SET TERM ^ ;

create or alter procedure ING_SUPER_MODIF (
    EOPCION integer,
    ECOD varchar(10),
    ENOM varchar(40),
    ECEDULA varchar(25))
as
begin
-------------------- INGENIEROS
  if (eopcion = 1) then
  begin
      update ingenieros i
      set i.in_nombres = :enom,
            i.in_cedula = :ecedula
      where i.in_codigo = :ecod;
  end
-------------------- SUPERVISORES
  if (eopcion = 2) then
  begin
      update supervisores s
      set s.su_nombre = :enom,
            s.su_cedula = :ecedula
      where s.su_codigo = :ecod;
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON INGENIEROS TO PROCEDURE ING_SUPER_MODIF;
GRANT SELECT,UPDATE ON SUPERVISORES TO PROCEDURE ING_SUPER_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE ING_SUPER_MODIF TO SYSDBA;
