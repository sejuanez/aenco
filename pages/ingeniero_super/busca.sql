SET TERM ^ ;

create or alter procedure ING_SUPER_BUSCA (
    EOPCION integer,
    ECOD varchar(15))
returns (
    TIPO char(10),
    CODIGO varchar(10),
    NOMBRE varchar(40),
    CEDULA varchar(15))
as
BEGIN
  if (eopcion = 1) then
  begin
      FOR
        select 'Supervisor' Tipo, s.su_codigo codigo, s.su_nombre nombre, s.su_cedula cedula
        from supervisores s
        union
        select 'Ingeniero' Tipo, i.in_codigo codigo, i.in_nombres nombre, i.in_cedula cedula
        from ingenieros i
        INTO :TIPO,
             :CODIGO,
             :NOMBRE,
             :CEDULA
      DO
      BEGIN
        SUSPEND;
      END
  end
  -----------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        select 'Supervisor' Tipo, s.su_codigo codigo, s.su_nombre nombre, s.su_cedula cedula
        from supervisores s
        where upper(s.su_codigo) || upper(s.su_nombre) || s.su_cedula like '%'||(:ecod)||'%'
        union
        select 'Ingeniero' Tipo, i.in_codigo codigo, i.in_nombres nombre, i.in_cedula cedula
        from ingenieros i
        where upper(i.in_codigo) || upper(i.in_nombres) || i.in_cedula like '%'||(:ecod)||'%'
        INTO :TIPO,
             :CODIGO,
             :NOMBRE,
             :CEDULA
      DO
      BEGIN
        SUSPEND;
      END
  end
END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON SUPERVISORES TO PROCEDURE ING_SUPER_BUSCA;
GRANT SELECT ON INGENIEROS TO PROCEDURE ING_SUPER_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE ING_SUPER_BUSCA TO SYSDBA;
