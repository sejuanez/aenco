SET TERM ^ ;

create or alter procedure ING_SUPER_BORRA (
    EOPCION integer,
    ECOD varchar(10))
as
begin
-------------------- INGENIEROS
  if (eopcion = 1) then
  begin
      delete from ingenieros
      where in_codigo = :ecod;
  end
-------------------- SUPERVISORES
  if (eopcion = 2) then
  begin
      delete from supervisores
      where su_codigo = :ecod;
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON INGENIEROS TO PROCEDURE ING_SUPER_BORRA;
GRANT SELECT,DELETE ON SUPERVISORES TO PROCEDURE ING_SUPER_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE ING_SUPER_BORRA TO SYSDBA;
