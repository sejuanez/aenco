SET TERM ^ ;

create or alter procedure ING_SUPER_CREA (
    EOPCION integer,
    ECOD varchar(10),
    ENOM varchar(40),
    ECEDULA varchar(25))
as
begin
-------------------- INGENIEROS
  if (eopcion = 1) then
  begin
      insert into ingenieros (in_codigo, in_nombres, in_cedula)
      values (:ecod, :enom, :ecedula);
  end
-------------------- SUPERVISORES
  if (eopcion = 2) then
  begin
      insert into supervisores (su_codigo, su_nombre, su_cedula)
      values (:ecod, :enom, :ecedula);
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON INGENIEROS TO PROCEDURE ING_SUPER_CREA;
GRANT INSERT ON SUPERVISORES TO PROCEDURE ING_SUPER_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE ING_SUPER_CREA TO SYSDBA;
