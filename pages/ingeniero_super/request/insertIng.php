<?php

define('MAX_SEGMENT_SIZE', 65535);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");


$tipo = utf8_decode($_POST['tipo']);
$nombre = utf8_decode($_POST['nombre']);
$cedula = utf8_decode($_POST['cedula']);

$codigo = "";

$sql = 'SELECT GEN_ID(SUPERVISOR, 1) NUMERO FROM RDB$DATABASE';
$result = ibase_query($conexion, $sql);
while ($fila = ibase_fetch_row($result)) {
    $codigo = utf8_encode($fila[0]);
}


if ($tipo == '1') {
    $stmt = "EXECUTE PROCEDURE ING_SUPER_CREA(1, '" . $codigo . " ', '" . $nombre . "', '" . $cedula . "')";
} else {
    $stmt = "EXECUTE PROCEDURE ING_SUPER_CREA(2, '" . $codigo . " ', '" . $nombre . "', '" . $cedula . "')";
}


$query = ibase_prepare($stmt);
$result = ibase_execute($query);


echo $result;
