// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });

    $(".selectActividad").select2().change(function (e) {

        var usuario = $(".selectActividad").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onchangeActividad(usuario, nombreUsuario);

    });

    $(".selectActividad2").change(function (e) {

        var usuario = $(".selectActividad2").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        codigo: "",
        nombre: "",
        dias: "",

        valorActividad: "",
        selectActividades: [],

        busqueda: "",

        codigo_actualizar: "",
        codigo_modal: "",
        nombre_modal: "",
        dias_modal: "",


        tablaDepartamentos: [],

        noResultados: false,

    },


    methods: {

        loadAnio: function () {


            var app = this;
            $.post('./request/getAno.php', {
                opcion: '1',
                buscar: ''
            }, function (data) {
                app.tablaDepartamentos = JSON.parse(data);
            });
        },

        loadClaseActividades: function () {
            var app = this;
            $.get('./request/getClaseAcividades.php', function (data) {
                console.log(data)
                app.selectActividades = JSON.parse(data);
            });
        },

        onchangeActividad: function (cod, nombre) {
            var app = this;
            console.log(cod)
            app.valorActividad = cod;
        },

        verDepartamento: function (dato) {

            this.codigo_actualizar = dato.modulo;
            this.codigo_modal = dato.codigo;
            this.nombre_modal = dato.desc;
            this.dias_modal = dato.dias;
            this.valorActividad = dato.modulo;

            $('#addFucionario').modal('show');
        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        buscar: function () {
            var app = this;

            if (app.busqueda != "") {

                var app = this;
                $.post('./request/getAno.php', {
                    opcion: '2',
                    buscar: app.busqueda.toUpperCase()
                }, function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                    app.busqueda = "";
                });


            } else {

                app.loadAnio()

            }

        },

        actualizar: function () {

            if (this.valorActividad == "" ||
                this.codigo_modal == "" ||
                this.dias_modal == "" ||
                this.nombre_modal == "") {

                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        modulo: app.valorActividad,
                        codigo: app.codigo_modal,
                        desc: app.nombre_modal,
                        dias: app.dias_modal,
                    },

                }).done(function (data) {


                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.resetModal();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {
                    console.log(data)
                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.desc + " ?",
                function () {
                    $.post('./request/deleteMes.php', {
                        codigo: dato.codigo,
                        modulo: dato.modulo,
                    }, function (data) {

                        console.log(data)
                        app.valorActividad = "";

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {


                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {

            console.log($(".selectActividad").val())

            if (this.codigo == "" ||
                this.nombre == "" ||
                this.dias == "" ||
                $(".selectActividad").val() == null) {

                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: this.codigo,
                        nombre: this.nombre,
                        dias: this.dias,
                        codigo_clas: $(".selectActividad").val(),
                    },

                }).done(function (data) {

                    console.log(data)


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadAnio();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {
                    console.log(data)
                });


            }
        },

        resetCampos: function () {

            app = this;

            app.codigo = "";
            app.nombre = "";
            app.valorActividad = "";
            app.dias = "";
            $("#selectActividad").val("").trigger('change')

        }
    },


    watch: {},

    mounted() {

        this.loadAnio();
        this.loadClaseActividades();


    },

});
