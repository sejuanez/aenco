<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 10/10/2018
     * Time: 12.25
     */
    /*
    session_start();
    if (!$_SESSION['user']) {
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }
    */

    include("../../../init/gestion.php");


    $sql = "SELECT  UM_CODIGO CODIGO, UM_NOMBRE NOMBRE, UM_DECIMALES DECIMALES
			from UNIDAD_MEDIDA 
                                ";

    $result = ibase_query($conexion, $sql);

    $return_arr = array();


    while ($fila = ibase_fetch_row($result)) {

        $row_array['CODIGO'] = utf8_encode($fila[0]);
        $row_array['NOMBRE'] = utf8_encode($fila[1]);
        $row_array['DECIMAL'] = utf8_encode($fila[2]);
        $row_array['CANTIDAD'] = count($return_arr);
        array_push($return_arr, $row_array);
    }


    echo json_encode($return_arr);
