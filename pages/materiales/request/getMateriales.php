<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 09/10/2018
     * Time: 3.31
     */

    /*
     session_start();

     if (!$_SESSION['user']) {
         echo
         "<script>
             window.location.href='../index.php';
         </script>";
         exit();
     }
    */
    include("../../../init/gestion.php");

    $opcion = $_GET['opcion'];
    $codigo = $_GET['codigo'];

    if ($opcion == "todos") {

        $sql = "    SELECT
                    MA_CODIGO, 
                    MA_GRUPO, 
                    MA_DESCRIPCION, 
                    MA_UNIDAD, 
                    MA_VR_UNITARIO, 
                    MA_STOCK_MIN, 
                    MA_EXISTENCIA, 
                    MA_USUARIO_SIS, 
                    MA_VR_VENTA, 
                    MA_VR_VENTA_USU, 
                    MA_PORCENTAJE, 
                    MA_SERVICIO, 
                    MA_LEGAL, 
                    MA_ACTIVO, 
                    cast (MA_OBSERVACION as varchar(200) ), 
                    MA_DESCRIP_COMERC, 
                    MA_FECHA_GRABA, 
                    MA_HORA_GRABA, 
                    MA_USU_MODIFICA, 
                    MA_FECHA_MODIFICA, 
                    MA_HORA_MODIFICA, 
                    MA_USU_SOLICIT,                      
                    cast (MA_JUSTIFIC as varchar(200) ),
                    MA_CODEQUI
 
  
                  
                                                    
                    from MATERIALES 
                    
                    
                    
                    ";

        $return_arr = array();

        $result = ibase_query($conexion, $sql);

        while ($fila = ibase_fetch_row($result)) {
            $row_array['codigo'] = utf8_encode($fila[0]);
            $row_array['grupo'] = utf8_encode($fila[1]);
            $row_array['descripcion'] = utf8_encode($fila[2]);
            $row_array['unidad'] = utf8_encode($fila[3]);
            $row_array['valorUnitario'] = utf8_encode($fila[4]);
            $row_array['stockMinimo'] = utf8_encode($fila[5]);
            $row_array['existencias'] = utf8_encode($fila[6]);
            $row_array['usuarioSistema'] = utf8_encode($fila[7]);
            $row_array['valorVenta'] = utf8_encode($fila[8]);
            $row_array['valorVentaUsu'] = utf8_encode($fila[9]);
            $row_array['iva'] = utf8_encode($fila[10]);
            $row_array['servicio'] = utf8_encode($fila[11]);
            $row_array['legaliza'] = utf8_encode($fila[12]);
            $row_array['activo'] = utf8_encode($fila[13]);
            $row_array['observacion'] = utf8_encode($fila[14]);
            $row_array['descripcionComercial'] = utf8_encode($fila[15]);
            $row_array['fechaRegistro'] = utf8_encode($fila[16]);
            $row_array['horaRegistro'] = utf8_encode($fila[17]);
            $row_array['usuModifica'] = utf8_encode($fila[18]);
            $row_array['fechaModifica'] = utf8_encode($fila[19]);
            $row_array['horaModifica'] = utf8_encode($fila[20]);
            $row_array['usuSolicita'] = utf8_encode($fila[21]);
            $row_array['justificacion'] = utf8_encode($fila[22]);
            $row_array['codEqui'] = utf8_encode($fila[23]);

            array_push($return_arr, $row_array);
        }
        echo json_encode($return_arr);
    } else {


        $sql = "    SELECT
                     
                    MA_GRUPO, 
                    MA_DESCRIPCION, 
                    MA_UNIDAD, 
                    MA_VR_UNITARIO, 
                    MA_STOCK_MIN, 
                    MA_EXISTENCIA, 
                    MA_USUARIO_SIS, 
                    MA_VR_VENTA, 
                    MA_VR_VENTA_USU, 
                    MA_PORCENTAJE, 
                    MA_SERVICIO, 
                    MA_LEGAL, 
                    MA_ACTIVO, 
                    cast (MA_OBSERVACION as varchar(200) ), 
                    MA_DESCRIP_COMERC, 
                    MA_FECHA_GRABA, 
                    MA_HORA_GRABA, 
                    MA_USU_MODIFICA, 
                    MA_FECHA_MODIFICA, 
                    MA_HORA_MODIFICA, 
                    MA_USU_SOLICIT, 
                    cast (MA_JUSTIFIC as varchar(200) ), 
                    MA_CODEQUI
 
  
                  
                                                    
                    from MATERIALES 
                    
                    where MA_CODIGO = '" . $codigo . "'
                    
                    ";

        $return_arr = array();

        $result = ibase_query($conexion, $sql);

        while ($fila = ibase_fetch_row($result)) {

            $row_array['grupo'] = utf8_encode($fila[0]);
            $row_array['descripcion'] = utf8_encode($fila[1]);
            $row_array['unidad'] = utf8_encode($fila[2]);
            $row_array['valorUnitario'] = utf8_encode($fila[3]);
            $row_array['stockMinimo'] = utf8_encode($fila[4]);
            $row_array['existencias'] = utf8_encode($fila[5]);
            $row_array['usuarioSistema'] = utf8_encode($fila[6]);
            $row_array['valorVenta'] = utf8_encode($fila[7]);
            $row_array['valorVentaUsu'] = utf8_encode($fila[8]);
            $row_array['iva'] = utf8_encode($fila[9]);
            $row_array['servicio'] = utf8_encode($fila[10]);
            $row_array['legaliza'] = utf8_encode($fila[11]);
            $row_array['activo'] = utf8_encode($fila[12]);
            $row_array['observacion'] = utf8_encode($fila[13]);
            $row_array['descripcionComercial'] = utf8_encode($fila[14]);
            $row_array['fechaRegistro'] = utf8_encode($fila[15]);
            $row_array['horaRegistro'] = utf8_encode($fila[16]);
            $row_array['usuModifica'] = utf8_encode($fila[17]);
            $row_array['fechaModifica'] = utf8_encode($fila[18]);
            $row_array['horaModifica'] = utf8_encode($fila[19]);
            $row_array['usuSolicita'] = utf8_encode($fila[20]);
            $row_array['justificacion'] = utf8_encode($fila[21]);
            $row_array['codEqui'] = utf8_encode($fila[22]);

            array_push($return_arr, $row_array);
        }
        echo json_encode($return_arr);

    }

