//=================================================
//=================================================

jQuery(document).ready(function ($) {

    $(".selectMaterial").select2().change(function (e) {
        var codMaterial = $(this).val();
        var selectedIndex = document.getElementById("selectMaterial").selectedIndex
        app.onChangeMaterial(codMaterial, selectedIndex);
    });

    $(".selectUnidadMedida").select2().change(function (e) {
        var codMedida = $(this).val();
        var selectedIndex = document.getElementById("selectUnidadMedida").selectedIndex
        app.onChangeMedida(selectedIndex, codMedida);
    });


    $(".selectGrupo").select2().change(function (e) {
        var codGrupo = $(this).val();
        var selectedIndex = document.getElementById("selectGrupo").selectedIndex
        app.onChangeGrupo(selectedIndex, codGrupo);
    });

    $(".selectServicio").select2().change(function (e) {
        var codServicio = $(this).val();
        app.onChangeServicio(codServicio);
    });

    $(".selectSolicitante").select2().change(function (e) {
        var codSoli = $(this).val();
        app.onChangeSolicitante(codSoli);
    });

});

//=================================================
//=================================================


let app = new Vue({
    el: '#app',
    data: {

        materiales: [],

        codigo: "",
        codigoEquivalente: "",
        descripcion: "",
        selectMateriales: [],
        selectedMaterial: "",
        legaliza: false,
        activo: false,

        selectUnidadMedidas: [],
        unidadMedida: "",
        medidaDecimales: "",
        selectGrupos: [],
        grupo: "",
        requiereSerie: "",

        stockMinimo: "",
        existencias: "",
        valorCompra: "",
        valorVenta: "",

        observacion: "",
        descripcionComercial: "",

        valorUnitario: "",
        iva: "",
        selectServicios: [],
        servicio: "",

        fechaCreacion: "",
        fechaModificacion: "",
        selectSolicitantes: [],
        solicitante: "",

        usuarioRegistra: "",
        usuarioModifica: "",
        justificacion: "",


    },

    methods: {

        onChangeServicio: function (cod) {

            var app = this;
            app.servicio = cod

        },

        onChangeSolicitante: function (cod) {

            var app = this;
            app.solicitante = cod

        },

        loadFecha: function () {

            var today = new Date();
            this.fechaCreacion = (getFormattedDate(today));
            this.fechaModificacion = (getFormattedDate(today));


            // Get date formatted as YYYY-MM-DD
            function getFormattedDate(date) {
                return date.getFullYear() +
                    "-" +
                    ("0" + (date.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("0" + date.getDate()).slice(-2);
            }

        },

        loadMateriales: function (codigoMaterial) {
            var app = this;

            if (codigoMaterial != null) {

                $.get('./request/getMateriales.php?opcion=1&codigo=' + codigoMaterial + "", function (data) {

                    if (data.length <= 2) {

                        alertify.warning("No existe un material con este codigo")


                    } else {

                        var data = JSON.parse(data)


                        if (data[0].legaliza) {
                            app.legaliza = true
                        }

                        if (data[0].activo) {
                            app.activo = true
                        }

                        app.descripcion = data[0].descripcion;


                        $('#selectUnidadMedida').select2().val(data[0].unidad).trigger('change')
                        $('#selectGrupo').select2().val(data[0].grupo).trigger('change')

                        $('#selectSolicitante').select2().val(data[0].usuarioSistema).trigger('change')

                        $('#selectServicio').select2().val(data[0].servicio).trigger('change')

                        app.codigoEquivalente = data[0].codEqui;
                        app.stockMinimo = data[0].stockMinimo;
                        app.existencias = data[0].existencias;
                        app.valorCompra = data[0].valorVentaUsu;
                        app.valorVenta = data[0].valorVenta;

                        app.observacion = data[0].observacion;
                        app.descripcionComercial = data[0].descripcionComercial;

                        app.valorUnitario = data[0].valorUnitario;
                        app.iva = data[0].iva;
                        app.servicio = data[0].servicio;

                        app.fechaCreacion = data[0].fechaRegistro;
                        app.fechaModificacion = data[0].fechaModifica;
                        app.solicitante = data[0].usuSolicita;

                        app.usuarioRegistra = data[0].usuarioSistema;
                        app.usuarioModifica = data[0].usuModifica;
                        app.justificacion = data[0].justificacion;

                    }
                });

            } else {

                $.get('./request/getMateriales.php?opcion=todos&codigo=1', function (data) {
                    var data = JSON.parse(data);
                    app.materiales = data;
                });
            }
        },

        onChangeMaterial: function (cod, index) {

            var app = this;

            this.codigo = cod;

            if (index == 0) {

                this.selectedMaterial = "";

            } else {

                app.codigo = app.materiales[index - 1].codigo;
                app.codigoEquivalente = app.materiales[index - 1].codEqui;

                if (app.materiales[index - 1].legaliza == 'S') {
                    app.legaliza = true
                } else {
                    app.legaliza = false
                }

                if (app.materiales[index - 1].activo == 'S') {
                    app.activo = true
                } else {
                    app.activo = false
                }

                app.descripcion = app.materiales[index - 1].descripcion;

                $('#selectUnidadMedida').select2().val(app.materiales[index - 1].unidad).trigger('change')
                $('#selectGrupo').select2().val(app.materiales[index - 1].grupo).trigger('change')

                $('#selectSolicitante').select2().val(app.materiales[index - 1].usuSolicita).trigger('change')

                $('#selectServicio').select2().val(app.materiales[index - 1].servicio).trigger('change')

                app.stockMinimo = app.materiales[index - 1].stockMinimo;
                app.existencias = app.materiales[index - 1].existencias;
                app.valorCompra = app.materiales[index - 1].valorVentaUsu;
                app.valorVenta = app.materiales[index - 1].valorVenta;

                app.observacion = app.materiales[index - 1].observacion;
                app.descripcionComercial = app.materiales[index - 1].descripcionComercial;

                app.valorUnitario = app.materiales[index - 1].valorUnitario;
                app.iva = app.materiales[index - 1].iva;
                app.servicio = app.materiales[index - 1].servicio;

                app.fechaCreacion = app.materiales[index - 1].fechaRegistro;
                app.fechaModificacion = app.materiales[index - 1].fechaModifica;
                app.solicitante = app.materiales[index - 1].usuSolicita;

                app.usuarioRegistra = app.materiales[index - 1].usuarioSistema;
                app.usuarioModifica = app.materiales[index - 1].usuModifica;
                app.justificacion = app.materiales[index - 1].justificacion;


            }

        },

        loadUnidadMedida: function () {
            var app = this;
            $.get('./request/getUnidadMedida.php', function (data) {


                app.selectUnidadMedidas = JSON.parse(data);

            });
        },

        onChangeMedida: function (index, codMedida) {

            var app = this;

            app.unidadMedida = codMedida;

            try {

                app.medidaDecimales = app.selectUnidadMedidas[index - 1].DECIMAL;

            } catch (e) {

            }


        },

        loadGrupos: function () {
            var app = this;
            $.get('./request/getGrupos.php', function (data) {

                app.selectGrupos = JSON.parse(data);
                console.log(app.selectGrupos)

            });
        },

        onChangeGrupo: function (index, codGrupo) {

            var app = this;

            app.grupo = codGrupo;
            try {
                app.requiereSerie = app.selectGrupos[index - 1].SERIE;
            } catch (e) {
                app.requiereSerie = '';
            }

        },

        loadServicios: function () {
            var app = this;
            $.get('./request/getServicios.php', function (data) {

                app.selectServicios = JSON.parse(data);

            });
        },

        loadSolicitantes: function () {
            var app = this;
            $.get('./request/getSolicitante.php', function (data) {

                app.selectSolicitantes = JSON.parse(data);

            });
        },

        loadUsuarioSistema: function () {
            var app = this;
            $.get('./request/getUsuarioSistema.php', function (data) {

                console.log(data)
                app.usuarioRegistra = data

            });
        },

        validarInput: function () {

            var app = this;


            if (app.medidaDecimales == "") {
                alertify.warning("Por favor seleccione primero una Unidad de medida")
                app.stockMinimo = "";
            } else {

                if (app.medidaDecimales == "S") {

                    if (parseFloat(app.stockMinimo, 10)) {
                        console.log("Valor Correcto: " + parseFloat(app.stockMinimo))
                    } else {
                        alertify.warning("Por favor Ingrese un valor valido")
                        app.stockMinimo = "";
                    }

                } else {


                    var num = app.stockMinimo.toString()

                    if (!(num.includes("."))) {
                        console.log("Valor Correcto: " + parseFloat(app.stockMinimo))
                    } else {
                        alertify.warning("Por favor ingrese un valor entero, Esta medida no puede tener decimales")
                        app.stockMinimo = "";
                    }


                }

            }
        },

        guardar: function () {

            var app = this;

            if (app.codigo == "" ||
                app.medidaDecimales == "" ||
                app.requiereSerie == "" ||
                app.stockMinimo == "" ||
                app.valorCompra == "" ||
                app.valorVenta == "" ||
                app.observacion == "" ||
                app.descripcionComercial == "" ||
                app.valorUnitario == "" ||
                app.iva == "" ||
                app.servicio == "" ||
                app.solicitante == "" ||
                app.justificacion == ""

            ) {


                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;

                var activoInsert
                var legalizaInsert

                if (app.legaliza) {
                    legalizaInsert = "S"
                } else {
                    legalizaInsert = "N"
                }

                if (app.activo) {
                    activoInsert = "S"
                } else {
                    activoInsert = "N"
                }

                app.loadFecha();


                var formData = new FormData($('.formGuardar')[0]);
                formData.append("legalizaInsert", legalizaInsert)
                formData.append("activoInsert", activoInsert)
                formData.append("codMedida", app.unidadMedida)
                formData.append("codGrupo", app.grupo)
                formData.append("codServ", app.servicio)
                formData.append("codSoli", app.solicitante)
                formData.append("usuRegistra", app.usuarioRegistra)
                formData.append("fechaCrea", app.fechaCreacion)
                formData.append("fechaModi", app.fechaModificacion)
                formData.append("usuModi", app.usuarioModifica)


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertMaterial.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log(data)

                        if (data.indexOf("UNIQUE KEY constraint") > -1) {

                            alertify.warning("Existe un material con este codigo");


                        }


                        //app.search();


                        if (data == 1) {

                            alertify.success("Registro guardado Exitosamente");
                            app.nuevo();
                            app.loadMateriales()

                        }

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });


            }

        },

        actualizar: function () {

            var app = this;

            if (app.codigo == "" ||
                app.medidaDecimales == "" ||
                app.requiereSerie == "" ||
                app.stockMinimo == "" ||
                app.valorCompra == "" ||
                app.valorVenta == "" ||
                app.observacion == "" ||
                app.descripcionComercial == "" ||
                app.valorUnitario == "" ||
                app.iva == "" ||
                app.servicio == "" ||
                app.solicitante == "" ||
                app.justificacion == ""

            ) {


                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;

                var activoInsert
                var legalizaInsert

                if (app.legaliza) {
                    legalizaInsert = "S"
                } else {
                    legalizaInsert = "N"
                }

                if (app.activo) {
                    activoInsert = "S"
                } else {
                    activoInsert = "N"
                }

                console.log(legalizaInsert)
                console.log(activoInsert)

                app.loadFecha();


                var formData = new FormData($('.formGuardar')[0]);
                formData.append("legalizaInsert", legalizaInsert)
                formData.append("activoInsert", activoInsert)
                formData.append("codMedida", app.unidadMedida)
                formData.append("codGrupo", app.grupo)
                formData.append("codServ", app.servicio)
                formData.append("codSoli", app.solicitante)
                formData.append("usuRegistra", app.usuarioRegistra)
                formData.append("fechaCrea", app.fechaCreacion)
                formData.append("fechaModi", app.fechaModificacion)
                formData.append("usuModi", app.usuarioModifica)


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateMaterial.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {

                        //app.search();

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");
                            app.nuevo();
                            app.loadMateriales()

                        }

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });


            }


        },

        nuevo: function () {

            var app = this;

            $('.formGuardar').removeClass('was-validated');

            app.codigo = "";
            app.legaliza = true

            app.activo = true

            app.descripcion = "";

            $('#selectUnidadMedida').select2().val("").trigger('change')
            app.medidaDecimales = "";

            $('#selectGrupo').select2().val("").trigger('change')
            app.requiereSerie = "";

            $('#selectSolicitante').select2().val("").trigger('change')

            $('#selectServicio').select2().val("").trigger('change')
            $('#selectMaterial').select2().val("").trigger('change')


            app.stockMinimo = "";
            app.existencias = "";
            app.valorCompra = "";
            app.valorVenta = "";
            app.codigoEquivalente = "";

            app.observacion = "";
            app.descripcionComercial = "";

            app.valorUnitario = "";
            app.iva = "";
            app.servicio = "";

            app.fechaCreacion = "";
            app.fechaModificacion = "";
            app.solicitante = "";

            app.usuarioRegistra = "";
            app.usuarioModifica = "";
            app.justificacion = "";

            app.loadFecha()
            app.loadUsuarioSistema()


        },

        exportar: function () {
            if (app.materiales.length > 0) {


                var elt = document.getElementById('datos');
                var wb = XLSX.utils.table_to_book(elt, {
                    sheet: "Materiales"
                });
                return XLSX.writeFile(wb, 'lista_materiales.xlsx');


            } else {
                alertify.error('No hay registros que exportar');
            }

        },

        eliminar: function () {


            var app = this;

            if (app.codigo == "") {

                alertify.warning("No hay ningun material seleccionado")

            } else {

                alertify.confirm("Eliminar", ".. Desea eliminar este Material?",
                    function () {
                        $.post('./request/deleteMaterial.php', {material: app.codigo}, function (data) {
                            // console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");

                                app.nuevo()
                                app.loadMateriales()
                                app.loadFecha()
                                app.loadUsuarioSistema()

                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            }
        },


    },


    watch:
        {},
    mounted() {

        this.loadMateriales();
        this.loadUnidadMedida();
        this.loadGrupos();
        this.loadServicios();
        this.loadSolicitantes();
        this.loadUsuarioSistema();
        this.loadFecha();

    },

});
