<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador


        echo
        "<script>
            window.location.href='../inicio/index.php';
            
        </script>";
        exit();
    } else {


    }
?>

<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>

    <style type="text/css">

        textarea {
            resize: none
        }

        input[type="checkbox"] {
            vertical-align: middle;
        }

        .vertical-center {
            min-height: 100%; /* Fallback for browsers do NOT support vh unit */
            min-height: 100%; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }

        #grafico-preventas, #grafico-clientesImpactados {
            min-width: 73%;
            height: 330px;
        }

        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        table {
            font-size: .8em;
        }


    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Materiales

            <span id="btnExportar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="exportar()">
            	<i class="fa fa-download" aria-hidden="true"></i> Exportar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="eliminar()">
            	<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
            </span>


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="nuevo()">
            	<i class="fa fa-edit" aria-hidden="true"></i> Nuevo
            </span>

            <span id="btnActualizar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="actualizar()">
            	<i class="fa fa-pencil" aria-hidden="true"></i> Actualizar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="guardar()">
            	<i class="fa fa-save" aria-hidden="true"></i> Guardar
            </span>


        </p>
    </header>


    <div class="container">

        <form class="formGuardar">

            <div class="row align-items-center">

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="codigo">Codigo</label>
                        <input type="text" id="codigo" name="codigo" class="form-control" required
                               aria-label="codigo" v-model="codigo" maxlength="40"
                               v-on:keyup.13="loadMateriales(codigo)"
                               @blur="loadMateriales(codigo)">
                    </div>
                </div>


                <div class="col-9">
                    <div class="form-group input-group-sm">
                        <label for="selectMaterial">Materiales Registrados</label>

                        <select class="selectMaterial" id="selectMaterial" name="selectMaterial" required
                                v-model="selectedMaterial">
                            <option value="">Seleccione...</option>
                            <option v-for="selectMaterial in materiales" :value="selectMaterial.codigo">
                                {{selectMaterial.codigo}} - {{selectMaterial.descripcion}}
                            </option>
                        </select>

                    </div>
                </div>


            </div>

            <div class="row align-items-center">

                <div class="col-5">
                    <div class="form-group input-group-sm">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" id="descripcion" name="descripcion" class="form-control" required
                               aria-label="descripcion" v-model="descripcion"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="codigoEquivalente">Codigo Equivalente</label>
                        <input type="text" id="codigoEquivalente" name="codigoEquivalente" class="form-control" required
                               aria-label="codigoEquivalente" v-model="codigoEquivalente" maxlength="40">
                    </div>
                </div>


                <div class="col-1 ">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value=""
                                   id="legaliza"
                                   name="legaliza"
                                   v-model="legaliza">
                            Legaliza
                        </label>
                    </div>
                </div>

                <div class="col-1 ">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value=""
                                   id="activo"
                                   name="activo"
                                   v-model="activo"
                            >
                            Activo
                        </label>
                    </div>
                </div>

            </div>


            <div class="row align-items-center">


                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectUnidadMedida">Unidad de medida </label>

                        <select class="selectUnidadMedida" id="selectUnidadMedida" name="selectUnidadMedida" required
                                v-model="unidadMedida">
                            <option value="">Seleccione...</option>
                            <option v-for="selectUnidadMedida in selectUnidadMedidas"
                                    :value="selectUnidadMedida.CODIGO">
                                {{selectUnidadMedida.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group input-group-sm">
                        <label for="medidaDecimales">Decimales</label>
                        <input type="text" id="medidaDecimales" name="medidaDecimales" class="form-control" disabled
                               aria-label="medidaDecimales" v-model="medidaDecimales" maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectGrupo">Grupo </label>

                        <select class="selectGrupo" id="selectGrupo" name="selectGrupo" required
                                v-model="grupo">
                            <option value="">Seleccione...</option>
                            <option v-for="selectGrupo in selectGrupos" :value="selectGrupo.CODIGO">
                                {{selectGrupo.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group input-group-sm">
                        <label for="requiereSerie">Requiere Serie</label>
                        <input type="text" id="requiereSerie" name="requiereSerie" class="form-control" disabled
                               aria-label="requiereSerie" v-model="requiereSerie" maxlength="40">
                    </div>
                </div>

            </div>


            <div class="row align-items-center">

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="stockMinimo">Stock Minimo</label>
                        <input type="text" id="stockMinimo" name="stockMinimo" class="form-control"

                               oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                    this.value = this.value.replace(/(\..*)\./g, '$1');"

                               @blur="validarInput()"
                               required
                               aria-label="stockMinimo"
                               v-model="stockMinimo" maxlength="40">
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="existencias">Existencias</label>
                        <input type="text" id="existencias" name="existencias" class="form-control" disabled
                               aria-label="existencias" v-model="existencias" maxlength="40">
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="valorCompra">Valor Compra</label>
                        <input type="text" id="valorCompra" name="valorCompra" class="form-control" required
                               aria-label="valorCompra" v-model="valorCompra" maxlength="40"
                               oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                        this.value = this.value.replace(/(\..*)\./g, '$1');">
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="valorVenta">Valor Venta</label>
                        <input type="text" id="valorVenta" name="valorVenta" class="form-control" required
                               aria-label="valorVenta" v-model="valorVenta" maxlength="40"
                               oninput="this.value = this.value.replace(/[^0-9.]/g, '');
                                        this.value = this.value.replace(/(\..*)\./g, '$1');">
                    </div>
                </div>

            </div>

            <div class="row align-items-center">

                <div class="col-6">
                    <div class="form-group input-group-sm">
                        <label for="observacion">Observacion </label>
                        <textarea class="form-control" rows="1" name="observacion"
                                  id="observacion"
                                  v-model="observacion">

                                     </textarea>
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group input-group-sm">
                        <label for="descripcionComercial">Descripcion Comercial </label>
                        <textarea class="form-control" rows="1" name="descripcionComercial"
                                  id="descripcionComercial"
                                  v-model="descripcionComercial">

                                     </textarea>
                    </div>
                </div>

            </div>

            <div class="row align-items-center">

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="valorUnitario">Valor Unitario</label>
                        <input type="text" id="valorUnitario" name="valorUnitario" class="form-control" required
                               aria-label="valorUnitario" v-model="valorUnitario" maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="iva">% IVA</label>
                        <input type="text" id="iva" name="iva" class="form-control" required
                               aria-label="iva" v-model="iva" maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectTipoGasto">Servicio</label>

                        <select class="selectServicio" id="selectServicio" name="selectServicio" required
                                v-model="servicio">
                            <option value="">Seleccione...</option>
                            <option v-for="selectServicio in selectServicios" :value="selectServicio.CODIGO">
                                {{selectServicio.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>

            </div>
            <hr>

            <div class="row align-items-center">

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="fechaCreacion">Fecha Creacion </label>
                        <input type="date" id="fechaCreacion" name="fechaCreacion" class="form-control" disabled
                               aria-label="fechaCreacion"
                               v-model="fechaCreacion"
                               :value="fechaCreacion">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="fechaModificacion">Fecha Modificacion</label>
                        <input type="date" id="fechaModificacion" name="fechaModificacion" class="form-control" disabled
                               aria-label="fechaModificacion"
                               v-model="fechaModificacion"
                               :value="fechaModificacion">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectSolicitante">Solicitante</label>

                        <select class="selectSolicitante" id="selectSolicitante" name="selectSolicitante" required
                                v-model="solicitante">
                            <option value="">Seleccione...</option>
                            <option v-for="selectSolicitante in selectSolicitantes" :value="selectSolicitante.CODIGO">
                                {{selectSolicitante.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>

            </div>

            <div class="row align-items-center">

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="usuarioRegistra">Usuario Ingresa</label>
                        <input type="text" id="usuarioRegistra" name="usuarioRegistra" class="form-control" disabled
                               aria-label="usuarioRegistra"
                               v-model="usuarioRegistra"
                               :value="usuarioRegistra"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="usuarioModifica">Usuario Modifica</label>
                        <input type="text" id="usuarioModifica" name="usuarioModifica" class="form-control" disabled
                               aria-label="usuarioModifica" v-model="usuarioModifica" maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="justificacion">Justificacion </label>
                        <textarea class="form-control" rows="2" name="justificacion"
                                  id="justificacion"
                                  v-model="justificacion">

                                     </textarea>
                    </div>
                </div>


            </div>
        </form>

    </div>
    <br>

    <div id="datos" style="display: none">
        <div class="row">

            <div class="col-12">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <td>Codigo</td>
                        <td>Descripcion</td>
                        <td>Unidad</td>
                        <td>Stock Minimo</td>
                        <td>Valor</td>
                        <td>Existencias</td>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos" style="font-size: .8em">

                    <tr v-for="(dato, index) in materiales">
                        <td v-text="dato.codigo"></td>
                        <td v-text="dato.descripcion"></td>
                        <td v-text="dato.unidad"></td>
                        <td v-text="dato.stockMinimo"></td>
                        <td v-text="dato.valorUnitario"></td>
                        <td v-text="dato.existencias"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script lang="javascript" src="js/xlsx.full.min.js"></script>
<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>
</html>
