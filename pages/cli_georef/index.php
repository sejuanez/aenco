<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>

<!DOCTYPE html5>

<html>

<head>

    <meta charset="utf-8">

    <title>Ubicacion de clientes</title>


    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

    <link rel='stylesheet' href='css/tablet.css' type='text/css'
          media='only screen and (min-width:481px) and (max-width:831px)'/>

    <!--<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>


    <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

    <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>-->
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
    <script type="text/javascript" src="../../js/gmaps.js"></script>

    <script type="text/javascript">


        var hoy = new Date();
        var dias = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];
        var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
        var dia = hoy.getDate();
        var mes = hoy.getMonth() + 1;
        var anio = hoy.getFullYear();


        var nodoActivo = null;
        var map;
        var limits;


        $(function () {


            function actualizar() {

                //ajax getClientes
                $.ajax({

                    url: 'request/getClientes.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {codVendedor: $("#vendedores").val()}

                }).done(function (repuesta) {

                    //(map.markers[index].infoWindow).open(map.map,map.markers[index]);
                    //nodoActivo=null;

                    map.removeMarkers();
                    if (repuesta.length > 0) {// si hay clientes para mostrar

                        limits = new google.maps.LatLngBounds();
                        map.setCenter(repuesta[0].lat, repuesta[0].lon);
                        map.setZoom(16);

                        var filas = "";

                        for (var i = 0; i < repuesta.length; i++) {
                            //map.addMarker({lat:repuesta[i].lat, lng:repuesta[i].lon, icon:'../imagenes/'+repuesta[i].icon+'.png', title:repuesta[i].nombre, infoWindow:{content:"<h5 id='title-foto'>"+repuesta[i].nombre+"</h5><img src='request/foto.php?ced="+repuesta[i].cedula+"' />"} });
                            map.addMarker({
                                lat: repuesta[i].lat,
                                lng: repuesta[i].lon,
                                icon: 'img/' + repuesta[i].codVendedor + '.png',
                                title: repuesta[i].raz,
                                infoWindow: {content: "<h3 id='title-foto'>" + repuesta[i].raz + "</h3><h5 class='h5-infowin'>" + repuesta[i].cliente + "</h5><h5 class='h5-infowin'>" + repuesta[i].direccion + "</h5><h5 class='h5-infowin'>" + repuesta[i].tel + "</h5><h5 class='h5-infowin'>Vendedor: " + repuesta[i].nombreVendedor + "</h5>"}
                            });

                            limits.extend(new google.maps.LatLng(repuesta[i].lat, repuesta[i].lon));

                            filas += "<tr class='fila'>" +
                                "<td>" + repuesta[i].nit + "</td><td>" + repuesta[i].lon + "</td>" + "<td>" + repuesta[i].lat + "</td><td>" + repuesta[i].raz + "</td><td>" + repuesta[i].direccion + "</td><td>" + repuesta[i].tel + "</td><td>" + repuesta[i].cliente + "</td><td>" + repuesta[i].codVendedor + "</td><td>" + repuesta[i].nombreVendedor + "</td></tr>";

                        }
                        map.fitBounds(limits);

                        $("#tabla-general tbody").html(filas);

                        $("#exportar a").attr("href", "request/exportarExcel.php?codVendedor=" + $("#vendedores").val());
                        $("#exportar").show();


                        $("#consultando").hide();
                        $("#consultar").show();
                    }//fin si hay tecnicos
                    else {
                        $("#tabla-general tbody").html("");
                        $("#exportar a").attr("href", "");
                        $("#exportar").hide();
                        $("#consultando").hide();
                        $("#consultar").show();
                        $("#alert").dialog("open");

                    }


                });//fin del ajax Clientes

            };


            function iniciarComponentes() {

                $("#alert").dialog({
                    modal: true,
                    autoOpen: false,
                    resizable: false,
                    buttons: {
                        Ok: function () {
                            //$("#alert p").html("");
                            $(this).dialog("close");
                        }
                    }
                });

                $("#tabs").tabs({
                    show: {effect: "slide", duration: 200},
                    active: 1,
                    activate: function (event, ui) {
                        /*var ancho = +$("#div-grafico-tecnico").width();
                        var alto = +$("#div-grafico-tecnico").height();
                        $("#grafico-tecnico").highcharts().setSize(ancho,alto, false); */
                    }
                });

                google.maps.event.addDomListener(window, "resize", function () {

                    var center_lat = parseFloat(map.getCenter().lat()).toPrecision(4);
                    var center_lng = parseFloat(map.getCenter().lng()).toPrecision(5);
                    $("#container-map").css({width: '100%', height: "100%"});
                    $("#div-map").css({width: '100%', height: "100%"});
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center_lat, center_lng);

                });


                $('#vendedores').attr('disabled', true);

                $("#exportar").hide();
                $("#consultar").hide();
                $("#consultando").hide();

                //ajax que carga la lista de vendedoress
                $.ajax({
                    url: 'request/getVendedores.php',
                    type: 'POST',
                    dataType: 'json'

                }).done(function (repuesta) {

                    if (repuesta.length > 0) {
                        var options = "";

                        for (var i = 0; i < repuesta.length; i++) {
                            if (repuesta[i] != null)
                                options += "<option value='" + repuesta[i].codVendedor + "'>" + repuesta[i].nombreVendedor + "</option>";

                        }

                        $("#vendedores").html(options);
                        $('#vendedores').attr('disabled', false);

                        $("#consultar").show();
                    }


                });// fin del ajax vendedores


                //listener del evento clic del boton consultar
                $('#consultar').click(function (e) {

                    e.preventDefault();
                    $(this).hide();
                    //$("#contenido").hide();
                    $("#consultando").show();

                    actualizar();
                });


            };//fin de iniciarComponentes()

            //===================================================================================

            function cargarMapa() {
                //GMaps.geolocate({
                //success: function(position){
                //milat=position.coords.latitude;
                //milon=position.coords.longitude;
                map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                    el: '#div-map',
                    lat: 8.749349, /*position.coords.latitude,*/
                    lng: -75.879568,/*position.coords.longitude,*/
                    zoom: 10
                });


                //$("#header").html("<div id='div-title'><h3>Ubicación de funcionarios</h3><h5>"+dias[hoy.getDay()]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></div><nav><ul id='menu'><li id='actualizar'><a href='#'><span class='ion-ios-loop-strong'></span><h6>Actualizar</h6></a></li><li id='reset'><a href='#' ><span class='ion-ios-reload'></span><h6>Reset</h6></a></li></ul></nav>");
                //$("#modal").show();
                //limits = new google.maps.LatLngBounds();
                limits = new google.maps.LatLngBounds();
                iniciarComponentes();
                /*},
                error: function(error){
                   alert('Fallo en la geolocalizacion:'+error.message);
                   return false;
                },
                not_supported:function(){
                  alert('Geolocalizacion no soportada');
                  return false;
                }
            });*/
            };
            //======================================================================================


            cargarMapa();

            //$("body").show();


        });//fin del onready
    </script>

</head>

<!--<body style="display:none">-->
<body>

<div id="alert" title="Mensaje">
    <p>No hay nada para mostrar.</p>
</div>

<header>
    <h3>Ubicación de clientes</h3>
    <nav>
        <ul id="menu">

            <li id="exportar"><a href=""><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
            <li id="consultar"><a href=""><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
            <li id="consultando"><a href=""><span class="ion-load-d"></span><h6>consultando...</h6></a></li>

        </ul>
    </nav>
</header>


<div id='subheader'>
    <div id="div-form">
        <form id="form">
            <div><label>Vendedor</label><select id='vendedores'></select></div>
        </form>
    </div>
</div>


<div id="contenido">
    <div id='tabs'>

        <ul>
            <li><a href="#tab1">1. <span class='titulo-tab'>Datos</span></a></li>
            <li><a href="#tab2">2. <span class='titulo-tab'>Mapa</span></a></li>
        </ul>


        <div id='tab1'>
            <table id='tabla-general'>
                <thead>
                <tr class='cabecera'>
                    <td>Nit</td>
                    <td>Longitud</td>
                    <td>Latitud</td>
                    <td>Razon Social</td>
                    <td>Direccion</td>
                    <td>Telefono</td>
                    <td>Cliente</td>
                    <td>Cod. vendedor</td>
                    <td>Nombre Vendedor</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table><!--End end tabla-->
        </div><!--End tab1-->

        <div id='tab2'>
            <div id="container-map">
                <div id="div-map"></div>
            </div>
        </div><!--End tab2-->
    </div><!--End tabs-->
</div><!--End contenido-->

</body>
</html>