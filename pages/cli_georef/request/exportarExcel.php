<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=cli_georef_codVendedor_".$_GET['codVendedor'].".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");


	$codVendedor = $_GET['codVendedor'];  


  if ($codVendedor!='null') {
    $consulta = "SELECT c.cg_nit nit, c.cg_longitud longitud, c.cg_latitud latitud, cl.cl_razonsocial razon_social, cl.cl_direcci direccion, cl.cl_tel Telefono, cl.cl_noape nombre_cliente, cl.cl_vendedor cod_vendedor, v.ve_nombres nombre_vendedor from cli_georef c left join clientes cl on cl.cl_nit=c.cg_nit left join vendedor v on v.ve_cedula=cl.cl_vendedor where v.ve_cedula='".$codVendedor."'";
  }
  else{
    $consulta = "SELECT c.cg_nit nit, c.cg_longitud longitud, c.cg_latitud latitud, cl.cl_razonsocial razon_social, cl.cl_direcci direccion, cl.cl_tel Telefono, cl.cl_noape nombre_cliente, cl.cl_vendedor cod_vendedor, v.ve_nombres nombre_vendedor from cli_georef c left join clientes cl on cl.cl_nit=c.cg_nit left join vendedor v on v.ve_cedula=cl.cl_vendedor";
  }



	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'><td>Nit</td><td>Longitud</td><td>Latitud</td><td>Razon Social</td><td>Direccion</td><td>Telefono</td><td>Cliente</td><td>Cod. vendedor</td><td>Nombre Vendedor</td></tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					     "<td>".utf8_encode($fila[0])."</td><td>".utf8_encode($fila[1])."</td>"."<td>".utf8_encode($fila[2])."</td><td>".utf8_encode($fila[3])."</td><td>".utf8_encode($fila[4])."</td><td>".utf8_encode($fila[5])."</td><td>".utf8_encode($fila[6])."</td><td>".utf8_encode($fila[7])."</td><td>".utf8_encode($fila[8])."</td>".
				    "</tr>";		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>