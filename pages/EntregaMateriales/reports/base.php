<?php

include("../request/gestion.php");

require('fpdf.php');

class PDF extends FPDF
{

	function Header()
	{
	    // Select Arial bold 15
		global $title;
		global $city;

	    $this->AddFont('Lato-Regular','','Lato-Regular.php');
		$this->SetFont('Lato-Regular','',12);

	    // Ancho de pagina 190 en X
	    // $this->setDrawColor(80,80,80);
	    // $this->setFillColor(0,80,170);
		// $this->setTextColor(255,255,255);
	    $this->setTextColor(80,80,80);
		$this->Image('../img/logo.png',10,10,40,12,'PNG');
	    $this->Cell(40,30,$city,0,0,'C',false);

		$this->SetFont('Lato-Regular','',14);
	    $this->Cell(110,20,$title,0,0,'C',false);
	    $this->Cell(40,20,'',0,0,'C', false);
	    // Line break
	    $this->Ln(25);
	}

	function Footer()
	{
	    // Go to 1.5 cm from bottom
	    $this->SetY(-15);

	    $this->AddFont('LatoL','','Lato-Light.php');
		$this->SetFont('LatoL','',8);
	    // Print centered page number
	    $this->setFillColor(0,80,170);
	    $this->setTextColor(80,80,80);
	    $this->Cell(95,10,'Fecha y hora de impresion: '.date('d/m/Y  -H:i:s',time()-21600),'T',0,'L');
	    $this->Cell(95,10,utf8_decode('Página '.$this->PageNo()),'T',0,'R');
	}

	// Simple table
	function BasicTable($header, $data, $w=10)
	{	$this->AddFont('LatoL','','Lato-Light.php');
		$this->SetFont('LatoL','',9);
		$this->setDrawColor(255,255,255);
		$this->setFillColor(0,80,170);

		// Header
		$this->setTextColor(255,255,255);
		foreach($header as $col)
			$this->Cell($w,7,$col,1,0,'C',true);
		$this->Ln();

		// Data
		$this->setTextColor(80,80,80);
		$this->setDrawColor(200,200,200);
		$this->AddFont('Lato','','Lato-Regular.php');
		$this->SetFont('Lato','',9);
		foreach($data as $row)
		{
			foreach($row as $col)
			$this->Cell($w,6,$col,'B',0,'C');
			$this->Ln();
		}
	}

	function HeaderTable($header, $w=10)
	{	$this->AddFont('LatoL','','Lato-Light.php');
		$this->SetFont('LatoL','',9);
		$this->setDrawColor(255,255,255);
		$this->setFillColor(255,100,100);

		// Header
		foreach($header as $col)
			$this->Cell($w,7,$col,1,0,'C',true);
		$this->Ln();
	}

	function DataTable($data, $type, $w=10)
	{
		// Data
		$this->setDrawColor(200,200,200);
		$this->AddFont('Lato','','Lato-Regular.php');
		$this->SetFont('Lato','',9);
		foreach($data as $row)
		{
			$index=0;
			foreach($row as $col){
			if ($type[$index]=='money') {
			$this->Cell($w,6,'$ '.number_format($col),'B',0,'C');

			}else{
			$this->Cell($w,6,$col,'B',0,'C');
			}
			$index++;
			}
			$this->Ln();
		}
	}

}//en Class PDF


 ?>