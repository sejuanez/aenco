<?php
ini_set('max_execution_time', 300);
set_time_limit(3000);

include("gestion.php");


$data = $_POST['data'];
$data = json_decode($data);
// var_dump($data);
// var_dump($data->detalle[0]);
// var_dump($data->detalle[0]->arraySeries[0]->CODMATERIAL);


  $return_arr = array();
  $sql = 'SELECT GEN_ID(SAL, 1) NUMERO, CURRENT_DATE FECHA FROM RDB$DATABASE';
  $result = ibase_query($conexion, $sql);
  while($fila = ibase_fetch_row($result)){
    $row_array['NUMERO'] = utf8_encode($fila[0]);
    $date = new DateTime($fila[1]);
    $row_array['FECHA'] = $date->format('d.m.Y');
    array_push($return_arr, $row_array);
  }

try {
    $tr = ibase_trans();

    $sql8 = "INSERT INTO HIDESESU (HS_NDOC, HS_TIPOMOV, HS_GRUPO, HS_MARCA, HS_SERIE,
                  HS_CODIGO_MAT, HS_NOMBRE_MAT, HS_CODSUPER, HS_NOMBRE_SUPER,
                  HS_CODTEC, HS_NOMBRE_TEC, HS_CODCUADRILLA, HS_NOMBRE_CUA,
                  HS_CONCEPTO, HS_FECHADOC, HS_USUARIO_SISTEMA)
                  VALUES (?, 'ENT', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $sql7 = "INSERT INTO HIDESEBO (HB_NDOC, HB_TIPOMOV, HB_GRUPO, HB_MARCA, HB_SERIE,
                  HB_CODIGO_MAT, HB_NOMBRE_MAT, HB_CODPROV, HB_NOMBRE_PROV,
                  HB_CONCEPTO, HB_FECHADOC, HB_USUARIO_SISTEMA, HB_DOCEXT)
                  VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $sql6 = "UPDATE BODEGA_SERIES
                SET SUPER = ?, NOMBRE_SUPER = ?, DOCUMENTO_S = ?, CONCEPTO_S = ?, FECHA_S = ?, USUARIO_SISTEMA = ?
                WHERE (GRUPO = ?) AND (MARCA = ?) AND (SERIE = ?) AND (CODIGOMAT = ?)";

    $sql5 = "UPDATE ACTAS_SERIES
                SET AS_CODTECNICO = ?, AS_NOMTECNICO = ?, AS_DOCUMENTO_S = ?, AS_CONCEPTO_S = ?, AS_FECHA_S = ?, AS_USUARIO_S = ?
                WHERE (AS_GRUPO = ?) AND (AS_MARCA = ?) AND (AS_SERIE = ?) AND (AS_CODMATER = ?)";

    $sql4 = "UPDATE BODEGASUPER
              SET BO_CODIGOSUPER = ?, BO_NOMBRE_MAT = ?, BO_EXISTENCIA = BO_EXISTENCIA+?
              WHERE (BO_CODIGOMAT = ?) AND (BO_CODIGOTEC = ?)";

    $sql3 = "INSERT INTO BODEGASUPER (BO_CODIGOSUPER, BO_CODIGOMAT, BO_NOMBRE_MAT, BO_EXISTENCIA, BO_CODIGOTEC)
              VALUES ( ?, ?, ?, ?, ?)";

    $sql2 = "UPDATE MATERIALES
              SET MA_EXISTENCIA = MA_EXISTENCIA-?
              WHERE (MA_CODIGO = ?)";

    $sql1 = "INSERT INTO MOVIMIENTO_ALMACEN (SA_TIPO, SA_NUMERO, SA_FECHA, SA_CODIGO,
                  SA_NOMBRE, SA_CODIGO_MA, SA_NOMBRE_MA,
                  SA_CANTIDAD, SA_USUARIO, SA_DOCUMENTO,
                  SA_VALOR, SA_CUADRILLA, SA_NCUADRILLA,
                  SA_CONCEPTO, SA_CODTECNICO, SA_NOMTECNICO,
                  SA_CODSUPER, SA_NOMSUPER, SA_FECHA_INGRESO, SA_OC_NUMERO)
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $consulta1 = ibase_prepare($tr, $sql1);
    $consulta2 = ibase_prepare($tr, $sql2);
    $consulta3 = ibase_prepare($tr, $sql3);
    $consulta4 = ibase_prepare($tr, $sql4);
    $consulta5 = ibase_prepare($tr, $sql5);
    $consulta6 = ibase_prepare($tr, $sql6);
    $consulta7 = ibase_prepare($tr, $sql7);
    $consulta8 = ibase_prepare($tr, $sql8);

    $dateE = new DateTime($data->fechaEntrega);
    $fechaEntrega = $date->format('d.m.Y');

    $detalleLength= count($data->detalle);
    for ($i=0; $i < $detalleLength; $i++) {
      // var_dump($data->tecnicos);
      // die();
        // GUARDAMOS EL MOVIMIENTO EFECTUADO
        $result1 = ibase_execute($consulta1,
                              'SAL',
                              $row_array['NUMERO'],
                              $fechaEntrega,
                              '',
                              '',
                              $data->detalle[$i]->codigo,
                              $data->detalle[$i]->descripcion,
                              $data->detalle[$i]->cantidad,
                              $_SESSION['user'],
                              $data->numeroEntrega,
                              0,
                              $data->cuadrilla,
                              '',
                              substr($data->concepto, 0, 50),
                              $data->codTecnico,
                              substr($data->nomTecnico, 0, 30),
                              $data->codSupervisor,
                              $data->supervisor,
                              $row_array['FECHA'],
                              ''
                            );

        // ACTUALIZAMOS LA EXISTENCIA DE LA BODEGA PRINCIPAL
        $result2 = ibase_execute($consulta2,
                                  $data->detalle[$i]->cantidad,
                                  $data->detalle[$i]->codigo);

        // Verificamos la existencia del material en la bosega del usuario
        $sqla = "SELECT * FROM BODEGASUPER WHERE BO_CODIGOMAT='".$data->detalle[$i]->codigo."' AND BO_CODIGOTEC= '".$data->codTecnico."'";
        $query = ibase_query($conexion, $sqla);
        $existe = ibase_fetch_row($query);

        if($existe){
            // INSERTAMOS EN LA BODEGA DEL TECNICO SI EL ITEM NUNCA HA SIDO ENTREGADO (ES DECIR SI NO EXISTE EN LA TABLA)
            $result4 = ibase_execute($consulta4,
                                  $data->codSupervisor,
                                  $data->detalle[$i]->descripcion,
                                  $data->detalle[$i]->cantidad,
                                  $data->detalle[$i]->codigo,
                                  $data->codTecnico);
        }else{
          // ACTUALIZAMOS LA EXISTENCIA DEL TECNICO AL QUE SE LE ESTA ENTREGANDO SI TIENE ESE MATERIAL EN SU EXISTENCIA
            $result3 = ibase_execute($consulta3,
                                  $data->codSupervisor,
                                  $data->detalle[$i]->codigo,
                                  $data->detalle[$i]->descripcion,
                                  $data->detalle[$i]->cantidad,
                                  $data->codTecnico);
        }

        $hasSerie = count($data->detalle[$i]->arraySeries);

        if ($hasSerie > 0) {
            if( $data->detalle[$i]->cod_grupo==='99'){
                // Actualiza acta series, hidesebo, hidesesu
                for ($j = 0; $j < $hasSerie; $j++) {
                      //actualiza acta series
                      $result5 = ibase_execute($consulta5,
                                      $data->codTecnico,
                                      $data->nomTecnico,
                                      $row_array['NUMERO'], //Doc. Interno
                                      $data->concepto,
                                      $fechaEntrega,
                                      $_SESSION['user'],
                                      $data->detalle[$i]->cod_grupo,
                                      $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                      $data->detalle[$i]->arraySeries[$j]->SERIE,
                                      $data->detalle[$i]->codigo);

                      //Insert hidesebo
                      $result7 = ibase_execute($consulta7,
                                      $row_array['NUMERO'], //Doc. Interno
                                      'SAL',
                                      $data->detalle[$i]->cod_grupo,
                                      $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                      $data->detalle[$i]->arraySeries[$j]->SERIE,
                                      $data->detalle[$i]->codigo,
                                      $data->detalle[$i]->descripcion,
                                      $data->codTecnico,
                                      substr($data->nomTecnico, 0, 29),
                                      $data->concepto,
                                      $fechaEntrega,
                                      $_SESSION['user'],
                                      $data->numeroEntrega);

                      //Insert hidesesu
                      $result8 = ibase_execute($consulta8,
                                  $row_array['NUMERO'], //Doc. Interno
                                  $data->detalle[$i]->cod_grupo,
                                  $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                  $data->detalle[$i]->arraySeries[$j]->SERIE,
                                  $data->detalle[$i]->codigo,
                                  $data->detalle[$i]->descripcion,
                                  $data->codSupervisor,
                                  $data->supervisor,
                                  $data->codTecnico,
                                  substr($data->nomTecnico, 0, 29),
                                  $data->cuadrilla,
                                  '',
                                  $data->concepto,
                                  $fechaEntrega,
                                  $_SESSION['user']);
                }
            }else{
            // Actualiza bodega series, hidesebo, hidesesu
              for ($j = 0; $j < $hasSerie; $j++) {
                  //actualiza acta series
                      $result6 = ibase_execute($consulta6,
                                  $data->codTecnico,
                                      $data->nomTecnico,
                                      $row_array['NUMERO'], //Doc. Interno
                                      $data->concepto,
                                      $fechaEntrega,
                                      $_SESSION['user'],
                                      $data->detalle[$i]->cod_grupo,
                                      $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                      $data->detalle[$i]->arraySeries[$j]->SERIE,
                                      $data->detalle[$i]->codigo);

                       //Insert hidesebo
                      $result7 = ibase_execute($consulta7,
                                      $row_array['NUMERO'], //Doc. Interno
                                      'SAL',
                                      $data->detalle[$i]->cod_grupo,
                                      $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                      $data->detalle[$i]->arraySeries[$j]->SERIE,
                                      $data->detalle[$i]->codigo,
                                      $data->detalle[$i]->descripcion,
                                      $data->codTecnico,
                                      substr($data->nomTecnico, 0, 29),
                                      substr($data->concepto, 0, 50),
                                      $fechaEntrega,
                                      $_SESSION['user'],
                                      $data->numeroEntrega);

                      //Insert hidesesu
                      $result8 = ibase_execute($consulta8,
                                  $row_array['NUMERO'], //Doc. Interno
                                  $data->detalle[$i]->cod_grupo,
                                  $data->detalle[$i]->arraySeries[$j]->CODMARCA,
                                  $data->detalle[$i]->arraySeries[$j]->SERIE,
                                  $data->detalle[$i]->codigo,
                                  $data->detalle[$i]->descripcion,
                                  $data->codSupervisor,
                                  $data->supervisor,
                                  $data->codTecnico,
                                  substr($data->nomTecnico, 0, 29),
                                  $data->cuadrilla,
                                  '',
                                  substr($data->concepto, 0, 50),
                                  $fechaEntrega,
                                  $_SESSION['user']);
                }

            }
        }
    }//end for

    ibase_commit($tr);

    $return = array();
    $return['response'] = 'success';
    $return['numero'] = $row_array['NUMERO'];
    $return['user'] = $_SESSION['user'];
    $return['fechaIng'] = $row_array['FECHA'];


    echo json_encode($return);

} catch (Exception $e) {
  ibase_rollback($tr);
  echo $e;
}

?>