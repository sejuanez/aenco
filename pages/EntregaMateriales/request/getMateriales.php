<?php
    include("gestion.php");


    $ID = addslashes(htmlspecialchars(strtoupper($_POST["material"])));
    $like = explode(" ", $ID);
    $length = count($like);

    $where = "";

    for ($i = 0; $i < $length; $i++) {
        $where .= "M.MA_DESCRIPCION CONTAINING '" . $like[$i] . "'";
        if ($i < ($length - 1)) {
            $where .= ' AND ';
        }

    }

    $sql = "SELECT COALESCE (M.MA_CODIGO, '') CODIGO,
                COALESCE (M.MA_DESCRIPCION, '') DESCRIPCION,
                COALESCE (M.MA_EXISTENCIA, 0) EXISTENCIA,
                COALESCE (G.GR_SERIE, '') SERIADO,
                COALESCE (G.GR_CODIGO, '') GRUPO
          FROM MATERIALES M, GRUPOS G
          WHERE GR_CODIGO = MA_GRUPO AND M.MA_EXISTENCIA > 0 AND  (M.MA_DESCRIPCION  LIKE '%" . $ID . "%' OR M.MA_CODIGO LIKE '%" . $ID . "%')";


    $return_arr = array();

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $row_array['CODIGO'] = utf8_encode($fila[0]);
        $row_array['DESCRIPCION'] = utf8_encode($fila[1]);
        $row_array['EXISTENCIA'] = utf8_encode($fila[2]);
        $row_array['SERIADO'] = utf8_encode($fila[3]);
        $row_array['COD_GRUPO'] = utf8_encode($fila[4]);
        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);

?>
