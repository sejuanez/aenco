// ----------------------------------------------
// Mostrar foto preview
// ----------------------------------------------
// (function() {
//   'use strict';

//   window.addEventListener('load', function() {
//     var form = document.getElementById('formImage');
//     form.addEventListener('submit', function(event) {
//       if (form.checkValidity() === false) {
//         event.preventDefault();
//         event.stopPropagation();
//       }
//       form.classList.add('was-validated');
//     }, false);
//   }, false);
// })();


 jQuery(document).ready(function($) {
	$("#foto").change(function(){
    	readURL(this);
	});
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgFuncionario').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


let app = new Vue({
		el:'#app',
		data : {

			valid : true,

			inputSearch : "",

			cedula: "",
			nombre1:"",
			nombre2 : "",
			apellido1 :"",
			apellido2 : "",
			cuadrilla : "",
			estado: "",


			listFuncionarios : [],

			btnGuardar : true ,
			btnActualizar : false ,
			btnEliminar : false ,
			btnEditar : false ,
			btnFile:true,
			btnCancelar : true,

		},


		methods:{

			nuevo : function () {
				this.resetModal();
				$('#addFucionario').modal('show');
			},

			verFuncionario: function (dato){
				this.cedula =  dato.cedula;
				this.nombre1 = dato.nombre1;
				this.nombre2 = dato.nombre2;
				this.apellido1 = dato.apellido1;
				this.apellido2 = dato.apellido2;
				this.cuadrilla = dato.cuadrilla;
				this.estado = dato.estado;

				$('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+dato.cedula);

				$('.formGuardar input').attr('disabled', 'disabled');
				$('.formGuardar select').attr('disabled', 'disabled');


				this.btnGuardar = false ;
				this.btnActualizar = false ;
				this.btnEliminar = true ;
				this.btnEditar = true ;

				this.btnCancelar = false;
				this.btnFile = false;

				$('#addFucionario').modal('show');
			},

			search : function () {
				var app = this;
				$.post('./request/getFuncionario.php', { funcionario : app.inputSearch }, function(data) {
					var datos = jQuery.parseJSON(data);
					app.listFuncionarios = datos;

				});
			},

			resetModal : function (){
				$('#addFucionario').modal('hide');
				this.cedula = "";
				this.nombre1 = "";
				this.nombre2 = "";
				this.apellido1 ="";
				this.apellido2 = "";
				this.cuadrilla = "";
				this.estado = "";
				$('input[type=file]').val(null);
				$('#imgFuncionario').attr('src', 'img/user.png');
				$('.formGuardar').removeClass('was-validated');
				this.btnGuardar = true ;
				this.btnActualizar = false ;
				this.btnEliminar = false ;
				this.btnEditar = false ;
				this.btnFile = true;
				$('.formGuardar input').removeAttr('disabled');
				$('.formGuardar select').removeAttr('disabled');
			},

			guardar : function () {

				if (this.cedula == "" ||
					this.nombre1 == "" ||
					this.apellido1 == "" ||
					this.apellido2 == "" ||
					this.cuadrilla == "" ||
					this.estado == "")
				{
					$('.formGuardar').addClass('was-validated');
				}else{

					var app = this;
					var formData = new FormData($('.formGuardar')[0]);

					//hacemos la petición ajax
			        $.ajax({
			            url: './uploadFile.php',
			            type: 'POST',
			            // Form data
			            //datos del formulario
			            data: formData,
			            //necesario para subir archivos via ajax
			            cache: false,
			            contentType: false,
			            processData: false,
			            //mientras enviamos el archivo
			            beforeSend: function(){},
			            success: function(data){
			            	// console.log(data);
			            	app.formDatos(data, "./request/insertFuncionario.php");
			            },
			            //si ha ocurrido un error
			            error: function(FormData){
			                console.log(data)
			            }
			        });
				}
			},

			editar : function () {
				this.btnFile = true;
				this.btnCancelar = true;
				this.btnEliminar = false;
				this.btnEditar = false;
				this.btnActualizar = true;
				$('.formGuardar input').removeAttr('disabled');
				$('.formGuardar #cedula').attr('disabled', 'disabled');
				$('.formGuardar select').removeAttr('disabled');
			},

			actualizar: function () {
				if (this.cedula == "" ||
					this.nombre1 == "" ||
					this.apellido1 == "" ||
					this.apellido2 == "" ||
					this.cuadrilla == "" ||
					this.estado == "")
				{
					$('.formGuardar').addClass('was-validated');
				}else{

					var app = this;
					var formData = new FormData($('.formGuardar')[0]);

					//hacemos la petición ajax
			        $.ajax({
			            url: './uploadFile.php',
			            type: 'POST',
			            // Form data
			            //datos del formulario
			            data: formData,
			            //necesario para subir archivos via ajax
			            cache: false,
			            contentType: false,
			            processData: false,
			            //mientras enviamos el archivo
			            beforeSend: function(){},
			            success: function(data){
			            	// console.log(data);
			            	app.formDatos(data, "./request/updateFuncionario.php");
			            },
			            //si ha ocurrido un error
			            error: function(FormData){
			                console.log(data)
			            }
			        });
				}
			},

			eliminar: function () {
				var app = this;
				alertify.confirm("Eliminar",".. Desea eliminar este item?",
				  function(){
				  	$.post('./request/eliminarFuncionario.php', { cedula : app.cedula }, function(data) {
				  		// console.log(data);
				  		if (data==1) {
				  			alertify.success("Registro Eliminado.");
				  			app.resetModal();
				  			app.search();

				  		}
				  	});

				  },
				  function(){
				    // alertify.error('Cancel');
				  });
			},

			formDatos : function (data, url) {
				var app = this;
				var formData = new FormData($('.formGuardar')[0]);
				formData.append('url', data);
				formData.append('cedula1', app.cedula);
					//hacemos la petición ajax
			        $.ajax({
			            url: url,
			            type: 'POST',
			            // Form data
			            //datos del formulario
			            data: formData,
			            //necesario para subir archivos via ajax
			            cache: false,
			            contentType: false,
			            processData: false,
			            //mientras enviamos el archivo
			            beforeSend: function(){},
			            success: function(data){
			            	console.log(data);
			            	if(data == 1){
			            		alertify.success("Registro guardado Exitosamente");
			            		app.resetModal();
			            		if(url=="./request/updateFuncionario.php"){
			            			app.search();
			            		}

			            	}else{
			            		alertify.error("Ha ocurrido un error");
			            	}

			            },
			            //si ha ocurrido un error
			            error: function(FormData){
			                console.log(data)
			            }
			        });
			},

		},
		watch: {

	    },
		mounted() {
			// this.loadSelectPlaca();
			// this.loadSelectGastos();
			// this.loadSelectProveedores();


			 jQuery(document).ready(function($) {
				$("#foto").change(function(){
			    	readURL(this);
				});
			});


		},

});