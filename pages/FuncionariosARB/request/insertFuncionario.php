<?php

  define('MAX_SEGMENT_SIZE', 65535);

  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

  include("../../../init/gestion.php");
// include("gestion.php");



function blob_create($data) {
    if (strlen($data) == 0)
        return false;
    $handle = ibase_blob_create();
    $len = strlen($data);
    for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
        $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
        $buf = substr($data, $pos, $buflen);
        ibase_blob_add($handle, $buf);
    }
    return ibase_blob_close($handle);
}


  $cedula = $_POST['cedula'];
  $nombre1 = $_POST['nombre1'];
  $nombre2 = $_POST['nombre2'];
  $apellido1 = $_POST['apellido1'];
  $apellido2 = $_POST['apellido2'];
  $cuadrilla = $_POST['cuadrilla'];
  $estado = $_POST['estado'];
  $url = $_POST['url'];
  $direccion = '';



  // var_dump($url);
 if ($url == 'null') {
      $foto = NULL;
    }else{
       $foto = blob_create(file_get_contents("../temp/".$url));
    }


  $sql = "INSERT into datos_rrhh
            ( rh_cedula,
              rh_nombre,
              rh_nombre1,
              rh_nombre2,
              rh_apellido1,
              rh_apeelido2,
              rh_estado,
              rh_profesion,
              rh_direccion,
              rh_foto)
           values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  // $result = ibase_query($conexion, $sql);

    $consulta = ibase_prepare($conexion , $sql);
    $result = ibase_execute($consulta,
                        $cedula,
                        $nombre1,
                        $nombre1,
                        $nombre2,
                        $apellido1,
                        $apellido2,
                        $estado,
                        $cuadrilla,
                        $direccion,
                        $foto
                    );

if ($url != 'null') {
       unlink("../temp/".$url);//acá le damos la direccion exacta del archivo
}

echo $result;

?>