<!DOCTYPE html>
<html>
<head>
	<title>Incripcion</title>
	<meta charset ="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
	<link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
	    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous"> -->


	<style type="text/css">
		label{
			font-size: 0.8em;
		}
		.fondoGris {
			 background: #EFEFEF;
		}
		.form-group {
		    margin-bottom: 0.2rem;
		}
		.nav-tabs .nav-link {
			background: #EFEFEF;
			color:#999;
		}
		.form-control:disabled, .form-control[readonly] {
		    background-color: #eee;
		    opacity: 0.8;
		}
		#btnBuscar:hover{
			color: #333;
			text-shadow: 1px 0 #CCC;
		}
		select{
			padding: 3px;
			width: 100%;
		}

		.danger label{color:red;}
		.danger input{border:1px solid red}
		.danger select{border:1px solid red}*/


	</style>

</head>
<body>

	<div id="app">

		<header>
		  <p class="text-center fondoGris" style="padding: 10px;">

		     Funcionarios ARB

			</p>
		</header>


		<div class="container-fluid">
				<div class="row" style="margin-bottom: 10px;">

					<div class="col-6">
						<button class="btn btn-primary btn-sm" @click="nuevo()">Nuevo</button>
					</div>
					<div class="col-6">
						<div class="input-group float-right input-group-sm">
			                <input type="text" class="form-control" v-model ="inputSearch" placeholder="Buscar funcionario">
			                    <span class="input-group-btn">
			                      <button type="button" class="btn btn-primary" @click="search()">Buscar</button>
			                    </span>
			             </div>
					</div>
				</div>

				<div class="row">

					<div class="col-12">
						<table class="table table-sm" >
						  <thead class="fondoGris">
						    <tr>
						      <th>Cedula</th>
						      <th>Nombre 1</th>
						      <th>Nombre 2</th>
						      <th>Apellido 1</th>
						      <th>Apellido 2</th>
						      <th>Cuadrilla</th>
						      <th width="80">Estado</th>
						      <th style="text-align: center;"  width="100">+</th>
						    </tr>
						  </thead>
						  <tbody id="detalle_gastos" style="font-size: .8em">

						  	  <tr v-for="(dato, index) in listFuncionarios">
							      <td v-text ="dato.cedula"></td>
							      <td v-text ="dato.nombre1"></td>
							      <td v-text ="dato.nombre2"></td>
							      <td v-text ="dato.apellido1"></td>
							      <td v-text ="dato.apellido2"></td>
							      <td v-text ="dato.cuadrilla"></td>
							      <td v-text ="dato.estado"></td>
							      <td width="100" style="text-align: center;">
						  			<i class="fa fa-eye" title="" style="cursor:pointer; margin: 0 0 10px 10px;" @click="verFuncionario(dato)" ></i>
						  		</td>

						      </tr>
						  </tbody>
						</table>
					</div>
				</div>

		</div>

		<div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">

			    	<div class="modal-header">
				        <h6 class="modal-title" id="exampleModalLabel">Nuevo Funcionario
				        	<small>( Los campos con <code>*</code> son obligatorios )</small></h6>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
					</div>

					<form enctype="multipart/form-data" class="formGuardar" novalidate >

						<div class="modal-body">
				          <div class="row">
				          		<div class="col-8">

				          		  <div class="row">

									<div class="col-6" >
										<div class="form-group input-group-sm">
											<label for="cedula">Cedula <code>*</code></label>
										    <input type="number" id="cedula" name="cedula" class="form-control" required aria-label="cedula" v-model="cedula">
										    <div class="invalid-feedback">Este campo es requerido</div>
										</div>

									</div>
									</div>

									<div class="row">
									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="nombre1">Nombre 1 <code>*</code></label>
										  	<input type="text" id="nombre1" name="nombre1" class="form-control" required aria-label="nombre1" v-model="nombre1">
										    <div class="invalid-feedback">Este campo es requerido</div>

										</div>
									</div>

									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="nombre2">Nombre 2 </label>
										    <input type="text" id="nombre2" name="nombre2" class="form-control" aria-label="nombre2" v-model="nombre2">
										</div>
									</div>

									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="apellido1">Apellido 1 <code>*</code></label>
										    <input type="text" id="apellido1" name="apellido1" class="form-control" required aria-label="apellido1" v-model="apellido1">
											 <div class="invalid-feedback">Este campo es requerido</div>

										</div>
									</div>

									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="apellido2">Apellido 2 <code>*</code></label>
										    <input type="text" id="apellido2" name="apellido2" class="form-control" required aria-label="apellido2" v-model="apellido2">
										    <div class="invalid-feedback">Este campo es requerido</div>
										</div>
									</div>

									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="cuadrilla">Cuadrilla <code>*</code></label>
										    <input type="text" id="cuadrilla" name="cuadrilla" class="form-control" aria-label="cuadrilla" required v-model="cuadrilla">
										    <div class="invalid-feedback">Este campo es requerido</div>
										</div>
									</div>



									<div class="col-6">
										<div class="form-group input-group-sm">
											<label for="estado">Estado <code>*</code></label>

											<select class="form-control form-control-sm" name="estado" v-model="estado" required >
												 <!-- <option >Seleccione...</option> -->
												 <option value="ACTIVO">ACTIVO</option>
												 <option value="INACTIVO">INACTIVO</option>
											</select>
											<div class="invalid-feedback">Este campo es requerido</div>

										</div>
									</div>

									<div class="col-6" v-if="btnFile">
									  <div class="form-group">
									    <label for="foto">Foto <code>*</code></label>
										<input type ="file" class="form-control-file" id="foto" accept="image/*" name="foto" required>
<!--
										<label class="custom-file">
										    <input type="file" id="file" class="custom-file-input" id="foto" accept="image/*" name="foto" required>
										    <span class="custom-file-control">Buscar</span>
										</label>
 -->
									  </div>
									</div>

								</div>
							</div>

							<div class="col-4">
								<img id="imgFuncionario" src="img/user.png" alt="" width="100%">
							</div>

							</div>

						</div>

						<div class="modal-footer">
								<button type="button" v-if="btnCancelar" class="btn btn-secondary btn-sm" @click="resetModal()">cancelar</button>

						        <button type="button" v-if="btnActualizar" class="btn btn-primary btn-sm" @click="actualizar()">Actualizar</button>
						        <button type="button" v-if="btnEditar" class="btn btn-info btn-sm" @click="editar()">Editar</button>
						        <button type="button" v-if="btnEliminar" class="btn btn-danger btn-sm" @click="eliminar()">Eliminar</button>

						        <button type="button" v-if="btnGuardar" class="btn btn-primary btn-sm" @click="guardar()">Guardar</button>

						</div>

					</form>




			    </div>
			  </div>
		</div>


	</div>



	<script src="js/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/select2.min.js"></script>
	<script src="js/bootstrap/popper.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/alertify/alertify.min.js"></script>
	<script src="js/vue/vue.js"></script>
	<script src="js/app.js"></script>



</body>
</html>