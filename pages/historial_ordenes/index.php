<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Historial órdenes</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<!--<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />-->

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});






			$("#consultando").hide();
			$("#contenido").hide();


			$("#div-asignadas").hide();
			$("#div-ejecutadas").hide();
			$("#div-pendientes").hide();




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$("#consultar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				
				


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



				$.ajax({
					url:'request/getHistorial.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin, nic:$("#txtNic").val()}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						$("#txtNombreUsuario").html(repuesta[0].nombre);
						$("#txtMunicipio").html(repuesta[0].municipio);
						$("#txtDepartamento").html(repuesta[0].departamento);
						$("#txtDireccion").html(repuesta[0].direccion);
						$("#txtBarrio").html(repuesta[0].barrio);

						
						var filas="";

						for (var i=0; i<repuesta.length; i++) {
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].acta+"</td><td>"+repuesta[i].nroOs+"</td><td>"+repuesta[i].fechaEje+"</td><td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].tipoOs+"</td><td>"+repuesta[i].resultado+"</td><td>"+repuesta[i].accionAnomalia+"</td></tr>";
                               
                              
						}
						
						$("table tbody").html(filas);

						
						$("#contenido").show();

					}
					else{
												
						
						$("table tbody").html("");

						$("#alert").dialog("open");
								
					}



					$("#consultando").hide();
					$("#consultar").show();
				});
			});
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});

			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay historial de órdenes para mostrar.</p>
	</div>

	<header>
		<h3>Historial órdenes de usuario</h3>
		<nav>
			<ul id="menu">
				
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				
				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<div><label>Nic</label><input type="text" id="txtNic" ></div>
				
				
			</form>
		
		</div>
		
		
		
	</div>



	<div id='contenido'>

		<div id="div-usuario">
			<span id="foto"><span class="ion-ios-person-outline"></span></span>
			<h3 id="txtNombreUsuario">Elkin Barreto Contreras</h3>
			<h4><span id="txtMunicipio">Corozal</span> - <span id="txtDepartamento">Sucre</span></h4>
			<h6><span id="txtDireccion">Cra 4a N 5 - 94</span> - <span id="txtBarrio">Pileta</span></h4>

		</div>

		<div id='div-historial'>
			<table>
				<thead>
					<tr class='cabecera'><td>Acta</td><td>Nro. OS</td><td>Fecha ejec.</td><td>Proceso</td><td>Tipo OS</td><td>Resultado</td><td>Acción y/o anomalía</td></tr>
				</thead>

				<tbody>
					
				</tbody>
			</table>
		</div>


		<!--<div id='div-graficos' >

			<div id="grafico-barras" ></div>

		</div>-->

	</div>

	

</body>
</html>