<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include('../../../init/gestion.php');

	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	$nic = $_POST['nic'];
	
	
	
	$consulta = "SELECT lc.ca_acta, lc.ca_orden, lc.ca_fechaej, dc.dc_nic, dc.dc_nombre, dc.dc_direccion, d.de_nombre, m.mu_nombre, dc.dc_nbarrio, c.c_nombre, tos.to_descripcion, dum.ui_codigo, rscr.r_descripcion, dua.ui_codigo, a.ac_descripcion from lega_cabecera lc inner join dato_clientes dc on dc.dc_acta=lc.ca_acta and dc.dc_nic = '".$nic."' left join departamentos d on d.de_codigo=lc.ca_depto left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio left join campanas c on c.c_codigo = lc.ca_campana	 left join tipo_orden_servicio tos on tos.to_codigo = lc.ca_codtipoordens left join dato_unirre dum on dum.ui_acta=lc.ca_acta and dum.ui_tipo='M' left join dato_unirre dua on dua.ui_acta=lc.ca_acta and dua.ui_tipo in ('A', 'R') left join acciones a on a.ac_tiporden=lc.ca_codtipoordens and a.ac_motivo=dum.ui_codigo and a.ac_codigo=dua.ui_codigo left join resultado_scr rscr on rscr.r_tiporden=lc.ca_codtipoordens and rscr.r_codigo=dum.ui_codigo where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' order by lc.ca_fechaej";
	
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['acta'] = utf8_encode($fila[0]);
		$row_array['nroOs'] = utf8_encode($fila[1]);		
		$row_array['fechaEje'] = utf8_encode($fila[2]);
		$row_array['nic'] = utf8_encode($fila[3]);
		$row_array['nombre'] = utf8_encode($fila[4]);
		$row_array['direccion'] = utf8_encode($fila[5]);
		$row_array['departamento'] = utf8_encode($fila[6]);
		$row_array['municipio'] = utf8_encode($fila[7]);
		$row_array['barrio'] = utf8_encode($fila[8]);
		$row_array['proceso'] = utf8_encode($fila[9]);
		$row_array['tipoOs'] = utf8_encode($fila[10]);
		$row_array['codResultado'] = utf8_encode($fila[11]);
		$row_array['resultado'] = utf8_encode($fila[12]);
		$row_array['codAnomalia'] = utf8_encode($fila[13]);
		$row_array['accionAnomalia'] = utf8_encode($fila[14]);
		

					
		array_push($return_arr, $row_array);
	}


	/*//prueba

		$row_array['acta'] = "prueba0";
		$row_array['nroOs'] = "prueba1";		
		$row_array['fechaEje'] = "prueba2";
		$row_array['nic'] = "prueba3";
		$row_array['nombre'] = "prueba4";
		$row_array['direccion'] = "prueba5";
		$row_array['departamento'] = "prueba6";
		$row_array['municipio'] = "prueba7";
		$row_array['barrio'] = "prueba8";
		$row_array['proceso'] = "prueba9";
		$row_array['tipoOs'] = "prueba10";
		$row_array['codResultado'] = "prueba11";
		$row_array['resultado'] = "prueba12";
		$row_array['codAnomalia'] = "prueba13";
		$row_array['accionAnomalia'] = "prueba14";
		

					
		array_push($return_arr, $row_array);


		$row_array['acta'] = "prueba0";
		$row_array['nroOs'] = "prueba1";		
		$row_array['fechaEje'] = "prueba2";
		$row_array['nic'] = "prueba3";
		$row_array['nombre'] = "prueba4";
		$row_array['direccion'] = "prueba5";
		$row_array['departamento'] = "prueba6";
		$row_array['municipio'] = "prueba7";
		$row_array['barrio'] = "prueba8";
		$row_array['proceso'] = "prueba9";
		$row_array['tipoOs'] = "prueba10";
		$row_array['codResultado'] = "prueba11";
		$row_array['resultado'] = "prueba12";
		$row_array['codAnomalia'] = "prueba13";
		$row_array['accionAnomalia'] = "prueba14";
		

					
		array_push($return_arr, $row_array);


		$row_array['acta'] = "prueba0";
		$row_array['nroOs'] = "prueba1";		
		$row_array['fechaEje'] = "prueba2";
		$row_array['nic'] = "prueba3";
		$row_array['nombre'] = "prueba4";
		$row_array['direccion'] = "prueba5";
		$row_array['departamento'] = "prueba6";
		$row_array['municipio'] = "prueba7";
		$row_array['barrio'] = "prueba8";
		$row_array['proceso'] = "prueba9";
		$row_array['tipoOs'] = "prueba10";
		$row_array['codResultado'] = "prueba11";
		$row_array['resultado'] = "prueba12";
		$row_array['codAnomalia'] = "prueba13";
		$row_array['accionAnomalia'] = "prueba14";
		

					
		array_push($return_arr, $row_array);

	// fin prueba*/

	echo json_encode($return_arr);

?>