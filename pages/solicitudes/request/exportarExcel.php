<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }
    
	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=solicitudes_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	include("../../../init/gestion.php");

	$fechaIni=$_GET["fechaIni"];
	$fechaFin=$_GET["fechaFin"];
	$mun=$_GET["mun"];
	$dpto=$_GET["dpto"];

	echo "fechaIni: ".$fechaIni." FechaFin:".$fechaFin." mun:".$mun." dpto:".$dpto;


	if($mun!="null"){

		$consulta = "SELECT o.oa_numero orden, o.oa_fecha_reporte Fecha_Reporte, case when lc.ca_fechaej is null AND (o.oa_acta is null) Then 'PENDIENTE POR EJECUTAR' when o.oa_acta = 'ANULADA' Then o.oa_acta ELSE lc.ca_fechaej end Fecha_Atencion, coalesce(o.oa_cedula,0) Cedula, o.oa_apellido1 Apellido1, o.oa_apellido2 Apellido2,  o.oa_nombre1 Nombre1, o.oa_nombre2 Nombre2, d.de_nombre Departamento, m.mu_nombre Municipio, c.co_nombre Corregimiento, b.ba_nombarrio Barrio_Vda, o.oa_direccion Direccion, o.oa_pto_ref Pto_Referencia, o.oa_telefonos Telefonos, tos.to_descripcion Tipo_de_orden, tam.descripcion Observacion, o.oa_serie Serie, o.oa_poste Poste, o.oa_ususis_genera Usuario_Genera, t.te_nombres Tecnico, o.oa_fecha_asigna Fecha_asignacion from ot_ap o left join departamentos d on d.de_codigo=o.oa_dpto left join municipios m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio left join corregimientos c on c.co_depto=o.oa_dpto and c.co_municipio=o.oa_mpio and c.co_codcorregimiento=o.oa_corregimiento left join barrios b on b.ba_depto=o.oa_dpto and b.ba_mpio=o.oa_mpio and b.ba_sector=o.oa_corregimiento and b.ba_codbarrio = o.oa_barrio left join tipo_orden_servicio tos on tos.to_codigo=o.oa_tos left join tablas_aforomantenimiento tam on tam.tipo='OBS LAMPARAS' and tam.codigo=o.oa_estado_lum left join tecnicos t on t.te_codigo=o.oa_tecnico left join lega_cabecera lc on lc.ca_acta=cast(o.oa_numero as varchar(20)) where o.oa_fecha_reporte between '".$fechaIni."' and '".$fechaFin."' and o.oa_dpto='".$dpto."' and o.oa_mpio='".$mun."'";
		
	}
	else{

		$consulta = "SELECT o.oa_numero orden, o.oa_fecha_reporte Fecha_Reporte, case when lc.ca_fechaej is null AND (o.oa_acta is null) Then 'PENDIENTE POR EJECUTAR' when o.oa_acta = 'ANULADA' Then o.oa_acta ELSE lc.ca_fechaej end Fecha_Atencion, coalesce(o.oa_cedula,0) Cedula, o.oa_apellido1 Apellido1, o.oa_apellido2 Apellido2,  o.oa_nombre1 Nombre1, o.oa_nombre2 Nombre2, d.de_nombre Departamento, m.mu_nombre Municipio, c.co_nombre Corregimiento, b.ba_nombarrio Barrio_Vda, o.oa_direccion Direccion, o.oa_pto_ref Pto_Referencia, o.oa_telefonos Telefonos, tos.to_descripcion Tipo_de_orden, tam.descripcion Observacion, o.oa_serie Serie, o.oa_poste Poste, o.oa_ususis_genera Usuario_Genera, t.te_nombres Tecnico, o.oa_fecha_asigna Fecha_asignacion from ot_ap o left join departamentos d on d.de_codigo=o.oa_dpto left join municipios m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio left join corregimientos c on c.co_depto=o.oa_dpto and c.co_municipio=o.oa_mpio and c.co_codcorregimiento=o.oa_corregimiento left join barrios b on b.ba_depto=o.oa_dpto and b.ba_mpio=o.oa_mpio and b.ba_sector=o.oa_corregimiento and b.ba_codbarrio = o.oa_barrio left join tipo_orden_servicio tos on tos.to_codigo=o.oa_tos left join tablas_aforomantenimiento tam on tam.tipo='OBS LAMPARAS' and tam.codigo=o.oa_estado_lum left join tecnicos t on t.te_codigo=o.oa_tecnico left join lega_cabecera lc on lc.ca_acta=cast(o.oa_numero as varchar(20)) where o.oa_fecha_reporte between '".$fechaIni."' and '".$fechaFin."'";

		
	}
	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Orden</td>".
						"<td>Fecha reporte</td>".
						"<td>Fecha atencion</td>".
						"<td>Cedula</td>".
						"<td>Apellido1</td>".
						"<td>Apellido2</td>".
						"<td>Nombre1</td>".
						"<td>Nombre2</td>".
						"<td>Departamento</td>".
						"<td>Municipio</td>".
						"<td>Corregimiento</td>".
						"<td>Barrio/Vda</td>".
						"<td>Direccion</td>".
						"<td>Pto. Referencia</td>".
						"<td>Telefonos</td>".
						"<td>Tipo orden</td>".
						"<td>Observacion</td>".
						"<td>Serie</td>".
						"<td>poste</td>".
						"<td>Usuario genera</td>".
						"<td>Tecnico</td>".
						"<td>Fecha asignacion</td>".						
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
					"<td>".utf8_encode($fila[5])."</td>".
					"<td>".utf8_encode($fila[6])."</td>".
					"<td>".utf8_encode($fila[7])."</td>".
					"<td>".utf8_encode($fila[8])."</td>".
					"<td>".utf8_encode($fila[9])."</td>".
					"<td>".utf8_encode($fila[10])."</td>".
					"<td>".utf8_encode($fila[11])."</td>".
					"<td>".utf8_encode($fila[12])."</td>".
					"<td>".utf8_encode($fila[13])."</td>".
					"<td>".utf8_encode($fila[14])."</td>".
					"<td>".utf8_encode($fila[15])."</td>".
					"<td>".utf8_encode($fila[16])."</td>".
					"<td>".utf8_encode($fila[17])."</td>".
					"<td>".utf8_encode($fila[18])."</td>".
					"<td>".utf8_encode($fila[19])."</td>".
					"<td>".utf8_encode($fila[20])."</td>".
					"<td>".utf8_encode($fila[21])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";

	echo $tabla;
	
?>