<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];
	
	$consulta = "SELECT m.mu_codigomun CodMunicipio, m.mu_nombre NombreMunicipio, d.de_codigo CodDepto, d.de_nombre NombreDpto from municipios m inner join departamentos d on d.de_codigo=m.mu_depto";
	

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$row_array['codigoMun'] = null;
	$row_array['nombreMun'] = '(Todos)';
	$row_array['codigoDpto'] = null;
	$row_array['nombreDpto'] = '(Todos)';
	
	array_push($return_arr, $row_array);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['codigoMun'] = utf8_encode($fila[0]);
		$row_array['nombreMun'] = utf8_encode($fila[1]);

		$row_array['codigoDpto'] = utf8_encode($fila[2]);
		$row_array['nombreDpto'] = utf8_encode($fila[3]);
		
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>