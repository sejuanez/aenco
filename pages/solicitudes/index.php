<?php
  session_start();
  
  if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  }  
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Solicitudes</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
		
  <script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  <script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  <script type="text/javascript" src="../../js/accounting.js"></script>
    

  	<script type="text/javascript">
          
  		$(function(){

        $("#modal").hide();

        $( "#alert" ).dialog({
            modal: true,
            autoOpen: false,
            resizable:false,
             buttons: {
                  Ok: function() {
                    //$("#alert p").html("");
                    $( this ).dialog( "close" );
                  }
              }
           });


        $("#tabs").tabs({
            show: { effect: "slide", duration: 200 },
            active:0,
            activate: function( event, ui ) {

              var ancho = +$("#div-grafico-tecnico").width();
              var alto = +$("#div-grafico-tecnico").height();
              $("#grafico-tecnico").highcharts().setSize(ancho,alto, false);              
            }
        });

        
        $(window).resize(function() {
            var ancho = +$("#div-grafico-tecnico").width();
            var alto = +$("#div-grafico-tecnico").height();
          $("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

          
        });



        function dibujarGraficoBarras(element, categorias, series, colores){
            
            $('#'+element).highcharts({
                  
                  chart: {
                      type: 'column',
                      backgroundColor: "#fafafa"
                  },

                  colors:colores,

                  title: {
                      text:null /*'Historic World Population by Region'*/
                  },
                  /*subtitle: {
                      text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
                  },*/
                  xAxis: {
                      categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
                      title: {
                          text: null
                      }
                  },
                  yAxis: {
                      min: 0,
                      title: {
                          text: 'Cantidad'/*,
                          align: 'high'*/
                      },
                      labels: {
                          overflow: 'justify'
                      }
                  },
                  tooltip: {
                      valueSuffix: ''
                  },
                  plotOptions: {
                      /*bar: {
                          dataLabels:{
                    enabled: true,
                    rotation: 90,
                    x:5,
                    y:-6
                  }
                      }*/
                column:{
                  dataLabels:{
                    enabled: true,
                    x:0,
                    y:-12,
                    //borderRadius: 5,
                              //backgroundColor: 'rgba(252, 255, 197, 0.7)',
                              /*borderWidth: 1,
                              borderColor: '#AAA',*/
                              padding:20
                  }
                }
                  },
                  legend: {
                      layout: 'horizontal',
                      align: 'right',
                      verticalAlign: 'top',
                      x: 0,
                      y: 0,
                      floating: false,
                      borderWidth: 0,
                      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                      shadow: false
                  },
                  credits: {
                      enabled: false
                  },
                  series: series/*[{
                      name: 'Year 1800',
                      data: [107, 31, 635, 203, 2]
                  }, {
                      name: 'Year 1900',
                      data: [133, 156, 947, 408, 6]
                  }, {
                      name: 'Year 2012',
                      data: [1052, 954, 4250, 740, 38]
                  }]*/
              });
        };
        
        function buscar(evt){
        
            evt.preventDefault();

            $("#exportar").hide();
            $("#consultar").hide();
            $("#consultando").show();

            $("#div-asignadas").fadeOut(200);
            $("#div-ejecutadas").fadeOut(200);
            $("#div-anuladas").fadeOut(200);
            $("#div-pendientes").fadeOut(200);

            $("#contenido").hide();

            
            

            
            //$("#div-tabla").html("<h4 style='text-align:center; color:#999; font-weight:normal'>Cargando...</h4>");


            var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
            if(diaIni<10)
              diaIni="0"+diaIni;

            var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
            if(mesIni<10)
              mesIni="0"+mesIni;

            var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
            if(diaFin<10)
              diaFin="0"+diaFin;

            var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
            if(mesFin<10)
              mesFin="0"+mesFin;



            //var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
            var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
                //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
            var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;

            var municipio = document.getElementById("municipio");
            
            
            $.ajax({
              
              url:'request/getSolicitudes.php',
                      type:'POST',
                      dataType:'json',
                      data:{
                          fechaIni:fechaIni,
                          fechaFin:fechaFin,
                          mun:municipio.value,
                          dpto:municipio.options[municipio.selectedIndex].dataset['dpto']
                      }
            }).done(function(repuesta){

              if(repuesta.length>0)
              {
                //excel = repuesta[0].tabla;
                $("#div-tabla").html(repuesta[0].tabla);
                
                $("#exportar a").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin+"&mun="+municipio.value+"&dpto="+municipio.options[municipio.selectedIndex].dataset['dpto']);
                $("#exportar").show();

                $("#total-asignadas").html(repuesta[0].asignadas);
                $("#total-ejecutadas").html(repuesta[0].ejecutadas);
                $("#total-anuladas").html(repuesta[0].anuladas);
                $("#total-pendientes").html(repuesta[0].pendientes);

                $("#div-asignadas").fadeIn(200);
                $("#div-ejecutadas").fadeIn(200);
                $("#div-pendientes").fadeIn(200);
                $("#div-anuladas").fadeIn(200);


                var categorias = ['Ejecutadas', 'Anuladas', 'Pendiente por ejecutar'];
                
                var cantidad = [+repuesta[0].ejecutadas,+repuesta[0].anuladas,+repuesta[0].pendientes];

                var series=[
                        {
                          name:'Cantidad', 
                          data:cantidad,
                          dataLabels: {
                            enabled: true,
                                            //rotation: -90,
                                            color: '#000',
                                            align: 'center',
                                          // format: '${point.y}', // one decimal
                                            /*formatter:function(){
                                              return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
                                            },*/
                                            y: 10, // 10 pixels down from the top
                                            style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                            }
                                        },
                                        colorByPoint: true
                                      }
                      ];

                //var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

                //var colors = [/*'#F3FD17', '#90FD17', '#17D5FD',*/ '#1724FD', '#FD174B'];
                var colors = ['#90FD17','#FF0000','orange'];
                dibujarGraficoBarras("grafico-tecnico",categorias,series,colors);

                $("#contenido").show();

              }
              else
              {
                $("#total-asignadas").html("");
                $("#total-ejecutadas").html("");
                $("#total-pendientes").html("");
                $("#total-anuladas").html("");

                $("#div-tabla").html("");

                //alert("No se encontraron solicitudes");
                $( "#alert" ).dialog("open");

                $("#exportar").hide();
              }


              $("#consultando").hide();
              $("#consultar").show();
              
            });

        };//fin de la funcion buscar()

        function iniciarComponentes(){

            //$("#div-vlrpago").hide();
            $("#div-asignadas").hide();
            $("#div-ejecutadas").hide();
            $("#div-anuladas").hide();
            $("#div-pendientes").hide();
           
            $("#consultar").hide();
            $("#consultando").hide();
            $("#exportar").hide();

            $('#municipio').attr('disabled',true);

            $("#contenido").hide();

            

          
            $("#txtFechaFin").datepicker({
                minDate: new Date(),
                changeMonth: true,
                changeYear: true,
                dateFormat:"dd/mm/yy",
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
              });

            
            $("#txtFechaFin").datepicker("setDate", new Date());



      
            $("#txtFechaIni").datepicker({
              changeMonth: true,
              changeYear: true,
              dateFormat:"dd/mm/yy",
              dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
            });

            $("#txtFechaIni").datepicker("setDate", new Date());




            $("#txtFechaIni").change(function(){
        
              $("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
            });


            //ajax que carga la lista de municipios
      $.ajax({
          url:'request/getMunicipios.php',
                  type:'POST',
                  dataType:'json'
                  
          }).done(function(repuesta){
          
          if(repuesta.length>0){
            var optionsMun="";
                        
            for(var i=0;i<repuesta.length;i++){
              if(repuesta[i]!=null)
                optionsMun+="<option value='"+repuesta[i].codigoMun+"' data-dpto='"+repuesta[i].codigoDpto+"' >"+repuesta[i].nombreMun+"</option>";
              
            }
            
            $("#municipio").html(optionsMun);           
            $('#municipio').attr('disabled',false);
            
            //doneAjax1= true;

            //if(doneAjax1&&doneAjax2)
              $("#consultar").show();
            
                      
          }
          
          
      });// fin del ajax departamentos//ajax que carga la lista de departamentos

        
          
            $("#consultar").click(buscar);

         
         
        };//fin de iniciarComponentes()

        //===================================================================================

        
         iniciarComponentes();                                      
                 
        //======================================================================================
        $("body").show();
       
  		});//fin del onready
  	</script>
</head>
<body style="display:none">

    <!--alert-->
    <div id="alert" title="Mensaje">
        <p>No se encontraron solicitudes</p>
    </div>

    <!--header-->
    <header>
      <h3>Solicitudes</h3>
      <nav>
        <ul id="menu">
          
          <li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
          <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
          <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
            
        </ul>
      </nav>    
    </header>

    <!--subheader-->

    <div id='subheader'>

          <div id="div-form">
            <form id="form">
              <div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>               
              <div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>               
              <div><label>Municipio</label><select id='municipio'></select></div>
            </form>          
          </div>
    
          <div id="div-subtotales">
              
                <div id="div-asignadas">        
                    <h4 id="total-asignadas"></h4>
                    <h5><span>Total </span> órdenes</h5>
                </div>

                <div id="div-ejecutadas">                    
                    <h4 id="total-ejecutadas"></h4>
                    <h5><span>Total </span> ejecutadas</h5>
                </div>

                <div id="div-anuladas">                    
                    <h4 id="total-anuladas"></h4>
                    <h5><span>Total </span>anuladas</h5>
                </div>

                <div id="div-pendientes">                    
                    <h4 id="total-pendientes"></h4>
                    <h5><span>Total </span>pendientes</h5>
                </div>

          </div>
    
  </div>



    <div id="contenido">

      <div id='tabs'>

        <ul>
          <li><a href="#tab1">1. <span class='titulo-tab'>General</span></a></li>
          <li><a href="#tab2">2. <span class='titulo-tab'>Gráfico</span></a></li>
        </ul>


        <div id='tab1'>
          <div id="div-tabla">
              <!--<table id='tabla-general'>
                <thead>
                  <tr class='cabecera'><td>Sol. Interna</td><td>Sol. Externa</td><td>Orden</td><td>Proceso</td><td>Contratista</td><td>Fecha Asig.</td><td>Fecha Aten.</td><td>Días Aten.</td><td>Fech. Env. Codensa</td><td>Días. Env. Codensa</td><td>Fech. Cargue Gom</td><td>Dias. Cargue Gom</td><td>Fech. Cargue Mercurio</td><td>Días. Cargue Mercurio</td><td>Cliente</td><td>Dirección</td><td>Departamento</td><td>Municipio</td><td>Localidad</td><td>Barrio</td><td>Resultado</td><td>Anomalía</td><td>Técnico</td></tr>
                </thead>  

                <tbody>
                  
                </tbody>
              </table>-->
          </div>
        </div>


      <div id='tab2'>
        <div id='div-grafico-tecnico'>
          <div id='grafico-tecnico'></div>
        </div>
      </div>

    </div>
         

    </div>

  </body>
</html>