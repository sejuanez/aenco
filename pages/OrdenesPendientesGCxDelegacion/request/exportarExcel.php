<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	//header("Content-Disposition: filename=consumo_".$_GET["tab"]."_".$_GET["tab"].".xls");
  //header("Content-Disposition: filename=consumo_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  

	include("../../../init/gestion.php");

  $bodega = $_GET['bodega'];
	$nombreBodega = $_GET['nombreBodega'];

  if($bodega === "00"){//todas las bodegas


    header("Content-Disposition: filename=OrdenesPendientes.xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $consulta = "SELECT bs.bo_codigomat CodMaterial, coalesce(M.ma_descripcion, 'Sin definir') NombreMaterial, sum(bs.bo_existencia) Existencia, coalesce(m.ma_vr_unitario,0) VrUnitario, cast(coalesce((m.ma_vr_unitario*sum(bs.bo_existencia)),0) as numeric(15,2)) Vr_total FROM bodegasuper BS INNER JOIN TECNICOS T ON T.te_codigo=BS.bo_codigotec left JOIN MATERIALES M ON M.ma_codigo=BS.bo_codigomat WHERE T.te_tipo='B' group by bs.bo_codigomat, M.ma_descripcion, m.ma_vr_unitario";

    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Bodega</td><td>Cod. Material</td><td>Nombre Material</td><td>Existencia</td><td>Vlr. Unitario</td><td>Vlr. Total</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>Todas".//Bodega
          "<td>".utf8_encode($fila[0]).//codMaterial
          "</td><td>".utf8_encode($fila[1]).//nombreMaterial
          "</td>"."<td>".utf8_encode($fila[2]).//Existencia
          "</td>"."<td>".utf8_encode($fila[3]).//VlrUnitario
          "</td>"."<td>".utf8_encode($fila[4]).//VlrTotal
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    
    echo $tabla;

  }else{

    
    $consulta = "select  ID, UNICOM, NIC, NIS, DEPTO, MUNICIPIO, CORREGIMIENTO, BARRIO, TIPO_VIA, NOM_CALLE, DUPLICADOR,  NRO_PUERTA, CGV, REF_DIR, CLIENTE, TELEFONO, TARIFA, ESTADO_SUM, RUTA, ITIN, AOL, MEDIDOR, TIPO_APA, MARCA_MED, DEUDA_ENERGIA, DEUDA_TERCEROS, IMPORTE_ACORDADO, FACT_VENC, FACT_ACO, TECNICO, VALOR_PAGO, PUNTO_PAGO, CEDULA, TELEFONO_ACTUAL, COD, RECAUDO, LECTURA, OBSERVACION, CAMPANA, F_ENTREGA, PERIODO, PLAN_COBRO, CONTRATISTA, DELEGACION from gestion_cobro_entrega gc where (gc.acta is null or (gc.acta='')) and (gc.tecnico is null or (gc.tecnico='')) and gc.delegacion='".$bodega."' ";


    $result = ibase_query($conexion,$consulta);

    header("Content-Disposition: filename=OrdenesPendientes_".$nombreBodega.".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $tabla = "<table>"."<tr class='cabecera'><td>ORDEN</td><td>UNICOM</td><td>NIC</td><td>NIS</td><td>DEPTO</td><td>MUNICIPIO</td><td>CORREGIMIENTO</td><td>BARRIO</td><td>TIPO_VIA</td><td>NOM_CALLE</td><td>DUPLICADOR</td><td>NRO_PUERTA</td><td>CGV</td><td>REF_DIR</td><td>CLIENTE</td><td>TELEFONO</td><td>TARIFA</td><td>ESTADO_SUM</td><td>RUTA</td><td>ITIN</td><td>AOL</td><td>MEDIDOR</td><td>TIPO_APA</td><td>MARCA_MED</td><td>DEUDA_ENERGIA</td><td>DEUDA_TERCEROS</td><td>IMPORTE_ACORDADO</td><td>FACT_VENC</td><td>FACT_ACO</td><td>TECNICO</td><td>VALOR_PAGO</td><td>PUNTO_PAGO</td><td>CEDULA</td><td>TELEFONO_ACTUAL</td><td>COD</td><td>RECAUDO</td><td>LECTURA</td><td>OBSERVACION</td><td>CAMPANA</td><td>F_ENTREGA</td><td>PERIODO</td><td>PLAN_COBRO</td><td>CONTRATISTA</td><td>DELEGACION</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//ORDEN
          "<td>".utf8_encode($fila[1]).//UNICOM
          "</td><td>".utf8_encode($fila[2]).//NIC
          "</td>"."<td>".utf8_encode($fila[3]).//NIS
          "</td>"."<td>".utf8_encode($fila[4]).//DPTO
          "</td>"."<td>".utf8_encode($fila[5]).//MUNICIPIO
          "</td>"."<td>".utf8_encode($fila[6]).//CORREGIMIENTO
          "</td>"."<td>".utf8_encode($fila[7]).//BARRIO
          "</td>"."<td>".utf8_encode($fila[8]).//TIPO_VIA
          "</td>"."<td>".utf8_encode($fila[9]).//NOM_CALLE
          "</td>"."<td>".utf8_encode($fila[10]).//DUPLICADOR
          "</td>"."<td>".utf8_encode($fila[11]).//NRO_PUERTA
          "</td>"."<td>".utf8_encode($fila[12]).//CGV
          "</td>"."<td>".utf8_encode($fila[13]).//REF_DIR
          "</td>"."<td>".utf8_encode($fila[14]).//CLIENTE
          "</td>"."<td>".utf8_encode($fila[15]).//TELEFONO
          "</td>"."<td>".utf8_encode($fila[16]).//TARIFA
          "</td>"."<td>".utf8_encode($fila[17]).//ESTADO_SUM
          "</td>"."<td>".utf8_encode($fila[18]).//RUTA
          "</td>"."<td>".utf8_encode($fila[19]).//ITIN
          "</td>"."<td>".utf8_encode($fila[20]).//AOL
          "</td>"."<td>".utf8_encode($fila[21]).//MEDIDOR
          "</td>"."<td>".utf8_encode($fila[22]).//TIPO_APA
          "</td>"."<td>".utf8_encode($fila[23]).//MARCA_MED
          "</td>"."<td>".utf8_encode($fila[24]).//DEUDA_ENERGIA
          "</td>"."<td>".utf8_encode($fila[25]).//DEUDA_TERCEROS
          "</td>"."<td>".utf8_encode($fila[26]).//IMPORTE_ACORDAD
          "</td>"."<td>".utf8_encode($fila[27]).//FACT_VENC
          "</td>"."<td>".utf8_encode($fila[28]).//FACT_ACO
          "</td>"."<td>".utf8_encode($fila[29]).//TECNICO
          "</td>"."<td>".utf8_encode($fila[30]).//VALOR_PAGO
          "</td>"."<td>".utf8_encode($fila[31]).//PUNTO_PAGO
          "</td>"."<td>".utf8_encode($fila[32]).//CEDULA
          "</td>"."<td>".utf8_encode($fila[33]).//TELEFONO_ACTUAL
          "</td>"."<td>".utf8_encode($fila[34]).//COD
          "</td>"."<td>".utf8_encode($fila[35]).//RECAUDO
          "</td>"."<td>".utf8_encode($fila[36]).//LECTURA
          "</td>"."<td>".utf8_encode($fila[37]).//OBSERVACION
          "</td>"."<td>".utf8_encode($fila[38]).//CAMPANA
          "</td>"."<td>".utf8_encode($fila[39]).//F_ENTREGA
          "</td>"."<td>".utf8_encode($fila[40]).//PERIODO
          "</td>"."<td>".utf8_encode($fila[41]).//PLAN_COBRO
          "</td>"."<td>".utf8_encode($fila[42]).//CONTRATISTA
          "</td>"."<td>".utf8_encode($fila[43]).//DELEGACION
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
  
   		
	
?>