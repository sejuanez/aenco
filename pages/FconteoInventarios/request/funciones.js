var invalido=0;
var vacio=true;

$(document).ready(function(){
  $.ajax({

      url:'request/get_numero_inventario.php',
      dataType:'json'

  }).done(function(respuesta){
  	$.each(respuesta,function(index,valor){
  		
  		$("#select_consecutivo").append("<option value='"+valor.consecutivo+"'>"+valor.consecutivo+" - "+	valor.fecha_inicial+" a "+valor.fecha_final+"</option>");	
  	});
  	    
  });

  $.ajax({

      url:'request/get_funcionario.php',
      dataType:'json'

  }).done(function(respuesta){
  	$.each(respuesta,function(index,valor){
  		
  		$("#select_funcionario").append("<option value='"+valor.codigo+"'>"+valor.nombre+"</option>");	
  	});
  	    
  });  
});

function toggleAlert(){
    $(".alert").fadeIn(); 
    
}
function closeAlert(){
    $(".alert").fadeOut(); 
    
}

$("#alerta").on("click", toggleAlert);
$("#close_alert").on("click", closeAlert);

$(document).ready(function(){
  $("#hide").click(function(){
    $("#tabInventario").hide();
  });
  $("#consultar").click(function(){
    $("#tabInventario").show();
  });
});

$("#exportar_excel").click(function(event){
	if(!vacio){
		$("#accion").val("exportar_excel");
		$("#formulario").submit();
		return;
	}

	mensaje="No hay datos para exportar!"
  	$("#mensaje_altera").html(mensaje);
  	toggleAlert();
	
});

$("#exportar_pdf").click(function(event){
	if(!vacio){
		$("#accion").val("exportar_pdf");
		$("#formulario").submit();
		return;
	}

	mensaje="No hay datos para exportar!"
  	$("#mensaje_altera").html(mensaje);
  	toggleAlert();
	
});


$("#btn_consultar").click(function(){
		invalido=0;
	    $("#accion").val("consultar");
		mensaje="<ul style='padding-left: 20px'>";
		$("#cuerpo_tabla").html("");
	
		if($("#select_consecutivo")[0].selectedIndex<1){
			mensaje+="<li>Seleccine <b>Consecutivo</b></strong></li>";
			invalido++;
		}

		if($("#select_funcionario")[0].selectedIndex<1){
			mensaje+="<li>Seleccine <b>Técnico</b></strong></li>";
			invalido++;
		}

		if(invalido==0){
			$.ajax({
			    // la URL para la petición
			    url : 'request/getconteo.php',
			 
			    // la información a enviar
			    // (también es posible utilizar una cadena de datos)
			    data : $("#formulario").serialize(),
			 
			    // especifica si será una petición POST o GET
			    type : 'POST',
			 
			    
			    // código a ejecutar si la petición es satisfactoria;
			    // la respuesta es pasada como argumento a la función
			    success : function(json) {

			    	if($.trim(json).length==0){
			    		vacio=true;
			    		return;
			    	}
			    	
			    	vacio=false;

			        $('#cuerpo_tabla')
			            .html(json);
			    },
			 
			    // código a ejecutar si la petición falla;
			    // son pasados como argumentos a la función
			    // el objeto de la petición en crudo y código de estatus de la petición
			    error : function(xhr, status) {
			        alert('Disculpe, existió un problema');
			    },
			 
			    // código a ejecutar sin importar si la petición falló o no
			    complete : function(xhr, status) {
			        //alert('Petición realizada');
			    }
			});
		}else{
			mensaje+="</ul>"
		  	$("#mensaje_altera").html(mensaje);
		  	toggleAlert();
		}
		
	
});

$('#select_funcionario').select2({
	theme: "bootstrap"
});