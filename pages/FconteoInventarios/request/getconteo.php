<?php
include('../../../init/gestion.php');
include('./exportar.php');
//require('print.php');


	$consulta = "select c.numeroinventario,
       i.fecha_inicial del,
       i.fecha_final al,
       c.codtecnico codtecnico,
       t.te_nombres nombre_tecnico,
       t.te_cuadrilla cuadrilla,
       c.codmaterial codmaterial,
       m.ma_descripcion nombre_material,
       u.um_nombre unidad_medida,
       c.conteo cantidad,
       c.fecha_conteo fecha_conteo,
       c.hora_conteo  Hora_conteo,
       c.usu_sis usuario_cuenta,
       ua.nombre contado_por,
       t.te_cedula cedula
from conteos c
  left join inventarios i on i.consecutivo=c.numeroinventario
  left join materiales m on m.ma_codigo=c.codmaterial
  left join tecnicos t on t.te_codigo=c.codtecnico
  left join unidad_medida u on u.um_codigo=m.ma_unidad
  left join usuarios ua on ua.usuario=c.usu_sis
where c.numeroinventario=$_POST[consecutivo] and c.codtecnico=$_POST[tecnico]";
	
	//var_dump($consulta);

	$return_arr = array(); 
	$result = ibase_query($conexion,$consulta);

	if($_POST['accion']=='consultar'){
		while($fila = ibase_fetch_row($result)){
		echo "
			<tr class='fila'>
				<td>".$row_array['numeroinventario'] = utf8_encode($fila[0])."</td>
				<td>".$row_array['del'] = utf8_encode($fila[1])."</td>
				<td>".$row_array['al'] = utf8_encode($fila[2])."</td>
				<td>".$row_array['codtecnico'] = utf8_encode($fila[3])."</td>
				<td>".$row_array['cuadrilla'] = utf8_encode($fila[4])."</td>
				<td>".$row_array['codmaterial'] = utf8_encode($fila[5])."</td>
				<td>".$row_array['nombre_material'] = utf8_encode($fila[6])."</td>
				<td>".$row_array['unidad_medida'] = utf8_encode($fila[7])."</td>
				<td>".$row_array['cantidad'] = utf8_encode($fila[8])."</td>
				<td>".$row_array['fecha_conteo'] = utf8_encode($fila[9])."</td>
				<td>".$row_array['hora_conteo'] = utf8_encode($fila[10])."</td>
				<td>".$row_array['hora_conteo'] = utf8_encode($fila[11])."</td>
				<td>".$row_array['usuario_cuenta'] = utf8_encode($fila[12])."</td>
				<td>".$row_array['contado_por'] =  utf8_encode($fila[13])."</td>
			</tr>";

		array_push($return_arr, $row_array);
		}
	}

	if($_POST['accion']=='exportar_excel'){
		while($rows = ibase_fetch_assoc($result)){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $rows['NUMEROINVENTARIO']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $rows['DEL']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $rows['AL']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $rows['CODTECNICO']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, utf8_encode($rows['NOMBRE_TECNICO']));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $rows['CUADRILLA']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $rows['CODMATERIAL']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, utf8_encode($rows['NOMBRE_MATERIAL']));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $rows['UNIDAD_MEDIDA']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $rows['CANTIDAD']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $rows['FECHA_CONTEO']);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $rows['HORA_CONTEO']);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $rows['USUARIO_CUENTA']);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, utf8_encode($rows['CONTADO_POR']));
			$fila++; //Sumamos 1 para pasar a la siguiente fila
		}

		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename="ConteoInventario.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 /* Obtenemos los caracteres adicionales o mensajes de advertencia y los
         guardamos en el archivo "depuracion.txt" si tenemos permisos */
        file_put_contents('depuracion.txt', ob_get_contents());
        /* Limpiamos el búfer */
        ob_end_clean();
		$objWriter->save('php://output');
	}

	

	if($_POST['accion']=='exportar_pdf'){

		$data=array();
		$nombre;
		$cuadrilla;
		$del;
		$al;
		$cedula;
		while($fila=ibase_fetch_row($result)){
			
			$data[]=$fila;
			$del = $fila[1];
			$al = $fila[2];
			$nombre = $fila[4];
			$cuadrilla = $fila[5];
			$cedula = $fila[14];
		}

		$pdf = new PDF();
		// Títulos de las columnas
		$header = array('CODIGO', 'DESCRIPCION', 'UNIDAD', 'CANTIDAD','FECHA CONTEO','HORA CONTEO');
		// Carga de datos
		
		$pdf->SetFont('helvetica','',9);
		$pdf->AliasNbPages();
		$pdf->AddPage();
        $pdf->SetFont('helvetica','',9);
        $pdf->Cell(100);
        $pdf->Cell(89, 4,'INFORME DE CONTEO DE INVENTARIO',0,1);
        $pdf->Cell(112);
        $pdf->Cell(89, 4,'INVENTARIO NRO.'.$_POST['consecutivo'] ,0,1);
        $pdf->Cell(100, 4, 'FUNCIONARIO: '.$_POST['tecnico'].' '.$nombre,0,0);
        $pdf->Cell(6);
        $pdf->Cell(10, 4,'DEL '.$del.' '.'AL '.$al,0,1);
        $pdf->Cell(80, 4, 'CUADRILLA: '.$cuadrilla,0,0);
        $pdf->Ln(10);

		$pdf->FancyTable($header,$data);

		$pdf->Ln(30);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(66, 5, '________________________________',0,0);
        $pdf->Cell(63, 5, '________________________' ,0,0);
        $pdf->Cell(63, 5,'__________________________',0,0);
        $pdf->Ln(4);
		$pdf->Cell(66, 5, 'FUNCIONARIO',0,0);
        $pdf->Cell(63, 5, 'SUPERVISOR' ,0,0);
        $pdf->Cell(63, 5,'INTERVENTOR',0,0);
        $pdf->Ln(3);
        $pdf->SetFont('helvetica','',8);
		$pdf->Cell(66, 5, 'NOMBRE: '.$nombre,0,0);
        $pdf->Cell(63, 5, 'NOMBRE: ' ,0,0);
        $pdf->Cell(63, 5, 'NOMBRE: ',0,0);
        $pdf->Ln(3);
        $pdf->Cell(66, 5, 'CEDULA: '.$cedula,0,0);
        $pdf->Cell(63, 5, 'CEDULA: ' ,0,0);
        $pdf->Cell(63, 5, 'CEDULA: ',0,0);
		$pdf->Output();
	}

?>