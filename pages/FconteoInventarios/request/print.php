
<?php
include('../../../init/gestion.php');
require('./fpdf/fpdf.php');


class PDF extends FPDF
{
    function Header()
    {
        // Logo
        $this->Image('../img/logo.png',20,20,50);

        $this->SetFont('helvetica','I',8);
        $this->Cell(155);
        $this->Cell(50,3,'Pag. '.$this->PageNo().'/{nb}',0,1,'C');
        $this->Cell(155);
        $this->Cell(45,4,date('d/m/Y'),0,1,'C');
        $this->Ln(1);
        // Arial bold 15
        $this->SetFont('helvetica','B',14);
        // Movernos a la derecha
        $this->Cell(56);
        // Título
        $this->Cell(145,20,'DISICO S.A.',0,0,'C');

        // Salto de línea
        $this->Ln(12.5);

    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Pag.'.$this->PageNo().'/{nb}',0,0,'C');
    }


    // Tabla coloreada
    function FancyTable($header, $data)
    {
        // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(153,153,153);
        $this->SetTextColor(255);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.2);
        $this->SetFont('','B');
        // Cabecera
        $w = array(18, 70, 24, 20, 30, 28);
        for($i=0;$i<count($header);$i++)
            $this->CellFitSpace($w[$i],7,$header[$i],1,0,'L',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill = false;
        foreach($data as $row)
        {
            $this->CellFitSpace($w[0],7,$row[6],'LR',0,'L',$fill);
            $this->CellFitSpace($w[1],7,$row[7],'LR',0,'L',$fill);
            $this->CellFitSpace($w[2],7,$row[8],'LR',0,'R',$fill);
            $this->CellFitSpace($w[3],7,$row[9],'LR',0,'R',$fill);
            $this->CellFitSpace($w[4],7,$row[10],'LR',0,'R',$fill);
            $this->CellFitSpace($w[5],7,$row[11],'LR',0,'R',$fill);

            $this->Ln();
            $fill = !$fill;
        }

    }

    //***** Aquí comienza código para ajustar texto *************
    //***********************************************************
    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);
 
        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $ratio = ($w-$this->cMargin*2)/$str_width;
 
        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }
 
        //Pass on to Cell method
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);
 
        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }
 
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }
 
    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }

}


?>
