	<!doctype html>
	<html lang="es">
	  <head>
	    <!-- Required meta tags -->
	    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

		<link rel="stylesheet" type="text/css" href="css/estilo.css">

		<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

	  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

		<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
		<link rel='stylesheet' href='css/ionicons/css/ionicons.min.css' type='text/css' />
	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/select2.min.css">
		<link rel="stylesheet" href="css/select2-bootstrap.min.css">
	    <style type="text/css">
	    	.size{
	    		width: 80px !important;
	    	}
	    </style>
	   
	    <title>CONELEC</title>
	  </head>
	  <body>
	  	<header>
			<h3>Conteos de Inventarios</h3>
			<nav>
				<ul id="menu">
				<li><a href="#" target="_self" id="exportar_excel"><span class="ion-ios-download-outline"></span><h6>Exportar Excel</h6></a></li>
				<li><a href="#" target="_self" id="exportar_pdf"><span class="ion-printer"></span><h6>Exportar PDF</h6></a></li>
				<li id="consultar"><a href="#" id="btn_consultar"><span class="ion-ios-search-strong"></span><h6>Consultar</h6></a></li>	
				</ul>
			</nav>
		</header>
		<div id='subheader'>
		<div class="container">
			<form id="formulario" action="./request/getconteo.php" method="POST">
				<div class="row">
					<div class="col-md-3">
						<label>Número de Inventario</label>
						<div class="form-group ">
						  <select class="form-control" id="select_consecutivo" name="consecutivo">
						    <option>Seleccione...</option>
						  </select>
						</div>
					</div>
					<div class="col-md-4">
						<label>Funcionario</label>
						<div class="form-group ">
						  <select class="form-control " id="select_funcionario" name="tecnico">
						    <option >Seleccione...</option>
						  </select>
						</div>
					</div>
				</div>
			<input type="hidden" value="consultar" name="accion" id="accion"/>
			</form>
		</div>
		</div>
	                                                            
		<div class="container-fluid" id="tabInventario" style="display: none;">
		  <div class="row">
			<div class="col-sm-12">
				<div class="table-responsive">
				<table class="table table-bordered table-condensed" id="myTable">
				  <thead>
					<tr style="text-align:center">
					  <th scope="col">No.INVENTARIO</th>
					  <th scope="col" class="size">DEL</th>
					  <th scope="col" class="size">AL</th>
					  <th scope="col">CODTECNICO</th>
					  <th scope="col">TECNICO</th>
					  <th scope="col">CUADRILLA</th>
					  <th scope="col">COD.MATERIAL</th>
					  <th scope="col">MATERIAL</th>
					  <th scope="col">U.MEDIDA</th>
					  <th scope="col">CANTIDAD</th>
					  <th scope="col">FECHA_CONTEO</th>
					  <th scope="col">HORA_CONTEO</th>
					  <th scope="col">USUARIO_CUENTA</th>
					  <th scope="col">CONTADO_POR</th>
					</tr>
				  </thead>
				  <tbody id="cuerpo_tabla"></tbody>
				</table>
			</div>
			</div>
			<br>
		  </div>
		</div>


		<div class="container">
				<div class="alert alert-danger" style="display:none" id="bsalert">
			  		<a href="#" class="close" id="close_alert"  aria-label="close">&times;</a>
			   		<div id="mensaje_altera">Mensaje</div>
				</div>
		</div>

	    <script src="js/jquery.min.js"></script>
	    <!-- Boostrap JavaScript -->
	    <script src="js/bootstrap.min.js"></script>
		<script src="js/select2.min.js"></script>
		<script src="request/funciones.js"></script>
		
	  </body>
	</html>