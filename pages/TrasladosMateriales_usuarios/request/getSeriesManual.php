<?php
include("gestion.php");

$data = $_POST['data'];
$prefijo = addslashes(htmlspecialchars(strtoupper($data['prefijo'])));
$inicioSerie = addslashes(htmlspecialchars(strtoupper($data['inicioSerie'])));
$finSerie = addslashes(htmlspecialchars(strtoupper($data['finSerie'])));
$sufijo = addslashes(htmlspecialchars(strtoupper($data['sufijo'])));
$cod_grupo = addslashes(htmlspecialchars(strtoupper($data["cod_grupo"])));
$marca = addslashes(htmlspecialchars(strtoupper($data["marca"])));
$material = addslashes(htmlspecialchars(strtoupper($data["material"])));
$serie = addslashes(htmlspecialchars(strtoupper($data["serie"])));
$bodOrigen = $data["tecnico"];


if ($cod_grupo==='99') {

    $return_arr1 = array();
    $return_arr2 = array();
        $sql = "SELECT BS.AS_CODMATER CODMATERIAL, BS.AS_DESMATER MATERIAL, BS.AS_GRUPO CODGRUPO, G.GR_DESCRIPCION NOMGRUPO,
                    BS.AS_MARCA CODMARCA, DS.DESCRIPCION NOMMARCA, BS.AS_SERIE SERIE, BS.AS_ACTALEGA LEGALIZADO, BS.AS_CODTECNICO CODSUPER,
                    BS.AS_NOMTECNICO ASIGNADO, BS.AS_CODPROVE CODPROVEEDOR, BS.AS_NOMPROVE NOMPROVEEDOR,
                    CAST(NULL AS VARCHAR (5)) CODPROPIETARIO, CAST(NULL AS VARCHAR (5)) PROPIETARIO
                FROM ACTAS_SERIES BS
                LEFT JOIN GRUPOS G ON G.GR_CODIGO = BS.AS_GRUPO
                LEFT JOIN DESCRIPCION_SERIES DS ON DS.GRUPO = BS.AS_GRUPO AND DS.CODIGO = BS.AS_MARCA
                WHERE BS.AS_CODMATER = '".$material."' AND
                      BS.AS_GRUPO = '".$cod_grupo."' AND
                      BS.AS_MARCA = '".$marca."' AND
                      BS.AS_SERIE = '".$serie."'";

        $result = ibase_query($conexion, $sql);
        while($fila = ibase_fetch_row($result)){
            $row_array['CODMATERIAL'] = utf8_encode($fila[0]);
            $row_array['MATERIAL'] = utf8_encode($fila[1]);
            $row_array['CODGRUPO'] = utf8_encode($fila[2]);
            $row_array['NOMGRUPO'] = utf8_encode($fila[3]);
            $row_array['CODMARCA'] = utf8_encode($fila[4]);
            $row_array['NOMMARCA'] = utf8_encode($fila[5]);
            $row_array['SERIE'] = utf8_encode($fila[6]);
            $row_array['LEGALIZADO'] = utf8_encode($fila[7]);
            $row_array['CODSUPER'] = utf8_encode($fila[8]);
            $row_array['ASIGNADO'] = utf8_encode($fila[9]);
            $row_array['CODPROVEEDOR'] = utf8_encode($fila[10]);
            $row_array['NOMPROVEEDOR'] = utf8_encode($fila[11]);
            $row_array['CODPROPIETARIO'] = utf8_encode($fila[12]);
            $row_array['PROPIETARIO'] = utf8_encode($fila[13]);

            if ($row_array['LEGALIZADO']==='' && $row_array['ASIGNADO'] != '' && $row_array['CODSUPER'] == $bodOrigen) {
              array_push($return_arr1, $row_array);
            }else
              array_push($return_arr2, $row_array);
          }

    $return_arrAll = array("limpias" => $return_arr1, "asignadas"=> $return_arr2);
    echo json_encode($return_arrAll);

}else{

    $return_arr1 = array();
    $return_arr2 = array();
        $sql = "SELECT BS.CODIGOMAT CODMATERIAL, BS.NOMBRE_MAT MATERIAL, BS.GRUPO CODGRUPO, G.GR_DESCRIPCION NOMGRUPO,
                  BS.MARCA CODMARCA, DS.DESCRIPCION NOMMARCA, BS.SERIE SERIE, BS.ACTALEGA LEGALIZADO, BS.SUPER CODSUPER,
                  BS.NOMBRE_SUPER ASIGNADO, BS.PROVEEDOR CODPROVEEDOR, BS.NOMBRE_PROV NOMPROVEEDOR,
                  BS.PROPIEDAD CODPROPIETARIO, P.PRO_DESCRIPCION PROPIETARIO
                FROM BODEGA_SERIES BS
                LEFT JOIN GRUPOS G ON G.GR_CODIGO = BS.GRUPO
                LEFT JOIN DESCRIPCION_SERIES DS ON DS.GRUPO = BS.GRUPO AND DS.CODIGO = BS.MARCA
                LEFT JOIN PROPIEDAD P ON P.PRO_CODIGO = BS.PROPIEDAD
                WHERE BS.CODIGOMAT = '".$material."' AND
                      BS.GRUPO = '".$cod_grupo."' AND
                      BS.MARCA = '".$marca."' AND
                      BS.SERIE = '".$serie."'";

        $result = ibase_query($conexion, $sql);
        while($fila = ibase_fetch_row($result)){
            $row_array['CODMATERIAL'] = utf8_encode($fila[0]);
            $row_array['MATERIAL'] = utf8_encode($fila[1]);
            $row_array['CODGRUPO'] = utf8_encode($fila[2]);
            $row_array['NOMGRUPO'] = utf8_encode($fila[3]);
            $row_array['CODMARCA'] = utf8_encode($fila[4]);
            $row_array['NOMMARCA'] = utf8_encode($fila[5]);
            $row_array['SERIE'] = utf8_encode($fila[6]);
            $row_array['LEGALIZADO'] = utf8_encode($fila[7]);
            $row_array['CODSUPER'] = utf8_encode($fila[8]);
            $row_array['ASIGNADO'] = utf8_encode($fila[9]);
            $row_array['CODPROVEEDOR'] = utf8_encode($fila[10]);
            $row_array['NOMPROVEEDOR'] = utf8_encode($fila[11]);
            $row_array['CODPROPIETARIO'] = utf8_encode($fila[12]);
            $row_array['PROPIETARIO'] = utf8_encode($fila[13]);

            if ($row_array['LEGALIZADO']==='' && $row_array['ASIGNADO'] != '' && $row_array['CODSUPER'] == $bodOrigen) {
              array_push($return_arr1, $row_array);
            }else{
              array_push($return_arr2, $row_array);
            }
        }

    $return_arrAll = array("limpias" => $return_arr1, "asignadas"=> $return_arr2);
    echo json_encode($return_arrAll);
}


?>