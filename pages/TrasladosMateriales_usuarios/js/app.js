// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });
    $('#fecha').val(app.fechaEntrega);

    $('#modalTecnico').on('shown.bs.modal', function () {
        $('#buscarTecnico').trigger('focus')
    })

    $('#modalTecnico').on('hidden.bs.modal', function () {
        // $('#myInput').trigger('focus')
        app.openModal = "";
    })

    $('.noSigno').keydown(function (e) {
        if (e.keyCode == 109 || e.keyCode == 107) {
            return false;
        }
    });

});


let app = new Vue({
    el: '#app',
    data: {
        codTecnOri: "",
        nomTecnOri: "",
        cuadrillaOri: "",
        codSuperOri: "",
        superOri: "",
        tecnsOri: "",

        usuariologueado: "",

        codTecnDes: "",
        nomTecnDes: "",
        cuadrillaDes: "",
        codSuperDes: "",
        superDes: "",
        tecnsDes: "",

        buscarTecnico: "",
        tablaTecnicos: [],
        openModal: "",

        numeroEntrega: "",
        fechaEntrega: "",

        buscarMaterial: "",
        idMaterial: "",
        material: "",
        hasSerie: "",
        existencia: "",
        tablaMateriales: [],

        buscarPropiedad: "",
        idPropiedad: "",
        propiedad: "",
        tablaPropiedades: [],

        buscarMarca: "",
        idMarca: "",
        marca: "",
        tablaMarcas: [],

        buscarTipoMaterial: "",
        idTipoMaterial: "",
        tipoMaterial: "",
        tablaTipoMateriales: [],
        grupo: '',
        cantidad: "",
        vlrUnitario: "",
        serie: "",
        serieManual: "",
        nDoc: "",
        concepto: "",


        picked: "Manual",
        nactivo: "",
        prefijo: "",
        inicioSerie: "",
        finSerie: "",
        sufijo: "",
        cod_grupo: "",
        paquete: "",


        btnAnadir: false,
        idPlaca: "",
        loadingSerie: true,
        loadingGuadar: true,

        fechaFactura: "",
        danger: true,

        tablaDetalle: [],
        tablaSeries: [],
        serieExiste: [],

        empresa: [],

    },
    methods: {
        getTecnicos: function () {
            var app = this;

            $.post('./request/getTecnicos.php', {
                PROVEEDOR: app.buscarTecnico,
                usuario: app.usuariologueado
            }, function (data) {
                var data = JSON.parse(data);
                app.tablaTecnicos = data;
            });

        },

        loadUsuarioSistema: function () {
            var app = this;
            $.get('./request/getUsuarioSistema.php', function (data) {

                console.log(data)
                app.usuariologueado = data

            });
        },

        getEmpresa: function () {
            var app = this;

            $.get('./request/getEmpresa.php', function (data) {
                var data = JSON.parse(data);
                app.empresa = data;
                console.log(app.empresa[0].NOMBRE)
            });

        },

        openModalMateriales: function () {

            var app = this;

            if (!(app.codTecnOri == "")) {
                $('#modalMateriales').modal('show');
                app.getMateriales();
            } else {
                alertify.warning('Seleccione la Bodega Origen')
            }
        },

        openModalTecnicos: function (data) {
            $('#modalTecnico').modal('show');
            this.getTecnicos();
            this.openModal = data;
        },
        seleccionarTecnico: function (data) {
            if (this.openModal === 'ORIGEN') {
                this.codTecnOri = data.CODTECNICO;
                if (this.codTecnOri !== this.codTecnDes) {
                    this.codTecnOri = data.CODTECNICO;
                    this.nomTecnOri = data.NOMTECNICO;
                    this.cuadrillaOri = data.CUADRILLA;
                    this.codSuperOri = data.CODSUPERVISOR;
                    this.superOri = data.NOMSUPERVISOR;
                    this.tecnsOri = data.TECNICOS;
                    $('#modalTecnico').modal('hide');
                    this.buscarTecnico = "";
                    this.tablaTecnicos = "";
                } else {
                    this.codTecnOri = '';
                    alertify.alert('Error', 'La bodega origen no puedes ser igual a la bodega destino');
                }
            } else {
                this.codTecnDes = data.CODTECNICO;
                if (this.codTecnDes !== this.codTecnOri) {
                    this.nomTecnDes = data.NOMTECNICO;
                    this.cuadrillaDes = data.CUADRILLA;
                    this.codSuperDes = data.CODSUPERVISOR;
                    this.superDes = data.NOMSUPERVISOR;
                    this.tecnsDes = data.TECNICOS;
                    $('#modalTecnico').modal('hide');
                    this.buscarTecnico = "";
                    this.tablaTecnicos = "";
                } else {
                    this.codTecnDes = '';
                    alertify.alert('Error', 'La bodega destino no puedes ser igual a la bodega origen');
                }
            }
        },

        getMateriales: function () {
            var app = this;
            $.post('./request/getMateriales.php', {
                codTecnico: app.codTecnOri,
                material: app.buscarMaterial
            }, function (data) {
                // console.log(data);
                var data = JSON.parse(data);
                app.tablaMateriales = data;
            });
        },

        seleccionarMaterial: function (data) {
            this.idMaterial = data.CODIGO;
            this.material = data.DESCRIPCION;
            this.existencia = data.EXISTENCIA;
            this.serie = data.SERIADO;
            this.cod_grupo = data.COD_GRUPO;
            $('#modalMateriales').modal('hide');
            this.buscarMaterial = "";
        },

        getPropiedades: function () {
            var app = this;
            $.post('./request/getPropiedades.php', {propiedad: app.buscarPropiedad}, function (data) {
                var data = JSON.parse(data);
                app.tablaPropiedades = data;
            });
        },

        seleccionarPropiedad: function (data) {
            this.idPropiedad = data.CODIGO;
            this.propiedad = data.DESCRIPCION;
            $('#modalPropiedades').modal('hide');
            this.buscarPropiedad = "";
        },

        getMarcas: function () {
            var app = this;
            $.post('./request/getMarcas.php', {marca: app.buscarMarca, grupo: app.cod_grupo}, function (data) {
                var data = JSON.parse(data);
                app.tablaMarcas = data;
            });
        },

        seleccionarMarca: function (data) {
            this.idMarca = data.CODIGO;
            this.marca = data.DESCRIPCION;
            $('#modalMarcas').modal('hide');
            this.buscarMarca = "";
        },

        getTipoMateriales: function () {
            var app = this;
            $.post('./request/getTipoMateriales.php', {tipoMaterial: app.buscarTipoMaterial}, function (data) {
                var data = JSON.parse(data);
                app.tablaTipoMateriales = data;
            });
        },

        seleccionarTipoMaterial: function (data) {
            this.idTipoMaterial = data.CODIGO;
            this.tipoMaterial = data.DESCRIPCION;
            this.grupo = data.GRUPO;
            this.serie = data.SERIE;
            $('#modalTipoMateriales').modal('hide');
            this.buscarTipoMaterial = "";
        },

        fechaActual: function () {
            var dt = new Date();
            var day = dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate();
            var month = (dt.getMonth() + 1) < 10 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1);
            var year = dt.getFullYear();
            this.fechaEntrega = year + '-' + month + '-' + day;
        },

        addItem: function () {
            var app = this;
            if (this.serie != 'S') {

                if (this.validaForm('#formMateriales')) {
                    if (this.tablaDetalle.length > 0) {
                        var arrDetalleExiste = []
                        for (var i = 0; i < this.tablaDetalle.length; i++) {
                            arrDetalleExiste.push(this.tablaDetalle[i].codigo);
                        }
                        if (arrDetalleExiste.includes(this.idMaterial)) {
                            alertify.alert("Aleta!", "El material ya fue ingresado", function () {
                                app.idMaterial = "";
                                app.material = "";
                                app.cantidad = "";
                                app.existencia = "";
                            });
                        } else {
                            this.tablaDetalle.push({
                                'cod_grupo': this.cod_grupo,
                                'codigo': this.idMaterial,
                                'descripcion': this.material,
                                'serie': this.serie,
                                'cantidad': this.cantidad,
                                'existencia': this.existencia,
                                'arraySeries': []
                            });
                            app.idMaterial = "";
                            app.material = "";
                            app.cantidad = "";
                            app.existencia = "";
                        }

                    } else {
                        this.tablaDetalle.push({
                            'cod_grupo': this.cod_grupo,
                            'codigo': this.idMaterial,
                            'descripcion': this.material,
                            'serie': this.serie,
                            'cantidad': this.cantidad,
                            'existencia': this.existencia,
                            'arraySeries': []
                        });
                        this.idMaterial = "";
                        this.material = "";
                        this.cantidad = "";
                        this.existencia = "";
                    }
                }

            } else {

                if (this.validaForm('#formMateriales')) {
                    if (this.tablaDetalle.length > 0) {
                        var arrDetalleExiste = []
                        for (var i = 0; i < this.tablaDetalle.length; i++) {
                            arrDetalleExiste.push(this.tablaDetalle[i].codigo);
                        }
                        if (arrDetalleExiste.includes(this.idMaterial)) {
                            alertify.alert("Aleta!", "El material ya fue ingresado", function () {
                                app.idMaterial = "";
                                app.material = "";
                                app.cantidad = "";
                                app.existencia = "";
                            });
                        } else {
                            this.hasSerie = true;
                            this.getMarcas();
                        }
                    } else {
                        this.hasSerie = true;
                        this.getMarcas();
                    }

                }
            }
        },

        deleteItemDetalle: function (index) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaDetalle.splice(index, 1);
                },
                function () {
                });
        },

        addRangoSerie: function () {

            var app = this;

            var formValid = false;

            if (app.picked == 'Manual') {

                app.marca == "" || app.serieManual == "" ? formValid = false : formValid = true;

            } else {

                console.log(app.inicioSerie)
                console.log(app.sufijo)
                console.log(app.finSerie)

                app.marca == "" || app.inicioSerie == "" ||
                app.finSerie == "" ? formValid = false : formValid = true;

            }


            if (formValid) {
                // app.loadingSerie = false;
                $('#form_serie').removeClass('was-validated');
                var datos = {
                    cod_grupo: app.cod_grupo,
                    prefijo: app.prefijo.toUpperCase(),
                    inicioSerie: app.inicioSerie,
                    finSerie: app.finSerie,
                    sufijo: app.sufijo.toUpperCase(),
                    marca: app.idMarca,
                    material: app.idMaterial,
                    serie: app.serieManual,
                    tecnico: app.codTecnOri
                };
                // console.log(datos);
                app.picked === "Auto" ? app.addSerieAuto(datos) : app.addSerieManual(datos);
            } else {
                $('#form_serie').addClass('was-validated');
                if (app.marca == "") {
                    alertify.error('Seleccione una marca')
                } else {
                    alertify.error('Campos vacios')
                }
            }
        },

        addSerieAuto: function (array) {
            var app = this;
            $.ajax({
                url: './request/getSeries.php',
                type: 'POST',
                // dataType: 'json',
                data: {data: array}
            })
                .done(function (data) {
                    // console.log(data);
                    var datos = JSON.parse(data);
                    // app.serieExiste = datos;
                    if (datos.limpias.length > 0) {
                        for (var i = 0; i < datos.limpias.length; i++) {
                            app.tablaSeries.push(datos.limpias[i]);
                        }
                    } else {
                        alertify.alert("Alerta", "No existen series en el rango establecido!");
                    }

                    if (datos.asignadas.length > 0) {
                        alertify.alert("Alerta", "Algunas series estan legalizadas o asignadas a otro tecnico!");
                        for (var i = 0; i < datos.asignadas.length; i++) {
                            app.serieExiste.push(datos.asignadas[i]);
                        }
                    }

                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    // console.log("complete");
                    // app.generaSerie();
                    // app.loadingSerie = true;
                });
        },

        addSerieManual: function (array) {

            var app = this;

            $.ajax({
                url: './request/getSeriesManual.php',
                type: 'POST',
                // dataType: 'json',
                data: {data: array},
            })
                .done(function (data) {
                    // console.log(data);
                    var datos = JSON.parse(data);
                    // app.serieExiste = datos;
                    if (datos.limpias.length > 0) {
                        for (var i = 0; i < datos.limpias.length; i++) {
                            app.tablaSeries.push(datos.limpias[i]);
                        }
                    } else {
                        alertify.alert("Alerta", "No existe esta serie!");
                    }

                    if (datos.asignadas.length > 0) {
                        alertify.alert("Alerta", "Algunas series estan legalizadas!")
                        for (var i = 0; i < datos.asignadas.length; i++) {
                            app.serieExiste.push(datos.asignadas[i]);
                        }
                    }
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    app.loadingSerie = true;
                });
        },

        generaSerie: function () {
            var app = this;

            if (app.serieExiste.length > 0) {
                alertify.alert("Existen series", "Hemos encontrado series en la BD!...");

                if (app.tablaSeries.length > 0) {

                    // alertify.alert('Existen Series', 'Existen series generadas en el rango');
                    var doubles = [];
                    for (var i = 0; i < app.tablaSeries.length; i++) {
                        doubles.push(app.tablaSeries[i].serie);
                    }

                    var arrSeriesExiste = [];
                    for (var i = 0; i < app.serieExiste.length; i++) {
                        arrSeriesExiste.push(app.serieExiste[i].SERIE);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        if (app.finSerie.toString().length > app.inicioSerie.toString().length) {
                            var n = app.finSerie.toString().length - i.toString().length;
                            for (var j = 0; j < n; j++) {
                                ceros += '0';
                            }
                        }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!arrSeriesExiste.includes(serieHP) && !doubles.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }
                } else {

                    var arrSeriesExiste = [];
                    for (var i = 0; i < app.serieExiste.length; i++) {
                        arrSeriesExiste.push(app.serieExiste[i].SERIE);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        if (app.finSerie.toString().length > app.inicioSerie.toString().length) {
                            var n = app.finSerie.toString().length - i.toString().length;
                            for (var j = 0; j < n; j++) {
                                ceros += '0';
                            }
                        }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!arrSeriesExiste.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }
                }

            } else {
                if (app.tablaSeries.length > 0) {

                    // alertify.alert('Existen Series', 'Existen series generadas en el rango');
                    var doubles = [];
                    for (var i = 0; i < app.tablaSeries.length; i++) {
                        doubles.push(app.tablaSeries[i].serie);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        if (app.finSerie.toString().length > app.inicioSerie.toString().length) {
                            var n = app.finSerie.toString().length - i.toString().length;
                            for (var j = 0; j < n; j++) {
                                ceros += '0';
                            }
                        }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!doubles.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }


                } else {

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        if (app.finSerie.toString().length > app.inicioSerie.toString().length) {
                            var n = app.finSerie.toString().length - i.toString().length;
                            for (var j = 0; j < n; j++) {
                                ceros += '0';
                            }
                        }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        // if(!arrSeriesExiste.includes(serieHP)){

                        app.tablaSeries.push({
                            'marca': app.marca,
                            'serie': serieHP,
                            'grupo': app.grupo,
                            'cod_grupo': app.cod_grupo,
                            'cod_marca': app.idMarca,
                            'idTipoMaterial': app.idTipoMaterial,
                            'material': app.material,
                            'paquete': app.paquete,
                            'material': app.material,
                            'nactivo': app.nactivo,
                            'idPropiedad': app.idPropiedad,
                            'propiedad': app.propiedad
                        });
                        // }
                    }
                }


            }
        },

        quitarSerie: function (index) {
            var app = this;
            // console.log(index);
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaSeries.splice(index, 1);
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        eliminarRangoSerie: function () {
            var app = this;
            // console.log(index);
            alertify.confirm("Eliminar", ".. Desea eliminar la serie creada?",
                function () {
                    $('#form_serie').removeClass('was-validated');
                    app.serie = "N";
                    app.clearDatosSerie();
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        clearDatosSerie: function () {
            this.tablaSeries = [];
            this.serieExiste = [];
            this.hasSerie = false;
            this.idMaterial = "",
                this.material = "",
                this.idPropiedad = "",
                this.propiedad = "",
                this.idMarca = "",
                this.marca = "",
                this.idTipoMaterial = "",
                this.tipoMaterial = "",
                this.prefijo = "",
                this.inicioSerie = "",
                this.finSerie = "",
                this.sufijo = "",
                this.paquete = "",
                this.cantidad = "",
                this.vlrUnitario = "",
                this.existencia = ""
        },

        cancelarSerie: function () {
            this.tablaSeries = [];
            this.serieExiste = [];
            this.hasSerie = false;
        },

        addRangoSerieDetalle: function () {

            var app = this;

            alertify.confirm("Agregar serie", "... Desea agregar esta serie al detalle ?",

                function () {

                    app.tablaDetalle.push({
                        'cantidad': app.tablaSeries.length,
                        'codigo': app.idMaterial,
                        'cod_grupo': app.cod_grupo,
                        'descripcion': app.material,
                        'fecha': app.fechaEntrada,
                        'idProveedor': app.idProveedor,
                        'nDoc': app.nDoc,
                        'proveedor': app.proveedor,
                        'serie': app.serie,
                        'valor': (app.vlrUnitario * app.tablaSeries.length),
                        'vlrUnitario': app.vlrUnitario,
                        'existencia': app.existencia,
                        'arraySeries': app.tablaSeries
                    });

                    app.detalle = [];
                    app.idMaterial = "";
                    app.material = "";
                    app.cantidad = "";
                    app.vlrUnitario = "";
                    $('#formMateriales').removeClass('was-validated');

                    app.clearDatosSerie();
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        insertProductos: function () {
            var app = this;

            if (app.validaFormGuardar('#form_encabezado')) {

                app.loadingGuadar = false;
                var datos = {
                    codTecnOri: this.codTecnOri,
                    nomTecnOri: this.nomTecnOri,
                    cuadrillaOri: this.cuadrillaOri,
                    codSuperOri: this.codSuperOri,
                    superOri: this.superOri,
                    tecnsOri: this.tecnsOri,

                    codTecnDes: this.codTecnDes,
                    nomTecnDes: this.nomTecnDes,
                    cuadrillaDes: this.cuadrillaDes,
                    codSuperDes: this.codSuperDes,
                    superDes: this.superDes,
                    tecnsDes: this.tecnsDes,

                    numeroEntrega: this.numeroEntrega,
                    concepto: this.concepto.toUpperCase(),
                    fechaEntrega: this.fechaEntrega,

                    detalle: this.tablaDetalle,

                    nombEmpresa: app.empresa[0].NOMBRE,
                    nitEmpresa: app.empresa[0].NIT,
                    dirEmpresa: app.empresa[0].DIRECCION,

                };

                var datos = JSON.stringify(datos);
                // console.log(datos);
                $.ajax({
                    url: './request/insertProductos.php',
                    type: 'POST',
                    data: {data: datos},
                })
                    .done(function (data) {
                        console.log(data);
                        alertify.success('Ingresando registros... espere.');
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function (data) {
                        var obj = JSON.parse(data);
                        // console.log(obj.response);
                        var server = JSON.stringify(obj);

                        if (obj.response === 'success') {
                            app.loadingGuadar = true;
                            // var response = {server: server, datos: datos};
                            var dir = [];
                            dir[0] = './reports/reporte_entrada_bodega.php';
                            dir[1] = './reports/reporte_salida_bodega.php';

                            for (var i = 0; i < dir.length; i++) {
                                var form = "<form action='" + dir[i] + "' method='post' target='_blank' id='_form" + i + "'>" +
                                    "<input type='hidden' name='server' value='" + server + "' />" +
                                    "<input type='hidden' name='datos' value='" + datos + "' />" +
                                    "</form>";
                                $('body').append(form);
                            }
                            alertify.confirm("Proceso Exitoso", "... Desea Imprimir el reporte de Devolucion N° " + obj.numeroEntrada + " ?",
                                function () {
                                    document.getElementById("_form0").submit();
                                }, function () {
                                }).set({
                                onshow: null,
                                onclose: function () {
                                    alertify.alert("Proceso Exitoso", "El reporte de Entrega N° " + obj.numeroSalida + " se abrira en otra",
                                        function () {
                                            document.getElementById("_form1").submit();
                                            setTimeout(function () {
                                                location.reload();
                                            }, 2000);
                                        });
                                }
                            });
                        }
                    });
            }
        },

        callReport: function () {
            document.getElementById("_form0").submit();
            document.getElementById("_form1").submit();
        },

        // callReport2 : function (dir2, data2) {
        //   var form2 = "<form action='"+dir2+"' method='post' target='_blank' class='_form2'>"+
        //           "<input type='hidden' name='server' value='"+data2.server+"' />"+
        //           "<input type='hidden' name='datos' value='"+data2.datos+"' />"+
        //         "</form>";
        //   $('body').append(form2);
        //   $('._form2').submit();
        //   // $('._form').remove();
        // },

        validaForm: function (idForm) {


            var cont = 0;
            $('' + idForm + ' input:required').each(function (index, val) {
                if ($(this).val() === "") {
                    cont++;
                }
            });
            if (cont === 0) {
                $(idForm).removeClass('was-validated');
                return true;
            } else {
                $(idForm).addClass('was-validated');
                alertify.error('Error: Campos obligatorios vacios');
                return false;
            }
        },

        validaFormGuardar: function (idForm) {

            var app = this;
            var isValid = true;

            console.log(app.tablaDetalle.length)

            if (app.codTecnOri == "" ||
                app.numeroEntrega == "" ||
                app.concepto == "" ||
                app.tablaDetalle.length <= 0
            ) {

                $(idForm).addClass('was-validated');


                if (this.tablaDetalle.length <= 0) {
                    alertify.error('Error: No ha agregado materiales');
                }

                if (app.codTecnOri == "") {
                    alertify.error('Error: No ha seleccionado una bodega origen');
                }

                if (app.codTecnDes == "") {
                    alertify.error('Error: No ha seleccionado una bodega destino');
                }

                isValid = false;

            } else {
                $(idForm).removeClass('was-validated');
                isValid = true;

            }


            return isValid;
        },


    },
    watch: {
        fechaEntrega: function () {
            var f = new Date();
            var f_actual = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
            var fechaAux = this.fechaEntrega.split("-");
            var fechaFac = new Date(parseInt(fechaAux[0]), parseInt(fechaAux[1] - 1), parseInt(fechaAux[2]));

            f.setHours(0, 0, 0, 0);
            fechaFac.setHours(0, 0, 0, 0);
            if (fechaFac.getTime() > f.getTime()) {
                alertify.error("No puede ingresar una fecha futura");
                this.fechaActual();
            }
        },
        cantidad: function () {
            var app = this;
            if (this.cantidad > this.existencia) {
                alertify.alert("No hay existencias", "No hay suficientes existencias en bodega", function () {
                    app.cantidad = "";
                });
            }
        }
    },
    computed: {
        armaSerie:
            {

                get: function () {
                    return this.prefijo + '' + (this.inicioSerie) + '' + this.sufijo;
                },

                set: function (serie) {
                    this.serieManual = serie.toUpperCase();
                }
            }
    },
    mounted() {
        this.fechaActual();
        this.getMateriales();
        this.getEmpresa();
        this.getPropiedades();
        this.getTipoMateriales();
        this.loadUsuarioSistema();
    },
    updated() {
        $('[data-toggle="tooltip"]').tooltip();

    },


});

