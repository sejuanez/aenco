<?php


    $server = json_decode($_POST['server']);
    $data = json_decode($_POST['datos']);

    include("../request/gestion.php");

    require('fpdf.php');

    class PDF extends FPDF
    {

        function Header()
        {
            // Select Arial bold 15
            global $title;
            global $city;
            global $fechaIng, $fechaEntrega, $numeroInt, $numeroMov, $concepto;
            global $codSuperDes, $superDes, $cuadrillaDes, $tecnsDes, $codTecnico, $nomTecnico;
            global $nombreEmpresa, $nitEmpresa, $direccionEmpresa;


            $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
            $this->SetFont('Lato-Regular', '', 12);

            // Ancho de pagina 190 en X
            // $this->setDrawColor(80,80,80);
            // $this->setFillColor(0,80,170);
            // $this->setTextColor(255,255,255);
            $this->setTextColor(80, 80, 80);

            try {
                $this->Image('../../../img/empresa1.jpg', 10, 10, 40, 12, 'jpeg');

            } catch (Exception $e) {

                $this->Image('../../../img/empresa1.jpg', 10, 10, 40, 12, 'jpg');
            }


            $this->SetFont('Lato-Regular', '', 14);
            $this->Cell(190, 10, $nombreEmpresa, 0, 0, 'C', false);
            $this->Ln(8);
            $this->SetFont('Lato-Regular', '', 8);
            $this->Cell(190, 5, 'NIT:  ' . $nitEmpresa, 0, 0, 'C', false);
            $this->Ln(5);
            $this->Cell(190, 5, $direccionEmpresa, 0, 0, 'C', false);

            $this->SetFont('Lato-Regular', '', 14);
            $this->Ln(10);
            $this->Cell(190, 5, $title, 0, 0, 'C', false);


            // Line break
            $this->Ln(10);

            $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
            $this->SetFont('Lato-Regular', '', 10);


            $this->setTextColor(0, 80, 170);
            $this->Cell(31, 7, 'Fecha Movimiento :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $dateI = new DateTime($fechaIng);
            $fechaI = $dateI->format('d/m/Y');
            $this->Cell(22, 7, $fechaI, 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(26, 7, 'Fecha Registro :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);

            $dateE = new DateTime($fechaEntrega);
            $fechaE = $dateE->format('d/m/Y');
            $this->Cell(22, 7, $fechaE, 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(18, 7, utf8_decode('N° Interno :'), 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(22, 7, $numeroInt, 0, 0, 'L');

            $this->setTextColor(0, 80, 170);
            $this->Cell(26, 7, utf8_decode('N° Movimiento :'), 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(22, 7, $numeroMov, 0, 0, 'L');
            $this->Ln(7);


            $this->setTextColor(0, 80, 170);
            $this->Cell(20, 7, 'Concepto :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(170, 7, $concepto, 0, 0, 'L');
            $this->Ln(7);


            $this->setTextColor(0, 80, 170);
            $this->Cell(20, 7, 'Tecnico :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);

            $this->Cell(110, 7, $codTecnico . '  -  ' . $nomTecnico, 0, 0, 'L');
            $this->setTextColor(0, 80, 170);
            $this->Cell(25, 7, 'Cuadrilla :', 0, 0, 'L');
            $this->setTextColor(80, 80, 80);
            $this->Cell(30, 7, $cuadrillaDes, 0, 0, 'L');
            $this->Ln(7);

            $this->setTextColor(0, 80, 170);
            $this->Cell(40, 7, 'Tecnicos de Cuadrilla :', 0, 0, 'L');
            $this->Ln(7);
            $this->setTextColor(80, 80, 80);
            $this->MultiCell(0, 4, $tecnsDes);

        }

        function Footer()
        {
            // Go to 1.5 cm from bottom
            $this->SetY(-15);

            $this->AddFont('LatoL', '', 'Lato-Light.php');
            $this->SetFont('LatoL', '', 8);
            // Print centered page number
            $this->setFillColor(0, 80, 170);
            $this->setTextColor(80, 80, 80);
            $this->Cell(95, 10, 'Fecha y hora de impresion: ' . date('d/m/Y  -H:i:s', time() - 21600), 'T', 0, 'L');
            $this->Cell(95, 10, utf8_decode('Página ' . $this->PageNo()) . '/{nb}', 'T', 0, 'R');
        }

    }//en Class PDF
    $pdf = new PDF();


    // echo "<pre>";
    // print_r ($data);
    // echo "</pre>";
    // die();


    $title = 'SALIDA DEL ALMACEN';

    $codTecnico = $data->codTecnDes;
    $nomTecnico = $data->nomTecnDes;

    $fechaIng = $server->fechaIng;
    $fechaEntrega = $data->fechaEntrega;
    $numeroInt = $data->numeroEntrega;
    $numeroMov = $server->numeroSalida;
    $concepto = utf8_decode($data->concepto);
    $codSuperDes = $data->codSuperDes;
    $superDes = utf8_decode($data->superDes);
    $cuadrillaDes = $data->cuadrillaDes;
    $tecnsDes = utf8_decode($data->tecnsDes);

    $nombreEmpresa = utf8_decode($data->nombEmpresa);
    $nitEmpresa = utf8_decode($data->nitEmpresa);
    $direccionEmpresa = utf8_decode($data->dirEmpresa);

    $pdf->SetTitle($title);
    $pdf->AddPage();
    $pdf->AddFont('Lato-Regular', '', 'Lato-Regular.php');
    $pdf->SetFont('Lato-Regular', '', 10);
    $pdf->Ln();


    // Header table
    $pdf->AddFont('LatoL', '', 'Lato-Light.php');
    $pdf->SetFont('LatoL', '', 9);
    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(0, 80, 170);
    $pdf->setTextColor(255, 255, 255);

    $pdf->Cell(20, 7, 'CODIGO', 1, 0, 'C', true);
    $pdf->Cell(110, 7, 'DESCRIPCION DE MATERIAL', 1, 0, 'C', true);
    $pdf->Cell(20, 7, 'MARCA', 1, 0, 'C', true);
    $pdf->Cell(20, 7, 'UNIDAD', 1, 0, 'C', true);
    $pdf->Cell(20, 7, 'CANTIDAD', 1, 0, 'C', true);
    $pdf->Ln();

    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(255, 255, 255);
    $pdf->setTextColor(80, 80, 80);
    $pdf->AddFont('Lato', '', 'Lato-Regular.php');
    $pdf->SetFont('Lato', '', 9);


    // DETALLE
    $n = count($data->detalle);
    for ($i = 0; $i < $n; $i++) {

        $n2 = count($data->detalle[$i]->arraySeries);
        // var_dump($data->detalle[$i]);
        if ($n2 > 0) {

            $pdf->Cell(20, 7, $data->detalle[$i]->codigo, 'B', 0, 'C', true);
            $pdf->Cell(110, 7, $data->detalle[$i]->descripcion, 'B', 0, 'C', true);
            $pdf->Cell(20, 7, $data->detalle[$i]->arraySeries[0]->NOMMARCA, 'B', 0, 'C', true);
            $pdf->Cell(20, 7, 'UNIDAD', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, $data->detalle[$i]->cantidad, 'B', 0, 'C', true);
            $pdf->Ln(10);
        } else {
            $pdf->Cell(20, 7, $data->detalle[$i]->codigo, 'B', 0, 'C', true);
            $pdf->Cell(110, 7, $data->detalle[$i]->descripcion, 'B', 0, 'C', true);
            $pdf->Cell(20, 7, '', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, 'UNIDAD', 'B', 0, 'C', true);
            $pdf->Cell(20, 7, $data->detalle[$i]->cantidad, 'B', 0, 'C', true);
            $pdf->Ln();
        }

    }

    // die();
    $pdf->Ln();
    $text = "";
    for ($i = 0; $i < $n; $i++) {

        $n2 = count($data->detalle[$i]->arraySeries);
        // var_dump($data->detalle[$i]->arraySeries);
        if ($n2 > 0) {
            $text = '>>> Series ' . $data->detalle[$i]->descripcion . ' : ';
            // var_dump($data->detalle[$i]->descripcion);
            for ($j = 0; $j < $n2; $j++) {
                $serie = $data->detalle[$i]->arraySeries[$j]->SERIE;
                $text .= $serie . ' - ';
                // var_dump($data->detalle[$i]->arraySeries[$j]->SERIE);
            }
            $pdf->SetFont('Lato', '', 7);
            $pdf->MultiCell(0, 3, $text);
            $pdf->Ln(10);
        }
    }


    $pdf->SetFont('Lato', '', 9);
    $pdf->Cell(60, 7, 'Registro : ' . $server->user);
    $pdf->Ln(20);

    $pdf->setTextColor(0, 80, 170);
    $pdf->Cell(90, 7, 'Entrego', 'T', 0, 'C', true);
    $pdf->Cell(10, 7, ' ');
    $pdf->Cell(90, 7, 'Recibio', 'T', 0, 'C', true);
    $pdf->Ln(10);


    $pdf->Ln(10);
    $pdf->Output();


?>
