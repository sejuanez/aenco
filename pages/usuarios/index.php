<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
    }
?>

<!DOCTYPE html>
<html>

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <style type="text/css">


        label {
            font-size: 0.8em;
        }

        .imgUsuario {
            max-height: 150px;
            min-height: 150px;
            max-width: 100%;
            margin-bottom: 10px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled,
        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        table {
            font-size: .8em;
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Usuarios

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;" @click="eliminar()">
					<i class="fa fa-trash" aria-hidden="true"></i> Eliminar
				</span>


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;" @click="actualizar()">
					<i class="fa fa-edit" aria-hidden="true"></i> Actualizar
				</span>


            <span id="btnExportar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="guardar()">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
				</span>


        </p>
    </header>


    <div class="container">

        <form enctype="multipart/form-data" class="formGuardar">
            <div class="row">

                <div class="col-12 col-sm-9">


                    <div class="row">


                        <div class="col-12 col-sm-4">
                            <div class="form-group input-group-sm">
                                <label for="usuario">Usuario</label>
                                <input type="text" id="usuario" name="usuario" class="form-control"
                                       aria-label="usuario"
                                       v-on:keyup.13="onChangeTipoUsuario(usuario)"
                                       @blur="onChangeTipoUsuario(usuario)"
                                       v-model="usuario" maxlength="20">
                            </div>
                        </div>


                        <div class="col-12 col-sm-4">
                            <div class="form-group input-group-sm">
                                <label for="nombreUsuario">Nombre</label>
                                <input type="text" id="nombreUsuario" name="nombreUsuario" class="form-control"
                                       aria-label="nombreUsuario"
                                       v-model="nombreUsuario" maxlength="100">
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="form-group input-group-sm">
                                <label for="nomProveedor">Usuarios Registrados </label>

                                <select class="selectBDUsuario" id="selectBDUsuario" name="selectBDUsuario"
                                        v-model="valorBDUsuario">
                                    <option value="">Seleccione...</option>
                                    <option v-for="selectBDUsuario in selectBDUsuarios"
                                            :value="selectBDUsuario.USUARIO">
                                        {{selectBDUsuario.NOMBRE}}
                                    </option>
                                </select>

                            </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="col-12 col-sm-6">
                            <div class="form-group input-group-sm">
                                <label for="password1">Contraseña</label>
                                <input type="password" id="password1" name="password1" class="form-control"
                                       aria-label="password1"
                                       v-model="password1" maxlength="20">
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group input-group-sm">
                                <label for="password2">Confirmar Contraseña</label>
                                <input type="password" id="password2" name="password2" class="form-control"
                                       aria-label="password2"
                                       v-model="password2" maxlength="20">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-12 col-sm-5">
                            <div class="form-group input-group-sm">
                                <label for="selectDepartamento">Departamento </label>

                                <select class="selectDepartamento" id="selectDepartamento" name="selectDepartamento"
                                        v-model="idDepartamento">
                                    <option value="">Seleccione...</option>
                                    <option v-for="selectDepartamento in selectDepartamentos"
                                            :value="selectDepartamento.CODIGO">
                                        {{selectDepartamento.NOMBRE}}
                                    </option>
                                </select>

                            </div>
                        </div>

                        <div class="col-12 col-sm-5">
                            <div class="form-group input-group-sm">
                                <label for="selectMunicipio">Municipios</label>

                                <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                                        v-model="idMunicipio">
                                    <option value="">Seleccione...</option>
                                    <option v-for="selectMunicipio in selectMunicipios" :value="selectMunicipio.CODIGO">
                                        {{selectMunicipio.NOMBRE}}
                                    </option>
                                </select>

                            </div>
                        </div>

                        <div class="col-12 col-sm-2">

                            <div class="text-center">
                                <label for="btnAgregarTabla">Agregar</label>
                                <br>

                                <span id="btnExportar" class="float-right"
                                      style="font-size: 1.2em; cursor: pointer; width: 100%; height: 100%"
                                      @click="cargarMunicipioTabla()">
                    					<i class="fa fa-plus-square-o" aria-hidden="true"></i>
                                    
				                </span>
                            </div>


                        </div>

                    </div>

                </div>

                <div class="col-12 col-sm-3 ">

                    <div class="image-upload">


                        <img class="imgUsuario " src="img/user.jpg" alt="" id="imgUsuario" name="imgUsuario"
                             style=" display:block;margin:auto;">


                        <input type="file" class="form-control" id="foto" accept="image/*" name="foto"
                               style="display: none;"/>
                        <input type="button" value="Foto" class="btn btn-block"
                               onclick="document.getElementById('foto').click();"/>


                    </div>


                </div>
            </div>
        </form>

        <br>

        <div class="row">
            <div class="col-12">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr>
                        <th>Codigo Departamento</th>
                        <th>Nombre Departamento</th>
                        <th>Codigo Municipio</th>
                        <th>Nombre Municipio</th>
                        <th>Quitar</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-for="(dato, index) in tablaDepartamentos" :id="index">
                        <td v-text="dato.codDep"></td>
                        <td v-text="dato.nombreDep"></td>
                        <td v-text="dato.codMun"></td>
                        <td v-text="dato.nombreMun"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-trash-o" title=""
                               style="cursor:pointer; margin: 0 0 10px 10px; text-align: center;"
                               @click="eliminarRow(index)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js "></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript" src="../../js/accounting.js"></script>
<script lang="javascript" src="js/xlsx.full.min.js"></script>
<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>


</body>

</html>
