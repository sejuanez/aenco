<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 26/09/2018
     * Time: 5.03
     */


    define('MAX_SEGMENT_SIZE', 65535);

    session_start();
    if (!$_SESSION['user']) {
        echo
        "<script>
                window.location.href='../index.php';
            </script>";
        exit();
    }


    function blob_create($data)
    {
        if (strlen($data) == 0)
            return false;
        $handle = ibase_blob_create();
        $len = strlen($data);
        for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
            $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
            $buf = substr($data, $pos, $buflen);
            ibase_blob_add($handle, $buf);
        }
        return ibase_blob_close($handle);
    }

    include("../../../init/gestion.php");
    $nombre = $_POST['nombreUsuario'];
    $usuario = $_POST['usuario'];
    $password = $_POST['password1'];


    $arrayDepartamentos = json_decode($_POST['array']);


    $url = $_POST['url'];


    if ($url == '0') {
        $foto = blob_create(file_get_contents("../img/user.jpg"));
    } else {
        $foto = blob_create(file_get_contents("../temp/" . $url));
    }


    $sql = "INSERT into USU_WEB
            ( 
                UW_USUARIO,
                UW_CLAVE,
                UW_NOMBRE,
                UW_FOTO
            )
            
            
            values (?, ?, ?, ?)";

    // $result = ibase_query($conexion, $sql);

    $consulta = ibase_prepare($conexion, $sql);
    $result = ibase_execute($consulta,
        utf8_decode($usuario),
        utf8_decode($password),
        utf8_decode($nombre),
        $foto
    );


    if ($result == '1') {

        for ($i = 0; $i < count($arrayDepartamentos); $i++) {

            $mun = $arrayDepartamentos[$i]->codMun;
            $dep = $arrayDepartamentos[$i]->codDep;

            $sql = "INSERT INTO USU_WEB_MUNICIPIO
            (   UWM_USUARIO,
                UWM_DPTO,
                UWM_MPIO
            )
            
            
            values ('" . $usuario . "', '" . $dep . "', '" . $mun . "')";

            ibase_query($conexion, $sql);


        }
    }


    if ($url != '0') {
        unlink("../temp/" . $url);//acá le damos la direccion exacta del archivo
    }

    echo $result;



