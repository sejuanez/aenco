<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 26/09/2018
     * Time: 10.55
     */


    session_start();

    if (!$_SESSION['user']) {
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

    include("../../../init/gestion.php");


    define('MAX_SEGMENT_SIZE', 65535);
    function blob_create($data)
    {
        if (strlen($data) == 0)
            return false;
        $handle = ibase_blob_create();
        $len = strlen($data);
        for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
            $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
            $buf = substr($data, $pos, $buflen);
            ibase_blob_add($handle, $buf);
        }
        return ibase_blob_close($handle);
    }


    include("../../../init/gestion.php");
    $nombre = $_POST['nombreUsuario'];
    $usuario = $_POST['usuario'];
    $password = $_POST['password1'];


    $arrayDepartamentos = json_decode($_POST['array']);


    $url = $_POST['url'];


    //Eliminar los registros de usu_web para luego insertar nuevos
    $query1 = "DELETE FROM USU_WEB_MUNICIPIO where UWM_USUARIO ='" . $usuario . "'";
    $resultado = ibase_query($conexion, $query1);

    //Insertar el array de minicipios
    for ($i = 0; $i < count($arrayDepartamentos); $i++) {

        $mun = $arrayDepartamentos[$i]->codMun;
        $dep = $arrayDepartamentos[$i]->codDep;

        $sql = "INSERT INTO USU_WEB_MUNICIPIO
            (   UWM_USUARIO,
                UWM_DPTO,
                UWM_MPIO
            )
            
            
            values ('" . $usuario . "', '" . $dep . "', '" . $mun . "')";

        ibase_query($conexion, $sql);


    }


    if ($url === '0') {


        $sql = "UPDATE USU_WEB SET
                                    UW_CLAVE = '" . $password . "' ,
                                    UW_NOMBRE = '" . $nombre . "'    
              WHERE UW_USUARIO = '" . $usuario . "'";

        $result = ibase_query($conexion, $sql);
        echo $result;

    } else {
        $foto = blob_create(file_get_contents("../temp/" . $url));

        $sql = "UPDATE USU_WEB SET
                                    UW_CLAVE = ?,
                                    UW_NOMBRE = ?,
                                    UW_FOTO = ?
                WHERE UW_USUARIO = '" . $usuario . "'
                ";

        // $result = ibase_query($conexion, $sql);

        $consulta = ibase_prepare($conexion, $sql);
        $result = ibase_execute($consulta,

            utf8_decode($password),
            utf8_decode($nombre),
            $foto
        );


        unlink("../temp/" . $url);//acá le damos la direccion exacta del archivo

        echo $result;
    }
