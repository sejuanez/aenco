let app = new Vue({
    el: '#app',
    data: {


        listFuncionarios: [],

        inputSearch: "",

        nombres: "",
        cedula: "",
        telefono: "",
        direccion: "",
        ciudad: "",
        cuadrilla: "",
        tipo: "",
        usuSistema: "",
        codigo: "",
        codEquivalente: "",

        usuariologueado: "",

        usuarioSeleccionado: "",

        editarModal: false,

        btnGuardar: true,
        btnActualizar: false,
        btnEliminar: false,
        btnEditar: false,
        btnFile: true,
        btnCancelar: true,

    },


    methods: {

        loadUsuarioSistema: function () {
            var app = this;
            $.get('./request/getUsuarioSistema.php', function (data) {

                console.log(data)
                app.usuariologueado = data

            });
        },

        nuevo: function () {
            this.editarModal = false;
            this.resetModal();
            $('#addFucionario').modal('show');
            this.usuarioSeleccionado = "";
        },

        verFuncionario: function (dato) {

            var app = this;
            app.editarModal = false;
            app.usuarioSeleccionado = dato;

            app.nombres = dato.nombres;
            app.cedula = dato.cedula;
            app.telefono = dato.telefono;
            app.direccion = dato.direccion;
            app.ciudad = dato.ciudad;
            app.cuadrilla = dato.cuadrilla;
            app.tipo = dato.tipo;
            app.codEquivalente = dato.codEquivalente;


            $('.formGuardar input').attr('disabled', 'disabled');
            $('.formGuardar select').attr('disabled', 'disabled');


            this.btnGuardar = false;
            this.btnActualizar = false;
            this.btnEliminar = true;
            this.btnEditar = true;

            this.btnCancelar = false;
            this.btnFile = false;

            $('#addFucionario').modal('show');
        },

        search: function () {
            var app = this;

            $.post('./request/getTecnicos.php', {tecnico: app.inputSearch}, function (data) {

                var datos = jQuery.parseJSON(data);
                app.listFuncionarios = datos;

            });
        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            var app = this;

            app.nombres = "";
            app.cedula = "";
            app.telefono = "";
            app.direccion = "";
            app.ciudad = "";
            app.cuadrilla = "";
            app.codEquivalente = "";

            app.usuarioSeleccionado = "";


            $('.formGuardar').removeClass('was-validated');
            this.btnGuardar = true;
            this.btnActualizar = false;
            this.btnEliminar = false;
            this.btnEditar = false;
            this.btnFile = true;
            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar select').removeAttr('disabled');
        },

        guardar: function () {

            if (this.nombres == "" ||
                this.cedula == "" ||
                this.telefono == "" ||
                this.tipo == ""

            ) {
                $('.formGuardar').addClass('was-validated');
                alertify.warning('Ingrese todos los campos marcados en rojo')
            } else {

                var app = this;
                var formData = new FormData($('.formGuardar')[0]);

                //hacemos la petición ajax
                $.ajax({
                    url: './request/getCodigoTecnico.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {

                        var datos = jQuery.parseJSON(data);
                        app.formDatos(datos[0].codigo, "./request/insertFuncionario.php");
                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });
            }
        },

        editar: function () {

            this.editarModal = true;

            this.btnFile = true;
            this.btnCancelar = true;
            this.btnEliminar = false;
            this.btnEditar = false;
            this.btnActualizar = true;
            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar #placa').attr('disabled', 'disabled');
            $('.formGuardar select').removeAttr('disabled');
        },

        actualizar: function () {
            if (this.placa == "") {
                $('.formGuardar').addClass('was-validated');
            } else {

                var data = this.usuariologueado.codigo;

                app.formDatos(data, "./request/updateFuncionario.php");

            }
        },

        eliminar: function () {
            var app = this;


            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    $.post('./request/eliminarFuncionario.php', {codigo: app.usuarioSeleccionado.codigo}, function (data) {
                        // console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.search();

                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        formDatos: function (data, url) {

            var app = this;
            var formData = new FormData($('.formGuardar')[0]);
            formData.append('codigo', data);
            formData.append('codigoActualizar', app.usuarioSeleccionado.codigo);
            formData.append('usu', app.usuariologueado);


            //hacemos la petición ajax
            $.ajax({
                url: url,
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    console.log(data)

                    if (data.indexOf("UNIQUE KEY constraint") > -1) {

                        alertify.warning("Ya se encuentra Registrado");

                    }

                    app.search();


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");
                        app.resetModal();
                        if (url == "./request/updateFuncionario.php") {
                            app.search();
                        }

                    } else {

                        alertify.error("Ha ocurrido un error");
                    }

                },
                //si ha ocurrido un error
                error: function (FormData) {
                    console.log(data)
                }
            });
        },

    },
    watch: {},
    mounted() {
        // this.loadSelectPlaca();
        // this.loadSelectGastos();
        this.search();
        this.loadUsuarioSistema();


    },

});
