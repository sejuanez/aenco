<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>


<!DOCTYPE html>

<html>

<head>
    <title>Incripcion</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous"> -->

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

    <style type="text/css">

        table tr.cabecera {
            background: #EFEFEF;

        }

        .table-wrapper-scroll-y {
            display: block;
            height: 75vh;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table td {
            padding: 0.3rem;
            vertical-align: middle;
            border-top: 1px solid #e9ecef;
        }

        .table th {
            padding: 0.4rem;
            vertical-align: middle;
            border-top: 1px solid #e9ecef;
        }

        table tr.cabecera td {
            color: #000000;
            /*text-align: center;*/

        }

        .modal-lg {
            max-width: 700px
        }

        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled,
        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        *

        /
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Gestion Subbodegas

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="nuevo()">
            	<i class="fa fa-save" aria-hidden="true"></i> Nuevo
            </span>

        </p>
    </header>


    <div class="container-fluid">
        <div class="row" style="margin-bottom: 10px;">


            <div class="col-6 offset-3">
                <div class="input-group float-right input-group-sm">
                    <input type="text" class="form-control" v-model="inputSearch"
                           placeholder="Buscar por nombre o codigo">
                    <span class="input-group-btn">
			                      <button type="button" class="btn btn-secondary" @click="search()">Buscar</button>
			                    </span>
                </div>
            </div>
        </div>

        <div id="datos" style="">
            <div class="row">

                <div class="col-12">
                    <div class="table-wrapper-scroll-y">
                        <table class="table table-hover ">
                            <thead class="headerTabla" style="font-size: 0.9rem;">
                            <tr class="cabecera">
                                <td>Codigo</td>
                                <td>Nombres</td>
                                <td>Ciudad</td>
                                <td>Direccion</td>
                                <td>Telefono</td>
                                <td>Tipo</td>


                                <td style="text-align: center;" width="100">+</td>
                            </tr>
                            </thead>
                            <tbody id="detalle_gastos" style="font-size: .8em">

                            <tr v-for="(dato, index) in listFuncionarios">
                                <td v-text="dato.codigo"></td>
                                <td v-text="dato.nombres"></td>
                                <td v-text="dato.ciudad"></td>
                                <td v-text="dato.direccion"></td>
                                <td v-text="dato.telefono"></td>


                                <td v-if="dato.tipo == 'B'"> Bodega</td>
                                <td v-else-if="dato.tipo == 'T'">Tecnico</td>
                                <td v-else v-text="dato.tipo"></td>


                                <td width="100" style="text-align: center;">
                                    <i class="fa fa-eye" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                                       @click="verFuncionario(dato)"></i>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Nuevo Registro

                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar" autocomplete="ño">

                    <div class="modal-body">
                        <div class="row">

                            <div class="col-8 offset-2">

                                <div class="row">

                                    <div class="col-12">
                                        <div class="form-group input-group-sm">
                                            <label for="nombres">Nombres </label>
                                            <input type="text" id="nombres" name="nombres" class="form-control" required
                                                   autocomplete="ño"
                                                   aria-label="nombres" v-model="nombres">
                                            <div class="invalid-feedback">Este campo es requerido</div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">


                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="cedula">Cedula </label>
                                            <input type="text" id="cedula" name="cedula" class="form-control" required
                                                   autocomplete="ño"
                                                   aria-label="cedula" v-model="cedula">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="telefono">Telefono </label>
                                            <input type="text" id="telefono" name="telefono" class="form-control"
                                                   required autocomplete="ño"
                                                   aria-label="telefono" v-model="telefono">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="direccion">Direccion </label>
                                            <input type="text" id="direccion" name="direccion" class="form-control"
                                                   autocomplete="ño"
                                                   required aria-label="direccion" v-model="direccion">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="ciudad">Ciudad </label>
                                            <input type="text" id="ciudad" name="ciudad" class="form-control" required
                                                   autocomplete="ño"
                                                   aria-label="ciudad" v-model="ciudad">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="cuadrilla">Cuadrilla</label>
                                            <input type="text" id="cuadrilla" name="cuadrilla" class="form-control"
                                                   aria-label="cuadrilla"
                                                   autocomplete="ño"
                                                   v-model="cuadrilla">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group input-group-sm">
                                            <label for="cuadrilla">Tipo</label>
                                            <select id="tipo" name="tipo" class="form-control"
                                                    aria-label="tipo" required
                                                    v-model="tipo">

                                                <option value="" disabled>Seleccionar</option>
                                                <option value="T">Teccnico</option>
                                                <option value="B">Bodega</option>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-12">
                                        <div class="form-group input-group-sm">
                                            <label for="nombres">Codigo Equivalente </label>
                                            <input type="text" id="codEquivalente" name="codEquivalente"
                                                   class="form-control"
                                                   required autocomplete="ño"
                                                   aria-label="nombres" v-model="codEquivalente">
                                            <div class="invalid-feedback">Este campo es requerido</div>
                                        </div>

                                    </div>


                                </div>

                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="btnCancelar" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="btnActualizar" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>
                        <button type="button" v-if="btnEditar" class="btn btn-info btn-sm" @click="editar()">Editar
                        </button>
                        <button type="button" v-if="btnEliminar" class="btn btn-danger btn-sm" @click="eliminar()">
                            Eliminar
                        </button>

                        <button type="button" v-if="btnGuardar" class="btn btn-primary btn-sm" @click="guardar()">
                            Guardar
                        </button>

                    </div>

                </form>

            </div>


        </div>
    </div>

</div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
