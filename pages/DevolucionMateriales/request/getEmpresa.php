<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 01/11/2018
     * Time: 8.40
     */

    include("gestion.php");


    $sql = "        select nombre, direccion,  nit
                    from empresa";

    $return_arr = array();

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $row_array['NOMBRE'] = utf8_encode($fila[0]);
        $row_array['DIRECCION'] = utf8_encode($fila[1]);
        $row_array['NIT'] = utf8_encode($fila[2]);
        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);
