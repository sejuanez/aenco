<?php
    include("gestion.php");

    // var_dump($_POST["codTecnico"]);
    // var_dump($_POST["material"]);

    // die();

    $CODTECNICO = $_POST["codTecnico"];
    $ID = addslashes(htmlspecialchars(strtoupper($_POST["material"])));


    $sql = "SELECT
                -- BS.BO_CODIGOTEC CODTECNICO,
               COALESCE (M.MA_CODIGO, '''') CODIGO,
               COALESCE (M.MA_DESCRIPCION, '''') DESCRIPCION,
               COALESCE (BS.BO_EXISTENCIA, 0) EXISTENCIA,
               COALESCE (G.GR_SERIE, '') SERIADO,
               COALESCE (G.GR_CODIGO, '''') GRUPO
        FROM MATERIALES M
        LEFT JOIN GRUPOS G ON G.GR_CODIGO = M.MA_GRUPO
        LEFT JOIN BODEGASUPER BS ON BS.BO_CODIGOMAT = M.MA_CODIGO
        WHERE BS.bo_existencia > 0 AND BS.BO_CODIGOTEC = '" . $CODTECNICO . "' AND  (M.MA_DESCRIPCION  LIKE '%" . $ID . "%' OR M.MA_CODIGO LIKE '%" . $ID . "%')";

    $return_arr = array();

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $row_array['CODIGO'] = utf8_encode($fila[0]);
        $row_array['DESCRIPCION'] = utf8_encode($fila[1]);
        $row_array['EXISTENCIA'] = utf8_encode($fila[2]);
        $row_array['SERIADO'] = utf8_encode($fila[3]);
        $row_array['COD_GRUPO'] = utf8_encode($fila[4]);
        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);

?>
