SET TERM ^ ;

create or alter procedure VEHICULO_BUSCA (
    EOPCION integer)
returns (
    PLACA varchar(10))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select v.ve_placa
      from vehiculos v
      into :placa
    do
    begin
      suspend;
    end
  end
  --------------------------------------------

  if (eopcion = 2) then
  begin
    --Futuras opciones
  end


end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON VEHICULOS TO PROCEDURE VEHICULO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE VEHICULO_BUSCA TO SYSDBA;
