SET TERM ^ ;

create or alter procedure TECNICO_BUSCA (
    EOPCION integer)
returns (
    COD varchar(20),
    NOM varchar(50))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select t.te_codigo, t.te_nombres
      from tecnicos t
      into :cod, :nom
    do
    begin
      suspend;
    end
  end
  --------------------------------------------
  if (eopcion = 2) then
  begin
    --Proimas opciones
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON TECNICOS TO PROCEDURE TECNICO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE TECNICO_BUSCA TO SYSDBA;
