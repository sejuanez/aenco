SET TERM ^ ;

create or alter procedure DEPARTAMENTO_BUSCA (
    EOPCION integer)
returns (
    COD varchar(10),
    NOM varchar(20))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select d.de_codigo, d.de_nombre
      from DEPARTAMENTOS d
      into :cod, :nom
    do
    begin
      suspend;
    end
  end
  --------------------------------------------

  if (eopcion = 2) then
  begin
    --Futuras opciones
  end


end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON DEPARTAMENTOS TO PROCEDURE DEPARTAMENTO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE DEPARTAMENTO_BUSCA TO SYSDBA;
