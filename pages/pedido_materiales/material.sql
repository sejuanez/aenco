SET TERM ^ ;

create or alter procedure MATERIAL_BUSCA (
    EOPCION integer,
    CODMATERIAL varchar(15))
returns (
    CODIGO varchar(20),
    DESCRIPCION varchar(100),
    EXISTENCIAS numeric(15,2))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select m.ma_codigo, m.ma_descripcion, m.ma_existencia
      from materiales m
      where m.ma_codigo || m.ma_descripcion like '%'||:codmaterial||'%'
      into :codigo, :descripcion, :existencias
    do
    begin
      suspend;
    end
  end
  --------------------------------------------

  if (eopcion = 2) then
  begin
      select  m.ma_codigo, m.ma_descripcion, m.ma_existencia
      from materiales m
      where m.ma_codigo = :codmaterial
      into :codigo, :descripcion, :existencias;
  end
    begin
      suspend;
    end



end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON MATERIALES TO PROCEDURE MATERIAL_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE MATERIAL_BUSCA TO SYSDBA;
