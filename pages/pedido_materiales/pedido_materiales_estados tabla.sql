/******************************************************************************/
/****             Generated by IBExpert 1/26/2019 12:00:40 PM              ****/
/******************************************************************************/

SET SQL DIALECT 3;

SET NAMES NONE;



/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/



CREATE TABLE PEDIDO_MATERIALES_ESTADOS (
    PME_NUMERO             INTEGER NOT NULL,
    PME_ESTADO             VARCHAR(30) NOT NULL,
    PME_FECHA              DATE DEFAULT CURRENT_DATE NOT NULL,
    PME_HORA               TIME DEFAULT CURRENT_TIME NOT NULL,
    PME_USU_CAMBIA_ESTADO  VARCHAR(30)
);




/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE PEDIDO_MATERIALES_ESTADOS ADD CONSTRAINT PK_PEDIDO_MATERIALES_ESTADOS PRIMARY KEY (PME_NUMERO, PME_ESTADO);


/******************************************************************************/
/****                             Foreign Keys                             ****/
/******************************************************************************/

ALTER TABLE PEDIDO_MATERIALES_ESTADOS ADD CONSTRAINT FK_PEDIDO_MATERIALES_ESTADOS_1 FOREIGN KEY (PME_NUMERO) REFERENCES PEDIDO_MATERIALES (PM_NUMERO) ON UPDATE CASCADE;
ALTER TABLE PEDIDO_MATERIALES_ESTADOS ADD CONSTRAINT FK_PEDIDO_MATERIALES_ESTADOS_2 FOREIGN KEY (PME_USU_CAMBIA_ESTADO) REFERENCES USU_WEB (UW_USUARIO) ON UPDATE CASCADE;


/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/

