<?php

define('MAX_SEGMENT_SIZE', 65535);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");

$data = $_POST['data'];

$n = count($data);


$sql = 'SELECT GEN_ID(PEDIDOS, 1) FROM RDB$DATABASE';

$result = ibase_query($conexion, $sql);

$consecutivo = "";

while ($fila = ibase_fetch_row($result)) {
    $consecutivo = utf8_encode($fila[0]);
}

// sql pedido_materiales


$sql = "insert into PEDIDO_MATERIALES (PM_NUMERO, 
                                       ORDEN_EXTERNA,
                                       PM_PROYECTO,                                                                
                                       PM_INSTALACION,
                                       PM_INGENIERO, 
                                       PM_CIRCUITO, 
                                       PM_NODO, 
                                       PM_DIRECCION, 
                                       PM_DEPARTAMENTO, 
                                       PM_MUNICIPIO,
                                       PM_TRABAJO_REALIZAR, 
                                       PM_SUPERVISOR_RESPONABLE, 
                                       PM_TECNICO_LIDER, 
                                       PM_PLACA_MOVIL,                                                                                                                   
                                       PM_USUARIO_CREA 
                                       )
            values (
                                       '" . $consecutivo . "', 
                                       '" . $data[0]['gom'] . "', 
                                       '" . $data[0]['proyecto'] . "',
                                       '" . $data[0]['instalacion'] . "',
                                       '" . $data[0]['valorING'] . "',
                                       '" . $data[0]['circuito'] . "',
                                       '" . $data[0]['nodo'] . "',
                                       '" . $data[0]['direccion'] . "',
                                       '" . $data[0]['valorDEP'] . "',
                                       '" . $data[0]['valorMunicipio'] . "',
                                       '" . $data[0]['trabajo'] . "',
                                       '" . $data[0]['valorSUP'] . "',
                                       '" . $data[0]['valorTEC'] . "',
                                       '" . $data[0]['valorPlaca'] . "',
                                       '" . $_SESSION['user'] . "'
                                       
                                       
                                       
                                       )";

$result = ibase_query($conexion, $sql);

$return_arr = array();

$row_array['array'] = array(
    "pedido" => $consecutivo,
    "cabezera" => $result,
    "detalle" => null
);
array_push($return_arr, $row_array);


for ($i = 0; $i < count($data); $i++) {

    $sql = "insert into PEDIDO_MATERIALES_DETALLE (PMD_NUMERO, 
                                       PMD_CODMATER,
                                       PMD_CANTIDAD_PEDIDA,
                                       PMD_CANT_APROBADA                                                                
                                       )
            values (
                                       '" . $consecutivo . "', 
                                       '" . $data[$i]['codigo'] . "', 
                                       '" . $data[$i]['cantidad_sol'] . "',
                                       0                                                                             
                                       )";

    $result = ibase_query($conexion, $sql);

    $row_array['array'] = array(
        "pedido" => null,
        "cabezera" => null,
        "detalle" => $result
    );


    array_push($return_arr, $row_array);

}


echo json_encode($return_arr);



