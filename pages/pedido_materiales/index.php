<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">

        input[type='number'] {
            -moz-appearance: textfield;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 52vh;
            overflow-y: scroll;
        }

        .tbody-materiales {
            display: block;
            height: 34vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Pedido de materiales

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div style="padding-bottom: 5px" class="row">

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="gom" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="gom">
                    <label for="gom">Orden Externa (GOM)</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="proyecto" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="proyecto">
                    <label for="proyecto">Proyecto</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="instalacion" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="instalacion">
                    <label for="instalacion">Instalacion</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectIngeniero" id="selectIngeniero" name="selectIngeniero"
                            v-model="valorING">
                        <option value="">Ingeniero...</option>
                        <option v-for="item in selectIngeniero"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>


        </div>

        <div style="padding-bottom: 5px" class="row">

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="circuito" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="circuito">
                    <label for="circuito">Circuito</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="nodo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="nodo">
                    <label for="nodo">Nodo</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="direccion" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="direccion">
                    <label for="direccion">Direccion</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectDepartamentos" id="selectDepartamentos" name="selectDepartamentos"
                            v-model="valorDEP">
                        <option value="">Departamento...</option>
                        <option v-for="item in selectDepartamentos"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>

        </div>

        <div style="padding-bottom: 5px" class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMunicipio">
                        <option value="">Municipio...</option>
                        <option v-for="item in selectMunicipio"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="trabajo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="trabajo">
                    <label for="trabajo">Trabajo a realizar</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectSupervisor" id="selectSupervisor" name="selectSupervisor"
                            v-model="valorSUP">
                        <option value="">Supervisor...</option>
                        <option v-for="item in selectSupervisor"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectTecnico" id="selectTecnico" name="selectTecnico"
                            v-model="valorTEC">
                        <option value="">Tecnico lider...</option>
                        <option v-for="item in selectTecnico"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>

        </div>

        <div style="padding-bottom: 5px" class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectPlaca" id="selectPlaca" name="selectPlaca"
                            v-model="valorPlaca">
                        <option value="">Placa movil...</option>
                        <option v-for="item in selectPlaca"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-6">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control col-2" name="idproveedor" readonly required
                           placeholder="Material">
                    <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="modalMateriales()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                    <input type="text" class="form-control" name="proveedor" required aria-label="" disabled
                           v-model="nombreMaterial">
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="number" id="cantidad" class="form-control noSigno" placeholder="Email address"
                           autofocus=""
                           autocomplete="nope"
                           v-model="cantidad">
                    <label for="cantidad">Catidad</label>
                </div>
            </div>

            <div class="col-12 col-sm-1">
                <div class="form-group">

                    <button type="button"
                            v-on:click="addMaterial()"
                            class="btn btn-secondary ">
                        <i class="fa fa-arrow-circle-down"> </i>
                    </button>
                </div>
            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="150">Codigo</th>
                        <th>Descripcion</th>
                        <th width="150">Cantidad solicitada</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaMateriales.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se han agregado materiales</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaMateriales">
                        <td width="150" v-text="dato.codigo"></td>
                        <td v-text="dato.descripcion"></td>
                        <td width="150" v-text="dato.cantidad_sol"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verDepartamento(dato, index)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato, index)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Material
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <form>

                                <div class="col-3">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal">Codigo </label>
                                        <input type="text" id="descripcion" name="descripcion_modal" readonly
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="codigo_modal">
                                    </div>
                                </div>

                                <div class="col-7">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal">Descripcion </label>
                                        <input type="text" id="descripcion" name="nombre_modal" readonly
                                               class="form-control" required aria-label="nombre_modal"
                                               v-model="descripcion_modal">
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal">Cantidad </label>
                                        <input type="number" id="descripcion" name="nombre_modal"
                                               class="form-control" required aria-label="nombre_modal"
                                               v-model="cantidad_modal">
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>

    <div id="modalMateriales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Materiales / Herramientas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-6"
                                       name="idproveedor" placeholder="Buscar..." aria-label=""
                                       v-model="buscarMaterial">
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="loadMaterial()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="100">Codigo</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody class="tbody-materiales">
                                <tr v-for="material in selectMaterial" @click="seleccionarMaterial(material)">
                                    <td width="100" v-text="material.codigo"></td>
                                    <td v-text="material.nombre"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
