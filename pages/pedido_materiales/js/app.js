// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 100);

    });


    $(".selectIngeniero").select2().change(function (e) {
        var usuario = $(".selectIngeniero").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeING(usuario, nombreUsuario);
    });

    $(".selectDepartamentos").select2().change(function (e) {
        var usuario = $(".selectDepartamentos").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeDEP(usuario, nombreUsuario);
    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeMunicipio(usuario, nombreUsuario);
    });

    $(".selectSupervisor").select2().change(function (e) {
        var usuario = $(".selectSupervisor").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeSUP(usuario, nombreUsuario);
    });

    $(".selectTecnico").select2().change(function (e) {
        var usuario = $(".selectTecnico").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeTecnico(usuario, nombreUsuario);
    });

    $(".selectPlaca").select2().change(function (e) {
        var usuario = $(".selectPlaca").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangePlaca(usuario, nombreUsuario);
    });

    $(".selectMaterial").select2().change(function (e) {
        var usuario = $(".selectMaterial").val();
        //var nombreUsuario = this.options[this.selectedIndex].text;
        var nombreUsuario = "";
        app.onChangeMaterial(usuario, nombreUsuario);
    });

    $('.noSigno').keydown(function (e) {
        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            ajax: false,

            selectIngeniero: [],
            valorING: "",

            selectDepartamentos: [],
            valorDEP: "",

            selectSupervisor: [],
            valorSUP: "",

            selectTecnico: [],
            valorTEC: "",

            selectPlaca: [],
            valorPlaca: "",

            selectMunicipio: [],
            valorMunicipio: "",

            selectMaterial: [],
            codigoMaterial: "",
            nombreMaterial: "",

            gom: "",
            proyecto: "",
            instalacion: "",
            nombre: "",
            circuito: "",
            nodo: "",
            direccion: "",
            trabajo: "",
            cantidad: "",

            buscarMaterial: "",

            index_material: "",


            codigo_modal: "",
            descripcion_modal: "",
            cantidad_modal: "",


            tablaMateriales: [],

            noResultados: false,

        },


        methods: {

            loadIngeniero: function () {
                var app = this;
                $.get('./request/getIngeniero.php', function (data) {
                    app.selectIngeniero = JSON.parse(data);
                });
            },

            onChangeING: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorING = cod;
            },

            loadDdepartamento: function () {
                var app = this;
                $.get('./request/getDepartamento.php', function (data) {
                    app.selectDepartamentos = JSON.parse(data);
                });
            },

            onChangeDEP: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorDEP = cod;
                app.selectMunicipio = [];
                app.loadMunicipio(cod);
            },

            loadMunicipio: function (cod) {

                var app = this;

                if (cod == "") {
                    app.selectMunicipio = [];
                } else {

                    $.post('./request/getMunicipio.php', {
                        mun: cod,
                    }, function (data) {
                        //console.log(data)
                        app.selectMunicipio = [];
                        app.selectMunicipio = JSON.parse(data);
                    });
                }
            },

            onChangeMunicipio: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorMunicipio = cod;
            },

            loadSupervisor: function () {
                var app = this;
                $.get('./request/getSupervisor.php', function (data) {
                    app.selectSupervisor = JSON.parse(data);
                });
            },

            onChangeSUP: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorSUP = cod;
            },

            loadTecnico: function () {
                var app = this;
                $.get('./request/getTecnico.php', function (data) {
                    app.selectTecnico = JSON.parse(data);
                });
            },

            onchangeTecnico: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorTEC = cod;
            },

            loadPlaca: function () {
                var app = this;
                $.get('./request/getPlaca.php', function (data) {
                    app.selectPlaca = JSON.parse(data);
                });
            },

            onChangePlaca: function (cod, nombre) {
                var app = this;
                //console.log(cod)
                app.valorPlaca = cod;
            },

            loadMaterial: function () {
                var app = this;
                $.post('./request/getMaterial.php', {
                    buscar: app.buscarMaterial.toUpperCase(),
                }, function (data) {
                    //console.log(data)
                    app.selectMaterial = JSON.parse(data);
                });
            },

            seleccionarMaterial: function (data) {

                this.valorMaterial = data.codigo;
                this.nombreMaterial = data.nombre;
                $('#modalMateriales').modal('hide');
                this.buscarMaterial = "";

            },

            addMaterial: function () {

                var app = this;

                if (

                    app.gom == "" ||
                    app.proyecto == "" ||
                    app.instalacion == "" ||
                    app.valorING == "" ||
                    app.circuito == "" ||
                    app.nodo == "" ||
                    app.direccion == "" ||
                    app.valorDEP == "" ||
                    app.valorMunicipio == "" ||
                    app.trabajo == "" ||
                    app.valorSUP == "" ||
                    app.valorTEC == "" ||
                    app.valorPlaca == "" ||
                    app.valorMaterial == "" ||
                    app.cantidad == ""
                ) {

                    alertify.error('Ingrese todos los cambios')

                } else {


                    if (app.tablaMateriales.length <= 0) {
                        array = {

                            gom: app.gom,
                            proyecto: app.proyecto,
                            instalacion: app.instalacion,
                            valorING: app.valorING,
                            circuito: app.circuito,
                            nodo: app.nodo,
                            direccion: app.direccion,
                            valorDEP: app.valorDEP,
                            valorMunicipio: app.valorMunicipio,
                            trabajo: app.trabajo,
                            valorSUP: app.valorSUP,
                            valorTEC: app.valorTEC,
                            valorPlaca: app.valorPlaca,
                            codigo: app.valorMaterial,
                            descripcion: app.nombreMaterial,
                            cantidad_sol: app.cantidad,
                        }

                        app.tablaMateriales.push(array);

                        app.disableInputs();
                    } else {

                        var bandera = false;

                        for (index in app.tablaMateriales) {

                            if (app.tablaMateriales[index].codigo == app.valorMaterial) {
                                alertify.success('Este material ya se encuentra en la lista');
                                bandera = false;
                                break;
                            } else {
                                bandera = true;
                            }
                        }

                        if (bandera) {
                            array = {
                                gom: app.gom,
                                proyecto: app.proyecto,
                                instalacion: app.instalacion,
                                valorING: app.valorING,
                                circuito: app.circuito,
                                nodo: app.nodo,
                                direccion: app.direccion,
                                valorDEP: app.valorDEP,
                                valorMunicipio: app.valorMunicipio,
                                trabajo: app.trabajo,
                                valorSUP: app.valorSUP,
                                valorTEC: app.valorTEC,
                                valorPlaca: app.valorPlaca,
                                codigo: app.valorMaterial,
                                descripcion: app.nombreMaterial,
                                cantidad_sol: app.cantidad,
                            }

                            app.tablaMateriales.push(array);

                        }

                    }

                    app.cantidad = "";
                    app.nombreMaterial = "";

                }


            },

            verDepartamento: function (dato, index) {

                this.codigo_modal = dato.codigo;
                this.descripcion_modal = dato.descripcion;
                this.cantidad_modal = dato.cantidad_sol;
                this.index_material = index;


                $('#addFucionario').modal('show');

            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        //console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadAnio()

                }

            },

            actualizar: function () {

                var app = this;

                if (this.cantidad_modal == "") {

                    alertify.error("Por favor ingresa la cantidad");

                } else {


                    app.tablaMateriales[app.index_material]['cantidad_sol'] = app.cantidad_modal;
                    $('#addFucionario').modal('hide');


                }


            },

            eliminar: function (dato, index) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.descripcion + " ?",
                    function () {

                        app.tablaMateriales.splice(index, 1)

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {

                var app = this;

                if (app.tablaMateriales.length == 0) {

                    alertify.error("Agrega materiales");

                } else {


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/pedirMateriales.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            data: app.tablaMateriales
                        },

                    }).done(function (data) {

                        var respuesta = JSON.parse(data);


                        alertify.alert('Pedido de materiales',
                            "Su numero de solicitud es: <strong> '" + respuesta[0]['array']['pedido'] + "' </strong>");

                        app.clear();


                    }).fail(function (data) {
                        //console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";

            },

            disableInputs: function () {

                var app = this;

                $("#gom").prop('disabled', true);
                $("#proyecto").prop('disabled', true);
                $("#instalacion").prop('disabled', true);
                $("#circuito").prop('disabled', true);
                $("#nodo").prop('disabled', true);
                $("#direccion").prop('disabled', true);
                $("#trabajo").prop('disabled', true);

                app.disableSelect2('.selectIngeniero')
                app.disableSelect2('.selectDepartamentos')
                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectSupervisor')
                app.disableSelect2('.selectTecnico')
                app.disableSelect2('.selectPlaca')

            },

            enableInputs: function () {

                var app = this;

                $("#gom").prop('disabled', false);
                $("#proyecto").prop('disabled', false);
                $("#instalacion").prop('disabled', false);
                $("#circuito").prop('disabled', false);
                $("#nodo").prop('disabled', false);
                $("#direccion").prop('disabled', false);
                $("#trabajo").prop('disabled', false);

                app.enableSelect2('.selectIngeniero')
                app.enableSelect2('.selectDepartamentos')
                app.enableSelect2('.selectMunicipio')
                app.enableSelect2('.selectSupervisor')
                app.enableSelect2('.selectTecnico')
                app.enableSelect2('.selectPlaca')

            },

            clear: function () {

                var app = this;

                app.enableInputs();

                app.gom = "";
                app.proyecto = "";
                app.instalacion = "";
                app.valorING = "";
                $("#selectIngeniero").val("").trigger('change')
                app.circuito = "";
                app.nodo = "";
                app.direccion = "";
                app.valorDEP = "";
                $("#selectDepartamentos").val("").trigger('change')
                app.valorMunicipio = "";
                $("#selectMunicipio").val("").trigger('change')
                app.trabajo = "";
                app.valorSUP = "";
                $("#selectSupervisor").val("").trigger('change')
                app.valorTEC = "";
                $("#selectTecnico").val("").trigger('change')
                app.valorPlaca = "";
                $("#selectPlaca").val("").trigger('change')
                app.nombreMaterial = "";

                app.tablaMateriales = [];

            },

            disableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', true);
                $(class_select).select2();

            },

            enableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', false);
                $(class_select).select2();

            },

            modalMateriales: function () {

                var app = this;

                app.buscarMaterial = "";
                app.selectMaterial = [];
                $('#modalMateriales').modal('show');

            }
        },


        watch:
            {}
        ,

        mounted() {

            this.loadIngeniero();
            this.loadDdepartamento();
            this.loadSupervisor();
            this.loadTecnico();
            this.loadPlaca();


        }
        ,

    })
;
