SET TERM ^ ;

create or alter procedure SUPERVISOR_BUSCA (
    EOPCION integer)
returns (
    COD varchar(10),
    NOM varchar(20))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select s.su_codigo, s.su_nombre
      from supervisores s
      into :cod, :nom
    do
    begin
      suspend;
    end
  end
  --------------------------------------------
  if (eopcion = 2) then
  begin
    --futuras actualizaciones

  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON SUPERVISORES TO PROCEDURE SUPERVISOR_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SUPERVISOR_BUSCA TO SYSDBA;
