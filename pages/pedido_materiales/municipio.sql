SET TERM ^ ;

create or alter procedure MUNICIPIO_BUSCA (
    EOPCION integer,
    ECOD_DEP varchar(10))
returns (
    COD varchar(10),
    NOM varchar(30))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select m.MU_CODIGOMUN, m.MU_NOMBRE
      from municipios m
      into :cod, :nom
    do
    begin
      suspend;
    end
  end
  --------------------------------------------
  if (eopcion = 2) then
  begin
    --Mostrar por codigo del departamento
    for
      select m.MU_CODIGOMUN, m.MU_NOMBRE
      from municipios m
      where m.mu_depto = :ecod_dep
      into :cod, :nom
    do
    begin
      suspend;
    end
  end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON MUNICIPIOS TO PROCEDURE MUNICIPIO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE MUNICIPIO_BUSCA TO SYSDBA;
