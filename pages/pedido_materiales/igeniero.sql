SET TERM ^ ;

create or alter procedure INGENIERO_BUSCA (
    EOPCION integer)
returns (
    CODIGO varchar(10),
    NOMBRES varchar(50))
as
begin
  if (eopcion = 1) then
  begin
    --Mostrar todos los datos.
    for
      select i.in_codigo, i.in_nombres
      from ingenieros i
      into :codigo, :nombres
    do
    begin
      suspend;
    end
  end
  --------------------------------------------

  if (eopcion = 2) then
  begin
    --Futuras opciones
  end


end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON INGENIEROS TO PROCEDURE INGENIERO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE INGENIERO_BUSCA TO SYSDBA;
