<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	$dpto=$_POST["dpto"];
	
		
	$consulta = "SELECT distinct da.dla_mpio, m.mu_nombre from dato_lev_alumbrado da left join municipios m on m.mu_depto=da.dla_dpto and m.mu_codigomun=da.dla_mpio where da.dla_dpto=".$dpto;

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['value'] = $fila[0];
		$row_array['label'] = $fila[1];
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>