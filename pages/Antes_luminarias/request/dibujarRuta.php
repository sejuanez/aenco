<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	
	
	$dpto = $_POST["dpto"];
	$mun = $_POST["mun"];
	//$trafo = $_POST["trafo"];

	
	function getFotosActa($cnx, $acta){

		$sql = "SELECT ea.ea_no_foto from evidencias_alumbrado ea where ea.ea_acta='".$acta."'";
		$divImg="<div>";

		$result = ibase_query($cnx,$sql);
		while($fila = ibase_fetch_row($result)){
			$divImg.="<img src='request/imagenEvidencias.php?acta=".$acta."&codfoto=".$fila[0]."'></img>";
		}

		$divImg.="</div>";

		return $divImg;
	};
	
	

	//$consulta = "SELECT DA.dla_longitud LON_INICIAL, DA.dla_latitud LAT_INICIAL, DA.dla_nodo NOD_INICIAL, DA.dla_nodo_anterior NODO_ANTES, (DA.dla_longitud) LON_ANTES, (DA.dla_latitud) LAT_ANTES, da.dla_pto_fisico, da.dla_tipo_lampara, cast(da.dla_potencia as integer) potencia, da.dla_tipo_encendido, da.dla_direccion, da.dla_luminaria, MAT.ma_descripcion, ds.descripcion, da.dla_serielum, d.de_nombre dpto, m.mu_nombre mpio, c.co_nombre corregimiento, b.ba_nombarrio, da.dla_fecha FECHA_CENSO,da.dla_acta FROM busca_serielum ('".$dpto."', '".$mun."') bs inner join dato_lev_alumbrado da on da.dla_serielum=bs.dla_serielum and da.dla_fecha=bs.fechaej and da.dla_dpto = '".$dpto."' and da.dla_mpio= '".$mun."' and da.dla_longitud<>0 LEFT JOIN departamentos d on d.de_codigo=da.dla_dpto LEFT JOIN municipios m on m.mu_depto = da.dla_dpto and m.mu_codigomun=da.dla_mpio LEFT JOIN corregimientos c on c.co_depto=da.dla_dpto and c.co_municipio=da.dla_mpio and c.co_codcorregimiento=da.dla_corregimiento LEFT JOIN barrios b on b.ba_depto=da.dla_dpto and b.ba_mpio=da.dla_mpio and b.ba_sector=da.dla_corregimiento and b.ba_codbarrio=da.dla_barrio LEFT JOIN materiales MAT ON MAT.ma_codigo=DA.dla_codigomat LEFT JOIN descripcion_series DS on ds.grupo=MAT.ma_grupo and ds.codigo=da.dla_codmarca";
	
	$consulta = "SELECT BS.longinicial LON_INICIAL, BS.latinicial LAT_INICIAL, BS.nodoinicial NOD_INICIAL, BS.nodoantes NODO_ANTES, (BS.lonantes) LON_ANTES, (BS.latantes) LAT_ANTES, BS.ptofisico, BS.tipolampara, cast(BS.potencia as integer) potencia, BS.tipoencendido, BS.direccion, BS.luminaria, BS.matdescripcion, BS.marcadescripcion, BS.serieluminaria, BS.dptonombre dpto, BS.mpionombre mpio, BS.corregimiento corregimiento, BS.barrio, BS.fechaej FECHA_CENSO,BS.acta FROM busca_serielum ('".$dpto."', '".$mun."') bs";

	$return_arr = array();
	

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		
		
		$row_array['latIni'] = $fila[1];
		$row_array['lonIni'] = $fila[0];
		$row_array['nodoIni'] = $fila[2];
		$row_array['nodoAnt'] = $fila[3];
		$row_array['latAnt'] = $fila[5];
		$row_array['lonAnt'] = $fila[4];
		$row_array['tipoLamp'] = $fila[7];
		$row_array['potencia'] = $fila[8];
		$row_array['acta'] = $fila[20];
		$row_array['infoWin'] = "<div id='infoWindow'><table><tr class='cabecera'><td>Acta ".$fila[20]."</td><td>Nodo</td><td>".$fila[2]."</td></tr>".
															"<tr class='fila'><td>Punto fisico</td><td>:</td><td>".utf8_encode($fila[6])."</td></tr>".
															"<tr class='fila'><td>Tipo Lamp.</td><td>:</td><td>".utf8_encode($fila[7])."</td></tr>".
															"<tr class='fila'><td>Potencia</td><td>:</td><td>".utf8_encode($fila[8])."</td></tr>".
															"<tr class='fila'><td>Encendido</td><td>:</td><td>".utf8_encode($fila[9])."</td></tr>".
															"<tr class='fila'><td>Dirección</td><td>:</td><td>".utf8_encode($fila[10])."</td></tr>".
															"<tr class='fila'><td>Luminaria</td><td>:</td><td>".utf8_encode($fila[11])."</td></tr>".
															"<tr class='fila'><td>Ma. Descripción</td><td>:</td><td>".utf8_encode($fila[12])."</td></tr>".
															"<tr class='fila'><td>Ds. Descripcion</td><td>:</td><td>".utf8_encode($fila[13])."</td></tr>".
															"<tr class='fila'><td>Serie</td><td>:</td><td>".utf8_encode($fila[14])."</td></tr></table></div>";
															//"<tr class='fila'><td>Serie</td><td>:</td><td>".utf8_encode($fila[14])."</td></tr></table>".getFotosActa($conexion, $fila[20])."</div>";
		//$row_array['title'] = "Trafo ".$fila[0];
		array_push($return_arr, $row_array);
		
	}

	echo json_encode($return_arr);
	//print_r($return_arr);
?>