<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=ordenes_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	$consulta = "SELECT l.ca_orden, o.oa_serie,  coalesce (o.oa_direccion,'')||'-'||coalesce(b.ba_nombarrio, '') DIRECCION,
                coalesce(c.co_nombre,'') CORREGIMIENTO, coalesce(m.mu_nombre, '') MUNICIPIO,
                O.oa_apellido1||' '||coalesce(o.oa_apellido2, '')||' '||coalesce(o.oa_nombre1,'')||' '||coalesce(o.oa_nombre2,'') CLIENTE,
                l.ca_fechaej, l.ca_horaej, t.te_nombres, tos.to_descripcion
                    from lega_cabecera l
                     left join ot_ap o on  o.oa_numero  = l.ca_acta
                     left join municipios m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio
                     left join corregimientos c on c.co_depto=o.oa_dpto and c.co_municipio=o.oa_mpio and c.co_codcorregimiento=o.oa_corregimiento
                     left join barrios b on b.ba_depto=o.oa_dpto and b.ba_mpio=o.oa_mpio and b.ba_sector=o.oa_corregimiento and b.ba_codbarrio=o.oa_barrio
                     left join tecnicos t on t.te_codigo=l.ca_tecnico
                     left join tipo_orden_servicio tos on tos.to_codigo=l.ca_codtipoordens
                    where l.ca_fechaej between '".$fechaini."' AND '".$fechafin."'";

	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Orden</td>".
						"<td>Serie</td>".
						"<td>Direccion</td>".
						"<td>Corregimiento</td>".
						"<td>Municipio</td>".
						"<td>Cliente</td>".
						"<td>Fecha</td>".
						"<td>Hora</td>".
						"<td>Tecnico</td>".
						"<td>Descripcion</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
					"<td>".utf8_encode($fila[5])."</td>".
					"<td>".utf8_encode($fila[6])."</td>".
					"<td>".utf8_encode($fila[7])."</td>".
					"<td>".utf8_encode($fila[8])."</td>".
					"<td>".utf8_encode($fila[9])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>