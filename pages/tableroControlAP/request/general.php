<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	
	
	$consulta = "SELECT l.ca_orden, o.oa_serie,  coalesce (o.oa_direccion,'')||'-'||coalesce(b.ba_nombarrio, '') DIRECCION,
                coalesce(c.co_nombre,'') CORREGIMIENTO, coalesce(m.mu_nombre, '') MUNICIPIO,
                O.oa_apellido1||' '||coalesce(o.oa_apellido2, '')||' '||coalesce(o.oa_nombre1,'')||' '||coalesce(o.oa_nombre2,'') CLIENTE,
                l.ca_fechaej, l.ca_horaej, t.te_nombres, tos.to_descripcion
                    from lega_cabecera l
                     left join ot_ap o on  o.oa_numero  = l.ca_acta
                     left join municipios m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio
                     left join corregimientos c on c.co_depto=o.oa_dpto and c.co_municipio=o.oa_mpio and c.co_codcorregimiento=o.oa_corregimiento
                     left join barrios b on b.ba_depto=o.oa_dpto and b.ba_mpio=o.oa_mpio and b.ba_sector=o.oa_corregimiento and b.ba_codbarrio=o.oa_barrio
                     left join tecnicos t on t.te_codigo=l.ca_tecnico
                     left join tipo_orden_servicio tos on tos.to_codigo=l.ca_codtipoordens
                    where l.ca_fechaej between '".$fechaini."' AND '".$fechafin."'";
	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['orden'] = utf8_encode($fila[0]);
		$row_array['serie'] = utf8_encode($fila[1]);		
		$row_array['direccion'] = utf8_encode($fila[2]);
		$row_array['corregimiento'] = utf8_encode($fila[3]);
		$row_array['municipio'] = utf8_encode($fila[4]);
		$row_array['cliente'] = utf8_encode($fila[5]);
		$row_array['fecha'] = utf8_encode($fila[6]);
		$row_array['hora'] = utf8_encode($fila[7]);
		$row_array['tecnico'] = utf8_encode($fila[8]);
		$row_array['descripcion'] = utf8_encode($fila[9]);

					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>