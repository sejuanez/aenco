<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">

    <title>Bodega Principal</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

    <link rel='stylesheet' href='css/tablet.css' type='text/css'
          media='only screen and (min-width:481px) and (max-width:831px)'/>

    <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>

    <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

    <script type="text/javascript">

        $(function () {


            $("#alert").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                buttons: {
                    Ok: function () {
                        $("#alert p").html("");
                        $(this).dialog("close");
                    }
                }
            });


            $("#cargando").hide();

            $("#consultando").hide();
            $("#guardando").hide();

            $("#seccion-tabla-detallado").hide();
            $("#seccion-tabla-agrupado").hide();
            $("#div-tecnico").hide();

            function seConsulta() {

                $.ajax({
                    url: 'request/getConsulta.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        opcion: $("#tConsulta").val(),
                    }
                }).done(function (repuesta) {

                    if (repuesta.length > 0) {
                        var options = "";
                        for (var i = 0; i < repuesta.length; i++) {
                            options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
                        }
                        $("#opcion").html(options);

                    }


                });// fin del ajax departamentos

            }

            $("#tConsulta").on("change", function () {
                var opcionSeleccionada = $('#tConsulta').val();
                var html = "";
                if (opcionSeleccionada != "Consecutivo" && opcionSeleccionada != "No.Documento") {
                    html = "<label id='titulo'>" + $('#tConsulta').val() + "</label>" +
                        "       <select id='opcion'></select>";
                    $("#opcionSeleccionada").html(html);
                    seConsulta();


                } else {
                    html = "<label id='titulo'>" + $('#tConsulta').val() + "</label>" +
                        "       <input type='text' id='opcion'>";
                    $("#opcionSeleccionada").html(html);
                }

                if (opcionSeleccionada == "Consecutivo" || opcionSeleccionada == "No.Documento") {
                    $("#rangoFechaF").hide();
                    $("#rangoFechaI").hide();
                } else {
                    $("#rangoFechaF").show();
                    $("#rangoFechaI").show();
                }
                if (opcionSeleccionada == "Todos") {
                    $("#opcion").hide();
                } else {
                    $("#opcion").show();
                }
            });


            $('#consultar').click(function (e) {

                $("#consultar").hide();
                $("#consultando").show();

                $("table tbody").html("");

                $("#seccion-tabla-detallado").hide();
                $("#seccion-tabla-agrupado").hide();


                e.preventDefault();

                $("#Exportar a").attr("href",
                    "request/exportarExcel.php?tm=" + $("#tMovimiento").val() +
                    "&tc=" + $("#tConsulta").val() +
                    "&opcion=" + $("#forma").val() +
                    "&f1=" + $("#fechaInicial").val() +
                    "&f2=" + $("#fechaFinal").val() +
                    "&valor=" + $("#opcion").val()
                );

                $.ajax({
                    url: 'request/consultar.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        tm: $("#tMovimiento").val(),
                        tc: $("#tConsulta").val(),
                        opcion: $("#forma").val(),
                        f1: $("#fechaInicial").val(),
                        f2: $("#fechaFinal").val(),
                        valor: $("#opcion").val()
                    }

                }).done(function (repuesta) {
                    console.log(repuesta);

                    var filas = "";

                    if (repuesta.length > 0) {

                        for (var i = 0; i < repuesta.length; i++) {

                            if ($("#forma").val() == "Agrupado") {
                                filas += "<tr class='fila'>" +
                                    "<td>" + repuesta[i].TIPO + "</td>" +
                                    "<td>" + repuesta[i].CODIGO + "</td>" +
                                    "<td>" + repuesta[i].NOMBRE + "</td>" +
                                    "<td>" + repuesta[i].CANTIDAD + "</td>" +
                                    "<td>" + repuesta[i].VALOR + "</td>" +
                                    "</tr>";
                            } else {
                                filas += "<tr class='fila'>" +
                                    "<td>" + repuesta[i].TIPO + "</td>" +
                                    "<td>" + repuesta[i].NUMERO + "</td>" +
                                    "<td>" + repuesta[i].FECHA + "</td>" +
                                    "<td>" + repuesta[i].COD_PROV + "</td>" +
                                    "<td>" + repuesta[i].NOM_PROV + "</td>" +
                                    "<td>" + repuesta[i].COD_MATER + "</td>" +
                                    "<td>" + repuesta[i].NOMBRE_MATER + "</td>" +
                                    "<td>" + repuesta[i].CANTIDAD + "</td>" +
                                    "<td>" + repuesta[i].VR_UNIT + "</td>" +
                                    "<td>" + repuesta[i].SUB_TOTAL + "</td>" +
                                    "<td>" + repuesta[i].USUARIO + "</td>" +
                                    "<td>" + repuesta[i].DOCUMENTO + "</td>" +
                                    "<td>" + repuesta[i].CUADRILLA + "</td>" +
                                    "<td>" + repuesta[i].DES_CUADRILLA + "</td>" +
                                    "<td>" + repuesta[i].CONCEPTO + "</td>" +
                                    "<td>" + repuesta[i].COD_FUNC + "</td>" +
                                    "<td>" + repuesta[i].NOM_FUNC + "</td>" +
                                    "<td>" + repuesta[i].COD_SUPER + "</td>" +
                                    "<td>" + repuesta[i].NOM_SUPER + "</td>" +
                                    "<td>" + repuesta[i].FECHA_INGRESO + "</td>" +
                                    "<td>" + repuesta[i].OC + "</td>" +
                                    "<td>" + repuesta[i].COD_GRUPO + "</td>" +
                                    "<td>" + repuesta[i].NOMBRE_GRUPO + "</td>" +
                                    "</tr>";
                            }
                        }


                        $("#div-tecnico").show();
                        if ($("#forma").val() == "Agrupado") {
                            $("#agrupado").html(filas);

                            $("#seccion-tabla-agrupado").show();
                        } else {
                            $("#detallado").html(filas);
                            $("#seccion-tabla-detallado").show();

                        }
                    } else {

                        if ($("#forma").val() == "Agrupado") {

                            $("#seccion-tabla-agrupado").show();
                            $("#agrupado").html("<tr class='fila'><td colspan='5'>(vacío)</td></tr>");

                        } else {
                            $("#seccion-tabla-detallado").show();
                            $("#detallado").html("<tr class='fila'><td colspan='23'>(vacío)</td></tr>");


                        }

                    }

                    $("#consultando").hide();
                    $("#consultar").show();

                });
            });

            $("body").show();
            $("#rangoFechaF").hide();
            $("#rangoFechaI").hide();
            seConsulta();

        });
    </script>

</head>
<body style="display:none">

<div id='cargando'>
    <progress></progress>
    <p>Espere...</p>
</div>

<div id="alert" title="Mensaje">
    <p>Ordenes asignadas correctamente</p>
</div>

<header>
    <h3>Bodega Principal</h3>
    <nav>
        <ul id="menu">

            <li id="consultar"><a href=""><span class="ion-ios-search-strong"></span><h6>Consultar</h6></a></li>
            <li id="Exportar"><a href=""><span class="ion-ios-download"></span><h6>Exportar</h6></a></li>
            <li id="consultando"><a href=""><span class="ion-load-d"></span><h6>Consultando...</h6></a></li>
        </ul>
    </nav>

</header>


<section
        style="padding-top: 44px; height: calc(100% - 80px); height: -webkit-calc(100% - 80px); height: -moz-calc(100% - 80px)">
    <div id='div-criterio'>
        <fieldset id='criterio'>
            <legend>Criterios de consulta</legend>

            <div>
                <div>
                    <label>Tipo de Movimiento</label>
                    <select id='tMovimiento'>
                        <option value="ENT">Entradas</option>
                        <option value="SAL">Salidas</option>
                    </select>
                </div>
            </div>

            <div>
                <div>
                    <label>Tipo de Consulta</label>
                    <select id='tConsulta'>
                        <option value="Consecutivo">Consecutivo</option>
                        <option value="Proveedor">Proveedor</option>
                        <option value="Material">Material</option>
                        <option value="Operador">Operador</option>
                        <option value="Supervisor">Supervisor</option>
                        <option value="No.Documento">No.Documento</option>
                        <option value="Grupo">Grupo</option>
                        <option value="Cuadrilla">Cuadrilla</option>
                        <option value="Tecnico">Tecnico</option>
                        <option value="Todos">Todos</option>
                    </select>
                </div>
            </div>


            <div>
                <div>
                    <label>Forma</label>
                    <select id='forma'>
                        <option value="Detallado">Detallado</option>
                        <option value="Agrupado">Agrupado</option>
                    </select>
                </div>
            </div>

            <div>
                <div id="opcionSeleccionada">
                    <label id='titulo'>Consecutivo</label>
                    <input type='text' id='opcion'>
                </div>
            </div>

            <div id="rangoFechaI">
                <label>Rango de Fecha</label>
                <div>
                    <input type="date" id="fechaInicial" name="fechaInicial">
                </div>
            </div>

            <div id="rangoFechaF">
                <div>
                    <input type="date" id="fechaFinal" name="fechaFinal">
                </div>
            </div>

        </fieldset>

    </div>


    <section class="seccion-tabla" id='seccion-tabla-detallado'>

        <div class="table-wrapper-scroll-y" style="height: 100%;">
            <div id='header-tabla'>

            </div>
            <div
                    style="height: calc(100% - 24px); height: -webkit-calc(100% - 24px); height: -moz-calc(100% - 24px);">
                <table>
                    <thead>
                    <tr class='cabecera'>
                        <td>TIPO</td>
                        <td>NUMERO</td>
                        <td>FECHA</td>
                        <td>COD_PROV</td>
                        <td>NOM_PROV</td>
                        <td>COD_MATER</td>
                        <td>NOMBRE_MATER</td>
                        <td>CANTIDAD</td>
                        <td>VR_UNIT</td>
                        <td>SUB_TOTAL</td>
                        <td>USUARIO</td>
                        <td>DOCUMENTO</td>
                        <td>CUADRILLA</td>
                        <td>DES_CUADRILLA</td>
                        <td>CONCEPTO</td>
                        <td>COD_FUNC</td>
                        <td>NOM_FUNC</td>
                        <td>COD_SUPER</td>
                        <td>NOM_SUPER</td>
                        <td>FECHA_INGRESO</td>
                        <td>OC</td>
                        <td>COD_GRUPO</td>
                        <td>NOMBRE_GRUPO</td>
                    </tr>
                    </thead>

                    <tbody id="detallado">
                    <tr class='fila'>
                        <td colspan='23'>(vacío)</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>

    </section>

    <section class="seccion-tabla" id='seccion-tabla-agrupado'>

        <div class="table-wrapper-scroll-y" style="height: 100%;">
            <div id='header-tabla'>

            </div>
            <div
                    style="height: calc(100% - 24px); height: -webkit-calc(100% - 24px); height: -moz-calc(100% - 24px);">
                <table>
                    <thead>
                    <tr class='cabecera'>
                        <td>TIPO</td>
                        <td>CODIGO</td>
                        <td>NOMBRE</td>
                        <td>CANTIDAD</td>
                        <td>VALOR</td>
                    </tr>
                    </thead>

                    <tbody id="agrupado">
                    <tr class='fila'>
                        <td colspan='5'>(vacío)</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>

    </section>
</section>

</body>
</html>