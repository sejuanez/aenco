<?php
session_start();
if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}


include("../../../init/gestion.php");

$tipoMovimiento = $_POST["tm"];
$tipoConsulta = $_POST["tc"];
$opcion = $_POST["opcion"];
$tconsulta = "";

$f1 = $_POST["f1"];
$f2 = $_POST["f2"];
$valor = $_POST["valor"];

if ($opcion == "Agrupado") {

    $consulta = "select ma.sa_tipo, ma.sa_codigo_ma, ma.sa_nombre_ma, sum(ma.sa_cantidad) cantidad, sum(ma.sa_valor*ma.sa_cantidad) sa_valor 
    from movimiento_almacen ma 
    where sa_tipo ='" . $tipoMovimiento . "'";

} else if ($opcion == "Detallado") {

    $consulta = "select SA_TIPO TIPO,
                          SA_NUMERO NUMERO,
                          SA_FECHA FECHA,
                          SA_CODIGO COD_PROV,
                          SA_NOMBRE NOM_PROV,
                          SA_CODIGO_MA COD_MATER,
                          SA_NOMBRE_MA NOMBRE_MATER,
                          SA_CANTIDAD CANTIDAD,
                          SA_VALOR VR_UNIT,
                          CAST(SA_CANTIDAD*SA_VALOR AS NUMERIC(15,2)) SUB_TOTAL,
                          SA_USUARIO USUARIO,
                          SA_DOCUMENTO DOCUMENTO,
                          SA_CUADRILLA CUADRILLA,
                          SA_NCUADRILLA DES_CUADRILLA,
                          SA_CONCEPTO CONCEPTO,
                          SA_CODTECNICO COD_FUNC,
                          SA_NOMTECNICO NOM_FUNC,
                          SA_CODSUPER COD_SUPER,
                          SA_NOMSUPER NOM_SUPER,
                          SA_FECHA_INGRESO FECHA_INGRESO,
                          SA_OC_NUMERO OC,
                          MA_GRUPO COD_GRUPO,
                          GR_DESCRIPCION NOMBRE_GRUPO
                   from movimiento_almacen
     left join materiales on ma_codigo=sa_codigo_ma
     left join grupos  on gr_codigo=ma_grupo
    where sa_tipo ='" . $tipoMovimiento . "'";

}


if ($tipoConsulta != 'null' && $tipoConsulta != '' && $tipoConsulta != "Consecutivo" && $tipoConsulta != "No.Documnto") {

    $consulta .= " and sa_fecha between '" . $f1 . "' and '" . $f2 . "'";
}

if ($tipoConsulta != 'null' && $tipoConsulta != '' && $tipoConsulta != "Todos") {

    if ($tipoConsulta == "Consecutivo") {

        $tconsulta .= "sa_numero";

    } else if ($tipoConsulta == "Proveedor") {

        $tconsulta .= "sa_codigo";

    } else if ($tipoConsulta == "Material") {

        $tconsulta .= "sa_codigo_ma";

    } else if ($tipoConsulta == "Operador") {

        $tconsulta .= "sa_usuario";

    } else if ($tipoConsulta == "Supervisor") {

        $tconsulta .= "sa_codsuper";

    } else if ($tipoConsulta == "No.Documnto") {

        $tconsulta .= "sa_documento";

    } else if ($tipoConsulta == "Grupo") {

        $tconsulta .= "gr_codigo";

    } else if ($tipoConsulta == "Cuadrilla") {

        $tconsulta .= "sa_cuadrilla";

    } else if ($tipoConsulta == "Tecnico") {

        $tconsulta .= "sa_codtecnico";
    }

    $consulta .= " and " . $tconsulta . "='" . $valor . "' ";

}

if ($opcion == "Agrupado") {

    $consulta .= " group by  ma.sa_tipo, ma.sa_codigo_ma, ma.sa_nombre_ma";


    $return_arr = array();
    //echo $consulta;
    $result = ibase_query($conexion, $consulta);


    while ($fila = ibase_fetch_row($result)) {

        $row_array['TIPO'] = utf8_encode($fila[0]);
        $row_array['CODIGO'] = utf8_encode($fila[1]);
        $row_array['NOMBRE'] = utf8_encode($fila[2]);
        $row_array['CANTIDAD'] = utf8_encode($fila[3]);
        $row_array['VALOR'] = utf8_encode($fila[4]);

        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);

} else {


    $return_arr = array();
    //echo $consulta;

    $result = ibase_query($conexion, $consulta);


    while ($fila = ibase_fetch_row($result)) {

        $row_array['TIPO'] = utf8_encode($fila[0]);
        $row_array['NUMERO'] = utf8_encode($fila[1]);
        $row_array['FECHA'] = utf8_encode($fila[2]);
        $row_array['COD_PROV'] = utf8_encode($fila[3]);
        $row_array['NOM_PROV'] = utf8_encode($fila[4]);
        $row_array['COD_MATER'] = utf8_encode($fila[5]);
        $row_array['NOMBRE_MATER'] = utf8_encode($fila[6]);
        $row_array['CANTIDAD'] = utf8_encode($fila[7]);
        $row_array['VR_UNIT'] = utf8_encode($fila[8]);
        $row_array['SUB_TOTAL'] = utf8_encode($fila[9]);
        $row_array['USUARIO'] = utf8_encode($fila[10]);
        $row_array['DOCUMENTO'] = utf8_encode($fila[11]);
        $row_array['CUADRILLA'] = utf8_encode($fila[12]);
        $row_array['DES_CUADRILLA'] = utf8_encode($fila[13]);
        $row_array['CONCEPTO'] = utf8_encode($fila[14]);
        $row_array['COD_FUNC'] = utf8_encode($fila[15]);
        $row_array['NOM_FUNC'] = utf8_encode($fila[16]);
        $row_array['COD_SUPER'] = utf8_encode($fila[17]);
        $row_array['NOM_SUPER'] = utf8_encode($fila[18]);
        $row_array['FECHA_INGRESO'] = utf8_encode($fila[19]);
        $row_array['OC'] = utf8_encode($fila[20]);
        $row_array['COD_GRUPO'] = utf8_encode($fila[21]);
        $row_array['NOMBRE_GRUPO'] = utf8_encode($fila[22]);

        array_push($return_arr, $row_array);
    }

    echo json_encode($return_arr);
}


