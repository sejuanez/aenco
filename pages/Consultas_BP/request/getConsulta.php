<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}


include("../../../init/gestion.php");

$usuario = $_SESSION['user'];
$opcion = $_POST["opcion"];


if ($opcion == "Consecutivo" || $opcion == "No.Documento") {
    //consulta por fecha
    $consulta = "";

    return;
} else if ($opcion == "Proveedor") {
    $consulta = "Select p.pr_codigo, p.pr_nombres from proveedores p";

} else if ($opcion == "Material") {
    $consulta = "Select m.ma_codigo, m.ma_descripcion from materiales m";

} else if ($opcion == "Operador") {
    $consulta = "Select u.uw_usuario, u.uw_nombre from usu_web u";

} else if ($opcion == "Supervisor") {
    $consulta = "select s.su_codigo, s.su_nombre from supervisores s";

} else if ($opcion == "Grupo") {
    $consulta = "select gr.gr_codigo, gr.gr_descripcion  from grupos gr";

} else if ($opcion == "Cuadrilla") {
    $consulta = "select cu.cu_codigo codigo, cu.cu_codigo cuadrilla  from cuadrillas cu";

} else if ($opcion == "Tecnico") {
    $consulta = "select t.te_codigo, t.te_nombres from tecnicos t";

}

$return_arr = array();

$result = ibase_query($conexion, $consulta);

$row_array['codigo'] = null;
$row_array['nombre'] = '(TODOS)';
array_push($return_arr, $row_array);

while ($fila = ibase_fetch_row($result)) {


    $row_array['codigo'] = utf8_encode($fila[0]);
    $row_array['nombre'] = utf8_encode($fila[1]);


    array_push($return_arr, $row_array);
}

echo json_encode($return_arr);

?>