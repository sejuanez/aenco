<?php
/**
 * Created by PhpStorm.
 * User: Enrique Torres
 * Date: 5/02/2019
 * Time: 11:51 AM
 */


session_start();
if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=REPORTE.xls");
header("Expires: 0");


include("../../../init/gestion.php");

$tipoMovimiento = $_GET["tm"];
$tipoConsulta = $_GET["tc"];
$opcion = $_GET["opcion"];
$tconsulta = "";

$f1 = $_GET["f1"];
$f2 = $_GET["f2"];
$valor = $_GET["valor"];

if ($opcion == "Agrupado") {

    $tabla = "<table>" .
        "<tr class='cabecera'>" .
        "<th>TIPO</th>" .
        "<th>CODIGO</th>" .
        "<th>MATERIAL</th>" .
        "<th>CANTIDAD</th>" .
        "<th>VALOR</th>" .
        "</tr>";

    $consulta = "select ma.sa_tipo, ma.sa_codigo_ma, ma.sa_nombre_ma, sum(ma.sa_cantidad) cantidad, sum(ma.sa_valor*ma.sa_cantidad) sa_valor 
    from movimiento_almacen ma 
    where sa_tipo ='" . $tipoMovimiento . "'";

} else if ($opcion == "Detallado") {

    $tabla = "<table>" .
        "<tr class='cabecera'>" .
        "<th>TIPO</th>" .
        "<th>NUMERO</th>" .
        "<th>FECHA</th>" .
        "<th>COD_PROV</th>" .
        "<th>NOM_PROV</th>" .
        "<th>COD_MATER</th>" .
        "<th>NOMBRE_MATER</th>" .
        "<th>CANTIDAD</th>" .
        "<th>VR_UNIT</th>" .
        "<th>SUB_TOTAL</th>" .
        "<th>USUARIO</th>" .
        "<th>DOCUMENTO</th>" .
        "<th>CUADRILLA</th>" .
        "<th>DES_CUADRILLA</th>" .
        "<th>CONCEPTO</th>" .
        "<th>COD_FUNC</th>" .
        "<th>NOM_FUNC</th>" .
        "<th>COD_SUPER</th>" .
        "<th>NOM_SUPER</th>" .
        "<th>FECHA_INGRESO</th>" .
        "<th>OC</th>" .
        "<th>COD_GRUPO</th>" .
        "<th>NOMBRE_GRUPO</th>" .
        "</tr>";

    $consulta = "select SA_TIPO TIPO,
                          SA_NUMERO NUMERO,
                          SA_FECHA FECHA,
                          SA_CODIGO COD_PROV,
                          SA_NOMBRE NOM_PROV,
                          SA_CODIGO_MA COD_MATER,
                          SA_NOMBRE_MA NOMBRE_MATER,
                          SA_CANTIDAD CANTIDAD,
                          SA_VALOR VR_UNIT,
                          CAST(SA_CANTIDAD*SA_VALOR AS NUMERIC(15,2)) SUB_TOTAL,
                          SA_USUARIO USUARIO,
                          SA_DOCUMENTO DOCUMENTO,
                          SA_CUADRILLA CUADRILLA,
                          SA_NCUADRILLA DES_CUADRILLA,
                          SA_CONCEPTO CONCEPTO,
                          SA_CODTECNICO COD_FUNC,
                          SA_NOMTECNICO NOM_FUNC,
                          SA_CODSUPER COD_SUPER,
                          SA_NOMSUPER NOM_SUPER,
                          SA_FECHA_INGRESO FECHA_INGRESO,
                          SA_OC_NUMERO OC,
                          MA_GRUPO COD_GRUPO,
                          GR_DESCRIPCION NOMBRE_GRUPO
                   from movimiento_almacen
     left join materiales on ma_codigo=sa_codigo_ma
     left join grupos  on gr_codigo=ma_grupo
    where sa_tipo ='" . $tipoMovimiento . "'";

}


if ($tipoConsulta != 'null' && $tipoConsulta != '' && $tipoConsulta != "Consecutivo" && $tipoConsulta != "No.Documnto") {

    $consulta .= " and sa_fecha between '" . $f1 . "' and '" . $f2 . "'";
}

if ($tipoConsulta != 'null' && $tipoConsulta != '' && $tipoConsulta != "Todos") {

    if ($tipoConsulta == "Consecutivo") {

        $tconsulta .= "sa_numero";

    } else if ($tipoConsulta == "Proveedor") {

        $tconsulta .= "sa_codigo";

    } else if ($tipoConsulta == "Material") {

        $tconsulta .= "sa_codigo_ma";

    } else if ($tipoConsulta == "Operador") {

        $tconsulta .= "sa_usuario";

    } else if ($tipoConsulta == "Supervisor") {

        $tconsulta .= "sa_codsuper";

    } else if ($tipoConsulta == "No.Documnto") {

        $tconsulta .= "sa_documento";

    } else if ($tipoConsulta == "Grupo") {

        $tconsulta .= "gr_codigo";

    } else if ($tipoConsulta == "Cuadrilla") {

        $tconsulta .= "sa_cuadrilla";

    } else if ($tipoConsulta == "Tecnico") {

        $tconsulta .= "sa_codtecnico";
    }

    $consulta .= " and " . $tconsulta . "='" . $valor . "' ";

}

if ($opcion == "Agrupado") {

    $consulta .= " group by  ma.sa_tipo, ma.sa_codigo_ma, ma.sa_nombre_ma";


    $return_arr = array();
    //echo $consulta;
    $result = ibase_query($conexion, $consulta);


    while ($fila = ibase_fetch_row($result)) {

        $tabla .= "<tr class='fila'>" .
            "<td>" . utf8_encode($fila[0]) . "</td>" .
            "<td>" . utf8_encode($fila[1]) . "</td>" .
            "<td>" . utf8_encode($fila[2]) . "</td>" .
            "<td>" . utf8_encode($fila[3]) . "</td>" .
            "<td>" . utf8_encode($fila[4]) . "</td>" .
            "</tr>";
    }


} else {


    $return_arr = array();
    //echo $consulta;

    $result = ibase_query($conexion, $consulta);


    while ($fila = ibase_fetch_row($result)) {

        $tabla .= "<tr class='fila'>" .
            "<td>" . utf8_encode($fila[0]) . "</td>" .
            "<td>" . utf8_encode($fila[1]) . "</td>" .
            "<td>" . utf8_encode($fila[2]) . "</td>" .
            "<td>" . utf8_encode($fila[3]) . "</td>" .
            "<td>" . utf8_encode($fila[4]) . "</td>" .
            "<td>" . utf8_encode($fila[5]) . "</td>" .
            "<td>" . utf8_encode($fila[6]) . "</td>" .
            "<td>" . utf8_encode($fila[7]) . "</td>" .
            "<td>" . utf8_encode($fila[8]) . "</td>" .
            "<td>" . utf8_encode($fila[9]) . "</td>" .
            "<td>" . utf8_encode($fila[10]) . "</td>" .
            "<td>" . utf8_encode($fila[11]) . "</td>" .
            "<td>" . utf8_encode($fila[12]) . "</td>" .
            "<td>" . utf8_encode($fila[13]) . "</td>" .
            "<td>" . utf8_encode($fila[14]) . "</td>" .
            "<td>" . utf8_encode($fila[15]) . "</td>" .
            "<td>" . utf8_encode($fila[16]) . "</td>" .
            "<td>" . utf8_encode($fila[17]) . "</td>" .
            "<td>" . utf8_encode($fila[18]) . "</td>" .
            "<td>" . utf8_encode($fila[19]) . "</td>" .
            "<td>" . utf8_encode($fila[20]) . "</td>" .
            "<td>" . utf8_encode($fila[21]) . "</td>" .
            "<td>" . utf8_encode($fila[22]) . "</td>" .
            "</tr>";

    }


}
$tabla .= "</table>";

echo $tabla;
