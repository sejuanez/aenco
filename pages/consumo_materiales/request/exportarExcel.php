<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	//header("Content-Disposition: filename=consumo_".$_GET["tab"]."_".$_GET["tab"].".xls");
  //header("Content-Disposition: filename=consumo_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  

	include("../../../init/gestion.php");

	$fechaini = $_GET["fechaini"];
  $fechafin = $_GET["fechafin"];
  $tab = $_GET["tab"];

  if($tab == "0"){

    header("Content-Disposition: filename=consumoGeneral_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $consulta = "SELECT dm.ma_codmater codigo_material, dm.ma_desmater descripcion, sum(dm.ma_canmater) cantidad from lega_cabecera lc inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='M' where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' group by dm.ma_codmater, dm.ma_desmater";

    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Codigo</td><td>Descripcion</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//codMaterial
          "</td><td>".utf8_encode($fila[1]).//descripcion
          "</td>"."<td>".utf8_encode($fila[2]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    
    echo $tabla;

  }else if($tab == "1"){

    header("Content-Disposition: filename=consumoPorMunicipio_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM

    $consulta = "SELECT m.mu_nombre municipio, dm.ma_codmater codigo_material, dm.ma_desmater descripcion, sum(dm.ma_canmater) cantidad from lega_cabecera lc inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='M' left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' group by m.mu_nombre, dm.ma_codmater, dm.ma_desmater";


    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Municipio</td><td>Codigo</td><td>Descripcion</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){


      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//municipio
          "</td><td>".utf8_encode($fila[1]).//codMaterial
          "</td><td>".utf8_encode($fila[2]).//descripcion
          "</td><td>".utf8_encode($fila[3]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
  else if($tab == "2"){

    header("Content-Disposition: filename=consumoPorTecnico_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");

    //echo "\xEF\xBB\xBF"; // UTF-8 BOM

    $consulta = "SELECT t.te_nombres tecnico, dm.ma_codmater codigo_material, dm.ma_desmater descripcion, sum(dm.ma_canmater) cantidad from lega_cabecera lc inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='M' left join tecnicos t on t.te_codigo=lc.ca_tecnico where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' group by t.te_nombres, dm.ma_codmater, dm.ma_desmater";


    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Tecnico</td><td>Codigo</td><td>Descripcion</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){


      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//tecnico
          "</td><td>".utf8_encode($fila[1]).//codMaterial
          "</td><td>".utf8_encode($fila[2]).//descripcion
          "</td><td>".utf8_encode($fila[3]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
  else {

    header("Content-Disposition: filename=consumoPorActas_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");

    //echo "\xEF\xBB\xBF"; // UTF-8 BOM

    $consulta = "SELECT LC.ca_acta Acta, lc.ca_fechaej FechaEj, t.te_nombres Tecnico, tos.to_descripcion Proceso, dm.ma_codmater codigo_material, dm.ma_desmater descripcion, dm.ma_canmater cantidad, m.mu_nombre municipio from lega_cabecera lc inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='M' left join tipo_orden_servicio tos on tos.to_codigo=lc.ca_codtipoordens left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio left join tecnicos t on t.te_codigo=lc.ca_tecnico where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."'";


    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Acta</td><td>Fecha ejec.</td><td>Tecnico</td><td>Proceso</td><td>Cod. material</td><td>Descripcion</td><td>Cantidad</td><td>Municipio</td></tr>";

    while($fila = ibase_fetch_row($result)){


      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//acta
          "</td><td>".utf8_encode($fila[1]).//fecha ejec
          "</td><td>".utf8_encode($fila[2]).//tecnico
          "</td><td>".utf8_encode($fila[3]).//proceso
          "</td><td>".utf8_encode($fila[4]).//cod material
          "</td><td>".utf8_encode($fila[5]).//descripcion
          "</td><td>".utf8_encode($fila[6]).//cantidad
          "</td><td>".utf8_encode($fila[7]).//municipio
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
		
	
?>