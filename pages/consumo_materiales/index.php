<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Consumo materiales</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<!--<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>-->

	
	<script type="text/javascript">
		
		$(function(){



			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			
			


			//$("#tecnico").attr('disabled',true);
			//$("#consultar").hide();

			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();
			//$("#div-total").hide();


			


			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					$("#exportar").hide();

					var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
					
					if(diaIni<10)
						diaIni="0"+diaIni;

					var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
					if(mesIni<10)
						mesIni="0"+mesIni;

					var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
					if(diaFin<10)
						diaFin="0"+diaFin;

					var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
					if(mesFin<10)
						mesFin="0"+mesFin;



				
					var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
			        
			        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;


					$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tab="+$("#tabs").tabs( "option", "active" ));
					$("#exportar").show();
				}
			});

			
			

			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				//$("#div-total").hide();

				
				var doneAjax1 = doneAjax2 = doneAjax3 = doneAjax4 = false;

				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;

		        var hayActas = false;

				//consumo general
		        $.ajax({
						url:'request/rq1.php',
		                type:'POST',
		                dataType:'json',
		                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax1
					doneAjax1=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						hayActas = true;

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							filas+="<tr class='fila'><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						};


						$("#tabla-general tbody").html(filas);
						


					}
					else{//la consulta no devolvio resultdos

						hayActas = false;
						
						$("#tabla-general tbody").html("");
					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						/*if(hayActas){
							$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}*/
						$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tab="+$("#tabs").tabs( "option", "active" ));
						$("#exportar").show();
							
						$("#contenido").show();						
						
					}

				});



				//consumo por municipio
		        $.ajax({
						url:'request/rq2.php',
		                type:'POST',
		                dataType:'json',
		                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax2
					doneAjax2=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							filas+="<tr class='fila'><td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						};


						$("#tabla-municipio tbody").html(filas);

						
					}
					else{

						$("#tabla-municipio tbody").html("");

					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						/*if(hayActas){
							$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}*/
						$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tab="+$("#tabs").tabs( "option", "active" ));
						$("#exportar").show();
							
						$("#contenido").show();						
						
					}

				});



				//consumo por tecnicos
		        $.ajax({
						url:'request/rq3.php',
		                type:'POST',
		                dataType:'json',
		                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax3
					doneAjax3=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							filas+="<tr class='fila'><td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						};


						$("#tabla-tecnicos tbody").html(filas);

						
					}
					else{//la consulta no devolvio resultdos

						$("#tabla-tecnicos tbody").html("");

					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						/*if(hayActas){
							$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}*/
						$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tab="+$("#tabs").tabs( "option", "active" ));	
						$("#exportar").show();
						$("#contenido").show();						
						
					}

				});



				//consumo por actas
		        $.ajax({
						url:'request/rq4.php',
		                type:'POST',
		                dataType:'json',
		                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax3
					doneAjax4=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							filas+="<tr class='fila'><td>"+repuesta[i].acta+"</td><td>"+repuesta[i].fecha+"</td><td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].material+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td><td>"+repuesta[i].municipio+"</td></tr>";
						};


						$("#tabla-actas tbody").html(filas);

						
					}
					else{//la consulta no devolvio resultdos

						$("#tabla-actas tbody").html("");

					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						/*if(hayActas){
							$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}*/
						$("#exportar a").attr("href","request/exportarExcel.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tab="+$("#tabs").tabs( "option", "active" ));	
						$("#exportar").show();
						$("#contenido").show();						
						
					}

				});



			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Consumo de materiales</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" target="_self"><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<!--<div><label>Técnico</label><select id='tecnico'></select></div>-->
				
				<!--<div id="div-total">
					<h5>Total</h5>        
                	<h4 id="total">0</h4>
                	
              	</div>-->
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Consumo general</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab' >Consumo por municipios</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab' >Consumo por técnicos</span></a></li>
				<li><a href="#tab4">4. <span class='titulo-tab' >Consumo por actas</span></a></li>
			
			</ul>


			<div id='tab1'>
				<div id='div-tabla'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Codigo</td><td>Descripción</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>




			<div id='tab2'>

				<div id='div-tabla-municipio'>
					<table id='tabla-municipio'>
						<thead>
							<tr class='cabecera'><td>Municipio</td><td>Codigo</td><td>Descripción</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>
				
				
			</div>




			<div id='tab3'>

				<div id='div-tabla-tecnicos'>
					<table id='tabla-tecnicos'>
						<thead>
							<tr class='cabecera'><td>Tecnico</td><td>Codigo</td><td>Descripcion</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>			
				
			</div>



			<div id='tab4'>

				<div id='div-tabla-actas'>
					<table id='tabla-actas'>
						<thead>
							<tr class='cabecera'><td>Acta</td><td>Fecha ejec.</td><td>Técnico</td><td>Proceso</td><td>Cod. material</td><td>Descripción</td><td>Cantidad</td><td>Municipio</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>			
				
			</div>



		</div>

		

	</div>

	

</body>
</html>