<?php
	// session_start();
 //  	if(!$_SESSION['user']){
 //        echo
 //        "<script>
 //            window.location.href='../../inicio/index.php';
 //        </script>";
 //        exit();
 //    }



	include("../../../init/gestion.php");

	$orden=$_POST["orden"];
		
	$consulta = "SELECT da.dla_luminaria     luminaria,
       da.dla_codigomat     Codmater,
       m.ma_descripcion     Nombre_mater,
       da.dla_codmarca      codmarca,
       ds.descripcion       nombre_marca,
       da.dla_serielum      serie,
       da.dla_nodo          nodo,
       da.dla_pto_fisico    punto_fisico,
       da.dla_corregimiento cod_cgto,
       co.co_nombre         nombre_corregimiento,
       da.dla_barrio        cod_barrio,
       ba.ba_nombarrio      nombre_barrio,
       da.dla_longitud      longitud,
       da.dla_latitud       latitud,
       da.dla_tipo_lampara  cod_tipo_lampara,
       tl.tl_descripcion    nombre_tipo_lampara,
       da.dla_nodo_anterior nodo_anterior,
       tl.tl_potencia_balasto potencia_balasto,
       tl.tl_vatios         potencia_kva,
       da.dla_tipo_encendido tipo_encendido,
       da.dla_funcionando    funcionando,
       da.dla_observacion    cod_observacion,
       ta.descripcion        des_observacion,
       da.dla_direccion      direccion,
       da.dla_cod_referencia cliente_ref,
       da.dla_observa_gral   obs_gral

		from dato_lev_alumbrado da
		    left join materiales         m  on m.ma_codigo=da.dla_codigomat
		    left join descripcion_series ds on ds.codigo=da.dla_codmarca and  ds.grupo=m.ma_grupo
		    left join corregimientos     co on co.co_depto = da.dla_dpto and co.co_municipio = da.dla_mpio and co.co_codcorregimiento = da.dla_corregimiento
		    left join barrios            ba on ba.ba_depto = da.dla_dpto and ba.ba_mpio = da.dla_mpio and ba.ba_sector=da.dla_corregimiento and ba.ba_codbarrio=da.dla_barrio
		    left join tipos_lamparas     tl on tl.tl_codigo = da.dla_tipo_lampara and tl.tl_vatios=da.dla_potencia
		    left join tablas_aforomantenimiento ta on ta.tipo='OBS LAMPARAS' and ta.codigo=da.dla_observacion
		where da.dla_acta='".$orden."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		// echo "<pre>";
		// 	var_dump($fila);
		// echo "</pre>";

		$row_array['luminaria'] = utf8_encode($fila[0]);
		$row_array['codMaterial'] = utf8_encode($fila[1]);
		$row_array['nombreMaterial'] = utf8_encode($fila[2]);
		$row_array['codMarca'] = utf8_encode($fila[3]);
		$row_array['nombreMarca'] = utf8_encode($fila[4]);
		$row_array['serie'] = utf8_encode($fila[5]);
		$row_array['nodo'] = utf8_encode($fila[6]);
		$row_array['ptoFisico'] = utf8_encode($fila[7]);
		$row_array['codCorregimiento'] = utf8_encode($fila[8]);
		$row_array['nombreCorregimiento'] = utf8_encode($fila[9]);
		$row_array['codBarrio'] = utf8_encode($fila[10]);
		$row_array['nombreBarrio'] = utf8_encode($fila[11]);
		$row_array['longitud'] = utf8_encode($fila[12]);
		
		$row_array['latitud'] = utf8_encode($fila[13]);
		$row_array['codTipoLampara'] = utf8_encode($fila[14]);
		$row_array['nombreTipoLampara'] = utf8_encode($fila[15]);
		$row_array['nodoAnterior'] = utf8_encode($fila[16]);
		$row_array['potenciaBalasto'] = utf8_encode($fila[17]);
		$row_array['potenciaKVA'] = utf8_encode($fila[18]);
		$row_array['tipoEncendido'] = utf8_encode($fila[19]);
		$row_array['funcionando'] = utf8_encode($fila[20]);
		$row_array['codObservacion'] = utf8_encode($fila[21]);
		$row_array['desObservacion'] = utf8_encode($fila[22]);
		$row_array['direccion'] = utf8_encode($fila[23]);
		$row_array['clienteRef'] = utf8_encode($fila[24]);
		// $row_array['nombreBarrio'] = utf8_encode($fila[25]);
		$row_array['obs_gral'] = utf8_encode($fila[25]);
		
								
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>