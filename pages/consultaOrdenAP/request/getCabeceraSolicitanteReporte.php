<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$orden=$_POST["orden"];
		
	$consulta = "SELECT o.oa_numero  orden_numero, 
		lc.ca_campana cod_campana,
       c.c_nombre nombre_campana,
       lc.ca_fechaej f_ejec,
       lc.ca_horaej  hora_ejec,
       lc.ca_tecnico cod_funcionario,
       t.te_nombres  nombre_funcionario,
       lc.ca_supervisor cod_supervisor,
       s.su_nombre nombre_supervisor,
       lc.ca_cuadrilla cod_cuadrilla,
       lc.ca_placa placa,
       dc.dc_nic cedula_testigo,
       dc.dc_nombre nombre_testigo,
       o.oa_cedula cedula_solicitante,
       o.oa_apellido1 primer_apellido,
       o.oa_apellido2 segundo_apellido,
       o.oa_nombre1   primer_nombre,
       o.oa_nombre2   segundo_nombre,
       o.oa_dpto      cod_departamento,
       d.de_nombre    nombre_departamento,
       o.oa_mpio      cod_municipio,
       m.mu_nombre    nombre_municipio,
       o.oa_corregimiento cod_corregimiento,
       co.co_nombre      nombre_corregimiento,
       o.oa_barrio       cod_barrio,
       ba.ba_nombarrio   nombre_barrio,
       o.oa_telefonos    telefonos,
       o.oa_direccion    direccion,
       o.oa_pto_ref      punto_referencia,
       o.oa_fecha_reporte  fecha_reporte,
       o.oa_hora_reporte   hora_reporte,
       o.oa_tos            cod_tos,
       tos.to_descripcion  nombre_tos,
       o.oa_estado_lum     cod_estado_lum,
       ta.descripcion      nombre_estado_lum,
       o.oa_serie          serie_lum,
       o.oa_poste          numero_poste,
       o.oa_obs            obs,
       o.oa_nic            nic,
       su.su_nombre        nombre_nic,
       su.su_direccion     direccion_nic,
       lc.ca_observa
	from ot_ap o
	 left join tecnicos            t on t.te_codigo=o.oa_tecnico
	 left join lega_cabecera       lc on lc.ca_acta= cast(o.oa_numero as varchar(20))
	 left join campanas            c on c.c_codigo=lc.ca_campana
	 left join supervisores        s on s.su_codigo = lc.ca_supervisor
	 left join dato_clientes       dc on dc.dc_acta=cast(o.oa_numero as varchar(20))
	 left join departamentos       d on d.de_codigo = o.oa_dpto
	 left join municipios          m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio
	 left join corregimientos      co on co.co_depto = o.oa_dpto and co.co_municipio=o.oa_mpio and co.co_codcorregimiento=o.oa_corregimiento
	 left join barrios             ba on ba.ba_depto=o.oa_dpto and ba.ba_mpio=o.oa_mpio and ba.ba_sector=o.oa_corregimiento and ba.ba_codbarrio=o.oa_barrio
	 left join tipo_orden_servicio tos on tos.to_codigo=o.oa_tos
	 left join tablas_aforomantenimiento ta on ta.tipo='OBS LAMPARAS' and ta.codigo=o.oa_estado_lum
	 left join suscriptores        su on su.su_codigo=o.oa_nic
	where o.oa_numero=".$orden."";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['orden'] = utf8_encode($fila[0]);
		$row_array['codCampana'] = utf8_encode($fila[1]);
		$row_array['nombreCampana'] = utf8_encode($fila[2]);
		$row_array['fechaEjecucion'] = utf8_encode($fila[3]);
		$row_array['horaEjecucion'] = utf8_encode($fila[4]);
		$row_array['codFuncionario'] = utf8_encode($fila[5]);
		$row_array['nombreFuncionario'] = utf8_encode($fila[6]);
		$row_array['codSupervisor'] = utf8_encode($fila[7]);
		$row_array['nombreSupervisor'] = utf8_encode($fila[8]);
		$row_array['codCuadrilla'] = utf8_encode($fila[9]);
		$row_array['placa'] = utf8_encode($fila[10]);
		$row_array['cedulaTestigo'] = utf8_encode($fila[11]);
		$row_array['nombreTestigo'] = utf8_encode($fila[12]);
		
		$row_array['cedulaSolicitante'] = utf8_encode($fila[13]);
		$row_array['apellido1Solicitante'] = utf8_encode($fila[14]);
		$row_array['apellido2Solicitante'] = utf8_encode($fila[15]);
		$row_array['nombre1Solicitante'] = utf8_encode($fila[16]);
		$row_array['nombre2Solicitante'] = utf8_encode($fila[17]);
		$row_array['codDepartamento'] = utf8_encode($fila[18]);
		$row_array['nombreDepartamento'] = utf8_encode($fila[19]);
		$row_array['codMunicipio'] = utf8_encode($fila[20]);
		$row_array['nombreMunicipio'] = utf8_encode($fila[21]);
		$row_array['codCorregimiento'] = utf8_encode($fila[22]);
		$row_array['nombreCorregimiento'] = utf8_encode($fila[23]);
		$row_array['codBarrio'] = utf8_encode($fila[24]);
		$row_array['nombreBarrio'] = utf8_encode($fila[25]);
		$row_array['telefonos'] = utf8_encode($fila[26]);
		$row_array['direccion'] = utf8_encode($fila[27]);
		$row_array['puntoReferencia'] = utf8_encode($fila[28]);

		$row_array['fechaReporte'] = utf8_encode($fila[29]);
		$row_array['horaReporte'] = utf8_encode($fila[30]);
		$row_array['codTos'] = utf8_encode($fila[31]);
		$row_array['nombreTos'] = utf8_encode($fila[32]);
		$row_array['codEstadoLuminaria'] = utf8_encode($fila[33]);
		$row_array['nombreEstadoLuminaria'] = utf8_encode($fila[34]);
		$row_array['serieLuminaria'] = utf8_encode($fila[35]);
		$row_array['numeroPoste'] = utf8_encode($fila[36]);
		$row_array['obs'] = utf8_encode($fila[37]);
		$row_array['nic'] = utf8_encode($fila[38]);
		$row_array['nombreNic'] = utf8_encode($fila[39]);
		$row_array['direccionNic'] = utf8_encode($fila[40]);
		$row_array['ca_observa'] = utf8_encode($fila[41]);

		
								
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>