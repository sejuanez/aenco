<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">

  <title>Alumbrado público</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

  <link rel='stylesheet' href='css/tablet.css' type='text/css'
        media='only screen and (min-width:481px) and (max-width:831px)'/>

  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  <style>
    #txtObservacion{width: 302%}
    @media only screen and (max-width: 768px) {
      #txtObservacion{width: 100%}
    }
    
  </style>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>


  <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
  <script type="text/javascript" src="../../js/gmaps.js"></script>

  <script type="text/javascript">

    $(function () {
      var map;
      var marker;

      $("#consultando").hide();

      //$("#contenido").hide();

      function resetearTodo() {
        $(".reset").val("");

        $("#tabla-luminaria").html("<tr class='cabecera'>" +
          "<td>Luminaria</td>" +
          "<td>CodigoMat</td>" +
          "<td>Marca</td>" +
          "<td>Serie</td>" +
          "<td>Nodo</td>" +
          "<td>Punto</td>" +
          "<td>Lámpara</td>" +
          /*"<td>Potencia</td>"+*/
          "<td>Encendido</td>" +
          "</tr>");

        $("#tabla-materiales").html("<tr class='cabecera'>" +
          "<td>CodMater</td>" +
          "<td>Desmater</td>" +
          "<td>CanMater</td>" +
          "<td>ValorMat</td>" +
          "<td>TipoMat</td>" +
          "<td>RI</td>" +
          "<td>Obs</td>" +
          "</tr>");
        $("#tabla-adecuaciones").html("<tr class='cabecera'>" +
          "<td>Código</td>" +
          "<td>Nombre</td>" +
          "</tr>");

      };

      function cargarMapa() {
        /*GMaps.geolocate({
            success: function(position){*/

        map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
          el: '#mapa',
          lat: 8.749349, /*position.coords.latitude,*/
          lng: -75.879568, /*position.coords.longitude,*/
          zoom: 10
        });


        marker = new google.maps.Marker({
          position: {lat: 8.749349 /*position.coords.latitude*/, lng: -75.879568 /*position.coords.longitude*/},
          //map: map
          //title: 'Hello World!'
        });

        map.addMarker(marker);

        /*},
        error: function(error){
           alert('Fallo en la geolocalizacion:'+error.message);
           return false;
        },
        not_supported:function(){
          alert('Geolocalizacion no soportada');
          return false;
        }
    });*/
      };
      cargarMapa();

      //========funcion buscar=================================
      function buscar(evt) {

        evt.preventDefault();

        $("#txtOrden").attr("disabled", true);
        //$("#buscar").button("disable");
        $("#consultar").hide();
        $("#contenido").hide();
        $("#consultando").show();

        var orden = $("#txtOrden").val();

        resetearTodo();

        var doneAjax1 = doneAjax2 = doneAjax3 = doneAjax4 = doneAjax5 = false;

        /* Ajax getCabeceraSolicitanteReporte.php*/
        $.ajax({
          url: 'request/getCabeceraSolicitanteReporte.php',
          type: 'POST',
          dataType: 'json',
          data: {orden: orden}
        }).done(function (repuesta) {

          if (repuesta.length > 0) {

            /*cabecera*/
            $("#txtCampana").val(repuesta[0].codCampana + "-" + repuesta[0].nombreCampana);
            $("#txtFEjec").val(repuesta[0].fechaEjecucion);
            $("#txtHoraEjec").val(repuesta[0].horaEjecucion);
            $("#txtCedulaTestigo").val(repuesta[0].cedulaTestigo);
            $("#txtNombreTestigo").val(repuesta[0].nombreTestigo);
            $("#txtFuncionario").val(repuesta[0].codFuncionario + "-" + repuesta[0].nombreFuncionario);
            $("#txtSupervisor").val(repuesta[0].codSupervisor + "-" + repuesta[0].nombreSupervisor);
            $("#txtCuadrilla").val(repuesta[0].codCuadrilla);
            $("#txtPlaca").val(repuesta[0].placa);
            $("#txtObservacion").val(repuesta[0].ca_observa);

            console.log(repuesta[0]);


            /*Solicitante*/
            $("#txtCedulaSolicitante").val(repuesta[0].cedulaSolicitante);
            $("#txtApellido1Solicitante").val(repuesta[0].apellido1Solicitante);
            $("#txtApellido2Solicitante").val(repuesta[0].apellido2Solicitante);
            $("#txtNombre1Solicitante").val(repuesta[0].nombre1Solicitante);
            $("#txtNombre2Solicitante").val(repuesta[0].nombre2Solicitante);
            $("#txtDepartamentoSolicitante").val(repuesta[0].codDepartamento + "-" + repuesta[0].nombreDepartamento);
            $("#txtMunicipioSolicitante").val(repuesta[0].codMunicipio + "-" + repuesta[0].nombreMunicipio);
            $("#txtCorregimientoSolicitante").val(repuesta[0].codCorregimiento + "-" + repuesta[0].nombreCorregimiento);
            $("#txtBarrioSolicitante").val(repuesta[0].codBarrio + "-" + repuesta[0].nombreBarrio);
            $("#txtDireccionSolicitante").val(repuesta[0].direccion);
            $("#txtTelefonoSolicitante").val(repuesta[0].telefonos);
            $("#txtPtoRefSolicitante").val(repuesta[0].puntoReferencia);


            /*Reporte*/
            $("#txtfechaReporte").val(repuesta[0].fechaReporte);
            $("#txtHoraReporte").val(repuesta[0].horaReporte);
            $("#txtTosReporte").val(repuesta[0].codTos + "-" + repuesta[0].nombreTos);
            $("#txtEstadoLumReporte").val(repuesta[0].codEstadoLuminaria + "-" + repuesta[0].nombreEstadoLuminaria);
            $("#txtSerieLumReporte").val(repuesta[0].serieLuminaria);
            $("#txtPosteReporte").val(repuesta[0].numeroPoste);
            $("#txtObsReporte").val(repuesta[0].obs);
            $("#txtNicReporte").val(repuesta[0].nic);
            $("#txtNombreReporte").val(repuesta[0].nombreNic);
            $("#txtDireccionReporte").val(repuesta[0].direccionNic);
          } else {
            //alert("No se encontraron datos para la cabecera, solicitante y reporte con este número de acta");

          }

          doneAjax1 = true;

          if (doneAjax1 && doneAjax2 && doneAjax3 && doneAjax4 && doneAjax5) {
            $("#txtOrden").attr("disabled", false);
            $("#consultando").hide();
            $("#consultar").show();
            $("#contenido").show();
          }


        });
        /*Fin Ajax getCabeceraSolicitanteReporte.php*/


        /*Ajax luminarias.php*/
        $.ajax({
          url: 'request/getLuminarias.php',
          type: 'POST',
          dataType: 'json',
          data: {orden: orden}
        }).done(function (repuesta) {

          if (repuesta.length > 0) {

            var filaActiva = 0;

            /*tabla*/
            var filas = "<tr class='cabecera'>" +
              "<td>Luminaria</td>" +
              "<td>CodigoMat</td>" +
              "<td>Marca</td>" +
              "<td>Serie</td>" +
              "<td>Nodo</td>" +
              "<td>Punto</td>" +
              "<td>Lámpara</td>" +
              /*"<td>Potencia</td>"+*/
              "<td>Encendido</td>" +
              "</tr>";

            for (var i = 0; i < repuesta.length; i++) {
              filas += "<tr name='" + i + "' class='fila'>" +
                "<td>" + repuesta[i].luminaria + "</td><td>" + repuesta[i].codMaterial + "</td><td>" + repuesta[i].nombreMarca + "</td><td>" + repuesta[i].serie + "</td><td>" + repuesta[i].nodo + "</td><td>" + repuesta[i].ptoFisico + "</td><td>" + repuesta[i].codTipoLampara + "</td><td>"/*+repuesta[i].potenciaKVA+"</td><td>"*/ + repuesta[i].tipoEncendido + "</td>" +
                "</tr>";
            }

            $("#tabla-luminaria").html(filas);

            marker.setPosition({lat: +repuesta[0].latitud, lng: +repuesta[0].longitud});
            map.setCenter({lat: +repuesta[0].latitud, lng: +repuesta[0].longitud});

            $("#tabla-luminaria tr.fila[name='0']").addClass("fila-activa");

            $("#txtTipoLampLuminaria").val(repuesta[0].codTipoLampara);
            $("#txtNodoAntLuminaria").val(repuesta[0].nodoAnterior);
            $("#txtPotBalastoLuminaria").val(repuesta[0].potenciaBalasto);
            $("#txtPotKvaLuminaria").val(repuesta[0].potenciaKVA);
            $("#txtTipoEncendidoLuminaria").val(repuesta[0].tipoEncendido);
            $("#txtFuncionandoLuminaria").val(repuesta[0].funcionando);
            $("#txtObservacionLuminaria").val(repuesta[0].codObservacion + "-" + repuesta[0].desObservacion);
            $("#txtDireccionLuminaria").val(repuesta[0].direccion);
            $("#txtClienteRefLuminaria").val(repuesta[0].clienteRef);
            $("#txtObsGralLuminaria").val(repuesta[0].obs_gral);
            $("#txtLuminaria").val(repuesta[0].luminaria);
            $("#txtCodigoLuminaria").val(repuesta[0].codMarca);
            $("#txtMarcaLuminaria").val(repuesta[0].nombreMarca);
            $("#txtSerieLuminaria").val(repuesta[0].serie);
            $("#txtNodoLuminaria").val(repuesta[0].nodo);
            $("#txtPtoFisicoLuminaria").val(repuesta[0].ptoFisico);
            $("#txtCorregimientoLuminaria").val(repuesta[0].codCorregimiento + "-" + repuesta[0].nombreCorregimiento);
            $("#txtBarrioLuminaria").val(repuesta[0].codBarrio + "-" + repuesta[0].nombreBarrio);
            $("#txtLongitudLuminaria").val(repuesta[0].longitud);
            $("#txtLatitudLuminaria").val(repuesta[0].latitud);


            $("#tabla-luminaria tr.fila").click(function () {

              var j = $(this).attr("name");

              if (j != filaActiva) {

                $("#tabla-luminaria tr.fila[name='" + filaActiva + "']").removeClass("fila-activa");
                $("#tabla-luminaria tr.fila[name='" + j + "']").addClass("fila-activa");

                filaActiva = j;
              }

              marker.setPosition({lat: +repuesta[j].latitud, lng: +repuesta[j].longitud});
              map.setCenter({lat: +repuesta[j].latitud, lng: +repuesta[j].longitud});

              $("#txtTipoLampLuminaria").val(repuesta[j].codTipoLampara);
              $("#txtNodoAntLuminaria").val(repuesta[j].nodoAnterior);
              $("#txtPotBalastoLuminaria").val(repuesta[j].potenciaBalasto);
              $("#txtPotKvaLuminaria").val(repuesta[j].potenciaKVA);
              $("#txtTipoEncendidoLuminaria").val(repuesta[j].tipoEncendido);
              $("#txtFuncionandoLuminaria").val(repuesta[j].funcionando);
              $("#txtObservacionLuminaria").val(repuesta[j].codObservacion + "-" + repuesta[j].desObservacion);
              $("#txtDireccionLuminaria").val(repuesta[j].direccion);
              $("#txtClienteRefLuminaria").val(repuesta[j].clienteRef);
              $("#txtObsGralLuminaria").val(repuesta[j].obsGeneral);
              $("#txtLuminaria").val(repuesta[j].luminaria);
              $("#txtCodigoLuminaria").val(repuesta[j].codMarca);
              $("#txtMarcaLuminaria").val(repuesta[j].nombreMarca);
              $("#txtSerieLuminaria").val(repuesta[j].serie);
              $("#txtNodoLuminaria").val(repuesta[j].nodo);
              $("#txtPtoFisicoLuminaria").val(repuesta[j].ptoFisico);
              $("#txtCorregimientoLuminaria").val(repuesta[j].codCorregimiento + "-" + repuesta[j].nombreCorregimiento);
              $("#txtBarrioLuminaria").val(repuesta[j].codBarrio + "-" + repuesta[j].nombreBarrio);
              $("#txtLongitudLuminaria").val(repuesta[j].longitud);
              $("#txtLatitudLuminaria").val(repuesta[j].latitud);

            });
          } else {
            //alert("No se encontraron datos para las luminarias con este número de acta");

          }


          doneAjax2 = true;

          if (doneAjax1 && doneAjax2 && doneAjax3 && doneAjax4 && doneAjax5) {
            $("#txtOrden").attr("disabled", false);
            $("#consultando").hide();
            $("#consultar").show();
            $("#contenido").show();
          }
        });
        /*Fin Ajax luminarias.php*/


        /*Ajax materiales.php*/
        $.ajax({
          url: 'request/getMateriales.php',
          type: 'POST',
          dataType: 'json',
          data: {orden: orden}
        }).done(function (repuesta) {

          if (repuesta.length > 0) {

            //var filaActiva=0;

            /*tabla*/
            var filas = "<tr class='cabecera'>" +
              "<td>CodMater</td>" +
              "<td>Desmater</td>" +
              "<td>CanMater</td>" +
              "<td>ValorMat</td>" +
              "<td>TipoMat</td>" +
              "<td>RI</td>" +
              "<td>Obs</td>" +
              "</tr>";

            for (var i = 0; i < repuesta.length; i++) {
              //filas+="<tr name='"+i+"' class='fila'>"+
              filas += "<tr class='fila'>" +
                "<td>" + repuesta[i].codigoMaterial + "</td><td>" + repuesta[i].nombreMaterial + "</td><td>" + repuesta[i].cantidadMaterial + "</td><td>" + repuesta[i].valorMaterial + "</td><td>" + repuesta[i].tipoMaterial + "</td><td>" + repuesta[i].RI + "</td><td>" + repuesta[i].observacion + "</td>" +
                "</tr>";
            }

            $("#tabla-materiales").html(filas);

            //$("#tabla-luminaria tr.fila[name='0']").addClass("fila-activa");


            /*$("#tabla-materiales tr.fila").click(function(){

                      var j=$(this).attr("name");

                      if(j!=filaActiva){

                          $("#tabla-luminaria tr.fila[name='"+filaActiva+"']").removeClass("fila-activa");
                          $("#tabla-luminaria tr.fila[name='"+j+"']").addClass("fila-activa");

                          filaActiva=j;
                      }

                  });*/
          } else {
            //alert("No se encontraron datos para los materiales con este número de acta");

          }


          doneAjax3 = true;

          if (doneAjax1 && doneAjax2 && doneAjax3 && doneAjax4 && doneAjax5) {
            $("#txtOrden").attr("disabled", false);
            $("#consultando").hide();
            $("#consultar").show();
            $("#contenido").show();
          }
        });
        /*fin Ajax materiales.php*/


        /*Ajax adecuaciones.php*/
        $.ajax({
          url: 'request/getAdecuaciones.php',
          type: 'POST',
          dataType: 'json',
          data: {orden: orden}
        }).done(function (repuesta) {

          if (repuesta.length > 0) {


            /*tabla*/
            var filas = "<tr class='cabecera'>" +
              "<td>Código</td>" +
              "<td>Nombre</td>" +
              "</tr>";

            for (var i = 0; i < repuesta.length; i++) {
              //filas+="<tr name='"+i+"' class='fila'>"+
              filas += "<tr class='fila'>" +
                "<td>" + repuesta[i].codigo + "</td><td>" + repuesta[i].nombre + "</td>" +
                "</tr>";
            }

            $("#tabla-adecuaciones").html(filas);


          } else {
            //alert("No se encontraron datos para las adecuaciones con este número de acta");

          }


          doneAjax4 = true;

          if (doneAjax1 && doneAjax2 && doneAjax3 && doneAjax4 && doneAjax5) {
            $("#txtOrden").attr("disabled", false);
            $("#consultando").hide();
            $("#consultar").show();
            $("#contenido").show();
          }
        });
        /*Fin del Ajax adecuaciones.php*/


        $.ajax({
          url: 'request/listarEvidencias.php',
          type: 'POST',
          dataType: 'json',
          data: {orden: orden}

        }).done(function (repuesta) {


          if (repuesta.length > 0) {

            $("#list-img").html("");//resetamos la lista de imagenes
            $("#list-img").animate({left: 0});//resetamos la lista de imagenes
            $("#img-max").html("");//reseteamos a la imagen maximizada
            $("#title-img").html("");//reseteamos el titulo de la imagen maximizada

            var arr_img = {};//vector q almacena las imagenes de cada nodo

            for (var j = 0; j < repuesta.length; j++) {//for que carga las imagenes del acta
              arr_img[j] = new Image();
              arr_img[j].onload = function () {
              };
              arr_img[j].src = repuesta[j].url_img;
              arr_img[j].name = j;
              arr_img[j].alt = "imagen " + (j + 1);
              //arr_img[j].width=600;
              //arr_img[j].height=400;
              arr_img[j].title = "Imagen " + (j + 1) + " de " + repuesta.length;
              $("#list-img").append(arr_img[j]);

            }//fin del for que carga las imagenes del acta

            /*var priImg= $("#list-img img:first-child").clone();

                $("#img-max").html(priImg);
                $("#list-img img:first-child").addClass("img-selected");
                $("#title-img").html("Imagen 1 de "+repuesta.length);

                $("#contenido").show();*/

            //$("#contenido").hide();
            //$("#menubar").hide();

            var zoom = 50;
            var rotacion = 0;
            var imgSeleccionada = null;
            $("#galeria").hide();

            $("#menubar-cerrar").click(function (e) {
              e.preventDefault();

              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              $("#galeria").hide();
              $("#img-max img").css("width", 50 + "%");
            });

            $("#menubar-aumentar").click(function (e) {
              e.preventDefault();
              if (zoom < 100) {
                zoom += 10;
                $("#img-max img").css("width", zoom + "%");
              }
            });

            $("#menubar-disminuir").click(function (e) {
              e.preventDefault();
              if (zoom > 50) {
                zoom -= 10;
                $("#img-max img").css("width", zoom + "%");
              }
            });

            $("#menubar-anterior").click(function (e) {
              e.preventDefault();

              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              if (imgSeleccionada > 0) {
                imgSeleccionada--;
                $("#img-max img").animate({opacity: 0}, 200, function () {
                  var img = $("#list-img img[name='" + imgSeleccionada + "']").clone();
                  $("#img-max").html(img);
                  $("#title-img").html("Imagen " + (imgSeleccionada + 1) + " de " + repuesta.length);
                  zoom = 50;
                  $("#img-max img").css("width", zoom + "%");
                  $("#img-max img").animate({opacity: 1}, 200, function () {
                    console.log("fin animacion");
                  });
                });

              }
            });

            $("#menubar-siguiente").click(function (e) {
              e.preventDefault();

              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              if (imgSeleccionada < repuesta.length - 1) {
                imgSeleccionada++;
                $("#img-max img").animate({opacity: 0}, 200, function () {
                  var img = $("#list-img img[name='" + imgSeleccionada + "']").clone();
                  $("#title-img").html("Imagen " + (imgSeleccionada + 1) + " de " + repuesta.length);
                  $("#img-max").html(img);
                  zoom = 50;
                  $("#img-max img").css("width", zoom + "%");
                  $("#img-max img").animate({opacity: 1}, 200, function () {
                    console.log("fin animacion");
                  });
                });

              }
            });

            $("#menubar-rotarIzq").click(function (e) {
              e.preventDefault();

              if (rotacion > -360) {
                rotacion = rotacion - 90;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });

              } else {
                rotacion = 0;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });
              }
              //console.log(rotacion);
            });

            $("#menubar-rotarDer").click(function (e) {
              e.preventDefault();

              if (rotacion < 360) {
                rotacion = rotacion + 90;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });

              } else {
                rotacion = 0;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });
              }
              //console.log(rotacion);
            });

            //EVENTO CLIC PARA CADA IMAGEN DEL CARRUSEL
            $("#list-img img").click(function (e) {
              e.preventDefault();
              //$("#list-img img").removeClass("img-selected");
              var img = $(this).clone();
              $("#img-max").html(img);
              //$(this).addClass("img-selected");
              $("#title-img").html("Imagen " + (+$(this).attr("name") + 1) + " de " + repuesta.length);
              imgSeleccionada = +$(this).attr("name");
              $("#galeria").show();

            });//fin del evento clic de las imagenes del carrusel


            $("#carrusel").show();
            $("#contenido").show();
            $("#modal").animate({right: '-100%'}, 500);
          }//fin del if si hay resultado
          else {
            $("#carrusel").hide();
            //alert("No se encontraron datos para las evidencias con este número de acta");
            //$("#contenido").hide();
            //$("#modal h1").html("Lo sentimos!");
            //$("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
          }

          doneAjax5 = true;

          if (doneAjax1 && doneAjax2 && doneAjax3 && doneAjax4 && doneAjax5) {
            $("#txtOrden").attr("disabled", false);
            $("#consultando").hide();
            $("#consultar").show();
            $("#contenido").show();
          }
        });//fin del done

      };
      //========fin de la funcion buscar========================

      document.getElementById("form").addEventListener("submit", buscar);

      $("#consultar").click(buscar);

      //alert("jquery ok");
      //$("#buscar").button();

      //$("#txtOrden").focus();
      document.getElementById("txtOrden").focus();

      $("#tabs").tabs({show: {effect: "slide", duration: 200}, active: 1});

      //cargarMapa();

      $("body").show();
      $("#txtOrden").focus();


    });//fin JQuery

  </script>

</head>
<body style="display:none">

<header>
  <h3>Alumbrado público</h3>
  <nav>
    <ul id="menu">

      <!--<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>-->
      <li id="consultar"><a href=""><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
      <li id="consultando"><a href=""><span class="ion-load-d"></span><h6>consultando...</h6></a></li>

    </ul>
  </nav>
</header>


<div id='subheader'>

  <div id="div-form">

    <form id="form">

      <div><label>Orden No.</label><input type="text" id="txtOrden" autofocus></div>

    </form>

  </div>

</div>


<div id="contenido">


  <div id="div_2">

    <div class="wrapper">

      <div><label>Campaña</label><input type="text" id="txtCampana" class="reset" readOnly></div>
      <div><label>F. Ejec.</label><input type="text" id="txtFEjec" class="reset" readOnly></div>
      <div><label>Hora Ejec.</label><input type="text" id="txtHoraEjec" class="reset" readOnly></div>
      <div><label>Funcionario</label><input type="text" id="txtFuncionario" class="reset" readOnly></div>
      <div><label>Cuadrilla</label><input type="text" id="txtCuadrilla" class="reset" readOnly></div>
      <div><label>Placa</label><input type="text" id="txtPlaca" class="reset" readOnly></div>
      <div><label>Supervisor</label><input type="text" id="txtSupervisor" class="reset" readOnly></div>
      <div><label>Cédula Testigo</label><input type="text" id="txtCedulaTestigo" class="reset" readOnly></div>
      <div><label>Nombre Testigo</label><input type="text" id="txtNombreTestigo" class="reset" readOnly></div>
      <div><label>Observacion</label><input type="text" id="txtObservacion" class="reset" readOnly></div>

    </div>
  </div>
  <!-- fin div_2 -->


  <div id="div_3">
    <div class="wrapper">
      <div id="tabs">
        <ul>
          <li><a href="#tab1">1. <span class="titulo-tab">Solicitante y Reporte</span></a></li>
          <li><a href="#tab4">2. <span class="titulo-tab">Luminarias</span></a></li>
          <li><a href="#tab5">3. <span class="titulo-tab">Materiales</span></a></li>
          <li><a href="#tab6">4. <span class="titulo-tab">Adecuaciones</span></a></li>
          <li><a href="#tab7">5. <span class="titulo-tab">Evidencias</span></a></li>
        </ul>

        <div id="tab1">

          <div>
            <input type="checkbox" id="solExterna" disabled>
            <span>Sol. Externa</span>
          </div>

          <div class="wrapper">


            <div>
              <label>Cédula</label>
              <input type="text" id="txtCedulaSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>1er. Apellido</label>
              <input type="text" id="txtApellido1Solicitante" class="reset" readOnly>
            </div>

            <div>
              <label>2do. Apellido</label>
              <input type="text" id="txtApellido2Solicitante" class="reset" readOnly>
            </div>

            <div>
              <label>1er. Nombre</label>
              <input type="text" id="txtNombre1Solicitante" class="reset" readOnly>
            </div>

            <div>
              <label>2do. Nombre</label>
              <input type="text" id="txtNombre2Solicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Departamento</label>
              <input type="text" id="txtDepartamentoSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Municipio</label>
              <input type="text" id="txtMunicipioSolicitante" class="reset" readOnly>
            </div>


          </div>

          <div class="wrapper">


            <div>
              <label>Corregimiento</label>
              <input type="text" id="txtCorregimientoSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Barrio/Vda</label>
              <input type="text" id="txtBarrioSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Dirección</label>
              <input type="text" id="txtDireccionSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Teléfono(s)</label>
              <input type="text" id="txtTelefonoSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Pto. Ref.</label>
              <input type="text" id="txtPtoRefSolicitante" class="reset" readOnly>
            </div>

            <div>
              <label>Fecha Rep.</label>
              <input type="text" id="txtfechaReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Hora Rep.</label>
              <input type="text" id="txtHoraReporte" class="reset" readOnly>
            </div>


          </div>

          <div class="wrapper">


            <div>
              <label>T.O.S</label>
              <input type="text" id="txtTosReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Estado Lum.</label>
              <input type="text" id="txtEstadoLumReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Serie</label>
              <input type="text" id="txtSerieLumReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Nro. Poste</label>
              <input type="text" id="txtPosteReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Obs.</label>
              <input type="text" id="txtObsReporte" class="reset" readOnly>
            </div>

            <div>
              <label>NIC</label>
              <input type="text" id="txtNicReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Nombre</label>
              <input type="text" id="txtNombreReporte" class="reset" readOnly>
            </div>

            <div>
              <label>Dirección</label>
              <input type="text" id="txtDireccionReporte" class="reset" readOnly>
            </div>
          </div>
          <!-- fin de wrapper tab1 -->
        </div>
        <!-- fin de tab1 -->


        <!--<div id="tab2">


                  </div>-->
        <!-- fin de tab2 -->

        <!--<div id="tab3">
                      <div class="wrapper">
                          <h3>tab3</h3>
                      </div>

                  </div>-->
        <!-- fin de tab3 -->

        <div id="tab4">
          <div class="wrapper">

            <div id="div-tabla">
              <div class="wrapper">
                <table id="tabla-luminaria">
                  <tr class="cabecera">
                    <td>Luminaria</td>
                    <td>CodigoMat</td>
                    <td>Marca</td>
                    <td>Serie</td>
                    <td>Nodo</td>
                    <td>Punto</td>
                    <td>Lámpara</td>
                    <!--<td>Potencia</td>-->
                    <td>Encendido</td>
                  </tr>
                  <!--<tr class="fila">
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>

                                      </tr>
                                      <tr class="fila">
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>
                                          <td>Vacío</td>

                                      </tr>-->
                </table>
              </div>

            </div>

            <div id="div-detalle">

              <div class="wrapper">

                <div>
                  <div>
                    <label>Tipo Lamp.</label>
                    <input type="text" id="txtTipoLampLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Nodo Ant.</label>
                    <input type="text" id="txtNodoAntLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Pot. Balasto</label>
                    <input type="text" id="txtPotBalastoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Pot. (KVA)</label>
                    <input type="text" id="txtPotKvaLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Tipo Encendido</label>
                    <input type="text" id="txtTipoEncendidoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Funcionando</label>
                    <input type="text" id="txtFuncionandoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Observación</label>
                    <input type="text" id="txtObservacionLuminaria" class="reset" readOnly>
                  </div>

                </div>


                <div>

                  <div>
                    <label>Dirección</label>
                    <input type="text" id="txtDireccionLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Cliente Ref.</label>
                    <input type="text" id="txtClienteRefLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Obs. Gral.</label>
                    <input type="text" id="txtObsGralLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Luminaria</label>
                    <input type="text" id="txtLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Código</label>
                    <input type="text" id="txtCodigoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Marca</label>
                    <input type="text" id="txtMarcaLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Serie</label>
                    <input type="text" id="txtSerieLuminaria" class="reset" readOnly>
                  </div>

                </div>


                <div>

                  <div>
                    <label>Nodo</label>
                    <input type="text" id="txtNodoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Pto. Físico</label>
                    <input type="text" id="txtPtoFisicoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Corregimiento</label>
                    <input type="text" id="txtCorregimientoLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Barrio/Vda</label>
                    <input type="text" id="txtBarrioLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Longitud</label>
                    <input type="text" id="txtLongitudLuminaria" class="reset" readOnly>
                  </div>

                  <div>
                    <label>Latitud</label>
                    <input type="text" id="txtLatitudLuminaria" class="reset" readOnly>
                  </div>

                </div>

              </div>
              <div id="div-map">
                <div id="mapa"></div>
              </div>
            </div>


          </div>

          <!-- fin de wrapper tab4 -->
        </div>

        <!-- fin de tab4 -->

        <div id="tab5">
          <div class="wrapper">
            <table id="tabla-materiales">
              <tr class="cabecera">
                <td>CodMater</td>
                <td>Desmater</td>
                <td>CanMater</td>
                <td>ValorMat</td>
                <td>TipoMat</td>
                <td>RI</td>
                <td>Obs</td>
              </tr>
            </table>
          </div>
          <!-- fin de wrapper tab5 -->
        </div>
        <!-- fin de tab5 -->

        <div id="tab6">
          <div class="wrapper">
            <table id="tabla-adecuaciones">
              <tr class="cabecera">
                <td>Código</td>
                <td>Descripción</td>
              </tr>
            </table>
          </div>
          <!-- fin de wrapper tab6 -->
        </div>
        <!-- fin de tab6 -->

        <div id="tab7">
          <div class="wrapper">

            <div id='carrusel'>
              <!--<a href='#' id='carrusel-anterior'></a>
                          <a href='#' id='carrusel-siguiente'></a>-->
              <div id="list-img">
              </div>
            </div>


            <div id="galeria">
              <div id="menubar">
                <span id="title-img"></span>
                <a href="#" title="anterior" id="menubar-anterior" class="ion-ios-arrow-thin-left"></a>
                <a href="#" title="siguiente" id="menubar-siguiente" class="ion-ios-arrow-thin-right"></a>
                <a href="#" title="disminuir" id="menubar-disminuir" class="ion-ios-minus-empty"></a>
                <a href="#" title="aumentar" id="menubar-aumentar" class="ion-ios-plus-empty"></a>
                <a href="#" title="Rotar -45º" id="menubar-rotarIzq" class="ion-ios-undo-outline"></a>
                <a href="#" title="Rotar 45º" id="menubar-rotarDer" class="ion-ios-redo-outline"></a>
                <a href="#" title="cerrar" id="menubar-cerrar" class="ion-ios-close-empty"></a>
              </div>
              <div id="img-max">
              </div>
            </div>

          </div>
          <!-- fin de wrapper tab7 -->
        </div>
        <!-- fin de tab7 -->

      </div>
      <!-- fin de tabs -->
    </div>
    <!-- fin de wrapper -->
  </div>
  <!-- fin de div_3 -->
  <!--<div>fin </div>-->

</div><!-- fin div contenido -->


</body>
</html>