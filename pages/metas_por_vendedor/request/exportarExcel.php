<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=Metas_por_proveedor_vendedor_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	//$consulta = "SELECT v.ve_nombres vendedor, sum(p.ped_total) cumplimiento, coalesce(md.md_meta, 0) * count(distinct p.ped_fecha) meta, case when (coalesce(md.md_meta, 0) * count(distinct p.ped_fecha)) - (sum(p.ped_total)) < 0 Then 0 else (coalesce(md.md_meta, 0) * count(distinct p.ped_fecha))- (sum(p.ped_total)) end Pendiente FROM PEDINET P left join vendedor v on v.ve_cedula=p.ped_codven left join metas_dia_vendedor md on md.md_codvendedor=p.ped_codven and md.md_ano=extract(year from ped_fecha) and md.md_mes=extract(month from ped_fecha) WHERE P.ped_fecha between '".$fechaini."' and '".$fechafin."' group by v.ve_nombres, md.md_meta";
	$consulta = "SELECT v.ve_nombres vendedor, sum(p.ped_total) cumplimiento, sum(distinct md.md_meta) *  count(distinct p.ped_fecha) meta, case when (sum(distinct md.md_meta) *  count(distinct p.ped_fecha))- (sum(p.ped_total)) < 0 Then 0 else (sum(distinct md.md_meta) *  count(distinct p.ped_fecha))- (sum(p.ped_total)) end Pendiente FROM PEDINET P left join vendedor v on v.ve_cedula=p.ped_codven left join metas_dia_vendedor md on md.md_codvendedor=p.ped_codven and md.md_ano=extract(year from ped_fecha) and md.md_mes=extract(month from ped_fecha) WHERE P.ped_fecha between '".$fechaini."' and '".$fechafin."' group by v.ve_nombres";

	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Vendedor</td>".
						"<td>Cumplimiento</td>".
						"<td>Pendiente</td>".
						"<td>Meta</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>