<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Metas por vendedor</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});

			$( "#alert-detalle" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico-tecnico").width();
					var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico3").width();
					alto = +$("#div-grafico3").height();
					$("#grafico3").highcharts().setSize(ancho,alto, false);
				}
			});
			

			$(window).resize(function() {
			    	var ancho = +$("#div-grafico-tecnico").width();
			    	var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico3").width();
					alto = +$("#div-grafico3").height();
					$("#grafico3").highcharts().setSize(ancho,alto, false);
			});
		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores ($)'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            //valueSuffix: ' Pesos'
			            
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}/*,
						series:{
							events:{
								click:function(e){
									console.log('clic');
								}
							}
						}*/
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};


			function drawPieChart(rows, element, colors, titulo){
          			//var center = [100, 50];
         
			          $('#'+element).highcharts({
			              chart: {
			                  type: 'pie',
			                  /*options3d: {
			                      enabled: true,
			                      alpha: 45,
			                      beta: 0
			                  },*/
			                  backgroundColor:'#ffffff'
			              },
			              colors:colors,
			              title: {text:titulo, style:{fontSize:'14px'}, useHTML:true},
			              credits:{enabled:false},
			              tooltip: {
			                  pointFormat: '{point.y}<br><b>({point.percentage:.1f}%)</b>'
			              },
			              legend:{
			                align:'center',
			                layout: 'horizontal',
			                verticalAlign: 'bottom'/*,
			                floating:true*/
			              },
			              plotOptions: {
			                  pie: {
			                      //center:center,
			                      size: "100%",
			                      allowPointSelect: true,
			                      cursor: 'pointer',
			                      depth: 20,
			                      dataLabels: {
			                          enabled: false,
			                          //format: '{point.name}<br>({point.percentage:.0f}%)</br>'
			                      },
			                      showInLegend: true
			                  }
			              },
			              series: [{
			                  type: 'pie',
			                  //name: 'Browser share',
			                  innerSize: '50%',
			                  data: rows,
			                  dataLabels: {
			                      enabled: true,

			                      rotation: 0,
			                      formatter: function() {
			                                  
			                                  if(this.percentage>=4){
			                                     return this.percentage.toFixed(1) + ' %';
			                                  }
			                                 
			                                },
			                      distance: -20,
			                      
			                      color:'rgba(0,0,0,0.7)',
			                      //color: '#858585',
			                     
			                      style: {
			                          fontSize: '10px',
			                          fontFamily: 'Verdana, sans-serif',
			                          //textShadow: '1px 1px #ffffff'
			                      }
			                  }
			              }]
			          });

       		};






			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();

			$("#div-total").hide();
			$("#div-meta").hide();
			$("#div-cumplimiento").hide();

			

			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			


			//Listeners de el evento Change de las listas vendedores
			$("#vendedor").on("change",function(){

				
				var v = document.getElementById("vendedor");
				var cumplimiento=v.options[v.selectedIndex].dataset['cumplimiento'];
				var pendiente=v.options[v.selectedIndex].dataset['pendiente'];
				var nombreVendedor=v.options[v.selectedIndex].dataset['nombre'];

				$("#tabla-general tr.fila").removeClass("fila-activa");
	            $("#tabla-general tr.fila[name='"+v.selectedIndex+"']").addClass("fila-activa");
	            
				
				drawPieChart([{name:'Pendiente', y:+pendiente},{name:'Cumplimiento', y:+cumplimiento}],'grafico-municipio',['#ff0000', '#00ff00'],nombreVendedor);
					
			});	
			//fin de Listeners de el evento Change de la lista vendedores

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				$("#div-total").hide();
				$("#div-meta").hide();
				$("#div-cumplimiento").hide();
				


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



		        // ajax GENERAL

				$.ajax({
					url:'request/general.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){
									
											
						
						var filas="";
						var optionsVendedor="";
						var vendedores=[];						
						var cumplimientos=[];
						var metas=[];
						
						var total = totalMeta = totalCumplimiento=0;
						
						var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						

						for (var i=0; i<repuesta.length; i++) {

							vendedores[i] = repuesta[i].vendedor;
							cumplimientos[i] = +repuesta[i].cumplimiento;
							metas[i] = +repuesta[i].meta;

							total+= +repuesta[i].cumplimiento;
							totalMeta+= +repuesta[i].meta;
							

							optionsVendedor+="<option data-cumplimiento='"+repuesta[i].cumplimiento+"' data-pendiente='"+repuesta[i].pendiente+"' data-nombre='"+repuesta[i].vendedor+"' data-meta='"+repuesta[i].meta+"'>"+repuesta[i].vendedor+"</option>";
							
							filas+="<tr class='fila' name='"+i+"' data-cumplimiento='"+repuesta[i].cumplimiento+"' data-pendiente='"+repuesta[i].pendiente+"' data-nombre='"+repuesta[i].vendedor+"' data-meta='"+repuesta[i].meta+"'>"+
                              "<td>"+repuesta[i].vendedor+"</td><td>"+accounting.formatMoney(+repuesta[i].cumplimiento, {symbol : '$', precision : 0,thousand : ','})+"</td>"+"<td>"+accounting.formatMoney(+repuesta[i].pendiente, {symbol : '$', precision : 0,thousand : ','})+"</td>"+"<td>"+accounting.formatMoney(+repuesta[i].meta, {symbol : '$', precision : 0,thousand : ','})+"</td></tr>";
						}



						
						$("#total").html(accounting.formatMoney(total, {symbol : '$', precision : 0,thousand : ','}));		
						$("#div-total").show();

						$("#total-meta").html(accounting.formatMoney(totalMeta, {symbol : '$', precision : 0,thousand : ','}));		
						$("#div-meta").show();

						totalCumplimiento = (total/totalMeta)*100;

						$("#total-cumplimiento").html(totalCumplimiento.toPrecision(3)+"%"/*accounting.formatMoney(totalCumplimiento, {symbol : '$', precision : 0,thousand : ','})*/);		
						$("#div-cumplimiento").show();

						
						$("#vendedor").html(optionsVendedor);
						
						$("#tabla-general tbody").html(filas);

						


	                      $("#tabla-general tr.fila").click(function(){

	                            var nodo=$(this).attr("name");
	                            var cumplimiento=$(this).attr("data-cumplimiento");
	                            var pendiente=$(this).attr("data-pendiente");
	                            var nombreVendedor= $(this).attr("data-nombre");

	            	                            
	                            $("#tabla-general tr.fila").removeClass("fila-activa");
	                            $("#tabla-general tr.fila[name='"+nodo+"']").addClass("fila-activa");

	                            document.getElementById("vendedor").selectedIndex= +nodo;
	                             
	                            drawPieChart([{name:'Pendiente', y:+pendiente},{name:'Cumplimiento', y:+cumplimiento}],'grafico-municipio',['#ff0000', '#00ff00'], nombreVendedor);

	                            /*if(nodo!=nodoActivo){

	                                 
	                                  $("#div-tecnicos table tr.fila[name='"+nodoActivo+"']").addClass("item-nodo");
	                                  $("#div-tecnicos table tr.fila[name='"+nodoActivo+"']").removeClass("nodo-activo");
	                              
	                                  $("#div-tecnicos table tr.fila[name='"+nodo+"']").addClass("nodo-activo");
	                                  $("#div-tecnicos table tr.fila[name='"+nodo+"']").removeClass("item-nodo");

	                                  map.setCenter(lat,lon);
	                                  map.setZoom(18);

	                                  nodoActivo=nodo;
	                                               
	                            }*/// fin si nodo!=nodoActivo    
	                        });

							$("#tabla-general tr.fila[name='0']").addClass("fila-activa");


						var series=[
										{
											name:'Cumplimientos', 
											data:cumplimientos,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				//format: '${point.y}', // one decimal
				                              	formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true,
	                          				events:{
												click:function(e){
													//console.log(e.point.x+'=>'+e.point.y);
													var pendiente=+($("#tabla-general tr.fila[name='"+e.point.x+"']").attr("data-pendiente"));
													var cumplimiento=+($("#tabla-general tr.fila[name='"+e.point.x+"']").attr("data-cumplimiento"));
													var meta=+($("#tabla-general tr.fila[name='"+e.point.x+"']").attr("data-meta"));

													$("#meta-detalle").html(accounting.formatMoney(+meta, {symbol : '$', precision : 0,thousand : ','}));
													$("#cumplimiento-detalle").html(accounting.formatMoney(+cumplimiento, {symbol : '$', precision : 0,thousand : ','}));
													$( "#alert-detalle" ).dialog("option", "title", $("#tabla-general tr.fila[name='"+e.point.x+"']").attr("data-nombre"));
													$( "#alert-detalle" ).dialog("open");
													drawPieChart([{name:'Pendiente', y:+pendiente},{name:'Cumplimiento', y:+cumplimiento}],'graficoDetalle',['#ff0000', '#00ff00'], "");
												}
												
	                          				}
	                          			},
	                          			{
											name:'Meta',
											type:'spline',
											data:metas,
											dataLabels:{
												enabled:false,
												color: '#f00'
											},
											color:"green"
										}
									];

						
						$("#contenido").show();
						dibujarGraficoBarras("grafico-tecnico",vendedores,series,colors);
						drawPieChart([{name:'Pendiente', y:+repuesta[0].pendiente},{name:'Cumplimiento', y:+repuesta[0].cumplimiento}],'grafico-municipio',['#ff0000', '#00ff00'], repuesta[0].vendedor);


						if((+totalMeta - total)<0)
							drawPieChart([{name:'Pendiente', y:0},{name:'Cumplimiento', y:+(total)}],'grafico3',['#ff0000', '#00ff00'], "Cumplimiento empresa");
						else
							drawPieChart([{name:'Pendiente', y:(+totalMeta - total)},{name:'Cumplimiento', y:+(total)}],'grafico3',['#ff0000', '#00ff00'], "Cumplimiento empresa");
						

						$("#exportar a").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin);
						$("#exportar").show();


						
					}
					else{												
						$("#total").html("");		
						$("#div-total").hide();
						$("#total-meta").html("");		
						$("#div-meta").hide();
						$("#total-cumplimiento").html("");		
						$("#div-cumplimiento").hide();
						$("#vendedor").html();	
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");
						$("#exportar").hide();								
					}

					$("#consultando").hide();
					$("#consultar").show();
				});

			});//fin clic Consultar
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>

	<div id="alert-detalle" title="Detalle">
		<div><table><tr class='cabecera'><td>Meta</td><td>Cumplimiento</td></tr><tr class='fila'><td id='meta-detalle'></td><td id='cumplimiento-detalle'>"</td></tr></table></div>
		<div id='div-graficoDetalle' style="height:250px; width:250px"><div id='graficoDetalle' style="height:100%;"></div></div>
	</div>
	

	<header>
		<h3>Seguimiento de metas por vendedor</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>

				<div id="div-subtotales">

					<div id="div-total"> 
						<h5>Total preventas</h5>       
	                	<h4 id="total">0</h4>                	
              		</div>

              		<div id="div-meta"> 
	              		<h5>Meta</h5>       
	                	<h4 id="total-meta">0</h4>                	
	              	</div>

	              	<div id="div-cumplimiento"> 
	              		<h5>Cumplimiento</h5>       
	                	<h4 id="total-cumplimiento">0</h4>                	
	              	</div>

	              	
              	</div><!--fin div-subtotales -->
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>General</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Por vendedor</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Empresa</span></a></li>
			
			</ul>


			<div id='tab1'>
				<div id='div-grafico-tecnico'>
					<div id='grafico-tecnico'></div>
				</div>
			</div>


			<div id='tab2'>
				<div id='div-vendedor'><label>Vendedor</label><select id="vendedor" ></select></div>

				<div id='div-tabla-general'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Vendedor</td><td>Cumplimiento</td><td>Pendiente</td><td>Meta</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>

				
				<div id='div-grafico-municipio'>
					<div id='grafico-municipio'></div>
				</div>
				

			</div>


			<div id='tab3'>
				<div id='div-grafico3'>
					<div id='grafico3'></div>
				</div>
			</div>


		</div>

	</div>

	

</body>
</html>