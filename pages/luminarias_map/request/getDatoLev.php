<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");
    
	$consulta = "SELECT dla.dla_acta, dla.dla_longitud, dla.dla_latitud fROM dato_lev_alumbrado dla where dla.dla_longitud<>0 and dla.dla_direccion_map is null";

    $return_arr = array();

	$result = ibase_query($conexion,$consulta) or die(ibase_errmsg());

	while($fila = ibase_fetch_row($result)){
		
		$row_array['serielum'] = utf8_encode($fila[0]);
		$row_array['longitud'] = utf8_encode($fila[1]);
		$row_array['latitud'] = utf8_encode($fila[2]);

								
		array_push($return_arr, $row_array);
    }
    
	echo json_encode($return_arr);
?>