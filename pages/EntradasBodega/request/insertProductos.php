<?php
ini_set('max_execution_time', 300);
set_time_limit(3000);
include("gestion.php");

$data = $_POST['data'];


$data = json_decode($data);

$n = count($data);

$sql = 'SELECT GEN_ID(ENT, 1) NUMERO, CURRENT_DATE FECHA FROM RDB$DATABASE';

  $return_arr = array();

  $result = ibase_query($conexion, $sql);

  while($fila = ibase_fetch_row($result)){
    $row_array['NUMERO'] = utf8_encode($fila[0]);
    $date = new DateTime($fila[1]);
    $row_array['FECHA'] = $date->format('d-m-Y');
    array_push($return_arr, $row_array);
  }


try {

    $tr = ibase_trans();

    $sql1 = 'EXECUTE PROCEDURE SUMA_BODEGAP(?,?)';

    $sql2 = "INSERT INTO MOVIMIENTO_ALMACEN (SA_TIPO,
                                             SA_NUMERO,
                                             SA_FECHA,
                                             SA_CODIGO,
                                             SA_NOMBRE,
                                             SA_CODIGO_MA,
                                             SA_NOMBRE_MA,
                                             SA_CANTIDAD,
                                             SA_USUARIO,
                                             SA_DOCUMENTO,
                                             SA_VALOR,
                                             SA_CONCEPTO,
                                             SA_FECHA_INGRESO)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_DATE)";

    $sql3 = 'INSERT INTO BODEGA_SERIES (GRUPO,
                                        MARCA,
                                        SERIE,
                                        TIPO_DOCUMENTO,
                                        CODIGOMAT,
                                        NOMBRE_MAT,
                                        PROVEEDOR,
                                        NOMBRE_PROV,
                                        DOCUMENTO_E,
                                        CONCEPTO_E,
                                        FECHA_E,
                                        USUARIO_SISTEMA,
                                        PAQUETE,
                                        PROPIEDAD,
                                        ORDENCOMPRA,
                                        NUMERO_SOLICITUD)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

    $sql4 = 'INSERT INTO ACTAS_SERIES (AS_GRUPO,
                                       AS_MARCA,
                                       AS_SERIE,
                                       AS_CODMATER,
                                       AS_DESMATER,
                                       AS_CODPROVE,
                                       AS_NOMPROVE,
                                       AS_DOCUMENTO_E,
                                       AS_CONCEPTO_E,
                                       AS_FECHA_E,
                                       AS_USUARIO_E)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?)';

    $sql5 = 'INSERT INTO HIDESEBO (HB_NDOC,
                                    HB_TIPOMOV,
                                    HB_GRUPO,
                                    HB_MARCA,
                                    HB_SERIE,
                                    HB_CODIGO_MAT,
                                    HB_NOMBRE_MAT,
                                    HB_CODPROV,
                                    HB_NOMBRE_PROV,
                                    HB_CONCEPTO,
                                    HB_FECHADOC,
                                    HB_USUARIO_SISTEMA,
                                    HB_DOCEXT)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';


    $consulta1 = ibase_prepare($tr, $sql1);
    $consulta2 = ibase_prepare($tr, $sql2);
    $consulta3 = ibase_prepare($tr, $sql3);
    $consulta4 = ibase_prepare($tr, $sql4);
    $consulta5 = ibase_prepare($tr, $sql5);


    for ($i=0; $i < $n; $i++) {

          // ejecuta procedimiento
          $result1 = ibase_execute($consulta1,
                              $data[$i]->codigo,
                              $data[$i]->cantidad
                            );

          // INSERTA EN MOVIMIENTO ALMACEN
          $string = $data[$i]->fecha;
          $bad_chars ="/";
          $fecha = str_replace($bad_chars, '.', $string);

          // echo "".$fecha;

          $result2 = ibase_execute($consulta2,
                            'ENT',
                            $row_array['NUMERO'],
                            $fecha,
                            $data[$i]->idProveedor,
                            $data[$i]->proveedor,
                            $data[$i]->codigo,
                            $data[$i]->descripcion,
                            $data[$i]->cantidad,
                            $_SESSION['user'],
                            $data[$i]->nDoc,
                            $data[$i]->vlrUnitario,
                            $data[$i]->concepto
                          );

           $length = count($data[$i]->arraySeries);

          if ($length>0) {

                if($data[$i]->cod_grupo==='99'){

                    // inserta en acta_serie


                    for ($j=0; $j < $length; $j++) {

                      $result4 = ibase_execute($consulta4,
                                              $data[$i]->cod_grupo,
                                              $data[$i]->arraySeries[$j]->cod_marca,
                                              $data[$i]->arraySeries[$j]->serie,
                                              $data[$i]->codigo,
                                              $data[$i]->descripcion,
                                              $data[$i]->idProveedor,
                                              $data[$i]->proveedor,
                                              $data[$i]->nDoc,
                                              $data[$i]->concepto,
                                              $fecha,
                                              $_SESSION['user']
                                            );

                      $result5 = ibase_execute($consulta5,
                                              $row_array['NUMERO'],
                                              'ENT',
                                              $data[$i]->cod_grupo,
                                              $data[$i]->arraySeries[$j]->cod_marca,
                                              $data[$i]->arraySeries[$j]->serie,
                                              $data[$i]->codigo,
                                              $data[$i]->descripcion,
                                              $data[$i]->idProveedor,
                                              $data[$i]->proveedor,
                                              $data[$i]->concepto,
                                              $fecha,
                                              $_SESSION['user'],
                                              $data[$i]->nDoc
                                            );



                    }

                  }else{
                // //inserta en bodega_serie

                    for ($j=0; $j < $length; $j++) {

                          $result3 = ibase_execute($consulta3,
                                          $data[$i]->cod_grupo,
                                          $data[$i]->arraySeries[$j]->cod_marca,
                                          $data[$i]->arraySeries[$j]->serie,
                                          'ENT',
                                          $data[$i]->codigo,
                                          $data[$i]->descripcion,
                                          $data[$i]->idProveedor,
                                          $data[$i]->proveedor,
                                          $row_array['NUMERO'],
                                          $data[$i]->concepto,
                                          $fecha,
                                          $_SESSION['user'],
                                          $data[$i]->arraySeries[$j]->paquete,
                                          $data[$i]->arraySeries[$j]->idPropiedad,
                                          $data[$i]->arraySeries[$j]->idTipoMaterial,
                                          $data[$i]->arraySeries[$j]->nactivo

                                        );
                          $result5 = ibase_execute($consulta5,
                                              $row_array['NUMERO'],
                                              'ENT',
                                              $data[$i]->cod_grupo,
                                              $data[$i]->arraySeries[$j]->cod_marca,
                                              $data[$i]->arraySeries[$j]->serie,
                                              $data[$i]->codigo,
                                              $data[$i]->descripcion,
                                              $data[$i]->idProveedor,
                                              $data[$i]->proveedor,
                                              $data[$i]->concepto,
                                              $fecha,
                                              $_SESSION['user'],
                                              $data[$i]->nDoc
                                            );
                  }

                }

          }else{

          }

    }

ibase_commit($tr);

$return = array();
$return['response'] = 'success';
$return['numero'] = $row_array['NUMERO'];
$return['user'] = $_SESSION['user'];
$return['fechaIng'] = $row_array['FECHA'];


echo json_encode($return);

} catch (Exception $e) {
  ibase_rollback($tr);
  echo $e;
}

?>