<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=ventas_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	$consulta = "SELECT v.ve_nombres, c.cl_razonsocial,  sum(p.ped_total) vr_sin_iva from clientes c left join vendedor v on v.ve_cedula = c.cl_vendedor inner join pedinet p  on p.ped_codven=v.ve_cedula and p.ped_nitcli=c.cl_nit and p.ped_fecha between '".$fechaini."' and '".$fechafin."' group by  v.ve_nombres, c.cl_razonsocial";

	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Vendedor</td>".
						"<td>Razon social</td>".
						"<td>Valor sin IVA</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	
?>