<?php
  session_start();
  
  if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  }  
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Preventas vendedor</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
		
  <script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  <script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  <script type="text/javascript" src="../../js/accounting.js"></script>
    

  	<script type="text/javascript">
          
  		$(function(){

        $("#contenido").hide();

        $("#modal").hide();

        $( "#alert" ).dialog({
            modal: true,
            autoOpen: false,
            resizable:false,
             buttons: {
                  Ok: function() {
                    //$("#alert p").html("");
                    $( this ).dialog( "close" );
                  }
              }
        });

        $("#tabs").tabs({
          show: { effect: "slide", duration: 200 },
          active:0,
          activate: function( event, ui ) {

            var ancho = +$("#div-grafico-preventas").width();
            var alto = +$("#div-grafico-preventas").height();
            $("#grafico-preventas").highcharts().setSize(ancho,alto, false);

            ancho = +$("#div-grafico-clientesImpactados").width();
            alto = +$("#div-grafico-clientesImpactados").height();
            $("#grafico-clientesImpactados").highcharts().setSize(ancho,alto, false);
          }
        });

        $(window).resize(function() {
            var ancho = +$("#div-grafico-preventas").width();
            var alto = +$("#div-grafico-preventas").height();
            $("#grafico-preventas").highcharts().setSize(ancho,alto, false);

            ancho = +$("#div-grafico-clientesImpactados").width();
            alto = +$("#div-grafico-clientesImpactados").height();
            $("#grafico-clientesImpactados").highcharts().setSize(ancho,alto, false);
      });
        

        function drawColumnChart(rows, element, colors) {

         
           $('#'+element).highcharts({
                                  chart: {
                                      type: 'column',
                                      backgroundColor:'#ffffff'
                                  },
                                  colors:colors,
                                  credits:{enabled:false},
                                  title:null /*{
                                      text: 'Recaudo diario'
                                  }*/,
                                  /*subtitle: {
                                      //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                                      text:dia+'/'+mes+'/'+anio
                                  },*/
                                  xAxis: {
                                      type: 'category',
                                      labels: {
                                          //rotation: -45,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  },
                                  yAxis: {
                                      min: 0,
                                      title:null /*{
                                          text: 'Recaudo diario ($)'
                                      }*/
                                  },
                                  legend: {
                                      enabled: false
                                  },
                                  tooltip: {
                                      pointFormat: '<b>${point.y}</b>',
                                      pointFormatter: function(){return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});}
                                  },
                                  series: [{
                                      name: 'recaudo',
                                      data:rows,
                                      colorByPoint: true,
                                      dataLabels: {
                                          enabled: true,
                                          //rotation: -90,
                                          color: '#000',
                                          align: 'center',
                                         // format: '${point.y}', // one decimal
                                          formatter:function(){
                                              return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
                                          },
                                          y: 10, // 10 pixels down from the top
                                          padding:20,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  }]
                              });
        };

        function drawColumnChartSinFormato(rows, element, colors) {

         
           $('#'+element).highcharts({
                                  chart: {
                                      type: 'column',
                                      backgroundColor:'#ffffff'
                                  },
                                  colors:colors,
                                  credits:{enabled:false},
                                  title:null /*{
                                      text: 'Recaudo diario'
                                  }*/,
                                  /*subtitle: {
                                      //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                                      text:dia+'/'+mes+'/'+anio
                                  },*/
                                  xAxis: {
                                      type: 'category',
                                      labels: {
                                          //rotation: -45,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  },
                                  yAxis: {
                                      min: 0,
                                      title:null /*{
                                          text: 'Recaudo diario ($)'
                                      }*/
                                  },
                                  legend: {
                                      enabled: false
                                  },
                                  tooltip: {
                                      pointFormat: '<b>{point.y}</b>',
                                      pointFormatter: function(){return accounting.formatMoney(this.y, {symbol : '', precision : 0,thousand : ','});}
                                  },
                                  series: [{
                                      name: 'recaudo',
                                      data:rows,
                                      colorByPoint: true,
                                      dataLabels: {
                                          enabled: true,
                                          //rotation: -90,
                                          color: '#000',
                                          align: 'center',
                                         // format: '${point.y}', // one decimal
                                          formatter:function(){
                                              return accounting.formatMoney(this.y, {symbol : '', precision : 0,thousand : ','});
                                          },
                                          y: 10, // 10 pixels down from the top
                                          padding:20,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  }]
                              });
        };

        function drawPieChart(rows, element, colors){
          var center = [100, 50];
         
          $('#'+element).highcharts({
              chart: {
                  type: 'pie',
                  options3d: {
                      enabled: true,
                      alpha: 45,
                      beta: 0
                  },
                  backgroundColor:'#ffffff'
              },
              colors:colors,
              title: null,
              credits:{enabled:false},
              tooltip: {
                  pointFormat: '{point.y}<br><b>({point.percentage:.1f}%)</b>'
              },
              legend:{
                align:'right',
                layout: 'horizontal',
                verticalAlign: 'top'/*,
                floating:true*/
              },
              plotOptions: {
                  pie: {
                      //center:center,
                      size: "100%",
                      allowPointSelect: true,
                      cursor: 'pointer',
                      depth: 20,
                      dataLabels: {
                          enabled: false,
                          //format: '{point.name}<br>({point.percentage:.0f}%)</br>'
                      },
                      showInLegend: true
                  }
              },
              series: [{
                  type: 'pie',
                  //name: 'Browser share',
                  data: rows,
                  dataLabels: {
                      enabled: true,

                      rotation: 0,
                      formatter: function() {
                                  
                                  if(this.percentage>=4){
                                     return this.percentage.toFixed(1) + ' %';
                                  }
                                 
                                },
                      distance: -20,
                      
                      color:'rgba(0,0,0,0.7)',
                      //color: '#858585',
                     
                      style: {
                          fontSize: '10px',
                          fontFamily: 'Verdana, sans-serif',
                          //textShadow: '1px 1px #ffffff'
                      }
                  }
              }]
          });

        };

        function iniciarComponentes(){

            
           
            $("#consultando").hide();
            $("#exportar").hide();         
           

           $("#txtFechaFin").datepicker({
              minDate: new Date(),
              changeMonth: true,
              changeYear: true,
              dateFormat:"dd/mm/yy",
              dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
            });


            $("#txtFechaFin").datepicker("setDate", new Date());



      
            $("#txtFechaIni").datepicker({
              changeMonth: true,
              changeYear: true,
              dateFormat:"dd/mm/yy",
              dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
            });

            $("#txtFechaIni").datepicker("setDate", new Date());




            $("#txtFechaIni").change(function(){
        
              $("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
            });
               

          //$("#atras").button();

         //=============== Boton aceptar ====================
          //$("#consultar").button().click(function(){
          $("#consultar").click(function(e){

              e.preventDefault();
              $(this).hide();
              $("#contenido").hide();
              $("#consultando").show();
              $("#exportar").hide();
              

              
              //$("#div-totalrecaudo").fadeOut(200);
              $("#div-total-preventas").fadeOut(200);
              $("#div-grafico-preventas").fadeOut(200);

              $("#div-total-clientesImpactados").fadeOut(200);
              $("#div-grafico-clientesImpactados").fadeOut(200);

              
              var ajax1=ajax2=false;

              var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
              if(diaIni<10)
                diaIni="0"+diaIni;

              var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
              if(mesIni<10)
                mesIni="0"+mesIni;

              var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
              if(diaFin<10)
                diaFin="0"+diaFin;

              var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
              if(mesFin<10)
                mesFin="0"+mesFin;



        
              var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
            
              var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;


              $.ajax({//ajax PREVENTAS que carga las tabla  y el grafico de barras
                        url:'request/getPreventas.php',
                        type:'POST',
                        dataType:'json',
                        data:{fechaini:fechaIni, fechafin:fechaFin}

                    }).done(function(repuesta){
                       
                       
                        if(repuesta.length>0){// si hay tecnicos que mostrar

                          var colors=['#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
                          
                          
                          var hc_column=[];// vendedores preventa
                          var hc_column_clientesImpactados=[];// vendedores preventa
                          
                          var totalrecaudo = 0;
                          var totalClientesImpactados = 0;
                          
                         

                          for(var i=0; i<repuesta.length;i++)
                          {                           
                              
                              hc_column.push([repuesta[i].vendedor,+repuesta[i].recaudo]);
                              hc_column_clientesImpactados.push([repuesta[i].vendedor,+repuesta[i].clientes]);
                              
                              totalrecaudo+= (+repuesta[i].recaudo);//total preventas
                              totalClientesImpactados+= (+repuesta[i].clientes);//total clientes impactados
                          }

                                                    
                          totalrecaudo=accounting.formatMoney(totalrecaudo, {symbol : '$', precision : 0,thousand : ','});
                          $("#total-preventas").html(totalrecaudo);

                          totalClientesImpactados=accounting.formatMoney(totalClientesImpactados, {symbol : '', precision : 0,thousand : ','});
                          $("#total-clientesImpactados").html(totalClientesImpactados);
                          

                          drawColumnChart(hc_column,"grafico-preventas",colors);//DIBUJAR EL GRAFICO preventas
                          drawColumnChartSinFormato(hc_column_clientesImpactados,"grafico-clientesImpactados",colors);//DIBUJAR EL GRAFICO preventas
                          $("g.highcharts-data-labels>g[opacity=0]").attr("opacity",1);

                          $("#div-total-preventas").fadeIn(200);
                          $("#div-grafico-preventas").fadeIn(200);

                          $("#div-total-clientesImpactados").fadeIn(200);
                          $("#div-grafico-clientesImpactados").fadeIn(200);

                         
                        }//fin si hay tecnicos
                        else
                        {
                          //$("#totalrecaudo").html("---");
                          //$("#alert").dialog("open");
                          //alert("No se encontraron resultados!");
                          //$("#modal h1").html("Lo sentimos!");
                          //$("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                        }

                        ajax1=true;
                        if(ajax1&&ajax2){

                          $("#consultando").hide();
                          $("#consultar").show();
                          $("#contenido").show();
                        }
                      
              });//fin del preventas

             





        
              $.ajax({// ajax Tabla
                  url:'request/general.php',
                  type:'POST',
                  dataType:'json',
                  data:{fechaini:fechaIni , fechafin:fechaFin}

              }).done(function(repuesta){
          
          if(repuesta.length>0){

                        
                var filas="";

                for (var i=0; i<repuesta.length; i++) {
                  
                  filas+="<tr class='fila'>"+
                                  "<td>"+repuesta[i].vendedor+"</td><td>"+repuesta[i].raz+"</td>"+"<td>"+repuesta[i].valor+"</td></tr>";
                }
            
                $("#tabla-general tbody").html(filas);

                
                $("#exportar").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin);
                $("#exportar").show();
            
          }
          else{                       
            
                $("#tabla-general tbody").html("");
                $("#exportar").attr("href","");
                 $("#exportar").hide();             
          }

            ajax2=true;
            if(ajax1&&ajax2){

              $("#consultando").hide();
              $("#consultar").show();
              $("#contenido").show();
            }
          
        });
              

            
          });
         //=============== fin Boton aceptar ================
        };//fin de iniciarComponentes()

        //===================================================================================

        
         iniciarComponentes();                                      
                 
        //======================================================================================

        $("body").show();
        
       
  		});//fin del onready
  	</script>
</head>
  
  <body style="display:none">

    <div id="alert" title="Mensaje">
        <p>No hay datos para mostrar.</p>
    </div>

    <!--header-->
    <header>
      <h3>Preventas vendedores</h3>
      <nav>
        <ul id="menu">
          
          <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
          <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
            
        </ul>
      </nav>    
    </header>

    <!--subheader-->

    <div id='subheader'>

          <div id="div-form">
            <form id="form">
              <div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>               
              <div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>               
            </form>          
          </div>    
  </div>




    <div id="contenido">

        
        
        <div id='tabs'>

            <ul>
              <li><a href="#tab1">1. <span class='titulo-tab'>Preventas</span></a></li>
              <li><a href="#tab2">2. <span class='titulo-tab'>Clientes impactados</span></a></li>
              <li><a href="#tab3">3. <span class='titulo-tab'>Tabla</span></a></li>
            
            </ul>


            <div id='tab1'>
              <div id="div-total-preventas">
                <h3 id="total-preventas">245</h3>
                <h5>Total preventas</h5>
              </div>

              <div id='div-grafico-preventas'>
                <div id='grafico-preventas'></div>
              </div>
            </div>


            <div id='tab2'>
              
              <div id="div-total-clientesImpactados">
                <h3 id="total-clientesImpactados">245</h3>
                <h5>Total clientes</h5>
              </div>

              <div id='div-grafico-clientesImpactados'>
                <div id='grafico-clientesImpactados'></div>
              </div>
            </div>


            <div id='tab3'>

              <div id="div-exportar"><a href="" id="exportar"><span class="ion-ios-download-outline"></span><h6>Exportar</h6></a></div>
              <div id="div-tabla">
                <table id='tabla-general'>
                  
                    <thead>
                      <tr class='cabecera'><td>Vendedor</td><td>Razón social</td><td>Vlr. sin IVA</td></tr>
                    </thead>  

                    <tbody>
                    
                    </tbody>

                </table>
              </div>
            </div>

          </div>

         

      </div>


  </body>
</html>