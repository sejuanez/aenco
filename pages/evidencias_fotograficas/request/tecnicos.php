<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT * FROM tecnicos ORDER BY te_nombres ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['te_codigo'] = utf8_encode($row[0]);
		$row_array['te_nombres'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>