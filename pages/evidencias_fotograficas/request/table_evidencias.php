<?php
	// session_start();
  	// if(!$_SESSION['user']){
    //     echo"<script>window.location.href='../../inicio/index.php';</script>";
    //     exit();
    // }
	include("../../../init/gestion.php");
	
	$fecha_inicio = $_POST['fecha_inicial'];
	$fecha_final = $_POST['fecha_final'];
	$co_departamento = $_POST['departamento'];
	$co_municipio = $_POST['municipio'];
	$proceso = $_POST['proceso'];
	$co_tecnico = $_POST['tecnico'];
	$acta = $_POST['acta'];

	// echo 'fecha_inicio '.$fecha_inicio;
	// echo 'fecha_final '.$fecha_final;
	//  || empty($co_tecnico) || empty($acta)
	// lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL
	// AND lc.ca_depto = '$mu_depto' $param
	$param = "";

	if (empty($fecha_inicio) && empty($fecha_final)) {
		$param = "WHERE lc.ca_acta = '$acta' ";
	}

	if (!empty($co_tecnico)) {
		$param = "WHERE lc.ca_tecnico = '$co_tecnico' ";
	}

	if (!empty($fecha_inicio) && !empty($fecha_final)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL ";
	}

	if (!empty($fecha_inicio) && !empty($fecha_final) && !empty($co_departamento)) {
        $param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL AND lc.ca_depto = '$co_departamento' ";
    }

	if (!empty($fecha_inicio) && !empty($fecha_final) && !empty($co_departamento) && !empty($co_municipio)) {
        $param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL AND lc.ca_depto = '$co_departamento' AND lc.ca_municipio = '$co_municipio' ";
    }

	if (!empty($fecha_inicio) && !empty($fecha_final) && !empty($co_departamento) && !empty($co_municipio) && !empty($proceso)) {
        $param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL AND lc.ca_depto = '$co_departamento' AND lc.ca_municipio = '$co_municipio' AND lc.ca_campana = '$proceso' ";
    }

	if (!empty($fecha_inicio) && !empty($fecha_final) && !empty($co_departamento) && !empty($co_municipio) && !empty($proceso) && !empty($co_tecnico)) {
        $param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL AND lc.ca_depto = '$co_departamento' AND lc.ca_municipio = '$co_municipio' AND lc.ca_campana = '$proceso' AND lc.ca_tecnico = '$co_tecnico' ";
    }

	if (!empty($fecha_inicio) && !empty($fecha_final && !empty($co_tecnico))) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fecha_inicio' AND '$fecha_final'  AND lc.ca_fechaej IS NOT NULL AND lc.ca_tecnico = '$co_tecnico' ";
	}

	if (!empty($proceso)) {
	 	$param = "WHERE lc.ca_campana = '$proceso' ";
	}
	
	//  if (!empty($co_tecnico)) {
	//  	$param = "AND lc.ca_tecnico = '$co_tecnico' ";
	//  }
	
	//  if (!empty($acta)) {
	//  	$param = "AND lc.ca_acta = '$acta' ";
	//  }

	$query = "SELECT lc.ca_acta acta, lc.ca_fechaej fechaejecucion, t.te_nombres tecnico, c.c_nombre proceso, COALESCE(rs.r_descripcion, '') resultado, COALESCE(a.ac_descripcion, '') accion_anomalia,
		   '/'||EXTRACT(YEAR FROM lc.ca_fechaej)||'/'|| EXTRACT(MONTH FROM lc.ca_fechaej)||'/'|| EXTRACT(DAY FROM lc.ca_fechaej)||'/'||lc.ca_tecnico||'/' AS ruta, CAST(LIST(ea.ea_acta||'_'||ea.ea_nodo||'_'||ea.ea_no_foto, ', ') AS VARCHAR(5000)) AS foto
	FROM lega_cabecera lc
	  LEFT JOIN tecnicos t ON t.te_codigo = lc.ca_tecnico
	  LEFT JOIN campanas c ON c.c_codigo = lc.ca_campana
	  LEFT JOIN dato_unirre du ON du.ui_acta = lc.ca_acta AND du.ui_tipo = 'M'
	  LEFT JOIN resultado_scr rs ON rs.r_tiporden = lc.ca_codtipoordens AND rs.r_codigo = du.ui_codigo
	  LEFT JOIN dato_unirre dua ON dua.ui_acta = lc.ca_acta AND dua.ui_tipo = 'R'
	  LEFT JOIN evidencias_alumbrado ea ON ea.ea_acta = lc.ca_acta
	  LEFT JOIN acciones a ON a.ac_tiporden = lc.ca_codtipoordens AND a.ac_motivo = du.ui_codigo AND a.ac_codigo = dua.ui_codigo
	  $param GROUP BY lc.ca_acta, lc.ca_fechaej, t.te_nombres, c.c_nombre, COALESCE(rs.r_descripcion, ''), COALESCE(a.ac_descripcion, ''),
		   '/'||EXTRACT(YEAR FROM lc.ca_fechaej)||'/'|| EXTRACT(MONTH FROM lc.ca_fechaej)||'/'|| EXTRACT(DAY FROM lc.ca_fechaej)||'/'||lc.ca_tecnico||'/'";


	// echo $query;

	$return_arr = array();
	$arr_img = array();
	$row_array = array();
	
	$data = ibase_query($conexion, $query);
	
	$urlImg = "SELECT codempresa FROM EMPRESA";
	$img = ibase_query($conexion, $urlImg);
	while ($row = ibase_fetch_row($img)) {
		$row_array['codempresa'] = $row[0];
		array_push($arr_img, $row_array);
	}

	#path de la ruta
	$path = 'http://localhost/apartadoapp/init/';
	$path_ruta = $path.$row_array['codempresa'];
  
  	$root = dirname("c:/xampp/htdocs/apartadoapp/init/");

	while($fila = ibase_fetch_row($data)){

		// echo "<pre>";
		// 	print_r($fila);
		// echo "</pre>";
		// exit();

		$row_array['acta'] = $fila[0];
		$row_array['fechaejecucion'] = $fila[1];
		$row_array['tecnico'] = $fila[2];
		$row_array['proceso'] = $fila[3];
		$row_array['resultado'] = $fila[4];
		$row_array['accion_animalia'] = $fila[5];
		$row_array['ruta'] = $fila[6];

		$arr_foto = array();	
		if (!is_null($fila[7])) {
			$foto = explode(', ', $fila[7]);
			foreach($foto as $f) {
				$img = $path_ruta.$row_array['ruta'].$f.".jpg";
				if (@fopen($img, r)) {
					array_push($arr_foto, $img);
				}
			}
		}
		$row_array['imagen'] = $arr_foto; 
		array_push($return_arr, $row_array);
	}
	echo json_encode($return_arr);
?>