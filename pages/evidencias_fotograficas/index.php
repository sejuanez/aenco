<?php
  session_start();
   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo"<script>window.location.href='../inicio/index.php';</script>";
    exit();
  } 
?>

<!DOCTYPE html>

<html>
<head>
  <meta charset="UTF-8">
  <title>Asignar órdenes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <!-- <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' /> -->
  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' /> -->
  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css"> -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <style>
    .el-form-item--mini.el-form-item, .el-form-item--small.el-form-item {
      width: 100%;
    }
    .el-select {
      width: 100%;
    }
    #img-max {
      background-color: #000;
      width: 100%;
      height: 100%;
      /* overflow: scroll; */
      text-align: center;
    }
    #galeria {
      display: none;
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      /* overflow: scroll; */
      z-index: 100;
    }
    #img-max img {
      width: 50%;
      /* overflow: scroll; */
      transition: all .2s;
    }
    #img-max img {
      width: 50%;
      /* overflow: scroll; */
      transition: all .2s;
    }
    #menubar {
      background-color: #000;
      padding: 5px 20px;
      text-align: right;
    }
    #title-img {
      padding: 5px;
      color: white;
      font-size: 1rem;
      font-family: verdana,Arial,sans-serif;
      margin-right: 40px;
      vertical-align: 8px;
    }
    #list-img:first-child {
      border: 0px solid;
    }
    #list-img {
      width: 100%;
    }
    #list-img img {
      border: 1px solid #ddd;
      border-radius: 2px;
      padding: 3px;
      margin-right: 1rem;
    }
    #carrusel {
      position: relative;
      width: 100%;
      height: auto;
      margin: 4px auto;
      /* padding: 4px; */
      overflow: hidden;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    @media only screen and (max-width: 768px) {
      #list-img img{
      	width: 296px;
    		height: 170px;
      }
    }
  </style>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
 <!--  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script> -->
</head>
<body>
  <div id="app">
    <header>
      <h3>Asignar órdenes</h3>
      <nav>
        <ul id="menu">
          <li id="consultar">
          	<a @click.stop.prevent="tableEvidenvias" href="">
          		<span class="ion-ios-search-strong"></span><h6>Consultar</h6></a>
          </li>  
        </ul>
      </nav>
    </header>

	  <section style="padding-top: 44px; height: calc(100% - 80px); height: -webkit-calc(100% - 80px); height: -moz-calc(100% - 80px)">
	    <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
	      <legend>Criterios de consulta</legend>
	      <el-form label-position="top" size="mini">
	        <el-row :gutter="4"> 
	          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
	            <el-form-item label="Departamento">
	              <el-select @change="municipios" v-model="ruleForm.departamento" placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsDpto"
	                  :key="item.de_codigo"
	                  :label="item.de_nombre"
	                  :value="item.de_codigo">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>
	       
	          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
	            <el-form-item label="Municipio">
	              <el-select @change="corregimientos" v-model="ruleForm.municipio" placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsMunicipio"
	                  :key="item.mu_codigomun"
	                  :label="item.mu_nombre"
	                  :value="item.mu_codigomun">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
	            <el-form-item label="Corregimiento">
	              <el-select @change="barrios" v-model="ruleForm.corregimiento" placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsCorregimientos"
	                  :key="item.cod"
	                  :label="item.co_nombre"
	                  :value="item.co_codcorregimiento">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
	            <el-form-item label="Barrio">
	              <el-select v-model="ruleForm.barrio" placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsBarrio"
	                  :key="item.cod"
	                  :label="item.ba_nombarrio"
	                  :value="item.ba_codbarrio">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>
	          
	          <el-col :xs="24" :sm="24" :md="4" :lg="6" :xl="6">
	            <el-form-item label="Fecha inicial">
	              <el-date-picker
	              	v-model="ruleForm.fecha_inicial"
	                style="width: 100%"
	                format="MM/dd/yyyy"
	                value-format="MM/dd/yyyy"
	                type="date"
	                placeholder="Seleccione fecha">
	              </el-date-picker>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :sm="24" :md="4" :lg="6" :xl="6">
	            <el-form-item label="Fecha final">
	              <el-date-picker
	              	v-model="ruleForm.fecha_final"
	                style="width: 100%"
	                format="MM/dd/yyyy"
	                value-format="MM/dd/yyyy"
	                type="date"
	                placeholder="Seleccione fecha">
	              </el-date-picker>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :sm="24" :md="4" :lg="6" :xl="6">
	            <el-form-item label="Proceso">
	              <el-select v-model="ruleForm.proceso" placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsProceso"
	                  :key="item.c_codigo"
	                  :label="item.c_nombre"
	                  :value="item.c_codigo">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :md="3" :lg="6">
	            <el-form-item label="Acta">
	              <el-input v-model="ruleForm.acta"></el-input>
	            </el-form-item>
	          </el-col>

	          <el-col :xs="24" :md="9" :lg="6">
	            <el-form-item label="Tecnico">
	              <el-select v-model="ruleForm.tecnico" filterable placeholder="(TODOS)">
	                <el-option
	                  v-for="item in optionsTecnicos"
	                  :key="item.te_codigo"
	                  :label="item.te_nombres"
	                  :value="item.te_codigo">
	                </el-option>
	              </el-select>
	            </el-form-item>
	          </el-col>

	        </el-row>
	      </el-form>
	    </fieldset>
		</section>

		<section id="seccion-tabla" style="margin: 1rem;" v-loading="loadingCard">
			<el-row>
		    <el-col :lg="24">
		      <el-row>
            <el-col :span="24">
              <el-table
                border
                height="200"
                size="mini"
                :data="tableData"
                style="width: 100%"
                @selection-change="handleSelectionChange">
                <el-table-column
                  type="selection"
                  align="center"
                  width="55">
                </el-table-column>
                <el-table-column
	                type="index"
	                header-align="center"
	                align="center"
	                :index="indexMethod"
	                width="60">
	              </el-table-column>
                <el-table-column
                  prop="acta"
                  align="center"
                  label="Acta"
                  width="80">
                  <template slot-scope="scope">{{ scope.row.acta }}</template>
                </el-table-column>
                <el-table-column
                  prop="fechaejecucion"
                  align="center"
                  label="FechaEje"
                  width="130">
                </el-table-column>
                <el-table-column
                  prop="tecnico"
                  label="Tecnico">
                </el-table-column>
                <el-table-column
                  prop="proceso"
                  label="Proceso"
                  width="130">
                </el-table-column>
                <el-table-column
                  prop="resultado"
                  label="Resultado"
                  width="100">
                </el-table-column>
                <el-table-column
                  prop="accion_anomalia"
                  label="Acción/Anomalia"
                  width="160">
                </el-table-column>
              </el-table>
            </el-col>
          </el-row>
		    </el-col>
			</el-row>
	  </section>

	  <section id="seccion-galeria" style="margin: 1rem;">
	  	<el-row>
  	    <el-col :lg="24">
          <div id="carrusel">
            <div id="list-img">
            </div>
          </div>

					<div id="galeria">
            <div id="menubar">
              <span id="title-img"></span>
              <a href="#" title="anterior" id="menubar-anterior" class="ion-ios-arrow-thin-left"></a>
              <a href="#" title="siguiente" id="menubar-siguiente" class="ion-ios-arrow-thin-right"></a>
              <a href="#" title="disminuir" id="menubar-disminuir" class="ion-ios-minus-empty"></a>
              <a href="#" title="aumentar" id="menubar-aumentar" class="ion-ios-plus-empty"></a>
              <a href="#" title="Rotar -45º" id="menubar-rotarIzq" class="ion-ios-undo-outline"></a>
              <a href="#" title="Rotar 45º" id="menubar-rotarDer" class="ion-ios-redo-outline"></a>
              <a href="#" title="cerrar" id="menubar-cerrar" class="ion-ios-close-empty"></a>
            </div>
            <div id="img-max"></div>
          </div>
  	    </el-col>
	  	</el-row>
	  </section>
  </div>

  <script>
  	ELEMENT.locale(ELEMENT.lang.es);
    new Vue({
      el: '#app',
      data: () => ({
      	ruleForm: {
	        departamento: '',
	        municipio: '',
	        corregimiento: '',
	        barrio: '',
	        fecha_inicial: '',
	        fecha_final: '',
	        proceso: '',
	        acta: '',
	        tecnico: ''
	      },
	      optionsDpto: [],
	      optionsMunicipio: [],
	      optionsCorregimientos: [],
	      optionsBarrio: [],
	      optionsProceso: [],
	      optionsTecnicos: [],
      	tableData: [],
      	loadingCard: false
      }),
      methods: {
      	tableEvidenvias () {
      		if (this.ruleForm.fecha_inicial === '' || this.ruleForm.fecha_final === '') {
            this.$alert('Debe seleccionar un rango de fecha para consultar.', 'AVISO..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleForm.fecha_final < this.ruleForm.fecha_inicial) {
            this.$alert('Lo sentimos la fecha final no debe ser menor a la fecha inicial.', 'AVISO..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleForm.acta !== '') {
          	this.ruleForm.fecha_final = '';
          	this.ruleForm.fecha_inicial = '';
            this.onQuery();
          } else {
            this.onQuery();
          }
	      },
        onQuery () {
          this.loadingCard = true
          var data = new FormData();
          data.append('fecha_inicial', this.ruleForm.fecha_inicial);
          data.append('fecha_final', this.ruleForm.fecha_final);
          data.append('departamento', this.ruleForm.departamento);
          data.append('municipio', this.ruleForm.municipio);
          data.append('proceso', this.ruleForm.proceso);
          data.append('tecnico', this.ruleForm.tecnico);
          data.append('acta', this.ruleForm.acta);
          axios.post('request/table_evidencias.php', data).then(response => {
            this.tableData = response.data;
            $('#list-img').html('');//resetamos la lista de imagenes
            $('#list-img').animate({left: 0});//resetamos la lista de imagenes
            $('#img-max').html('');//reseteamos a la imagen maximizada
            $('#title-img').html('');//reseteamos el titulo de la imagen maximizada
            var arr_img = {};//vector q almacena las imagenes de cada nodo
            var count = 0, c = 0;
            $.each(response.data, function (i, item) {
              $.each(item.imagen, function (k, img) {
                if(img !== undefined) { 
                  c++;
                }
              });
            });   
            $.each(response.data, function (i, item) {
              $.each(item.imagen, function (j, img) {
                if(img !== undefined) { 
                  arr_img[count] = new Image();
                  arr_img[count].onload = function () {};
                  arr_img[count].src = img; 
                  arr_img[count].name = count;
                  arr_img[count].alt = 'imagen ' + (count + 1);
                  arr_img[count].title = 'Imagen ' + (count + 1) + ' de ' + c;
                  $(arr_img[count]).addClass(img.acta).addClass('imagen');
                  $('#list-img').append(arr_img[count]);
                  count++;
                }
              });
            });

            var zoom = 50;
            var rotacion = 0;
            var imgSeleccionada = null;
            $('#galeria').hide();
          
            $("#menubar-cerrar").click(function (e) {
              e.preventDefault();
              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              $("#galeria").hide();
              $("#img-max img").css("width", 50 + "%");
            });

            $("#menubar-aumentar").click(function (e) {
              e.preventDefault();
              if (zoom < 100) {
                zoom += 10;
                $("#img-max img").css("width", zoom + "%");
              }
            });

            $("#menubar-disminuir").click(function (e) {
              e.preventDefault();
              if (zoom > 50) {
                zoom -= 10;
                $("#img-max img").css("width", zoom + "%");
              }
            });

            $("#menubar-anterior").click(function (e) {
              e.preventDefault();

              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              if (imgSeleccionada > 0) {
                imgSeleccionada--;
                $("#img-max img").animate({opacity: 0}, 200, function () {
                  var img = $("#list-img img[name='" + imgSeleccionada + "']").clone();
                  $("#img-max").html(img);
                  $("#title-img").html("Imagen " + (imgSeleccionada + 1) + " de " + c);
                  zoom = 50;
                  $("#img-max img").css("width", zoom + "%");
                  $("#img-max img").animate({opacity: 1}, 200, function () {
                    console.log("fin animacion");
                  });
                });

              }
            });

            $("#menubar-siguiente").click(function (e) {
              e.preventDefault();

              rotacion = 0;
              $("#img-max img").css({
                "transform": "rotate (" + rotacion + "deg)",
                "-moz-transform": "rotate(" + rotacion + "deg)",
                "-webkit-transform": "rotate(" + rotacion + "deg)",
                "-o-transform": "rotate(" + rotacion + "deg)",
                "-ms-transform": "rotate(" + rotacion + "deg)"
              });

              if (imgSeleccionada < c - 1) {
                imgSeleccionada++;
                $("#img-max img").animate({opacity: 0}, 200, function () {
                  var img = $("#list-img img[name='" + imgSeleccionada + "']").clone();
                  $("#title-img").html("Imagen " + (imgSeleccionada + 1) + " de " + c);
                  $("#img-max").html(img);
                  zoom = 50;
                  $("#img-max img").css("width", zoom + "%");
                  $("#img-max img").animate({opacity: 1}, 200, function () {
                    console.log("fin animacion");
                  });
                });

              }
            });

            $("#menubar-rotarIzq").click(function (e) {
              e.preventDefault();

              if (rotacion > -360) {
                rotacion = rotacion - 90;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });

              } else {
                rotacion = 0;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });
              }
              //console.log(rotacion);
            });

            $("#menubar-rotarDer").click(function (e) {
              e.preventDefault();

              if (rotacion < 360) {
                rotacion = rotacion + 90;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });

              } else {
                rotacion = 0;
                $("#img-max img").css({
                  "transform": "rotate (" + rotacion + "deg)",
                  "-moz-transform": "rotate(" + rotacion + "deg)",
                  "-webkit-transform": "rotate(" + rotacion + "deg)",
                  "-o-transform": "rotate(" + rotacion + "deg)",
                  "-ms-transform": "rotate(" + rotacion + "deg)"
                });
              }
              //console.log(rotacion);
            });
            //EVENTO CLIC PARA CADA IMAGEN DEL CARRUSEL
            $('#list-img img').click(function (e) {
              e.preventDefault();
              //$("#list-img img").removeClass("img-selected");
              var img = $(this).clone();
              $('#img-max').html(img);
              //$(this).addClass("img-selected");
              $('#title-img').html('Imagen ' + (+$(this).attr('name') + 1) + ' de ' + c);
              imgSeleccionada = +$(this).attr('name');
              $('#galeria').show();

            });
            $('#carrusel').show();
            $('#contenido').show();
            $('#modal').animate({right: '-100%'}, 500);
            // console.log(response)
            this.loadingCard = false;
          }).catch(e => {
            console.log(e.response);
            this.loadingCard = false;
          });
        },
	      handleSelectionChange (val) {
	        var clases='';
	        val.forEach(element => {
	         // alert(element.acta)
	          clases+='.'+element.acta+',';
	        });

	        clases = clases.substring(0, clases.length-1);
	        //clases.slice(0,-2);
	        //alert('class: '+clases);
	        $('#list-img').children('.imagen').not(clases).hide();
	        $('#list-img').children(clases).show();
	        if(clases==''){
	         // alert('vacio');
	          $('#list-img').children('.imagen').show();
	        }
	        // alert(JSON.stringify(val[0].acta))
	        this.multipleSelection = val;
	      },
	      departamentos () {
	        axios.get('request/departamentos.php').then(response => {
	          this.optionsDpto = response.data;
	        }).catch(e => {
	          console.log(e.response);
	        });
	      },
	      municipios () {
	        var mu_depto = new FormData();
	        mu_depto.append('mu_depto', this.ruleForm.departamento);
	        axios.post('request/municipios.php', mu_depto).then(response => {
	          this.optionsMunicipio = response.data;
	        }).catch(e => {
	          console.log(e.response);
	        });
          this.ruleForm.municipio = '';
          this.ruleForm.corregimiento = '';
          this.ruleForm.barrio = '';
	      },
	      corregimientos () {
	        var co_municipio = new FormData();
	        co_municipio.append('co_municipio', this.ruleForm.municipio);
	        axios.post('request/corregimientos.php', co_municipio).then(response => {
	          this.optionsCorregimientos = response.data;
	        }).catch(e => {
	          console.log(e.response);
	        });
          this.ruleForm.corregimiento = '';
	      },
	      barrios () {
	        var ba_sector = new FormData();
	        ba_sector.append('ba_sector', this.ruleForm.corregimiento);
	        axios.post('request/barrios.php', ba_sector).then(response => {
	          this.optionsBarrio = response.data;
	        }).catch(e => {
	          console.log(e.response);
	        });
          this.ruleForm.barrio = '';
	      },
	      procesos () {
	        axios.get('request/procesos.php').then(response => {
	          this.optionsProceso = response.data;
	        }).catch(e => {
	          console.log(e.response);
	        })
	      },
	      tecnicos () {
	        axios.get('request/tecnicos.php').then(response => {
	          this.optionsTecnicos = response.data;
	        }).catch(error => {
	          console.log(error.response);
	        })
	      },
	      indexMethod (index) {
          return index + 1
        }
    	},
	    mounted () {
	    	// $('#galeria').hide();
	      this.departamentos();
	      this.procesos();
	      this.tecnicos();
	    }
    })
  </script>

</body>
</html>