<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
    } 
?>

<!DOCTYPE html5>

<html>

<head>
	
  <meta charset="utf-8">
	
  <title>Localizador</title>

  
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  
  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />
  
  <!--<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

  
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  
  <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
  <script type="text/javascript" src="../../js/gmaps.js"></script>

	<script type="text/javascript">
  
         
    
    var hoy= new Date();
    var dias=["domingo","lunes","martes","miércoles","jueves","viernes","sábado"];
    var meses=["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
    var dia=hoy.getDate();
    var mes=hoy.getMonth()+1;
    var anio=hoy.getFullYear();


    
    
    var nodoActivo=null;
    var map;
    var limits;
   
    
		$(function(){

      $("#modal").hide();

      function actualizar(){

        $("#modal").animate({right:'0%'},500); 

        $.ajax({//ajax que carga las tabla tecnicos
                  url:'request/listarTecnicos.php',
                  type:'POST',
                  dataType:'json',
                  //data:{dia:dia, mes:mes, anio:anio}

                }).done(function(repuesta){
                    
                    //(map.markers[index].infoWindow).open(map.map,map.markers[index]);
                    nodoActivo=null;
                    

                    if(repuesta.length>0){// si hay tecnicos que mostrar
                      
                      map.removeMarkers();
                      limits=new google.maps.LatLngBounds();
                      map.setCenter(repuesta[0].lat,repuesta[0].lon);
                      map.setZoom(16);
                        
                      var list_tecnicos ="<table><tr class='titulo'><td colspan='4'>Listado de funcionarios</td></tr><tr class='cabecera'><td>Cuad.</td><td>Conv.</td><td>Nombre.</td><td>U. reporte</td></tr>";

                      for(var i=0; i<repuesta.length;i++)
                      {   
                          map.addMarker({lat:repuesta[i].lat, lng:repuesta[i].lon, icon:'../imagenes/'+repuesta[i].icon+'.png', title:repuesta[i].nombre, infoWindow:{content:"<h5 id='title-foto'>"+repuesta[i].nombre+"</h5><img src='request/foto.php?ced="+repuesta[i].cedula+"' />"} });

                          limits.extend(new google.maps.LatLng(repuesta[i].lat,repuesta[i].lon));

                          list_tecnicos+="<tr name='"+i+"' class='fila' data-lat='"+repuesta[i].lat+"' data-lon='"+repuesta[i].lon+"'>"+
                          "<td>"+repuesta[i].icon+"</td><td><img src='../imagenes/"+repuesta[i].icon+".png' /></td><td>"+repuesta[i].nombre+"</td><td>"+repuesta[i].hora+"</td>"+
                          "</tr>";

                          
                      }
                      map.fitBounds(limits);

                      list_tecnicos+="</table>";

                      $("#div-tecnicos").html(list_tecnicos);

                      

                      $("#div-tecnicos table tr.fila").click(function(){

                            var nodo=$(this).attr("name");
                            var lat=$(this).attr("data-lat");
                            var lon=$(this).attr("data-lon");

                            //console.log(map.markers[nodo]);
                            google.maps.event.trigger(map.markers[nodo],'click');


                            if(nodo!=nodoActivo){

                                 
                                  $("#div-tecnicos table tr.fila[name='"+nodoActivo+"']").addClass("item-nodo");
                                  $("#div-tecnicos table tr.fila[name='"+nodoActivo+"']").removeClass("nodo-activo");
                              
                                  $("#div-tecnicos table tr.fila[name='"+nodo+"']").addClass("nodo-activo");
                                  $("#div-tecnicos table tr.fila[name='"+nodo+"']").removeClass("item-nodo");

                                  map.setCenter(lat,lon);
                                  map.setZoom(18);

                                  nodoActivo=nodo;
                                               
                            }// fin si nodo!=nodoActivo    
                        });
                        
                         

                       $("#modal").animate({right:'-100%'},500); 
                       //$("#actualizar").button({disabled:false});
                       //$("#actualizar").prop('disabled',false);
                       

                       //$("#reset").button({disabled:false});
                       //$("#reset").prop('disabled',false);
                       //$("#actualizar").show();
                       $("#reset").show();
                      }//fin si hay tecnicos
                      else
                      {
                        //alert("No se encontraron resultados!");
                        $("#modal h1").html("Lo sentimos!");
                        $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                      }
                      $("#actualizar").show();

            });//fin del ajax Tecnicos

      };
      
      
      function iniciarComponentes(){
        
           
        //$("#actualizar").button({disabled:true});
        //$("#actualizar").prop('disabled',true);
        $("#actualizar").hide();
        $("#reset").hide();

        
        $("#actualizar").click(function(){
          //$(this).button({disabled:true});
          //$(this).prop('disabled',true);
          $(this).hide();
          
          //$("#reset").button({disabled:true});
          //$("#reset").prop('disabled',true);
          $("#reset").hide();

          actualizar();
        });

       
        //$("#reset").button({disabled:true});
        $("#reset").click(function(){
          
          map.fitBounds(limits);
        });


        actualizar();

                     

      };//fin de iniciarComponentes()

      //===================================================================================

      function cargarMapa(){
            //GMaps.geolocate({
                //success: function(position){
                      //milat=position.coords.latitude;
                      //milon=position.coords.longitude;
                       map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                          el: '#div-map',
                          lat: 8.749349, /*position.coords.latitude,*/
                          lng: -75.879568,/*position.coords.longitude,*/
                          zoom:10
                        });

                       
                       //$("#header").html("<ul><li><a href='#' id='actualizar'>Actualizar</a><a href='#' id='reset'>Reset mapa</a></li><li><h3>Ubicación de funcionarios</h3><h5>"+dias[hoy.getDay()-1]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></li></ul><img src='request/cargaLogo.php' id='logo'>");
                       //$("#header").html("<ul><li><a href='#' id='actualizar'>Actualizar</a><a href='#' id='reset'>Reset mapa</a></li><li><h3>Ubicación de funcionarios</h3><h5>"+dias[hoy.getDay()-1]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></li></ul>");
                       $("#header").html("<div id='div-title'><h3>Ubicación de funcionarios</h3><h5>"+dias[hoy.getDay()]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></div><nav><ul id='menu'><li id='actualizar'><a href='#'><span class='ion-ios-loop-strong'></span><h6>Actualizar</h6></a></li><li id='reset'><a href='#' ><span class='ion-ios-reload'></span><h6>Reset</h6></a></li></ul></nav>");
                       $("#modal").show();
                       //limits = new google.maps.LatLngBounds();
                       limits = new google.maps.LatLngBounds();
                       iniciarComponentes();                                      
                /*},
                error: function(error){
                   alert('Fallo en la geolocalizacion:'+error.message);
                   return false;
                },
                not_supported:function(){
                  alert('Geolocalizacion no soportada');
                  return false;
                }
            });*/
      };
      //======================================================================================

      
      
      cargarMapa();

      $("body").show();
      
     
		});//fin del onready
	</script>

</head>

  <!--<body style="display:none">-->
  <body>
    
    <div id="header"></div>

    <div id='modal'><h1>Cargando...</h1><p>Por favor espere mientras realizamos la consulta.</p></div>


    
    <div id="contenido">

      <div id="div-tecnicos">
        <!--<table>
          <tr class='titulo'>
            <td colspan='4'>Listado de funcionarios</td>
          </tr>
          <tr class='cabecera'>
            <td>Cuad.</td>
            <td>Conv.</td>
            <td>Nombre.</td>
            <td>U. reporte</td>
          </tr>
          <tr class='fila'>
            <td>Cuad.</td>
            <td>Conv.</td>
            <td>Nombre.</td>
            <td>U. reporte</td>
          </tr>
          <tr class='fila'>
            <td>Cuad.</td>
            <td>Conv.</td>
            <td>Nombre.</td>
            <td>U. reporte</td>
          </tr>
         
        </table>-->
      </div>



      <div id="container-map">
        <div id="div-map">

        </div>
      </div>

       <!--<div id="detalles" ></div>-->
      

      

    </div>

  </body>
</html>