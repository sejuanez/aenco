<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Resumen materiales</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){



			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0/*,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico1").width();
					var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					var ancho = +$("#div-grafico2").width();
			    	var alto = +$("#div-grafico2").height();
					$("#grafico2").highcharts().setSize(ancho,alto, false);
				}*/
			});
			

			





			$('#tecnico').attr('disabled',true);
			$("#consultar").hide();

			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();
			//$("#div-total").hide();


			


			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			
			//ajax que carga la lista de tecnicos
			$.ajax({
					url:'request/getTecnicos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsProceso="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsProceso+="<option value='"+repuesta[i].value+"'>"+repuesta[i].label+"</option>";
							
						}
						
						$("#tecnico").html(optionsProceso);						
						$('#tecnico').attr('disabled',false);
						

						$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax procesos


			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				//$("#div-total").hide();

				
				var doneAjax1 = doneAjax2 = doneAjax3 = doneAjax4 = false;

				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;

		        var hayActas = false;

				//obtener actas
		        $.ajax({
						url:'request/rq1.php',
		                type:'POST',
		                dataType:'json',
		                data:{tecnico:$("#tecnico").val(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax1
					doneAjax1=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						hayActas = true;

						var li_actas="";

						for (var i = 0; i < repuesta.length; i++) {
							li_actas+="<li><h5>"+repuesta[i].acta+"</h5></li>";
						};


						$("#lista_actas").html(li_actas);
						


					}
					else{//la consulta no devolvio resultdos

						hayActas = false;
						$("#lista_actas").html("<li><h5>No hay actas para mostrar</h5></li>");						
						
						/*$("#exportar a").attr("href","");
						$("#exportar").hide();
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();*/
					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						if(hayActas){
							$("#exportar a").attr("href","request/imprimirReporte.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}
							
						$("#contenido").show();						
						
					}

				});



				//obtener materiales
		        $.ajax({
						url:'request/rq2.php',
		                type:'POST',
		                dataType:'json',
		                data:{tecnico:$("#tecnico").val(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax2
					doneAjax2=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							filas+="<tr class='fila'><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						};


						$("#tabla-general tbody").html(filas);

						
					}
					else{

						$("#tabla-general tbody").html("");

						/*$("#exportar a").attr("href","");

						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();*/
					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						if(hayActas){
							$("#exportar a").attr("href","request/imprimirReporte.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}
							
						$("#contenido").show();						
						
					}

				});



				var totalItems=0;
				//obtener items
		        $.ajax({
						url:'request/rq4.php',
		                type:'POST',
		                dataType:'json',
		                data:{tecnico:$("#tecnico").val(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax2
					doneAjax4=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var filas="";

						for (var i = 0; i < repuesta.length; i++) {

							filas+="<tr class='fila'><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].cantidad+"</td><td>"+accounting.formatMoney(+repuesta[i].valor, {symbol : '$', precision : 0,thousand : ','})+"</td></tr>";
							//console.log("Valor => "+repuesta[i].valor);
							totalItems = totalItems + (+repuesta[i].valor);
						};

						filas+="<tr class='fila'><td colspan='2'></td><td style='font-weight:bold;'>Valor Total</td><td style='font-weight:bold;'>"+accounting.formatMoney(+totalItems, {symbol : '$', precision : 0,thousand : ','})+"</td></tr>";
						//console.log("Total => "+totalItems);
						$("#tabla-items tbody").html(filas);

						
					}
					else{

						$("#tabla-items tbody").html("");

						/*$("#exportar a").attr("href","");

						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();*/
					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						if(hayActas){
							$("#exportar a").attr("href","request/imprimirReporte.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}
							
						$("#contenido").show();						
						
					}

				});




				//obtener series instaladas
		        $.ajax({
						url:'request/rq3.php',
		                type:'POST',
		                dataType:'json',
		                data:{tecnico:$("#tecnico").val(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					//marcamos como completado el ajax3
					doneAjax3=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						var li_series="";

						for (var i = 0; i < repuesta.length; i++) {
							li_series+="<li><h5>"+repuesta[i].serie+"</h5></li>";
						};


						$("#lista_series").html(li_series);

						
					}
					else{//la consulta no devolvio resultdos

						$("#lista_series").html("<li><h5>No hay series para mostrar</h5></li>");

						/*$("#exportar a").attr("href","");

						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();*/
					}

					//si se completaron los tres ajax
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){

						$("#consultando").hide();
						$("#consultar").show();

						if(hayActas){
							$("#exportar a").attr("href","request/imprimirReporte.php?fechaini="+fechaIni+"&fechafin="+fechaFin+"&tecnico="+$("#tecnico").val()+"&nombre="+$("#tecnico option:selected").text());
							$("#exportar").show();
						}
						else{
							$("#exportar a").attr("");
							$("#exportar").hide();
						}
							
						$("#contenido").show();						
						
					}

				});



			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Resumen de materiales e items</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" target="_self"><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<div><label>Técnico</label><select id='tecnico'></select></div>
				
				<!--<div id="div-total">
					<h5>Total</h5>        
                	<h4 id="total">0</h4>
                	
              	</div>-->
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Actas</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab' >Materiales utilizados</span><span id="tab2-total"></span></a></li>
				<li><a href="#tab4">3. <span class='titulo-tab' >Items</span><span  id="tab4-total"></span></a></li>
				<li><a href="#tab3">4. <span class='titulo-tab' >Series instaladas</span><span  id="tab3-total"></span></a></li>
				<!--<li><a href="#tab4">4. <span class='titulo-tab'>Ingreso adicional</span></a></li>-->
			
			</ul>


			<div id='tab1'>

				<div id='div_actas'>
					<ul id='lista_actas'></ul>
				</div>

				
			</div>




			<div id='tab2'>

				<div id='div-tabla'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Codigo</td><td>Descripción</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>
				
				
			</div>




			<div id='tab4'>

				<div id='div-tabla'>
					<table id='tabla-items'>
						<thead>
							<tr class='cabecera'><td>Item</td><td>Descripción</td><td>Cantidad</td><td>Valor</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>			
				
			</div>




			<div id='tab3'>
				
				<div id='div_series'>
					<ul id='lista_series'></ul>
				</div>


				
				
			</div>


		</div>

		

	</div>

	

</body>
</html>