<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	include('fpdf/fpdf.php');

	$fechaini=$_GET["fechaini"];
	$fechafin=$_GET["fechafin"];
	$tecnico=$_GET["tecnico"];
	$nombre=$_GET["nombre"];


	//obtenemos la fecha del servidor
	date_default_timezone_set('America/Bogota');
	$hoy = date('d/m/Y h:i:s a', time());


	//consultamos el nombre de la empresa
	$consulta="select * from empresa";
   	$resultado=ibase_query($conexion, $consulta);
   	$empresa=ibase_fetch_assoc($resultado);
   	$empresa=$empresa['NOMBRE'];





	function nuevaPagina($pdf, $hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa){

		$pdf->AddPage();
		
		//RENGLON 1

		//imprimimos el nombre de la empresa
		$pdf->SetXY(0,15);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(0,5,$empresa,0,1,'C');

		//imprimimos la imagen
		$pdf->Image("../../../img/empresa1.jpg",10,10,55,15);

		//imprimimos la fecha
		$pdf->SetXY(0,15);
		$pdf->SetFont('Arial','I',7);
		$pdf->Cell(0,5,$hoy,0,1,'R');

		//imprimimos el titulo del reporte
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,5,'RESUMEN DE MATERIALES E ITEMS',0,1,'C');
		$pdf->Cell(0,5,'FECHA INICIAL:  '.$fechaini,0,1,'L');
		$pdf->Cell(0,5,'FECHA FINAL:  '.$fechafin,0,1,'L');
		$pdf->Cell(0,5,'TECNICO:  '.$tecnico." - ".$nombre,0,1,'L');
		$pdf->Ln();


		$pdf->SetFont('Arial','',9);

		
	}

	

	class Encabezado{
		 
		 var $hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa;

		function Encabezado($hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa){
			$this->hoy = $hoy;
			$this->fechaini = $fechaini;
			$this->fechafin = $fechafin;
			$this->tecnico = $tecnico;
			$this->nombre = $nombre;
			$this->empresa = $empresa;
		}

		
		function establecerEncabezado($hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa){
			$this->hoy = $hoy;
			$this->fechaini = $fechaini;
			$this->fechafin = $fechafin;
			$this->tecnico = $tecnico;
			$this->nombre = $nombre;
			$this->empresa = $empresa;
		}

		
		function getHoy(){
			return $this->hoy;
		}
		function getFechaini(){
			return $this->fechaini;
		}
		function getFechafin(){
			return $this->fechafin;
		}
		function getTecnico(){
			return $this->tecnico;
		}
		function getNombre(){
			return $this->nombre;
		}
		function getEmpresa(){
			return $this->empresa;
		}
			
	}

	class PDF extends FPDF
	{
		private $encabezado; 

		

		function initEncabezado(){
			$this->encabezado = new Encabezado(0,0,0,0,0,0);
		}

		function setEncabezado($hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa){
			$this->encabezado->establecerEncabezado($hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa);
			
		}
		
		function AcceptPageBreak()
		{
		    //nuevaPagina($pdf,/*$pagina,*/$mun,$tecnico,$fecha,$placa,$dirTrafo,$potencia,$ptoFisico,$macro,$se,$cto)
		    nuevaPagina($this, $this->encabezado->getHoy(), $this->encabezado->getFechaini(), $this->encabezado->getFechafin(), $this->encabezado->getTecnico(), $this->encabezado->getNombre(), $this->encabezado->getEmpresa());
		    
		    return false;
		}

		// Pie de página
		function Footer()
		{
		    $this->SetFont('Arial','',8);
		    $this->Cell(0,5,'Pagina '.$this->PageNo().' de {nb}',0,0,'C');
		}
	}
	


	$pdf = new PDF('P','mm','Legal');
	$pdf->AliasNbPages();
	
	$pdf->initEncabezado();
	$pdf->setEncabezado($hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa);
	nuevaPagina($pdf, $hoy, $fechaini, $fechafin, $tecnico, $nombre, $empresa);


	//consultamos las actas y las imprimimos
	$consulta = "SELECT  lc.ca_acta FROM LEGA_CABECERA LC WHERE LC.ca_tecnico ='".$tecnico."' AND LC.ca_fechaej BETWEEN '".$fechaini."' and '".$fechafin."'";


	$result = ibase_query($conexion,$consulta);

	$actas = "";

	while($fila = ibase_fetch_row($result)){
		
		$actas = $actas."".utf8_encode($fila[0])."   ";
									
				
	}


	//imprimimos el titulo ACTAS
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(0,5,'ACTAS',0,1,'L');
	$pdf->Ln();

	//imprimimos las actas consultadas
	$pdf->SetFont('Arial','',9);
	$pdf->Write(5, $actas);
	$pdf->Ln();





	//consultamos los MATERIALES y los imprimimos
	//imprimimos el titulo MATERIALES
	$pdf->Ln();
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(0,5,'MATERIALES UTILIZADOS',0,1,'L');
	$pdf->SetFont('Arial','',9);
	$pdf->Ln();

	$consulta = "SELECT LC.ca_tecnico, t.te_nombres, dm.ma_codmater, dm.ma_desmater, sum(dm.ma_canmater) cantidad from lega_cabecera lc inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='M' left join  municipios m on m.mu_depto = lc.ca_depto and m.mu_codigomun=lc.ca_municipio left join tecnicos t on t.te_codigo = lc.ca_tecnico left join tipo_orden_servicio tos on tos.to_codigo = lc.ca_codtipoordens left join dato_clientes dc on dc.dc_acta = lc.ca_acta where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' AND LC.ca_tecnico='".$tecnico."' group by LC.ca_tecnico, t.te_nombres, dm.ma_codmater, dm.ma_desmater";


	$result = ibase_query($conexion,$consulta);

	$pdf->Cell(40,5,'CODIGO',1,0,'C');
	$pdf->Cell(110,5,'DESCRIPCION',1,0,'C');
	$pdf->Cell(40,5,'CANTIDAD',1,1,'C');

	while($fila = ibase_fetch_row($result)){
		
		$pdf->Cell(40,5,utf8_encode($fila[2]),1,0,'C');
		$pdf->Cell(110,5,utf8_encode($fila[3]),1,0,'C');
		$pdf->Cell(40,5,utf8_encode($fila[4]),1,1,'C');									
				
	}

	$pdf->Ln();
	$pdf->Ln();


	//consultamos los ITEMS y los imprimimos
	//imprimimos el titulo ITEMS
	$pdf->Ln();
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(0,5,'ITEMS',0,1,'L');
	$pdf->SetFont('Arial','',9);
	$pdf->Ln();

	$consulta = "SELECT dm.ma_codmater, mo.mo_descripciond, sum(dm.ma_canmater) cantidad, sum(dm.ma_valmater) valor from lega_cabecera lc left join  municipios m on m.mu_depto = lc.ca_depto and m.mu_codigomun=lc.ca_municipio inner join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_ri='I' and dm.ma_tipomat='O' inner join mobras mo on mo.mo_codigod = dm.ma_codmater and mo.mo_tipo = m.mu_tipo where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' AND LC.ca_tecnico= '".$tecnico."' group by dm.ma_codmater, mo.mo_descripciond";


	$result = ibase_query($conexion,$consulta);

	$pdf->Cell(20,5,'ITEM',1,0,'C');
	$pdf->Cell(110,5,'DESCRIPCION',1,0,'C');
	$pdf->Cell(20,5,'CANTIDAD',1,0,'C');
	$pdf->Cell(40,5,'VALOR',1,1,'C');

	$totalItem = 0;
	while($fila = ibase_fetch_row($result)){
		
		$pdf->Cell(20,5,utf8_encode($fila[0]),1,0,'C');
		$pdf->Cell(110,5,utf8_encode($fila[1]),1,0,'C');
		$pdf->Cell(20,5,$fila[2],1,0,'C');
		$pdf->Cell(40,5,number_format($fila[3], 2, ".", ","),1,1,'C');

		$totalItem = $totalItem + 	$fila[3];	
	}

	$pdf->Cell(20,5,'',0,0,'C');
	$pdf->Cell(110,5,'',0,0,'C');

	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(20,5,'Valor Total',1,0,'C');
	$pdf->Cell(40,5,number_format($totalItem, 2, ".", ","),1,1,'C');

	$pdf->Ln();
	$pdf->Ln();



	//consultamos las SERIES y las imprimimos
	$consulta = "SELECT dmm.me_serie from lega_cabecera lc inner join dato_medidor dmm on dmm.me_acta = lc.ca_acta and dmm.me_meins='INSTALADO' WHERE LC.ca_tecnico ='".$tecnico."' AND LC.ca_fechaej BETWEEN '".$fechaini."' and '".$fechafin."' union select ds.se_serie from lega_cabecera lc inner join dato_sellos  ds on ds.se_acta = lc.ca_acta and ds.se_meins='INSTALADO' WHERE LC.ca_tecnico ='".$tecnico."' AND LC.ca_fechaej BETWEEN '".$fechaini."' and '".$fechafin."'";


	$result = ibase_query($conexion,$consulta);

	$series = "";

	while($fila = ibase_fetch_row($result)){
		
		$series = $series."".utf8_encode($fila[0])."   ";
									
				
	}


	//imprimimos el titulo SERIES
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(0,5,'SERIES INSTALADAS',0,1,'L');
	$pdf->Ln();

	//imprimimos las series consultadas
	$pdf->SetFont('Arial','',9);
	$pdf->Write(5, $series);
	$pdf->Ln();
	$pdf->Ln();



	$pdf->Output("Resumen_Materiales_del_".$fechaini."_al_".$fechafin.".pdf", "D");

?>