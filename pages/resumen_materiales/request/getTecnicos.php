<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];
	
	$consulta = "SELECT t.te_codigo, t.te_nombres from tecnicos t";
	

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	//$row_array['codigoDpto'] = null;
	//$row_array['nombreDpto'] = '(Todos)';
	
	//array_push($return_arr, $row_array);

	while($fila = ibase_fetch_row($result)){
		
		//codigo tecnico
		$row_array['value'] = utf8_encode($fila[0]);

		//nombre tecnico
		$row_array['label'] = utf8_encode($fila[1]);
		
						
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>