<?php
session_start();

if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo
    "<script>
            window.location.href='../inicio/index.php';
        </script>";
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">

  <title>Alumbrado público</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)'/> -->

  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    header{
      /* margin-top: -.3rem; */
      height: 8rem;
    }
    #menu li {padding: 17px 5px;}
    #menu span {font-size: 2rem;}
    #menu h6 {font-size: 1rem;}
    table{font-size: .8em;}
    input[type="text"], select, input[type="date"], input[type="time"], textarea {font-size: 1em !important;}
    #div-map {
      width: 520px;
      height: 504px;
      /*height: 100%;*/
      /*position: unset !important;*/
    }
    .titulo{
      font-size: 18px;
      margin-left: 19rem;
    }
    .buscar-serie{
      /* margin-top: -6rem; */
    }
    .consultar-serie{
      margin-left: -2rem;
      margin-top: 2.4rem;
    }
    .anterior{
      margin-left: -13rem;
      margin-top: 2.4rem;
    }
    .siguiente{
      margin-left: -5.8rem;
      margin-top: 2.4rem;
    }
    .contador-serie{
      margin-top: 2.5rem;
      margin-left: -22rem;
    }
    .datos{
      margin-top: 9rem;
    }
    #contador{
      border-radius: 0;
      height: 30px;
      text-align: center;
    }
    .historial-map{
      margin-top: 9rem;
    }
    .total{
      margin-top: 2.5rem;
      margin-left: -13rem;
    }
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
      background-color: #FFFFFF;
    }
    .conP{
      margin-left: 6.5rem;
      margin-top: -2.5rem;
      font-size: 1em;
    }
    @media only screen and (max-width: 768px) {
      .titulo{
        /* display: none; */
      }
      .buscar-serie{
        /* margin-top: -8.5rem; */
      }
      .consultar-serie{
        margin-left: 9.5rem;
        margin-top: -4.6rem;
      }
      .contador-serie #contador{
        margin-top: -4.6rem;
        margin-left: 40rem;
      }
      .anterior{
        margin-left: 14rem;
        margin-top: -4.8rem;
      }
      .siguiente{
        margin-left: 30.5rem;
        margin-top: -4.6rem;
      }
      .contador-serie{
        margin-top: 0rem;
      }
      .datos{
        margin-top: 10.5rem;
      }
      .historial-map{
        margin-top: 1rem;
      }
      #div-map{
        width: 328px;
      }
      .conP{
        margin-left: 46.5rem;
      }
      #total{
        width: 64%;
        margin-left: 39.5rem;
        margin-top: -7rem;
      }
      .historial{
        font-size: 10px !important;
      }
    }
  </style>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>


  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_JT78TYJWd1YFr6xL19MmkoLUBYFBkGc"></script>
  <!-- <script type="text/javascript" src="../../js/gmaps.js"></script> -->


</head>
<body>
<div id="app">
  <header>
    
    <!-- <div class="col-md-2 col-xs-12">
      <div class="form-group">
        <h3 class="titulo">Historial Luminaria</h3>
      </div>
    </div> -->
    <!--  @click="query"-->
    <form id="form_series" action="request/listarEvidencias.php" method="post">
      <div class="col-md-2 col-sm-2 col-xs-4 buscar-serie">
        <div class="form-group">
          <label style="font-size: 1.3rem;">Serie No.</label>
          <input type="text" class="form-control" id="serie" name="serie" autofocus style="border-radius: 0;height: 30px;">
          <input id="skip" name="skip" value="1" type="hidden" class="form-control" style="border-radius: 0;height: 25px;" readonly>
        </div>
      </div>

      <div class="col-md-2 col-xs-12 consultar-serie">
        <div class="form-group">
          <button id="buscar" class="btn btn-default" style="height: 3.3rem;">
            <i class="fa fa-search" aria-hidden="true"></i>
            <!-- <p style="margin: -4px 0 10px;">Consultar</p> -->
          </button>
        </div>
      </div>

      <div class="col-md-3 col-xs-12 anterior">
        <div class="form-group">
          <button id="prev" type="button" class="btn btn-default" style="height: 3.3rem;">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
            <!-- <p style="margin: -4px 0 10px;">Anterior</p> -->
          </button>
        </div>
      </div>

      <div class="col-md-1 col-xs-3 contador-serie">
        <div class="form-group">
          <input id="contador" type="text" class="form-control" readonly>
          <p class="conP"> de </p>
        </div>
      </div>

      <div class="col-md-1 col-xs-3 total">
        <div class="form-group">
          <input id="total" name="total" type="text" class="form-control" style="border-radius: 0;height: 30px;text-align: center;" readonly>
        </div>
      </div>

      <div class="col-md-3 col-xs-12 siguiente">
        <div class="form-group">
          <button id="next" type="button" class="btn btn-default" style="height: 3.3rem;">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
            <!-- <p style="margin: -4px 0 10px;">Siguiente</p> -->
          </button>
        </div>
      </div>

    </form>

    <nav>
      <ul id="menu">

        <!--<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>-->
<!--         <li id="consultar">
          <a href=""><span class="ion-ios-search-strong"></span><h6>consultar</h6></a>
        </li>
        <li id="consultando"><a href=""><span class="ion-load-d"></span><h6>consultando...</h6></a></li> -->

      </ul>
    </nav>
  </header>

  <div id="contenido" style="overflow: hidden;">

    <div class="col-md-6 col-xs-12 datos">
      <div class="row">

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Serie</label>
            <input id="sl_serielum" name="sl_serielum" type="text" class="form-control historial" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Acta</label>
            <input id="dla_acta" name="dla_acta" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Nodo</label>
            <input id="dla_nodo" name="dla_nodo" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Punto Fisico</label>
            <input id="dla_pto_fisico" name="dla_pto_fisico" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Tipo lampara</label>
            <input id="dla_tipo_lampara" name="dla_tipo_lampara" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Potencia</label>
            <input id="dla_potencia" name="dla_potencia" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Tipo encendido</label>
            <input id="dla_tipo_encendido" name="dla_tipo_encendido" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Funciona</label>
            <input id="dla_funcionando" name="dla_funcionando" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Referencia</label>
            <input id="dla_cod_referencia" name="dla_cod_referencia" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Observacion</label>
            <input id="dla_observacion" name="dla_observacion" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-5 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Material</label>
            <input id="ma_descripcion" name="ma_descripcion" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Marca</label>
            <input id="descripcion" name="descripcion" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Latitud</label>
            <input id="dla_longitud" name="dla_longitud" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Longitud</label>
            <input id="dla_latitud" name="dla_latitud" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Nodo ant</label>
            <input id="dla_nodo_anterior" name="dla_nodo_anterior" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Estado</label>
            <input id="dla_estado" name="dla_estado" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Fecha</label>
            <input id="dla_fecha" name="dla_fecha" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Hora</label>
            <input id="dla_hora" name="dla_hora" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Luminaria</label>
            <input id="dla_luminaria" name="dla_luminaria" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-2 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Tipo</label>
            <input id="co_tipo" name="co_tipo" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-7 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Tecnico</label>
            <input id="tecn" name="tecn" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Departamento</label>
            <input id="dpto" name="dpto" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Municipio</label>
            <input id="mpio" name="mpio" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Corregimiento</label>
            <input id="corr" name="corr" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Barrio</label>
            <input id="barrio" name="barrio" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>
        
        <div class="col-md-8 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Direccion</label>
            <input id="dla_direccion" name="dla_direccion" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Obv general</label>
            <input id="dla_obsv_luminaria" name="dla_obsv_luminaria" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <div class="form-group">
            <label style="font-size: 1.3rem;">Obv luminaria</label>
            <input id="dla_observa_gral" name="dla_observa_gral" type="text" class="form-control" style="border-radius: 0;font-size: 12px !important;height: 25px;" readonly>
          </div>
        </div>

      </div>
    </div>
    
    <div class="col-md-6 col-xs-12 historial-map">
      <div id="div-map">
    </div>

  </div>
<!-- <script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<script>
  
  $(document).ready(function() {
    cargarMapa();
  });

  var pos;
  var map;
  var marker;
  var directionsDisplay;
  var directionsService;
  var infoWindow;
  var bounds;

  $('#form_series').on('submit', function (e) {
    e.preventDefault();
    let action = $(this).attr('action');
    let serialize = $(this).serialize();
    $.post(action, serialize, function (data, status) {
      var json = JSON.parse(data);
      console.log(json);
      if (json[0] !== undefined) {
        $('#sl_serielum').val(json[0].sl_serielum);
        $('#dla_acta').val(json[0].dla_acta);
        $('#dla_nodo').val(json[0].dla_nodo);
        $('#dla_pto_fisico').val(json[0].dla_pto_fisico);
        $('#dla_tipo_lampara').val(json[0].dla_tipo_lampara);
        $('#dla_potencia').val(json[0].dla_potencia);
        $('#dla_tipo_encendido').val(json[0].dla_tipo_encendido);
        $('#dla_funcionando').val(json[0].dla_funcionando);
        $('#dla_observacion').val(json[0].dla_observacion);
        $('#dla_direccion').val(json[0].dla_direccion);
        $('#dla_cod_referencia').val(json[0].dla_cod_referencia);
        $('#dla_observa_gral').val(json[0].dla_observa_gral);
        $('#dla_longitud').val(json[0].dla_longitud);
        $('#dla_latitud').val(json[0].dla_latitud);
        $('#dla_nodo_anterior').val(json[0].dla_nodo_anterior);
        $('#dla_estado').val(json[0].dla_estado);
        $('#dla_fecha').val(json[0].dla_fecha);
        $('#dla_hora').val(json[0].dla_hora);
        $('#dla_luminaria').val(json[0].dla_luminaria);
        $('#ma_descripcion').val(json[0].material);
        $('#descripcion').val(json[0].descripcion);
        $('#dla_obsv_luminaria').val(json[0].ca_observa);
        $('#tecn').val(json[0].tecn);
        $('#dpto').val(json[0].dpto);
        $('#mpio').val(json[0].mpio);
        $('#corr').val(json[0].corr);
        $('#barrio').val(json[0].barrio);
        $('#co_tipo').val(json[0].co_tipo);
        $('#total').val(json[0].total);

        console.log(json[0]);

        cargarMapa(json[0].dla_latitud, json[0].dla_longitud);
      } else {
        // alert('No ay registros para mostrar')
        $('#prev').click();
        // alertify.alert('Alert Title', 'Alert Message!');
      }

    });
  });

  $(document).on('click', '#buscar', function (e) {
    e.preventDefault();
    $('#skip').val(1);
    $('#contador').val(1);
    $('#form_series').submit();
  });

  $(document).on('click', '#prev', function (e) {
    e.preventDefault();
    if (parseInt($('#skip').val()) !== 1) {
      // alert('No ay registros para mostrar.')
      $('#skip').val(parseInt($('#skip').val()) - 1);
      $('#form_series').submit();
      var prev = parseInt($('#skip').val());
      $('#contador').val(prev);
      var dla_latitud = $('#dla_latitud').val();
      var dla_longitud = $('#dla_longitud').val();
      cargarMapa(dla_latitud, dla_longitud);
      
    }
  });

  $(document).on('click', '#next', function (e) {
    e.preventDefault();
    $('#skip').val(parseInt($('#skip').val()) + 1);
    $('#form_series').submit();
    var next = parseInt($('#skip').val());
    if ($('#contador').val() < $('#total').val()) {
      $('#contador').val(next);
      var dla_latitud = $('#dla_latitud').val();
      var dla_longitud = $('#dla_longitud').val();
      cargarMapa(dla_latitud, dla_longitud);
    }    
  });

  function cargarMapa(lat, lon) {
    // new google.maps.LatLng(lat, lon)
    var pos = {
      lat: 4.0000000,
      lng: -72.0000000
    };
    map = new google.maps.Map(document.getElementById('div-map'), {
      center: pos,
      zoom: 6,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      zoomControlOptions: {position: google.maps.ControlPosition.TOP_RIGHT},
      streetView: null,
    });

    var positions = new google.maps.LatLng(lat, lon);
    infoWindow = new google.maps.InfoWindow({
      content: $('#ma_descripcion').val()
    });
    // infoWindow.setPosition(positions);
    if (isNaN(positions)) {
      map.setCenter(pos);
    } else {
      map.setCenter(positions);
    }
    // infoWindow.setContent('Location found.');
    var dla_tipo_lampara = $('#dla_tipo_lampara').val();
    var dla_potencia = $('#dla_potencia').val();
    marker = new google.maps.Marker({
      map: map,
      icon: '../luminarias/img/'+dla_tipo_lampara+dla_potencia+'.png',
      position: new google.maps.LatLng(lat, lon)
    });
    clickMarker(lat, lon);
    //Clien en el icono del mapa
    // marker.addListener('click', function() {
    //   map.setZoom(16);
    //   map.setCenter(lat, lon);
    //   infoWindow.open(marker.get('div-map'), marker);
    // });
  }

  // $(document).on('click', '.gmnoprint', function (e) {
  //   e.preventDefault();
  //   var dla_latitud = $('#dla_latitud').val();
  //   var dla_longitud = $('#dla_longitud').val();
  //   clickMarker(dla_latitud, dla_longitud);
  // });

  function clickMarker (lat, lon) {
    var coor = new google.maps.LatLng(lat, lon);
    map.setZoom(18);
    map.setCenter(coor);
    infoWindow.open(marker.get('div-map'), marker);
  }

</script>
</body>
</html>