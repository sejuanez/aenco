<?php
	// session_start();
	require_once("../../../init/gestion.php");
  	 // if (!$_SESSION['user']) {
    //     echo"<script>window.location.href='../../inicio/index.php';</script>"; 
    //     exit(); 
    //  }

    function utf8_converter($array) {
	    array_walk_recursive($array, function( & $item, $key) {
	        if ( ! mb_detect_encoding($item, 'utf-8', true)) {
	                $item = utf8_encode($item); 
	        }
	    }); 

    	return $array; 
	}

	

	$serie = $_POST["serie"]; 
	$skip = $_POST["skip"]; 

	// $param = ($skip <= 0) ? '' : $skip ;FIRST 1 SKIP ($skip - 1)

	// if ($skip >= 0) {
		$query = "SELECT FIRST 1 SKIP ($skip - 1) sl.sl_serielum, dla.dla_acta, dla.dla_nodo, dla.dla_pto_fisico,
		dla.dla_tipo_lampara, TRUNC(dla.dla_potencia) dla_potencia, dla.dla_tipo_encendido, dla.dla_funcionando, dla.dla_observacion,
		dla.dla_direccion, dla.dla_cod_referencia, dla.dla_observa_gral, dla.dla_longitud, dla.dla_latitud,
		dla.dla_nodo_anterior, dla.dla_estado, dla.dla_fecha, dla.dla_hora, dla.dla_luminaria, sl.sl_codmater||' - '||ma.ma_descripcion material, sl.sl_codmarca||' - '||ds.descripcion descripcion, te.te_codigo||' - '||te.te_nombres tecn,
		dla.dla_dpto||' - '||de.de_nombre dpto, dla.dla_mpio||' - '||mu.mu_nombre mpio, dla.dla_corregimiento||' - '||co.co_nombre corr, co.co_tipo, dla.dla_barrio||' - '||ba.ba_nombarrio barrio, lc.ca_observa
		FROM series_luminarias sl
		LEFT JOIN dato_lev_alumbrado dla ON dla.dla_codmarca = sl.sl_codmarca AND dla.dla_codigomat = sl.sl_codmater AND dla.dla_serielum = sl.sl_serielum
		LEFT JOIN materiales ma ON ma.ma_codigo = dla.dla_codigomat
		LEFT JOIN lega_cabecera lc ON lc.ca_acta = dla.dla_acta
		LEFT JOIN tecnicos te ON te.te_codigo = lc.ca_tecnico
		LEFT JOIN departamentos de ON de.de_codigo = dla.dla_dpto
		LEFT JOIN municipios mu ON mu.mu_depto = dla.dla_dpto AND mu.mu_codigomun = dla.dla_mpio
		LEFT JOIN corregimientos co ON co.co_depto = dla.dla_dpto AND co.co_municipio = dla.dla_mpio AND co.co_codcorregimiento = dla.dla_corregimiento
		LEFT JOIN barrios ba ON ba.ba_depto = dla.dla_dpto AND ba.ba_mpio = dla.dla_mpio AND ba.ba_sector = dla.dla_corregimiento AND ba.ba_codbarrio = dla.dla_barrio
		LEFT JOIN descripcion_series ds ON ds.codigo = dla.dla_codmarca AND ds.grupo = '1'
		WHERE sl.sl_serielum LIKE '%'||'$serie'||'%'"; 

		#Total para el contador
		$queryTotal = "SELECT  COUNT(*) total
	    FROM series_luminarias sl
	    LEFT JOIN dato_lev_alumbrado dla ON dla.dla_codmarca = sl.sl_codmarca AND dla.dla_codigomat = sl.sl_codmater AND dla.dla_serielum = sl.sl_serielum
	    LEFT JOIN materiales ma ON ma.ma_codigo = dla.dla_codigomat
	    LEFT JOIN lega_cabecera lc ON lc.ca_acta = dla.dla_acta
	    LEFT JOIN tecnicos te ON te.te_codigo = lc.ca_tecnico
	    LEFT JOIN departamentos de ON de.de_codigo = dla.dla_dpto
	    LEFT JOIN municipios mu ON mu.mu_depto = dla.dla_dpto AND mu.mu_codigomun = dla.dla_mpio
	    LEFT JOIN corregimientos co ON co.co_depto = dla.dla_dpto AND co.co_municipio = dla.dla_mpio AND co.co_codcorregimiento = dla.dla_corregimiento
	    LEFT JOIN barrios ba ON ba.ba_depto = dla.dla_dpto AND ba.ba_mpio = dla.dla_mpio AND ba.ba_sector = dla.dla_corregimiento AND ba.ba_codbarrio = dla.dla_barrio
	    LEFT JOIN descripcion_series ds ON ds.codigo = dla.dla_codmarca AND ds.grupo = '1'
	    WHERE sl.sl_serielum LIKE '%'||'$serie'||'%'";

		$return_arr = array();
		$arT = array();

		$result = ibase_query($conexion, $query);

		$resulTotal = ibase_query($conexion, $queryTotal);
		// $totall = ibase_fetch_row($resulTotal);
		while ($con = ibase_fetch_row($resulTotal)) {
			$row_array['total'] = $con[0];
			array_push($arT, $row_array);
		}
		
		// echo json_encode($arT);

		 // echo "<pre>";
			// 	print_r($totall);
			// echo "</pre>";
			// exit();

		while ($fila = ibase_fetch_row($result)) {
			// echo "<pre>";
			// 	print_r(str_replace(Array(",", ","), "", $fila[5]));
			// echo "</pre>";
			// exit();
			$row_array['sl_serielum'] = utf8_encode($fila[0]); 
			$row_array['dla_acta'] = utf8_encode($fila[1]); 
			$row_array['dla_nodo'] = utf8_encode($fila[2]); 
			$row_array['dla_pto_fisico'] = utf8_encode($fila[3]); 
			$row_array['dla_tipo_lampara'] = utf8_encode($fila[4]); 
			$row_array['dla_potencia'] = utf8_encode($fila[5]); 
			$row_array['dla_tipo_encendido'] = utf8_encode($fila[6]); 
			$row_array['dla_funcionando'] = utf8_encode($fila[7]); 
			$row_array['dla_observacion'] = utf8_encode($fila[8]); 
			$row_array['dla_direccion'] = utf8_encode($fila[9]); 
			$row_array['dla_cod_referencia'] = utf8_encode($fila[10]); 
			$row_array['dla_observa_gral'] = utf8_encode($fila[11]); 
			$row_array['dla_longitud'] = utf8_encode($fila[12]); 
			$row_array['dla_latitud'] = utf8_encode($fila[13]); 
			$row_array['dla_nodo_anterior'] = utf8_encode($fila[14]); 
			$row_array['dla_estado'] = utf8_encode($fila[15]); 
			$row_array['dla_fecha'] = utf8_encode($fila[16]); 
			$row_array['dla_hora'] = utf8_encode($fila[17]); 
			$row_array['dla_luminaria'] = utf8_encode($fila[18]); 
			$row_array['material'] = utf8_encode($fila[19]); 
			$row_array['descripcion'] = utf8_encode($fila[20]); 
			$row_array['tecn'] = utf8_encode($fila[21]); 
			$row_array['dpto'] = utf8_encode($fila[22]); 
			$row_array['mpio'] = utf8_encode($fila[23]); 
			$row_array['corr'] = utf8_encode($fila[24]); 
			$row_array['co_tipo'] = utf8_encode($fila[25]);
			$row_array['barrio'] = utf8_encode($fila[26]);
			$row_array['ca_observa'] = utf8_encode($fila[27]);
			array_push($return_arr, $row_array); 
		}
		echo json_encode(utf8_converter($return_arr)); 
?>