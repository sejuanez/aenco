<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	//header("Content-Disposition: filename=consumo_".$_GET["tab"]."_".$_GET["tab"].".xls");
  //header("Content-Disposition: filename=consumo_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  

	include("../../../init/gestion.php");

  $bodega = $_GET['bodega'];
	$nombreBodega = $_GET['nombreBodega'];

  if($bodega === "00"){//todas las bodegas


    header("Content-Disposition: filename=existenciasBodegaTec_en_Todas.xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $consulta = "SELECT bs.bo_codigomat CodMaterial, coalesce(M.ma_descripcion, 'Sin definir') NombreMaterial, sum(bs.bo_existencia) Existencia, coalesce(m.ma_vr_unitario,0) VrUnitario, cast(coalesce((m.ma_vr_unitario*sum(bs.bo_existencia)),0) as numeric(15,2)) Vr_total FROM bodegasuper BS INNER JOIN TECNICOS T ON T.te_codigo=BS.bo_codigotec left JOIN MATERIALES M ON M.ma_codigo=BS.bo_codigomat WHERE T.te_tipo='B' group by bs.bo_codigomat, M.ma_descripcion, m.ma_vr_unitario";

    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Bodega</td><td>Cod. Material</td><td>Nombre Material</td><td>Existencia</td><td>Vlr. Unitario</td><td>Vlr. Total</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>Todas".//Bodega
          "<td>".utf8_encode($fila[0]).//codMaterial
          "</td><td>".utf8_encode($fila[1]).//nombreMaterial
          "</td>"."<td>".utf8_encode($fila[2]).//Existencia
          "</td>"."<td>".utf8_encode($fila[3]).//VlrUnitario
          "</td>"."<td>".utf8_encode($fila[4]).//VlrTotal
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    
    echo $tabla;

  }else{

    
    $consulta = "SELECT t.te_nombres Bodega, bs.bo_codigomat CodMaterial, coalesce(M.ma_descripcion, 'Sin definir') NombreMaterial,bs.bo_existencia Existencia, coalesce(M.ma_vr_unitario,0) vrunitario, cast(coalesce((bo_existencia*ma_vr_unitario),0) as numeric(15,2)) Vr_total FROM bodegasuper BS INNER JOIN TECNICOS T ON T.te_codigo=BS.bo_codigotec left JOIN MATERIALES M ON M.ma_codigo=BS.bo_codigomat WHERE T.te_tipo='B' and t.te_codigo='".$bodega."' order by t.te_codigo";


    $result = ibase_query($conexion,$consulta);

    header("Content-Disposition: filename=existenciasBodegaTec_en_".$nombreBodega.".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $tabla = "<table>"."<tr class='cabecera'><td>Bodega</td><td>Cod. Material</td><td>Nombre Material</td><td>Existencia</td><td>Vlr. Unitario</td><td>Vlr. Total</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//bodega
          "<td>".utf8_encode($fila[1]).//codMaterial
          "</td><td>".utf8_encode($fila[2]).//nombreMaterial
          "</td>"."<td>".utf8_encode($fila[3]).//Existencia
          "</td>"."<td>".utf8_encode($fila[4]).//Vlr.Unitario
          "</td>"."<td>".utf8_encode($fila[5]).//Vlr. Total
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
  
   		
	
?>