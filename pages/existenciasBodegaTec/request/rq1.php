<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	
	$bodega = $_POST['bodega'];
	
	if($bodega === "00"){//todas las bodegas

		$consulta = "SELECT bs.bo_codigomat CodMaterial, coalesce(M.ma_descripcion, 'Sin definir') NombreMaterial, sum(bs.bo_existencia) Existencia, coalesce(m.ma_vr_unitario,0) VrUnitario, cast(coalesce((m.ma_vr_unitario*sum(bs.bo_existencia)),0) as numeric(15,2)) Vr_total FROM bodegasuper BS INNER JOIN TECNICOS T ON T.te_codigo=BS.bo_codigotec left JOIN MATERIALES M ON M.ma_codigo=BS.bo_codigomat WHERE T.te_tipo='B' group by bs.bo_codigomat, M.ma_descripcion, m.ma_vr_unitario";

		$return_arr = array();

		$result = ibase_query($conexion,$consulta);

		while($fila = ibase_fetch_row($result)){
			
				
			$row_array['bodega'] ="Todas";
			$row_array['codMaterial'] = utf8_encode($fila[0]);
			$row_array['nombreMaterial'] = utf8_encode($fila[1]);
			$row_array['existencia'] = utf8_encode($fila[2]);
			$row_array['vlrUnitario'] = utf8_encode($fila[3]);
			$row_array['vlrTotal'] = utf8_encode($fila[4]);
			
								
			array_push($return_arr, $row_array);
			//$return_arr[utf8_encode($fila[0])]=$row_array;
		}

		echo json_encode($return_arr);
	}
	else{

		$consulta = "SELECT t.te_nombres Bodega, bs.bo_codigomat CodMaterial, coalesce(M.ma_descripcion, 'Sin definir') NombreMaterial,bs.bo_existencia Existencia, coalesce(M.ma_vr_unitario,0) vrunitario, cast(coalesce((bo_existencia*ma_vr_unitario),0) as numeric(15,2)) Vr_total FROM bodegasuper BS INNER JOIN TECNICOS T ON T.te_codigo=BS.bo_codigotec left JOIN MATERIALES M ON M.ma_codigo=BS.bo_codigomat WHERE T.te_tipo='B' and t.te_codigo='".$bodega."' order by t.te_codigo";

		$return_arr = array();

		$result = ibase_query($conexion,$consulta);

		while($fila = ibase_fetch_row($result)){
			
				
			$row_array['bodega'] =utf8_encode($fila[0]);
			$row_array['codMaterial'] = utf8_encode($fila[1]);
			$row_array['nombreMaterial'] = utf8_encode($fila[2]);
			$row_array['existencia'] = utf8_encode($fila[3]);
			$row_array['vlrUnitario'] = utf8_encode($fila[4]);
			$row_array['vlrTotal'] = utf8_encode($fila[5]);
			
								
			array_push($return_arr, $row_array);
		}

		echo json_encode($return_arr);
	}	
	
?>