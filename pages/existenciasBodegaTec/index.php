<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Existencias bodega</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>


	<script type="text/javascript" src="../../js/accounting.js"></script>

	

	<!--<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>-->

	
	<script type="text/javascript">
		
		$(function(){



			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			
			$('#bodega').attr('disabled',true);


			//$("#tecnico").attr('disabled',true);
			$("#consultar").hide();

			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();
			$("#div-total").hide();

		
			

			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0
			});




			//ajax que carga la lista bodega
			$.ajax({
					url:'request/getBodegas.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsBodega="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsBodega+="<option value='"+repuesta[i].codigoBodega+"'>"+repuesta[i].nombreBodega+"</option>";
							
						}
						
						$("#bodega").html(optionsBodega);						
						$('#bodega').attr('disabled',false);

						
						$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax que carga la lista de bodegas
			
			

			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				$("#div-total").hide();
				$("#total").html("0");

				
				var doneAjax1 = false;

				
		        var hayActas = false;

				//consumo general
		        $.ajax({
						url:'request/rq1.php',
		                type:'POST',
		                dataType:'json',
		                data:{bodega:$("#bodega").val()}

				}).done(function(repuesta){

					//marcamos como completado el ajax1
					doneAjax1=true;
					
					if(repuesta.length>0){// si la consulta devolvio resultados

						hayActas = true;

						var vlrTotal = 0;
						var filas="";

						for (var i = 0; i < repuesta.length; i++) {
							
							vlrTotal = vlrTotal + (+repuesta[i].vlrTotal);

							filas+="<tr class='fila'><td>"+repuesta[i].bodega+"</td><td>"+repuesta[i].codMaterial+"</td><td>"+repuesta[i].nombreMaterial+"</td><td>"+accounting.formatMoney(+repuesta[i].existencia, {symbol : '', precision : 0,thousand : ','})+"</td><td>"+accounting.formatMoney(+repuesta[i].vlrUnitario, {symbol : '$', precision : 0,thousand : ','})+"</td><td>"+accounting.formatMoney(+repuesta[i].vlrTotal, {symbol : '$', precision : 0,thousand : ','})+"</td></tr>";
						}


						$("#tabla-general tbody").html(filas);
						
						$("#total").html(accounting.formatMoney(+vlrTotal, {symbol : '$', precision : 0,thousand : ','}));


					}
					else{//la consulta no devolvio resultdos

						hayActas = false;
						
						$("#tabla-general tbody").html("");
					}

					//si se completaron los tres ajax
					if(doneAjax1){

						$("#consultando").hide();
						$("#div-total").show();
						$("#consultar").show();

						

						$("#exportar a").attr("href","request/exportarExcel.php?bodega="+$("#bodega").val()+"&nombreBodega="+$("#bodega option:selected").text());
						$("#exportar").show();
							
						$("#contenido").show();						
						
					}

				});


			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Existencias bodega</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" target="_self"><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<!--<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>-->
				<div><label>Bodega</label><select id='bodega'></select></div>
				
				<div id="div-total">
					<h5>Total</h5>        
                	<h4 id="total">0</h4>
                	
              	</div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Resultados</span></a></li>
				<!--<li><a href="#tab2">2. <span class='titulo-tab' >Consumo por municipios</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab' >Consumo por técnicos</span></a></li>-->
			
			</ul>


			<div id='tab1'>
				
				<div id='div-tabla'>

					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Bodega</td><td>Cod. Material</td><td>Nombre material</td><td>Existencia</td><td>Vlr. Unitario</td><td>Vlr. Total</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>




			<!--<div id='tab2'>

				<div id='div-tabla-municipio'>
					<table id='tabla-municipio'>
						<thead>
							<tr class='cabecera'><td>Municipio</td><td>Codigo</td><td>Descripción</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>
				
				
			</div>-->




			<!--<div id='tab3'>

				<div id='div-tabla-tecnicos'>
					<table id='tabla-tecnicos'>
						<thead>
							<tr class='cabecera'><td>Tecnico</td><td>Codigo</td><td>Descripcion</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>			
				
			</div>-->



		</div>

		

	</div>

	

</body>
</html>