// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function($) {
    $(".selectPlaca").select2().change(function(e) {
        var placa = $(this).val();
        app.onChangePlaca(placa);
    });;
});




// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        btnAnadir: false,
        idPlaca: "",
        t_vehiculo: "",
        descripcion: "",
        fecha: "",
        placaInfo: [],
        selectPlacas: [],
        selectTipos: [],
        fecha: "",

        selectPlacas_modal: [],
        descripcion_modal: "",
        fechavence_modal: "",
        t_vehiculo_modal: "",

        danger: true,



        tablaGastos: [],

        tablaVencidos: [],



        btnActualizar: false,
        btnEliminar: false,
        btnEditar: false,
        btnFile: true,
        btnCancelar: true

    },


    methods: {
        loadSelectPlaca: function() {
            var app = this;
            $.get('./request/getSelectPlacas.php', function(data) {
                app.selectPlacas = JSON.parse(data);
            });
        },

        loadTipos: function() {

            $.get('request/getTipoVencimiento.php', function(data) {
                app.selectTipos = JSON.parse(data);
                app.selectPlacas_modal = JSON.parse(data);
            });

        },

        loadFecha: function() {

            var today = new Date();
            this.fecha = (getFormattedDate(today));


            // Get date formatted as YYYY-MM-DD
            function getFormattedDate(date) {
                return date.getFullYear() +
                    "-" +
                    ("0" + (date.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("0" + date.getDate()).slice(-2);
            }

        },

        onChangePlaca: function(PLACA) {
            var app = this;
            app.idPlaca = PLACA;
            if (app.idPlaca != "") {
                $.post('./request/getVehiculos.php', { placa: PLACA }, function(data) {
                    var data = JSON.parse(data);
                    app.placaInfo = data[0];
                    app.btnAnadir = true;

                    app.search();

                });
            } else {


                app.btnAnadir = false;
                app.placaInfo = [];
                app.resetCampos();
            }
        },

        verFuncionario: function(dato) {

            console.log(dato.CODTIPOVENCE);

            this.idPlaca = dato.CEDULA;
            this.descripcion_modal = dato.DESCRIPCION;
            this.t_vehiculo_modal = dato.CODTIPOVENCE;
            this.fechavence_modal = dato.FECHAVENCE;

            $('.formGuardar input').attr('disabled', 'disabled');
            $('.formGuardar select').attr('disabled', 'disabled');

            this.btnActualizar = false;
            this.btnEliminar = true;
            this.btnEditar = true;

            this.btnCancelar = false;


            $('#addFucionario').modal('show');
        },

        editar: function() {
            this.btnFile = true;
            this.btnCancelar = true;
            this.btnEliminar = false;
            this.btnEditar = false;
            this.btnActualizar = true;
            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar select').removeAttr('disabled');

        },

        search: function() {
            var app = this;

            $.post('./request/getFuncionario.php', { funcionario: app.idPlaca }, function(data) {
                var datos = jQuery.parseJSON(data);
                app.tablaVencidos = [];
                app.tablaVencidos = datos;

            });
        },

        resetCampos: function() {


            this.t_vehiculo = "";
            this.descripcion = "";
            this.fecha = "";
            app.tablaVencidos = [];


        },

        resetModal: function() {
            $('#addFucionario').modal('hide');

            this.selectPlacas_modal = "";
            this.descripcion_modal = "";
            this.fechavence_modal = "";
            this.t_vehiculo_modal = "";



            $('.formGuardar').removeClass('was-validated');

            this.btnActualizar = false;
            this.btnEliminar = false;
            this.btnEditar = false;

            $('.formGuardar input').removeAttr('disabled');
            $('.formGuardar select').removeAttr('disabled');
        },

        actualizar: function() {


            if (this.t_vehiculo_modal == "" ||
                this.descripcion_modal == "" ||
                this.fechavence_modal == "") {


                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;
                var formData = new FormData($('.formGuardar')[0]);
                formData.append('placa1', app.idPlaca);
                formData.append('t_vehiculo_modal', app.t_vehiculo_modal);

                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateFuncionario.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function() {},
                    success: function(data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.search();

                        } else {
                            alertify.error("Ha ocurrido un error");
                        }

                    },
                    //si ha ocurrido un error
                    error: function(FormData) {
                        console.log(data)
                    }
                });

            }


        },

        eliminar: function() {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function() {
                    $.post('./request/eliminarFuncionario.php', { placa: app.idPlaca, tipovence: app.t_vehiculo_modal }, function(data) {
                        console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.search();

                        }
                    });

                },
                function() {
                    // alertify.error('Cancel');
                });
        },

        guardar: function() {


            if (this.t_vehiculo == "" ||
                this.descripcion == "" ||
                this.fecha == "") {

                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;
                var formData = new FormData($('.formGuardar_vencimiento')[0]);

                //hacemos la petición ajax
                $.ajax({
                    url: './request/inserVehiculoVencimiento.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function() {},
                    success: function(data) {

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.search();
                            app.loadFecha();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este vencimiento ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    },
                    //si ha ocurrido un error
                    error: function(FormData) {
                        console.log(data)
                    }
                });

            }
        }
    },



    watch: {

    },

    mounted() {

        this.loadSelectPlaca();
        this.loadTipos();
        this.loadFecha();




    },

});