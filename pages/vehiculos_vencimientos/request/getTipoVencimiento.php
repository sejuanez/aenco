<?php
session_start();

  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	
	$sql =  "	SELECT tvcodigo CODIGO, tvdescripcion NOMBRE 
					FROM tipovencimiento 
					WHERE tvmodulo='VEHICULOS'";

	$return_arr = array();
  
	$result = ibase_query($conexion, $sql);
  
	while($fila = ibase_fetch_row($result)){
	  $row_array['id'] = utf8_encode($fila[0]);
	  $row_array['text'] = utf8_encode($fila[1]);
	  array_push($return_arr, $row_array);
	}
  
	// $array = array("result"=>$return_arr);
  
	echo json_encode($return_arr);
	// echo json_encode($array);

?>

