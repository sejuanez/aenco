<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Personas por fechas</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});




		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			        	type: 'category',
			            //categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Cantidad'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        /*tooltip: {
			            valueSuffix: ' visitas'
			        },*/
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			        	enabled:false/*
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false*/
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};






			$("#consultando").hide();
			$("#contenido").hide();


			$("#div-total").hide();
			$("#div-hombres").hide();
			$("#div-mujeres").hide();



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());





			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#contenido").hide();
				$("#consultando").show();
				
				$("#div-total").fadeOut(500);
				$("#div-hombres").fadeOut(500);
				$("#div-mujeres").fadeOut(500);

				//$("#div-tabla").html("<h4 style='text-align:center; color:#999; font-weight:normal'>Cargando...</h4>");


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				
				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        


				$.ajax({
					url:'request/graficobarras.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var totalMujeres =  0;
						var totalHombres =  0;

						for (var i=0; i<repuesta.length;i++){
							if(repuesta[i].sexo=="Hombres"){
								totalHombres=(+repuesta[i].cantidad);
							}
							else if(repuesta[i].sexo=="Mujeres"){
								totalMujeres=(+repuesta[i].cantidad);
							}
							else{
								totalMujeres =  0;
								totalHombres =  0;
							}
						}

						
						var total = totalMujeres + totalHombres;

						$("#total-total").html(total);
						$("#div-total").fadeIn(500);

						var generos=['Femenino','Masculino'];
						var datos=[['Hombres',totalHombres],['Mujeres',totalMujeres]];
						

						var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						
				
							
						var filas="<tr class='fila'><td>Hombres</td><td>"+totalHombres+"</td></tr>"+
								  "<tr class='fila'><td>Mujeres</td><td>"+totalMujeres+"</td></tr>";
                              
                               
                               
						
						$("table tbody").html(filas);

						var series=[
										{
											name:'Cantidad', 
											data:datos,
											colorByPoint: true,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				}
	                          			}
									];
						
						
						
						$("#total-hombres").html(totalHombres);
						$("#div-hombres").fadeIn(500);

						$("#total-mujeres").html(totalMujeres);
						$("#div-mujeres").fadeIn(500);

						$("#contenido").show();

						dibujarGraficoBarras("grafico-barras",generos,series,colors);

						//drawColumnChart(hc_column,"grafico-barras",colors);//DIBUJAR EL GRAFICO
                        //$("g.highcharts-data-labels>g[opacity=0]").attr("opacity",1);

					}
					else{
												
						$("#total-total").html("");						
						
						$("#total-hombres").html("");						

						$("#total-mujeres").html("");

						$("table tbody").html("");

						$("#alert").dialog("open");
								
					}



					$("#consultando").hide();
					$("#consultar").show();
				});
			});
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});

			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada que mostrar.</p>
	</div>

	<header>
		<h3>Personas</h3>
		<nav>
			<ul id="menu">
				
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				
				
			</form>
		
		</div>
		
		<div id="div-subtotales">

			<div id="div-total">				
				<h4 id="total-total">1</h4>
				<h5>Total</h5>
			</div>

			<div id="div-hombres">				
				<h4 id="total-hombres">2</h4>
				<h5>Hombres</h5>
			</div>

			<div id="div-mujeres">				
				<h4 id="total-mujeres">3</h4>
				<h5>Mujeres</h5>
			</div>
		</div>
		
	</div>



	<div id='contenido'>

		<div id='div-funcionarios'>
			<table>
				<thead>
					<tr class='cabecera'><td>Género</td><td>Cant.</td></tr>
				</thead>

				<tbody>
					
				</tbody>
			</table>
		</div>


		<div id='div-graficos' >

			<div id="grafico-barras" ></div>

		</div>

	</div>

	

</body>
</html>