<?php
  session_start();

  if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo "<script>window.location.href='../inicio/index.php';</script>";
    exit();
  }
?>

<!DOCTYPE html5>

<html>

  <head>
    <meta charset="utf-8">
    <title>Localizador</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <!-- <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' /> -->

  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' /> -->

  <!--<link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>


  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>
    #div-map{
      margin-top: 2.8rem;
    }
    #container-map {
      width: 100%;
    }
    iframe#frameMap{
      width: 100%;
      height: 100%;
    }
  </style>
  <!--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>-->
  <!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>-->
  </head>

<!--<body style="display:none">-->
<body>

<div id="header">
  <div class="col-md-6 col-xs-6">
    <div class="form-group">
      <label>Técnico</label>
      <select class="form-control tecnico"></select>
    </div>
  </div>
  <div class="form-group">
    <button type="button" class="btn btn-default llevame" style="margin-top: 2.5rem;">Levame</button>
  </div>
  <!-- <nav>
    <ul id='menu'>
      <li id='actualizar'>
        <a href='#'><span class='ion-ios-loop-strong'></span><h6>Actualizar</h6></a>
      </li>
      <li id='reset'>
        <a href='#'><span class='ion-ios-reload'></span><h6>Reset</h6></a>
      </li>
    </ul>
  </nav> -->
</div>

<!--     <div id='modal'>
      <h1>Cargando...</h1>
      <p>Por favor espere mientras realizamos la consulta.</p>
    </div> -->

  <div id="contenido">
    <div id="container-map">
      
      <div id="div-map">
      <!-- <iframe id="frameMap" src="http://maps.google.com/maps?saddr=8.748335899999999,-75.877111&daddr=-76.6265517, -76.6265517"></iframe> -->
      </div>
    </div>
  </div>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_JT78TYJWd1YFr6xL19MmkoLUBYFBkGc"></script>
  <!-- <script type="text/javascript" src="../../js/gmaps.js"></script> -->
  <script>
    var hoy = new Date();
    var dias = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];
    var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
    var dia = hoy.getDate();
    var mes = hoy.getMonth() + 1;
    var anio = hoy.getFullYear();

    var nodoActivo = null;
    var coor;
    var pos;
    var map;
    var limits;
    var latitude = 0;
    var longitude = 0;
    var directionsDisplay;
    var directionsService;
    
    $(document).ready(function (){
      tecnicos(); cargarMapa();
    });

    $(document).on('change', '.tecnico', function () {
      var id = $(this).val();
      latitude = $(this).find('option:selected').data('latitude');
      longitude = $(this).find('option:selected').data('longitude');
    });

    $(document).on('click', '.llevame', function (e) {
      e.preventDefault();
      var corrActual = `${pos.lat}, ${pos.lng}`;
      var coorLlevame = `${latitude}, ${longitude}`;
      calcRoute(corrActual, coorLlevame);
      // var url = "http://maps.google.com/maps?saddr="+pos.lat+","+pos.lng+"&daddr="+latitude+","+longitude+"";
      // // window.open(url);
      // $('#container-map').html(
      //   '<iframe src="'+url+'" frameborder="0" scrolling="no" id="frameMap" allowfullscreen></iframe>'
      // );
    });

    function tecnicos() {
      var list = '';
      $.ajax({
        url: 'request/listarTecnicos.php',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
          list += '<option value="" >Seleccione tecnico</option>';
          $.each(data, function (index, pac) {
            list += "<option value='" + pac.cedula + "' data-latitude='" + pac.lat + "' data-longitude='" + pac.lon + "'>" + pac.nombre + "</option>";
          });
          $(".tecnico").html(list);
        }
      });
    }

    function cargarMapa() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          directionsService = new google.maps.DirectionsService();
          directionsDisplay = new google.maps.DirectionsRenderer();
          map = new google.maps.Map(document.getElementById('div-map'), {
            center: new google.maps.LatLng(pos),
            zoom: 8
          });
          $.ajax({
            url: 'request/listarTecnicos.php',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
              $.each(data, function (index, pac) {
                coor = new google.maps.LatLng(pac.lat, pac.lon);
                var marker = new google.maps.Marker({
                  map: map,
                  position: coor/*{lat: pac.lat, lng: pac.lon}*/,
                  icon: '../imagenes/' + pac.icon + '.png',
                  title: pac.nombre
                });
                // marker.addListener('click', function() {
                //   infowindow.open(marker.get('map'), marker);
                // });
              });
            }
          });
          directionsDisplay.setMap(map);
          // var infoWindow = new google.maps.InfoWindow;
          infoWindow.setPosition(coor);
          new google.maps.Marker({map: map, position: pos});
          map.setCenter(pos);
        }, function () {
          // handleLocationError(true, infoWindow, map.getCenter());
        });
      }
    }

    function calcRoute(start, end) {
      var request = {
        origin: start,
        destination: end,
        travelMode: 'DRIVING'
      };
      directionsService.route(request, function(result, status) {
        if (status == 'OK') {
          directionsDisplay.setDirections(result);
        }
      });
    }
  </script>
</body>
</html>