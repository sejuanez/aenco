<?php
	session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
  	} 

	include("../../../init/gestion.php");


	
	$consulta = "SELECT t.te_cuadrilla CUAD, t.te_cedula cedula, coalesce(dr.rh_nombre1, '')||' '||coalesce(dr.rh_apellido1, '') FUNCIONARIO, dgo.dgr_lat LATITUD, dgo.dgr_long LONGITUD, dgo.dgr_hora UR FROM datos_geo_referenciacion dgo inner join tecnicos t on t.te_codigo = dgo.dgr_tecnico inner join datos_rrhh dr on dr.rh_cedula = t.te_cedula WHERE dgo.dgr_fecha = current_date order by dgr_hora desc";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['icon'] = utf8_encode($fila[0]);
		$row_array['cedula'] = utf8_encode($fila[1]);
		$row_array['nombre'] = utf8_encode($fila[2]);
		$row_array['lat'] = utf8_encode($fila[3]);
		$row_array['lon'] = utf8_encode($fila[4]);
		$row_array['hora'] = utf8_encode($fila[5]);
		
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>