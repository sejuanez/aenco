<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Vencimientos</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			var repuestaAjax2 = repuestaAjax3 = requestAjax4 = requestAjax5 = diasTrabajados = null;
			

			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico1").width();
					var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					
				}
			});
			

			$(window).resize(function() {
			    	
			    	var ancho = +$("#div-grafico1").width();
			    	var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					
					
			});
		
			
			function dibujarGraficoBarras_(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        //colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores ($)',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};

			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            zoomType: 'xy',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Cantidad',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};





			$('#tipoVencimiento').attr('disabled',true);
			$("#consultar").hide();

			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();
			$("#div-total").hide();


			

			
			//ajax que carga la lista de tipoVencimientos
			$.ajax({
					url:'request/getTipoVencimientos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsTipoVencimiento="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsTipoVencimiento+="<option value='"+repuesta[i].tipoVencimiento+"'>"+repuesta[i].tipoVencimiento+"</option>";
							
						}
						
						$("#tipoVencimiento").html(optionsTipoVencimiento);						
						$('#tipoVencimiento').attr('disabled',false);
						

						$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax tipoVencimientos


			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				$("#div-total").hide();

				var doneAjax2=doneAjax3=null;
				
				
				//ajax1:  rq1
				$.ajax({
					url:'request/rq1.php',
	                type:'POST',
	                dataType:'json',
	                data:{tipo:$("#tipoVencimiento").val()}

				}).done(function(repuesta){

					if(repuesta.length>0){

						var filas="";
						
						for(var i=0;i<repuesta.length;i++){

							
							filas+="<tr class='fila'>"+
									                   "<td>"+repuesta[i].tipo+"</td><td>"+repuesta[i].cedula+"</td><td>"+repuesta[i].descripcion+"</td><td>"+repuesta[i].fecha+"</td><td>"+repuesta[i].diasVencidos+"</td><td>"+repuesta[i].estado+"</td></tr>";

						}

						$("#tabla-general tbody").html(filas);

						doneAjax2=true;

						$("#exportar a").attr("href","request/exportarExcel.php?tipo="+$("#tipoVencimiento").val());
						

						if(doneAjax2&&doneAjax3){
							$("#consultar").show();
							$("#consultando").hide();
							$("#exportar").show();
							$("#contenido").show();

						}
	

					}
					else{

						//$("#total").html("");		
						//$("#div-total").hide();	
						$("#tabla-general tbody").html("");
						$("#grafico1").html("");

						$("#consultando").hide();
						$("#consultar").show();
					}
				});


				




				$.ajax({
					url:'request/rq2.php',
	                type:'POST',
	                dataType:'json',
	                data:{tipo:$("#tipoVencimiento").val()}

				}).done(function(repuesta){

					if(repuesta.length>0){

						
						var tipoVencimientos=[];
						var diasVencidos=[];
						var total=0;

						for(var i=0;i<repuesta.length;i++){

							tipoVencimientos.push(repuesta[i].descripcion);
							diasVencidos.push(+repuesta[i].cantidad);
							total+=(+repuesta[i].cantidad);
						}

						

						var series=[
									{
										name:'Vencidos',
										type: 'column',
										data:diasVencidos,
										dataLabels: {
											enabled: true,
                              				//rotation: -90,
                              				color: '#00034A',
                              				align: 'center',
                             				// format: '${point.y}', // one decimal
			                              	/*formatter:function(){
			                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
			                              	},*/
                              				y: 10, // 10 pixels down from the top
			                                style: {
			                                  fontSize: '10px',
			                                  fontFamily: 'Verdana, sans-serif'
			                                }
			                                
                          				},
                          				colorByPoint: true

									}
						];

						var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						dibujarGraficoBarras("grafico1",tipoVencimientos,series,colors);

						doneAjax3=true;

										
						$("#total").html(total);

						if(doneAjax2&&doneAjax3){
							$("#div-total").show();	
							$("#consultar").show();
							$("#consultando").hide();
							$("#exportar").show();
							$("#contenido").show();

						}
	

					}
					else{

						$("#total").html("");		
						$("#div-total").hide();	
						$("#tabla-general tbody").html("");
						$("#grafico1").html("");

						$("#consultando").hide();
						$("#consultar").show();
					}
				});




			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Vencimientos</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<!--<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>-->
				<div><label>Tipo vencimiento</label><select id='tipoVencimiento'></select></div>
				
				<div id="div-total">
					<h5>Total</h5>        
                	<h4 id="total">0</h4>
                	
              	</div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Detalle</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab' >Resumen</span></a></li>
							
			</ul>


			<div id='tab1'>
				<div id='div-tabla'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Tipo</td><td>Id</td><td>Descripcion</td><td>Fecha vence</td><td>Dias vencidos</td><td>Estado</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
				</table>
				</div>
			</div>




			<div id='tab2'>
				
				<div id='div-grafico1'>
					<div id='grafico1'></div>
				</div>
			</div>


		</div>

		

	</div>

	

</body>
</html>