<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	
	
	$consulta = "SELECT distinct tve.tvmodulo from tipovencimiento tve";
	

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$row_array['tipoVencimiento']="TODOS";
	array_push($return_arr, $row_array);

	while($fila = ibase_fetch_row($result)){
		
		
		$row_array['tipoVencimiento'] = utf8_encode($fila[0]);
			
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>