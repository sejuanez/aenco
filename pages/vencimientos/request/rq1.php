<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$tipo = $_POST['tipo'];
	
	
	if(strcmp($tipo,"TODOS")==0){
		$consulta = "SELECT v.modulo TIPO, v.cedula ID, tv.tvdescripcion DESCRIPCION, V.fechavence FECHA_VENCE,
       CASE
        WHEN  (current_date - v.fechavence) <=0 THEN 0
        WHEN  (current_date - v.fechavence) >0 THEN (current_date - v.fechavence)
       END DIAS_VENCIDOS,

       CASE
        WHEN CURRENT_DATE > V.fechavence THEN 'Vencida'
        WHEN CURRENT_DATE <= V.fechavence THEN 'Vigente'
       END ESTADO
		from  vencimientos v
		  left join tipovencimiento tv on tv.tvmodulo=v.modulo and tv.tvcodigo=v.codtipovence order by 1";
	}
	else{
		$consulta = "SELECT v.modulo TIPO, v.cedula ID, tv.tvdescripcion DESCRIPCION, V.fechavence FECHA_VENCE,
       CASE
        WHEN  (current_date - v.fechavence) <=0 THEN 0
        WHEN  (current_date - v.fechavence) >0 THEN (current_date - v.fechavence)
       END DIAS_VENCIDOS,

       CASE
        WHEN CURRENT_DATE > V.fechavence THEN 'Vencida'
        WHEN CURRENT_DATE <= V.fechavence THEN 'Vigente'
       END ESTADO
		from  vencimientos v
		  left join tipovencimiento tv on tv.tvmodulo=v.modulo and tv.tvcodigo=v.codtipovence
		where v.modulo='".$tipo."' order by 1";
	}
	
		
	
	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['tipo'] = utf8_encode($fila[0]);
		$row_array['cedula'] = utf8_encode($fila[1]);		
		$row_array['descripcion'] = utf8_encode($fila[2]);		
		$row_array['fecha'] = utf8_encode($fila[3]);		
		$row_array['diasVencidos'] = utf8_encode($fila[4]);		
		$row_array['estado'] = utf8_encode($fila[5]);		
									
		array_push($return_arr, $row_array);
		
	}

	echo json_encode($return_arr);

?>