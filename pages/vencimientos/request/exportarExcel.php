<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=Vencimientos_".$_GET['tipo'].".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$tipo = $_GET['tipo'];
	
	if(strcmp($tipo,"TODOS")==0){
		$consulta = "SELECT v.modulo TIPO, v.cedula ID, tv.tvdescripcion DESCRIPCION, V.fechavence FECHA_VENCE,
       CASE
        WHEN  (current_date - v.fechavence) <=0 THEN 0
        WHEN  (current_date - v.fechavence) >0 THEN (current_date - v.fechavence)
       END DIAS_VENCIDOS,

       CASE
        WHEN CURRENT_DATE > V.fechavence THEN 'Vencida'
        WHEN CURRENT_DATE <= V.fechavence THEN 'Vigente'
       END ESTADO
		from  vencimientos v
		  left join tipovencimiento tv on tv.tvmodulo=v.modulo and tv.tvcodigo=v.codtipovence order by 1";
	}
	else{
		$consulta = "SELECT v.modulo TIPO, v.cedula ID, tv.tvdescripcion DESCRIPCION, V.fechavence FECHA_VENCE,
       CASE
        WHEN  (current_date - v.fechavence) <=0 THEN 0
        WHEN  (current_date - v.fechavence) >0 THEN (current_date - v.fechavence)
       END DIAS_VENCIDOS,

       CASE
        WHEN CURRENT_DATE > V.fechavence THEN 'Vencida'
        WHEN CURRENT_DATE <= V.fechavence THEN 'Vigente'
       END ESTADO
		from  vencimientos v
		  left join tipovencimiento tv on tv.tvmodulo=v.modulo and tv.tvcodigo=v.codtipovence
		where v.modulo='".$tipo."' order by 1";
	}

	

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Tipo</td>".
						"<td>Id</td>".
						"<td>Descripcion</td>".
						"<td>Fecha vencimiento</td>".
						"<td>Dias vencidos</td>".
						"<td>Estado</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
					"<td>".utf8_encode($fila[5])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	
?>