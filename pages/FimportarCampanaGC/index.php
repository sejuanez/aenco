<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  } 
?>
<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />
    <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />
    <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='css/ionicons/css/ionicons.min.css' type='text/css' />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style type="text/css">
      .size{
        width: 80px !important;
      }
    </style>
    <title>CONELEC</title>
  </head>
  <body>
    <header>
      <h3>Cargue Datos</h3>
      <nav>
        <ul id="menu">
          <li id="consultar"><a href="#" id="btn_consultar"><span class="ion-ios-bolt"></span><h6>Ejecutar</h6></a></li>
          <li><a href="#" target="_self" id="exportar_excel" class="botonExcel"><span class="ion-ios-download-outline"></span><h6>Exportar</h6></a></li>
          <li><a href="request/modelo.xls" target="_self" id="exportar_pdf"><span class="ion-ios-browsers-outline"></span><h6>Modelo</h6></a></li>	
        </ul>
      </nav>
    </header>
    <div id='subheader'>
      <div class="container">
       <form id="formulario" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-12"><br>	<br>
              <label><b>Ruta del Archivo:</b></label>
              <input type="file" class="" id="archivo" name="archivo" >
              <br>
              <label><b>Lectura</b></label>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" id="barra">0%</div>
              </div>
            </div>
            <div class="col-md-4">
            <label></label>
            <div class="form-row">
              <div class="col-md-3" style="text-align: right;">
                <label for="inputEmail4">Procesados:</label>
              </div>
              <div class="col-md-2" style="text-align: right;">
                <span class="badge badge-primary badge-pill" id="correctos">0</span>
              </div>
            </div>
            <div class="form-row" style="text-align: right;">
              <div class="col-md-3" style="text-align: right;">
                <label for="inputEmail4">Rechazados:</label>
              </div>
              <div class="col-md-2">
                <span class="badge badge-primary badge-pill" id="errores">0</span>
              </div>
            </div>
            <div class="form-row" style="text-align: right;">
              <div class="col-md-3" style="text-align: right;">
                <label for="inputEmail4">Total:</label>
              </div>
              <div class="col-md-2">
                <span class="badge badge-primary badge-pill" id="total">0</span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="container">
              <div class="alert alert-success" style="top:1em;display:none" id="bsalert">
                <a href="#" class="close" id="close_alert"  aria-label="close">&times;</a>
                <div id="mensaje_altera">Proceso de importación terminado!</div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" value="consultar" name="accion" id="accion"/>
        </form>
      </div>
    </div>
                                            
    <div class="container-fluid" id="tabInventario" >
      <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive">
            <table class="table table-bordered table-condensed" id="Exportar_a_Excel" border="1">
              <thead>
                  <tr style="text-align:center">
                  <th scope="col">Campaña</th>
                  <th scope="col">Unicom</th>
                  <th scope="col">Nic</th>
                  <th scope="col">NisRad</th>
                  <th scope="col">Departamento</th>
                  <th scope="col">Municipio</th>
                  <th scope="col">Corregimiento</th>
                  <th scope="col">Barrio</th>
                  <th scope="col">F.Entrega</th>
                  <th scope="col">Error </th>
                </tr>
              </thead>
              <tbody id="cuerpo_tabla"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <form action="./request/exportar.php" method="post" target="_blank" id="FormularioExportacion">
      <input type="hidden" id="datos_a_enviar" name="datos_a_enviar"/>
    </form>
    <script src="js/jquery.min.js"></script>
    <!-- Boostrap JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/xls.js"></script>
    <script src="js/readXLS.js"></script>
  </body>
</html>
