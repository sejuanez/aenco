// ----------------------------------------------
// On Load page
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(".selectVendedor").select2();
    $(".selectLista").select2().change(function (e) {
        var lista = $(this).val();
        app.onChangeLista(lista);

    });

    $(".selectArticulo").select2().change(function (e) {
        var articulo = $(this).val();
        app.codArticulo = articulo;

    });
    ;

    $(".selectVendedor").select2().change(function (e) {
        var vendedor = $(this).val();
        app.codVendedor = vendedor;

    });
    ;

    $("#exportar_excel").click(function () {

        var elt = document.getElementById('datos');
        var wb = XLSX.utils.table_to_book(elt, {
            sheet: "Sheet JS"
        });
        return XLSX.writeFile(wb, 'reporte_cosunsulta_vendedores.xlsx');
    });

    $("#tabValorizado").click(function () {

        app.graficar(app.obtenerValorizadoYCantidades(app.tablaPedidos));


    });

    $("#tabCantidades").click(function () {

        app.graficar(app.obtenerValorizadoYCantidades(app.tablaPedidos));


    });


});
// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        btnBuscar: true,
        btnBuscando: false,
        btnExportar: false,

        idLista: "",
        codArticulo: "",
        fecha: "",
        codVendedor: "",


        selectVendedores: [],
        selectListas: [],
        selectArticulos: [],
        mapCords:[],


        tablaPedidos: [],

    },


    methods: {

        loadVendedores: function () {

            var app = this;
            $.get('./request/getVendedor.php', function (data) {
                app.selectVendedores = JSON.parse(data);
            });

        },

        loadListas: function () {

            $.get('request/getLista.php', function (data) {
                app.selectListas = JSON.parse(data);
            });

        },

        onChangeLista: function (Lista) {

            var app = this;
            app.idLista = Lista;
            app.selectArticulos = [];
            if (app.idLista != "") {
                $.post('./request/getArticulos.php', {lista: Lista}, function (data) {

                    var data = JSON.parse(data);
                    app.selectArticulos = data;

                });
            }

        },


        loadFecha: function () {

            var today = new Date();
            this.fecha = (getFormattedDate(today));

            // Get date formatted as YYYY-MM-DD
            function getFormattedDate(date) {
                return date.getFullYear() +
                    "-" +
                    ("0" + (date.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("0" + date.getDate()).slice(-2);
            }

        },

        loadMapa: function () {

            var map;
            var limits;

            map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                el: '#div-map',
                lat: 8.749349, /*position.coords.latitude,*/
                lng: -75.879568, /*position.coords.longitude,*/
                zoom: 15
            });

            limits = new google.maps.LatLngBounds();

            $("#container-map").css({width: '100%', height: "100%"});
            $("#div-map").css({width: '100%', height: "400px"});
            google.maps.event.addDomListener(window, "resize", function () {

                var center_lat = parseFloat(map.getCenter().lat()).toPrecision(4);
                var center_lng = parseFloat(map.getCenter().lng()).toPrecision(5);


                map.setCenter(center_lat, center_lng);


            });

            

            // si hay clientes para mostrar

            if (app.tablaPedidos.length > 0) {

                if(app.tablaPedidos.length>0){

                    for (var i = 0; i < app.tablaPedidos.length; i++) {

                        if (app.tablaPedidos[i].Latitud == null || app.tablaPedidos[i].Longitud == null) {
    
                        } else {

                            app.mapCords.push(app.tablaPedidos[i]);
                            
                        }

                    }

                }

                
                map.removeMarkers();

                if(app.mapCords.length>0){

                    var lat = parseFloat(app.mapCords[0].Latitud);
                    var lng = parseFloat(app.mapCords[0].Longitud);

                    limits=new google.maps.LatLngBounds();
                    map.setCenter( lat, lng );
                    map.setZoom(16);

                

                for (var i = 0; i < app.mapCords.length; i++) {                

                    var lat = parseFloat(app.tablaPedidos[i].Latitud);
                    var lng = parseFloat(app.tablaPedidos[i].Longitud);

                    map.addMarker({

                        lat: lat,
                        lng: lng,
                        icon: './img/' + app.tablaPedidos[i].ESTADO + '.png',
                        title: app.tablaPedidos[i].razon_social,
                        infoWindow: {
                            content: "<h6 class='title-foto' >" + app.tablaPedidos[i].razon_social + "</h6>" +
                                "<p>" + app.tablaPedidos[i].direcccion +
                                "<br>Vendedor: " + app.tablaPedidos[i].vendedor + "" +
                                "<br>Numero Pedido: " + app.tablaPedidos[i].NumeroPedido + "" +
                                "<br>Valor Pedido: " + app.tablaPedidos[i].ValorPedido +
                                "</P>"

                        }
                    });

                    
                    limits.extend(new google.maps.LatLng(lat, lng));

                }
            }

                

            }


        },

        consultar: function () {


            if (this.fecha == "" ||
                this.selectListas == "" ||
                this.selectArticulos == ""
            ) {
                $('.formConsultarPedido').addClass('was-validated');
                alertify.error("Por favor selecciona una Lista y un Articulo");

            } else {


                app.btnBuscando = true;
                app.btnBuscar = false;
                app.btnExportar = false;

                var fecha = new Date(this.fecha);
                var dias = ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"];
                var diaRuta = dias[fecha.getUTCDay()];

                // ruta a enviar como parametros para consulta
                // console.log(ruta);


                $.post('./request/getPedidos.php', {
                    codArticulo: app.codArticulo,
                    fecha: app.fecha,
                    vendedor: app.codVendedor,
                    ruta: diaRuta
                }, function (data) {

                    var datos = jQuery.parseJSON(data);
                    app.tablaPedidos = [];
                    app.tablaPedidos = datos;

                    app.btnBuscando = false;
                    app.btnBuscar = true;

                    var arrayGraficas = app.obtenerValorizadoYCantidades(datos);
                    app.graficar(arrayGraficas);

                    app.loadMapa();

                    app.btnExportar = true;


                });

            }


        },

        obtenerValorizadoYCantidades: function (arrayDatos) {

            let result = [];

            const elementExist = (array, value) => {
                let i = 0;
                while (i < array.length) {
                    if (array[i].vendedor == value) return i;
                    i++;
                }
                return false;
            }

            arrayDatos.forEach((e) => {
                let i = elementExist(result, e.vendedor);
                if (i === false) {
                    // Si no existe, creo agrego un nuevo objeto.
                    var Cantidad_float = parseFloat(e.Cantidad);
                    var ValorPedido_float = parseFloat(e.ValorPedido);
                    result.push({
                        "vendedor": e.vendedor,
                        "Cantidad": Cantidad_float,
                        "ValorPedido": ValorPedido_float
                    });
                } else {
                    // Si el ya existe agrego el nuevo elemento a el array valor.
                    var Cantidad_float = parseFloat(e.Cantidad);
                    var ValorPedido_float = parseFloat(e.ValorPedido);
                    result[i].Cantidad += Cantidad_float;
                    result[i].ValorPedido += ValorPedido_float;
                }
            });

            return result;
        },

        graficar: function (arrayDatos) {

            var colors = ['#800080', '#00FF7F', '#FF6347', '#FFFF00', '#8A2BE2', '#9932CC', '#FF8C00', '#FF1493', '#ADFF2F', '#FFD700', '#008000', '800000', '#6B8E23', '#FF4500', '#FF0000', '#800080', '#00FF7F', '#FF6347', '#FFFF00', '#8A2BE2', '#9932CC', '#FF8C00', '#FF1493', '#ADFF2F', '#FFD700', '#008000', '800000', '#6B8E23', '#FF4500', '#FF0000', '#800080', '#00FF7F', '#FF6347', '#FFFF00', '#8A2BE2', '#9932CC', '#FF8C00', '#FF1493', '#ADFF2F', '#FFD700', '#008000', '800000', '#6B8E23', '#FF4500', '#FF0000'];


            var hc_column = [];// vendedores vendidos
            var hc_column_clientesImpactados = [];// vendedores cantidad

            var totalrecaudo = 0;
            var totalClientesImpactados = 0;

            for (var i = 0; i < arrayDatos.length; i++) {

                hc_column.push([arrayDatos[i].vendedor, arrayDatos[i].ValorPedido]);
                hc_column_clientesImpactados.push([arrayDatos[i].vendedor, arrayDatos[i].Cantidad]);

                totalrecaudo += (arrayDatos[i].ValorPedido);//total preventas
                totalClientesImpactados += (arrayDatos[i].Cantidad);//total clientes impactados
            }


            totalrecaudo = accounting.formatMoney(totalrecaudo, {symbol: '$', precision: 0, thousand: ','});
            $("#total-preventas").html(totalrecaudo);

            totalClientesImpactados = accounting.formatMoney(totalClientesImpactados, {
                symbol: '',
                precision: 0,
                thousand: ','
            });
            $("#total-clientesImpactados").html(totalClientesImpactados);


            app.drawColumnChart(hc_column, "grafico-preventas", colors);//DIBUJAR EL GRAFICO preventas
            app.drawColumnChartSinFormato(hc_column_clientesImpactados, "grafico-clientesImpactados", colors);//DIBUJAR EL GRAFICO preventas
            $("g.highcharts-data-labels>g[opacity=0]").attr("opacity", 1);

            $("#div-total-preventas").fadeIn(200);
            $("#div-grafico-preventas").fadeIn(200);

            $("#div-total-clientesImpactados").fadeIn(200);
            $("#div-grafico-clientesImpactados").fadeIn(200);
        },

        drawColumnChart: function (rows, element, colors) {


            $('#' + element).highcharts({
                chart: {
                    type: 'column',
                    backgroundColor: '#ffffff'
                },
                colors: colors,
                credits: {enabled: false},
                title: null /*{
                                       text: 'Recaudo diario'
                                   }*/,
                /*subtitle: {
                    //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                    text:dia+'/'+mes+'/'+anio
                },*/
                xAxis: {
                    type: 'category',
                    labels: {
                        //rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: null /*{
                                           text: 'Recaudo diario ($)'
                                       }*/
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<b>${point.y}</b>',
                    pointFormatter: function () {
                        return accounting.formatMoney(this.y, {symbol: '$', precision: 0, thousand: ','});
                    }
                },
                series: [{
                    name: 'recaudo',
                    data: rows,
                    colorByPoint: true,
                    dataLabels: {
                        enabled: true,
                        //rotation: -90,
                        color: '#000',
                        align: 'center',
                        // format: '${point.y}', // one decimal
                        formatter: function () {
                            return accounting.formatMoney(this.y, {symbol: '$', precision: 0, thousand: ','});
                        },
                        y: 10, // 10 pixels down from the top
                        padding: 20,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        },

        drawColumnChartSinFormato: function (rows, element, colors) {


            $('#' + element).highcharts({
                chart: {
                    type: 'column',
                    backgroundColor: '#ffffff'
                },
                colors: colors,
                credits: {enabled: false},
                title: null /*{
                                       text: 'Recaudo diario'
                                   }*/,
                /*subtitle: {
                    //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                    text:dia+'/'+mes+'/'+anio
                },*/
                xAxis: {
                    type: 'category',
                    labels: {
                        //rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: null /*{
                                           text: 'Recaudo diario ($)'
                                       }*/
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<b>{point.y}</b>',
                    pointFormatter: function () {
                        return accounting.formatMoney(this.y, {symbol: '', precision: 0, thousand: ','});
                    }
                },
                series: [{
                    name: 'recaudo',
                    data: rows,
                    colorByPoint: true,
                    dataLabels: {
                        enabled: true,
                        //rotation: -90,
                        color: '#000',
                        align: 'center',
                        // format: '${point.y}', // one decimal
                        formatter: function () {
                            return accounting.formatMoney(this.y, {symbol: '', precision: 0, thousand: ','});
                        },
                        y: 10, // 10 pixels down from the top
                        padding: 20,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        }


    },

    watch: {},

    mounted() {

        this.loadVendedores();
        this.loadListas();
        this.loadFecha();


    },

});