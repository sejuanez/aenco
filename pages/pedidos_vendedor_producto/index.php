<!DOCTYPE html>
<html>

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
    <script type="text/javascript" src="../../js/gmaps.js"></script>

    <script lang="javascript" src="js/xlsx.full.min.js"></script>
    <script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>


    <style type="text/css">

        #grafico-preventas, #grafico-clientesImpactados {
            min-width: 73%;
            height: 340px;
        }

        #container-map {
            height: 95%;
            width: 80%;
        }

        #div-map {
            height: 100%;
            width: 100%;
        }

        table tr.cabecera {
            background-color: #00034A;
        }

        table tr.cabecera td {
            color: #fff;
            text-align: center;
        }

        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled,
        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .table-wrapper-scroll-y {
            display: block;
            max-height: 370px;
            overflow-y: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }


    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Pedidos Vendedor Producto

            <span id="consultar" v-show="btnBuscar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;"
                  @click="consultar();">
                    <i class="ion-ios-search-strong" aria-hidden="true"></i> Consultar
                </span>


            </span>

            <span id="consultando" v-show="btnBuscando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;">
                    <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Consultando...
                </span>


        </p>
    </header>


    <div class="container" style="max-width: 95%;">
        <form class="formConsultarPedido">
            <div class="row">


                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label for="placa">Lista</label>
                        <div class="input-group input-group-sm">
                            <select class="selectLista" id="selectLista" @change="datosLista(this.value);">
                                <option value=" ">Seleccione una Lista...</option>
                                <option v-for="lista in selectListas " :value="lista.id ">{{ lista.text }}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label for="placa">Articulo</label>
                        <div class="input-group input-group-sm">
                            <select class="selectArticulo" id="selectArticulo">
                                <option value=" ">Seleccione un Articulo...</option>
                                <option v-for="articulo in selectArticulos " :value="articulo.id ">{{ articulo.text
                                    }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label for="placa">Vendedor</label>
                        <div class="input-group input-group-sm">
                            <select class="selectVendedor" id="selectVendedor">
                                <option value=" ">Todos</option>
                                <option v-for="vendedor in selectVendedores " :value="vendedor.id ">{{vendedor.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label for="placa">Fecha</label>
                        <div class="input-group input-group-sm">
                            <input type="date" id="fecha" name="fecha" class="form-control" aria-label="fecha"
                                   style="height: 28px;"
                                   v-model="fecha" :value="fecha">
                        </div>
                    </div>
                </div>


            </div>


        </form>


        <div class="row">

            <div class="container" style="max-width: 100%;">

                <br>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#datos">Datos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#mapa">Mapa</a>
                    </li>
                    <li class="nav-item" id="tabValorizado">
                        <a class="nav-link" data-toggle="tab" href="#valorizado">Valorizado por Vendedor</a>
                    </li>
                    <li class="nav-item" id="tabCantidades">
                        <a class="nav-link" data-toggle="tab" href="#cantidades">Cantidades por Vendedor</a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="datos" class=" tab-pane active"><br>

                        <div class="row">

                            <div class="col-12">
                                        <span id="exportar_excel" v-show="btnExportar" class="float-right"
                                              style="font-size: 1em; cursor: pointer; width: 130px;">
                                            <i class="ion-ios-download-outline" aria-hidden="true"></i> Exportar
                                        </span>
                                <br>
                            </div>
                        </div>


                        <div class="row ">
                            <div class="col-12 table-wrapper-scroll-y">
                                <table class="table table-sm table-striped" style="font-size: 0.8rem;">
                                    <thead class="fondoGris ">
                                    <tr class="cabecera ">
                                        <td>N° Pedido</td>
                                        <td>Nit Cliente</td>
                                        <td>Empresa</td>
                                        <td>Direccion</td>
                                        <td>Articulo</td>
                                        <td>Cantidad</td>
                                        <td>Valor del Pedido</td>
                                        <td>Vendedor</td>
                                        <td>Ruta</td>
                                        <td>Estado</td>
                                    </tr>
                                    </thead>
                                    <tbody id="detalle_gastos ">
                                    <tr v-for="(dato, index) in tablaPedidos ">
                                        <td v-text="dato.NumeroPedido "></td>
                                        <td v-text="dato.nit_cliente "></td>
                                        <td v-text="dato.razon_social "></td>
                                        <td v-text="dato.direcccion "></td>
                                        <td v-text="dato.Articulo "></td>
                                        <td v-text="dato.Cantidad "></td>
                                        <td v-text="dato.ValorPedido "></td>
                                        <td v-text="dato.vendedor "></td>
                                        <td v-text="dato.ruta "></td>
                                        <td v-text="dato.ESTADO "></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>


                    <!-- Datos conductor -->

                    <div id="mapa" class="container tab-pane fade"><br>


                        <div id='tab2'>
                            <div id="container-map">
                                <div id="div-map"></div>
                            </div>
                        </div><!--End tab2-->


                    </div>

                    <div id="valorizado" class="container tab-pane fade"><br>

                        <div id="div-total-preventas">
                            <h3 id="total-preventas" class="text-center">0</h3>
                            <h5 class="text-center">Valorizado por Vendedor</h5>
                        </div>

                        <div id='div-grafico-preventas'>
                            <div id='grafico-preventas'></div>
                        </div>


                    </div>

                    <div id="cantidades" class="container tab-pane fade"><br>

                        <div id="div-total-clientesImpactados">
                            <h3 class="text-center" id="total-clientesImpactados">0</h3>
                            <h5 class="text-center">Total</h5>
                        </div>

                        <div id='div-grafico-clientesImpactados'>
                            <div id='grafico-clientesImpactados'></div>
                        </div>
                    </div>


                </div>


            </div>
        </div>

    </div>


</div>


</div>


<script src="js/select2.min.js "></script>
<script src="js/bootstrap/popper.js "></script>
<script src="js/bootstrap/bootstrap.min.js "></script>
<script src="js/alertify/alertify.min.js "></script>
<script src="js/vue/vue.js "></script>
<script src="js/app.js "></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript" src="../../js/accounting.js"></script>


</body>

</html>