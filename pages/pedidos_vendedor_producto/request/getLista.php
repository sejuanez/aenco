<?php
  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

  include("../../../init/gestion.php");
  // include("gestion.php");


  $sql = "  SELECT l.li_codigo, l.li_nombre
            from listas l";

  $return_arr = array();

  $result = ibase_query($conexion, $sql);

  while($fila = ibase_fetch_row($result)){
    $row_array['id'] = utf8_encode($fila[0]);
    $row_array['text'] = utf8_encode($fila[1]);
    array_push($return_arr, $row_array);
  }

  // $array = array("result"=>$return_arr);

  echo json_encode($return_arr);
  // echo json_encode($array);


?>