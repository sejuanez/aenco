<?php
  session_start();
  if (!$_SESSION['user']) {
    echo "<script>window.location.href='../inicio/index.php';</script>";
    exit();
  }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Legalización</title>
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    *{
      margin: 0;
      padding: 0;
    }
    .el-header{
      background-color: #EDEDED;
      border-bottom: 1px solid #DEDEDE;
      color: #333;
      /*text-align: center;*/
      /*line-height: 60px;*/
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .buscar {
      padding-left: 0px !important;
    }
    .btn-excel{
      text-decoration: none;
      height: 29px;
      margin-left: -1.5rem;
      margin-top: 1.75rem;
      font-size: 12px;
      /*padding: 10px 2px 1px;
      font-weight: 600;
      
      color: #000000;
      background-color: #FFFFFF;
      border-radius: 3px;
      padding: 0px 21px 11px 17px;
      border: 0px solid #ccc;
      margin-left: 1.5rem;*/
    }
    #header {
      background-color: #ededed;
      border-bottom: 1px solid #dedede;
      padding: 4px 10px;
      margin-bottom: 2px;
      -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 2;
    }
    /* #totales {
      border-collapse: collapse !important;
      font-family: "Segoe UI", verdana,Arial,sans-serif !important;
      color: #5f5f5f !important;
      text-shadow: 0px 1px #ffffff !important;
      font-size: 80% !important;
    } */
    #totales td {
      padding: 0 8px;
      border-right: 1px solid #d3d3d3;
      text-align: center;
    }
    #totales tr:hover{
      background-color: transparent;
    }
    #totales, .data tr{
      color: #5f5f5f !important;
      font-weight: normal;
    }
    .el-table__footer-wrapper tbody td, .el-table__header-wrapper tbody td{
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
    .tg .tg-lqy6{text-align:right;vertical-align:top}
    .tg .tg-cjgz{background-color:rgb(0, 3, 74);color:#ffffff;border-color:rgb(0, 3, 74);text-align:left;vertical-align:top}
    .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
    .tg .tg-dvpl{border-color:inherit;text-align:right;vertical-align:top}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    tr:hover {
      background-color: #f5f5f5;
    }
    .table-striped>tbody>tr:nth-of-type(odd){background-color: #e8e8e8;}
    #modal {
      margin-top: -1rem;
      padding-top: 150px;
      width: 100%;
      height: 100%;
      position: fixed;
      right: 0%;
      background-color: #e7e7e7;
      z-index: 1;
    }
    #modal h1 {
      color: #5f5f5f;
      text-shadow: 0 1px #ffffff;
      font-size: 300%;
      text-align: center;
      font-family: verdana,Arial,sans-serif;
    }
    #modal p {
      color: #5f5f5f;
      text-shadow: 0 1px #ffffff;
      font-size: 150%;
      text-align: center;
      font-family: verdana,Arial,sans-serif;
    }
    @media only screen and (max-width: 768px) {
      .btn-mobile{
        margin-left: 93px !important;
      }
      .el-date-editor.el-input, .el-date-editor.el-input__inner {
        width: 141px;
      }
    }
  </style>
  <script src="js/jquery/jquery-3.2.1.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<body>
  <div id="app">
    <el-container>
      <div id="header">
        <el-form size="mini">
          <el-row :gutter="4">
            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Fecha programación">
                <el-date-picker
                  class="fecha"
                  style="width: 100%"
                  v-model="fecha"
                  type="date"
                  format="MM/dd/yyyy"
                  value-format="MM/dd/yyyy">
                </el-date-picker>
              </el-form-item>
            </el-col>

            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Delegación">
                <el-select v-model="delegacion" clearable placeholder="(TODOS)" style="width: 100%">
                  <el-option
                    v-for="item in optionsContratista"
                    :key="item.t_delegacion"
                    :label="item.t_delegacion"
                    :value="item.t_delegacion">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="6" :sm="24" :md="3" :lg="3" :xl="3">
              <el-form-item>
                <el-button @click="onQuery" icon="el-icon-search" style="margin-top: 1.75rem;">Consultar</el-button>
              </el-form-item>
            </el-col>

            <el-col v-show="sw" :xs="6" :sm="24" :md="1" :lg="1" :xl="1" class="excel">
              <a id="exportar" href="" class="el-button btn-excel">
                <span><p style="margin-top: -.3rem;">Exportar</p></span>
              </a>
            </el-col>

            <ul v-show="sw" style="list-style: none;margin-top: 1.2rem;float: right;">
              <li>
                <table id="totales">
                  <thead>
                    <tr>
                      <td>Asignadas</td>
                      <td>Ejecutadas</td>
                      <td>Pendientes</td>
                    </tr>
                  </thead>
                  <tbody id="cuerpo">
                    <tr class="data">
                      <td id="asignada" v-text="asignadas"></td>
                      <td id="ejecutada" v-text="ejecutadas"></td>
                      <td id="pendiente" v-text="pendientes"></td>
                    </tr>
                  </tbody>
                </table>
              </li>
            </ul>
          </el-row>
        </el-form>
      </div>
      <!-- v-show="sw" <el-header>
        <el-form size="mini">
          <el-row :gutter="4">
            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Fecha">
                <el-date-picker
                  style="width: 100%"
                  v-model="fecha"
                  type="date"
                  format="MM/dd/yyyy"
                  value-format="MM/dd/yyyy">
                </el-date-picker>
              </el-form-item>
            </el-col>

            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Delegación">
                <el-select v-model="delegacion" clearable placeholder="(TODOS)" style="width: 100%">
                  <el-option
                    v-for="item in optionsContratista"
                    :key="item.t_delegacion"
                    :label="item.t_delegacion"
                    :value="item.t_delegacion">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="6" :sm="24" :md="3" :lg="3" :xl="3">
              <el-form-item>
                <el-button @click="onQuery" icon="el-icon-search" style="margin-top: 1.75rem;">Consultar</el-button>
              </el-form-item>
            </el-col>

            <el-col :xs="6" :sm="24" :md="1" :lg="1" :xl="1" v-show="sw">
              <el-form-item>
                <el-button @click="exportar" class="btn-mobile">Exportar</el-button>
              </el-form-item>
              
                <a id="exportar" href="" class="el-button btn-excel">
                  <span>Exportar</span>
                </a>
            </el-col>
          </el-row>
        </el-form>
        <h3>Funcionarios</h3>
      </el-header> -->
      <el-main style="margin-top: 5rem;">

        <!-- <div id="modal">
          <h1 id="titulo_modal">Bienvenido!</h1>
          <h1 id="buscando" style="display: none;">Consultando por favor espere..!</h1>
          <p>Escoge una fecha y selecciona una delegación, luego pulsa Consultar...</p>
        </div> -->

        <el-row>
          <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
            <!-- <table class="tg table-striped" style="width: 100%;">
              <thead>
                <tr>
                  <th class="tg-cjgz">Codigo</th>
                  <th class="tg-cjgz">Nombre</th>
                  <th class="tg-cjgz">Asignadas</th>
                  <th class="tg-cjgz">Ejecutadas</th>
                  <th class="tg-cjgz">Pendientes</th>
                </tr>
              </thead>
              <tbody id="datos">
              </tbody>
            </table> -->
            <el-table
              v-loading="loadingTable"
              class="x"
              :data="tableData"
              height="500"
              size="mini"
              border
              style="width: 100%">
              <el-table-column
                prop="codfuncionario"
                label="Codigo"
                width="80">
              </el-table-column>
              <el-table-column
                prop="te_nombres"
                label="Nombre">
              </el-table-column>
              <el-table-column
                prop="asigadas"
                label="Asignadas"
                width="100">
              </el-table-column>
              <el-table-column
                prop="ejecutadas"
                label="Ejecutadas"
                width="100">
              </el-table-column>
              <el-table-column
                prop="total"
                label="Pendientes"
                width="100">
              </el-table-column>
            </el-table>
          </el-col>
        </el-row>
      </el-main>
    </el-container>
  </div>
  <script>
    ELEMENT.locale(ELEMENT.lang.es);
    new Vue({
      el: '#app',
      data: () => ({
        fecha: '',
        delegacion: '',
        tableData: [],
        optionsContratista: [],
        loadingTable: false,
        sw: false,
        state: 0,
        totalAsigadas: 0,
        totalEjecutada: 0,
        totalPendientes: 0,
        asignadas: 0,
        ejecutadas: 0,
        pendientes: 0
      }),
      methods: {
        onQuery () {
          this.loadingTable = true
          var data = new FormData();
          data.append('fecha', this.fecha);
          data.append('delegacion', this.delegacion);
          axios.post('request/getGestionCobroEntrega.php', data).then(response => {
            // this.tableData = response.data;
            // console.log(response.data);
            if (response.data.length !== 0) {
              this.tableData = response.data;
              this.totalAsigadas = 0;
              this.totalEjecutada = 0;
              this.totalPendientes = 0;
              response.data.forEach(items => {
                this.totalAsigadas += parseFloat(items.asigadas);
                this.totalEjecutada += parseFloat(items.ejecutadas);
                this.totalPendientes += parseFloat(items.total);
              });
              this.asigadas = 0;
              this.ejecutadas = 0;
              this.pendientes = 0;
              this.asignadas = this.totalAsigadas.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
              this.ejecutadas = this.totalEjecutada.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
              this.pendientes = this.totalPendientes.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
              this.sw = true;
              var fecha = this.fecha;
              $('#exportar').attr('href', 'request/exportarExcel.php?fecha='+fecha+'&delegacion='+this.delegacion+'');
            } else {
              this.tableData = [];
              this.asigadas = 0;
              this.ejecutadas = 0;
              this.pendientes = 0;
              this.sw = false;
            }
            this.loadingTable = false
          }).catch(e => {
            console.log(e.response);
            this.loadingTable = false
          });
        },
        showContratistas () {
          axios.get('request/contratistas.php').then(response => {
            this.optionsContratista = response.data;
          }).catch(error => {
            console.log(error.response);
          })
        },
        getSummaries(param) {
          const { columns, data } = param;
          const sums = [];
          columns.forEach((column, index) => {
            if (index === 0) {
              sums[index] = 'Resumen';
              return;
            }
            const values = data.map(item => Number(item[column.property]));
            if (!values.every(value => isNaN(value))) {
              sums[index] = values.reduce((prev, curr) => {
                const value = Number(curr);
                if (!isNaN(value)) {
                  var resultado = prev + curr;
                  return resultado;
                } else {
                  return prev;
                }
              }, 0);
            } else {
              sums[index] = '';
            }
          });
          return sums;
        },
        formatNumber (num) {
          return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        },
        round (value) {
          return Math.round(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        },
        format(input) {
          var num = input.value.replace(/\./g,'');
          if(!isNaN(num)){
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            input.value = num;
          } else{ alert('Solo se permiten numeros');
            input.value = input.value.replace(/[^\d\.]*/g,'');
          }
        }
      },
      mounted () {
        $('.excel').hide();
        this.showContratistas()
        var f = new Date();
        this.fecha = (f.getMonth() +1) + '/' + f.getDate() + '/' + f.getFullYear();
      }
    })
  </script>
</body>
</html>