<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
            header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
            header('Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
            header('content-type: application/json; charset=utf-8');
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }
    /* session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    } */
    include('../../../init/gestion.php');

    $query = "SELECT a.*, COALESCE(b.ejecutadas, 0)ejecutadas from
                (SELECT t.te_codigo CodFuncionario, t.te_nombres, COUNT(*) asigadas
                FROM gestion_cobro_entrega gce
                LEFT JOIN tecnicos t ON t.te_codigo = gce.tecnico
                WHERE gce.fecha_programacion = '07/31/2018' AND gce.delegacion='10 Sucre' GROUP BY t.te_codigo, t.te_nombres) as a
                LEFT JOIN
                (SELECT lc.ca_tecnico, COUNT(*) ejecutadas FROM lega_cabecera lc
                WHERE lc.ca_fechaej = '07/31/2018' GROUP BY lc.ca_tecnico) as b
                on a.CodFuncionario = b.ca_tecnico";
    $data = ibase_query($conexion, $query);

    $return_arr = array();
    // $return_arr2 = array();
    while ($row = ibase_fetch_row($data)) {
        $row_array['CodFuncionario'] = utf8_encode($row[0]);
        $row_array['te_nombres']     = utf8_encode($row[1]);
        $row_array['asigadas']       = utf8_encode($row[2]);
        $row_array['ejecutadas']     = utf8_encode($row[3]);
        $row_array['total'] = $row_array['asigadas'] - $row_array['ejecutadas'];

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);

    // echo "<pre>";
    // print_r ($return_arr);
    // echo "</pre>";
    // exit();
?>