<?php
	session_start();
  	if(!$_SESSION['user']){
        echo"<script>window.location.href='../../inicio/index.php';</script>";
        exit();
    }
    
    header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=gestion_".$_GET["fecha"]."-".$_GET["delegacion"].".xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	include("../../../init/gestion.php");

	$fecha = $_GET["fecha"];
	$delegacion = $_GET["delegacion"];

	$param = "";

	if (!empty($delegacion)) {
		$param = "AND gce.delegacion='$delegacion' ";
	}

	$query = "SELECT a.*, COALESCE(b.ejecutadas, 0)ejecutadas FROM
			(SELECT t.te_codigo CodFuncionario, t.te_nombres, COUNT(*) asigadas
			FROM gestion_cobro_entrega gce
			LEFT JOIN tecnicos t ON t.te_codigo = gce.tecnico
			WHERE gce.fecha_programacion = '$fecha' $param GROUP BY t.te_codigo, t.te_nombres) as a
			LEFT JOIN
			(SELECT lc.ca_tecnico, COUNT(*) ejecutadas FROM lega_cabecera lc
			WHERE lc.ca_fechaej = '$fecha' GROUP BY lc.ca_tecnico) as b
			on a.CodFuncionario = b.ca_tecnico";
	
	$return_arr = array();

	$data = ibase_query($conexion, $query);

	$table = "<table><tr class='cabecera'><th>Codigo</th><th>Nombre</th><th>Asignadas</th><th>Ejecutadas</th><th>Pendientes</th></tr>";

	$asignadas  = 0;
	$ejecutadas = 0;
	$pendientes = 0;
	$total = 0;
	while($row = ibase_fetch_row($data)) {
		$table.="<tr class='fila'>";
			$table.="<td>".$row_array['CodFuncionario'] = utf8_encode($row[0])."</td>";
			$table.="<td>".$row_array['te_nombres']     = utf8_encode($row[1])."</td>";
			$table.="<td>".$row_array['asigadas']       = utf8_encode($row[2])."</td>";
			$table.="<td>".$row_array['ejecutadas']     = utf8_encode($row[3])."</td>";
			$table.="<td>".$row_array['total'] = intval($row[2] - $row[3])."</td>";
		$table.="</tr>";
		$asignadas += $row[2];
		$ejecutadas += $row[3];
		$total = intval($row[2] - $row[3]);
		$pendientes += $total;
	}
		$table.="<tr>";
			$table.="<td colspan='2'>Resumen</td>";
			$table.="<td>".number_format($asignadas, 0)."</td>";
			$table.="<td>".number_format($ejecutadas, 0)."</td>";
			$table.="<td>".number_format($pendientes, 0)."</td>";
		$table.="</tr>";
	$table.="</table>";
	echo $table;
?>