<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $fecha = $_POST['fecha'];
    $delegacion = $_POST["delegacion"];

	$param = "";

	if (!empty($delegacion)) {
		$param = "AND gce.delegacion='$delegacion' ";
	}

    $query = "SELECT a.*, COALESCE(b.ejecutadas, 0)ejecutadas from
                (SELECT t.te_codigo CodFuncionario, t.te_nombres, COUNT(*) asigadas
                FROM gestion_cobro_entrega gce
                LEFT JOIN tecnicos t ON t.te_codigo = gce.tecnico
                WHERE gce.fecha_programacion = '$fecha' $param GROUP BY t.te_codigo, t.te_nombres) as a
                LEFT JOIN
                (SELECT lc.ca_tecnico, COUNT(*) ejecutadas FROM lega_cabecera lc
                WHERE lc.ca_fechaej = '$fecha' GROUP BY lc.ca_tecnico) as b
                on a.CodFuncionario = b.ca_tecnico";
    $data = ibase_query($conexion, $query);

    $return_arr = array();
    // $return_arr2 = array();
    while ($row = ibase_fetch_assoc($data)) {
    //    $row_array['CodFuncionario'] = utf8_encode($row[0]);
       // $row_array['te_nombres']     = utf8_encode($row[1]);
       // $row_array['asigadas']       = utf8_encode($row[2]);
       // $row_array['ejecutadas']     = utf8_encode($row[3]);
       // $row_array['total'] = $row_array['asigadas'] - $row_array['ejecutadas'];
        $row['total'] = $row['ASIGADAS'] - $row['EJECUTADAS'];
        array_push($return_arr, array_change_key_case($row));
    }
    echo json_encode($return_arr);

    // echo "<pre>";
    // print_r ($return_arr);
    // echo "</pre>";
    // exit();
?>