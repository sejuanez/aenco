<?php

include("../mpdf.php");
include("../../../../../init/gestion.php");
ini_set('memory_limit', '-1');
set_time_limit(0);

$agente = $_POST['placa'];
$fecha = $_POST['fecha'];

if($agente == ""){
    $sql  = "   SELECT 
            
            g.id ORDEN, 
            g.nic NIC, 
            g.corregimiento CORREGIMIENTO, 
            g.municipio MUNICIPIO,
            G.barrio BARRIO, 
            g.cliente CLIENTE, 
            g.aol AOL,

            G.TIPO_VIA,
            G.NOM_CALLE,
            G.DUPLICADOR,
            G.NRO_PUERTA,
            G.CGV,
            G.REF_DIR,
            G.BARRIO,

            g.tarifa TARIFA,
            G.estado_sum ESTADOSUM, 
            g.medidor MEDIDOR, 
            G.DEUDA_TERCEROS,
            G.DEUDA_ENERGIA,
            g.importe_acordado IMPORTE_ACO, 
            g.fact_venc FV,
            g.fact_aco FA, 
            g.fecha_programacion, 
            t.te_nombres GESTOR, ITIN
            from gestion_cobro_entrega g
            left join tecnicos t on t.te_codigo = g.tecnico

            where
           
            g.fecha_programacion = '".$fecha."'
            and g.acta is null
            order by g.tecnico
";

} else {
    $sql  = "   SELECT 
            
            g.id ORDEN, 
            g.nic NIC, 
            g.corregimiento CORREGIMIENTO, 
            g.municipio MUNICIPIO,
            G.barrio BARRIO, 
            g.cliente CLIENTE, 
            g.aol AOL,

            G.TIPO_VIA,
            G.NOM_CALLE,
            G.DUPLICADOR,
            G.NRO_PUERTA,
            G.CGV,
            G.REF_DIR,
            G.BARRIO,

            g.tarifa TARIFA,
            G.estado_sum ESTADOSUM, 
            g.medidor MEDIDOR, 
            G.DEUDA_TERCEROS,
            G.DEUDA_ENERGIA,
            g.importe_acordado IMPORTE_ACO, 
            g.fact_venc FV,
            g.fact_aco FA, 
            g.fecha_programacion, 
            t.te_nombres GESTOR, ITIN
            from gestion_cobro_entrega g
            left join tecnicos t on t.te_codigo = g.tecnico

            where
            g.tecnico = '".$agente."'  and
            g.fecha_programacion = '".$fecha."'
            and g.acta is null
            order by g.tecnico
";
}




$result = ibase_query($conexion, $sql);

$datos = array();

while($fila = ibase_fetch_row($result)){
    $datos[] = $fila;
  }

 
json_encode($datos);

$contQuery = count($datos);


//==============================================================
//==============================================================
//==============================================================


$pdf=new mPDF('c','Legal','0', '5', '5', '5', '5', '5'); 



$pdf->AddPage('L');


$pdf->writeHTML($cabecera);


$cabeceraTabla = <<<EOD

<style type="text/css">
    table tr td {
        text-align: center; font-size: 10px;
        height:30px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        
    }
    table tr {
     
        height: 100px;
    }
</style>

<table border="1" width="100%" style="border-collapse: collapse;">
	<tbody>
		<tr style="text-align: center;  ">
            <td style="width:3.5%"> Orden </td>
            <td style="width:3.5%"> Nic </td>
            <td style="width:5%"  > Correg </td>
            <td style="width:10%" > Cliente AOL </td>
            <td style="width:18%" > Direccion </td>
            <td style="width:2%"  > TAR </td>
            <td style="width:2%"  > ES </td>
            <td style="width:4%"  > Medidor </td>
            <td style="width:4%"  > Deuda Total </td>
            <td style="width:4%"  > Importe Acorda </td>
            <td style="width:5%"  > Deuda Energi </td>
            <td style="width:1.5%"> F.V </td>
            <td style="width:1.5%"> F.A </td>
            <td style="width:4%"  > Cedula </td>
            <td style="width:4%"  > Contacto </td>
            <td style="width:2%"  > TG </td>
            <td style="width:4%"  > Telefono </td>
            <td style="width:2%"  > COD </td>
            <td style="width:3%"  > Recaudo </td>
            <td style="width:2%"  > P/A </td>
            <td style="width:2%"  > Hora </td>
            <td style="width:7%"  > Observacion </td>
            <td style="width:5%"  > Lecura/Firma </td>
    
		</tr>
		
EOD;



$final = "</tbody> </table>";



$cantidadFilas =0;

$limitePorPagina = 25;

for ($i=0; $i < $contQuery ; $i++) {

    $cantidadFilas++;
    
	if ( $i == $contQuery){

        $pdf->writeHTML($final);
        $final = mb_convert_encoding($final, 'UTF-8', 'UTF-8');

	}else{

        $orden = utf8_encode($datos[$i][0]);
        $NIC = utf8_encode($datos[$i][1]);
        $corregimiento = utf8_encode($datos[$i][2]);
        $municipio = utf8_encode($datos[$i][3]);
        $municipioNext = utf8_encode($datos[$i+1][3]);
        $barrio = utf8_encode($datos[$i][5]);
        $cAOL = $barrio." ". utf8_encode($datos[$i][6]);
        $direcion = utf8_encode($datos[$i][7]) . " " . utf8_encode($datos[$i][8]) . " " . utf8_encode($datos[$i][9]) . " " . utf8_encode($datos[$i][10]) . " " . utf8_encode($datos[$i][11]) . " " . utf8_encode($datos[$i][12]) . " " . utf8_encode($datos[$i][13]) ;
        $tar = utf8_encode($datos[$i][14]);
        $es = utf8_encode($datos[$i][15]);
        $medidor = utf8_encode($datos[$i][16]);
        $deudaEnergia = utf8_encode($datos[$i][18]);
        $deudaTotal = utf8_encode($datos[$i][17]) + $deudaEnergia;
        $importe = utf8_encode($datos[$i][19]);
        $fv = utf8_encode($datos[$i][20]);
        $fa = utf8_encode($datos[$i][21]);
        $fecha = utf8_encode($datos[$i][22]);
        $gestor = utf8_encode($datos[$i][23]);
        $gestorNext = utf8_encode($datos[$i+1][23]);
     
        if(strcmp($es, 'SITUACION CORRECTA') === 0){
            $es = "COR";
        }else{
            $es = "SUSP";
        }
        
        $cabecera = <<<EOD

            <style type="text/css">
                table tr td {
                    text-align: center; font-size: 10px;
                    height:33px;
                    overflow: hidden;
                    white-space: nowrap;
                    text-overflow: ellipsis;                   
                }
            </style>


            <table border="0" width="50%">
                <tbody>
                    <tr>
                        <td ><strong>FECHA PROGRAMACION</strong></td>
                        <td > $fecha - $municipio</td>
                        <td ><strong>AGENTE PROGRAMADO</strong></td>
                        <td > $gestor </td>   
                        <td >  </td>                       
                    </tr>
                </tbody>
            </table>




EOD;
    // if ($i==0){
    //         $html = $cabecera;
    //         $pdf->writeHTML( $html);
    //         $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
    //     }
        
        

		$row = <<<EOD
			<tr >
				<td > $orden </td>
                <td > $NIC </td>
                <td > $corregimiento </td>
				<td > $cAOL </td>
                <td > $direcion </td>
                <td > $tar</td>
                <td > $es</td>
                <td > $medidor</td>
                <td > $deudaTotal</td>
                <td > $importe</td>                
                <td > $deudaEnergia</td>
                <td > $fv</td>
                <td > $fa</td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
                <td > </td>
				<td > </td>
				
				
			</tr>	
EOD;

		$rowsAux .= $row;

	}

	if( $cantidadFilas == $limitePorPagina || $i+1 == $contQuery || ($gestor != $gestorNext) || ($municipio != $municipioNext)){



        $html = $cabecera. $cabeceraTabla. $rowsAux . $final;
        $pdf->writeHTML( $html);
        $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
		$arrayAux = "";
        $rowsAux = "";

            if($cantidadFilas == $limitePorPagina){
                $cantidadFilas = 0;
            }

            if( !($i+1 == $contQuery)){
                $pdf->AddPage('L'); 
            }
		
			
	}


	

	
	}
	
	





// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.


//============================================================+
// END OF FILE
//============================================================+








$pdf->Output('reporte.pdf', 'D');
exit;

//==============================================================
//==============================================================
//==============================================================


?>