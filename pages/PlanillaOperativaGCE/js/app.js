// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function($) {
    $(".selectPlaca").select2().change(function(e) {
        var placa = $(this).val();
        app.onChangePlaca(placa);
    });;
});




// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        idPlaca: "",
        fecha: "",
        selectPlacas: [],
        btnCancelar: true
    },


    methods: {


        loadSelectPlaca: function() {
            var app = this;
            $.get('./request/getSelectPlacas.php', function(data) {
                app.selectPlacas = JSON.parse(data);
            });
        },

        loadFecha: function() {
            var today = new Date();
            this.fecha = (getFormattedDate(today));
            // Get date formatted as YYYY-MM-DD
            function getFormattedDate(date) {
                return date.getFullYear() +
                    "-" +
                    ("0" + (date.getMonth() + 1)).slice(-2) +
                    "-" +
                    ("0" + date.getDate()).slice(-2);
            }

        },

        onChangePlaca: function(PLACA) {
            this.idPlaca = PLACA;
        },

        exportarF: function() {



            alertify.confirm("Exportar", "Desea exportar este item? <br> Al no seleccionar un tecnico el proceso puede tardar varios minutos. <br> Por favor espere",
                function() {

                    console.log(app.idPlaca + "-" + app.fecha)

                    var dir = "./reporte/mpdf-master/examples/example01_basic.php";


                    var form = "<form action='" + dir + "' method='post' target='_blank' class='_form'>" +
                        "<input type='hidden' name='placa' value='" + app.idPlaca + "' />" +
                        "<input type='hidden' name='fecha' value='" + app.fecha + "' />" +
                        "</form>";
                    $('body').append(form);
                    $('._form').submit();

                },
                function() {
                    // alertify.error('Cancel');
                });



        },

        exportar: function() {



            var formData = new FormData($('.formExportar')[0]);



            $.ajax({
                url: './request/getRows.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function() {},
                success: function(data) {

                    console.error(data)

                    if (data == 0) {

                        alertify.warning("No hay registros en esta fecha");

                    } else {
                        console.log(data)
                        app.exportarF();
                    }

                },
                //si ha ocurrido un error
                error: function(FormData) {
                    console.log(data)
                }
            });




            // $('._form').remove();





        },






    },



    watch: {

    },

    mounted() {

        this.loadSelectPlaca();

        this.loadFecha();




    },

});