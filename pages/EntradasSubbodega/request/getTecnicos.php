<?php
    session_start();

    if (!$_SESSION['user']) {
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

    include("../../../init/gestion.php");
    // include("gestion.php");


    $ID = $_POST['tecnico'];

    $ID = strtoupper($ID);

    if ($ID == "") {

        $sql = "select te_codigo, 
                    te_nombres, 
                    te_direccion, 
                    te_telefono, 
                    te_ciudad, 
                    te_usu_sistema, 
                    te_cuadrilla, 
                    te_cedula, 
                    TE_TIPO
            from tecnicos
            where te_tipo = 'B'
            order by te_tipo desc 
            
            ";

    } else {

        $sql = "select  
                    te_codigo, 
                    te_nombres, 
                    te_direccion, 
                    te_telefono, 
                    te_ciudad, 
                    te_usu_sistema, 
                    te_cuadrilla, 
                    te_cedula, 
                    TE_TIPO
            from tecnicos
            where 
            te_tipo = 'B' AND
            (te_nombres LIKE '%" . $ID . "%' OR  te_codigo LIKE '%" . $ID . "%')
            order by te_tipo
            ";

    }


    $return_arr = array();

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $row_array['codigo'] = utf8_encode($fila[0]);
        $row_array['nombres'] = utf8_encode($fila[1]);
        $row_array['direccion'] = utf8_encode($fila[2]);
        $row_array['telefono'] = utf8_encode($fila[3]);
        $row_array['ciudad'] = utf8_encode($fila[4]);
        $row_array['usuSistema'] = utf8_encode($fila[5]);
        $row_array['cuadrilla'] = utf8_encode($fila[6]);
        $row_array['cedula'] = utf8_encode($fila[7]);
        $row_array['tipo'] = utf8_encode($fila[8]);

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);

?>
