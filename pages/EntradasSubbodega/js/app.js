// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });
// $('#fecha').val(app.fechaEntrada);

    $('.noSigno').keydown(function (e) {


        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });

});


let app = new Vue({
    el: '#app',
    data: {


        empresa: [],

        tablaBodegas: [],
        buscarBodega: [],
        idBodega: "",
        bodega: "",

        buscarProveedor: "",
        idProveedor: "",
        proveedor: "",
        tablaProveedores: [],

        buscarMaterial: "",
        idMaterial: "",
        material: "",
        hasSerie: false,
        tablaMateriales: [],

        buscarPropiedad: "",
        idPropiedad: "",
        propiedad: "",
        tablaPropiedades: [],

        buscarMarca: "",
        idMarca: "",
        marca: "",
        tablaMarcas: [],

        buscarTipoMaterial: "",
        idTipoMaterial: "",
        tipoMaterial: "",
        tablaTipoMateriales: [],

        fechaEntrada: "",
        grupo: '',
        cantidad: "",
        vlrUnitario: "",
        serie: "",
        serieManual: "",
        nDoc: "",
        concepto: "",


        picked: "Manual",

        nactivo: "",

        prefijo: "",
        inicioSerie: "",
        finSerie: "",
        sufijo: "",

        cod_grupo: "",
        paquete: "",


        btnAnadir: false,
        idPlaca: "",
        loadingSerie: true,
        loadingGuadar: true,

        fechaFactura: "",
        danger: true,

        detalle: {
            codigo: "",
            descripcion: "",
            cantidad: "",
            vlrUnitario: "",
            valor: "",
            serie: "",
            arraySeries: []
        },

        tablaDetalle: [],
        tablaSeries: [],
        serieExiste: [],


    },


    methods: {

        getProveedores: function () {
            var app = this;
            $.post('./request/getProveedores.php', {PROVEEDOR: app.buscarProveedor}, function (data) {
                var data = JSON.parse(data);
                app.tablaProveedores = data;
            });
        },

        getBodegas: function () {
            var app = this;
            $.post('./request/getTecnicos.php', {tecnico: app.buscarBodega}, function (data) {
                var data = JSON.parse(data);
                app.tablaBodegas = data;
            });
        },

        getEmpresa: function () {
            var app = this;

            $.get('./request/getEmpresa.php', function (data) {
                var data = JSON.parse(data);
                app.empresa.push(data[0]);
                console.log(app.empresa[0].NOMBRE)
            });

        },

        seleccionarProveedor: function (data) {
            this.idProveedor = data.CODIGO;
            this.proveedor = data.DESCRIPCION;
            $('#modalProveedor').modal('hide');
            this.buscarProveedor = "";
            // this.tablaProveedores="";
        },

        seleccionarBodega: function (data) {
            this.idBodega = data.codigo;
            this.bodega = data.nombres;
            $('#modalBodega').modal('hide');
            this.buscarBodega = "";
        },

        getMateriales: function () {
            var app = this;
            // if (app.buscarMaterial.length>0) {
            $.post('./request/getMateriales.php', {material: app.buscarMaterial}, function (data) {
                // console.log(data);
                var data = JSON.parse(data);
                app.tablaMateriales = data;
            });
            // }
        },

        seleccionarMaterial: function (data) {
            this.idMaterial = data.CODIGO;
            this.material = data.DESCRIPCION;
            this.grupo = data.GRUPO;
            // this.hasSerie = (data.SERIE=="S") ? true : false;
            this.serie = data.SERIE;
            this.cod_grupo = data.COD_GRUPO;

            $('#modalMateriales').modal('hide');
            this.buscarMaterial = "";
            // this.tablaMateriales="";

            this.getMarcas();
        },

        getPropiedades: function () {
            var app = this;
            $.post('./request/getPropiedades.php', {propiedad: app.buscarPropiedad}, function (data) {
                // console.log(data);
                var data = JSON.parse(data);
                app.tablaPropiedades = data;
            });
        },

        seleccionarPropiedad: function (data) {
            this.idPropiedad = data.CODIGO;
            this.propiedad = data.DESCRIPCION;
            $('#modalPropiedades').modal('hide');
            this.buscarPropiedad = "";
            // this.tablaPropiedades="";
        },

        getMarcas: function () {
            var app = this;
            $.post('./request/getMarcas.php', {marca: app.buscarMarca, grupo: app.cod_grupo}, function (data) {
                // console.log(data);
                var data = JSON.parse(data);
                app.tablaMarcas = data;
            });
        },

        seleccionarMarca: function (data) {
            this.idMarca = data.CODIGO;
            this.marca = data.DESCRIPCION;
            $('#modalMarcas').modal('hide');
            this.buscarMarca = "";
            // this.tablaMarcas="";
        },

        getTipoMateriales: function () {

            var app = this;

            $.post('./request/getTipoMateriales.php', {tipoMaterial: app.buscarTipoMaterial}, function (data) {
                // console.log(data);
                try {
                    var data = JSON.parse(data);
                    app.tablaTipoMateriales = data;
                } catch (e) {

                }
            });

        },

        seleccionarTipoMaterial: function (data) {
            this.idTipoMaterial = data.CODIGO;
            this.tipoMaterial = data.DESCRIPCION;

            $('#modalTipoMateriales').modal('hide');
            this.buscarTipoMaterial = "";
            // this.tablaTipoMateriales="";
        },

        fechaActual: function () {

            var dt = new Date();
            var day = dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate();
            var month = (dt.getMonth() + 1) < 10 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1);
            var year = dt.getFullYear();
            this.fechaEntrada = year + '-' + month + '-' + day;
        },

        addItem: function () {

            var app = this;

            if (!(app.idProveedor == "") || app.validaForm('#form_encabezado') || app.concepto || app.nDoc) {

                if (this.serie != 'S') {

                    if (this.validaForm('#formMateriales')) {

                        this.tablaDetalle.push({
                            'cantidad': this.cantidad,
                            'codigo': this.idMaterial,
                            'cod_grupo': this.cod_grupo,
                            'concepto': this.concepto.toUpperCase(),
                            'descripcion': this.material,
                            'fecha': this.fechaEntrada,
                            'idProveedor': this.idProveedor,
                            'nDoc': this.nDoc,
                            'proveedor': this.proveedor,
                            'serie': this.serie,
                            'valor': (this.vlrUnitario * this.cantidad),
                            'vlrUnitario': this.vlrUnitario,
                            'arraySeries': []
                        });

                        this.detalle = [];
                        this.idMaterial = "";
                        this.material = "";
                        this.cantidad = "";
                        this.vlrUnitario = "";
                    } else {

                        alertify.warning('Seleccione un material')

                    }
                } else {


                    if (this.validaForm('#formMateriales')) {
                        this.hasSerie = true;
                    }
                }

            } else {
                alertify.error("Seleccione un proveedor");
            }
        },

        deleteItemDetalle: function (index) {
            var app = this;
            // console.log(index);
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaDetalle.splice(index, 1);
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        addRangoSerie: function () {
            var app = this;

            var formValid = false;

            if (app.picked == 'Manual') {

                app.marca == "" || app.serieManual == "" ||
                app.nactivo == "" ? formValid = false : formValid = true;

            } else {

                console.log(app.inicioSerie)
                console.log(app.sufijo)
                console.log(app.finSerie)

                app.idPropiedad == "" || app.paquete == "" ||
                app.idMarca == "" || app.idTipoMaterial == "" ||
                app.inicioSerie == "" ||
                app.finSerie == "" ? formValid = false : formValid = true;


            }


            if (formValid) {

                $('#form_serie').removeClass('was-validated');

                app.loadingSerie = false;

                var datos = {
                    prefijo: app.prefijo,
                    inicioSerie: app.inicioSerie,
                    finSerie: app.finSerie,
                    sufijo: app.sufijo,
                    cod_grupo: app.cod_grupo,
                    marca: app.idMarca,
                    material: app.idMaterial
                };
                app.picked === "Auto" ? app.addSerieAuto(datos) : app.addSerieManual(app.serieManual);
            } else {

                $('#form_serie').addClass('was-validated');

                alertify.error('Campos vacios')


            }

        },

        addSerieAuto: function (array) {
            var app = this;

            $.ajax({
                url: './request/getSeries.php',
                type: 'POST',
                // dataType: 'json',
                data: {data: array}
            })
                .done(function (data) {
                    console.log(data);
                    var datos = JSON.parse(data);
                    app.serieExiste = datos;
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    // console.log("complete");
                    app.generaSerie();
                    app.loadingSerie = true;
                });
        },

        addSerieManual: function (serie) {

            var app = this;

            $.ajax({
                url: './request/getSeriesManual.php',
                type: 'POST',
                // dataType: 'json',
                data: {data: serie},
            })
                .done(function (data) {
                    var datos = JSON.parse(data);
                    // console.log(data);
                    app.serieExiste = datos;
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    if (app.serieExiste.length > 0) {
                        alertify.error('Esta serie ya se encuentra registrada');
                    } else {
                        app.tablaSeries.push({
                            'marca': app.marca,
                            'serie': serie,
                            'grupo': app.grupo,
                            'cod_grupo': app.cod_grupo,
                            'cod_marca': app.idMarca,
                            'idTipoMaterial': app.idTipoMaterial,
                            'paquete': app.paquete,
                            'material': app.material,
                            'nactivo': app.nactivo,
                            'idPropiedad': app.idPropiedad,
                            'propiedad': app.propiedad
                        });

                    }
                    app.loadingSerie = true;
                });
        },

        generaSerie: function () {
            var app = this;

            if (app.serieExiste.length > 0) {
                alertify.alert("Existen series", "Hemos encontrado series en la BD!...");

                if (app.tablaSeries.length > 0) {

                    // alertify.alert('Existen Series', 'Existen series generadas en el rango');
                    var doubles = [];
                    for (var i = 0; i < app.tablaSeries.length; i++) {
                        doubles.push(app.tablaSeries[i].serie);
                    }

                    var arrSeriesExiste = [];
                    for (var i = 0; i < app.serieExiste.length; i++) {
                        arrSeriesExiste.push(app.serieExiste[i].SERIE);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        // if (app.finSerie.toString().length>app.inicioSerie.toString().length) {
                        // 	var n = app.finSerie.toString().length - i.toString().length;
                        // 	for (var j = 0; j < n ; j++) {
                        // 		ceros+='0';
                        // 	}
                        // }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!arrSeriesExiste.includes(serieHP) && !doubles.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }
                } else {

                    var arrSeriesExiste = [];
                    for (var i = 0; i < app.serieExiste.length; i++) {
                        arrSeriesExiste.push(app.serieExiste[i].SERIE);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        // if (app.finSerie.toString().length>app.inicioSerie.toString().length) {
                        // 	var n = app.finSerie.toString().length - i.toString().length;
                        // 	for (var j = 0; j < n ; j++) {
                        // 		ceros+='0';
                        // 	}
                        // }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!arrSeriesExiste.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }
                }

            } else {
                if (app.tablaSeries.length > 0) {

                    // alertify.alert('Existen Series', 'Existen series generadas en el rango');
                    var doubles = [];
                    for (var i = 0; i < app.tablaSeries.length; i++) {
                        doubles.push(app.tablaSeries[i].serie);
                    }

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        // if (app.finSerie.toString().length>app.inicioSerie.toString().length) {
                        // 	var n = app.finSerie.toString().length - i.toString().length;
                        // 	for (var j = 0; j < n ; j++) {
                        // 		ceros+='0';
                        // 	}
                        // }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        if (!doubles.includes(serieHP)) {

                            app.tablaSeries.push({
                                'marca': app.marca,
                                'serie': serieHP,
                                'grupo': app.grupo,
                                'cod_grupo': app.cod_grupo,
                                'cod_marca': app.idMarca,
                                'idTipoMaterial': app.idTipoMaterial,
                                'material': app.material,
                                'paquete': app.paquete,
                                'material': app.material,
                                'nactivo': app.nactivo,
                                'idPropiedad': app.idPropiedad,
                                'propiedad': app.propiedad
                            });
                        }
                    }


                } else {

                    for (var i = app.inicioSerie; i < (app.finSerie + app.inicioSerie); i++) {
                        var ceros = '';
                        // if (app.finSerie.toString().length>app.inicioSerie.toString().length) {
                        // 	var n = app.finSerie.toString().length - i.toString().length;
                        // 	for (var j = 0; j < n ; j++) {
                        // 		ceros+='0';
                        // 	}
                        // }

                        var serieHP = app.prefijo.toUpperCase() + ceros + i + app.sufijo.toUpperCase();
                        // if(!arrSeriesExiste.includes(serieHP)){

                        app.tablaSeries.push({
                            'marca': app.marca,
                            'serie': serieHP,
                            'grupo': app.grupo,
                            'cod_grupo': app.cod_grupo,
                            'cod_marca': app.idMarca,
                            'idTipoMaterial': app.idTipoMaterial,
                            'material': app.material,
                            'paquete': app.paquete,
                            'material': app.material,
                            'nactivo': app.nactivo,
                            'idPropiedad': app.idPropiedad,
                            'propiedad': app.propiedad
                        });
                        // }
                    }
                }


            }
        },

        quitarSerie: function (index) {
            var app = this;
            // console.log(index);
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    app.tablaSeries.splice(index, 1);
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        eliminarRangoSerie: function () {
            var app = this;
            // console.log(index);
            alertify.confirm("Eliminar", ".. Desea eliminar la serie creada?",
                function () {
                    $('#form_serie').removeClass('was-validated');
                    app.serie = "N";
                    app.clearDatosSerie();
                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        clearDatosSerie: function () {
            this.tablaSeries = [];
            this.serieExiste = [];
            this.hasSerie = false;
            this.idMaterial = "",
                this.material = "",
                this.idPropiedad = "",
                this.propiedad = "",
                this.idMarca = "",
                this.marca = "",
                this.idTipoMaterial = "",
                this.tipoMaterial = "",
                this.prefijo = "",
                this.inicioSerie = "",
                this.finSerie = "",
                this.sufijo = "",
                this.paquete = "",
                this.cantidad = "",
                this.vlrUnitario = ""
        },

        cancelarSerie: function () {
            this.tablaSeries = [];
            this.serieExiste = [];
            this.hasSerie = false;
        },

        addRangoSerieDetalle: function () {

            var app = this;


            alertify.confirm("Agregar serie", "... Desea agregar esta serie al detalle ?",

                function () {

                    app.tablaDetalle.push({
                        'cantidad': app.tablaSeries.length,
                        'codigo': app.idMaterial,
                        'cod_grupo': app.cod_grupo,
                        'concepto': app.concepto.toUpperCase(),
                        'descripcion': app.material,
                        'fecha': app.fechaEntrada,
                        'idProveedor': app.idProveedor,
                        'nDoc': app.nDoc,
                        'proveedor': app.proveedor,
                        'serie': app.serie,
                        'valor': (app.vlrUnitario * app.tablaSeries.length),
                        'vlrUnitario': app.vlrUnitario,
                        'arraySeries': app.tablaSeries
                    });

                    app.detalle = [];
                    app.idMaterial = "";
                    app.material = "";
                    app.cantidad = "";
                    app.vlrUnitario = "";
                    $('#formMateriales').removeClass('was-validated');

                    app.clearDatosSerie();
                },
                function () {
                    // alertify.error('Cancel');
                });


        },

        insertProductos: function () {
            var app = this;


            if (app.validaForm('#form_encabezado') && app.tablaDetalle.length > 0) {

                app.loadingGuadar = false;

                var datos = JSON.stringify(app.tablaDetalle);


                $.ajax({
                    url: './request/entradaBodega.php',
                    type: 'POST',
                    data: {
                        data: datos

                    },
                }).done(function (data) {

                    alertify.success('Ingresando registros... espere.');

                    var datosEntrega = {
                        codTecnico: app.idBodega,
                        nomTecnico: app.bodega,
                        //cuadrilla: app.cuadrilla,
                        numeroEntrega: app.nDoc,
                        fechaEntrega: app.fechaEntrega,
                        //codSupervisor: this.codSupervisor,
                        //supervisor: this.supervisor,
                        //tecnicos: this.tecnicos,
                        concepto: app.concepto.toUpperCase(),
                        detalle: app.tablaDetalle,
                        nombEmpresa: app.empresa[0].NOMBRE,
                        nitEmpresa: app.empresa[0].NIT,
                        dirEmpresa: app.empresa[0].DIRECCION,
                    };


                    var datosEntrega = JSON.stringify(datosEntrega);

                    $.ajax({
                        url: './request/entregaMateriales.php',
                        type: 'POST',
                        data: {data: datosEntrega},
                    }).done(function (data) {


                        var server = JSON.parse(data);
                        var server = JSON.stringify(server);
                        console.log(server)


                        var obj = JSON.parse(data);
                        if (obj.response === 'success') {
                            app.loadingGuadar = true;
                            alertify.confirm("Proceso Exitoso", "... Desea Imprimir el reporte de entrega N° " + obj.numero + "?",
                                function () {
                                    var dir = './reports/reporte_entrega_materiales.php';
                                    var response = {server: server, datos: datosEntrega};
                                    app.callReport(dir, response);
                                    alertify.alert("Reporte", "El reporte abrira una nueva pestaña", function () {
                                        location.reload();
                                    });
                                },

                                function () {
                                    //location.reload();
                                });
                        }


                    }).fail(function () {
                        console.log("error");
                    }).always(function (data) {


                    });


                }).fail(function () {
                    console.log("error");
                }).always(function (data) {
                    var obj = JSON.parse(data);
                    // console.log(obj.response);
                    var server = JSON.stringify(obj);


                });

            } else {

                if (app.tablaDetalle == 0) {
                    alertify.warning('Agregue un producto')
                }

                if (app.idProveedor == "") {
                    alertify.warning('Seleccione un proveedor')
                }


            }

        }
        ,

        callReport: function (dir, data) {


            var form = "<form action='" + dir + "' method='post' target='_blank' class='_form'>" +
                "<input type='hidden' name='server' value='" + data.server + "' />" +
                "<input type='hidden' name='datos' value='" + data.datos + "' />" +
                "<input type='hidden' name='empresa' value='" + data.empresa + "' />" +
                "<input type='hidden' name='nit' value='" + data.nit + "' />" +
                "<input type='hidden' name='direccion' value='" + data.direccion + "' />" +
                "</form>";
            $('body').append(form);
            $('._form').submit();
            // $('._form').remove();

        },

        validaForm: function (idForm) {
            var cont = 0;
            $('' + idForm + ' input:required').each(function (index, val) {
                if ($(this).val() === "") {
                    cont++;
                }
            });

            if (cont === 0) {

                $(idForm).removeClass('was-validated');
                return true;

            } else {
                $(idForm).addClass('was-validated');
                alertify.error('Error: Campos obligatorios vacios');
                return false;
            }
        }


    },
    watch: {
        fechaEntrada: function () {
            var f = new Date();
            var f_actual = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
            var fechaAux = this.fechaEntrada.split("-");
            var fechaFac = new Date(parseInt(fechaAux[0]), parseInt(fechaAux[1] - 1), parseInt(fechaAux[2]));

            f.setHours(0, 0, 0, 0);
            fechaFac.setHours(0, 0, 0, 0);
            if (fechaFac.getTime() > f.getTime()) {
                alertify.error("No puede ingresar una fecha futura");
                this.fechaActual();
            }

        }
    },
    computed: {
        armaSerie:
            {

                get: function () {
                    return this.prefijo + '' + (this.inicioSerie) + '' + this.sufijo;
                },

                set: function (serie) {
                    this.serieManual = serie.toUpperCase();
                }
            }
    },
    created() {

        this.fechaActual();
        this.getProveedores();
        this.getMateriales();
        this.getPropiedades();
        this.getTipoMateriales();
        this.getBodegas();


    },
    mounted() {

        this.getEmpresa();
    },

    updated() {
        $('[data-toggle="tooltip"]').tooltip();

    },


});

