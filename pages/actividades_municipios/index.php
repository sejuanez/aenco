<?php
  session_start();
  
  if(!$_SESSION['user']){
    echo"<script>window.location.href='../inicio/index_.php';</script>";
    exit();
  }  
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Actividades por Municipio</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
		
  <script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  <script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  <script type="text/javascript" src="../../js/accounting.js"></script>
  <style>
    #subheader {
      padding: 43px 10px 0px;
    }
    .flex-table{
      margin: 1rem;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-form-item--mini.el-form-item, .el-form-item--small.el-form-item {
      width: 100%;
    }
    .el-select {
      width: 100%;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .el-table{
      color: #000000;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .95em;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    @media only screen and (max-width: 768px) {
      .el-form--label-top .el-form-item__label{
        float: left;
      }
      .el-input__icon, .el-icon-date{
        margin-top: -1rem;
      }
    }
  </style>
</head>
<body>
  <div id="app">
    <el-form :model="ruleForm" :rules="rules" ref="ruleForm" label-position="top" size="mini">
      <header>
        <h3>Actividades por Municipio</h3>
        <nav>
          <ul id="menu">
            <li v-show="sw">
              <a id="exportar" href="">
                <span class="ion-ios-download-outline"></span><h6>exportar</h6>
                <input type="hidden" name="fechaIni" id="fechaIni">
                <input type="hidden" name="fechaFin" id="fechaFin">
              </a>
            </li>
            <li id="consultar">
              <a @click.stop.prevent="onQuery()" href="">
                <span class="ion-ios-search-strong"></span><h6>consultar</h6>
              </a>
            </li>       
          </ul>
        </nav>    
      </header>

      <div id='subheader'>
        <div id="div-form">
          <el-row :gutter="4">
            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Departamento">
                <el-select @change="municipios" clearable v-model="ruleForm.departamento" placeholder="(TODOS)">
                  <el-option
                    v-for="item in optionsDpto"
                    :key="item.de_codigo"
                    :label="item.de_nombre"
                    :value="item.de_codigo">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>
          
            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Municipio">
                <el-select @change="corregimientos" clearable v-model="ruleForm.municipio" placeholder="(TODOS)">
                  <el-option
                    v-for="item in optionsMunicipio"
                    :key="item.mu_codigomun"
                    :label="item.mu_nombre"
                    :value="item.mu_codigomun">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Corregimiento">
                <el-select @change="barrios" clearable v-model="ruleForm.corregimiento" placeholder="(TODOS)">
                  <el-option
                    v-for="item in optionsCorregimientos"
                    :key="item.cod"
                    :label="item.co_nombre"
                    :value="item.co_codcorregimiento">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Barrio">
                <el-select v-model="ruleForm.barrio" clearable placeholder="(TODOS)">
                  <el-option
                    v-for="item in optionsBarrio"
                    :key="item.cod"
                    :label="item.ba_nombarrio"
                    :value="item.ba_codbarrio">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Fecha inicial">
                <el-date-picker
                  v-model="ruleForm.fecha_inicial"
                  style="width: 100%"
                  format="MM/dd/yyyy"
                  value-format="MM/dd/yyyy"
                  type="date"
                  placeholder="Seleccione fecha">
                </el-date-picker>
              </el-form-item>
            </el-col>

            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              <el-form-item label="Fecha final">
                <el-date-picker
                  v-model="ruleForm.fecha_final"
                  style="width: 100%"
                  format="MM/dd/yyyy"
                  value-format="MM/dd/yyyy"
                  type="date"
                  placeholder="Seleccione fecha">
                </el-date-picker>
              </el-form-item>
            </el-col>
          </el-row>   
        </div>
      </div>
    
      <div class="flex-table" v-loading="loadingCard">
        <el-row>
          <el-col :xs="24" :md="24" :lg="24">
            <el-table
              :data="tableData"
              border
              height="450"
              size="mini"
              style="width: 100%">
              <el-table-column
                type="index"
                header-align="center"
                align="center"
                :index="indexMethod"
                width="60">
              </el-table-column>
              <el-table-column
                prop="acta"
                label="Acta"
                align="center"
                width="80">
              </el-table-column>
              <el-table-column
                prop="fecha_ejecucion"
                label="Fecha ejecuci�n"
                align="center"
                width="130">
              </el-table-column>
              <el-table-column
                prop="serie"
                label="Serie"
                align="center"
                width="90">
              </el-table-column>
              <el-table-column
                prop="tipo_luminaria"
                label="Tipo luminaria"
                header-align="left"
                align="left"
                width="110">
              </el-table-column>
              <el-table-column
                prop="potencia"
                align="center"
                label="Potencia"
                width="90">
              </el-table-column>
              <el-table-column
                prop="luminaria"
                label="Luminaria"
                header-align="left"
                align="left"
                width="120">
              </el-table-column>
              <el-table-column
                prop="dpto"
                label="Departamento"
                header-align="left"
                align="left"
                width="120">
              </el-table-column>
              <el-table-column
                prop="mpio"
                label="Municipio"
                header-align="left"
                align="left"
                width="120">
              </el-table-column>
              <el-table-column
                prop="corregimiento"
                label="Corregimiento"
                header-align="left"
                align="left"
                width="120">
              </el-table-column>
              <el-table-column
                prop="barrio"
                label="Barrio"
                header-align="left"
                align="left"
                width="120">
              </el-table-column>
              <el-table-column
                prop="nro_actividades"
                label="N� Actividades"
                header-align="left"
                align="center"
                width="110">
              </el-table-column>
              <el-table-column
                prop="descripcion"
                label="Descrip_act"
                header-align="left"
                align="left"
                width="700">
              </el-table-column>
            </el-table>
          </el-col>
        </el-row>
      </div>
    </el-form>
  </div>
  <script>
    ELEMENT.locale(ELEMENT.lang.es)
    new Vue({
      el: '#app',
      data: () => ({
        ruleForm: {
          departamento: '',
          municipio: '',
          corregimiento: '',
          barrio: '',
          fecha_inicial: '',
          fecha_final: ''                   
        },
        rules: {
          fecha_inicial: [
            { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          fecha_final: [
            { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
          ]
        },
        tableData: [],
        optionsDpto: [],
        optionsMunicipio: [],
        optionsCorregimientos: [],
        optionsBarrio: [],
        sw: false,
        loadingCard: false
      }),
      methods: {
        onQuery () {
          this.loadingCard = true;
          if (this.ruleForm.fecha_inicial === '' || this.ruleForm.fecha_final === '') {
            this.$alert('Debe seleccionar un rango de fecha para consultar.', 'AVISO..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleForm.fecha_final < this.ruleForm.fecha_inicial) {
            this.$alert('Lo sentimos la fecha final no debe ser menor a la fecha inicial.', 'AVISO..!', {
              confirmButtonText: 'OK'
            });
          } else {
            var data = new FormData();
            data.append('fechaIni', this.ruleForm.fecha_inicial);
            data.append('fechaFin', this.ruleForm.fecha_final);
            data.append('dpto', this.ruleForm.departamento);
            data.append('mpio', this.ruleForm.municipio);
            data.append('cto', this.ruleForm.corregimiento);
            data.append('barrio', this.ruleForm.barrio);
            axios.post('request/getSolicitudes.php', data).then(response => {
              if (response.data.length !== 0) {
                this.tableData = response.data;
                this.sw = true;
                var fechaIni = this.ruleForm.fecha_inicial;
                var fechaFin = this.ruleForm.fecha_final;
                var dpto = this.ruleForm.departamento;
                var mpio = this.ruleForm.municipio;
                var cto = this.ruleForm.corregimiento;
                var barrio = this.ruleForm.barrio;
                // alert(fechaIni)
                $('#fechaIni').val(fechaIni);
                $('#fechaFin').val(fechaFin);
                $("#exportar").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin+"&dpto="+dpto+"&mpio="+mpio+"&cto="+cto+"&barrio="+barrio);
              } else {
                this.tableData = [];
                this.sw = false;
              }
              this.loadingCard = false;
            }).catch(e => {
              console.log(e.response);
              this.loadingCard = false;
            });
          }
        },
        departamentos () {
          axios.get('request/departamentos.php').then(response => {
            this.optionsDpto = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        municipios () {
          var mu_depto = new FormData();
          mu_depto.append('mu_depto', this.ruleForm.departamento);
          axios.post('request/municipios.php', mu_depto).then(response => {
            this.optionsMunicipio = response.data;
          }).catch(e => {
            console.log(e.response);
          });
          this.ruleForm.municipio = '';
        },
        corregimientos () {
          var co_municipio = new FormData();
          co_municipio.append('co_municipio', this.ruleForm.municipio);
          axios.post('request/corregimientos.php', co_municipio).then(response => {
            this.optionsCorregimientos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
          this.ruleForm.corregimiento = '';
        },
        barrios () {
          var ba_sector = new FormData();
          ba_sector.append('ba_sector', this.ruleForm.corregimiento);
          axios.post('request/barrios.php', ba_sector).then(response => {
            this.optionsBarrio = response.data;
          }).catch(e => {
            console.log(e.response);
          });
          this.ruleForm.barrio = '';
        },
        indexMethod (index) {
          return index + 1
        }
      },
      mounted () {
        this.departamentos();
      }
    })
  </script>
  </body>
</html>