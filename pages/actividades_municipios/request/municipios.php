<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $mu_depto = $_POST['mu_depto'];

    $query = "SELECT * FROM municipios  WHERE mu_depto = '$mu_depto' ORDER BY mu_nombre ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['mu_codigomun'] = utf8_encode($row[1]);
		$row_array['mu_nombre'] = utf8_encode($row[2]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>