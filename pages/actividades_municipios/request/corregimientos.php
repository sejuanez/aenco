<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $co_municipio = $_POST['co_municipio'];

    $query = "SELECT co_depto||co_municipio||co_codcorregimiento AS cod, co_codcorregimiento, co_nombre FROM corregimientos WHERE co_municipio = '$co_municipio' ORDER BY co_nombre ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['cod'] = utf8_encode($row[0]);
        $row_array['co_codcorregimiento'] = utf8_encode($row[1]);
		$row_array['co_nombre'] = utf8_encode($row[2]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>