<?php
	// session_start();
 //  	if(!$_SESSION['user']){
 //        echo
 //        "<script>window.location.href='../../inicio/index_.php';</script>";
 //        exit();
 //    }

	include("../../../init/gestion.php");
	

	$fechaIni = $_POST["fechaIni"];
	$fechaFin = $_POST["fechaFin"];
	$dpto     = $_POST["dpto"];
	$mpio     = $_POST["mpio"];
	$cto      = $_POST["cto"];
	$barrio   = $_POST["barrio"];

	$param = "";

	if (!empty($fechaIni) && !empty($fechaFin)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio) && !empty($cto)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' AND dla.dla_corregimiento = '$cto' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio) && !empty($cto) && !empty($barrio)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' AND dla.dla_corregimiento = '$cto' AND dla.dla_barrio = '$barrio' ";
	}

	$query = "select lc.ca_acta acta,
		      lc.ca_fechaej fecha_ejecucion,
		      dla.dla_serielum serie,
		      dla.dla_tipo_lampara tipo_luminaria,
		      dla.dla_potencia potencia,
		      dla.dla_luminaria luminaria,
		      d.de_nombre dpto,
		      m.mu_nombre mpio,
		      co.co_nombre corregimiento,
		      b.ba_nombarrio barrio,
		      (count(dm.ma_acta) + COUNT(da.ac_acta)) nro_actividades, 
                       cast ((
                        select  coalesce(list(dmc.ma_desmater),'')||','|| coalesce(list(distinct dad.ac_nombre),'')
                         from dato_material dmc
                          left join dato_adecuacenso dad on dad.ac_acta=lc.ca_acta
                       where dmc.ma_acta=lc.ca_acta and  dmc.ma_canmater>0
                      )  as varchar(200))  descripcion
			  from lega_cabecera lc
 			  left join ot_ap ot on ot.oa_numero = lc.ca_orden
			  left join dato_material dm on dm.ma_acta = lc.ca_acta AND dm.ma_canmater > 0
 			  left join dato_lev_alumbrado dla on dla.dla_acta = lc.ca_acta
 			  left join departamentos d on d.de_codigo = lc.ca_depto
 			  left join municipios m on m.mu_depto = lc.ca_depto and m.mu_codigomun = lc.ca_municipio
 			  left join corregimientos co on co.co_depto = lc.ca_depto and co.co_municipio = lc.ca_municipio
              and co.co_codcorregimiento = ot.oa_corregimiento
 			  left join barrios b on b.ba_depto = lc.ca_depto and b.ba_mpio = lc.ca_municipio and b.ba_sector = ot.oa_corregimiento
			  and b.ba_codbarrio = ot.oa_barrio
			  left join dato_adecuacenso da on da.ac_acta = lc.ca_acta
              $param

group by lc.ca_acta,
         lc.ca_fechaej,
       dla.dla_serielum,
       dla.dla_tipo_lampara,
       dla.dla_potencia,
       dla.dla_luminaria,
       d.de_nombre,
       m.mu_nombre,
       co.co_nombre,
       b.ba_nombarrio";
	
	$return_arr = array();

	$data = ibase_query($conexion, $query);

	// echo "<pre>";
	// 	print_r ($data);
	// echo "</pre>";

	while($fila = ibase_fetch_row($data)) {
		$row_array['acta']            = utf8_encode($fila[0]);
		$row_array['fecha_ejecucion'] = utf8_encode($fila[1]);
		$row_array['serie']           = utf8_encode($fila[2]);
		$row_array['tipo_luminaria']  = utf8_encode($fila[3]);
		$row_array['potencia']        = utf8_encode($fila[4]);
		$row_array['luminaria']       = utf8_encode($fila[5]);
		$row_array['dpto']            = utf8_encode($fila[6]);
		$row_array['mpio']            = utf8_encode($fila[7]);
		$row_array['corregimiento']   = utf8_encode($fila[8]);
		$row_array['barrio']          = utf8_encode($fila[9]);
		$row_array['nro_actividades'] = utf8_encode($fila[10]);
                $row_array['descripcion']     = utf8_encode($fila[11]);

		array_push($return_arr, $row_array);
	}
	echo json_encode($return_arr);
?>