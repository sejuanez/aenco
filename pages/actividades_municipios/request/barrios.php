<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $ba_sector = $_POST['ba_sector'];

    $query = "SELECT ba_depto||ba_mpio||ba_sector||ba_codbarrio AS cod, ba_codbarrio, ba_nombarrio FROM barrios WHERE ba_sector = '$ba_sector' ORDER BY ba_nombarrio ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['cod'] = utf8_encode($row[0]);
        $row_array['ba_codbarrio'] = utf8_encode($row[1]);
		$row_array['ba_nombarrio'] = utf8_encode($row[2]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>