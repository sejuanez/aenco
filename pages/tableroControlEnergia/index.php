<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Tablero de control energía</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico-tecnico").width();
					var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-campana").width();
					alto = +$("#div-grafico-campana").height();
					$("#grafico-campana").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-anomalias").width();
					alto = +$("#div-grafico-anomalias").height();
					$("#grafico-anomalias").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-items").width();
					alto = +$("#div-grafico-items").height();
					$("#grafico-items").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-zonas").width();
					alto = +$("#div-grafico-zonas").height();
					$("#grafico-zonas").highcharts().setSize(ancho,alto, false);
				}
			});
			

			$(window).resize(function() {
			    	var ancho = +$("#div-grafico-tecnico").width();
			    	var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-campana").width();
					alto = +$("#div-grafico-campana").height();
					$("#grafico-campana").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-anomalias").width();
					alto = +$("#div-grafico-anomalias").height();
					$("#grafico-anomalias").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-items").width();
					alto = +$("#div-grafico-items").height();
					$("#grafico-items").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-zonas").width();
					alto = +$("#div-grafico-zonas").height();
					$("#grafico-zonas").highcharts().setSize(ancho,alto, false);
			});
		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Cantidad'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            valueSuffix: ' ordenes'
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};






			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();

			$("#div-total").hide();

			/*$("#div-asignadas").hide();
			$("#div-ejecutadas").hide();
			$("#div-pendientes").hide();*/




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				$("#div-total").hide();
				
				/*$("#div-asignadas").fadeOut(500);
				$("#div-ejecutadas").fadeOut(500);
				$("#div-pendientes").fadeOut(500);*/

				//$("#div-tabla").html("<h4 style='text-align:center; color:#999; font-weight:normal'>Cargando...</h4>");


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;


		        var doneAjax1=doneAjax2=doneAjax3=doneAjax4=doneAjax5=doneAjax6=doneAjax7=false;

		        // ajax GENERAL

				$.ajax({
					url:'request/general.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

											
						$("#total").html(repuesta.length);		
						$("#div-total").show();					
						
						var filas="";

						for (var i=0; i<repuesta.length; i++) {
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].acta+"</td><td>"+repuesta[i].fecha+"</td>"+"<td>"+repuesta[i].hora+"</td><td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].trafo+"</td><td>"+repuesta[i].cuenta+"</td><td>"+repuesta[i].cliente+"</td><td>"+repuesta[i].direccion+"</td><td>"+repuesta[i].anomalia+"</td><td>"+repuesta[i].zona+"</td><td>"+repuesta[i].irregularidades+"</td><td>"+repuesta[i].items+"</td><td>"+repuesta[i].sellos+"</td><td>"+repuesta[i].medInstalado+"</td><td>"+repuesta[i].medEncontrado+"</td><td>"+repuesta[i].obs+"</td><td>"+repuesta[i].supervisor+"</td><td>"+repuesta[i].censoTotal+"</td><td>"+repuesta[i].consumoEstimado+"</td><td>"+repuesta[i].consumoProm+"</td><td>"+repuesta[i].tipoCliente+"</td><td>"+repuesta[i].cantItem+"</td></tr>";
						}
						
						$("#tabla-general tbody").html(filas);


						

						$("#exportar a").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin);
						$("#exportar").show();


						
					}
					else{												
						$("#total").html("");		
						$("#div-total").hide();	
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");								
					}

					doneAjax1=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}

					
				});






				//ajax POR TECNICOS
				$.ajax({
					url:'request/porTecnico.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var tecnicos=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							tecnicos[i]=repuesta[i].tecnico;
							cantidad[i]=+repuesta[i].cantidad;
							
						//	filas+="<tr class='fila'>"+
                        //     "<td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-tecnico tbody").html(filas);

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-tecnico",tecnicos,series,colors);

						
					}
					else{												
						
						//$("#tabla-tecnico tbody").html("");
						$("#grafico-tecnico").html("");
					}

					doneAjax2=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});
				

				

				//ajax POR MUNICIPIOS
				$.ajax({
					url:'request/porMunicipio.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var municipios=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							municipios[i]=repuesta[i].municipio;
							cantidad[i]=+repuesta[i].cantidad;
							
							//filas+="<tr class='fila'>"+
                            //  "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-municipio",municipios,series,colors);
						

						
					}
					else{												
						
						//$("#tabla-municipio tbody").html("");
						$("#grafico-municipio").html("");
					}

					doneAjax3=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});


				//ajax POR CAMPANA
				$.ajax({
					url:'request/porCampana.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var campana=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							campana[i]=repuesta[i].nombre;
							cantidad[i]=+repuesta[i].cantidad;
							
							//filas+="<tr class='fila'>"+
                            //  "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-campana",campana,series,colors);
						

						
					}
					else{												
						
						//$("#tabla-municipio tbody").html("");
						$("#grafico-campana").html("");
					}

					doneAjax4=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});



				//ajax POR ANOMALIAS
				$.ajax({
					url:'request/porAnomalias.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var anomalias=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							anomalias[i]=repuesta[i].descripcion;
							cantidad[i]=+repuesta[i].cantidad;
							
							//filas+="<tr class='fila'>"+
                            //  "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-anomalias",anomalias,series,colors);
						

						
					}
					else{												
						
						//$("#tabla-municipio tbody").html("");
						$("#grafico-anomalias").html("");
					}

					doneAjax5=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});//ajax POR ANOMALIAS
				




				//POR ITEMS
				$.ajax({
					url:'request/porItems.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var items=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							items[i]=repuesta[i].descripcion;
							cantidad[i]=+repuesta[i].cantidad;
							
							//filas+="<tr class='fila'>"+
                            //  "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-items",items,series,colors);
						

						
					}
					else{												
						
						//$("#tabla-municipio tbody").html("");
						//$("#grafico-municipio").html("");

						//$("#tabla-municipio tbody").html("");
						$("#grafico-items").html("");
					}

					doneAjax6=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});



				//POR ZONAS
				$.ajax({
					url:'request/porZonas.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var zonas=[];						
						var cantidad=[];
						//var colors=['#00034A','#0008C5','#FF6347','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#6ce600','#FF1493','#FF8C00','#9932CC','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						//var filas="";

						for (var i=0; i<repuesta.length; i++) {

							zonas[i]=repuesta[i].descripcion;
							cantidad[i]=+repuesta[i].cantidad;
							
							//filas+="<tr class='fila'>"+
                            //  "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						//$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Atendidas', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

						
						//$("#contenido").show();
						dibujarGraficoBarras("grafico-zonas",zonas,series,colors);
						

						
					}
					else{												
						
						//$("#tabla-municipio tbody").html("");
						//$("#grafico-municipio").html("");

						//$("#tabla-municipio tbody").html("");
						$("#grafico-zonas").html("");
					}

					doneAjax7=true;

					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5&&doneAjax6&&doneAjax7){
						
						$("#consultando").hide();
						$("#consultar").show();
						$("#contenido").show();
					}
				});


			});// fin clic buscar
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Tablero de control energía</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>

				<div id="div-total">                	
                	<h5>Total</h5>
                	<h4 id="total">0</h4>
              	</div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>General</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Resumen por técnico</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Resumen por municipio</span></a></li>
				<li><a href="#tab4">4. <span class='titulo-tab'>Resumen por campana</span></a></li>
				<li><a href="#tab5">5. <span class='titulo-tab'>Resumen por anomalías</span></a></li>
				<li><a href="#tab6">6. <span class='titulo-tab'>Resumen por items</span></a></li>
				<li><a href="#tab7">7. <span class='titulo-tab'>Resumen por zonas</span></a></li>
			
			</ul>


			<div id='tab1'>
				<table id='tabla-general'>
					<thead>
						<tr class='cabecera'><td>Acta</td><td>Fecha</td><td>Hora</td><td>Tecnico</td><td>Municipio</td><td>Proceso</td><td>Trafo</td><td>Cuenta</td><td>Cliente</td><td>Direccion</td><td>Anomalia</td><td>Zona</td><td>Irregularidades</td><td>Items</td><td>Sellos</td><td>Med. Instalado</td><td>Med. Ret. Encontrado</td><td>Observaciones</td><td>Supervisor</td><td>Censo Total</td><td>Consumo estimado</td><td>Consumo promedio</td><td>Tipo cliente</td><td>Cant. Item</td></tr>
					</thead>	

					<tbody>
						
					</tbody>
				</table>
			</div>


			<div id='tab2'>
				
				<div id='div-grafico-tecnico'>
					<div id='grafico-tecnico'></div>
				</div>
			</div>


			<div id='tab3'>
				
				<div id='div-grafico-municipio'>
					<div id='grafico-municipio'></div>
				</div>
			</div>


			<div id='tab4'>
				
				<div id='div-grafico-campana'>
					<div id='grafico-campana'></div>
				</div>
			</div>

			<div id='tab5'>
				
				<div id='div-grafico-anomalias'>
					<div id='grafico-anomalias'></div>
				</div>
			</div>


			<div id='tab6'>
				
				<div id='div-grafico-items'>
					<div id='grafico-items'></div>
				</div>
			</div>


			<div id='tab7'>
				
				<div id='div-grafico-zonas'>
					<div id='grafico-zonas'></div>
				</div>
			</div>


		</div>

	</div>

	

</body>
</html>