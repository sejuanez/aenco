<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=tableroControlEnergia_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	
	$consulta = "SELECT acta, Fecha_ejecucion, Hora, Tecnico, Municipio, Proceso, Trafo, cuenta, Nombre_cliente, Direccion,anomalia, Zona, irregularidades, items, Sellos_Instalados, med_instalado, med_ret_encontrado, observa, supervisor, censo_total, consumo_estimado, consumo_prom, tipo_cliente, cant_item FROM bitacora('".$fechaini."','".$fechafin."')";
	
	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Acta</td>".
						"<td>Fecha</td>".
						"<td>Hora</td>".
						"<td>Tecnico</td>".
						"<td>Municipio</td>".
						"<td>Proceso</td>".
						"<td>Trafo</td>".
						"<td>Cuenta</td>".
						"<td>Cliente</td>".
						"<td>Direccion</td>".
						"<td>Anomalia</td>".
						"<td>Zona</td>".
						"<td>Irregularidades</td>".
						"<td>Items</td>".
						"<td>Sellos</td>".
						"<td>Med. Instalado</td>".
						"<td>Med. Ret. Encontrado</td>".
						"<td>Observaciones</td>".
						"<td>Supervisor</td>".
						"<td>Censo Total</td>".
						"<td>Consumo estimado</td>".
						"<td>Consumo promedio</td>".
						"<td>Tipo cliente</td>".
						"<td>Cant. Item</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
					"<td>".utf8_encode($fila[5])."</td>".
					"<td>".utf8_encode($fila[6])."</td>".
					"<td>".utf8_encode($fila[7])."</td>".
					"<td>".utf8_encode($fila[8])."</td>".
					"<td>".utf8_encode($fila[9])."</td>".
					"<td>".utf8_encode($fila[10])."</td>".
					"<td>".utf8_encode($fila[11])."</td>".
					"<td>".utf8_encode($fila[12])."</td>".
					"<td>".utf8_encode($fila[13])."</td>".
					"<td>".utf8_encode($fila[14])."</td>".
					"<td>".utf8_encode($fila[15])."</td>".
					"<td>".utf8_encode($fila[16])."</td>".
					"<td>".utf8_encode($fila[17])."</td>".
					"<td>".utf8_encode($fila[18])."</td>".
					"<td>".utf8_encode($fila[19])."</td>".
					"<td>".utf8_encode($fila[20])."</td>".
					"<td>".utf8_encode($fila[21])."</td>".
					"<td>".utf8_encode($fila[22])."</td>".
					"<td>".utf8_encode($fila[23])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>