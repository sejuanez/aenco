<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	
	
	
    //$consulta = "SELECT lc.ca_acta acta, lc.ca_fechaej Fecha_ejecucion, lc.ca_horaej Hora, t.te_nombres Tecnico, m.mu_nombre Municipio, c.c_nombre Proceso, du.ui_nombre Anomalia, list(dm.ma_codmater) items from lega_cabecera lc left join tecnicos t on t.te_codigo=lc.ca_tecnico left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio left join campanas c on c.c_codigo=lc.ca_campana left join dato_unirre du on du.ui_acta=lc.ca_acta and du.ui_tipo='A' left join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_tipomat='O' where lc.ca_fechaej between '".$fechaini."' and '".$fechafin."' group by lc.ca_acta, lc.ca_fechaej, lc.ca_horaej, t.te_nombres, m.mu_nombre, c.c_nombre, du.ui_nombre";


    $consulta = "SELECT acta, Fecha_ejecucion, Hora, Tecnico, Municipio, Proceso, Trafo, cuenta, Nombre_cliente, Direccion,anomalia, Zona, irregularidades, items, Sellos_Instalados, med_instalado, med_ret_encontrado, observa, supervisor, censo_total, consumo_estimado, consumo_prom, tipo_cliente, cant_item FROM bitacora('".$fechaini."','".$fechafin."')";

	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['acta'] = utf8_encode($fila[0]);
		$row_array['fecha'] = utf8_encode($fila[1]);		
		$row_array['hora'] = utf8_encode($fila[2]);
		$row_array['tecnico'] = utf8_encode($fila[3]);
		$row_array['municipio'] = utf8_encode($fila[4]);
		$row_array['proceso'] = utf8_encode($fila[5]);
		$row_array['trafo'] = utf8_encode($fila[6]);
		$row_array['cuenta'] = utf8_encode($fila[7]);
		$row_array['cliente'] = utf8_encode($fila[8]);
		$row_array['direccion'] = utf8_encode($fila[9]);
		$row_array['anomalia'] = utf8_encode($fila[10]);
		$row_array['zona'] = utf8_encode($fila[11]);
		$row_array['irregularidades'] = utf8_encode($fila[12]);
		$row_array['items'] = utf8_encode($fila[13]);
		$row_array['sellos'] = utf8_encode($fila[14]);
		$row_array['medInstalado'] = utf8_encode($fila[15]);
		$row_array['medEncontrado'] = utf8_encode($fila[16]);
		$row_array['obs'] = utf8_encode($fila[17]);
		$row_array['supervisor'] = utf8_encode($fila[18]);
		$row_array['censoTotal'] = utf8_encode($fila[19]);
		$row_array['consumoEstimado'] = utf8_encode($fila[20]);
		$row_array['consumoProm'] = utf8_encode($fila[21]);
		$row_array['tipoCliente'] = utf8_encode($fila[22]);
		$row_array['cantItem'] = utf8_encode($fila[23]);
		
		
					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>