<?php

  define('MAX_SEGMENT_SIZE', 65535);

  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

  include("../../../init/gestion.php");
  // include("gestion.php");



function blob_create($data) {
    if (strlen($data) == 0)
        return false;
    $handle = ibase_blob_create();
    $len = strlen($data);
    for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
        $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
        $buf = substr($data, $pos, $buflen);
        ibase_blob_add($handle, $buf);
    }
    return ibase_blob_close($handle);
}


  $datos = $_POST['datos'];
  $sql = "INSERT into vehiculos_mtto
            (VM_PLACA,
            VM_COD_TIPOMTTO,
            VM_DETALLE,
            VM_FECHA_MTTO,
            VM_VALOR,
            VM_KM,
            VM_PROVEEDOR,
            VM_FACTURA,
            VM_DESCUENTO,
            VM_SOPORTE,
            VM_USUSIS)
           values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  $consulta = ibase_prepare($conexion , $sql);

  foreach ($datos as $key => $value) {

        // var_dump($value);
       $soporte = blob_create(file_get_contents("../temp/".$value['soporte']));

       ibase_execute($consulta,
                        $value['idPlaca'],
                        $value['tipoGasto'],
                        $value['observacionGasto'],
                        $value['fechaFactura'],
                        $value['valorFactura'],
                        $value['km'],
                        $value['idProveedor'],
                        $value['factura'],
                        $value['descuento'],
                        $soporte,
                        $_SESSION['user']
                      );

       unlink("../temp/".$value['soporte']);//acá le damos la direccion exacta del archivo

  }


?>