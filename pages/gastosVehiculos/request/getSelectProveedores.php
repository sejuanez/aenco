<?php
  session_start();

    if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }

  include("../../../init/gestion.php");
  // include("gestion.php");

  $sql = "SELECT PR_CODIGO AS CODIGO, PR_RAZONSOCIAL AS DESCRIPCION
          FROM PROVEEDORES ORDER BY PR_RAZONSOCIAL";

  $return_arr = array();

  $result = ibase_query($conexion, $sql);

  while($fila = ibase_fetch_row($result)){
    $row_array['CODIGO'] = utf8_encode($fila[0]);
    $row_array['DESCRIPCION'] = utf8_encode($fila[1]);
    array_push($return_arr, $row_array);
  }

  echo json_encode($return_arr);

?>