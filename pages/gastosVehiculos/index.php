<!DOCTYPE html>
<html>
<head>
	<title>Incripcion</title>
	<meta charset ="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
	<link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

	<style type="text/css">
		label{
			font-size: 0.8em;
		}
		.fondoGris {
			 background: #EFEFEF;
		}
		.form-group {
		    margin-bottom: 0.2rem;
		}
		.nav-tabs .nav-link {
			background: #EFEFEF;
			color:#999;
		}
		.form-control:disabled, .form-control[readonly] {
		    background-color: #eee;
		    opacity: 0.8;
		}
		#btnBuscar:hover{
			color: #333;
			text-shadow: 1px 0 #CCC;
		}
		select{
			padding: 3px;
			width: 100%;
		}

		.danger label{color:red;}
		.danger input{border:1px solid red}
		.danger select{border:1px solid red}


	</style>

</head>
<body>

	<div id="app">

		<header>
		  <p class="text-center fondoGris" style="padding: 10px;">

		     Gastos de Vehiculos

		    <span id="btnGuardar" v-if="tablaGastos.length!=0" class="float-right" style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
		    	<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
		    </span>

		     <span id="btnBuscar" v-show="btnAnadir" class="float-right" style="font-size: 1.2em; cursor: pointer; width: 130px; margin-top: -5px;" >
		    	 <b data-toggle="modal" data-target="#addGastos">+</b>
		    </span>

		  </p>
		</header>


		<div class="container">

				<div class="row">
					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="placa">Placa</label>
							<div class="input-group input-group-sm">
								<select class="selectPlaca" id="selectplaca" name="placa" @change="datosPlaca(this.value);" :disabled="tablaGastos.length!=0">
									<option value="">Seleccione una placa...</option>
									<option v-for="placa in selectPlacas" :value ="placa.id">{{ placa.text }}</option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="color">Color</label>
							<div class="input-group input-group-sm">
						      <input type="text" id="color" class="form-control" readonly aria-label="color" v-model="placaInfo.COLOR">
						    </div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="tipoVehiculo">Tipo Vehiculo</label>
							<div class="input-group input-group-sm">
						      <input type="text" id="tipoVehiculo" class="form-control" readonly aria-label="tipoVehiculo" v-model="placaInfo.TIPO_VEHICULO">

						    </div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="marca">Marca</label>
							<div class="input-group input-group-sm">
						      <input type="text" id="marca" class="form-control" readonly aria-label="marca" v-model="placaInfo.MARCA">
						    </div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="cuadrilla">Cuadrilla</label>
							<div class="input-group input-group-sm">
						      <input type="text" id="cuadrilla" class="form-control" readonly aria-label="Cuadrilla" v-model="placaInfo.CUADRILLA">

						    </div>
						</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label for="modelo">Modelo</label>
							<div class="input-group input-group-sm">
						      <input type="text" id="modelo" class="form-control" readonly aria-label="modelo" v-model="placaInfo.MODELO">
						    </div>
						</div>
					</div>

				</div>
				<br>
				<div class="row">
					<div class="col-12">
						<table class="table table-sm" >
						  <thead class="fondoGris">
						    <tr>
						      <th>Tipo</th>
						      <th>Descripción Gasto</th>
						      <th>Nombre Proveedor</th>
						      <th>Factura</th>
						      <th>Valor</th>
						      <th>Fecha</th>
						      <th>Km</th>
						      <th>Observación</th>
						      <th>Soporte</th>
						      <th>+</th>
						    </tr>
						  </thead>
						  <tbody id="detalle_gastos">
						  	  <tr v-for="(dato, index) in tablaGastos">
							      <td v-text ="dato.tipoGasto"></td>
							      <td v-text ="dato.gasto"></td>
							      <td v-text ="dato.proveedor"></td>
							      <td v-text ="dato.factura"></td>
							      <td v-text ="dato.valorFactura"></td>
							      <td v-text ="dato.fechaFactura"></td>
							      <td v-text ="dato.km"></td>
							      <td v-text ="dato.observacionGasto"></td>
							      <td style="text-align: center;">
							      	<i class="fa fa-file-picture-o"  style="cursor: pointer; font-size: 1.5em" @click="showSoporte(dato.soporte)"></i>
							      </td>
							      <th><i class="fa fa-times" title="Quitar item" style="cursor:pointer;" @click="deleteItem( dato, index);"></i class="fa fa-times"></th>
						      </tr>
						  </tbody>
						</table>
					</div>
				</div>

		</div>

		<div id="addGastos" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">

			    	<div class="modal-header">
				        <h6 class="modal-title" id="exampleModalLabel">Nuevo Gasto
				        	<small>( Los campos con <code>*</code> son obligatorios )</small></h6>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
					</div>

						<div class="modal-body">
				          <div class="row">
									 <div class="col-12 col-sm-3" >
										<div class="form-group input-group-sm">
											<label for="tipoGasto">Tipo Gasto <code>*</code></label>
										    <input type="text" id="gasto" name="gasto" class="form-control" required readonly aria-label="gasto" v-model="modalGastos.tipoGasto">
										</div>
									</div>

									<div class="col-12 col-sm-9">
										<div class="form-group input-group-sm">
											<label for="gasto">Descripcion Tipo <code>*</code></label>

												<select class="selectGasto" id="selectGasto" name="selectGasto" required>
													<option value="">Seleccione...</option>
													<option v-for="selectGasto in selectGastos"
														:value ="selectGasto.CODIGO">{{ selectGasto.NOMBRE }}</option>
												</select>

										</div>
									</div>

									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="idProveedor">Id Proveedor <code>*</code></label>
										  	<input type="text" id="idProveedor" name="idProveedor" class="form-control" required readonly aria-label="idProveedor" v-model="modalGastos.idProveedor">
										</div>
									</div>

									<div class="col-12 col-sm-9">
										<div class="form-group input-group-sm">
											<label for="nomProveedor">Proveedor <code>*</code></label>

												<select class="selectProveedor" id="selectProveedor" name="selectProveedor" required>
													 <option value="">Seleccione...</option>
													 <option v-for="selectProveedor in selectProveedores" :value="selectProveedor.CODIGO">{{selectProveedor.DESCRIPCION}}</option>
												</select>

										</div>
									</div>

									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="factura">Factura <code>*</code></label>
										    <input type="text" id="factura" name="factura" class="form-control" required aria-label="factura" v-model="modalGastos.factura">
										</div>
									</div>

									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="valorFactura">Valor <code>*</code></label>
										    <input type="text" id="valorFactura" name="valorFactura" class="form-control" required aria-label="valorFactura" v-model="modalGastos.valorFactura">
										</div>
									</div>

									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="fechaFactura">Fecha de Factura <code>*</code></label>
										    <input type="date" id="fechaFactura" name="fechaFactura" class="form-control" required aria-label="fechaFactura" v-model="fechaFactura">
										</div>
									</div>

									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="km">Km <code>*</code></label>
										    <input type="text" id="km" name="km" class="form-control" aria-label="km" required v-model="modalGastos.km">
										</div>
									</div>



									<div class="col-12 col-sm-3">
										<div class="form-group input-group-sm">
											<label for="nomProveedor">Descuento <code>*</code></label>

											<select v-model="modalGastos.descuento" required>
												 <option value="">Seleccione...</option>
												 <option value="SI">SI</option>
												 <option value="NO">NO</option>
											</select>

										</div>
									</div>

									<div class="col-12 col-sm-6">
										<div class="form-group input-group-sm">
											<form enctype="multipart/form-data" class="soporte">
												<label for="soporte">Soporte <code>*</code></label>
												<input type="file" id="soporte" name="archivo" accept="image/*" required>
											</form>
										</div>
									</div>

									<div class="col-12 col-sm-12">
										<div class="form-group input-group-sm">
											<label for="observacionGasto">Observación</label>
											<textarea class="form-control" id="observacionGasto" name="observacionGasto" rows="1" v-model="modalGastos.observacionGasto"></textarea>
										</div>
									</div>

							</div>

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">cancelar</button>
					        <button type="button" class="btn btn-primary" id="btnAñadir" @click="addItem();">Añadir</button>
						</div>




			    </div>
			  </div>
		</div>


		<div id="modalSoporte" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">

			    	<div class="modal-header">
				        <h6 class="modal-title" id="exampleModalLabel">Soporte</h6>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
					</div>

					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<img src="" id="imgSoporte" style="width: inherit;">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
					</div>




			    </div>
			  </div>
		</div>

	</div>



	<script src="js/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/select2.min.js"></script>
	<script src="js/bootstrap/popper.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/alertify/alertify.min.js"></script>
	<script src="js/vue/vue.js"></script>
	<script src="js/app.js"></script>



</body>
</html>