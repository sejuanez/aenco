// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

 jQuery(document).ready(function($) {
 	$(".selectPlaca").select2().change(function(e) {
 		var placa = $(this).val();
 		app.onChangePlaca(placa);
 	});;
});



// ----------------------------------------------
// Evento select Gastos
// ----------------------------------------------

jQuery(document).ready(function($) {
		$('.selectGasto').change(function(e) {
			var TIPOGASTO = $(this).val();
 			app.onChangeGastos(TIPOGASTO);
		});

});


// ----------------------------------------------
// Evento select Proveedor
// ----------------------------------------------

jQuery(document).ready(function($) {
	$('.selectProveedor').change(function(event){
		var PROVEEDOR = $(this).val();
 		app.onChangeProveedor(PROVEEDOR);
	});

});





let app = new Vue({
		el:'#app',
		data : {
			btnAnadir : false,
			idPlaca: "",
			placaInfo: [],
			selectPlacas: [],
			selectGastos: [],
			selectProveedores: [],
			fechaFactura:"",
			danger : true,

			modalGastos: {
				idPlaca: "",
				tipoGasto :"",
				gasto :"",
				idProveedor : "",
				proveedor : "",
				factura : "",
				valorFactura:"",
				fechaFactura  :"",
				km : "",
				observacionGasto : "",
				descuento: "",
				soporte : ""
			},

			tablaGastos : []

		},


		methods:{
			loadSelectPlaca: function () {
				var app = this;
				$.get('./request/getSelectPlacas.php', function(data) {
					app.selectPlacas = JSON.parse(data);
				});
			},

			onChangePlaca: function(PLACA){
				var app = this;
				app.idPlaca = PLACA;
				if (app.idPlaca!= "") {
					$.post('./request/getVehiculos.php', {placa: PLACA }, function(data) {
						var data = JSON.parse(data);
						app.placaInfo = data[0];
						app.btnAnadir = true;
					});
				}else {
					app.btnAnadir = false;
					app.placaInfo = [];
				}
			},

			loadSelectGastos: function (){
				var app = this;
				$.get('./request/getSelectMantenimientos.php', function(data) {
					var data = JSON.parse(data);
					app.selectGastos = data ;
				});
			},

			onChangeGastos : function (TIPOGASTO) {
				var app = this;
				$.post('./request/getSelectMantenimientosId.php', {tipo: TIPOGASTO }, function(data) {
					var tipo = jQuery.parseJSON(data);
					app.modalGastos.tipoGasto = tipo[0].CODIGO;
					app.modalGastos.gasto = tipo[0].NOMBRE;
				});
			},

			loadSelectProveedores : function (){
				var app = this;
				$.get('./request/getSelectProveedores.php', function(data) {
					var data = JSON.parse(data);
					app.selectProveedores = data;
				});
			},

			onChangeProveedor : function (PROVEEDOR) {
				var app = this;
				$.post('./request/getSelectProveedoresId.php', {PROVEEDOR: PROVEEDOR }, function(data) {
					var tipo = jQuery.parseJSON(data);
					app.modalGastos.idProveedor = tipo[0].CODIGO;
					app.modalGastos.proveedor = tipo[0].DESCRIPCION;
				});
			},

			addItem : function () {
				var count = 0;
				$('#addGastos input, #addGastos select').each(function(index, el) {
					// alert(el);
					if ($(this).val()=="") {
						$(this).parent('div').addClass('danger');
						count++;
					}else{
						$(this).parent('div').removeClass('danger');
					}
				});
				if (count==0) {
					var app = this;
					var formData = new FormData($(".soporte")[0]);
			        var message = "";
			        //hacemos la petición ajax
			        $.ajax({
			            url: './uploadFile.php',
			            type: 'POST',
			            // Form data
			            //datos del formulario
			            data: formData,
			            //necesario para subir archivos via ajax
			            cache: false,
			            contentType: false,
			            processData: false,
			            //mientras enviamos el archivo
			            beforeSend: function(){

			            },

			            success: function(data){
			            	// console.log(data);
			            	app.modalGastos.idPlaca = app.idPlaca;
			            	app.modalGastos.soporte = data;
			            	app.tablaGastos.push(app.modalGastos);
							app.clearModalGastos();
							$('#addGastos').modal('hide');
			            },
			            //si ha ocurrido un error
			            error: function(FormData){
			                console.log(data)
			            }
			        });
			     }else{
			     	alertify.error("Todos los campos con * son obligatorios");
			     }

			},
			clearModalGastos: function(){
				this.modalGastos = {
						idPlaca: "",
						tipoGasto :"",
						gasto :"",
						idProveedor : "",
						proveedor : "",
						factura : "",
						valorFactura:"",
						fechaFactura  :"",
						km : "",
						observacionGasto : ""
					};
					this.fechaFactura = "";
					$('select option:contains("Seleccione...")').prop('selected', true);
					$('input[type=file]').val(null);

			},
			showSoporte: function(data){
				$('#imgSoporte').attr('src', 'temp/'+data);
				$('#modalSoporte').modal('show');

			},
			deleteItem : function (dato, index){
				var app = this;
				alertify.confirm("Eliminar",".. Desea eliminar este item?",
				  function(){
				  	$.post('./request/unlink.php', {soporte: dato.soporte}, function(data) {
				  		if (data==1) {
				  			app.tablaGastos.splice(index, 1);
				  		}
				  	});

				  },
				  function(){
				    // alertify.error('Cancel');
				  });
			},

			guardar : function () {
				var app = this;
				alertify.confirm("Guardar",".. Desea guardar los registros?",
					function(){
						$.post('./request/insertGastos.php', {datos: app.tablaGastos}, function(data) {
							console.log(data);
							if (data=="") {
								alertify.success('Registro Guardado!!!');
								setTimeout(function(){location.reload();}, 500);
							}

						});
					},
					function(){

					});
			}


		},
		watch: {
	        fechaFactura: function(){
	        	var f = new Date();
 				// var f_actual= f.getDate()+"/"+(f.getMonth()+1)+"/"+f.getFullYear();
 				var fechaAux= this.fechaFactura.split("-");
 				var fechaFac = new Date(parseInt(fechaAux[0]), parseInt(fechaAux[1]-1), parseInt(fechaAux[2]));

 				f.setHours(0,0,0,0);
 				fechaFac.setHours(0,0,0,0);
	        	if (fechaFac.getTime() > f.getTime()) {
	        		alertify.error("No puede ingresar una fecha futura");
	        		this.fechaFactura = "";
	        	}else{
	        		this.modalGastos.fechaFactura = this.fechaFactura;
	        		// alert(this.fechaFactura);
	        	}

	        }
	    },
		mounted() {
			this.loadSelectPlaca();
			this.loadSelectGastos();
			this.loadSelectProveedores();

		},

});