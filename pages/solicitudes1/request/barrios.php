<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $ba_sector = $_POST['ba_sector'];

    $query = "SELECT * FROM barrios WHERE ba_sector = '$ba_sector' ORDER BY ba_nombarrio ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['ba_codbarrio'] = utf8_encode($row[3]);
		$row_array['ba_nombre'] = utf8_encode($row[4]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>