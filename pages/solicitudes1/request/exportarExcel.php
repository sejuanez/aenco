<?php
	session_start();
  	if(!$_SESSION['user']){
        echo"<script>window.location.href='../../inicio/index.php';</script>";
        exit();
    }
    header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=solicitudes_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	include("../../../init/gestion.php");

	$fechaIni = $_GET["fechaIni"];
	$fechaFin = $_GET["fechaFin"];
	$dpto     = $_GET["dpto"];
	$mpio     = $_GET["mpio"];
	$cto      = $_GET["cto"];
	$barrio   = $_GET["barrio"];

	$param = "";

	if (!empty($fechaIni) && !empty($fechaFin)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio) && !empty($cto)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' AND dla.dla_corregimiento = '$cto' ";
	}

	if (!empty($fechaIni) && !empty($fechaFin) && !empty($dpto) && !empty($mpio) && !empty($cto) && !empty($barrio)) {
		$param = "WHERE lc.ca_fechaej BETWEEN '$fechaIni' AND '$fechaFin' AND lc.ca_depto = '$dpto' AND lc.ca_municipio = '$mpio' AND dla.dla_corregimiento = '$cto' AND dla.dla_barrio = '$barrio' ";
	}


	$query = "SELECT lc.ca_acta acta, lc.ca_fechaej fecha_ejecucion,
				     dla.dla_serielum serie, dla.dla_tipo_lampara tipo_luminaria,
				     dla.dla_potencia potencia, dla.dla_luminaria luminaria,
				     d.de_nombre dpto, m.mu_nombre mpio, co.co_nombre corregimiento,
				     b.ba_nombarrio barrio, COUNT(dm.ma_acta) nro_actividades
			  FROM lega_cabecera lc
			  LEFT JOIN ot_ap ot ON ot.oa_numero = lc.ca_orden
			  LEFT JOIN dato_material dm ON dm.ma_acta = lc.ca_acta
		      LEFT JOIN dato_lev_alumbrado dla ON dla.dla_acta = lc.ca_acta
			  LEFT JOIN departamentos d ON d.de_codigo = lc.ca_depto
			  LEFT JOIN municipios m ON m.mu_depto = lc.ca_depto AND m.mu_codigomun = lc.ca_municipio
		      LEFT JOIN corregimientos co ON co.co_depto = lc.ca_depto AND co.co_municipio = lc.ca_municipio AND co.co_codcorregimiento = ot.oa_corregimiento
              LEFT JOIN barrios b ON b.ba_depto = lc.ca_depto AND b.ba_mpio = lc.ca_municipio AND b.ba_sector = ot.oa_corregimiento AND b.ba_codbarrio = ot.oa_barrio

			  $param
              GROUP BY lc.ca_acta, lc.ca_fechaej, dla.dla_serielum, dla.dla_tipo_lampara, dla.dla_potencia, dla.dla_luminaria, d.de_nombre, m.mu_nombre, co.co_nombre, b.ba_nombarrio";
	
	$return_arr = array();

	$data = ibase_query($conexion, $query);

	while($fila = ibase_fetch_row($data)) {
		utf8_encode($fila[0]);
		utf8_encode($fila[1]);
		utf8_encode($fila[2]);
		utf8_encode($fila[3]);
		utf8_encode($fila[4]);
		utf8_encode($fila[5]);
		utf8_encode($fila[6]);
		utf8_encode($fila[7]);
		utf8_encode($fila[8]);
		utf8_encode($fila[9]);
		utf8_encode($fila[10]);
		// array_push($return_arr, $row_array);
		// array_walk($fila, __NAMESPACE__ . '\cleanData');
    	echo implode("\t", array_values($fila)) . "\r\n";
	}
	
	
?>