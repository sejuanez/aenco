<?php
  session_start();
  
  if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  }  
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ResultadoGC</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
		
  <script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  <script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  <script type="text/javascript" src="../../js/accounting.js"></script>
    

  	<script type="text/javascript">
          
  		$(function(){

        $("#modal").hide();

        $( "#alert" ).dialog({
            modal: true,
            autoOpen: false,
            resizable:false,
             buttons: {
                  Ok: function() {
                    //$("#alert p").html("");
                    $( this ).dialog( "close" );
                  }
              }
           });
        

        function drawColumnChart(rows, element, colors) {

         
           $('#'+element).highcharts({
                                  chart: {
                                      type: 'column',
                                      backgroundColor:'#ffffff'
                                  },
                                  colors:colors,
                                  credits:{enabled:false},
                                  title:null /*{
                                      text: 'Recaudo diario'
                                  }*/,
                                  /*subtitle: {
                                      //text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                                      text:dia+'/'+mes+'/'+anio
                                  },*/
                                  xAxis: {
                                      type: 'category',
                                      labels: {
                                          //rotation: -45,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  },
                                  yAxis: {
                                      min: 0,
                                      title:null /*{
                                          text: 'Recaudo diario ($)'
                                      }*/
                                  },
                                  legend: {
                                      enabled: false
                                  },
                                  tooltip: {
                                      pointFormat: '<b>${point.y}</b>',
                                      pointFormatter: function(){return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});}
                                  },
                                  series: [{
                                      name: 'recaudo',
                                      data:rows,
                                      colorByPoint: true,
                                      dataLabels: {
                                          enabled: true,
                                          //rotation: -90,
                                          color: '#000',
                                          align: 'center',
                                         // format: '${point.y}', // one decimal
                                          formatter:function(){
                                              return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
                                          },
                                          y: 10, // 10 pixels down from the top
                                          padding:20,
                                          style: {
                                              fontSize: '10px',
                                              fontFamily: 'Verdana, sans-serif'
                                          }
                                      }
                                  }]
                              });
        };

        function drawPieChart(rows, element, colors){
          var center = [100, 50];
         
          $('#'+element).highcharts({
              chart: {
                  type: 'pie',
                  options3d: {
                      enabled: true,
                      alpha: 45,
                      beta: 0
                  },
                  backgroundColor:'#ffffff'
              },
              colors:colors,
              title: null,
              credits:{enabled:false},
              tooltip: {
                  pointFormat: '{point.y}<br><b>({point.percentage:.1f}%)</b>'
              },
              legend:{
                align:'right',
                layout: 'horizontal',
                verticalAlign: 'top'/*,
                floating:true*/
              },
              plotOptions: {
                  pie: {
                      //center:center,
                      size: "100%",
                      allowPointSelect: true,
                      cursor: 'pointer',
                      depth: 20,
                      dataLabels: {
                          enabled: false,
                          //format: '{point.name}<br>({point.percentage:.0f}%)</br>'
                      },
                      showInLegend: true
                  }
              },
              series: [{
                  type: 'pie',
                  //name: 'Browser share',
                  data: rows,
                  dataLabels: {
                      enabled: true,

                      rotation: 0,
                      formatter: function() {
                                  
                                  if(this.percentage>=4){
                                     return this.percentage.toFixed(1) + ' %';
                                  }
                                 
                                },
                      distance: -20,
                      
                      color:'rgba(0,0,0,0.7)',
                      //color: '#858585',
                     
                      style: {
                          fontSize: '10px',
                          fontFamily: 'Verdana, sans-serif',
                          //textShadow: '1px 1px #ffffff'
                      }
                  }
              }]
          });

        };

        function iniciarComponentes(){

            $("#div-vlrpago").hide();
            $("#div-convenios").hide();
            $("#div-atrasada").hide();
            $("#div-totalrecaudo").hide();

            $("#consultando").hide();

            $("#div-tecnicos").hide();
            $("#container-map").hide();

            $("#div-graf1").hide(); 
            $("#div-graf2").hide(); 
            $("#div-graf3").hide(); 
          
           $.datepicker.regional['es'] = {
           closeText: 'Cerrar',
           prevText: '<Ant',
           nextText: 'Sig>',
           currentText: 'Hoy',
           monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
           monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
           dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
           dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
           dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
           weekHeader: 'Sm',
           dateFormat: 'dd/mm/yy',
           firstDay: 1,
           isRTL: false,
           showMonthAfterYear: false,
           yearSuffix: ''
           };
           $.datepicker.setDefaults($.datepicker.regional['es']);

          $("#fecha").datepicker({
                  changeMonth: true,
                  changeYear: true,
                  dateFormat:"dd/mm/yy"
          });
          $( "#fecha" ).datepicker( "setDate", new Date());
         

          //$("#atras").button();

         //=============== Boton aceptar ====================
          //$("#consultar").button().click(function(){
          $("#consultar").click(function(e){

              e.preventDefault();
              $(this).hide();
              //$("#contenido").hide();
              $("#consultando").show();
              

              $("#div-vlrpago").fadeOut(200);
              $("#div-convenios").fadeOut(200);
              $("#div-atrasada").fadeOut(200);
              $("#div-totalrecaudo").fadeOut(200);

                           

              $("#div-tecnicos").fadeOut(200);
              $("#div-tecnicos").html("");              
              $("#container-map").fadeOut(200);

              $("#div-graf1").fadeOut(200);
              $("#div-graf2").fadeOut(200);
              $("#div-graf3").fadeOut(200);
              
              

              var dia = +($('#fecha').datepicker('getDate').getDate());
              if(dia<10)
                dia="0"+dia;

              var mes = +($('#fecha').datepicker('getDate').getMonth() + 1);
              if(mes<10)
                mes="0"+mes;

              var anio = $('#fecha').datepicker('getDate').getFullYear();
        
        
              var doneAjax0=doneAjax1=doneAjax2=doneAjax3=doneAjax4=false;

              //ajax que muestra los totales
              $.ajax({
                        url:'request/totales.php',
                        type:'POST',
                        dataType:'json',
                        data:{anio:anio,mes:mes,dia:dia,tecnico:null}
                    }).done(function (repuesta){
                            if(repuesta.length>0){//hay totales para mostrar
                              var vlrtotal= (+repuesta[0].vlrpago) + (+repuesta[0].vlrconvenios) + (+repuesta[0].vlratrasada);
                              
                              var vlrpago=accounting.formatMoney(repuesta[0].vlrpago, {symbol : '$', precision : 0,thousand : ','});
                              var vlrconvenios=accounting.formatMoney(repuesta[0].vlrconvenios, {symbol : '$', precision : 0,thousand : ','});
                              var vlratrasada=accounting.formatMoney(repuesta[0].vlratrasada, {symbol : '$', precision : 0,thousand : ','});
                              
                              vlrtotal=accounting.formatMoney(vlrtotal, {symbol : '$', precision : 0,thousand : ','});
                              $("#vlrpago").html(vlrpago);
                              $("#vlrconvenios").html(vlrconvenios);
                              $("#vlratrasada").html(vlratrasada);
                              $("#totalrecaudo").html(vlrtotal);
                              
                              $("#div-vlrpago").fadeIn(200);
                              $("#div-convenios").fadeIn(200);
                              $("#div-atrasada").fadeIn(200);
                              $("#div-totalrecaudo").fadeIn(200);
                            }
                            else{
                                $("#vlrpago").html("---");
                                $("#vlrconvenios").html("---");
                                $("#vlratrasada").html("---");
                                $("#totalrecaudo").html("---");

                                $("#alert").dialog("open");
                            }

                            doneAjax0=true;
                            if(doneAjax0&&doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){
                              
                              $("#consultando").hide();
                              $("#consultar").show();
                            }
              });//fin del done()

              

              $.ajax({//  ajax que carga el grafico dde torta 1
                    url:'request/grafico1.php',
                    type:'POST',
                    dataType:'json',
                    data:{dia:dia, mes:mes, anio:anio}

              }).done(function(repuesta){

                    if(repuesta.length>0){

                        $("#graf1-vlrcampana").html(accounting.formatMoney(+repuesta[0].campana, {symbol : '$', precision : 0,thousand : ','}));
                        $("#graf1-recaudo").html(accounting.formatMoney(+repuesta[0].recaudo, {symbol : '$', precision : 0,thousand : ','}));
                        var piechart=[];
                        
                        var colors=['#ff0000','#00ff00'];
                        

                        
                        
                        for(var i=0; i<repuesta.length;i++)
                        {
                          piechart.push(['Meta',+repuesta[i].campana]);
                          piechart.push(['Valor recaudo mes',+repuesta[i].recaudo]);

                        }
                        $("#div-graf1").fadeIn(200);

                        drawPieChart(piechart,"graf1",colors);//DIBUJAR EL GRAFICO
                        //$("svg>text:last-child").html("");//quitamos la firma higcharts.com

                        
                    }
                    else{

                        $("graf1").html("<p>No hay nada que graficar</p>");
                    }

                    doneAjax1=true;
                    if(doneAjax0&&doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){
                      
                      $("#consultando").hide();
                      $("#consultar").show();
                    }
              });//fin del ajax que carga el grafico1


              $.ajax({ //ajax que carga el grafico2
                    url:'request/grafico2.php',
                    type:'POST',
                    dataType:'json',
                    data:{dia:dia, mes:mes, anio:anio}

              }).done(function(repuesta){

                    if(repuesta.length>0){

                        $("#graf2-vlrcampana").html(accounting.formatMoney(+repuesta[0].campana, {symbol : '', precision : 0,thousand : ','}));
                        $("#graf2-recaudo").html(accounting.formatMoney(+repuesta[0].visitados, {symbol : '', precision : 0,thousand : ','}));
                        var piechart=[];
                        var colors=['blue','yellow'];
                        
                        for(var i=0; i<repuesta.length;i++)
                        {
                          piechart.push(['Usuarios Campaña',+repuesta[i].campana]);
                          piechart.push(['Total visitados',+repuesta[i].visitados]);
                         }

                         $("#div-graf2").fadeIn(200);
                         drawPieChart(piechart,"graf2",colors);//DIBUJAR EL GRAFICO
                         //$("svg>text:last-child").html("");//quitamos la firma higcharts.com

                    }
                    else{

                        $("graf2").html("<p>No hay nada que graficar</p>");
                    }

                    doneAjax2=true;
                    if(doneAjax0&&doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){
                      
                      $("#consultando").hide();
                      $("#consultar").show();
                    }
              });



              $.ajax({ //ajax que carga el grafico3
                    url:'request/grafico3.php',
                    type:'POST',
                    dataType:'json',
                    data:{dia:dia, mes:mes, anio:anio}

              }).done(function(repuesta){

                    if(repuesta.length>0){

                        
                        var piechart=[];
                        var colors=['#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
                        
                        for(var i=0; i<repuesta.length;i++)
                        {
                          piechart.push([repuesta[i].descripcion,+repuesta[i].valor]);
                          
                         }
                         $("#div-graf3").fadeIn(200);

                         drawPieChart(piechart,"graf3",colors);//DIBUJAR EL GRAFICO
                         //$("svg>text:last-child").html("");//quitamos la firma higcharts.com

                    }
                    else{

                        $("graf3").html("<p>No hay nada que graficar</p>");
                    }

                    doneAjax3=true;
                    if(doneAjax0&&doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){
                      
                      $("#consultando").hide();
                      $("#consultar").show();
                    }
              });


                     

             /* $.ajax({//ajax que carga las tabla tecnicos y el grafico de barras
                        url:'request/grafico4.php',
                        type:'POST',
                        dataType:'json',
                        //data:{mes:($("#mes")[0].selectedIndex) + 1,anio:$("#anio").val()}
                        data:{dia:dia, mes:mes, anio:anio}

                    }).done(function(repuesta){
                       
                        nodoActivo=null;
                        

                        if(repuesta.length>0){// si hay tecnicos que mostrar

                          var colors=['#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
                          
                          
                          var hc_column=[];
                          
                          var list_tecnicos ="<table><tr class='titulo'><td colspan='4'>Resultado del día por gestor</td></tr><tr class='cabecera'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>";

                          for(var i=0; i<repuesta.length;i++)
                          {                           
                               list_tecnicos+="<tr name='"+repuesta[i].tecnico+"' class='fila'>"+
                              "<td>"+repuesta[i].nombre+"</td><td>"+(accounting.formatMoney(+repuesta[i].recaudoDia, {symbol : '$', precision : 0,thousand : ','}))+"</td>"+"<td>"+repuesta[i].cupones+"</td><td>"+repuesta[i].convenios+"</td></tr>";

                              
                              hc_column.push([repuesta[i].nombre,+repuesta[i].recaudoDia]);
                          }

                          
                          list_tecnicos+="</table>";

                          $("#div-tecnicos").html(list_tecnicos);

                          

                          
                          drawColumnChart(hc_column,"container-map",colors);//DIBUJAR EL GRAFICO
                          $("g.highcharts-data-labels>g[opacity=0]").attr("opacity",1);

                                                                            
                          $("#div-tecnicos").fadeIn(200);                       
                          $("#container-map").fadeIn(200);
                         
                        }//fin si hay tecnicos
                        else
                        {
                          //alert("No se encontraron resultados!");
                          $("#modal h1").html("Lo sentimos!");
                          $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                        }

                      $("#consultando").hide();
                      $("#consultar").show();
              });*///fin del ajax Tecnicos 


              var repuestaAjax4_1=repuestaAjax4_2=repuestaAjax4_3=null;

              $.ajax({//ajax grafico4_1: cod_tecnico, nombre_tecnico, recaudo_dia 

                  url:'request/grafico4_1.php',
                  type:'POST',
                  dataType:'json',
                  data:{dia:dia, mes:mes, anio:anio}

              }).done(function(repuesta){

                  if (repuesta.length>0){

                    //gardamos una copia de la repuesta del primer ajax
                    repuestaAjax4_1 = repuesta.slice();


                    $.ajax({//ajax grafico4_2: cod_tecnico, cupones 

                        url:'request/grafico4_2.php',
                        type:'POST',
                        dataType:'json',
                        data:{dia:dia, mes:mes, anio:anio}

                    }).done(function(repuesta){

                        if(repuesta.length>0){
                          
                          //gardamos una copia de la repuesta del segundo ajax
                          repuestaAjax4_2 = repuesta.slice();

                          
                          var cupones=[];

                          for (var i=0; i<repuestaAjax4_1.length;i++){

                            var sw=false;

                            for (var j=0;(j<repuestaAjax4_2.length)&&(!sw);j++){

                                if(repuestaAjax4_2[j].tecnico==repuestaAjax4_1[i].tecnico){
                                  cupones[i]=repuestaAjax4_2[j].cupones;
                                  sw=true;
                                }

                            }
                            if(!sw)
                                cupones[i]=0;

                          }//fin del for para cupones

                          $.ajax({//ajax grafico4_3: cod_tecnico, convenios 

                              url:'request/grafico4_3.php',
                              type:'POST',
                              dataType:'json',
                              data:{dia:dia, mes:mes, anio:anio}

                          }).done(function(repuesta){

                            if(repuesta.length>0){

                                //gardamos una copia de la repuesta del segundo ajax
                                repuestaAjax4_3 = repuesta.slice();

                                
                                var convenios=[];

                                for (var i=0; i<repuestaAjax4_1.length;i++){

                                  var sw=false;

                                  for (var j=0;(j<repuestaAjax4_3.length)&&(!sw);j++){

                                      if(repuestaAjax4_3[j].tecnico==repuestaAjax4_1[i].tecnico){
                                        convenios[i]=repuestaAjax4_3[j].convenios;
                                        sw=true;
                                      }

                                  }
                                  if(!sw)
                                    convenios[i]=0;

                                }//fin del for para convenios


                                


                                // construir tabla y grafico de barras

                                var colors=['#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
                                var hc_column=[];
                                var list_tecnicos ="<table><tr class='titulo'><td colspan='4'>Resultado del día por gestor</td></tr><tr class='cabecera'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>";

                                for (var i=0; i<repuestaAjax4_1.length;i++){
                                      
                                      list_tecnicos+="<tr name='"+repuestaAjax4_1[i].tecnico+"' class='fila'>"+
                                  "<td>"+repuestaAjax4_1[i].nombre+"</td><td>"+(accounting.formatMoney(+repuestaAjax4_1[i].recaudoDia, {symbol : '$', precision : 0,thousand : ','}))+"</td>"+"<td>"+cupones[i]+"</td><td>"+convenios[i]+"</td></tr>";

                                  
                                  hc_column.push([repuestaAjax4_1[i].nombre,+repuestaAjax4_1[i].recaudoDia]);
                                }

                                list_tecnicos+="</table>";
                                $("#div-tecnicos").html(list_tecnicos);

                                drawColumnChart(hc_column,"container-map",colors);//DIBUJAR EL GRAFICO

                                // fin construir tabla y grafico de barras

                                $("#div-tecnicos").fadeIn(200);                       
                                $("#container-map").fadeIn(200);


                            }
                            else
                            {
                              $("#modal h1").html("Lo sentimos!");
                              $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                            }

                            doneAjax4=true;
                            if(doneAjax0&&doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4){
                              
                              $("#consultando").hide();
                              $("#consultar").show();
                            }

                          });

                        }
                        else
                        {
                          
                          $("#modal h1").html("Lo sentimos!");
                          $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");

                        }
                        

                    });


                  }
                  else{

                    $("#modal h1").html("Lo sentimos!");
                    $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                  }       



              }); //  fin del ajax4_1      

            
          });
         //=============== fin Boton aceptar ================
        };//fin de iniciarComponentes()

        //===================================================================================

        
         iniciarComponentes();                                      
                 
        //======================================================================================
        $("body").show();
       
  		});//fin del onready
  	</script>
</head>
  <body style="display:none">

    <!--alert-->
    <div id="alert" title="Mensaje">
        <p>No hay nada que mostrar.</p>
    </div>

    <!--header-->
    <header>
      <h3>Gestión por día y Acumulado</h3>
      <nav>
        <ul id="menu">
          
          <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
          <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
            
        </ul>
      </nav>    
    </header>

    <!--subheader-->

    <div id='subheader'>

          <div id="div-form">
            <form id="form">
              <div><label>Fecha</label><input type="text" id="fecha" readOnly></div>               
            </form>          
          </div>
    
          <div id="div-subtotales">
              <div id="div-vlrpago">        
                <h4 id="vlrpago">0</h4>
                <h5><span>Vlr. </span>Pago</h5>
              </div>

              <div id="div-convenios">        
                <h4 id="vlrconvenios">0</h4>
                <h5><span>Vlr. Cta. </span>Convenios</h5>
              </div>

              <div id="div-atrasada">        
                <h4 id="vlratrasada">0</h4>
                <h5><span>Vlr. Cta. </span>Atrasada</h5>
              </div>

              <div id="div-totalrecaudo">        
                <h4 id="totalrecaudo">0</h4>
                <h5><span>Total </span>Recaudo</h5>
              </div>
          </div>
    
  </div>




    <div id='modal'><h1>Comenzar</h1><p>Escoge una fecha, luego pulsa el boton Consultar. </p></div>


    <div id="contenido">

      <div id="div-tecnicos">
        <!--<table><tr class='titulo'><td colspan='4'>Resultado del día por gestor</td></tr><tr class='cabecera'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
          <tr class='fila'><td>Gestor</td><td>Recaudo</td><td>Cupones</td><td>Convenios</td></tr>
        </table>-->
      </div>



      <!--<div id="container-map" style="min-width: 300px; height: 600px  margin: 0 auto">
        
      </div>-->
      <div id="container-map">
        
      </div>

         

    </div>

   
    <div id="div-graficos">

         <div id="div-graf1">
                <table class="detalle-grafico">
                        <tr class="title"><td>Meta</td><td>Valor recaudo mes</td></tr>
                        <tr class="cabecera" ><td id="graf1-vlrcampana"></td><td id="graf1-recaudo"></td></tr>
                </table> 

                <div id="container-graf1">
                  <div id="graf1"></div></div>

                

          </div>


          <div id="div-graf2">
                <table class="detalle-grafico">
                        <tr class="title"><td>Usuarios Campaña</td><td>Total visitados</td></tr>
                        <tr class="cabecera"><td id="graf2-vlrcampana"></td><td id="graf2-recaudo"></td></tr>
                </table> 
                <div id="graf2"></div>

                
          </div>


          <div id="div-graf3">
                <table class="detalle-grafico">
                        <tr class="title"><td>Resultado de la campaña</td><td> &nbsp;</td></tr>
                        <tr class="cabecera"><td> &nbsp;</td><td> &nbsp;</td></tr>
                </table> 

                <div id="graf3">
                </div>

               
          </div>
       
          
    </div>

       

  </body>
</html>