<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	$dia = $_POST["dia"];
	$mes = $_POST["mes"];
	$anio = $_POST["anio"];
	
	include("../../../init/gestion.php");

	
	
	
	$consulta = "SELECT  gc.tecnico, rh.rh_nombre1||' '||rh.rh_apellido1, (sum(coalesce (gc.valor_pago,0)) + sum(coalesce(gc.vr_c_convenio,0)) + sum(coalesce(gc.vr_c_atrasada,0)))  RECAUDO_DIA, 0 cupones, 0 convenios  FROM gestion_cobro_entrega gc inner join lega_cabecera lc on lc.ca_orden = gc.id left join tecnicos t on t.te_codigo=gc.tecnico left join datos_rrhh rh on rh.rh_cedula = t.te_cedula WHERE gc.ano=".$anio."  AND gc.mes =".$mes."   AND lc.ca_fechaej='".$anio."-".$mes."-".$dia."' AND gc.tecnico is not null group by gc.tecnico, rh.rh_nombre1||' '||rh.rh_apellido1";
	
	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['tecnico'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]);
		
		$row_array['recaudoDia'] = utf8_encode($fila[2]);
		$row_array['cupones'] = utf8_encode($fila[3]);
		$row_array['convenios'] = utf8_encode($fila[4]);
		
				
		array_push($return_arr, $row_array);
	}

	/*//prueba
	$row_array['tecnico'] ="123";
	$row_array['nombre'] = "Elkin Barreto";
		
	$row_array['recaudoDia'] = 123;
	$row_array['cupones'] = 123;
	$row_array['convenios'] = 123;		
				
	array_push($return_arr, $row_array);

	$row_array['tecnico'] ="456";
	$row_array['nombre'] = "Alexis Contreras";
		
	$row_array['recaudoDia'] = 456;
	$row_array['cupones'] = 456;
	$row_array['convenios'] = 456;		
				
	array_push($return_arr, $row_array);
	//fin prueba*/

	echo json_encode($return_arr);

?>