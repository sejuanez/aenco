<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">

	<title>PQR</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<!--<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />-->

  	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	<script type="text/javascript">
		
		$(function(){

			var clientes;
			var clienteSeleccionado = null;

			

			$('#pais').attr('disabled',true);
			$('#pais-editarCliente').attr('disabled',true);

			$('#departamento').attr('disabled',true);
			$('#departamento-editarCliente').attr('disabled',true);

			$('#municipio').attr('disabled',true);
			$('#municipio-editarCliente').attr('disabled',true);

			



			//alert
			$( "#alert" ).dialog({
	            modal: true,
	            autoOpen: false,
	            resizable:false,
	            buttons: {
	                  Ok: function() {
	                    //$("#alert p").html("");
	                    $( this ).dialog( "close" );
	                  }
	            }
	        });

			//alert-eliminar-cliente
			$( "#alert-eliminar-cliente" ).dialog({
	            resizable: false,
	            /*height:200,
	            width:400,*/
	            modal: true,
	            autoOpen:false,
	            buttons:{
	            	Si: function(){

	            		$.ajax({
                                url:'request/eliminarCliente.php',
                                type:'POST',
                                dataType:'json',
                                data:{idcliente:clientes[clienteSeleccionado].id}
                            }).done(function(repuesta){

                            	$( "#alert-eliminar-cliente" ).dialog("close");

                            	if(repuesta[0].success){
								
									getClientes(null);
									$("#alert p").html(repuesta[0].msg);
									$("#alert").dialog("open");
									
								}else{
									
									$("#alert p").html(repuesta[0].msg);
									$("#alert").dialog("open");
								}

                            });
	            	},

	            	no:function(){$( this ).dialog( "close" );}
	            }
	        });


		
	        //alert-eliminar-tel-editarCliente
			$( "#alert-eliminar-tel-editarCliente" ).dialog({
	            resizable: false,
	            /*height:200,
	            width:400,*/
	            modal: true,
	            autoOpen:false,
	        });


	        //alert-eliminar-tel-editarCliente
			$( "#alert-eliminar-correo-editarCliente" ).dialog({
	            resizable: false,
	            /*height:200,
	            width:400,*/
	            modal: true,
	            autoOpen:false,
	        });
		


			//Evento click del boton nuevo-cliente
			$("#nuevo-cliente").click(function(e){

				e.preventDefault();

				$("#form-nuevoCliente").show();
			});	




			//================== function delay =======================================
    		var delay = (function(){
                  var timer = 0;
                  return function(callback, ms){
                    clearTimeout (timer);
                    timer = setTimeout(callback, ms);
                  };
            })();
        //================== function delay fin   =================================

        


        //Evento change de cuadro de busqueda de clientes

        $("#txtbuscar").keyup(function(event){
          
            if((+event.keyCode>=46)&&(+event.keyCode<=90)||(+event.keyCode>=96)&&(+event.keyCode<=111)||(+event.keyCode==8||+event.keyCode==46)){
                
                delay(function(){
                  
                  $("#lista-contactos").html("<h6 style='text-align:center;'>Buscando...</h6>");
                  
                  $("#panel-detalles > div").hide();
                  
                  getClientes($("#txtbuscar").val());

                }, 1000 );
            } 
  		});

        //fin del evento change del cuadrode busquedas de clientes






//Eventos para componentes de form-nuevoCliete

		$("#guardando-form-nuevoCliente").click(function(e){e.preventDefault()});
		$("#guardando-form-editarCliente").click(function(e){e.preventDefault()});

		$("#guardando-form-nuevoCliente").hide();
		$("#guardando-form-editarCliente").hide();

        //Evento clic del boton cerrar - form-nuevoCliente
        $("#cancelar-form-nuevoCliente").click(function(e){

        	e.preventDefault();
        	limpiarFormNuevoCliente();
        	$("#form-nuevoCliente").hide();

        });

        //Evento clic del boton cerrar - form-editarCliente
        $("#cancelar-form-editarCliente").click(function(e){

        	e.preventDefault();
        	limpiarFormEditarCliente();
        	$("#form-editarCliente").hide();

        });


        //Evento click de a.borrar-tel form-nuevoCliente

        $("a.borrar-tel").click(function(e){
        	e.preventDefault();
        	$(this).parent().parent().remove();
        });





        //Evento click de a#agregar-tel form_nuevoCliente

        $("#agregar-tel").click(function(e){
        	e.preventDefault();
        	$("#tel-panel-form-nuevoCliente tbody").append("<tr class='fila'><td><input type='text' class='nuevo-tel' value=''></td><td><a href='#' class='borrar-tel' title='Borrar teléfono'><span class='ion-ios-close-empty'></span></a></td></tr>");

        	$("#tel-panel-form-nuevoCliente tbody > tr:last-child input[type='text']").focus();

        	$("a.borrar-tel").click(function(e){
	        	e.preventDefault();
	        	$(this).parent().parent().remove();

        	});
        }); 

        $("#agregar-tel-editarCliente").click(function(e){
        	e.preventDefault();
        	$("#tel-panel-form-editarCliente tbody").append("<tr class='fila'><td><input type='text' class='nuevo-tel-editarCliente' value=''></td><td><a href='#' class='borrar-tel-nuevo' title='Borrar teléfono'><span class='ion-ios-close-empty'></span></a></td></tr>");

        	$("#tel-panel-form-editarCliente tbody > tr:last-child input[type='text']").focus();

        	$("a.borrar-tel-nuevo").click(function(e){
	        	e.preventDefault();
	        	$(this).parent().parent().remove();

        	});
        });



        //Evento click de a.borrar-correo form-nuevoCliente

        $("a.borrar-correo").click(function(e){
        	e.preventDefault();
        	$(this).parent().parent().remove();
        });



        //Evento click de a#agregar-correo form-nuevoCliente

        $("#agregar-correo").click(function(e){
        	e.preventDefault();
        	$("#correo-panel-form-nuevoCliente tbody").append("<tr class='fila'><td><input type='text' class='nuevo-correo' value=''></td><td><a href='#' class='borrar-correo' title='Borrar correo'><span class='ion-ios-close-empty'></span></a></td></tr>");

        	$("#correo-panel-form-nuevoCliente tbody > tr:last-child input[type='text']").focus();

        	$("a.borrar-correo").click(function(e){
	        	e.preventDefault();
	        	$(this).parent().parent().remove();

        	});
        });


        //Evento click de a#agregar-correo form-editarCliente

        $("#agregar-correo-editarCliente").click(function(e){
        	e.preventDefault();
        	$("#correo-panel-form-editarCliente tbody").append("<tr class='fila'><td><input type='text' class='nuevo-correo-editarCliente' value=''></td><td><a href='#' class='borrar-correo-nuevo' title='Borrar correo'><span class='ion-ios-close-empty'></span></a></td></tr>");

        	$("#correo-panel-form-editarCliente tbody > tr:last-child input[type='text']").focus();

        	$("a.borrar-correo-nuevo").click(function(e){
	        	e.preventDefault();
	        	$(this).parent().parent().remove();

        	});
        });

			

		function limpiarFormNuevoCliente(){
			
			$("#form-nuevoCliente input[type=text]").val("");

			$("#tel-panel-form-nuevoCliente tbody").html("<tr class='cabecera'><td colspan='2'>Teléfonos</td></tr>");

			$("#correo-panel-form-nuevoCliente tbody").html("<tr class='cabecera'><td colspan='2'>Correos</td></tr>");
			
		};

		function limpiarFormEditarCliente(){
			
			$("#form-editarCliente input[type=text]").val("");

			$("#tel-panel-form-editarCliente tbody").html("<tr class='cabecera'><td colspan='2'>Teléfonos</td></tr>");

			$("#correo-panel-form-editarCliente tbody").html("<tr class='cabecera'><td colspan='2'>Correos</td></tr>");
			
		};
		

		function validarFormulario(){
			if($("#txtNitId").val()!="" && $("#txtRazonSocial").val()!="" && $("#txtNombre1").val()!="" && $("#txtNombre2").val()!="" && $("#apellido1").val()!="" && $("#txtApellido2").val()!="" && $("#pais").val()!=null && $("#departamento").val()!=null && $("#municipio").val()!=null ){
				return true;
			}else{
				return false;
			}
		};//fin de la funcion validar formulario



		function validarFormularioEditarCliente(){
			if($("#txtNitId-editarCliente").val()!="" && $("#txtRazonSocial-editarCliente").val()!="" && $("#txtNombre1-editarCliente").val()!="" && $("#txtNombre2-editarCliente").val()!="" && $("#apellido1-editarCliente").val()!="" && $("#txtApellido2-editarCliente").val()!="" && $("#pais-editarCliente").val()!=null && $("#departamento-editarCliente").val()!=null && $("#municipio-editarCliente").val()!=null ){
				return true;
			}else{
				return false;
			}
		};//fin de la funcion validar formulario

			
			

		function setMunicipio(pais, dpto){
				$.ajax({
					url:'request/getMunicipios.php',
	                type:'POST',
	                dataType:'json',
	                data:{pais:pais, dpto:dpto}
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#municipio").html(options);
						$('#municipio').attr('disabled',false);
						//setCorregimiento(Dpto,$("#municipio").val());
					}
					
				});

		};// fin de la funcion setMunicipio

		function setMunicipioEditarCliente(pais, dpto,munSeleccionado){
				$.ajax({
					url:'request/getMunicipios.php',
	                type:'POST',
	                dataType:'json',
	                data:{pais:pais, dpto:dpto}
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#municipio-editarCliente").html(options);

						if(munSeleccionado!=undefined)
							$("#municipio-editarCliente").val(munSeleccionado);

						$('#municipio-editarCliente').attr('disabled',false);
						//setCorregimiento(Dpto,$("#municipio").val());
					}
					
				});

		};// fin de la funcion setMunicipio

			
		function setDepartamento(pais){
				$.ajax({
					url:'request/getDepartamentos.php',
	                type:'POST',
	                dataType:'json',
	                data:{pais:pais}
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#departamento").html(options);
						$('#departamento').attr('disabled',false);
						setMunicipio(pais,$("#departamento").val());
					}
					
				});

		};// fin de la funcion setDepartamento


		function setDepartamentoEditarCliente(pais, dptoSeleccionado){
				$.ajax({
					url:'request/getDepartamentos.php',
	                type:'POST',
	                dataType:'json',
	                data:{pais:pais}
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#departamento-editarCliente").html(options);
						
						if(dptoSeleccionado!=undefined)
							$("#departamento-editarCliente").val(dptoSeleccionado);
						else
							setMunicipioEditarCliente(pais,$("#departamento-editarCliente").val());
						
						$('#departamento-editarCliente').attr('disabled',false);

						//setMunicipio(pais,$("#departamento-editarCliente").val());
					}
					
				});

		};// fin de la funcion setDepartamento


		function setPaisEditarCliente(paisSeleccionado){
			//ajax que carga la liista de paises
			$.ajax({
					url:'request/getPaises.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsPais="";
						
						
						for(var i=0;i<repuesta.length;i++){
							optionsPais+="<option value='"+repuesta[i].codigoPais+"'>"+repuesta[i].nombrePais+"</option>";
							
						}
						
						$("#pais-editarCliente").html(optionsPais);
						$("#pais-editarCliente").val(paisSeleccionado);
						$('#pais-editarCliente').attr('disabled',false);
						//
						//setDepartamento($("#pais-editarCliente").val());
						
					}
					
			});// fin del ajax paises
		};


		//ajax que carga la liista de paises
		$.ajax({
				url:'request/getPaises.php',
                type:'POST',
                dataType:'json'
                
			}).done(function(repuesta){
				
				if(repuesta.length>0){
					var optionsPais="";
					
					
					for(var i=0;i<repuesta.length;i++){
						optionsPais+="<option value='"+repuesta[i].codigoPais+"'>"+repuesta[i].nombrePais+"</option>";
						
					}
					
					$("#pais").html(optionsPais);
					$('#pais').attr('disabled',false);
					//
					setDepartamento($("#pais").val());
					//setCorregimiento($("#departamento").val(),$("#municipio").val());
				}
				
		});// fin del ajax paises

		


			
			//Listeners de el evento Change de las listas pais
			$("#pais").on("change",function(){
					
					$("#departamento").html("");
					$("#departamento").attr("disabled",true);

					$("#municipio").html("");
					$("#municipio").attr("disabled",true)

					//$("#barrio").html("");
					setDepartamento($("#pais").val());
			});	
			//fin de Listeners de el evento Change de la lista pais


			//Listeners de el evento Change de las listas pais-editar cliente
			$("#pais-editarCliente").on("change",function(){
					
					$("#departamento-editarCliente").html("");
					$("#departamento-editarCliente").attr("disabled",true);

					$("#municipio-editarCliente").html("");
					$("#municipio-editarCliente").attr("disabled",true)

					//$("#barrio").html("");
					setDepartamentoEditarCliente($("#pais-editarCliente").val());
			});	
			//fin de Listeners de el evento Change de la lista pais


			//Listeners de el evento Change de las listas departamentos
			$("#departamento").on("change",function(){
					$("#municipio").html("");
					$("#municipio").attr("disabled",true);
					//$("#corregimiento").html("");
					//$("#barrio").html("");
					setMunicipio($("#pais").val(), $("#departamento").val());
			});	
			//fin de Listeners de el evento Change de la lista departamentos

			//Listeners de el evento Change de las listas departamentos
			$("#departamento-editarCliente").on("change",function(){
					$("#municipio-editarCliente").html("");
					$("#municipio-editarCliente").attr("disabled",true);
					//$("#corregimiento").html("");
					//$("#barrio").html("");
					setMunicipioEditarCliente($("#pais-editarCliente").val(), $("#departamento-editarCliente").val());
			});	
			//fin de Listeners de el evento Change de la lista departamentos


			// funcion setInfoPanelDetalles(indiceArrayClientes)
			function setInfoPanelDetalles(i){
				$("#info-panel-detalles-nombres").html(clientes[i].nombre1+" "+clientes[i].apellido1+" "+clientes[i].apellido2);
				$("#info-panel-detalles-razonsocial").html(clientes[i].razonsocial+" - "+clientes[i].nit+"</h4>");
				$("#info-panel-detalles-ciudad").html(clientes[i].mun.nombre.toLowerCase()+", ");
				$("#info-panel-detalles-dpto").html(clientes[i].dpto.nombre.toLowerCase()+", ");
				$("#info-panel-detalles-pais").html(clientes[i].pais.nombre.toLowerCase());
			};


			// funcion setCorreosPanelDetalles(indiceArrayClientes)
			function setCorreosPanelDetalles(i){
				var filas="<tr class='cabecera'><td>Correos</td></tr>";

				for(j in clientes[i].correos){
					filas+="<tr class='fila'><td>"+clientes[i].correos[j]+"</td</tr>";
					//console.log(clientes[i].correos[j]);
				}
				
				$("#correo-panel table tbody").html(filas);
			};


			// funcion setCorreosPanelDetalles(indiceArrayClientes)
			function setCorreosPanelDetallesEditarCliente(i){
				var filas="<tr class='cabecera'><td colspan='2'>Correos</td><td></td></tr>";
				
				for(j in clientes[i].correos){
					filas+="<tr class='fila'><td><input type='text' class='correo-viejo' value='"+clientes[i].correos[j]+"'></td><td><a href='#' class='borrar-correo-viejo' name='"+j+"' title='Borrar correo'><span class='ion-ios-close-empty'></span></a></td></tr>";
					//console.log(clientes[i].correos[j]);
				}
				
				$("#correo-panel-form-editarCliente table tbody").html(filas);

				//Evento click de a.borrar-tel-viejo form-editarCliente
			        $("a.borrar-correo-viejo").click(function(e){
			        	e.preventDefault();

			        	//console.log("clic en borrar tel viejo");
			        	var indexCorreo = $(this).attr("name");
			        	var correo = clientes[clienteSeleccionado].correos[indexCorreo];
			        	var that = $(this);

			        	$( "#alert-eliminar-correo-editarCliente" ).dialog({
				            resizable: false,
				            modal: true,
				            autoOpen:false,
				            buttons:{
				            	Si: function(){

				            		$.ajax({
			                                url:'request/eliminarCorreo.php',
			                                type:'POST',
			                                dataType:'json',
			                                data:{idcliente:clientes[clienteSeleccionado].id, correo:correo}
			                            }).done(function(repuesta){

			                            	$("#alert-eliminar-correo-editarCliente").dialog("close");

			                            	if(repuesta[0].success){
											
												//getClientes(null);
												clientes[clienteSeleccionado].correos.splice(+indexCorreo,1);
												//console.log(clientes);
												that.parent().parent().remove();
												$("#alert p").html(repuesta[0].msg);
												$("#alert").dialog("open");
												
											}else{
												
												$("#alert p").html(repuesta[0].msg);
												$("#alert").dialog("open");
											}

			                            });
				            	},

				            	no:function(){$( this ).dialog( "close" );}
				            }
				        });

			        	$( "#alert-eliminar-correo-editarCliente" ).dialog("open");

			        	//$(this).parent().parent().remove();
			        });


			};



			// funcion setTelsPanelDetalles(indiceArrayClientes)
			function setTelsPanelDetalles(i){
				var filas="<tr class='cabecera'><td>Teléfonos</td></tr>";

				for(j in clientes[i].tels){
					filas+="<tr class='fila'><td>"+clientes[i].tels[j]+"</td</tr>";
					//console.log(clientes[i].tels[j]);
				}
				
				$("#tel-panel table tbody").html(filas);
			};

			// funcion setTelsPanelDetallesEditarCliente(indiceArrayClientes)
			function setTelsPanelDetallesEditarCliente(i){
				var filas="<tr class='cabecera'><td colspan='2'>Teléfonos</td><td></td></tr>";

				for(j in clientes[i].tels){
					filas+="<tr class='fila'><td><input type='text' class='tel-viejo' value='"+clientes[i].tels[j]+"'></td><td><a href='#' class='borrar-tel-viejo' name='"+j+"' title='Borrar teléfono'><span class='ion-ios-close-empty'></span></a></td></tr>";
					//console.log(clientes[i].tels[j]);
				}
				
				$("#tel-panel-form-editarCliente table tbody").html(filas);

					//Evento click de a.borrar-tel-viejo form-editarCliente
			        $("a.borrar-tel-viejo").click(function(e){
			        	e.preventDefault();

			        	//console.log("clic en borrar tel viejo");
			        	var indexTel = $(this).attr("name");
			        	var tel = clientes[clienteSeleccionado].tels[indexTel];
			        	var that = $(this);

			        	$( "#alert-eliminar-tel-editarCliente" ).dialog({
				            resizable: false,
				            modal: true,
				            autoOpen:false,
				            buttons:{
				            	Si: function(){

				            		$.ajax({
			                                url:'request/eliminarTel.php',
			                                type:'POST',
			                                dataType:'json',
			                                data:{idcliente:clientes[clienteSeleccionado].id, tel:tel}
			                            }).done(function(repuesta){

			                            	$("#alert-eliminar-tel-editarCliente").dialog("close");

			                            	if(repuesta[0].success){
											
												//getClientes(null);
												clientes[clienteSeleccionado].tels.splice(+indexTel,1);
												//console.log(clientes);
												that.parent().parent().remove();
												$("#alert p").html(repuesta[0].msg);
												$("#alert").dialog("open");
												
											}else{
												
												$("#alert p").html(repuesta[0].msg);
												$("#alert").dialog("open");
											}

			                            });
				            	},

				            	no:function(){$( this ).dialog( "close" );}
				            }
				        });

			        	$( "#alert-eliminar-tel-editarCliente" ).dialog("open");

			        	//$(this).parent().parent().remove();
			        });
			};




			function getClientes(val){
				//ajax getClientes
				$.ajax({
						url:'request/getClientes.php',
		                type:'POST',
		                dataType:'json',
		                data:{valor:val}
		                
					}).done(function(repuesta){

						clientes=repuesta;
						
						if(repuesta.length>0){

							var lista_contactos="";
							
							
							for(var i=0;i<repuesta.length;i++){
								lista_contactos+="<li name="+i+"><div><a href='#'><span class='foto'><span class='ion-ios-person-outline'></span></span><h5>"+repuesta[i].nombre1.toLowerCase()+" "+repuesta[i].apellido1.toLowerCase()+" "+"<span>"+repuesta[i].apellido2.toLowerCase()+"</span></h5><h6>"+repuesta[i].razonsocial.toLowerCase()+"</h6></a></div></li>";
								//console.log(repuesta[i].pais.nombre);
							}
							
							clienteSeleccionado = 0;
							$("#lista-contactos").html(lista_contactos);
							$("#lista-contactos>li:first-child").addClass("contacto-activo");
							setInfoPanelDetalles(0);
							setCorreosPanelDetalles(0);
							setTelsPanelDetalles(0);



							// Listener clic de cada contacto de la lista
							$("#lista-contactos li").click(function(e){
								
								e.preventDefault();

								$("#lista-contactos li").removeClass("contacto-activo");

								$(this).addClass("contacto-activo");

								/*var name= $(this).attr('name');

								setInfoPanelDetalles(name);
								setCorreosPanelDetalles(name);
								setTelsPanelDetalles(name);*/

								clienteSeleccionado = $(this).attr('name');

								setInfoPanelDetalles(clienteSeleccionado);
								setCorreosPanelDetalles(clienteSeleccionado);
								setTelsPanelDetalles(clienteSeleccionado);

							});

							$("#panel-detalles > div").show();
							
						}
						else{//no se encontraron clientes que coincidan con el trmino de busqueda

							$("#lista-contactos").html("<h6 style='text-align:center;'>No hay resultados</h6>");
							$("#panel-detalles > div").hide();

						}
						
				});// fin del ajax getClientes
			};







			getClientes(null);


				
			// listener del evento click del boton eliminar

			$("#eliminar-cliente").click(function(e){

				e.preventDefault();

				$("#alert-eliminar-cliente").dialog("open");


			});


			//Evento click del boton editar-cliente
			$("#editar-cliente").click(function(e){

				e.preventDefault();

				$("#txtNitId-editarCliente").val(clientes[clienteSeleccionado].nit);
				$("#txtRazonSocial-editarCliente").val(clientes[clienteSeleccionado].razonsocial);
				$("#txtNombre1-editarCliente").val(clientes[clienteSeleccionado].nombre1);
				$("#txtNombre2-editarCliente").val(clientes[clienteSeleccionado].nombre2);
				$("#txtApellido1-editarCliente").val(clientes[clienteSeleccionado].apellido1);
				$("#txtApellido2-editarCliente").val(clientes[clienteSeleccionado].apellido2);

				setPaisEditarCliente(clientes[clienteSeleccionado].pais.codigo);
				setDepartamentoEditarCliente(clientes[clienteSeleccionado].pais.codigo, clientes[clienteSeleccionado].dpto.codigo );
				setMunicipioEditarCliente(clientes[clienteSeleccionado].pais.codigo, clientes[clienteSeleccionado].dpto.codigo, clientes[clienteSeleccionado].mun.codigo );
				setCorreosPanelDetallesEditarCliente(clienteSeleccionado);
				setTelsPanelDetallesEditarCliente(clienteSeleccionado);

				$("#form-editarCliente").show();
			});

		

			//listener del evento click del boton GUARDAR

			$("#guardar-form-nuevoCliente").click(function(e){
				
				e.preventDefault();
				$("#guardar-form-nuevoCliente").hide();
				$("#cancelar-form-nuevoCliente").hide();

				
				//capturamos los telefonos agregados
				var tels_agregados = $(".nuevo-tel");
				var array_tels=[];
				var tels="";


				for(i = 0; i < tels_agregados.length; i++){
					

					if(tels_agregados[i].value != ""){
						
						array_tels.push(tels_agregados[i].value);
								
					}
					
				}
				
				
				for(var i = 0; i < array_tels.length; i++){
					
					tels+=array_tels[i];
					
					if(i < (array_tels.length-1))
							tels+="-";
				}

				//console.log(tels);
				
				//capturamos los correos agregados
				var correos_agregados = $(".nuevo-correo");
				var array_correos=[];
				var correos="";

				for(i = 0; i < correos_agregados.length; i++){
					
					if(correos_agregados[i].value != ""){

						array_correos.push(correos_agregados[i].value);

					}
							
				}


				for(var i = 0; i < array_correos.length; i++){
					
					correos+=array_correos[i];
					
					if(i < (array_correos.length-1))
							correos+="-";
				}

				//console.log(correos);


				if(validarFormulario()){
					
					$("#guardando-form-nuevoCliente").show();
					
					$.ajax({
							url:'request/guardarNuevoCliente.php',
			                type:'POST',
			                dataType:'json',
			                data:{
			                	nitId:$("#txtNitId").val().toUpperCase(),
			                	razonsocial:$("#txtRazonSocial").val().toUpperCase(),
			                	nombre1:$("#txtNombre1").val().toUpperCase(),
			                	nombre2:$("#txtNombre2").val().toUpperCase(),
			                	apellido1:$("#txtApellido1").val().toUpperCase(),
			                	apellido2:$("#txtApellido2").val().toUpperCase(),
			                	pais:$("#pais").val(),
			                	departamento:$("#departamento").val(),
			                	municipio:$("#municipio").val(),
			                	telefonos:tels,
			                	correos:correos
			                }
			                
						}).done(function(repuesta){
							
							if(repuesta[0].success){
								
								getClientes(null);
								$("#alert p").html(repuesta[0].msg);
								$("#alert").dialog("open");
								limpiarFormNuevoCliente();

							}else{
								
								$("#alert p").html(repuesta[0].msg);
								$("#alert").dialog("open");
							}

							$("#guardar-form-nuevoCliente").show();
							$("#cancelar-form-nuevoCliente").show();
							$("#guardando-form-nuevoCliente").hide();
						});

				}else{

					//alert("Debes diligenciar todos los campos");
					$("#alert p").html("Debes diligenciar todos los campos");
					$("#alert").dialog("open");
					$("#guardar-form-nuevoCliente").show();
					$("#cancelar-form-nuevoCliente").show();
					$("#guardando-form-nuevoCliente").hide();
				}
				
			});// fin del evento clic de guardar






			//listener del evento click del boton GUARDAR - editarCliente

			$("#guardar-form-editarCliente").click(function(e){
				
				e.preventDefault();
				$("#guardar-form-editarCliente").hide();
				$("#cancelar-form-editarCliente").hide();

				
				//capturamos los telefonos agregados (nuevos)
				var tels_agregados = $(".nuevo-tel-editarCliente");
				var array_tels_agregados=[];
				var telsAgregados="";


				for(i = 0; i < tels_agregados.length; i++){
					

					if(tels_agregados[i].value != ""){
						
						array_tels_agregados.push(tels_agregados[i].value);
								
					}
					
				}
				
				
				for(var i = 0; i < array_tels_agregados.length; i++){
					
					telsAgregados+=array_tels_agregados[i];
					
					if(i < (array_tels_agregados.length-1))
							telsAgregados+="-";
				}

				//console.log(tels);

				//capturamos los telefonos viejos (ya existentes)
				var tels_viejos = $(".tel-viejo");
				var array_tels_viejos=[];
				var telsViejos="";
				var array_tels_originales=[];
				var telsOriginales="";
				


				for(i = 0; i < tels_viejos.length; i++){
					

					if(tels_viejos[i].value != ""){
						
						array_tels_viejos.push(tels_viejos[i].value);
						array_tels_originales.push(clientes[clienteSeleccionado].tels[i]);
								
					}
					
				}
				
				
				for(var i = 0; i < array_tels_viejos.length; i++){
					
					telsViejos+=array_tels_viejos[i];
					telsOriginales+=array_tels_originales[i];
					
					if(i < (array_tels_viejos.length-1)){

						telsViejos+="-";
						telsOriginales+="-";
					}
							
				}

				//console.log(tels);







				
				//capturamos los correos agregados (nuevos)
				var correos_agregados = $(".nuevo-correo-editarCliente");
				var array_correos_agregados=[];
				var correosAgregados="";

				for(i = 0; i < correos_agregados.length; i++){
					
					if(correos_agregados[i].value != ""){

						array_correos_agregados.push(correos_agregados[i].value);

					}
							
				}


				for(var i = 0; i < array_correos_agregados.length; i++){
					
					correosAgregados+=array_correos_agregados[i];
					
					if(i < (array_correos_agregados.length-1))
							correosAgregados+="-";
				}

				//console.log(correos);



				//capturamos los correos viejos (existentes)
				var correos_viejos = $(".correo-viejo");
				var array_correos_viejos=[];
				var correosViejos="";
				var array_correos_originales=[];
				var correosOriginales="";

				for(i = 0; i < correos_viejos.length; i++){
					
					if(correos_viejos[i].value != ""){

						array_correos_viejos.push(correos_viejos[i].value);
						array_correos_originales.push(clientes[clienteSeleccionado].correos[i]);

					}
							
				}


				for(var i = 0; i < array_correos_viejos.length; i++){
					
					correosViejos+=array_correos_viejos[i];
					correosOriginales+=array_correos_originales[i];
					
					if(i < (array_correos_viejos.length-1)){

						correosViejos+="-";
						correosOriginales+="-";
					}
							
				}

				//console.log(correos);


				if(validarFormularioEditarCliente()){
					
					$("#guardando-form-editarCliente").show();
					
					$.ajax({
							url:'request/editarCliente.php',
			                type:'POST',
			                dataType:'json',
			                data:{
			                	idcliente:clientes[clienteSeleccionado].id,
			                	nitId:$("#txtNitId-editarCliente").val().toUpperCase(),
			                	razonsocial:$("#txtRazonSocial-editarCliente").val().toUpperCase(),
			                	nombre1:$("#txtNombre1-editarCliente").val().toUpperCase(),
			                	nombre2:$("#txtNombre2-editarCliente").val().toUpperCase(),
			                	apellido1:$("#txtApellido1-editarCliente").val().toUpperCase(),
			                	apellido2:$("#txtApellido2-editarCliente").val().toUpperCase(),
			                	pais:$("#pais-editarCliente").val(),
			                	departamento:$("#departamento-editarCliente").val(),
			                	municipio:$("#municipio-editarCliente").val(),
			                	telefonosNuevos:telsAgregados,
			                	telefonosViejos:telsViejos,
			                	telefonosOriginales:telsOriginales,
			                	correosNuevos:correosAgregados,
			                	correosViejos:correosViejos,
			                	correosOriginales:correosOriginales
			                }
			                
						}).done(function(repuesta){
							
							if(repuesta[0].success){
								
								getClientes(null);
								$("#alert p").html(repuesta[0].msg);
								$("#alert").dialog("open");
								limpiarFormEditarCliente();


							}else{
								
								$("#alert p").html(repuesta[0].msg);
								$("#alert").dialog("open");
							}

							$("#guardar-form-editarCliente").show();
							$("#cancelar-form-editarCliente").show();
							$("#guardando-form-editarCliente").hide();
							$("#form-editarCliente").hide();
						});

				}else{

					//alert("Debes diligenciar todos los campos");
					$("#alert p").html("Debes diligenciar todos los campos");
					$("#alert").dialog("open");
					$("#guardar-form-editarCliente").show();
					$("#cancelar-form-editarCliente").show();
					$("#guardando-form-editarCliente").hide();
				}
				
			});// fin del evento clic de guardar
			


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

<!-- Alert -->
	<div id="alert" title="Mensaje">
        <p>No hay datos para mostrar.</p>
    </div>


<!-- Alert-eliminar-cliente -->
<div id="alert-eliminar-cliente" title="Eliminar cliente?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>¿Seguro que desea eliminar éste cliente de forma permanente?</p>
</div>

<!-- Alert-eliminar-tel-editarCliente -->
<div id="alert-eliminar-tel-editarCliente" title="Eliminar teléfono?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>¿Seguro que desea eliminar éste teléfono de forma permanente?</p>
</div>

<!-- Alert-eliminar-correo-editarCliente -->
<div id="alert-eliminar-correo-editarCliente" title="Eliminar correo?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>¿Seguro que desea eliminar éste correo de forma permanente?</p>
</div>

	<header>
      <h3>Clientes</h3>
      <nav>
        <ul id="menu">
          
          	<li id="nuevo-cliente"><a href="" ><span class="ion-ios-personadd-outline"></span><h6>Nuevo</h6></a></li>
			<!--<li id="guardando"><a href="" ><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>-->
			<!--<li id="cancelar"><a href="" ><span class="ion-android-close"></span><h6>Cancelar</h6></a></li>       -->
            
        </ul>
      </nav>    
    </header>





    <div id='subheader'>

	    <div id="div-form">
	      
	      <form id="form">

	        <div><input type="text" id="txtbuscar" placeholder="Buscar" autofocus></div>	        
	        
	      </form>
	    
	    </div>
	    
	</div> 


	<div id="contenido">

			<div id="panel-contactos">
				<div>
					<ul id="lista-contactos">
						
						<!--<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres1 y apellidos1</h5>
								</a>								
							</div>
						</li>

						<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres2 y apellidos2</h5>
								</a>								
							</div>
						</li>

						<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres2 y apellidos2</h5>
								</a>								
							</div>
						</li>

						<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres2 y apellidos2</h5>
								</a>								
							</div>
						</li>

						<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres2 y apellidos2</h5>
								</a>								
							</div>
						</li>

						<li>
							<div>
								<a href="#">
									<img src="">
									<h5>Nombres2 y apellidos2</h5>
								</a>								
							</div>
						</li>-->

					</ul>
				</div>
			</div>

			<div id="panel-detalles">				

				<div style="display:none">

					<div id="nav-panel-detalles">
						<ul id="menu-panel-detalles">						
							
							<li id="editar-cliente"><a href="" ><span class="ion-ios-compose-outline"></span><h6>Editar</h6></a></li>
							<li id="eliminar-cliente"><a href="" ><span class="ion-ios-trash-outline"></span><h6>Eliminar</h6></a></li>
							<!--<li id="cerrar"><a href="" ><span class="ion-android-close"></span><h6>Cerrar</h6></a></li>-->
						</ul>
					</div>


					<div id="info-panel-detalles">
						<!--<img src="">-->
						<span class='foto'><span class='ion-ios-person-outline'></span></span>
						<h2 id="info-panel-detalles-nombres"><!--Elkin Barreto Contreras--></h2>
						<h4 id="info-panel-detalles-razonsocial"><!--Razón social--></h4>
						<h6><span id="info-panel-detalles-ciudad"></span><span id="info-panel-detalles-dpto"></span><span id="info-panel-detalles-pais"></span></h6>
					</div>

					<div id="tel-correo-panel-detalles">
						
						<div id="tel-panel">
							<div>
								<table>
									
									<tbody>
										<!--<tr class="cabecera"><td>Teléfonos</td></tr>
										<tr class="fila"><td>3003369761</td></tr>
										<tr class="fila"><td>3003369761</td></tr>
										<tr class="fila"><td>3003369761</td></tr>-->
									</tbody>
									
								</table>
							</div>
						</div>

						<div id="correo-panel">

							<div>
								<table>
									
									<tbody>
										<!--<tr class="cabecera"><td>Correos</td></tr>
										<tr class="fila"><td>nikle28@hotmail.com</td></tr>
										<tr class="fila"><td>lumabaco@hotmail.com</td></tr>
										<tr class="fila"><td>nikle2828@gmail.com</td></tr>-->
									</tbody>
									
								</table>
							</div>
						</div>

					</div>

				</div>
			</div>

	</div>


	<!-- Form Nuevo cliente -->

	<div id="form-nuevoCliente">
		
		<div class="header-form">
			
			<h3>Nuevo cliente</h3>

			<nav>
		        <ul class="menu">
		          
		          	<li id="guardar-form-nuevoCliente"><a href="" ><span class="ion-android-done"></span><h6>Guardar</h6></a></li>
		          	<li id="guardando-form-nuevoCliente"><a href="" ><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>
					<li id="cancelar-form-nuevoCliente"><a href="" ><span class="ion-android-close"></span><h6>Cerrar</h6></a></li>
		            
		        </ul>
	      	</nav> 
		</div>
		
		<div class="contenido-form">
			
			<div>

				<div>
					<label>Nit / identificación</label>
					<input type="text" id="txtNitId">
				</div>

				<div>
					<label>Razón social</label>
					<input type="text" id="txtRazonSocial">
				</div>

				<div>
					<label>Nombre1</label>
					<input type="text" id="txtNombre1">
				</div>

				<div>
					<label>Nombre2</label>
					<input type="text" id="txtNombre2">
				</div>

				<div>
					<label>Apellido1</label>
					<input type="text" id="txtApellido1">
				</div>

				<div>
					<label>Apellido2</label>
					<input type="text" id="txtApellido2">
				</div>

				<div>
					<label>País</label>
					<select id="pais"></select>
				</div>

				<div>
					<label>Departamento</label>
					<select id="departamento"></select>
				</div>

				<div>
					<label>Municipio</label>
					<select id="municipio"></select>
				</div>

				<div id="tel-correo-panel-form-nuevoCliente">
						
						<div id="tel-panel-form-nuevoCliente">
							<div>
								
								<table>
									
									<tbody>
										
										<tr class="cabecera"><td colspan="2">Teléfonos</td></tr>
										
										<!--<tr class="fila">
											<td><input type="text" class="nuevo-tel" value="3003369761"></td>
											<td><a href="#" class="borrar-tel" title="Borrar teléfono"><span class="ion-ios-close-empty"></span></a></td>
										</tr>

										<tr class="fila">
											<td><input type="text" class="nuevo-tel" value="3003369761"></td>
											<td><a href="#" class="borrar-tel" title="Borrar teléfono"><span class="ion-ios-close-empty"></span></a></td>
										</tr>-->
										<!--<tr class="fila"><td>3003369761</td></tr>
										<tr class="fila"><td>3003369761</td></tr>-->
									</tbody>
									
								</table>

								<a href="#" id="agregar-tel"><span class="ion-ios-plus-empty"></span><span>Agregar teléfono</span></a>

							</div>

						</div>

						<div id="correo-panel-form-nuevoCliente">

							<div>
								<table>
									
									<tbody>
										<tr class="cabecera"><td colspan="2">Correos</td></tr>
										<!--<tr class="fila">
											<td>nikle28@hotmail.com</td>
											<td><a href="#" class="borrar-correo" title="Borrar correo"><span class="ion-ios-close-empty"></span></a></td>
										</tr>-->
										<!--<tr class="fila"><td>lumabaco@hotmail.com</td></tr>
										<tr class="fila"><td>nikle2828@gmail.com</td></tr>-->
									</tbody>
									
								</table>

								<a href="#" id="agregar-correo"><span class="ion-ios-plus-empty"></span><span>Agregar correo</span></a>

							</div>
						</div>

				</div>

			</div>
		</div>
	</div>



	<!-- Form Editar cliente -->

	<div id="form-editarCliente">
		
		<div class="header-form">
			
			<h3>Editar cliente</h3>

			<nav>
		        <ul class="menu">
		          
		          	<li id="guardar-form-editarCliente"><a href="" ><span class="ion-android-done"></span><h6>Guardar</h6></a></li>
		          	<li id="guardando-form-editarCliente"><a href="" ><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>
					<li id="cancelar-form-editarCliente"><a href="" ><span class="ion-android-close"></span><h6>Cerrar</h6></a></li>
		            
		        </ul>
	      	</nav> 
		</div>
		
		<div class="contenido-form">
			
			<div>

				<div>
					<label>Nit / identificación</label>
					<input type="text" id="txtNitId-editarCliente">
				</div>

				<div>
					<label>Razón social</label>
					<input type="text" id="txtRazonSocial-editarCliente">
				</div>

				<div>
					<label>Nombre1</label>
					<input type="text" id="txtNombre1-editarCliente">
				</div>

				<div>
					<label>Nombre2</label>
					<input type="text" id="txtNombre2-editarCliente">
				</div>

				<div>
					<label>Apellido1</label>
					<input type="text" id="txtApellido1-editarCliente">
				</div>

				<div>
					<label>Apellido2</label>
					<input type="text" id="txtApellido2-editarCliente">
				</div>

				<div>
					<label>País</label>
					<select id="pais-editarCliente"></select>
				</div>

				<div>
					<label>Departamento</label>
					<select id="departamento-editarCliente"></select>
				</div>

				<div>
					<label>Municipio</label>
					<select id="municipio-editarCliente"></select>
				</div>

				<div id="tel-correo-panel-form-editarCliente">
						
						<div id="tel-panel-form-editarCliente">
							<div>
								
								<table>
									
									<tbody>
										
										<tr class="cabecera"><td colspan="2">Teléfonos</td></tr>
										
										<!--<tr class="fila">
											<td><input type="text" class="nuevo-tel" value="3003369761"></td>
											<td><a href="#" class="borrar-tel" title="Borrar teléfono"><span class="ion-ios-close-empty"></span></a></td>
										</tr>

										<tr class="fila">
											<td><input type="text" class="nuevo-tel" value="3003369761"></td>
											<td><a href="#" class="borrar-tel" title="Borrar teléfono"><span class="ion-ios-close-empty"></span></a></td>
										</tr>-->
										<!--<tr class="fila"><td>3003369761</td></tr>
										<tr class="fila"><td>3003369761</td></tr>-->
									</tbody>
									
								</table>

								<a href="#" id="agregar-tel-editarCliente"><span class="ion-ios-plus-empty"></span><span>Agregar teléfono</span></a>

							</div>

						</div>

						<div id="correo-panel-form-editarCliente">

							<div>
								<table>
									
									<tbody>
										<tr class="cabecera"><td colspan="2">Correos</td></tr>
										<!--<tr class="fila">
											<td>nikle28@hotmail.com</td>
											<td><a href="#" class="borrar-correo" title="Borrar correo"><span class="ion-ios-close-empty"></span></a></td>
										</tr>-->
										<!--<tr class="fila"><td>lumabaco@hotmail.com</td></tr>
										<tr class="fila"><td>nikle2828@gmail.com</td></tr>-->
									</tbody>
									
								</table>

								<a href="#" id="agregar-correo-editarCliente"><span class="ion-ios-plus-empty"></span><span>Agregar correo</span></a>

							</div>
						</div>

				</div>

			</div>
		</div>
	</div>
	


</body>
</html>