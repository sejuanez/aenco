<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	error_reporting(0);	
	include("../../../init/gestion.php");



	$idCliente = $_POST["idcliente"];
	$huboError = false;		
	$return_arr = array();

	




	//eliminamos los telefonos asociados al cliente
	$consulta = "DELETE FROM ch_telefonos WHERE cht_id='".$idcliente."'";

	$result = ibase_query($conexion,$consulta);

	if(!$result){

		$huboError=true;
		$row_array['success'] = false;
		$row_array['msg'] = "Error al eliminar los teléfonos del cliente: ".ibase_errmsg();
		array_push($return_arr, $row_array);
		echo json_encode($return_arr);
	}
	else{

		//eliminamos los correos asociados al cliente
		$consulta = "DELETE FROM ch_correos WHERE chc_id='".$idcliente."'";
		$result = ibase_query($conexion,$consulta);

		if(!result){
			$huboError=true;
			$row_array['success'] = false;
			$row_array['msg'] = "Error al eliminar los correos del cliente: ".ibase_errmsg();
			array_push($return_arr, $row_array);
			echo json_encode($return_arr);
		}
		else{

			//eliminamos el cliente
			$consulta = "DELETE FROM ch_clientes WHERE chc_id=".$idcliente;
			$result = ibase_query($conexion,$consulta);

			if(!result){
				$huboError=true;
				$row_array['success'] = false;
				$row_array['msg'] = "Error al eliminar el cliente: ".ibase_errmsg();
				array_push($return_arr, $row_array);
				echo json_encode($return_arr);
			}
		}
	}



	if(!$huboError){
		$return_arr = array();
		$row_array['success'] = true;
		$row_array['msg'] = "Cliente eliminado con éxito";

		array_push($return_arr, $row_array);

		echo json_encode($return_arr);
	}

?>