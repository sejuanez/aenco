<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];
	
	
	$consulta = "SELECT p.cp_codigo, p.cp_nombre from ch_pais p where p.cp_activo='S' order by p.cp_nombre";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		
		$row_array['codigoPais'] = utf8_encode($fila[0]);
		$row_array['nombrePais'] = utf8_encode($fila[1]);
						
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>