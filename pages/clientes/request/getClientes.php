<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");

	
	function getPais($codPais, $conexion){

		$nombrePais=null;

		$consulta = "SELECT cp_nombre from ch_pais WHERE cp_codigo='".$codPais."'";
		
		$result = ibase_query($conexion,$consulta);
		
		while($fila = ibase_fetch_row($result)){
			
			$nombrePais = $fila[0];
		}
		
		return array('codigo' => $codPais, 'nombre' => utf8_encode($nombrePais));
	};






	function getDpto($codPais, $codDpto, $conexion){

		$nombreDpto=null;

		$consulta = "SELECT cdp_nombre from ch_departamentos WHERE cdp_cod_pais='".$codPais."' and cdp_cod_dpto='".$codDpto."'";
		
		$result = ibase_query($conexion,$consulta);
		
		while($fila = ibase_fetch_row($result)){
			
			$nombreDpto = $fila[0];
		}
		
		return array('codigo' => $codDpto, 'nombre' => utf8_encode($nombreDpto));
	};







	function getMun($codPais, $codDpto, $codMun, $conexion){

		$nombreMun=null;

		$consulta = "SELECT cm_nombrempio from ch_municipios WHERE cm_codpais='".$codPais."' and cm_coddepto='".$codDpto."' and cm_codmpio='".$codMun."'";
		
		$result = ibase_query($conexion,$consulta);
		
		while($fila = ibase_fetch_row($result)){
			
			$nombreMun = $fila[0];
		}
		
		return array('codigo' => $codMun, 'nombre' => utf8_encode($nombreMun));
	};







	function getCorreos($idCliente, $conexion){

		//$correo=null;
		$correo=array();

		$consulta = "SELECT chc_correo from ch_correos WHERE chc_id='".$idCliente."'";
		
		$result = ibase_query($conexion,$consulta);
		
		while($fila = ibase_fetch_row($result)){
			
			//$correo = $fila[0];
			array_push($correo, $fila[0]);
		}
		
		return $correo;
	};







	function getTels($idCliente, $conexion){

		$telefono=array();

		$consulta = "SELECT cht_telefono from ch_telefonos WHERE cht_id='".$idCliente."'";
		
		$result = ibase_query($conexion,$consulta);
		
		while($fila = ibase_fetch_row($result)){
			
			//$telefono[] = $fila[0];
			array_push($telefono, $fila[0]);
		}
		
		return $telefono;//array('telefono' => utf8_encode($telefono));
	};



	$val = $_POST["valor"];

	if($val==null)
		$consulta = "SELECT * from ch_clientes";
	else
		$consulta = "SELECT DISTINCT * FROM ch_clientes WHERE (chc_cedulanit LIKE '%".$val."%') OR (lower(chc_razonsocial) LIKE lower('%".$val."%')) OR (lower(chc_nombre1) LIKE lower('%".$val."%')) OR (lower(chc_nombre2) LIKE lower('%".$val."%')) OR (lower(chc_apellido1) LIKE lower('%".$val."%')) OR (lower(chc_apellido2) LIKE lower('%".$val."%')) ORDER BY chc_nombre1 ASC";

	
	//$consulta = "SELECT * from ch_clientes";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['id'] = utf8_encode($fila[0]);
		$row_array['nit'] = utf8_encode($fila[1]);
		$row_array['razonsocial'] = utf8_encode($fila[2]);
		$row_array['nombre1'] = utf8_encode($fila[3]);
		$row_array['nombre2'] = utf8_encode($fila[4]);
		$row_array['apellido1'] = utf8_encode($fila[5]);
		$row_array['apellido2'] = utf8_encode($fila[6]);
		$row_array['pais'] = getPais($fila[7], $conexion);//utf8_encode($fila[7]);
		$row_array['dpto'] = getDpto($fila[7], $fila[8], $conexion);
		$row_array['mun'] = getMun($fila[7], $fila[8], $fila[9], $conexion);
		$row_array['correos'] = getCorreos($fila[0], $conexion);
		$row_array['tels'] = getTels($fila[0], $conexion);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>