<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	error_reporting(0);

	include("../../../init/gestion.php");

	

	$idcliente = $_POST["idcliente"];
	$nitId = $_POST["nitId"];
	$razonsocial = $_POST["razonsocial"];
	$apellido1 = $_POST["apellido1"];
	$apellido2 = $_POST["apellido2"];
	$nombre1 = $_POST["nombre1"];
	$nombre2 = $_POST["nombre2"];
	$pais = $_POST["pais"];
	$departamento = $_POST["departamento"];
	$municipio = $_POST["municipio"];

	$telefonosNuevos=null;
	$telefonosViejos=null;
	$telefonosOriginales=null;

	$correosNuevos = null;	
	$correosViejos = null;
	$correosOriginales = null;



	if($_POST["telefonosNuevos"]!="")
		$telefonosNuevos = explode("-", $_POST["telefonosNuevos"]);

	if($_POST["telefonosViejos"]!="")
		$telefonosViejos = explode("-", $_POST["telefonosViejos"]);

	if($_POST["telefonosOriginales"]!="")
		$telefonosOriginales = explode("-", $_POST["telefonosOriginales"]);
	


	if($_POST["correosNuevos"])
		$correosNuevos = explode("-", $_POST["correosNuevos"]);

	if($_POST["correosViejos"])
		$correosViejos = explode("-", $_POST["correosViejos"]);

	if($_POST["correosOriginales"])
		$correosOriginales = explode("-", $_POST["correosOriginales"]);

	

	$usuario = $_SESSION['user'];
	$huboError = false;
	

					
	// guardamos los telefonos Nuevos
	if($telefonosNuevos!=null){
		$count_tel_nuevos = count($telefonosNuevos);

		for($i=0; $i< $count_tel_nuevos; $i++){
			
			
				$consulta = "INSERT INTO ch_telefonos (CHT_TIPO, CHT_ID, CHT_TELEFONO) values ('CLIENTE', '".$idcliente."', '".$telefonosNuevos[$i]."')";
				$result=ibase_query($conexion,$consulta);

				if(!$result){// si hubo error al guardar el telefono
					$huboError=true;
					$return_arr = array();
					$row_array['success'] = false;
					$row_array['msg'] = "Error al guardar teléfono: ".ibase_errmsg();
					array_push($return_arr, $row_array);
					echo json_encode($return_arr);
				}

		}//fin del for que recorre el array telefono
	}//fin del if telefonos


	// actualizamos los telefonos viejos
	if($telefonosViejos!=null){
		$count_tel_viejos = count($telefonosViejos);

		for($i=0; $i< $count_tel_viejos; $i++){
			
			
				$consulta = "UPDATE ch_telefonos SET cht_telefono='".$telefonosViejos[$i]."' WHERE cht_id='".$idcliente."' and cht_telefono='".$telefonosOriginales[$i]."'";
				$result=ibase_query($conexion,$consulta);

				if(!$result){// si hubo error al actualizar el telefono
					$huboError=true;
					$return_arr = array();
					$row_array['success'] = false;
					$row_array['msg'] = "Error al actualizar teléfono: ".ibase_errmsg();
					array_push($return_arr, $row_array);
					echo json_encode($return_arr);
				}

		}//fin del for que recorre el array telefono
	}//fin del if telefonos

	
	// guardamos los correos Nuevos
	if($correosNuevos!=null){
		$count_correo_nuevos = count($correosNuevos);

		for($i=0; $i<$count_correo_nuevos ; $i++){

			
				$consulta = "INSERT INTO ch_correos (CHC_TIPO, CHC_ID, CHC_CORREO) values ('CLIENTE', '".$idcliente."', '".$correosNuevos[$i]."')";
				$result=ibase_query($conexion,$consulta);

			if(!$result) {//si hubo error al guardar un correo
				$huboError=true;
				$return_arr = array();
				$row_array['success'] = false;
				$row_array['msg'] = "Error al guardar correo: ".ibase_errmsg();
				array_push($return_arr, $row_array);
				echo json_encode($return_arr);
			}			
		}
	}// fin del if correos nuevos


	// actualizamos los correos viejos
	if($correosViejos!=null){
		$count_correos_viejos = count($correosViejos);

		for($i=0; $i< $count_correos_viejos; $i++){
			
			
				$consulta = "UPDATE ch_correos SET chc_correo='".$correosViejos[$i]."' WHERE chc_id='".$idcliente."' and chc_correo='".$correosOriginales[$i]."'";
				$result=ibase_query($conexion,$consulta);

				if(!$result){// si hubo error al actualizar el telefono
					$huboError=true;
					$return_arr = array();
					$row_array['success'] = false;
					$row_array['msg'] = "Error al actualizar correo: ".ibase_errmsg();
					array_push($return_arr, $row_array);
					echo json_encode($return_arr);
				}

		}//fin del for que recorre el array telefono
	}//fin del if telefonos


//actualizamos el cliente en la tabla
	$consulta= "UPDATE ch_clientes SET chc_cedulanit='".$nitId."', chc_razonsocial='".$razonsocial."', chc_nombre1='".$nombre1."', chc_nombre2='".$nombre2."', chc_apellido1='".$apellido1."', chc_apellido2='".$apellido2."', chc_pais='".$pais."', chc_dpto='".$departamento."', chc_mpio='".$municipio."' WHERE chc_id=".$idcliente;

	$result = ibase_query($conexion,$consulta);
	//guardamos el cliente
	
	if(!$result){// si hubo error al insertar el cliente

		$huboError=true;
		$return_arr = array();
		$row_array['success'] = false;
		$row_array['msg'] = "Error al actualizar el cliente: ".ibase_errmsg();
		array_push($return_arr, $row_array);
		echo json_encode($return_arr);
		
	}
	
			
		
		

		

	if(!$huboError){
		$return_arr = array();
		$row_array['success'] = true;
		$row_array['msg'] = "Cliente actualizado con éxito";

		array_push($return_arr, $row_array);

		echo json_encode($return_arr);
	}
	

?>