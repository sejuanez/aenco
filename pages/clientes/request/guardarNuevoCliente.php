<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	error_reporting(0);

	include("../../../init/gestion.php");

	

	$nitId = $_POST["nitId"];
	$razonsocial = $_POST["razonsocial"];
	$apellido1 = $_POST["apellido1"];
	$apellido2 = $_POST["apellido2"];
	$nombre1 = $_POST["nombre1"];
	$nombre2 = $_POST["nombre2"];
	$pais = $_POST["pais"];
	$departamento = $_POST["departamento"];
	$municipio = $_POST["municipio"];

	$telefonos=null;
	$correos = null;

	if($_POST["telefonos"]!="")
		$telefonos = explode("-", $_POST["telefonos"]);
	


	if($_POST["correos"])
		$correos = explode("-", $_POST["correos"]);

	

	$usuario = $_SESSION['user'];
	$huboError = false;
	$clienteId = null;

	// obtenemos el id del cliente
	

		$consulta= "SELECT GEN_ID(ch_clientes, 1) FROM RDB\$DATABASE";

		$result = ibase_query($conexion,$consulta);

		if(!$result){
			$huboError=true;
			$return_arr = array();
			$row_array['success'] = false;
			$row_array['msg'] = "Error al obtener el código del cliente: ".ibase_errmsg();
			array_push($return_arr, $row_array);
			echo json_encode($return_arr);
		}
		else{//si no hay error al generar el id de un cliente
			
			$fila = ibase_fetch_row($result);
			$clienteId = $fila[0];


			//insertamos el cliente en la tabla
			$consulta= "INSERT INTO ch_clientes (chc_id, chc_cedulanit, chc_razonsocial, chc_nombre1, chc_nombre2, chc_apellido1, chc_apellido2, chc_pais, chc_dpto, chc_mpio, chc_usuarioregistra) VALUES (".$clienteId.", '".$nitId."', '".$razonsocial."', '".$nombre1."', '".$nombre2."', '".$apellido1."', '".$apellido2."', '".$pais."', '".$departamento."', '".$municipio."', '".$usuario."' )";
			$result = ibase_query($conexion,$consulta);
			//guardamos el cliente
			
			if(!$result){// si hubo error al insertar el cliente

				$huboError=true;
				$return_arr = array();
				$row_array['success'] = false;
				$row_array['msg'] = "Error al guardar el cliente: ".ibase_errmsg();
				array_push($return_arr, $row_array);
				echo json_encode($return_arr);
				
			}else{//si no hubo error al insertar el cliente
				
					// guardamos los telefonos
					if($telefonos!=null){
						$count_tel = count($telefonos);

						for($i=0; $i< $count_tel; $i++){
							
							
								$consulta = "INSERT INTO ch_telefonos (CHT_TIPO, CHT_ID, CHT_TELEFONO) values ('CLIENTE', '".$clienteId."', '".$telefonos[$i]."')";
								$result=ibase_query($conexion,$consulta);

								if(!$result){// si hubo error al guardar el telefono
									$huboError=true;
									$return_arr = array();
									$row_array['success'] = false;
									$row_array['msg'] = "Error al guardar teléfono: ".ibase_errmsg();
									array_push($return_arr, $row_array);
									echo json_encode($return_arr);
								}

						}//fin del for que recorre el array telefono
					}//fin del if telefonos


					// guardamos los correos
					if($correos!=null){
						$count_correo = count($correos);

						for($i=0; $i<$count_correo ; $i++){

							
								$consulta = "INSERT INTO ch_correos (CHC_TIPO, CHC_ID, CHC_CORREO) values ('CLIENTE', '".$clienteId."', '".$correos[$i]."')";
								$result=ibase_query($conexion,$consulta);

							if(!$result) {//si hubo error al guardar un correo
								$huboError=true;
								$return_arr = array();
								$row_array['success'] = false;
								$row_array['msg'] = "Error al guardar correo: ".ibase_errmsg();
								array_push($return_arr, $row_array);
								echo json_encode($return_arr);
							}			
						}
					}


			}//fin si no hubo error al insertar el cliente
		}// fin si no hubo error al generar el id de un cliente
		

		

	if(!$huboError){
		$return_arr = array();
		$row_array['success'] = true;
		$row_array['msg'] = "Cliente guardado con éxito";

		array_push($return_arr, $row_array);

		echo json_encode($return_arr);
	}
	

?>