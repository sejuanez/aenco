<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador


        echo
        "<script>
            window.location.href='../inicio/index.php';
            
        </script>";
        exit();
    } else {


    }
?>

<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>

    <style type="text/css">

        textarea {
            resize: none
        }

        input[type="checkbox"] {
            vertical-align: middle;
        }

        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        table {
            font-size: .8em;
        }


    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Proveedores

            <span id="btnExportar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="exportar()">
            	<i class="fa fa-download" aria-hidden="true"></i> Exportar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="eliminar()">
            	<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
            </span>


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="nuevo()">
            	<i class="fa fa-edit" aria-hidden="true"></i> Nuevo
            </span>

            <span id="btnActualizar" class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="actualizar()">
            	<i class="fa fa-pencil" aria-hidden="true"></i> Actualizar
            </span>

            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="guardar()">
            	<i class="fa fa-save" aria-hidden="true"></i> Guardar
            </span>


        </p>
    </header>


    <div class="container">

        <form class="formGuardar">

            <div class="row align-items-center">

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="codigoProveedor">Codigo/Nit</label>
                        <input type="text" id="codigoProveedor" name="codigoProveedor" class="form-control" required
                               aria-label="codigoProveedor" v-model="codigoProveedor" maxlength="40"
                               v-if="inputCodigo"
                               autocomplete="nope"
                               v-on:keyup.13="loadProveedores(codigoProveedor)"
                               @blur="loadProveedores(codigoProveedor)">
                    </div>
                </div>

                <div class="col-5">
                    <div class="form-group input-group-sm">
                        <label for="nombresProveedor">Nombres</label>
                        <input type="text" id="nombresProveedor" name="nombresProveedor" class="form-control" required
                               autocomplete="nope"
                               aria-label="nombresProveedor" v-model="nombresProveedor"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="selectProveedor">Proveedores Registrados</label>

                        <select class="selectProveedor" id="selectProveedor" name="selectProveedor"
                                v-model="selectedProveedor">
                            <option value="">Seleccione...</option>
                            <option v-for="selectProveedor in proveedores" :value="selectProveedor.codigo">
                                {{selectProveedor.nombres}}
                            </option>
                        </select>

                    </div>
                </div>


            </div>

            <div class="row align-items-center">

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="razonSocial">Razon Social</label>
                        <input type="text" id="razonSocial" name="razonSocial" class="form-control" required
                               aria-label="razonSocial" v-model="razonSocial"
                               autocomplete="nope"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="dirProveedor">Direccion</label>
                        <input type="text" id="dirProveedor" name="dirProveedor" class="form-control" required
                               aria-label="dirProveedor" v-model="dirProveedor"
                               autocomplete="nope"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group input-group-sm">
                        <label for="telProveedor">Telefono</label>
                        <input type="text" id="telProveedor" name="telProveedor" class="form-control" required
                               autocomplete="nope"
                               aria-label="telProveedor" v-model="telProveedor"
                               maxlength="40">
                    </div>
                </div>


            </div>

            <div class="row align-items-center">


                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="selectDepartamento">Departamento</label>

                        <select class="selectDepartamento" id="selectDepartamento" name="selectDepartamento" required
                                v-model="selectedDepartamento">
                            <option value="">Seleccione...</option>
                            <option v-for="selectDepartamento in selectDepartamentos"
                                    :value="selectDepartamento.CODIGO">
                                {{selectDepartamento.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="selectMunicipio">Ciudad</label>

                        <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio" required
                                v-model="selectedMunicipio">
                            <option value="">Seleccione...</option>
                            <option v-for="selectMunicipio in selectMunicipios"
                                    :value="selectMunicipio.CODIGO">
                                {{selectMunicipio.NOMBRE}}
                            </option>
                        </select>

                    </div>
                </div>


                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="correoProveedor">E-Mail</label>
                        <input type="text" id="correoProveedor" name="correoProveedor" class="form-control" required
                               aria-label="correoProveedor"
                               autocomplete="nope"
                               v-model="correo"
                               maxlength="40">
                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group input-group-sm">
                        <label for="fechaCreacion">Fecha Creacion </label>
                        <input type="date" id="fechaCreacion" name="fechaCreacion" class="form-control" disabled
                               aria-label="fechaCreacion"
                               v-model="fechaCreacion"
                               :value="fechaCreacion">
                    </div>
                </div>


            </div>


        </form>

    </div>
    <br>

    <div id="datos" style="display: none">
        <div class="row">

            <div class="col-12">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <td>Codigo</td>
                        <td>Nombres</td>
                        <td>Razon Social</td>
                        <td>Direccion</td>
                        <td>Telefono</td>
                        <td>Email</td>
                        <td>Fecha Registro</td>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos" style="font-size: .8em">

                    <tr v-for="(dato, index) in proveedores">
                        <td v-text="dato.codigo"></td>
                        <td v-text="dato.nombres"></td>
                        <td v-text="dato.razonSocial"></td>
                        <td v-text="dato.direccion"></td>
                        <td v-text="dato.telefono"></td>
                        <td v-text="dato.correo"></td>
                        <td v-text="dato.fecha"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script lang="javascript" src="js/xlsx.full.min.js"></script>
<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>
<script src="https://terrylinooo.github.io/jquery.disableAutoFill/assets/js/jquery.disableAutoFill.min.js"></script>


</body>
</html>
