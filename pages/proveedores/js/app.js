//=================================================
//=================================================

jQuery(document).ready(function ($) {

    $(".selectProveedor").select2().change(function (e) {
        var codProveedor = $(this).val();
        var selectedIndex = document.getElementById("selectProveedor").selectedIndex
        app.onChangeProveedor(codProveedor, selectedIndex);
    });

    $(".selectDepartamento").select2().change(function (e) {
        var codDep = $(this).val();
        app.onChangeDepartamento(codDep);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var codMunicipio = $(this).val();
        var selectedIndex = document.getElementById("selectMunicipio").selectedIndex
        app.onChangeMunicipio(codMunicipio, selectedIndex);
    });


});

//=================================================
//=================================================


let app = new Vue({
        el: '#app',
        data: {

            proveedores: [],
            selectDepartamentos: [],
            selectMunicipios: [],

            codigoProveedor: "",
            nombresProveedor: "",
            selectedProveedor: "",
            razonSocial: "",
            dirProveedor: "",
            telProveedor: "",
            selectedDepartamento: "",
            selectedMunicipio: "",
            correo: "",
            fechaCreacion: "",

            inputCodigo: true,


        },

        methods: {


            loadFecha: function () {

                var today = new Date();
                this.fechaCreacion = (getFormattedDate(today));
                this.fechaModificacion = (getFormattedDate(today));


                // Get date formatted as YYYY-MM-DD
                function getFormattedDate(date) {
                    return date.getFullYear() +
                        "-" +
                        ("0" + (date.getMonth() + 1)).slice(-2) +
                        "-" +
                        ("0" + date.getDate()).slice(-2);
                }

            },

            loadDepartamentos: function () {

                $.get('./request/getDepartamentos.php', function (data) {
                    var data = JSON.parse(data);

                    app.selectDepartamentos = data

                });

            },

            onChangeDepartamento: function (cod) {

                var app = this;

                app.selectedDepartamento = cod;
                app.selectMunicipios = []

                $.get('./request/getMunicipios.php?codigo=' + cod + "", function (data) {
                    var data = JSON.parse(data);


                    app.selectMunicipios = data


                    $('#selectMunicipio').select2().val(app.selectedMunicipio).trigger('change')

                });


            },

            onChangeMunicipio: function (cod, index) {

                var app = this

                app.selectedMunicipio = cod


            },

            loadProveedores: function (codProveedor) {
                var app = this;

                $('#fechaCreacion').prop('disabled', true)

                app.codigoProveedor = codProveedor

                if (codProveedor != null) {

                    console.log(codProveedor)

                    $.get('./request/getProveedores.php?opcion=1&codigo=' + codProveedor + "", function (data) {

                        var data = JSON.parse(data)

                        try {


                            $('#selectDepartamento').select2().val(data[0].departamento).trigger('change')
                            app.selectedDepartamento = data[0].departamento

                            $.get('./request/getMunicipios.php?opcion=2&codigo=' + data[0].departamento + "", function (data2) {
                                var data2 = JSON.parse(data2);
                                app.selectMunicipios = []
                                app.selectMunicipios = data2

                                $('#selectMunicipio').select2().val(data[0].ciudad).trigger('change')

                            });


                            app.nombresProveedor = data[0].nombres
                            app.razonSocial = data[0].razonSocial
                            app.dirProveedor = data[0].direccion
                            app.telProveedor = data[0].telefono
                            app.correo = data[0].correo
                            app.fechaCreacion = data[0].fecha

                            if (app.fechaCreacion == "") {
                                $('#fechaCreacion').prop('disabled', false)
                            }


                        } catch (e) {

                        }

                    });

                } else {

                    $.get('./request/getProveedores.php?opcion=todos&codigo=1', function (data) {
                        var data = JSON.parse(data);

                        app.proveedores = data

                    });
                }
            },

            onChangeProveedor: function (cod, index) {

                $('#fechaCreacion').prop('disabled', true)

                var app = this;

                try {


                    $('#selectDepartamento').select2().val(app.proveedores[index - 1].departamento).trigger('change')
                    app.selectedDepartamento = app.proveedores[index - 1].departamento


                    $.get('./request/getMunicipios.php?opcion=1&codigo=' + app.selectedDepartamento + "", function (data) {
                        var data = JSON.parse(data);
                        app.selectMunicipios = []
                        app.selectMunicipios = data

                        $('#selectMunicipio').select2().val(app.proveedores[index - 1].ciudad).trigger('change')

                    });

                    $("#codigoProveedor").prop("disabled", true);
                    app.codigoProveedor = app.proveedores[index - 1].codigo
                    app.nombresProveedor = app.proveedores[index - 1].nombres
                    app.razonSocial = app.proveedores[index - 1].razonSocial
                    app.dirProveedor = app.proveedores[index - 1].direccion
                    app.telProveedor = app.proveedores[index - 1].telefono
                    app.correo = app.proveedores[index - 1].correo
                    app.fechaCreacion = app.proveedores[index - 1].fecha

                    if (app.fechaCreacion == "") {
                        $('#fechaCreacion').prop('disabled', false)
                    }


                } catch (e) {

                }

            },

            guardar: function () {

                var app = this;


                if (app.codigoProveedor == "" ||
                    app.nombresProveedor == "" ||
                    app.razonSocial == "" ||
                    app.dirProveedor == "" ||
                    app.telProveedor == "" ||
                    app.selectedDepartamento == "" ||
                    app.selectedMunicipio == "" ||
                    app.correo == "" ||
                    app.fechaCreacion == ""

                ) {


                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {


                    app.loadFecha();


                    var formData = new FormData($('.formGuardar')[0]);
                    formData.append("codigoProveedor2", app.codigoProveedor)
                    formData.append("selectedDepartamento", app.selectedDepartamento)
                    formData.append("selectedMunicipio", app.selectedMunicipio)
                    formData.append("fechaCreacion", app.fechaCreacion)


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertProveedor.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: formData,

                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
                        //mientras enviamos el archivo
                        beforeSend: function () {
                        },
                        success: function (data) {

                            console.log(data)

                            if (data.indexOf("UNIQUE KEY constraint") > -1) {

                                alertify.warning("Existe un material con este codigoProveedor");


                            }


                            //app.search();


                            if (data == 1) {

                                alertify.success("Registro guardado Exitosamente");
                                app.nuevo();
                                app.loadProveedores()

                            }

                        },
                        //si ha ocurrido un error
                        error: function (FormData) {
                            console.log(data)
                        }
                    });


                }

            },

            actualizar: function () {

                var app = this;


                if (app.codigoProveedor == "" ||
                    app.nombresProveedor == "" ||
                    app.razonSocial == "" ||
                    app.dirProveedor == "" ||
                    app.telProveedor == "" ||
                    app.selectedDepartamento == "" ||
                    app.selectedMunicipio == "" ||
                    app.correo == "" ||
                    app.fechaCreacion == ""

                ) {


                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {


                    app.loadFecha();


                    var formData = new FormData($('.formGuardar')[0]);
                    formData.append("codigoProveedor2", app.codigoProveedor)
                    formData.append("selectedDepartamento", app.selectedDepartamento)
                    formData.append("selectedMunicipio", app.selectedMunicipio)
                    formData.append("fechaCreacion", app.fechaCreacion)


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateProveedor.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: formData,

                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
                        //mientras enviamos el archivo
                        beforeSend: function () {
                        },
                        success: function (data) {

                            console.log(data)

                            app.nuevo();
                            app.loadProveedores()

                            if (data == "1") {
                                $("#codigoProveedor").prop("disabled", false);
                                alertify.success("Actualizado Correctamente");
                            }


                        },
                        //si ha ocurrido un error
                        error: function (FormData) {
                            console.log(data)
                        }
                    });


                }


            },

            nuevo: function () {

                var app = this;

                $('.formGuardar').removeClass('was-validated');

                app.codigoProveedor = ""
                app.nombresProveedor = ""
                app.selectedProveedor = ""
                app.razonSocial = ""
                app.dirProveedor = ""
                app.telProveedor = ""
                app.selectedDepartamento = ""
                app.selectedMunicipio = ""
                app.correo = ""
                app.fechaCreacion = ""

                $("#codigoProveedor").prop("disabled", false);
                $('#selectProveedor').select2().val("").trigger('change')
                $('#selectDepartamento').select2().val("").trigger('change')
                $('#selectMunicipior').select2().val("").trigger('change')

                app.loadProveedores()
                app.loadFecha()

            },

            exportar: function () {
                if (app.proveedores.length > 0) {


                    var elt = document.getElementById('datos');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "Proveedores"
                    });
                    return XLSX.writeFile(wb, 'lista_Proveedores.xlsx');


                } else {
                    alertify.error('No hay registros que exportar');
                }

            },

            eliminar: function () {


                var app = this;

                if (app.codigoProveedor == "") {

                    alertify.warning("No hay ningun material seleccionado")

                } else {

                    alertify.confirm("Eliminar", ".. Desea eliminar este Material?",
                        function () {
                            $.post('./request/deleteProveedor.php', {proveedor: app.codigoProveedor}, function (data) {
                                // console.log(data);
                                if (data == 1) {
                                    alertify.success("Registro Eliminado.");

                                    app.nuevo()
                                    app.loadFecha()
                                    app.loadProveedores()

                                }
                            });

                        },
                        function () {
                            // alertify.error('Cancel');
                        });
                }
            },


        },


        watch:
            {}
        ,
        mounted() {

            this.loadProveedores();
            this.loadFecha();
            this.loadDepartamentos()

        }
        ,

    })
;
