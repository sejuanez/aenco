<?php
    /**
     * Created by PhpStorm.
     * User: KIKE
     * Date: 10/10/2018
     * Time: 5.23
     */

    include("../../../init/gestion.php");


    $sql = "SELECT  DISTINCT  MU_CODDPTO, MU_DEPARTAMENTO
			from MUNICIPIOS_COL 
                                ";

    $result = ibase_query($conexion, $sql);

    $return_arr = array();


    while ($fila = ibase_fetch_row($result)) {

        $row_array['CODIGO'] = utf8_encode($fila[0]);
        $row_array['NOMBRE'] = utf8_encode($fila[1]);

        array_push($return_arr, $row_array);
    }


    echo json_encode($return_arr);
