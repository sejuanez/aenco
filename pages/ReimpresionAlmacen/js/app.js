// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });
// $('#fecha').val(app.fechaEntrada);

    $('.noSigno').keydown(function (e) {


        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });

    $("#tipoDocumento").change(function () {

        app.tablaTodos = [];
        app.tablaNoSeriados = [];
        app.tablaSeriados = [];
        app.tablaImprimir = [];
        app.tablaNombreSeries = [];

        app.codigo = ""
        app.concepto = ""
        app.nombre = ""
        app.fecha = ""

    });

});


let app = new Vue({
        el: '#app',
        data: {

            flangImprimir: false,

            empresa: [],
            tablaTodos: [],
            tablaNoSeriados: [],
            tablaSeriados: [],
            tablaImprimir: [],

            tablaNombreSeries: [],

            idDocumento: "",
            tipoDocumento: "",

            codigo: "",
            concepto: "",
            nombre: "",
            fecha: "",


        },


        methods: {


            getEmpresa: function () {
                var app = this;

                $.get('./request/getEmpresa.php', function (data) {
                    var data = JSON.parse(data);
                    app.empresa.push(data[0]);
                    console.log(app.empresa[0].NOMBRE)
                });

            },

            buscarDocumento: function () {

                var app = this;

                if (app.idDocumento == "" || app.tipoDocumento == "") {

                    alertify.warning("Seleccione los criterios de busqueda")

                } else {

                    $.ajax({
                        url: './request/getDocumento.php',
                        type: 'GET',
                        data: {
                            codigo: app.idDocumento,
                            tipo: app.tipoDocumento,
                        },

                    }).done(function (response) {

                        try {
                            var data = JSON.parse(response);

                            if (data.length > 0) {
                                app.flangImprimir = true;
                            }

                            console.log(data)

                            app.codigo = data[0].codTecnico;
                            app.concepto = data[0].concepto;
                            app.nombre = data[0].nomTecnico;
                            app.fecha = data[0].fecha;

                            console.log(data[0].serie)

                            app.tablaNoSeriados = [];

                            app.tablaTodos = data;


                            for (var i = 0; i < data.length; i++) {


                                app.tablaNoSeriados.push(
                                    {
                                        codigo: data[i].codigo,
                                        descripcion: data[i].descripcion,
                                        marca: "",
                                        unidad: data[i].nomUnidad,
                                        cantidad: data[i].cantidad
                                    }
                                )


                            }

                            console.log(app.tablaNoSeriados)

                        } catch (e) {

                            console.log('doc no encontrado')

                        }


                    }).fail(function (error) {

                        console.log(error)

                    });


                    $.ajax({
                        url: './request/getSeriados.php',
                        type: 'GET',
                        data: {
                            codigo: app.idDocumento,
                            tipo: app.tipoDocumento,
                        },

                    }).done(function (response) {

                        try {

                            var data = JSON.parse(response);

                            app.tablaSeriados = [];

                            for (var i = 0; i < data.length; i++) {

                                app.tablaSeriados.push(
                                    {
                                        codigo: data[i].serie,
                                        descripcion: data[i].material,
                                        marca: data[i].marca,
                                        unidad: data[i].codigo,
                                        cantidad: 1
                                    }
                                )

                            }

                            console.log(app.tablaSeriados)

                        } catch (e) {

                            console.log('no tiene serie')

                        }


                    }).fail(function (error) {

                        console.log(error)

                    });


                }
            },

            imprimir: function () {


                var app = this;


                var imprimir = {};
                var unicos = app.tablaSeriados.filter(function (e) {
                    return imprimir[e.descripcion] ? false : (imprimir[e.descripcion] = true);
                });

                unicos.forEach(function (seriado) {

                    app.tablaNombreSeries.push(
                        {
                            descripcion: seriado.descripcion,
                        }
                    )

                });

                var arraySeriesimprimir = [];

                for (var i = 0; i < app.tablaNombreSeries.length; i++) {

                    var nombreSerie = app.tablaNombreSeries[i].descripcion;

                    console.log(nombreSerie + " => ")

                    var array = [];

                    for (var j = 0; j < app.tablaSeriados.length; j++) {

                        var nombre = app.tablaSeriados[j].descripcion;
                        var codigo = app.tablaSeriados[j].codigo;

                        if (nombreSerie == nombre) {


                            array.push(
                                {
                                    serie: codigo,
                                }
                            )
                        }

                    }

                    arraySeriesimprimir.push(
                        {
                            descripcion: nombreSerie,
                            arraySeries: array,
                        }
                    )

                }

                console.log(arraySeriesimprimir)


                var app = this;

                app.tablaImprimir = [];

                var seire = {};
                var nombresUnicosSerie = app.tablaSeriados.filter(function (e) {
                    return seire[e.unidad] ? false : (seire[e.unidad] = true);
                });


                if (app.tablaSeriados.length == 0) {

                    app.tablaNoSeriados.forEach(function (noSeriado) {

                        app.tablaImprimir.push(noSeriado)

                    });

                } else {

                    console.log(app.tablaSeriados.length)

                    app.tablaTodos.forEach(function (noSeriado) {
                        nombresUnicosSerie.forEach(function (seriado) {

                            console.log(noSeriado.codigo + "=>" + seriado.unidad + " : " + noSeriado.nomUnidad)

                            if ((noSeriado.codigo == seriado.unidad)) {


                                app.tablaImprimir.push(
                                    {
                                        codigo: noSeriado.codigo,
                                        descripcion: noSeriado.descripcion,
                                        marca: seriado.marca,
                                        unidad: noSeriado.nomUnidad,
                                        cantidad: noSeriado.cantidad
                                    }
                                )


                            } else {

                                if (noSeriado.serie == 'N') {

                                    app.tablaImprimir.push(
                                        {
                                            codigo: noSeriado.codigo,
                                            descripcion: noSeriado.descripcion,
                                            marca: "",
                                            unidad: noSeriado.nomUnidad,
                                            cantidad: noSeriado.cantidad
                                        }
                                    )
                                }

                            }

                        });
                    });

                }

                console.log(app.tablaImprimir)

                var imprimir = {};
                var unicos = app.tablaImprimir.filter(function (e) {
                    return imprimir[e.codigo] ? false : (imprimir[e.codigo] = true);
                });

                app.tablaImprimir = [];

                unicos.forEach(function (seriado) {

                    app.tablaImprimir.push(
                        {
                            codigo: seriado.codigo,
                            descripcion: seriado.descripcion,
                            marca: seriado.marca,
                            unidad: seriado.unidad,
                            cantidad: seriado.cantidad
                        }
                    )

                });

                console.log(app.tablaImprimir)


                if (app.tablaImprimir.length == 0) {

                    alertify.error("No hay registros para imprimir")

                } else {

                    alertify.confirm("Proceso Exitoso", "... Desea Imprimir el reporte de entrada N° " + app.tablaTodos[0].documento + "?",

                        function () {

                            console.log(app.tablaTodos)

                            if (app.tipoDocumento == "ENT") {

                                var dir = './reports/reporte_entrada_bodega.php';

                            } else {

                                var dir = './reports/reporte_entrega_materiales.php';

                            }


                            var response = {
                                server: JSON.stringify(app.tablaTodos),
                                datos: JSON.stringify(app.tablaImprimir),
                                arraySeries: JSON.stringify(arraySeriesimprimir),
                                empresa: app.empresa[0].NOMBRE,
                                nit: app.empresa[0].NIT,
                                idDocumento: app.idDocumento,
                                direccion: app.empresa[0].DIRECCION
                            };
                            app.callReport(dir, response);

                            alertify.alert("Reporte", "El reporte abrira una nueva pestaña", function () {
                                location.reload();
                            });
                        },

                        function () {
                            location.reload();
                        });

                }


            },


            callReport: function (dir, data) {


                var form = "<form action='" + dir + "' method='post' target='_blank' class='_form'>" +
                    "<input type='hidden' name='server' value='" + data.server + "' />" +
                    "<input type='hidden' name='datos' value='" + data.datos + "' />" +
                    "<input type='hidden' name='empresa' value='" + data.empresa + "' />" +
                    "<input type='hidden' name='idDocumento' value='" + data.idDocumento + "' />" +
                    "<input type='hidden' name='arraySeries' value='" + data.arraySeries + "' />" +
                    "<input type='hidden' name='nit' value='" + data.nit + "' />" +
                    "<input type='hidden' name='direccion' value='" + data.direccion + "' />" +
                    "</form>";
                $('body').append(form);
                $('._form').submit();
                // $('._form').remove();
            }

            ,


        },
        watch:
            {}
        ,


        mounted() {

            this.getEmpresa();
        }
        ,


    })
;

