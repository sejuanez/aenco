<?php

include("../../../init/gestion.php");


$codigo = $_GET["codigo"];
$tipo = $_GET["tipo"];
// $tipo="01";


$sql = "      SELECT MA.SA_TIPO ,
                         MA.SA_NUMERO ,
                         MA.SA_CODIGO_MA CODIGO, 
                         MA.SA_NOMBRE_MA DESCRIPCION, 
                         G.GR_CODIGO CODGRUPO, 
                         G.GR_DESCRIPCION NOMGRUPO,
                         MA.SA_CANTIDAD CANTIDAD,
                         MA.SA_VALOR VALOR, 
                         COALESCE(MA.SA_CODTECNICO, MA.SA_CODIGO) CODTECNICO, 
                         COALESCE(MA.SA_NOMTECNICO, MA.SA_NOMBRE) NOMTECNICO,
                         MA.SA_CONCEPTO CONCEPTO, 
                         MA.SA_FECHA , 
                         MA.SA_USUARIO ,
                         G.GR_SERIE SERIALIZADO, 
                         MA.SA_DOCUMENTO , 
                         (MA.SA_VALOR*MA.SA_CANTIDAD) ,
                         MA.SA_CUADRILLA, 
                         MA.SA_CODSUPER, 
                         MA.SA_NOMSUPER, 
                         MA.SA_NCUADRILLA, 
                         MA.SA_FECHA_INGRESO ,
                         MA.SA_CODIGO CODPROVEEDOR, 
                         MA.SA_NOMBRE NOMPROVEEDOR, 
                         UM.UM_CODIGO CODUNIDAD, 
                         UM.UM_NOMBRE NOMUNIDAD,
                         NULL PAQUETE, 
                         NULL PROPIEDAD, 
                         MA.SA_OC_NUMERO 
                  FROM MOVIMIENTO_ALMACEN MA
                       LEFT JOIN MATERIALES M ON M.MA_CODIGO = MA.SA_CODIGO_MA
                       LEFT JOIN GRUPOS G ON G.GR_CODIGO = M.MA_GRUPO
                       LEFT JOIN UNIDAD_MEDIDA UM ON UM.UM_CODIGO = M.MA_UNIDAD
                  WHERE MA.SA_NUMERO = '" . $codigo . "' AND MA.SA_TIPO = '" . $tipo . "'
  ";

$return_arr = array();

$result = ibase_query($conexion, $sql);

while ($fila = ibase_fetch_row($result)) {
    $row_array['tipo'] = utf8_encode($fila[0]);
    $row_array['documento'] = utf8_encode($fila[1]);
    $row_array['codigo'] = utf8_encode($fila[2]);
    $row_array['descripcion'] = utf8_encode($fila[3]);
    $row_array['codGrupo'] = utf8_encode($fila[4]);
    $row_array['nomGrupo'] = utf8_encode($fila[5]);
    $row_array['cantidad'] = utf8_encode($fila[6]);
    $row_array['valor'] = utf8_encode($fila[7]);
    $row_array['codTecnico'] = utf8_encode($fila[8]);
    $row_array['nomTecnico'] = utf8_encode($fila[9]);
    $row_array['concepto'] = utf8_encode($fila[10]);
    $row_array['fecha'] = utf8_encode($fila[11]);
    $row_array['usuarioSistema'] = utf8_encode($fila[12]);
    $row_array['serie'] = utf8_encode($fila[13]);
    $row_array['docExterno'] = utf8_encode($fila[14]);
    $row_array['valorTotal'] = utf8_encode($fila[15]);
    $row_array['codCuadrilla'] = utf8_encode($fila[16]);
    $row_array['saCodSuper'] = utf8_encode($fila[17]);
    $row_array['saNomSuper'] = utf8_encode($fila[18]);
    $row_array['saCuadrilla'] = utf8_encode($fila[19]);
    $row_array['fechaIngresa'] = utf8_encode($fila[20]);
    $row_array['codProveedor'] = utf8_encode($fila[21]);
    $row_array['nomProveedor'] = utf8_encode($fila[22]);
    $row_array['codUnidad'] = utf8_encode($fila[23]);
    $row_array['nomUnidad'] = utf8_encode($fila[24]);

    array_push($return_arr, $row_array);
}

echo json_encode($return_arr);
      

