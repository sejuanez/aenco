<!DOCTYPE html>
<html>
<head>
    <title>Entradas a Bodega</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">


    <style type="text/css">
        label {
            font-size: 0.8em;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .btn-app {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 50px;
            margin-bottom: 10px;
            color: #555;
            font-size: 0.8em;
        }

        .btn-app:hover {
            background: #007bff;
            color: #FFF;
        }

        .btn-app:active {
            background: #009dff;
            color: #FFF;
        }

        label {
            margin-bottom: 0;
        }

        thead {
            font-size: .8em;
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }

        td {
            font-size: .8em;
            cursor: pointer;
        }

        .gj-datepicker input, .gj-datepicker span {
            padding: 5px;
            font-size: .9em;

        }

        input[type="text"] {
            text-transform: uppercase;
        }

        .form-control-sm, .input-group-sm > .form-control, .input-group-sm > .input-group-addon, .input-group-sm > .input-group-btn > .btn {
            line-height: initial;
        }

    </style>

</head>
<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Reeimpresion de Entradas/Salidas


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="imprimir()">
					<i class="fa fa-print" aria-hidden="true"></i>
                Imprimir
            </span>


            <span class="float-right" style="font-size: .9em; cursor: pointer; width: 100px;"
                  @click="buscarDocumento()">
					<i class="fa fa-search" aria-hidden="true"></i>
                Consultar
            </span>
        </p>
    </header>

    <div class="container-fluid">

        <form action="" id="form_encabezado">


            <div class="row">

                <div class="col-3 ">

                    <label for="">Numero de Documento</label>
                    <div class="input-group input-group-sm">
                        <input type="text" v-model="idDocumento" required="required"
                               aria-label="" class="form-control">
                    </div>

                </div>


                <div class="col-2 ">

                    <label for="">Tipo</label>
                    <div class="input-group input-group-sm">
                        <select class="form-control" v-model="tipoDocumento" id="tipoDocumento">
                            <option value="">Seleccione...</option>
                            <option value="ENT">Entrada</option>
                            <option value="SAL">Salida</option>
                        </select>
                    </div>

                </div>


            </div>

            <div class="row">

                <div class="col-3 ">
                    <label for="">Concepto</label>
                    <div class="input-group input-group-sm">
                        <input type="text" id="" disabled
                               v-model="concepto"
                               aria-label="" class="form-control">
                    </div>
                </div>


                <div class="col-3 ">
                    <label for="">Codigo</label>
                    <div class="input-group input-group-sm">
                        <input type="text" id="" disabled
                               v-model="codigo"
                               aria-label="" class="form-control">
                    </div>
                </div>


                <div class="col-4 ">
                    <label for="">Nombre</label>
                    <div class="input-group input-group-sm">
                        <input type="text" id="" disabled
                               v-model="nombre"
                               aria-label="" class="form-control">
                    </div>
                </div>

                <div class="col-2 ">
                    <label for="">Fecha</label>
                    <div class="input-group input-group-sm">
                        <input type="date" id="" disabled
                               v-model="fecha"
                               aria-label="" class="form-control">
                    </div>
                </div>


            </div>


        </form>

        <br>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#noSerie" role="tab"
                   aria-controls="noSerie"
                   aria-selected="true">No Seriados</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#serie" role="tab"
                   aria-controls="serie"
                   aria-selected="false">Seriados</a>
            </li>


        </ul>
        <div class="tab-content" id="myTabContent">

            <br>
            <div class="tab-pane fade show active" id="noSerie" role="tabpanel" aria-labelledby="home-tab">
                <div class="container">

                    <div class="col-12">
                        <table class="table table-sm">
                            <thead class="fondoGris">
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Marca</th>
                                <th>Unidad</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody id="detalle_gastos">
                            <tr v-for="(dato, index) in tablaNoSeriados" :id="index">
                                <td v-text="dato.codigo"></td>
                                <td v-text="dato.descripcion"></td>
                                <td v-text="dato.marca"></td>
                                <td v-text="dato.unidad"></td>
                                <td v-text="dato.cantidad"></td>

                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="serie" role="tabpanel" aria-labelledby="profile-tab">
                <div class="container">

                    <div class="col-12">
                        <table class="table table-sm">
                            <thead class="fondoGris">
                            <tr>
                                <th>Serie</th>
                                <th>Descripcion</th>
                                <th>Marca</th>

                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody id="detalle_gastos">
                            <tr v-for="(dato, index) in tablaSeriados" :id="index">
                                <td v-text="dato.codigo"></td>
                                <td v-text="dato.descripcion"></td>
                                <td v-text="dato.marca"></td>

                                <td v-text="dato.cantidad"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="tab-pane fade" id="imprimir" role="tabpanel" aria-labelledby="profile-tab">
                <div class="container">

                    <div class="col-12">
                        <table class="table table-sm">
                            <thead class="fondoGris">
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Marca</th>
                                <th>Unidad</th>
                                <th>Cantidad</th>
                            </tr>
                            </thead>
                            <tbody id="detalle_gastos">
                            <tr v-for="(dato, index) in tablaImprimir" :id="index">
                                <td v-text="dato.codigo"></td>
                                <td v-text="dato.descripcion"></td>
                                <td v-text="dato.marca"></td>
                                <td v-text="dato.unidad"></td>
                                <td v-text="dato.cantidad"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/datepicker.js"></script>

<script src="js/pace/pace.min.js"></script>


<!-- <script src="js/vue/vue.min.js"></script> -->
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>
</html>
