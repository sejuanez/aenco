<?php


$server = json_decode($_POST['server']);
$data = json_decode($_POST['datos']);
$arraySeries = json_decode($_POST['arraySeries']);
$empresa = $_POST['empresa'];
$nit = $_POST['nit'];
$direccion = $_POST['direccion'];
$idDocumento = $_POST['idDocumento'];


include("../request/gestion.php");

require('fpdf.php');

class PDF extends FPDF
{

    function Header()
    {
        // Select Arial bold 15
        global $title;
        global $city;
        global $nDoc, $numero, $fechaIng, $fechaEntrega, $numeroEntrega, $concepto;
        global $codSupervisor, $supervisor, $cuadrilla, $tecnicos, $codTecnico, $nomTecnico;
        global $nombreEmpresa, $nitEmpresa, $direccionEmpresa;


        $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');

        $this->SetFont('Lato-Regular', '', 12);

        // Ancho de pagina 190 en X
        // $this->setDrawColor(80,80,80);
        // $this->setFillColor(0,80,170);
        // $this->setTextColor(255,255,255);
        $this->setTextColor(80, 80, 80);

        try {
            $this->Image('../../../img/empresa1.jpg', 10, 10, 40, 12, 'jpeg');

        } catch (Exception $e) {

            $this->Image('../../../img/empresa1.jpg', 10, 10, 40, 12, 'jpg');
        }


        $this->SetFont('Lato-Regular', '', 14);
        $this->Cell(190, 10, $nombreEmpresa, 0, 0, 'C', false);
        $this->Ln(8);
        $this->SetFont('Lato-Regular', '', 8);
        $this->Cell(190, 5, 'NIT:  ' . $nitEmpresa, 0, 0, 'C', false);
        $this->Ln(5);
        $this->Cell(190, 5, $direccionEmpresa, 0, 0, 'C', false);

        $this->SetFont('Lato-Regular', '', 14);
        $this->Ln(10);
        $this->Cell(190, 5, $title, 0, 0, 'C', false);


        // Line break
        $this->Ln(10);

        $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
        $this->SetFont('Lato-Regular', '', 10);


        $this->setTextColor(0, 80, 170);
        $this->Cell(31, 7, 'Fecha Movimiento :', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $dateI = new DateTime($fechaIng);
        $fechaI = $dateI->format('d/m/Y');
        $this->Cell(22, 7, $fechaI, 0, 0, 'L');

        $this->setTextColor(0, 80, 170);
        $this->Cell(26, 7, 'Fecha Registro :', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);

        $dateE = new DateTime($fechaEntrega);
        $fechaE = $dateE->format('d/m/Y');
        $this->Cell(22, 7, $fechaE, 0, 0, 'L');

        $this->setTextColor(0, 80, 170);
        $this->Cell(18, 7, utf8_decode('N° Interno :'), 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $this->Cell(22, 7, $nDoc, 0, 0, 'L');

        $this->setTextColor(0, 80, 170);
        $this->Cell(26, 7, utf8_decode('N° Movimiento : '), 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $this->Cell(22, 7, $numero, 0, 0, 'L');
        $this->Ln(7);


        $this->setTextColor(0, 80, 170);
        $this->Cell(20, 7, 'Concepto :', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $this->Cell(170, 7, $concepto, 0, 0, 'L');
        $this->Ln(7);


        $this->setTextColor(0, 80, 170);
        $this->Cell(20, 7, 'Tecnico :', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);

        // aqui va el tecnico + cedula

        $this->Cell(110, 7, $codTecnico . '  -  ' . $nomTecnico, 0, 0, 'L');
        $this->setTextColor(0, 80, 170);
        $this->Cell(25, 7, 'Cuadrilla :', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $this->Cell(30, 7, $cuadrilla, 0, 0, 'L');
        $this->Ln(7);

        $this->setTextColor(0, 80, 170);
        $this->Cell(40, 7, 'Tecnicos de Cuadrilla :', 0, 0, 'L');
        $this->Ln(7);
        $this->setTextColor(80, 80, 80);
        $this->MultiCell(0, 4, $tecnicos);

    }

    function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);

        $this->AddFont('LatoL', '', 'Lato-Light.php');
        $this->SetFont('LatoL', '', 8);
        // Print centered page number
        $this->setFillColor(0, 80, 170);
        $this->setTextColor(80, 80, 80);
        $this->Cell(95, 10, 'Fecha y hora de impresion: ' . date('d/m/Y  -H:i:s', time() - 21600), 'T', 0, 'L');
        $this->Cell(95, 10, utf8_decode('Página ' . $this->PageNo()) . '/{nb}', 'T', 0, 'R');
    }

}//en Class PDF

$pdf = new PDF();

$title = 'SALIDA DE ALMACEN';

$fecha = $server[0]->fecha;
$fechaIng = $server[0]->fechaIngresa;
$nDoc = $server[0]->docExterno;
$numero = $server[0]->documento;
$concepto = $server[0]->concepto;
$idProveedor = $server[0]->codTecnico;
$proveedor = $server[0]->nomTecnico;

$nombreEmpresa = $empresa;
$nitEmpresa = $nit;
$direccionEmpresa = $direccion;

$codTecnico = $server[0]->codTecnico;
$nomTecnico = $server[0]->nomTecnico;

$numeroEntrega = $idDocumento;
///$numeroEntrega = $data->numeroEntrega;


$cuadrilla = $server[0]->codCuadrilla;
$tecnicos = $server[0]->saCuadrilla;


$pdf->SetTitle($title);
$pdf->AddPage();
$pdf->AliasNbPages();


$pdf->Ln(10);


// Header table
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('LatoL', '', 9);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(0, 80, 170);
$pdf->setTextColor(255, 255, 255);

$pdf->Cell(20, 7, 'CODIGO', 1, 0, 'C', true);
$pdf->Cell(110, 7, 'DESCRIPCION DE MATERIAL', 1, 0, 'C', true);
$pdf->Cell(20, 7, 'MARCA', 1, 0, 'C', true);
$pdf->Cell(20, 7, 'UNIDAD', 1, 0, 'C', true);
$pdf->Cell(20, 7, 'CANTIDAD', 1, 0, 'C', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 9);


// DETALLE
// var_dump($data);
$n = count($data);
for ($i = 0; $i < $n; $i++) {

    $pdf->Cell(20, 7, $data[$i]->codigo, 'B', 0, 'C', true);
    $pdf->Cell(100, 7, $data[$i]->descripcion, 'B', 0, 'C', true);
    $pdf->Cell(30, 7, $data[$i]->marca, 'B', 0, 'C', true);
    $pdf->Cell(20, 7, $data[$i]->unidad, 'B', 0, 'C', true);
    $pdf->Cell(20, 7, $data[$i]->cantidad, 'B', 0, 'C', true);
    $pdf->Ln(8);


}

//$pdf->Ln(2);

$n = count($arraySeries);
for ($i = 0; $i < $n; $i++) {


    if ($n > 0) {
        $text = '>> SERIES DE ' . $arraySeries[$i]->descripcion . ' <<';
        $text2 = '';

        $n2 = count($arraySeries[$i]->arraySeries);
        for ($j = 0; $j < $n2; $j++) {
            $text2 .= $arraySeries[$i]->arraySeries[$j]->serie . ' - ';
        }

        $pdf->SetFont('Lato', '', 7);
        $pdf->Cell(190, 7, $text);
        $pdf->Ln(5);
        $pdf->MultiCell(0, 3, $text2);
        $pdf->Ln(1);
    }
}


$pdf->Ln();
$pdf->SetFont('Lato', '', 9);
$pdf->Cell(60, 7, 'Registro : ' . $server[0]->usuarioSistema);
$pdf->Ln(20);

$pdf->setTextColor(0, 80, 170);
$pdf->Cell(90, 7, 'Entrego', 'T', 0, 'C', true);
$pdf->Cell(10, 7, ' ');
$pdf->Cell(90, 7, 'Recibio', 'T', 0, 'C', true);
$pdf->Ln(10);

$pdf->Ln(10);
$pdf->Output();


?>
