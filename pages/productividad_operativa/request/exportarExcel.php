<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	//header("Content-Disposition: filename=consumo_".$_GET["tab"]."_".$_GET["tab"].".xls");
  //header("Content-Disposition: filename=consumo_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  

	include("../../../init/gestion.php");

	$fechaini = $_GET["fechaini"];
  $fechafin = $_GET["fechafin"];
  $tab = $_GET["tab"];

  if($tab == "0"){

    //header("Content-Disposition: filename=consumoGeneral_desde_".str_replace('-',"",$_GET["fechaini"])."_hasta_".str_replace('-',"",$_GET["fechafin"]).".xls");
    header("Content-Disposition: filename=productividadOperativa_desde_".$fechaini."_hasta_".$fechafin.".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM


    $consulta = "SELECT  substring(cast (extract ( year from lc.ca_fechaej) as integer)+10000 from 2 for 4) || substring(cast (extract (month from lc.ca_fechaej) as integer)+100 from 2 for 2) ca_fechaej, (CASE WHEN EXTRACT (month FROM LC.ca_fechaej) = 1 THEN 'ENE'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 2 THEN 'FEB'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 3 THEN 'MAR'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 4 THEN 'ABR'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 5 THEN 'MAY'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 6 THEN 'JUN'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 7 THEN 'JUL'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 8 THEN 'AGO'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 9 THEN 'SEP'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 10 THEN 'OCT'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 11 THEN 'NOV'||'-'||extract (year from lc.ca_fechaej) WHEN EXTRACT (month FROM LC.ca_fechaej) = 12 THEN 'DIC'||'-'||extract (year from lc.ca_fechaej) END) anomes, sum(dm.ma_valmater) valor from lega_cabecera lc left join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_tipomat='O' where substring(cast (extract ( year from lc.ca_fechaej) as integer)+10000 from 2 for 4) || substring(cast (extract (month from lc.ca_fechaej) as integer)+100 from 2 for 2) between '".$fechaini."' and '".$fechafin."' group by anomes,  ca_fechaej order by ca_fechaej";

    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Periodo</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){

      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[1]).//periodo
          "</td><td>".utf8_encode($fila[2]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    
    echo $tabla;

  }else if($tab == "1"){

    header("Content-Disposition: filename=productividadOperativaPorMuncipio_desde_".$fechaini."_hasta_".$fechafin.".xls");

    echo "\xEF\xBB\xBF"; // UTF-8 BOM

    $consulta = "SELECT m.mu_nombre, sum(dm.ma_valmater) valor from lega_cabecera lc left join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_tipomat='O' left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio where substring(cast (extract ( year from lc.ca_fechaej) as integer)+10000 from 2 for 4) || substring(cast (extract (month from lc.ca_fechaej) as integer)+100 from 2 for 2) between '".$fechaini."' and '".$fechafin."' group by m.mu_nombre";


    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Municipio</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){


      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//municipio
          "</td><td>".utf8_encode($fila[1]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
  else{

    header("Content-Disposition: filename=productividadOperativaPorZona_desde_".$fechaini."_hasta_".$fechafin.".xls");

    //echo "\xEF\xBB\xBF"; // UTF-8 BOM

    $consulta = "SELECT z.zo_nombre, sum(dm.ma_valmater) valor from lega_cabecera lc left join dato_material dm on dm.ma_acta=lc.ca_acta and dm.ma_tipomat='O' left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio left join zonas z on z.zo_codigo=m.mu_peaje where substring(cast (extract ( year from lc.ca_fechaej)as integer)+10000 from 2 for 4) || substring(cast (extract (month from lc.ca_fechaej) as integer)+100 from 2 for 2) between '".$fechaini."' and '".$fechafin."' group by z.zo_nombre order by z.zo_nombre";


    $result = ibase_query($conexion,$consulta);


    $tabla = "<table>"."<tr class='cabecera'><td>Zona</td><td>Cantidad</td></tr>";

    while($fila = ibase_fetch_row($result)){


      $tabla.="<tr class='fila'>".
          "<td>".utf8_encode($fila[0]).//zona
          "</td><td>".utf8_encode($fila[1]).//cantidad
          "</td>".
        "</tr>";
    }

    $tabla.="</table>";
    echo $tabla;
  }
		
	
?>