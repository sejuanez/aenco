<?php
session_start();

//incluimos el archivo que contiene la configuracion de la conexion a la bd
//include('gestion.php');


?>
<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">

    <title>Inicio</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>


    <!--<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />-->

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">


    <script type="text/javascript">

        $(function () {


        });

    </script>

</head>

<body>

<?php
if (!$_SESSION['user']) {
    ?>
    <h3>:(</h3>
    <h1>Lo sentimos!</h1>
    <p>Página no disponible.</p>
    <?php
} else {
    ?>
    <h3> :) Hola! </h3>
    <h1><?php echo $_SESSION['nombreUsuario']; ?></h1>
    <p>Nos place tenerte nuevamente por aquí.</p>
<?php } ?>
</body>

</html>