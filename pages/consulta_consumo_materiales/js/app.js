// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectTecnicos").select2().change(function (e) {
        var usuario = $(".selectTecnicos").val();
        app.onChangeTecnico(usuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        fechaIni: "",
        fechaFin: "",

        tablaDataos: [],

        selectTecnicos: [],
        valorTecnico: "",

    },


    methods: {

        loadTecnicos: function () {

            var app = this;

            $.post('./request/getTecnicos.php', function (data) {
                app.selectTecnicos = JSON.parse(data);
            });

        },

        onChangeTecnico: function (cod) {
            let app = this;
            app.valorTecnico = cod;
        },

        buscar: function () {

            let app = this;

            $.ajax({
                url: './request/getDatos.php',
                type: 'POST',
                data: {
                    tecnico: app.valorTecnico,
                    fechaIni: app.fechaIni,
                    fechaFin: app.fechaFin
                },

            }).done(function (response) {
                console.log(response);
                app.tablaDataos = JSON.parse(response);
            }).fail(function (error) {

                console.log(error)

            });

        },

        loadFechas: function () {

            let app = this;

            var dt = new Date();
            var day = dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate();
            var month = (dt.getMonth() + 1) < 10 ? '0' + (dt.getMonth() + 1) : (dt.getMonth() + 1);
            var year = dt.getFullYear();
            app.fechaIni = year + '-' + month + '-' + day;
            app.fechaFin = year + '-' + month + '-' + day;


        },

    },


    watch: {},

    mounted() {
        this.loadTecnicos();
        this.loadFechas();
    },

});
