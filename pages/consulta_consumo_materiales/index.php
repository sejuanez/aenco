<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <style type="text/css">

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }

        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 55vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            text-align: center;
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Consulta Consumo de Materiales


            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="buscar();">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">

            <div class="col-sm-3"></div>

            <div class="col-12 col-sm-6">
                <div class="form-group input-group-sm">


                    <select class="selectTecnicos" id="selectTecnicos" name="selectTecnicos"
                            v-model="valorTecnico">
                        <option value="">Tecnico...</option>
                        <option v-for="item in selectTecnicos"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>

                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 7px">


            <div class="col-sm-2"></div>

            <div class="col-12 col-sm-4">
                <fieldset>
                    <legend>&nbsp Fecha &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaIni">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>

            <div class="col-12 col-sm-4">
                <fieldset>
                    <legend>&nbsp Fecha Fin &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input type="date" id="fecha" class="form-control" required aria-label="fecha"
                                           v-model="fechaFin">
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th class="text-center" width="80">Pedido</th>
                        <th class="text-center" width="80">Orden</th>
                        <th class="text-left">Tecnico</th>
                        <th class="text-center" width="120">Cod Material</th>
                        <th class="text-center">Desc</th>
                        <th class="text-center" width="80">Tipo</th>
                        <th class="text-center" width="80">Cantidad</th>
                        <th class="text-center" width="80">RI</th>
                        <th class="text-center" width="80">NR</th>
                        <th class="text-center" width="80">Chatarra</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaDataos.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaDataos">
                        <td class="text-center" width="80" v-text="dato.numPed"></td>
                        <td class="text-center" width="80" v-text="dato.numOrden"></td>
                        <td v-text="dato.te_nombre"></td>
                        <td class="text-center" width="120" v-text="dato.maCod"></td>
                        <td v-text="dato.maDesc"></td>
                        <td class="text-center" width="80" v-text="dato.maTipo"></td>
                        <td class="text-center" width="80" v-text="dato.maCantidad"></td>
                        <td class="text-center" width="80" v-text="dato.maRi"></td>
                        <td class="text-center" width="80" v-text="dato.maNR"></td>
                        <td class="text-center" width="80" v-text="dato.maChatarra"></td>


                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
