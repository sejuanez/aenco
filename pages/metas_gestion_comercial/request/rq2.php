<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$mes = $_POST['mes'];
	$anio = $_POST['anio'];
	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	
	
	$consulta = "SELECT distinct GC.tecnico CODIGOFUN,coalesce(dr.rh_apellido1, '')||' '||coalesce(dr.rh_nombre1, '') FUNCIONARIO, coalesce(mgm.mgm_meta_mes,0) / coalesce(mgc.dias_mes,0) META_DIA, (sum(coalesce (gc.valor_pago,0)) + sum(coalesce(gc.vr_c_convenio,0)) + sum(coalesce(gc.vr_c_atrasada,0))) PAGOS_EFECTIVO, count(gc.punto_pago) cupones FROM gestion_cobro_entrega gc left join tecnicos t on t.te_codigo=gc.tecnico left join datos_rrhh dr on dr.rh_cedula=t.te_cedula left join metas_gestor_mes mgm on mgm.mgm_ano=gc.ano and mgm.mgm_mes=gc.mes and mgm.mgm_codgestor=gc.tecnico left join metas_gestionc mgc on mgc.ano=gc.ano and mgc.mes=gc.mes inner join puntos_pago pp on pp.pp_codigo=gc.punto_pago and pp.pp_perativa='S' where gc.ano=".$anio." and gc.mes =".$mes." and gc.fecha_programacion between '".$fechaini."' and '".$fechafin."' GROUP BY CODIGOFUN, FUNCIONARIO,  META_DIA ";
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['codFun'] = utf8_encode($fila[0]);
		$row_array['funcionario'] = utf8_encode($fila[1]);		
		$row_array['metaDia'] = utf8_encode($fila[2]);
		$row_array['pagosEfectivos'] = utf8_encode($fila[3]);
		$row_array['cupones'] = utf8_encode($fila[4]);
							
		array_push($return_arr, $row_array);
		//$return_arr[utf8_encode($fila[0])]=$row_array;
	}

	echo json_encode($return_arr);

?>