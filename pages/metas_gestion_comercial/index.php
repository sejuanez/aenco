<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Metas gestión comercial</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			var repuestaAjax2 = repuestaAjax3 = requestAjax4 = requestAjax5 = diasTrabajados = null;
			

			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico1").width();
					var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					/*ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);*/
				}
			});
			

			$(window).resize(function() {
			    	
			    	var ancho = +$("#div-grafico1").width();
			    	var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					/*ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);*/
			});
		
			
			function dibujarGraficoBarras_(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        //colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores ($)',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};

			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            zoomType: 'xy',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};






			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();


			


			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				repuestaAjax2 = repuestaAjax3 = repuestaAjax4 = repuestaAjax5= diasTrabajados = metaDiaria = media = null;
				
				


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;


		        //obtener dias trabajados
		        $.ajax({
						url:'request/rq1.php',
		                type:'POST',
		                dataType:'json',
		                data:{mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					if(repuesta.length>0){// si hay dias trabajados

						diasTrabajados = +(repuesta[0].diasTrabajados);
					

					        // ajax2: obtener primera parte de info de funcionarios

							$.ajax({
								url:'request/rq2.php',
				                type:'POST',
				                dataType:'json',
				                data:{mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear(), fechaini:fechaIni , fechafin:fechaFin}

							}).done(function(repuesta){

								
								if(repuesta.length>0){

									repuestaAjax2 = repuesta.slice();

									var media = 0;

									for(var i=0; i < repuesta.length; i++){

										media = media + (+repuestaAjax2[i].pagosEfectivos);
									}
									
									media = media / repuesta.length;
									
									//ajax3:  obtener segunda parte de info de funcionarios
									$.ajax({
										url:'request/rq3.php',
						                type:'POST',
						                dataType:'json',
						                data:{mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear(), fechaini:fechaIni , fechafin:fechaFin}

									}).done(function(repuesta){

										
										if(repuesta.length>0){

											repuestaAjax3 = repuesta.slice();

											var funcionarios=[];						
											var pagosEfectivos=[];
											var pagosCancelados=[];
											var metasDiarias=[];
											var medias=[];

											for (var i=0; i<repuestaAjax2.length;i++){

												var sw=false;
												
												for (var j=0;(j<repuestaAjax3.length)&&(!sw);j++){
													
													if(repuestaAjax3[j].codFun==repuestaAjax2[i].codFun){
														//console.log("i="+i+" CodFun[i]:"+repuestaAjax2[i].codFun);
														//console.log("j="+j+" CodFun[j]:"+repuestaAjax3[j].codFun);
														//console.log("-----------");
														funcionarios[i]=repuestaAjax2[i].funcionario;
														pagosEfectivos[i]=+repuestaAjax2[i].pagosEfectivos;
														metasDiarias[i]=(+repuestaAjax2[i].metaDia)*diasTrabajados;
														pagosCancelados[i]=(+repuestaAjax3[j].pagosCancelados - (+repuestaAjax2[i].pagosEfectivos));
														medias[i]=media;
														sw=true;
													}
												}
											}

											$.ajax({
													url:'request/rq4.php',
									                type:'POST',
									                dataType:'json',
									                data:{fechaini:fechaIni , fechafin:fechaFin}

													}).done(function(repuesta){

														if(repuesta.length>0){

															repuestaAjax4 = repuesta.slice();

															/*var funcionarios=[];						
															var pagosEfectivos=[];
															var pagosCancelados=[];
															var metasDiarias=[];
															var medias=[];*/

															var compromisos=[];
															var cupones=[];
															var cuponesX1300=[];
															var visitas=[];

															var colors=['#00034A','#0008C5','#0f0','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

															var filas="";



															for(var i=0;i<repuestaAjax2.length; i++){																

																var sw=false;

																 for (var j=0;(j<repuestaAjax4.length)&&(!sw);j++){

																 	if(repuestaAjax4[j].codFun==repuestaAjax2[i].codFun){
																 		
																 		sw=true;

																 		/*funcionarios[i]=repuestaAjax2[i].funcionario;
																		pagosEfectivos[i]=+repuestaAjax2[i].pagosEfectivos;
																		metasDiarias[i]=(+repuestaAjax2[i].metaDia)*diasTrabajados;
																		pagosCancelados[i]=(+repuestaAjax3[i].pagosCancelados - (+repuestaAjax2[i].pagosEfectivos));
																		medias[i]=media;*/

																		cupones[i]=+repuestaAjax2[i].cupones;
																		cuponesX1300[i]=(+repuestaAjax2[i].cupones)*1300;

																 		compromisos[i]=+repuestaAjax4[j].compromisos;
																		visitas[i]=+repuestaAjax4[j].cantVisitas;

																		/*filas+="<tr class='fila'>"+
									                              				"<td>"+repuestaAjax2[i].codFun+"</td><td>"+repuestaAjax2[i].funcionario+"</td><td>"+repuestaAjax2[i].metaDia+"</td><td>"+(+repuestaAjax2[i].metaDia*diasTrabajados)+"</td><td>"+repuestaAjax2[i].pagosEfectivos+"</td><td>"+(+repuestaAjax3[i].pagosCancelados - (+repuestaAjax2[i].pagosEfectivos))+"</td><td>"+repuestaAjax2[i].cupones+"</td><td>"+repuestaAjax4[j].cantVisitas+"</td><td>"+repuestaAjax4[j].compromisos+"</td></tr>";*/
																 	}
																 }
															}

															

															for (var i=0; i<repuestaAjax2.length;i++){
																filas+="<tr class='fila'>"+
									                              				"<td>"+repuestaAjax2[i].codFun+"</td><td>"+repuestaAjax2[i].funcionario+"</td><td>"+repuestaAjax2[i].metaDia+"</td><td>"+(+repuestaAjax2[i].metaDia*diasTrabajados)+"</td><td>"+repuestaAjax2[i].pagosEfectivos+"</td><td>"+pagosCancelados[i]+"</td><td>"+repuestaAjax2[i].cupones+"</td><td>"+visitas[i]+"</td><td>"+compromisos[i]+"</td></tr>";
															}

	
															$("#tabla-general tbody").html(filas);

															//grafico1
															var series=[
																		{
																			name:'Pagos efectivos', 
																			type: 'column',
																			data:pagosEfectivos,
																			dataLabels: {
																				enabled: true,
									                              				//rotation: -90,
									                              				color: '#00034A',
									                              				align: 'center',
									                             				// format: '${point.y}', // one decimal
												                              	formatter:function(){
												                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
												                              	},
									                              				y: 10, // 10 pixels down from the top
												                                style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
									                          				}
									                          			},
																		{
																			name:'Pagos cancelados',
																			type: 'column', 
																			data:pagosCancelados,
																			dataLabels: {
																				enabled: true,
									                              				//rotation: -90,
									                              				color: '#0008C5',
									                              				align: 'center',
									                             				// format: '${point.y}', // one decimal
												                              	formatter:function(){
												                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
												                              	},
									                              				y: 10, // 10 pixels down from the top
												                                style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
									                          				}
																		},
																		{
																			name:'Meta',
																			type: 'spline',
																			data:metasDiarias,
																			dataLabels:{
																				enabled:false,
																				color: '#008000'
																			}
																		},
																		{
																			name:'Media',
																			type:'spline',
																			data:medias,
																			dataLabels:{
																				enabled:false,
																				color: '#f00'
																			}
																		}
																	];

															colors =["#7CFC00",'#0008C5','#32CD32','#FF6347'];

															dibujarGraficoBarras("grafico1",funcionarios,series,colors);


															//grafico2
															var series=[
																		{
																			name:'Compromisos', 
																			type: 'column',
																			data:compromisos,
																			dataLabels: {
																				enabled: true,
									                              				//rotation: -90,
									                              				color: '#000',
									                              				align: 'center',
									                             				// format: '${point.y}', // one decimal
												                              	/*formatter:function(){
												                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
												                              	},*/
									                              				y: 10, // 10 pixels down from the top
												                                style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
									                          				}
									                          			},
																		{
																			name:'Cupones',
																			type: 'column', 
																			data:cupones,
																			dataLabels: {
																				enabled: true,
									                              				//rotation: -90,
									                              				color: '#000',
									                              				align: 'center',
									                             				// format: '${point.y}', // one decimal
												                              	/*formatter:function(){
												                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
												                              	},*/
									                              				y: 10, // 10 pixels down from the top
												                                style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
									                          				}
																		},
																		{
																			name:'Visitas',
																			type: 'column',
																			data:visitas,
																			dataLabels:{
																				enabled:true,
																				color: '#000',
																				align: 'center',
																				y: 10,
																				style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
																			}
																		},
																		
																	];

															//var colors=['#0f0','#FFD700','#0008C5','#00034A','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
															colors =['#FF6347',"#7CFC00",'#0008C5','#32CD32',];
															dibujarGraficoBarras("grafico2",funcionarios,series,colors);


															//grafico3
															var series=[
																		{
																			name:'Cupones', 
																			type: 'column',
																			data:cuponesX1300,
																			dataLabels: {
																				enabled: true,
									                              				//rotation: -90,
									                              				color: '#000',
									                              				align: 'center',
									                             				// format: '${point.y}', // one decimal
												                              	formatter:function(){
												                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
												                              	},
									                              				y: 10, // 10 pixels down from the top
												                                style: {
												                                  fontSize: '10px',
												                                  fontFamily: 'Verdana, sans-serif'
												                                }
									                          				},
									                          				colorByPoint: true
									                          			}
																		
																	];

															var colors=['#00034A','#0008C5','#0f0','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

															dibujarGraficoBarras("grafico3",funcionarios,series,colors);


															$("#contenido").show();
											

															$("#consultando").hide();
															$("#consultar").show();
															$("#exportar a").attr("href","request/exportarExcel.php?info="+$("#div-tabla").html());
															$("#exportar").show();




														}
														else{

															$("#tabla-general tbody").html("");
															$("#grafico1").html("");

															$("#consultando").hide();
															$("#consultar").show();
														}
													});

										}
										else{												
											
											$("#tabla-general tbody").html("");
											$("#grafico1").html("");

											$("#consultando").hide();
											$("#consultar").show();
										}

										
									});

									
								}
								else{												
									
									$("#tabla-general tbody").html("");
									$("#exportar a").attr("href","");

									$("#consultando").hide();
									$("#consultar").show();								
								}


								
							});
					}
					else{
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");

						$("#consultando").hide();
						$("#consultar").show();
					}

					
				});




			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay programaciones para mostrar.</p>
	</div>
	

	<header>
		<h3>Metas gestión comercial</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Datos</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Análisis meta</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Compromisos, cupones y visitas</span></a></li>
				<li><a href="#tab4">4. <span class='titulo-tab'>Ingreso adicional</span></a></li>
			
			</ul>


			<div id='tab1'>
				<div id='div-tabla'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Codigo</td><td>Funcionario</td><td>Meta dia</td><td>MD x DT</td><td>Pagos efectivos</td><td>Pago cancelado</td><td>Cupones</td><td>Cant. visitas</td><td>Compromisos</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
				</table>
				</div>
			</div>




			<div id='tab2'>
				
				<div id='div-grafico1'>
					<div id='grafico1'></div>
				</div>
			</div>




			<div id='tab3'>
				
				<div id='div-grafico2'>
					<div id='grafico2'></div>
				</div>
				
			</div>




			<div id='tab4'>
				
				<div id='div-grafico3'>
					<div id='grafico3'></div>
				</div>

			</div>



		</div>

		

	</div>

	

</body>
</html>