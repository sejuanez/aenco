<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='css/ionicons/css/ionicons.min.css' type='text/css' />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

   

    <title>CONELEC</title>
  </head>
  <body>
  	<header>
		<h3>Control de Vehículos</h3>
		<nav>
			<ul id="menu">
			<li><a href="#" target="_self" id="exportar"><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
			<li id="consultar"><a href="#" id="btn_consultar"><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>	
			</ul>
		</nav>
	</header>
	

	<div id='subheader'>
		<div id="div-form">
			<form id="form" action="./request/getVehiculos.php" method="POST">
				<div><label>Placa Vehículos</label><input type="text" class="form-control" id="myInput"  placeholder="Todos" title="Type in a name" name="placa"></div>
				<div><label>Fecha inicial</label><input value="<?php echo date("Y-m-d");?>" id="txtFechaIni" class="form-control" type="date" name="fecha1"></div>
				<div><label>Fecha final</label><input value="<?php echo date("Y-m-d");?>" id="txtFechaFin" class="form-control" type="date" name="fecha2"></div>
				<div>
					<label>Tipo Movimiento</label>
					<div class="card">
					  <div class="card-body" style="padding: 1px; margin-left: 4px" id="movimientos">
		    			<div class="custom-control custom-radio custom-control-inline">
						  <input type="radio" id="rEntrada" name="movimiento" class="custom-control-input" value="Entrada">
						  <label class="custom-control-label" for="rEntrada">Entrada</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
						  <input type="radio" id="rSalida" name="movimiento" class="custom-control-input" value="Salida">
						  <label class="custom-control-label" for="rSalida">Salida</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
						  <input type="radio" id="rAmbos" name="movimiento" class="custom-control-input" checked value="%%">
						  <label class="custom-control-label" for="rAmbos">Ambos</label>
						</div>
					  </div>
					</div>
				</div>
				<input type="hidden" value="consultar" name="accion" id="accion"/>
			</form>
		</div>
	</div>
                                                            
	<div class="container-fluid" id="tabVehiculos" style="display: none;">
	  <div class="row" style="float: left;">
		<div class="col-xl-12" >
			<table class="table table-bordered" id="myTable">
			  <thead>
				<tr style="text-align:center">
				  <th scope="col">PLACA</th>
				  <th scope="col">FECHA</th>
				  <th scope="col">HORA</th>
				  <th scope="col">MOVIMIENTO</th>
				  <th scope="col">KM</th>
				  <th scope="col">CONDUCTOR</th>
				  <th scope="col">MUNICIPIO</th>
				  <th scope="col">OBSERVACION</th>
				</tr>
			  </thead>
			  <tbody id="cuerpo_tabla"></tbody>
			</table>
		</div>
		<br>
	  </div>
	</div>


	<div class="container">
			<div class="alert alert-danger" style="display:none" id="bsalert">
		  <a href="#" class="close" id="close_alert"  aria-label="close">&times;</a>
		   <div id="mensaje_altera">Mensaje<div>
		</div>
	</div>


    <script src="js/jquery.min.js"></script>
    <!-- Boostrap JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	

	<script src="request/funciones.js"></script>

  </body>
</html>