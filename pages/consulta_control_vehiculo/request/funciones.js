function toggleAlert(){
		    $(".alert").fadeIn(); 
		    
		}
		function closeAlert(){
		    $(".alert").fadeOut(); 
		    
		}


		$("#alerta").on("click", toggleAlert);
		$("#close_alert").on("click", closeAlert);
		
		$(document).ready(function(){
		  $("#hide").click(function(){
		    $("#tabVehiculos").hide();
		  });
		  $("#consultar").click(function(){
		    $("#tabVehiculos").show();
		  });
		});

		$("#exportar").click(function(event){
			$("#accion").val("exportar");
			$("#form").submit();
		});

		$("#btn_consultar").click(function(event) {
			$("#accion").val("consultar");
		  valido=0;
		  $("#cuerpo_tabla").html("");

	
		  fecha_inicial=new Date($("#txtFechaIni").val());
		  fecha_final= new Date($("#txtFechaFin").val());

		 mensaje="<ul style='padding-left: 20px'>";

		  if(!fecha_inicial.getTime()){
		  	mensaje+="<li>Falta fecha <strong>Inicial</strong></li>"
		  	
		  }else{
		  	valido++;
		  }

		  if(!fecha_final.getTime()){
		  	mensaje+="<li>Falta fecha <strong>Final</strong></li>"
		  }else{
		  	valido++;
		  }

		  if(fecha_final<fecha_inicial){
		  	mensaje+="<li>Fecha <strong>Final</strong> debe ser mayor a fecha <strong>Inicial</strong></li>"
		  }

		  if(!$("#movimientos input[name='movimiento']").is(':checked')){
		  	mensaje+="<li>Debe seleccionar una <strong>Tipo de Movimiento.</strong></li>"
		  }else{
		  	valido++;
		  }

		  if(valido==3){
		  	
		  	$.ajax({
			    // la URL para la petición
			    url : './request/getVehiculos.php',
			 
			    // la información a enviar
			    // (también es posible utilizar una cadena de datos)
			    data : $("#form").serialize(),
			 
			    // especifica si será una petición POST o GET
			    type : 'POST',
			 
			    
			    // código a ejecutar si la petición es satisfactoria;
			    // la respuesta es pasada como argumento a la función
			    success : function(json) {
			        $('#cuerpo_tabla')
			            .html(json);
			    },
			 
			    // código a ejecutar si la petición falla;
			    // son pasados como argumentos a la función
			    // el objeto de la petición en crudo y código de estatus de la petición
			    error : function(xhr, status) {
			        alert('Disculpe, existió un problema');
			    },
			 
			    // código a ejecutar sin importar si la petición falló o no
			    complete : function(xhr, status) {
			        //alert('Petición realizada');
			    }
			});
		  }else{
		  	mensaje+="</ul>"
		  	$("#mensaje_altera").html(mensaje);
		  	toggleAlert();
		  }
		});