<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  } 
?>

<!DOCTYPE html5>
<html>
<head>

  <meta charset="UTF-8">

  <title>Consultar Actas</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <!--<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />-->

  
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>



	<script type="text/javascript">
      
      var acta;
      
      
  		$(function(){

        
        //funcion que se invoca al presionar ENTER o al hacer click en el boton consultar
        function buscar(event){
          
          event.preventDefault();

          $( "#tabs" ).tabs( "option", "active", 0 );

          //var loading=0;
          var listarEvidencias=getCabecera=getCenso=getCliente=getMas=getMateriales=getMedidor=getSellos=getUnidadesIrregularidades=getPruebasMedidor=false;

          //$("#buscar").button({label:"("+loading+"%)"});
          //$("#buscar").button("disable");
          $("#consultar").hide();
          $("#contenido").hide();
          $("#consultando").show();

          var acta = $("#acta").val();
          $("#box1-txtHora").val("");
          $("#section1  input[type='text']").val("");//reset los txt de la cabecera
          $("#seccion2  input[type='text']").val("");//reset los txt de la seccion2
          $("#txtObservaciones").html("");
          $("#acta").val(acta);
          $("#tab3-tablaSello").html("<tr class='cabecera'><td>Sellos</td><td>Ubicación</td><td>Serie</td><td>Cod. sello</td><td>Marca</td><td>Causa</td><td>Nombre</td></tr>");
          $("#tab4-tablaPruebasMedidor").html("<tr class='cabecera'><td>Factor</td><td>Fase</td><td>Volaje</td><td>Lect. Inicial</td><td>Lect. Final</td><td>Dosifica</td><td>Giro</td><td>Exactitud</td><td>Frena</td><td>Perror</td></tr>");
          $("#tab5-tablaMaterialObras").html("<tr class='cabecera'><td>Código</td><td>Descripción</td><td>Cantidad</td><td>Tipo</td><td>Cobro</td><td>RI</td><td>Entregado</td></tr>");
          $("#tab6-tablaUnidades").html("<tr class='cabecera'><td>Código</td><td>Descripción</td></tr>");
          $("#tab6-tablaIrregularidades").html("<tr class='cabecera'><td>Código</td><td>Descripción</td></tr>");
          $("#tab7-tablaCenso").html("<tr class='cabecera'><td>Código</td><td>Nombre</td><td>Cant.</td><td>Carga</td></tr>");
          $("#listaImagenes").html("");//resetamos la lista de imagenes
          $("#imgMaximizada").html("");//reseteamos a la imagen maximizada
          $("#fotoEvidencias").hide();
          //==============================================

          //==============================================

          $.ajax({
            url:'request/listarEvidenciasActas.php',
            type:'POST',
            dataType:'json',
            data:{acta:acta}

           }).done(function(repuesta){
              //console.log(repuesta);

                if(repuesta.length>0){

                 $("#listaImagenes").html("");//resetamos la lista de imagenes
                 $("#imgMaximizada").html("");//reseteamos a la imagen maximizada
                 $("#title-img").html("");//reseteamos el titulo de la imagen maximizada

                 var arr_img={};//vector q almacena las imagenes de cada nodo
                  
                  for(var j=0 ;j<repuesta.length; j++){//for que carga las imagenes del acta
                    arr_img[j]=new Image();
                    arr_img[j].onload=function(){};
                    arr_img[j].src=repuesta[j].url_img;
                    arr_img[j].name=j;
                    arr_img[j].width=600;
                    arr_img[j].height=400;
                    arr_img[j].title="Imagen "+(j+1)+" de "+repuesta.length;
                    
                    $("#listaImagenes").append(arr_img[j]);
                                        
                  }//fin del for que carga las imagenes del acta
                  var imgActual=0;
                  var priImg= $("#listaImagenes img:first-child").clone();
                  //console.log(priImg);
                  $("#imgMaximizada").html(priImg);
                  $("#listaImagenes img:first-child").addClass("img-selected");
                  $("#title-img").html("Imagen 1 de "+repuesta.length);

                  //EVENTO CLIC PARA CADA IMAGEN DEL CARRUSEL
                  $("#listaImagenes img").click(function(event){

                          imgActual=+($(this).attr("name"));
                    //--------------------------------------------------
                          if(imgActual<(repuesta.length-1)){
                            $("#siguiente").show();
                          }
                          else{
                            $("#siguiente").hide();
                          }
                    //--------------------------------------------------
                    //--------------------------------------------------
                          if(imgActual>0){
                            $("#anterior").show();
                          }
                          else{
                            $("#anterior").hide();
                          }
                    //--------------------------------------------------
                    $("#listaImagenes img").removeClass("img-selected");
                    var img=$(this).clone();                   
                    $("#imgMaximizada").html(img);
                    $(this).addClass("img-selected");
                    $("#title-img").html("Imagen "+(+$(this).attr("name")+1)+" de "+repuesta.length);
                    
                    //console.log("imgActual: "+imgActual);
                                        
                  });//fin del evento clic de las imagenes del carrusel

                  $("#siguiente").click(function(event){
                      event.preventDefault();
                      if(imgActual<(repuesta.length-1)){

                          imgActual++;

                          //--------------------------------------------------
                          if(imgActual<(repuesta.length-1)){
                            $("#siguiente").show();
                          }
                          else{
                            $("#siguiente").hide();
                          }

                          if(imgActual!=0){
                            $("#anterior").show();
                          }
                          //--------------------------------------------------

                        $("#listaImagenes img").removeClass("img-selected");
                        
                        //console.log("imgActual: "+imgActual);
                        
                        $("#imgMaximizada").html($("#listaImagenes img[name='"+imgActual+"']").clone());
                        $("#title-img").html("Imagen "+(imgActual+1)+" de "+repuesta.length);
                        $("#listaImagenes img[name='"+imgActual+"']").addClass("img-selected");
                      }
                  });

                  $("#anterior").click(function(){
                      event.preventDefault();
                      if(imgActual>0){
                        imgActual--;
                        //--------------------------------------------------
                          if(imgActual>0){
                            $("#anterior").show();
                          }
                          else{
                            $("#anterior").hide();
                          }

                          if(imgActual!=(repuesta.length-1)){
                            $("#siguiente").show();
                          }
                          //--------------------------------------------------
                        $("#listaImagenes img").removeClass("img-selected");
                        
                        //console.log("imgActual: "+imgActual);
                        $("#imgMaximizada").html($("#listaImagenes img[name='"+imgActual+"']").clone());
                        $("#title-img").html("Imagen "+(imgActual+1)+" de "+repuesta.length);
                        $("#listaImagenes img[name='"+imgActual+"']").addClass("img-selected");
                      }
                  });

                  $("#fotoEvidencias").show();
                }//fin del if si hay resultado
                else{
                  $("#listaImagenes").html("");//resetamos la lista de imagenes
                  $("#imgMaximizada").html("");//reseteamos a la imagen maximizada                  
                  $("#fotoEvidencias").hide();
                }
                listarEvidencias=true;
                //loading++;
                //$("#buscar").button({label:"("+Math.floor(((loading*100)/10))+"%)"});
                if(getCabecera&&getCenso&&getCliente&&getMas&&getMateriales&&getMedidor&&getSellos&&getUnidadesIrregularidades&&getPruebasMedidor){

                  //$("#buscar").button({label:"Buscar"});
                  //$("#buscar").button("enable");
                  
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
           });
          //==============================================


          $.ajax({

              url:'request/getCabecera.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}

          }).done(function(repuesta){

              if(repuesta.length>0){// hay resultado
                  //console.log(repuesta);
              
                  $("#box1-txtorden").val(repuesta[0].orden);
                  $("#box1-txtCodDpto").val(repuesta[0].codDpto);
                  $("#box1-txtNombreDpto").val(repuesta[0].nombreDpto); 
                  $("#box1-txtCodMun").val(repuesta[0].codMun);
                  $("#box1-txtNombreMun").val(repuesta[0].nombreMun);
                  $("#box1-codCampana").val(repuesta[0].codCampana);
                  $("#box1-txtNombreCampana").val(repuesta[0].nombreCampana);
                  $("#box1-txtFecha").datepicker("setDate", repuesta[0].fechaEjecucion);
                  $("#box1-txtHora").val(repuesta[0].horaEjecucion);

                  $("#box2-txtCodTecnico").val(repuesta[0].codTecnico);
                  $("#box2-txtNombreTecnico").val(repuesta[0].nombreTecnico);
                  $("#box2-txtSupervisor").val(repuesta[0].supervisor);
                  $("#box2-txtCuadrilla").val(repuesta[0].cuadrilla);
                  $("#box2-txtCodLineaAccion").val(repuesta[0].codLinea);
                  $("#box2-txtNombreLineaAccion").val(repuesta[0].nombreLinea);
                  $("#box2-txtCodSublinea").val(repuesta[0].codSubLinea);
                  $("#box2-txtNombreSublinea").val(repuesta[0].nombreSubLinea);
                  $("#box2-txtTos").val(repuesta[0].tipoOrden);
                  $("#box2-txtPlaca").val(repuesta[0].placa);

                  $("#box3-txtCodMotivo").val(repuesta[0].codMotivo);
                  $("#box3-txtNombreMotivo").val(repuesta[0].nombreMotivo);
                  $("#box3-txtCodAccion").val(repuesta[0].codAccion);
                  $("#box3-txtNombreAccion").val(repuesta[0].nombreAccion);

                  $("#txtObservaciones").html(repuesta[0].observaciones);
              }
              else{// no hay resultado
                  //alert("No hay informacion con este numero de acta");
              }

              getCabecera=true;
              
                if(listarEvidencias&&getCenso&&getCliente&&getMas&&getMateriales&&getMedidor&&getSellos&&getUnidadesIrregularidades&&getPruebasMedidor){

                                    
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
              
          }); 

          //==============================================
          $.ajax({
              url:'request/getCliente.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if(repuesta.length>0)
              {
                $("#tab1-txtNic").val(repuesta[0].nic);
                $("#tab1-txtNif").val(repuesta[0].nif);
                $("#tab1-txtNis").val(repuesta[0].nis);
                $("#tab1-txtEstrato").val(repuesta[0].estrato);

                $("#tab1-txtDireccion").val(repuesta[0].direccion);
                $("#tab1-txtNombre").val(repuesta[0].nombre);
                $("#tab1-txtCodBarrio").val(repuesta[0].codBarrio);
                $("#tab1-txtNombreBarrio").val(repuesta[0].nombreBarrio);
                $("#tab1-txtCodPredio").val(repuesta[0].codEstadoPredio);
                $("#tab1-txtNombrePredio").val(repuesta[0].nombreEstadoPredio);

                $("#tab1-txtAtendio").val(repuesta[0].personaAtendio);
                $("#tab1-txtMt").val(repuesta[0].mt);
                $("#tab1-txtCt").val(repuesta[0].ct);

                  if(repuesta[0].tipocenso=="DICTADO"){
                    $("#tab7-chkVerificado").prop('checked', false);
                    $("#tab7-chkDictado").prop('checked', true);
                    
                  }else{

                    $("#tab7-chkDictado").prop('checked', false);
                    $("#tab7-chkVerificado").prop('checked', true);
                  }

              }else
              {
                //alert("No se encontró datos del cliente para este numero de acta");
              }
              getCliente=true;
              
              
                if(listarEvidencias&&getCenso&&getCabecera&&getMas&&getMateriales&&getMedidor&&getSellos&&getUnidadesIrregularidades&&getPruebasMedidor){

                                    
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });          
          //================================================ 

          $.ajax({
              url:'request/getMedidor.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if(repuesta.length>0){

                for(var i=0;i<repuesta.length;i++){

                    var meins = repuesta[i].meins;
                    var fieldset;

                    if(meins=="ENCONTRADO"){
                      fieldset="fieldset1";
                    }else if(meins=="RETIRADO"){
                      fieldset="fieldset2";
                      $("#tab2-"+fieldset+"-txtCodCausa").val(repuesta[i].codRetiro);
                      $("#tab2-"+fieldset+"-txtNombreCausa").val(repuesta[i].nombreRetiro);
                    }
                    else{
                      fieldset="fieldset3";
                    }
                    
                    $("#tab2-"+fieldset+"-txtCodMedida").val(repuesta[i].codMedida);
                    $("#tab2-"+fieldset+"-txtNombreMedida").val(repuesta[i].nombreMedida);

                    $("#tab2-"+fieldset+"-txtCodCodigo").val(repuesta[i].codMedidor);
                    $("#tab2-"+fieldset+"-txtNombreCodigo").val(repuesta[i].nombreMedidor);

                    $("#tab2-"+fieldset+"-txtCodMarca").val(repuesta[i].codMarca);
                    $("#tab2-"+fieldset+"-txtNombreMarca").val(repuesta[i].nombreMarca);

                    $("#tab2-"+fieldset+"-txtSerie").val(repuesta[i].serie);

                    $("#tab2-"+fieldset+"-txtLectura").val(repuesta[i].lectura);

                    $("#tab2-"+fieldset+"-txtDigitos").val(repuesta[i].nroDigitos);

                    $("#tab2-"+fieldset+"-txtFases").val(repuesta[i].nroFases);

                    $("#tab2-"+fieldset+"-txtRango").val(repuesta[i].rango);

                    $("#tab2-"+fieldset+"-txtKd").val(repuesta[i].kd);

                    $("#tab2-"+fieldset+"-txtKh").val(repuesta[i].kh);

                }

              }else{
                //alert("No se encontró datos de medidores para este núumero de acta");
              }

              getMedidor=true;
              //loading++;
              //$("#buscar").button({label:"("+Math.floor(((loading*100)/10))+"%)"});
                if(listarEvidencias&&getCenso&&getCabecera&&getMas&&getMateriales&&getCliente&&getSellos&&getUnidadesIrregularidades&&getPruebasMedidor){

                  /*$("#buscar").button({label:"Buscar"});
                  $("#buscar").button("enable")*/
                  
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          
          //================================================


          $.ajax({
              url:'request/getMas.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  
                  $("#tab2-fieldset4-txtCodPropiedad").val(repuesta[0].codPropiedad);
                  $("#tab2-fieldset4-txtNombrePropiedad").val(repuesta[0].propiedad);
                  $("#tab2-fieldset4-txtCodTipoFase").val(repuesta[0].codTipoFase);
                  $("#tab2-fieldset4-txtNombreTipoFase").val(repuesta[0].tipoFase);
                  $("#tab2-fieldset4-txtCodTension").val(repuesta[0].codTension);
                  $("#tab2-fieldset4-txtNombreTension").val(repuesta[0].tension);
                  $("#tab2-fieldset4-txtCodIntensidad").val(repuesta[0].codIntensidad);
                  $("#tab2-fieldset4-txtNombreIntensidad").val(repuesta[0].intensidad);
                  $("#tab2-fieldset4-txtCodRegulador").val(repuesta[0].codRegulador);
                  $("#tab2-fieldset4-txtNombreRegulador").val(repuesta[0].regulador);
                  $("#tab2-fieldset4-txtCodNaturaleza").val(repuesta[0].codNaturaleza);
                  $("#tab2-fieldset4-txtNombreNaturaleza").val(repuesta[0].naturaleza);

              }else
              {
                    //alert("No se encontraron sellos para este numero de acta");
              }
              getMas=true;
              
                if(listarEvidencias&&getCenso&&getCabecera&&getMedidor&&getMateriales&&getCliente&&getSellos&&getUnidadesIrregularidades&&getPruebasMedidor){

                  
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          //====================================================
          $.ajax({
              url:'request/getSellos.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  
                  for (var i = 0; i<repuesta.length;i++) {
                      $("#tab3-tablaSello")
                          .append(
                              "<tr class='fila'><td>"+repuesta[i].sello+" </td><td>"+repuesta[i].ubicacion+" </td><td>"+repuesta[i].serie+"</td><td>"+repuesta[i].codSello+"</td><td>"+repuesta[i].marca+"</td><td>"+repuesta[i].causa+"</td><td>"+repuesta[i].nombre+"</td></tr>");
                  }

              }else
              {
                    //alert("No se encontraron sellos para este numero de acta");
              }
              getSellos=true;
              
              
                if(listarEvidencias&&getCenso&&getCabecera&&getMedidor&&getMateriales&&getCliente&&getMas&&getUnidadesIrregularidades&&getPruebasMedidor){

                                    
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          //====================================================

          $.ajax({
              url:'request/getMateriales.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  
                  for (var i = 0; i<repuesta.length;i++) {
                      $("#tab5-tablaMaterialObras")
                          .append(
                              "<tr class='fila'><td>"+repuesta[i].codigo+" </td><td>"+repuesta[i].descripcion+" </td><td>"+repuesta[i].cantidad+"</td><td>"+repuesta[i].tipo+"</td><td>"+repuesta[i].cobro+"</td><td>"+repuesta[i].ri+"</td><td>"+repuesta[i].entregado+"</td></tr>");
                  }

              }else
              {
                    //alert("No se encontraron sellos para este numero de acta");
              }
              getMateriales=true;
              
                if(listarEvidencias&&getCenso&&getCabecera&&getMedidor&&getSellos&&getCliente&&getMas&&getUnidadesIrregularidades&&getPruebasMedidor){

                                    
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          //====================================================

          //====================================================

          $.ajax({
              url:'request/getCenso.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  var totalCarga=0;
                  for (var i = 0; i<repuesta.length;i++) {
                      totalCarga+=(+repuesta[i].cantidad)*(+repuesta[i].carga);
                      $("#tab7-tablaCenso")
                          .append(
                              "<tr class='fila'><td>"+repuesta[i].codigo+" </td><td>"+repuesta[i].nombre+" </td><td>"+repuesta[i].cantidad+"</td><td>"+repuesta[i].carga+"</td></tr>");
                  }

                  $("#tab7-txtTotalCarga").val(totalCarga);

              }else
              {
                    //alert("No se encontraron sellos para este numero de acta");
              }
              getCenso=true;
              
                if(listarEvidencias&&getMateriales&&getCabecera&&getMedidor&&getSellos&&getCliente&&getMas&&getUnidadesIrregularidades&&getPruebasMedidor){

                  
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          //====================================================

           $.ajax({
              url:'request/getUnidadesIrregularidades.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  //console.log(repuesta);
                  for (var i = 0; i<repuesta.length;i++) {
                      if(repuesta[i].tipo="I"){
                        $("#tab6-tablaIrregularidades")
                          .append("<tr class='fila'><td>"+repuesta[i].codigo+"</td><td>"+repuesta[i].nombre+"</td></tr>");
                      }
                      else if(repuesta[i].tipo="U"){
                          $("#tab6-tablaUnidades")
                          .append("<tr class='fila'><td>"+repuesta[i].codigo+"</td><td>"+repuesta[i].nombre+"</td></tr>");
                      }
                      
                  }

              }else
              {
                    //alert("No se encontraron unidades / irregularidades para este numero de acta");
              }
              getUnidadesIrregularidades=true;
              
                if(listarEvidencias&&getMateriales&&getCabecera&&getMedidor&&getSellos&&getCliente&&getMas&&getCenso&&getPruebasMedidor){

                  
                  
                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });

          //====================================================
          $.ajax({
              url:'request/getPruebasMedidor.php',
              type:'POST',
              dataType:'json',
              data:{acta:acta}
          }).done(function(repuesta){

              if (repuesta.length>0) {
                  
                  for (var i = 0; i<repuesta.length;i++) {
                      $("#tab4-tablaPruebasMedidor")
                          .append("<tr class='fila'><td>"+repuesta[i].factor+"</td><td>"+repuesta[i].fase+"</td><td>"+repuesta[i].voltaje+"</td><td>"+repuesta[i].lectInicial+"</td><td>"+repuesta[i].lectFinal+"</td><td>"+repuesta[i].dosifica+"</td><td>"+repuesta[i].giro+"</td><td>"+repuesta[i].exactitud+"</td><td>"+repuesta[i].frena+"</td><td>"+repuesta[i].perror+"</td></tr>");
                  }

              }else
              {
                    
              }
              getPruebasMedidor=true;
              
                if(listarEvidencias&&getCenso&&getCabecera&&getMedidor&&getSellos&&getCliente&&getMas&&getUnidadesIrregularidades&&getMateriales){

                  

                  $("#consultando").hide();
                  $("#contenido").show();
                  $("#consultar").show();
                }
          });
          //====================================================

        };
        //fin de la  funcion buscar



        $( "#alert" ).dialog({
            modal: true,
            autoOpen: false,
            resizable:false,
            buttons: {
              Ok: function() {
                
                $( this ).dialog( "close" );
              }
            }
          });


        $("#form").on("submit", buscar);
        
        $("#carrusel").hide();       
        $("#contenido").hide();
        $("#consultando").hide();      
        
       
        /*================= datepicker ========================*/
        $.datepicker.regional['es'] = {
           closeText: 'Cerrar',
           prevText: '<Ant',
           nextText: 'Sig>',
           currentText: 'Hoy',
           monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
           monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
           dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
           dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
           dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
           weekHeader: 'Sm',
           dateFormat: 'dd/mm/yy',
           firstDay: 1,
           isRTL: false,
           showMonthAfterYear: false,
           yearSuffix: ''
           };
           $.datepicker.setDefaults($.datepicker.regional['es']);

        $("#box1-txtFecha").datepicker({

              changeMonth: true,
              changeYear: true,
              dateFormat:"yy-mm-dd"
        });
        $( "#box1-txtFecha" ).datepicker( "option", "disabled",true );
        /*================= fin datepicher ========================*/
        /*================= timepicher ============================*/
        //$("#box1-txtHora").timepicker({ timeFormat: 'h:mm:ss p' });
        /*================= timepicher ============================*/

        $("#tabs").tabs({show: { effect: "slide", duration: 200 }});

        
        $("#consultar").click(buscar);

        $("body").show();
        $("#acta").focus();


      });//fin del onready

  	</script>

</head>

<body style="display:none">

  <div id="alert" title="Mensaje">
    <p>No hay actas para mostrar.</p>
  </div>
  

  <header>
    <h3>Consultar actas</h3>
    <nav>
      <ul id="menu">
        
        <!--<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>-->
        <li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
        <li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>       
          
      </ul>
    </nav>
    
  </header>
  
  


  <div id='subheader'>

    <div id="div-form">
      <form id="form">

        <div><label>Acta</label><input type="text" id="acta" name="acta" autofocus></div>
        
        
      </form>
    
    </div>
    
  </div>    
    

    <div id="contenido">
        <!--======================= seccion1 ==============================-->
        <div id="section1">

              <div id="box1"><!-- box1 -->
                <div class="box-container">
                    <div id="actaYorden">
                        
                      <div id="infoOrden">
                          <div>
                            <label>No. Orden</label>
                            <input type="text" id="box1-txtorden" readOnly >
                          </div>
                      </div>
                    </div>

                  
                  <div id="infoDpto">
                        <label>Departamento</label>
                        <input type="text" id="box1-txtCodDpto" readOnly class="input-cod">
                        <input type="text" id="box1-txtNombreDpto" readOnly class="input-nombre">
                  </div>

                  <div id="infoMun">
                        <label>Municipio</label>
                        <input type="text" id="box1-txtCodMun" class="input-cod" readOnly>
                        <input type="text" id="box1-txtNombreMun" class="input-nombre"readOnly>
                  </div>

                  <div id="infoCampana">
                        <label>Campaña</label>
                        <input type="text" id="box1-codCampana" class="input-cod" readOnly>
                        <input type="text" id="box1-txtNombreCampana" class="input-nombre" readOnly>
                  </div>

                  <div id="fechaYhora">
                        <div><label>Fecha ejecución</label><input type="text" id="box1-txtFecha" readOnly></div>
                        <div><label>Hora ejecución</label><input type="time" value=""id="box1-txtHora" readOnly></div>
                  </div>
                 </div> 

              </div>




              <div id="box2">
                <div class="box-container">
                  <div id="infoTecnico">
                        <label>Técnico</label>
                        <input type="text" id="box2-txtCodTecnico" class="input-cod" readOnly>
                        <input type="text" id="box2-txtNombreTecnico" class="input-nombre" readOnly>
                  </div>

                  <div id="supervisorYcuadrilla">
                        <div><label>Supervisor</label><input type="text" id="box2-txtSupervisor" readOnly></div>
                        <div><label>Cuadrilla</label><input type="text" id="box2-txtCuadrilla" readOnly></div>
                  </div>

                  <div id="accion">
                        <label>L. acción</label>
                        <input type="text" id="box2-txtCodLineaAccion" class="input-cod" readOnly>
                        <input type="text" id="box2-txtNombreLineaAccion" class="input-nombre" readOnly>
                  </div>

                  <div id="sublinea">
                        <label>Sub línea</label>
                        <input type="text" id="box2-txtCodSublinea" class="input-cod" readOnly>
                        <input type="text" id="box2-txtNombreSublinea" class="input-nombre" readOnly>
                  </div>

                  <div id="tosYplacaYtipser">
                        <div><label>T.O.S</label><input type="text" id="box2-txtTos" readOnly></div>
                        <div><label>Placa</label><input type="text" id="box2-txtPlaca" readOnly></div>
                        <div><label>TipSer</label><input type="text" id="box2-txtTipServ" readOnly></div>
                  </div>
                </div>
              </div>




              <div id="box3">
                <div class="box-container">
                  <fieldset>
                        <legend>Resultado</legend>

                        <div id="motivo">
                            <label>Motivo</label>
                            <input type="text" id="box3-txtCodMotivo" class="input-cod" readOnly>
                            <input type="text" id="box3-txtNombreMotivo" class="input-nombre" readOnly>
                        </div>

                        <div id="box3-accion">
                            <label>Acción</label>
                            <input type="text" id="box3-txtCodAccion" class="input-cod" readOnly>
                            <input type="text" id="box3-txtNombreAccion" class="input-nombre" readOnly>
                        </div>

                  </fieldset> 
                  </div>
              </div>

        </div>
        <!--====================fin seccion1 ==============================-->

        <!--====================== seccion2 ===============================-->
        <div id="seccion2">
          
          <div id="tabs">
            <ul>
                <li><a href="#tab1">1. <span class='titulo-tab'>Cliente</span></a></li>
                <li><a href="#tab2">2. <span class='titulo-tab'>Medidor(es)</span></a></li>
                <li><a href="#tab3">3. <span class='titulo-tab'>Sellos</span></a></li>
                <li><a href="#tab4">4. <span class='titulo-tab'>Pruebas</span></a></li>
                <li><a href="#tab5">5. <span class='titulo-tab'>Mat. obras</span></a></li>
                <li><a href="#tab6">6. <span class='titulo-tab'>Unid. / Irreg.</span></a></li>
                <li><a href="#tab7">7. <span class='titulo-tab'>Anom. / Censo</span></a></li>
                <li><a href="#tab8">8. <span class='titulo-tab'>Evidencias</span></a></li>
            </ul>

            <div id="tab1">
                  <div class="tab-container">
                   
                      <div id="tab1-box1">
                        <div class="tab1-box-container">
                          <div id="nicYnif">
                              <div><label>Nic</label><input type="text" id="tab1-txtNic" readOnly></div>
                              <div><label>Nif / Dig</label><input type="text" id="tab1-txtNif" readOnly></div>
                          </div>
                          <div><label>Nombre</label><input type="text" id="tab1-txtNombre" readOnly></div>
                          <div><label>Dirección</label><input type="text" id="tab1-txtDireccion" readOnly></div>
                          <div><label>Barrio</label><input type="text" id="tab1-txtCodBarrio" readOnly><input type="text" id="tab1-txtNombreBarrio" readOnly></div>
                          <div><label>Est. predio</label><input type="text" id="tab1-txtCodPredio" readOnly><input type="text" id="tab1-txtNombrePredio" readOnly></div>
                        </div>
                      </div>

                      <div id="tab1-box2">
                        <div class="tab1-box-container">
                          <div id="nisYestrato">
                              <div><label>Nis / Ruta</label><input type="text" id="tab1-txtNis" readOnly></div>
                              <div><label>Estrato</label><input type="text" id="tab1-txtEstrato" readOnly></div>
                          </div>
                          <div><label>Persona atendió</label><input type="text" id="tab1-txtAtendio" readOnly></div>
                          <div id="mtYct">
                            <div><label>MT</label><input type="text" id="tab1-txtMt" readOnly></div>
                            <div><label>CT</label><input type="text" id="tab1-txtCT" readOnly></div>
                          </div>
                        </div>
                      </div>

                      <div id="tab1-box3">
                        <div class="tab1-box-container">
                          <div><label>Observaciones</label><textarea  id="txtObservaciones" readOnly ></textarea></div>
                        </div>
                      </div>
                          
                    
                  </div>
            </div><!--fin tab1-->

            <div id="tab2">
                  <div class="tab-container">

                      <div class="container_fieldset">
                        <fieldset>
                          <legend>Encontrado</legend>
                          <div>
                            <label>Medida</label>
                            <input type="text" id="tab2-fieldset1-txtCodMedida"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset1-txtNombreMedida" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Código</label>
                            <input type="text" id="tab2-fieldset1-txtCodCodigo"  class="tab2-txtCodCodigo" readOnly>
                            <input type="text" id="tab2-fieldset1-txtNombreCodigo" class="tab2-txtNombreCodigo"readOnly>
                          </div>

                          <div>
                            <label>Marca</label>
                            <input type="text" id="tab2-fieldset1-txtCodMarca"  class="tab2-txtCodMarca" readOnly>
                            <input type="text" id="tab2-fieldset1-txtNombreMarca"  class="tab2-txtNombreMarca" readOnly>
                          </div>

                          <div>
                            <label>Serie</label>
                            <input type="text" id="tab2-fieldset1-txtSerie"  class="tab2-txtSerie" readOnly>
                          </div>

                          <div id="lecturaYdigitos">
                            <div><label>Lectura</label><input type="text" id="tab2-fieldset1-txtLectura" class="tab2-txtLectura" readOnly></div>
                            <div><label>No. Dígitos</label><input type="text" id="tab2-fieldset1-txtDigitos" class="tab2-txtDigitos" readOnly></div>
                          </div>

                          <div id="kdYfases">
                            <div><label>Kd (rev/kWh)</label><input type="text" id="tab2-fieldset1-txtKd" class="tab2-txtKd" readOnly></div>
                            <div><label>No. Fases</label><input type="text" id="tab2-fieldset1-txtFases" class="tab2-txtFases" readOnly></div>
                          </div>

                          <div id="khYrango">
                            <div><label>Kh (kWh/rev)</label><input type="text" id="tab2-fieldset1-txtKh" class="tab2-txtKh" readOnly></div>
                            <div><label>Rango Corriente</label><input type="text" id="tab2-fieldset1-txtRango" class="tab2-txtRango" readOnly></div>
                          </div>

                          <div>
                            <label>Long. acometida</label>
                            <input type="text" id="tab2-fieldset1-longitudAcometida" class="tab2-longitudAcometida"  readOnly>
                          </div>
                        </fieldset>
                      </div>

                      <div class="container_fieldset">
                        <fieldset>
                          <legend>Retirado</legend>

                          <!--<div>
                            <label></label>
                            <input type="text" id="tab2-fieldset2-txt" class="tab2-longitudAcometida"  readOnly>
                          </div>-->

                          <div>
                            <label>Causa</label>
                            <input type="text" id="tab2-fieldset2-txtCodCausa"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset2-txtNombreCausa" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Medida</label>
                            <input type="text" id="tab2-fieldset2-txtCodMedida"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset2-txtNombreMedida" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Código</label>
                            <input type="text" id="tab2-fieldset2-txtCodCodigo"  class="tab2-txtCodCodigo" readOnly>
                            <input type="text" id="tab2-fieldset2-txtNombreCodigo" class="tab2-txtNombreCodigo"readOnly>
                          </div>

                          <div>
                            <label>Marca</label>
                            <input type="text" id="tab2-fieldset2-txtCodMarca"  class="tab2-txtCodMarca" readOnly>
                            <input type="text" id="tab2-fieldset2-txtNombreMarca"  class="tab2-txtNombreMarca" readOnly>
                          </div>

                          <div>
                            <label>Serie</label>
                            <input type="text" id="tab2-fieldset2-txtSerie"  class="tab2-txtSerie" readOnly>
                          </div>

                          <div id="lecturaYdigitos">
                            <div><label>Lectura</label><input type="text" id="tab2-fieldset2-txtLectura" class="tab2-txtLectura" readOnly></div>
                            <div><label>No. Dígitos</label><input type="text" id="tab2-fieldset2-txtDigitos" class="tab2-txtDigitos" readOnly></div>
                          </div>

                          <div id="kdYfases">
                            <div><label>Kd (rev/kWh)</label><input type="text" id="tab2-fieldset2-txtKd" class="tab2-txtKd" readOnly></div>
                            <div><label>No. Fases</label><input type="text" id="tab2-fieldset2-txtFases" class="tab2-txtFases" readOnly></div>
                          </div>

                          <div id="khYrango">
                            <div><label>Kh (kWh/rev)</label><input type="text" id="tab2-fieldset2-txtKh" class="tab2-txtKh" readOnly></div>
                            <div><label>Rango Corriente</label><input type="text" id="tab2-fieldset2-txtRango" class="tab2-txtRango" readOnly></div>
                          </div>

                          <div>
                            <label>Long. acometida</label>
                            <input type="text" id="tab2-fieldset2-longitudAcometida" class="tab2-longitudAcometida"  readOnly>
                          </div>
                        </fieldset>
                      </div>

                      <div class="container_fieldset">
                        <fieldset>
                          <legend>Instalado</legend>
                           <div>
                            <label>Medida</label>
                            <input type="text" id="tab2-fieldset3-txtCodMedida"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset3-txtNombreMedida" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Código</label>
                            <input type="text" id="tab2-fieldset3-txtCodCodigo"  class="tab2-txtCodCodigo" readOnly>
                            <input type="text" id="tab2-fieldset3-txtNombreCodigo" class="tab2-txtNombreCodigo"readOnly>
                          </div>

                          <div>
                            <label>Marca</label>
                            <input type="text" id="tab2-fieldset3-txtCodMarca"  class="tab2-txtCodMarca" readOnly>
                            <input type="text" id="tab2-fieldset3-txtNombreMarca"  class="tab2-txtNombreMarca" readOnly>
                          </div>

                          <div>
                            <label>Serie</label>
                            <input type="text" id="tab2-fieldset3-txtSerie"  class="tab2-txtSerie" readOnly>
                          </div>

                          <div id="lecturaYdigitos">
                            <div><label>Lectura</label><input type="text" id="tab2-fieldset3-txtLectura" class="tab2-txtLectura" readOnly></div>
                            <div><label>No. Dígitos</label><input type="text" id="tab2-fieldset3-txtDigitos" class="tab2-txtDigitos" readOnly></div>
                          </div>

                          <div id="kdYfases">
                            <div><label>Kd (rev/kWh)</label><input type="text" id="tab2-fieldset3-txtKd" class="tab2-txtKd" readOnly></div>
                            <div><label>No. Fases</label><input type="text" id="tab2-fieldset3-txtFases" class="tab2-txtFases" readOnly></div>
                          </div>

                          <div id="khYrango">
                            <div><label>Kh (kWh/rev)</label><input type="text" id="tab2-fieldset3-txtKh" class="tab2-txtKh" readOnly></div>
                            <div><label>Rango Corriente</label><input type="text" id="tab2-fieldset3-txtRango" class="tab2-txtRango" readOnly></div>
                          </div>

                          <div id="acometidaYventa">
                            <div>
                              <label>Long. acometida</label>
                              <input type="text" id="tab2-fieldset3-longitudAcometida" class="tab2-longitudAcometida"  readOnly>
                            </div>
                            
                            <div>

                              <input type="checkbox" id="chkVenta" readOnly>
                              <span>Venta permuta</span>
                            </div>
                            

                          </div>
                        </fieldset>
                      </div>

                      <div class="container_fieldset">
                        <fieldset>
                          <legend>Más</legend>
                           <div>
                            <label>Propiedad</label>
                            <input type="text" id="tab2-fieldset4-txtCodPropiedad"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombrePropiedad" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Tipo fase</label>
                            <input type="text" id="tab2-fieldset4-txtCodTipoFase"  class="tab2-txtCodCodigo" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombreTipoFase" class="tab2-txtNombreCodigo"readOnly>
                          </div>

                          <div>
                            <label>Tensión</label>
                            <input type="text" id="tab2-fieldset4-txtCodTension"  class="tab2-txtCodMarca" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombreTension"  class="tab2-txtNombreMarca" readOnly>
                          </div>

                           <div>
                            <label>Intensidad</label>
                            <input type="text" id="tab2-fieldset4-txtCodIntensidad"  class="tab2-txtCodMedida" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombreIntensidad" class="tab2-txtNombreMedida" readOnly>
                          </div>

                          <div>
                            <label>Regulador</label>
                            <input type="text" id="tab2-fieldset4-txtCodRegulador"  class="tab2-txtCodCodigo" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombreRegulador" class="tab2-txtNombreCodigo"readOnly>
                          </div>

                          <div>
                            <label>Naturaleza</label>
                            <input type="text" id="tab2-fieldset4-txtCodNaturaleza"  class="tab2-txtCodMarca" readOnly>
                            <input type="text" id="tab2-fieldset4-txtNombreNaturaleza"  class="tab2-txtNombreMarca" readOnly>
                          </div>
                        </fieldset>
                      </div>
                  </div>
            </div>

            <div id="tab3">
                  <div class="tab-container">
                    <table id="tab3-tablaSello">
                        <tr class="cabecera">
                            <td>Sellos</td>
                            <td>Ubicación</td>
                            <td>Serie</td>
                            <td>Cod. sello</td>
                            <td>Marca</td>
                            <td>Causa</td>
                            <td>Nombre</td>
                        </tr>
                        

                    </table>
                      
                  </div>
            </div>

            <div id="tab4">
                  <div class="tab-container">

                    <table id="tab4-tablaPruebasMedidor">
                        <tr class="cabecera">
                            <td>Factor</td>
                            <td>Fase</td>
                            <td>Volaje</td>
                            <td>Lect. Inicial</td>
                            <td>Lect. Final</td>
                            <td>Dosifica</td>
                            <td>Giro</td>
                            <td>Exactitud</td>
                            <td>Frena</td>
                            <td>Perror</td>
                        </tr>
                    </table>
                      
                  </div>
            </div>

            <div id="tab5">
                  <div class="tab-container">
                    <table id="tab5-tablaMaterialObras">
                        <tr class="cabecera">
                            <td>Código</td>
                            <td>Descripción</td>
                            <td>Cantidad</td>
                            <td>Tipo</td>
                            <td>Cobro</td>
                            <td>RI</td>
                            <td>Entregado</td>
                        </tr>
                        
                    </table>
                      
                  </div>
            </div>

            <div id="tab6">
                  <div class="tab-container">

                    <div>

                        <fieldset>

                          <legend>Unidades</legend>
                          <table id="tab6-tablaUnidades">
                              <tr class="cabecera">
                                  <td>Código</td>
                                  <td>Descripción</td>
                              </tr>

                              
                          </table>

                        </fieldset>

                    </div>

                    <div>

                        <fieldset>

                          <legend>Irregularidades</legend>
                          <table id="tab6-tablaIrregularidades">
                              <tr class="cabecera">
                                  <td>Código</td>
                                  <td>Descripción</td>
                              </tr>

                          </table>

                        </fieldset>

                    </div>
                      
                  </div>
            </div>

            <div id="tab7">
                  <div class="tab-container">

                      

                      <div>

                          <div>
                              <input type="radio" id="tab7-chkDictado" name="tab7-radio" readOnly disabled><span>Dictado</span>
                              <input type="radio" id="tab7-chkVerificado" name="tab7-radio" readOnly disabled><span>Verificado</span>
                              <span>Total carga</span><input type="text" id="tab7-txtTotalCarga" readOnly>

                          </div>

                          <fieldset>

                            <legend>Censo de carga</legend>
                            <table id="tab7-tablaCenso">
                                <tr class="cabecera">
                                    <td>Código</td>
                                    <td>Nombre</td>
                                    <td>Cant.</td>
                                    <td>Carga</td>
                                </tr>

                            </table>

                          </fieldset>

                      </div>

                      <div>

                        <fieldset>

                          <legend>Anomalias</legend>
                          <table id="tab7-tablaAnomalias">
                              <tr class="cabecera">
                                  <td>Código</td>
                                  <td>Nombre</td>
                              </tr>

                          </table>

                        </fieldset>

                      </div>
                      
                  </div>
            </div>

            <div id="tab8">
                  <div class="tab-container">
                    <div id="fotoEvidencias">

                        <div>
                            <div id="listaImagenes"></div>
                        </div>

                        <div>
                            <div id="div-img">
                                <div id="imgMaximizada"></div>
                                <a href='#' id='anterior'></a>
                                <a href='#' id='siguiente'></a>
                                <h6 id="title-img"></h6>
                              </div>
                        </div>

                    </div>
                      
                  </div>
            </div>

          </div>

        </div>
        <!--==================== fin seccion2 =============================-->

    </div>

  </body>
</html>