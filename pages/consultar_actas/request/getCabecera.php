<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT LC.ca_orden orden, lc.ca_depto Dpto,
					       d.de_nombre Nombre_Dpto,
					       lc.ca_municipio codigomun,
					       m.mu_nombre nombre_municipio,
					       lc.ca_campana cod_campana,
					       c.c_nombre nom_campana,
					       lc.ca_fechaej fecha_ejecucion,
					       lc.ca_horaej hora_ejecucion,
					       lc.ca_tecnico cod_tecnico,
					       lc.ca_ntecnicos nombre_tecnico,
					       lc.ca_nsupervisor supervisor,
					       lc.ca_cuadrilla cuadrilla,
					       lc.ca_lineaaccion cod_linea_accion,
					       la.la_nombre nombre_linea,
					       lc.ca_lineabolsa cod_sublinea,
					       b.bo_nombrebolsa nombre_sublinea,
					       lc.ca_codtipoordens tipo_orden,
					       lc.ca_placa placa,
					       DU.ui_codigo cod_motivo,
					       r.r_descripcion nombre_motivo,
					       dua.ui_codigo cod_accion,
					       a.ac_descripcion nombre_accion,
					       lc.ca_observa observaciones
				from lega_cabecera lc
							 left join departamentos d on d.de_codigo=lc.ca_depto
							 left join municipios m on m.mu_depto=lc.ca_depto and m.mu_codigomun=lc.ca_municipio
							 left join campanas c on c.c_codigo=lc.ca_campana
							 left join lineas_accion la on la.la_campana=lc.ca_campana and la.la_codigo=lc.ca_lineaaccion
							 left join bolsas b on b.bo_barrio=lc.ca_lineabolsa and b.bo_codigobolsa=lc.ca_lineaaccion
							 left join dato_unirre du on du.ui_acta=lc.ca_acta and du.ui_tipo='M'
							 left join dato_unirre dua on dua.ui_acta=lc.ca_acta and dua.ui_tipo='A'
							 left join resultado_scr r on r.r_tiporden=lc.ca_codtipoordens and r.r_codigo=du.ui_codigo
							 left join acciones a on a.ac_tiporden=lc.ca_codtipoordens and a.ac_codigo=dua.ui_codigo and a.ac_motivo=du.ui_codigo
				where lc.ca_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['orden'] = utf8_encode($fila[0]);
		$row_array['codDpto'] = utf8_encode($fila[1]);
		$row_array['nombreDpto'] = utf8_encode($fila[2]);
		$row_array['codMun'] = utf8_encode($fila[3]);
		$row_array['nombreMun'] = utf8_encode($fila[4]);
		$row_array['codCampana'] = utf8_encode($fila[5]);
		$row_array['nombreCampana'] = utf8_encode($fila[6]);
		$row_array['fechaEjecucion'] = utf8_encode($fila[7]);
		$row_array['horaEjecucion'] = utf8_encode($fila[8]);

		$row_array['codTecnico'] = utf8_encode($fila[9]);
		$row_array['nombreTecnico'] = utf8_encode($fila[10]);
		$row_array['supervisor'] = utf8_encode($fila[11]);
		$row_array['cuadrilla'] = utf8_encode($fila[12]);
		$row_array['codLinea'] = utf8_encode($fila[13]);
		$row_array['nombreLinea'] = utf8_encode($fila[14]);
		$row_array['codSubLinea'] = utf8_encode($fila[15]);
		$row_array['nombreSubLinea'] = utf8_encode($fila[16]);
		$row_array['tipoOrden'] = utf8_encode($fila[17]);
		$row_array['placa'] = utf8_encode($fila[18]);

		$row_array['codMotivo'] = utf8_encode($fila[19]);
		$row_array['nombreMotivo'] = utf8_encode($fila[20]);
		$row_array['codAccion'] = utf8_encode($fila[21]);
		$row_array['nombreAccion'] = utf8_encode($fila[22]);

		$row_array['observaciones'] = utf8_encode($fila[23]);
		
		array_push($return_arr, $row_array);
	}

	/*//prueba
	
		$row_array['orden'] = utf8_encode("prueba");
		$row_array['codDpto'] = utf8_encode("prueba");
		$row_array['nombreDpto'] = utf8_encode("prueba");
		$row_array['codMun'] = utf8_encode("prueba");
		$row_array['nombreMun'] = utf8_encode("prueba");
		$row_array['codCampana'] = utf8_encode("prueba");
		$row_array['nombreCampana'] = utf8_encode("prueba");
		$row_array['fechaEjecucion'] = utf8_encode("prueba");
		$row_array['horaEjecucion'] = utf8_encode("prueba");

		$row_array['codTecnico'] = utf8_encode("prueba");
		$row_array['nombreTecnico'] = utf8_encode("prueba");
		$row_array['supervisor'] = utf8_encode("prueba");
		$row_array['cuadrilla'] = utf8_encode("prueba");
		$row_array['codLinea'] = utf8_encode("prueba");
		$row_array['nombreLinea'] = utf8_encode("prueba");
		$row_array['codSubLinea'] = utf8_encode("prueba");
		$row_array['nombreSubLinea'] = utf8_encode("prueba");
		$row_array['tipoOrden'] = utf8_encode("prueba");
		$row_array['placa'] = utf8_encode("prueba");

		$row_array['codMotivo'] = utf8_encode("prueba");
		$row_array['nombreMotivo'] = utf8_encode("prueba");
		$row_array['codAccion'] = utf8_encode("prueba");
		$row_array['nombreAccion'] = utf8_encode("prueba");

		$row_array['observaciones'] = utf8_encode("prueba");
		
		array_push($return_arr, $row_array);
	
	//fin prueba*/

	echo json_encode($return_arr);
?>