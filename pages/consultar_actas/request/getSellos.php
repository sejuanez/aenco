<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT ds.se_meins SELLOS,
				       ds.se_ubica UBICACION,
				       ds.se_serie SERIE,
				       ds.se_sello COD_SELLO,
				       m.ma_descripcion DESCRIPCION_SELLO,
				       de.descripcion MARCA,
				       ds.se_crcodigo CAUSA,
				       ds.se_crnombre NOMBRE
				from dato_sellos ds
				 left join materiales m on m.ma_codigo=ds.se_sello
				 left join descripcion_series de on de.codigo=ds.se_marca and de.grupo='7'
				where ds.se_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['sello'] = utf8_encode($fila[0]);
		$row_array['ubicacion'] = utf8_encode($fila[1]);
		$row_array['serie'] = utf8_encode($fila[2]);
		$row_array['codSello'] = utf8_encode($fila[3]);
		$row_array['descripcion'] = utf8_encode($fila[4]);
		$row_array['marca'] = utf8_encode($fila[5]);
		$row_array['causa'] = utf8_encode($fila[6]);
		$row_array['nombre'] = utf8_encode($fila[7]);
						
		array_push($return_arr, $row_array);
	}

	/*//pruebas
	
		$row_array['sello'] = utf8_encode("prueba");
		$row_array['ubicacion'] = utf8_encode("prueba");
		$row_array['serie'] = utf8_encode("prueba");
		$row_array['codSello'] = utf8_encode("prueba");
		$row_array['descripcion'] = utf8_encode("prueba");
		$row_array['marca'] = utf8_encode("prueba");
		$row_array['causa'] = utf8_encode("prueba");
		$row_array['nombre'] = utf8_encode("prueba");

		array_push($return_arr, $row_array);
	

	//fin pruebas*/

	echo json_encode($return_arr);
?>