<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT dm.me_meins MEINS,
			       dm.me_medida COD_MEDIDA,
			       tc.tc_descriptipoconsumo NOMBRE_MEDIDA,
			       dm.me_medidor CODIGO,
			       m.ma_descripcion NOMBRE_MATERIAL,
			       dm.me_marca COD_MARCA,
			       ds.descripcion DES_MARCA,
			       dm.me_serie SERIE,
			       dm.me_lectura LECTURA,
			       dm.me_ndigitos NRO_DIGITOS,
			       dm.me_nfases NRO_FASES,
			       dm.me_rango RANGO,
			       dm.me_kd KD,
			       dm.me_kh KH,
			       dm.me_crcodigo COD_CAUSARET,
			       cr.cr_nombre NOMBRE_CAUSA
			from dato_medidor dm

			 left join tipo_consumo tc on tc.tc_cod_tipoaparato=dm.me_medida
			 left join materiales m on m.ma_codigo=dm.me_medidor
			 left join descripcion_series ds on ds.codigo=dm.me_marca and ds.grupo='1'
			 left join causa_retiro cr on cr.cr_codigo=dm.me_crcodigo  and cr.cr_tipo='1'
			where dm.me_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['meins'] = utf8_encode($fila[0]);
		$row_array['codMedida'] = utf8_encode($fila[1]);
		$row_array['nombreMedida'] = utf8_encode($fila[2]);
		$row_array['codMedidor'] = utf8_encode($fila[3]);
		$row_array['nombreMedidor'] = utf8_encode($fila[4]);
		$row_array['codMarca'] = utf8_encode($fila[5]);
		$row_array['nombreMarca'] = utf8_encode($fila[6]);
		$row_array['serie'] = utf8_encode($fila[7]);
		$row_array['lectura'] = utf8_encode($fila[8]);
		$row_array['nroDigitos'] = utf8_encode($fila[9]);
		$row_array['nroFases'] = utf8_encode($fila[10]);
		$row_array['rango'] = utf8_encode($fila[11]);
		$row_array['kd'] = utf8_encode($fila[12]);
		$row_array['kh'] = utf8_encode($fila[13]);
		$row_array['codRetiro'] = utf8_encode($fila[14]);
		$row_array['nombreRetiro'] = utf8_encode($fila[15]);

				
		array_push($return_arr, $row_array);
	}

	/*//prueba
	
		$row_array['meins'] = utf8_encode("prueba");
		$row_array['codMedida'] = utf8_encode("prueba");
		$row_array['nombreMedida'] = utf8_encode("prueba");
		$row_array['codMedidor'] = utf8_encode("prueba");
		$row_array['nombreMedidor'] = utf8_encode("prueba");
		$row_array['codMarca'] = utf8_encode("prueba");
		$row_array['nombreMarca'] = utf8_encode("prueba");
		$row_array['serie'] = utf8_encode("prueba");
		$row_array['lectura'] = utf8_encode("prueba");
		$row_array['nroDigitos'] = utf8_encode("prueba");
		$row_array['nroFases'] = utf8_encode("prueba");
		$row_array['rango'] = utf8_encode("prueba");
		$row_array['kd'] = utf8_encode("prueba");
		$row_array['kh'] = utf8_encode("prueba");
		$row_array['codRetiro'] = utf8_encode("prueba");
		$row_array['nombreRetiro'] = utf8_encode("prueba");

		array_push($return_arr, $row_array);
	
	//fin prueba*/

	echo json_encode($return_arr);
?>