<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT dc.dc_nic NIC,
			       dc.dc_nif NIF,
			       dc.dc_nis NIS,
			       dc.dc_nombre NOMBRE,
			       dc.dc_direccion DIRECCION,
			       dc.dc_cbarrio COD_BARRIO,
			       dc.dc_nbarrio NOMBRE_BARRIO,
			       dc.dc_estrato ESTRATO,
			       du.ui_codigo cod_estadopredio,
			       ep.ep_nombre nombre_estado_predio,
			       dc.dc_observa persona_atiende,
			       dc.dc_lecturae1 mt,
			       dc.dc_lecturae2 ct,
			       dc.dc_lecturaa tipocenso
			from dato_clientes dc
			  left join dato_unirre du on du.ui_acta=dc.dc_acta and du.ui_tipo='E'
			  left join estados_predios ep on ep.ep_codigo=du.ui_codigo
			where dc.dc_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['nic'] = utf8_encode($fila[0]);
		$row_array['nif'] = utf8_encode($fila[1]);
		$row_array['nis'] = utf8_encode($fila[2]);
		$row_array['nombre'] = utf8_encode($fila[3]);
		$row_array['direccion'] = utf8_encode($fila[4]);
		$row_array['codBarrio'] = utf8_encode($fila[5]);
		$row_array['nombreBarrio'] = utf8_encode($fila[6]);
		$row_array['estrato'] = utf8_encode($fila[7]);
		$row_array['codEstadoPredio'] = utf8_encode($fila[8]);
		$row_array['nombreEstadoPredio'] = utf8_encode($fila[9]);
		$row_array['personaAtendio'] = utf8_encode($fila[10]);
		$row_array['mt'] = utf8_encode($fila[11]);
		$row_array['ct'] = utf8_encode($fila[12]);
		$row_array['tipocenso'] = utf8_encode($fila[13]);

				
		array_push($return_arr, $row_array);
	}

	/*//prueba
	

		$row_array['nic'] = utf8_encode("prueba");
		$row_array['nif'] = utf8_encode("prueba");
		$row_array['nis'] = utf8_encode("prueba");
		$row_array['nombre'] = utf8_encode("prueba");
		$row_array['direccion'] = utf8_encode("prueba");
		$row_array['codBarrio'] = utf8_encode("prueba");
		$row_array['nombreBarrio'] = utf8_encode("prueba");
		$row_array['estrato'] = utf8_encode("prueba");
		$row_array['codEstadoPredio'] = utf8_encode("prueba");
		$row_array['nombreEstadoPredio'] = utf8_encode("prueba");
		$row_array['personaAtendio'] = utf8_encode("prueba");
		$row_array['mt'] = utf8_encode("prueba");
		$row_array['ct'] = utf8_encode("prueba");
		$row_array['tipocenso'] = utf8_encode("prueba");

		array_push($return_arr, $row_array);
	
	//fin prueba*/

	echo json_encode($return_arr);
?>