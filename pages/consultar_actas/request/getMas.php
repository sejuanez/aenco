<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT dma.dma_cod_propiedad,  tda.descripcion  nombre_propiedad,
			       dma.dma_cod_tipo_fase,  tdaf.descripcion nombre_tipo_fase,
			       dma.dma_cod_tension,    tdat.descripcion nombre_tension,
			       dma.dma_cod_intensidad, tdai.descripcion nombre_intensidad,
			       dma.dma_cod_regulador,  tdar.descripcion nombre_regulador,
			       dma.dma_cod_naturaleza,  tdan.descripcion nombre_naturaleza
					from  dato_medidor_adicionales dma
					 left join tipo_datos_adicion_medidor tda  on tda.tipo='PROPIEDAD' and tda.codigo=dma.dma_cod_propiedad
					 left join tipo_datos_adicion_medidor tdaf on tdaf.tipo='TIPO_FASE' and tdaf.codigo=dma.dma_cod_tipo_fase
					 left join tipo_datos_adicion_medidor tdat on tdat.tipo='TENSION'  and tdat.codigo=dma.dma_cod_tension
					 left join tipo_datos_adicion_medidor tdai on tdai.tipo='INTENSIDAD' and tdai.codigo=dma.dma_cod_intensidad
					 left join tipo_datos_adicion_medidor tdar on tdar.tipo='REGULADOR'  and tdar.codigo=dma.dma_cod_regulador
					 left join tipo_datos_adicion_medidor tdan on tdan.tipo='NATURALEZA' and tdan.codigo=dma.dma_cod_naturaleza
					where dma.dma_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['codPropiedad'] = utf8_encode($fila[0]);
		$row_array['propiedad'] = utf8_encode($fila[1]);
		$row_array['codTipoFase'] = utf8_encode($fila[2]);
		$row_array['tipoFase'] = utf8_encode($fila[3]);
		$row_array['codTension'] = utf8_encode($fila[4]);
		$row_array['tension'] = utf8_encode($fila[5]);
		$row_array['codIntensidad'] = utf8_encode($fila[6]);
		$row_array['intensidad'] = utf8_encode($fila[7]);
		$row_array['codRegulador'] = utf8_encode($fila[8]);
		$row_array['regulador'] = utf8_encode($fila[9]);
		$row_array['codNaturaleza'] = utf8_encode($fila[10]);
		$row_array['naturaleza'] = utf8_encode($fila[11]);
		
								
		array_push($return_arr, $row_array);
	}

	/*//prueba
	
		$row_array['codPropiedad'] = utf8_encode("prueba");
		$row_array['propiedad'] = utf8_encode("prueba");
		$row_array['codTipoFase'] = utf8_encode("prueba");
		$row_array['tipoFase'] = utf8_encode("prueba");
		$row_array['codTension'] = utf8_encode("prueba");
		$row_array['tension'] = utf8_encode("prueba");
		$row_array['codIntensidad'] = utf8_encode("prueba");
		$row_array['intensidad'] = utf8_encode("prueba");
		$row_array['codRegulador'] = utf8_encode("prueba");
		$row_array['regulador'] = utf8_encode("prueba");
		$row_array['codNaturaleza'] = utf8_encode("prueba");
		$row_array['naturaleza'] = utf8_encode("prueba");
		
								
		array_push($return_arr, $row_array);
	
	//fin prueba*/

	echo json_encode($return_arr);
?>