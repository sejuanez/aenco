<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$acta=$_POST["acta"];
	
	
	$consulta = "SELECT
					pr.pm_factor factor,
					pr.pm_fase fase,
					pr.pm_voltaje voltaje,
					PR.pm_corriente lect_inicial,
					pr.pm_tiempo    lect_final,
					pr.pm_vueltas   dosifica,
					pr.pm_giro      giro,
					pr.pm_rozamiento exactitud,
					pr.pm_frena  frena,
					pr.pm_perror perror
				FROM dato_pruebamedidor pr
				where pr.pm_acta='".$acta."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['factor'] = utf8_encode($fila[0]);
		$row_array['fase'] = utf8_encode($fila[1]);
		$row_array['voltaje'] = utf8_encode($fila[2]);
		$row_array['lectInicial'] = utf8_encode($fila[3]);
		$row_array['lectFinal'] = utf8_encode($fila[4]);
		$row_array['dosifica'] = utf8_encode($fila[5]);
		$row_array['giro'] = utf8_encode($fila[6]);
		$row_array['exactitud'] = utf8_encode($fila[7]);
		$row_array['frena'] = utf8_encode($fila[8]);
		$row_array['perror'] = utf8_encode($fila[9]);
								
		array_push($return_arr, $row_array);
	}

	/*//prueba

	
		
		$row_array['factor'] = utf8_encode("prueba");
		$row_array['fase'] = utf8_encode("prueba");
		$row_array['voltaje'] = utf8_encode("prueba");
		$row_array['lectInicial'] = utf8_encode("prueba");
		$row_array['lectFinal'] = utf8_encode("prueba");
		$row_array['dosifica'] = utf8_encode("prueba");
		$row_array['giro'] = utf8_encode("prueba");
		$row_array['exactitud'] = utf8_encode("prueba");
		$row_array['frena'] = utf8_encode("prueba");
		$row_array['perror'] = utf8_encode("prueba");
								
		array_push($return_arr, $row_array);
	
	//fin prueba*/
		
	echo json_encode($return_arr);
?>