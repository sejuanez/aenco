<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
  $fechafin = $_POST['fechafin'];
  $dpto = $_POST['dpto'];
	$proceso = $_POST['proceso'];
	
	
	$consulta = "SELECT distinct
                   o.ot_solicitud,
                   o.ot_fecha_asigna_contrata,
                   o.ot_fechaej,
                   oec.ote_fecha_envio ENVIO_CODENSA,
                   oeg.ote_fecha_envio ENVIO_GOM,
                   oem.ote_fecha_envio ENVIO_MERCURIO,
                   o.ot_cod_dpto,
                   o.ot_proceso_campana,
                   ((coalesce(o.ot_fechaej, current_date) -  o.ot_fecha_asigna_contrata)+1   -  (select CANT_DIAS_MUERTOS from  calcular_dias2(o.ot_fecha_asigna_contrata, coalesce(o.ot_fechaej, current_date)))
                                      ) Dias_visita,

                              case
                                when o.ot_proceso_campana='FA' and o.ot_cod_dpto='11'    then    --BOGOTA
                                coalesce(((oec.ote_fecha_envio - o.ot_fecha_asigna_contrata)+1) - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fecha_asigna_contrata, oec.ote_fecha_envio)),'SIN ENVIAR')
                                  else
                                   COALESCE( (oec.ote_fecha_envio -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oec.ote_fecha_envio)),'SIN ENVIAR')
                              end  Dias_envio_codensa,

                              case
                               WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25'   then     --CUNDINAMARCA
                                 coalesce((oeg.ote_fecha_envio  -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oeg.ote_fecha_envio)),'SIN CARGAR')
                                   ELSE
                                      'NO APLICA'
                              END  Dias_cargue_gom,

                              case
                               WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25'   then
                                 coalesce((oem.ote_fecha_envio  -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oem.ote_fecha_envio)),'SIN CARGAR')
                                   ELSE
                                      'NO APLICA'
                              END  Dias_cargue_mercurio

                      from ot o
                       left join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio
                       left join ot_enviadas oec   on oec.ote_solicitud=o.ot_solicitud and oec.ote_tipo_envio='01'
                       left join ot_enviadas oeg   on oeg.ote_solicitud=o.ot_solicitud and oeg.ote_tipo_envio='02'
                       left join ot_enviadas oem   on oem.ote_solicitud=o.ot_solicitud and oem.ote_tipo_envio='03'
                      where o.ot_proceso_campana='".$proceso."' and o.ot_cod_dpto='".$dpto."'
                      and o.ot_ejecutada ='S' and o.ot_fechaej between '".$fechaini."' and '".$fechafin."' order by 10";
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['solicitud'] = utf8_encode($fila[0]);
		$row_array['fechaAsignacion'] = utf8_encode($fila[1]);		
		$row_array['fechaEjecucion'] = utf8_encode($fila[2]);
		$row_array['fechaEnvioCodensa'] = utf8_encode($fila[3]);
		$row_array['fechaEnvioGom'] = utf8_encode($fila[4]);
		$row_array['fechaEnvioMercurio'] = utf8_encode($fila[5]);
		$row_array['dpto'] = utf8_encode($fila[6]);
		$row_array['proceso'] = utf8_encode($fila[7]);
		$row_array['diasVisita'] = utf8_encode($fila[8]);
		$row_array['diasEnvioCodensa'] = utf8_encode($fila[9]);
		$row_array['diasCargueGom'] = utf8_encode($fila[10]);
		$row_array['diasCargueMercurio'] = utf8_encode($fila[11]);
		

					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>