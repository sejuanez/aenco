<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=ans_fac_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

  echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
  $fechafin = $_GET['fechaFin'];
  $dpto = $_GET['dpto'];
	$proceso = $_GET['proceso'];
		
	$consulta = "SELECT distinct
                   o.ot_solicitud,
                   o.ot_fecha_asigna_contrata,
                   o.ot_fechaej,
                   oec.ote_fecha_envio ENVIO_CODENSA,
                   oeg.ote_fecha_envio ENVIO_GOM,
                   oem.ote_fecha_envio ENVIO_MERCURIO,
                   o.ot_cod_dpto,
                   o.ot_proceso_campana,
                   (O.ot_fecha_asigna_contrata - O.ot_fechaej) DIAS_VISITA,

                              case
                                when o.ot_proceso_campana='FA' and o.ot_cod_dpto='11'    then    --BOGOTA
                                coalesce(((oec.ote_fecha_envio - o.ot_fecha_asigna_contrata)+1) - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fecha_asigna_contrata, oec.ote_fecha_envio)),'SIN ENVIAR')
                                  else
                                   COALESCE( (oec.ote_fecha_envio -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oec.ote_fecha_envio)),'SIN ENVIAR')
                              end  Dias_envio_codensa,

                              case
                               WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25'   then     --CUNDINAMARCA
                                 coalesce((oeg.ote_fecha_envio  -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oeg.ote_fecha_envio)),'SIN CARGAR')
                                   ELSE
                                      'NO APLICA'
                              END  Dias_cargue_gom,

                              case
                               WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25'   then
                                 coalesce((oem.ote_fecha_envio  -  o.ot_fechaej)
                                      - (select CANT_DIAS_MUERTOS
                                      from  calcular_dias2(o.ot_fechaej, oem.ote_fecha_envio)),'SIN CARGAR')
                                   ELSE
                                      'NO APLICA'
                              END  Dias_cargue_mercurio

                      from ot o
                       left join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio
                       left join ot_enviadas oec   on oec.ote_solicitud=o.ot_solicitud and oec.ote_tipo_envio='01'
                       left join ot_enviadas oeg   on oeg.ote_solicitud=o.ot_solicitud and oeg.ote_tipo_envio='02'
                       left join ot_enviadas oem   on oem.ote_solicitud=o.ot_solicitud and oem.ote_tipo_envio='03'
                      where o.ot_proceso_campana='".$proceso."' and o.ot_cod_dpto='".$dpto."'
                      and o.ot_ejecutada ='S' and o.ot_fechaej between '".$fechaini."' and '".$fechafin."' order by 10";

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'><td>Solicitud</td><td>Fecha asig.</td><td>Fecha ejec.</td><td>Fecha env. Codensa</td><td>Fecha env. Gom</td><td>Fecha env. Mercurio</td><td>Departamento</td><td>Proceso</td><td>Días visita</td><td>Días. Env. Codensa</td><td>Dias. Cargue Gom</td><td>Días. Cargue Mercurio</td></tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td><td>".utf8_encode($fila[1])."</td>"."<td>".utf8_encode($fila[2])."</td><td>".utf8_encode($fila[3])."</td><td>".utf8_encode($fila[4])."</td><td>".utf8_encode($fila[5])."</td><td>".utf8_encode($fila[6])."</td><td>".utf8_encode($fila[7])."</td><td>".utf8_encode($fila[8])."</td><td>".utf8_encode($fila[9])."</td><td>".utf8_encode($fila[10])."</td><td>".utf8_encode($fila[11])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>