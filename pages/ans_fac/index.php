<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Ans_fac</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico-tecnico").width();
					var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					
				}
			});
			

			$(window).resize(function() {
			    	var ancho = +$("#div-grafico-tecnico").width();
			    	var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					
			});
		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Cantidad'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            valueSuffix: ''
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};





			$('#departamento').attr('disabled',true);
			$('#proceso').attr('disabled',true);
			$("#consultar").hide();
			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();

			$("#div-total").hide();

			var doneAjax1=doneAjax2=false;
			
			//ajax que carga la lista de departamentos
			$.ajax({
					url:'request/getDepartamentos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsDpto="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsDpto+="<option value='"+repuesta[i].codigoDpto+"'>"+repuesta[i].nombreDpto+"</option>";
							
						}
						
						$("#departamento").html(optionsDpto);						
						$('#departamento').attr('disabled',false);

						doneAjax1= true;

						if(doneAjax1&&doneAjax2)
							$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax departamentos//ajax que carga la lista de departamentos


			
			//ajax que carga la lista de procesos
			$.ajax({
					url:'request/getProcesos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsProceso="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsProceso+="<option value='"+repuesta[i].codigoProceso+"'>"+repuesta[i].nombreProceso+"</option>";
							
						}
						
						$("#proceso").html(optionsProceso);						
						$('#proceso').attr('disabled',false);
						

						doneAjax2= true;

						if(doneAjax1&&doneAjax2)
							$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax procesos




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();

				$("#div-total").hide();
				
				
				var doneAjax1=doneAjax2=doneAjax3=doneAjax4=doneAjax5=false;


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



		        // ajax GENERAL

				$.ajax({
					url:'request/general.php',
	                type:'POST',
	                dataType:'json',
	                data:{dpto:$("#departamento").val(), proceso:$("#proceso").val(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

											
						$("#total").html(repuesta.length);		
						$("#div-total").show();					
						
						var filas="";
												

						for (var i=0; i<repuesta.length; i++) {
							
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].solicitud+"</td><td>"+repuesta[i].fechaAsignacion+"</td>"+"<td>"+repuesta[i].fechaEjecucion+"</td><td>"+repuesta[i].fechaEnvioCodensa+"</td><td>"+repuesta[i].fechaEnvioGom+"</td><td>"+repuesta[i].fechaEnvioMercurio+"</td><td>"+repuesta[i].dpto+"</td><td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].diasVisita+"</td><td>"+repuesta[i].diasEnvioCodensa+"</td><td>"+repuesta[i].diasCargueGom+"</td><td>"+repuesta[i].diasCargueMercurio+"</td></tr>";
						}
						
						$("#tabla-general tbody").html(filas);
					
						

						$("#exportar a").attr("href","request/exportarExcel.php?dpto="+$('#departamento').val()+"&proceso="+$('#proceso').val()+"&fechaIni="+fechaIni+"&fechaFin="+fechaFin);
						
						$("#exportar").show();


						
						
					}
					else{												
						$("#total").html("");		
						$("#div-total").hide();	
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");								
					}

					doneAjax1=true;
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5){
						
						$("#contenido").show();
						$("#consultando").hide();
						$("#consultar").show();

					}
					
				});


				
				//ajax diasVisitas

				$.ajax({
					url:'request/getDiasVisitas.php',
	                type:'POST',
	                dataType:'json',
	                data:{dpto:$("#departamento").val(), proceso:$("#proceso").val(), fechaini:fechaIni , fechafin:fechaFin}
				}).done(function(repuesta){

					if(repuesta.length>0){

						var categorias = [];						
						var cantidad = [];

						for (var i=0; i<repuesta.length; i++) {
								
								categorias[i]=""+repuesta[i].dias;
								cantidad[i]=+repuesta[i].cantidad;
						}

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

							


							//var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							dibujarGraficoBarras("grafico-tecnico",categorias,series,colors);
					}
					else{

						$("#grafico-tecnico").html("");
					}

					doneAjax2=true;
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5){
						
						$("#contenido").show();
						$("#consultando").hide();
						$("#consultar").show();

					}

				});



				//ajax diasEnvCodensa

				$.ajax({
					url:'request/getDiasEnvCodensa.php',
	                type:'POST',
	                dataType:'json',
	                data:{dpto:$("#departamento").val(), proceso:$("#proceso").val(), fechaini:fechaIni , fechafin:fechaFin}
				}).done(function(repuesta){

					if(repuesta.length>0){

						var categorias = [];						
						var cantidad = [];

						for (var i=0; i<repuesta.length; i++) {
								
								categorias[i]=""+repuesta[i].dias;
								cantidad[i]=+repuesta[i].cantidad;
						}

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

							

							

							//var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							dibujarGraficoBarras("grafico2",categorias,series,colors);
					}
					else{

						$("#grafico2").html("");
					}

					doneAjax3=true;
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5){
						
						$("#contenido").show();
						$("#consultando").hide();
						$("#consultar").show();

					}

				});


				//ajax diasCargueGom

				$.ajax({
					url:'request/getDiasCargueGom.php',
	                type:'POST',
	                dataType:'json',
	                data:{dpto:$("#departamento").val(), proceso:$("#proceso").val(), fechaini:fechaIni , fechafin:fechaFin}
				}).done(function(repuesta){

					if(repuesta.length>0){

						var categorias = [];						
						var cantidad = [];

						for (var i=0; i<repuesta.length; i++) {
								
								categorias[i]=""+repuesta[i].dias;
								cantidad[i]=+repuesta[i].cantidad;
						}

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

							

							

							//var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							dibujarGraficoBarras("grafico3",categorias,series,colors);
					}
					else{

						$("#grafico3").html("");
					}

					doneAjax4=true;
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5){
						
						$("#contenido").show();
						$("#consultando").hide();
						$("#consultar").show();

					}

				});


				//ajax diasCargueGom

				$.ajax({
					url:'request/getDiasCargueMercurio.php',
	                type:'POST',
	                dataType:'json',
	                data:{dpto:$("#departamento").val(), proceso:$("#proceso").val(), fechaini:fechaIni , fechafin:fechaFin}
				}).done(function(repuesta){

					if(repuesta.length>0){

						var categorias = [];						
						var cantidad = [];

						for (var i=0; i<repuesta.length; i++) {
								
								categorias[i]=""+repuesta[i].dias;
								cantidad[i]=+repuesta[i].cantidad;
						}

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				},
	                          				colorByPoint: true
	                          			}
									];

							

							

							//var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							var colors=['#FF6347','#FFFF00','#8A2BE2','#00FF00'/*'#9932CC'*/,'#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

							dibujarGraficoBarras("grafico4",categorias,series,colors);
					}
					else{

						$("#grafico4").html("");
					}

					doneAjax5=true;
					if(doneAjax1&&doneAjax2&&doneAjax3&&doneAjax4&&doneAjax5){
						
						$("#contenido").show();
						$("#consultando").hide();
						$("#consultar").show();

					}

				});


			});//fin de consultar.click()
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>ANS</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<div><label>Departamento</label><select id='departamento'></select></div>
				<div><label>Proceso</label><select id='proceso'></select></div>

				<div id="div-total">        
                	<h4 id="total">0</h4>
                	<h5>Total</h5>
              	</div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Detalles</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Días visita</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Días env. Codensa</span></a></li>
				<li><a href="#tab4">4. <span class='titulo-tab'>Días cargue Gom.</span></a></li>
				<li><a href="#tab5">5. <span class='titulo-tab'>Días cargue Mercurio.</span></a></li>
			
			</ul>


			<div id='tab1'>
				<table id='tabla-general'>
					<thead>
						<tr class='cabecera'><td>Solicitud</td><td>Fecha asig.</td><td>Fecha ejec.</td><td>Fecha env. Codensa</td><td>Fecha env. Gom</td><td>Fecha env. Mercurio</td><td>Departamento</td><td>Proceso</td><td>Días visita</td><td>Días. Env. Codensa</td><td>Dias. Cargue Gom</td><td>Días. Cargue Mercurio</td></tr>
					</thead>	

					<tbody>
						
					</tbody>
				</table>
			</div>


			<div id='tab2'>
				<!--<div>
					<table id='tabla-tecnico'>
						<thead>
							<tr class='cabecera'><td>Técnico</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>-->

				<div id='div-grafico-tecnico'>
					<div id='grafico-tecnico'></div>
				</div>
			</div>




			<div id='tab3'>
				
				<div id='div-grafico2'>
					<div id='grafico2'></div>
				</div>
				
			</div>




			<div id='tab4'>
				
				<div id='div-grafico3'>
					<div id='grafico3'></div>
				</div>

			</div>




			<div id='tab5'>
				
				<div id='div-grafico4'>
					<div id='grafico4'></div>
				</div>

			</div>

		</div>

	</div>

	

</body>
</html>