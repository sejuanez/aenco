<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Metas por Tecnicos</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

  	<script type="text/javascript" src="../../js/accounting.js"></script>

	
	<script type="text/javascript">
		
		$(function(){

			var repuestaAjax2 = repuestaAjax3 = requestAjax4 = requestAjax5 = diasTrabajados = null;
			

			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico1").width();
					var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					var ancho = +$("#div-grafico2").width();
			    	var alto = +$("#div-grafico2").height();
					$("#grafico2").highcharts().setSize(ancho,alto, false);
				}
			});
			

			$(window).resize(function() {
			    	
			    	var ancho = +$("#div-grafico1").width();
			    	var alto = +$("#div-grafico1").height();
					$("#grafico1").highcharts().setSize(ancho,alto, false);

					var ancho = +$("#div-grafico2").width();
			    	var alto = +$("#div-grafico2").height();
					$("#grafico2").highcharts().setSize(ancho,alto, false);

					
			});
		
			
			function dibujarGraficoBarras_(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        //colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores ($)',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};

			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            zoomType: 'xy',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Valores',
			                align: 'middle'
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            /*valueSuffix: ' programaciones'*/
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};





			$('#proceso').attr('disabled',true);
			$("#consultar").hide();

			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();
			$("#div-total").hide();


			


			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			
			//ajax que carga la lista de procesos
			$.ajax({
					url:'request/getProcesos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					
					if(repuesta.length>0){
						var optionsProceso="";
												
						for(var i=0;i<repuesta.length;i++){
							if(repuesta[i]!=null)
								optionsProceso+="<option value='"+repuesta[i].codigoProceso+"'>"+repuesta[i].nombreProceso+"</option>";
							
						}
						
						$("#proceso").html(optionsProceso);						
						$('#proceso').attr('disabled',false);
						

						$("#consultar").show();
						
											
					}
					
					
			});// fin del ajax procesos


			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				$("#div-total").hide();

				repuestaAjax2 = repuestaAjax3 = repuestaAjax4 = repuestaAjax5= diasTrabajados = metaDiaria = media = null;
				
				


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;


		        //obtener dias trabajados
		        $.ajax({
						url:'request/rq1.php',
		                type:'POST',
		                dataType:'json',
		                data:{proceso:$("#proceso").val(), mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear(), fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					if(repuesta.length>0){// si hay dias trabajados

						diasTrabajados = +(repuesta[0].diasTrabajados);
					

					        // ajax2: obtener primera parte de info de funcionarios

							$.ajax({
								url:'request/rq2.php',
				                type:'POST',
				                dataType:'json',
				                data:{proceso:$("#proceso").val(), mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear(), fechaini:fechaIni , fechafin:fechaFin}

							}).done(function(repuesta){

								
								if(repuesta.length>0){

									repuestaAjax2 = repuesta.slice();

									/*var media = 0;

									for(var i=0; i < repuesta.length; i++){

										media = media + (+repuestaAjax2[i].pagosEfectivos);
									}
									
									media = media / repuesta.length;*/
									
									//ajax3:  obtener segunda parte de info de funcionarios
									$.ajax({
										url:'request/rq3.php',
						                type:'POST',
						                dataType:'json',
						                data:{mes:mesIni, anio:$('#txtFechaIni').datepicker('getDate').getFullYear()/*, fechaini:fechaIni , fechafin:fechaFin*/}

									}).done(function(repuesta){

										
										if(repuesta.length>0){

											repuestaAjax3 = repuesta.slice();

											var funcionarios=[];						
											var funcionarios_livianas=[];						
											var funcionarios_pesadas=[];
											
											
											var produccion_livianas=[];
											var produccion_pesadas=[];

											var min_livianas=[];
											var min_pesadas=[];
											var max_livianas=[];
											var max_pesadas=[];

											var total_produccion_livianas=0;
											var total_produccion_pesadas=0;
											

											for (var i=0; i<repuestaAjax2.length;i++){

												var sw=false;
												
												for (var j=0;(j<repuestaAjax3.length)&&(!sw);j++){
													
													if(repuestaAjax3[j].codFun==repuestaAjax2[i].codFun){

														funcionarios.push({
															codFun:repuestaAjax2[i].codFun,
															funcionario:repuestaAjax2[i].funcionario,
															brigada:repuestaAjax2[i].brigada,
															produccion:(+repuestaAjax2[i].produccion),
															metaMin:(+repuestaAjax3[j].metaMin),
															metaMax:(+repuestaAjax3[j].metaMax)
														});
														
														
														

														if(repuestaAjax2[i].brigada=="LIVIANA"){
															funcionarios_livianas.push(repuestaAjax2[i].funcionario);
															min_livianas.push((+repuestaAjax3[j].metaMin)*diasTrabajados);
															max_livianas.push((+repuestaAjax3[j].metaMax)*diasTrabajados);
															produccion_livianas.push(+repuestaAjax2[i].produccion);
															total_produccion_livianas+=(+repuestaAjax2[i].produccion);
														}															
														else{
															funcionarios_pesadas.push(repuestaAjax2[i].funcionario);
															min_pesadas.push((+repuestaAjax3[j].metaMin)*diasTrabajados);
															max_pesadas.push((+repuestaAjax3[j].metaMax)*diasTrabajados);
															produccion_pesadas.push(+repuestaAjax2[i].produccion);
															total_produccion_pesadas+=(+repuestaAjax2[i].produccion);
														}


															
														sw=true;
													}
												}//fin for ajax3
											}//fin for ajax2
											

											var filas="";
											var total_produccion=0;

											for (var i=0; i<funcionarios.length;i++){

												total_produccion+=funcionarios[i].produccion;
												
												filas+="<tr class='fila'>"+
									                   "<td>"+funcionarios[i].codFun+"</td><td>"+funcionarios[i].funcionario+"</td><td>"+funcionarios[i].brigada+"</td><td>"+(funcionarios[i].produccion)+"</td><td>"+funcionarios[i].metaMin+"</td><td>"+funcionarios[i].metaMax+"</td><td>"+diasTrabajados+"</td><td>"+(funcionarios[i].metaMin*diasTrabajados)+"</td><td>"+(funcionarios[i].metaMax*diasTrabajados)+"</td></tr>";
											}
											

											total_produccion_livianas = accounting.formatMoney(total_produccion_livianas, {symbol : '$', precision : 0,thousand : ','});
											$("#tab2-total").html(" ("+total_produccion_livianas+")");

											total_produccion_pesadas = accounting.formatMoney(total_produccion_pesadas, {symbol : '$', precision : 0,thousand : ','});
											$("#tab3-total").html(" ("+total_produccion_pesadas+")");

											total_produccion = accounting.formatMoney(total_produccion, {symbol : '$', precision : 0,thousand : ','});
											$("#total").html(total_produccion);		
											$("#div-total").show();	
	
											$("#tabla-general tbody").html(filas);

											//grafico1
												var series=[
															{
																name:'Produccion', 
																type: 'column',
																data:produccion_livianas,
																dataLabels: {
																	enabled: true,
						                              				//rotation: -90,
						                              				color: '#00034A',
						                              				align: 'center',
						                             				// format: '${point.y}', // one decimal
									                              	formatter:function(){
									                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
									                              	},
						                              				y: 10, // 10 pixels down from the top
									                                style: {
									                                  fontSize: '10px',
									                                  fontFamily: 'Verdana, sans-serif'
									                                }
									                                
						                          				},
						                          				colorByPoint: true
						                          			},
															{
																name:'Minima',
																type: 'spline',
																data:min_livianas,
																dataLabels:{
																	enabled:false,
																	color: '#008000'
																},
																color:"blue"
															},
															{
																name:'Maxima',
																type:'spline',
																data:max_livianas,
																dataLabels:{
																	enabled:false,
																	color: '#f00'
																},
																color:"green"
															}
														];

												//colors =["#ff6600",'#0008C5','#32CD32','#FF6347'];

												var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
												dibujarGraficoBarras("grafico1",funcionarios_livianas,series,colors);
												

												//grafico2
												var series=[
															{
																name:'Produccion', 
																type: 'column',
																data:produccion_pesadas,
																dataLabels: {
																	enabled: true,
						                              				//rotation: -90,
						                              				color: '#00034A',
						                              				align: 'center',
						                             				// format: '${point.y}', // one decimal
									                              	formatter:function(){
									                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
									                              	},
						                              				y: 10, // 10 pixels down from the top
									                                style: {
									                                  fontSize: '10px',
									                                  fontFamily: 'Verdana, sans-serif'
									                                }
						                          				},
						                          				colorByPoint:true
						                          			},
															{
																name:'Minima',
																type: 'spline',
																data:min_pesadas,
																dataLabels:{
																	enabled:false,
																	color: '#008000'
																},
																color:"blue"
															},
															{
																name:'Maxima',
																type:'spline',
																data:max_pesadas,
																dataLabels:{
																	enabled:false,
																	color: '#f00'
																},
																color:"green"
															}
														];

												//colors =["#7CFC00",'#0008C5','#32CD32','#FF6347'];

												var colors=[/*'#00034A','#0008C5',*/'#FF6347','#FFFF00','#8AFFAD','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

												dibujarGraficoBarras("grafico2",funcionarios_pesadas,series,colors);



												$("#contenido").show();
											

												$("#consultando").hide();
												$("#consultar").show();
												$("#exportar a").attr("href","request/exportarExcel.php?info="+$("#div-tabla").html());
												$("#exportar").show();								

										}
										else{	

											$("#total").html("");		
											$("#div-total").hide();	
											$("#tabla-general tbody").html("");
											$("#grafico1").html("");

											$("#consultando").hide();
											$("#consultar").show();
										}

										
									});

									
								}
								else{												
									
									$("#tabla-general tbody").html("");
									$("#exportar a").attr("href","");

									$("#consultando").hide();
									$("#consultar").show();								
								}


								
							});
					}
					else{
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");

						$("#consultando").hide();
						$("#consultar").show();
					}

					
				});




			});// fin de consultar.Click
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay nada para mostrar.</p>
	</div>
	

	<header>
		<h3>Metas por Técnicos</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				<div><label>Proceso</label><select id='proceso'></select></div>
				
				<div id="div-total">
					<h5>Total</h5>        
                	<h4 id="total">0</h4>
                	
              	</div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>Datos</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab' >Livianas</span><span id="tab2-total"></span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab' >Pesadas</span><span  id="tab3-total"></span></a></li>
				<!--<li><a href="#tab4">4. <span class='titulo-tab'>Ingreso adicional</span></a></li>-->
			
			</ul>


			<div id='tab1'>
				<div id='div-tabla'>
					<table id='tabla-general'>
						<thead>
							<tr class='cabecera'><td>Codigo</td><td>Tecnico</td><td>Brigada</td><td>Produccion</td><td>Meta min. dia</td><td>Meta max. dia</td><td>Dias trabaj.</td><td>Minima</td><td>Maxima</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
				</table>
				</div>
			</div>




			<div id='tab2'>
				
				<div id='div-grafico1'>
					<div id='grafico1'></div>
				</div>
			</div>




			<div id='tab3'>
				
				<div id='div-grafico2'>
					<div id='grafico2'></div>
				</div>
				
			</div>


		</div>

		

	</div>

	

</body>
</html>