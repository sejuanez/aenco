<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];

	
	//$consulta = "SELECT distinct da.dla_dpto, d.de_nombre from dato_lev_alumbrado da left join departamentos d on d.de_codigo=da.dla_dpto";

	$consulta = "SELECT  distinct  d.de_codigo dla_dpto, d.de_nombre de_nombre
				 FROM usu_web_municipio U
 				 LEFT JOIN departamentos d ON d.de_codigo=u.uwm_dpto
				 WHERE U.uwm_usuario='$usuario'";


	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['value'] = $fila[0];
		$row_array['label'] = $fila[1];
		
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);
?>