<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">

  <title>PQR</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>
  <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)'/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/css/alertify.css">
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.1/alertify.js"></script>
  <script type="text/javascript">

    var tipo;

    $(function () {
      $("#alert").dialog({
        modal: true,
        autoOpen: false,
        resizable: false,
        buttons: {
          Ok: function () {
            //$("#alert p").html("");
            $(this).dialog("close");
          }
        }
      });

      var externa = "SI";

      $("#guardando").hide();


      $("#fecha").datepicker({
        maxDate: new Date(),
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
      });

      $("#fecha").datepicker("setDate", new Date());
      var barrio = null;

      $("#barrio").autocomplete({
        minLength: 0,
        select: function (event, ui) {
          event.preventDefault();
          $("#barrio").val(ui.item.label);
          barrio = ui.item.value;
        },
        change: function (event, ui) {
          if (ui.item == null || ui.item == undefined) {
            $("#barrio").val("");
            barrio = null;
          }
        }
      });

      function limpiarFormulario() {
        $("input[type=text]").val("");
        $("#obs").val("");
        $("#barrio").val("");
        barrio = null;
      };

      function validarFormulario() {
        if (tipo === 'ADMINISTRATIVO') {
          if ($("#cedula").val() != "" && $("#apellido1").val() != "" && $("#apellido2").val() != "" && $("#nombre1").val() != "" && $("#nombre2").val() != "" && $("#departamento").val() != "" && $("#municipio").val() != "" && $("#corregimiento").val() != "" && barrio != null/*$("#barrio").val()!=""*/ && $("#direccion").val() != "" && $("#ptoref").val() != "" && $("#telefono").val() != "" && $("#fecha").val() != "" && $("#hora").val() != "" && $("#tos").val() != "" && $("#obs").val() != "" ) {
            return true;
          } else {
            return false;
          }
        } else {
          if ($("#cedula").val() != "" && $("#apellido1").val() != "" && $("#apellido2").val() != "" && $("#nombre1").val() != "" && $("#nombre2").val() != "" && $("#departamento").val() != "" && $("#municipio").val() != "" && $("#corregimiento").val() != "" && barrio != null/*$("#barrio").val()!=""*/ && $("#direccion").val() != "" && $("#ptoref").val() != "" && $("#telefono").val() != "" && $("#fecha").val() != "" && $("#hora").val() != "" && $("#tos").val() != "" && $("#estadolum").val() != "" && $("#serie").val() != "" && $("#poste").val() != "" && $("#obs").val() != "" && $("#nic").val() != "") {
            return true;
          } else {
            return false;
          }
        }
      };//fin de la funcion validar formulario

      function setBarrio(Dpto, Mun, Cgto) {
        $.ajax({
          url: 'request/getBarrios.php',
          type: 'POST',
          dataType: 'json',
          data: {dpto: Dpto, mun: Mun, cgto: Cgto}
        }).done(function (repuesta) {
          if (repuesta.length > 0) {
            $("#barrio").autocomplete({source: repuesta});
          } else {
            $("#barrio").autocomplete({source: null});
            $("#barrio").val("");
            barrio = null;
          }
        });
      };// fin de la funcion setBarrio

      //setBarrio();
      function setCorregimiento(Dpto, Mun) {
        $.ajax({
          url: 'request/getCorregimientos.php',
          type: 'POST',
          dataType: 'json',
          data: {dpto: Dpto, mun: Mun}
        }).done(function (repuesta) {
          if (repuesta.length > 0) {
            var options = "";
            for (var i = 0; i < repuesta.length; i++) {
              options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
            }
            $("#corregimiento").html(options);
            setBarrio(Dpto, Mun, $("#corregimiento").val());
          }
        });
      };// fin de la funcion setCorregimiento
      function setMunicipio(Dpto) {
        $.ajax({
          url: 'request/getMunicipios.php',
          type: 'POST',
          dataType: 'json',
          data: {dpto: Dpto}
        }).done(function (repuesta) {
          if (repuesta.length > 0) {
            var options = "";
            for (var i = 0; i < repuesta.length; i++) {
              console.log(repuesta);
              options += "<option value='" + repuesta[i].uwm_mpio + "'>" + repuesta[i].mu_nombre + "</option>";
            }
            $("#municipio").html(options);
            setCorregimiento(Dpto, $("#municipio").val());
          }
        });
      };// fin de la funcion setMunicipio
      //ajax que carga la liista de departamentos
      $.ajax({
        url: 'request/getDepartamentos.php',
        type: 'POST',
        dataType: 'json'
      }).done(function (repuesta) {
        if (repuesta.length > 0) {
          var optionsDpto = "";
          var optionsMun = "";
          for (var i = 0; i < repuesta.length; i++) {
            optionsDpto += "<option value='" + repuesta[i].codigoDpto + "'>" + repuesta[i].nombreDpto + "</option>";
            // optionsMun+="<option value='"+repuesta[i].uwm_mpio+"'>"+repuesta[i].mu_nombre+"</option>";
          }
          $("#departamento").html(optionsDpto);
          $("#municipio").html(optionsMun);
          setMunicipio($("#departamento").val());
          setCorregimiento($("#departamento").val(), $("#municipio").val());
        }
      });// fin del ajax departamentos

      //listener del evento Change del checkbox externa

      $("#sol_externa").on("change", function () {
        if ($("#sol_externa").prop("checked"))
          externa = "SI";
        else
          externa = "NO";
      });

      //Listeners de el evento Change de las listas departamentos
      $("#departamento").on("change", function () {
        $("#municipio").html("");
        $("#corregimiento").html("");
        //$("#barrio").html("");
        barrio = null;
        $("#barrio").autocomplete({source: null});
        $("#barrio").val("");
        setMunicipio($("#departamento").val());
      });
      //fin de Listeners de el evento Change de la lista departamentos

      //Listeners de el evento Change de las listas municipios
      $("#municipio").on("change", function () {
        $("#corregimiento").html("");
        //$("#barrio").html("");
        barrio = null;
        $("#barrio").autocomplete({source: null});
        $("#barrio").val("");
        setCorregimiento($("#departamento").val(), $("#municipio").val());
      });
      //fin de Listeners de el evento Change de la lista municipio

      //Listeners de el evento Change de las listas corregimiento
      $("#corregimiento").on("change", function () {
        //$("#barrio").html("");
        barrio = null;
        $("#barrio").autocomplete({source: null});
        $("#barrio").val("");
        setBarrio($("#departamento").val(), $("#municipio").val(), $("#corregimiento").val());
      });
      //fin de Listeners de el evento Change de la lista corregimiento
      //ajax que carga la lista de tipo de ordenes
      $.ajax({
        url: 'request/getTipoOrdenes.php',
        type: 'POST',
        dataType: 'json'
      }).done(function (repuesta) {
        if (repuesta.length > 0) {
          var options = "";
          for (var i = 0; i < repuesta.length; i++) {
            options += "<option value='" + repuesta[i].codigo + "' data-tipo='"+repuesta[i].to_tipo+"'>" + repuesta[i].nombre + "</option>";
          }
          $("#tos").html(options);
          //setMunicipio($("#departamento").val());
        }
      });// fin del ajax tipo ordenes
      //ajax que carga la lista de estado luminarias
      $.ajax({
        url: 'request/getEstadoLuminaria.php',
        type: 'POST',
        dataType: 'json'
      }).done(function (repuesta) {
        if (repuesta.length > 0) {
          var options = "";
          for (var i = 0; i < repuesta.length; i++) {
            options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
          }
          $("#estadolum").html(options);
          //setMunicipio($("#departamento").val());
        }

      });// fin del ajax estado luminarias


      //listener del evento focusout del cuadro de texto Cedula
      $("#cedula").on("focusout", function () {

        if ($("#cedula").val() != "") {

          $("#apellido1").attr("disabled", true);
          $("#apellido2").attr("disabled", true);
          $("#nombre1").attr("disabled", true);
          $("#nombre2").attr("disabled", true);
          $("#direccion").attr("disabled", true);
          $("#telefono").attr("disabled", true);

          //ajax que carga Nombre y Direccion a partir del NIC digitado
          $.ajax({
            url: 'request/getDatosPersona.php',
            type: 'POST',
            dataType: 'json',
            data: {cedula: $("#cedula").val()}

          }).done(function (repuesta) {
            if (repuesta.length > 0) {
              $("#apellido1").val(repuesta[0].apellido1);
              $("#apellido2").val(repuesta[0].apellido2);
              $("#nombre1").val(repuesta[0].nombre1);
              $("#nombre2").val(repuesta[0].nombre2);
              $("#direccion").val(repuesta[0].direccion);
              $("#telefono").val(repuesta[0].telefono);
            } else {
              $("#apellido1").val("");
              $("#apellido2").val("");
              $("#nombre1").val("");
              $("#nombre2").val("");
              $("#direccion").val("");
              $("#telefono").val("");
            }


            $("#apellido1").attr("disabled", false);
            $("#apellido2").attr("disabled", false);
            $("#nombre1").attr("disabled", false);
            $("#nombre2").attr("disabled", false);
            $("#direccion").attr("disabled", false);
            $("#telefono").attr("disabled", false);
          });// fin del ajax getDatosPersona
        }

      });// fin listener del evento focusout del ccuadro de texto Cedula


      //listener del evento focusout del ccuadro de texto NIC
      $("#nic").on("focusout", function () {

        if ($("#nic").val() != "") {

          $("#nombre").attr("disabled", true);
          $("#dir").attr("disabled", true);
          //ajax que carga Nombre y Direccion a partir del NIC digitado
          $.ajax({
            url: 'request/getNombreDir.php',
            type: 'POST',
            dataType: 'json',
            data: {codigo: $("#nic").val()}

          }).done(function (repuesta) {
            if (repuesta.length > 0) {
              $("#nombre").val(repuesta[0].nombre);
              $("#dir").val(repuesta[0].direccion);
            } else {
              $("#nombre").val("");
              $("#dir").val("");
            }

            $("#nombre").attr("disabled", false);
            $("#dir").attr("disabled", false);

          });// fin del ajax getNombreDir
        }
      });// fin listener del evento focusout del ccuadro de texto NIC

      //listener del evento click del boton GUARDAR

      $("#guardar").click(function (e) {
        e.preventDefault();
        $("#guardar").hide();
        $("#cancelar").hide();

        //console.log(validarFormulario());
        if (validarFormulario()) {
          if (tipo == '')  {
            $("#alert p").html("Lo sentimos el tipo no esta especificado.");
            $("#alert").dialog("open");
          } else {
            $("#guardando").show();
            $.ajax({//ajax guardar
              url: 'request/guardar.php',
              type: 'POST',
              dataType: 'json',
              data: {
                cedula: $("#cedula").val().toUpperCase(),
                apellido1: $("#apellido1").val().toUpperCase(),
                apellido2: $("#apellido2").val().toUpperCase(),
                nombre1: $("#nombre1").val().toUpperCase(),
                nombre2: $("#nombre2").val().toUpperCase(),
                departamento: $("#departamento").val(),
                municipio: $("#municipio").val(),
                corregimiento: $("#corregimiento").val(),
                barrio: barrio,
                direccion: $("#direccion").val().toUpperCase(),
                ptoref: $("#ptoref").val().toUpperCase(),
                telefono: $("#telefono").val(),
                fecha: $("#fecha").val(),
                hora: $("#hora").val(),
                tos: $("#tos").val().toUpperCase(),
                estadolum: $("#estadolum").val(),
                serie: $("#serie").val().toUpperCase(),
                poste: $("#poste").val().toUpperCase(),
                obs: $("#obs").val().toUpperCase(),
                nic: $("#nic").val().toUpperCase(),
                externa: externa
              }

            }).done(function (repuesta) {
              if (repuesta.length > 0) {
                //alert("La solicitud  "+repuesta[0].consecutivo+" se guardó correctamente");
                $("#alert p").html("La solicitud  " + repuesta[0].consecutivo + " se guardó correctamente");
                $("#alert").dialog("open");
                limpiarFormulario();
              } else {
                //alert("Error al guardar");
                $("#alert p").html("Error al guardar");
                $("#alert").dialog("open");
              }

              $("#guardar").show();
              $("#cancelar").show();
              $("#guardando").hide();
            });
          }
        } else {

          //alert("Debes diligenciar todos los campos");
          $("#alert p").html("Debes diligenciar todos los campos");
          $("#alert").dialog("open");
          $("#guardar").show();
          $("#cancelar").show();
        }

      });// fin del evento clic de guardar

      //listener del evento clic del boton cancelar
      $("#cancelar").click(function (e) {
        e.preventDefault();
        limpiarFormulario();
      });//fin del evento clic del boton cancelar

      $("#guardando").click(function (e) {
        e.preventDefault();
      });


      $("body").show();

      $(document).on('change', '')
      $(document).on('change', '#tos', function() {
        tipo = $(this).find(':selected').data('tipo');
        if (tipo === 'ADMINISTRATIVO') {
          $('#estadolum').prop('disabled', true);
          $('#serie').prop('disabled', true);
          $('#poste').prop('disabled', true);
          $('#nic').prop('disabled', true);
          $('#nombre').prop('disabled', true);
          $('#dir').prop('disabled', true);
          $('#estadolum').val('');
          $('#serie').val('');
          $('#poste').val('');
          $('#nic').val('');
          $('#nombre').val('');
          $('#dir').val('');
          $("#guardar").show();
          $("#cancelar").show();
        } else if (tipo === 'OPERATIVO') {
          $('#serie').prop('disabled', false);
          $('#poste').prop('disabled', false);
          $('#nic').prop('disabled', false);
          $('#nombre').prop('disabled', false);
          $('#dir').prop('disabled', false);
          $('#estadolum').prop('disabled', false);
          $("#guardar").show();
          $("#cancelar").show();
          $.ajax({
            url: 'request/getEstadoLuminaria.php',
            type: 'POST',
            dataType: 'json'
          }).done(function (repuesta) {
            if (repuesta.length > 0) {
              var options = "";
              for (var i = 0; i < repuesta.length; i++) {
                options += "<option value='" + repuesta[i].codigo + "'>" + repuesta[i].nombre + "</option>";
              }
              $("#estadolum").html(options);
            }
          });
        } else {
          $("#alert p").html("Tipo de ordene sin clasificación");
          $("#alert").dialog("open");
          // alert('Tipo de ordene sin clasificación');
        }
      });
    });
  </script>
</head>
<body style="display:none">

<div id="alert" title="Mensaje">
  <p>No hay datos para mostrar.</p>
</div>

<header>
  <h3>Reporte de daños</h3>
  <nav>
    <ul id="menu">

      <li id="guardar"><a href=""><span class="ion-android-done"></span><h6>Guardar</h6></a></li>
      <li id="guardando"><a href=""><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>
      <li id="cancelar"><a href=""><span class="ion-android-close"></span><h6>Cancelar</h6></a></li>

    </ul>
  </nav>
</header>


<section>
  <form>
    <aside>
      <div>
        <fieldset>
          <legend>Solicitante</legend>

          <div>
            <input type="checkbox" id="sol_externa" name="sol_externa" checked><span>Sol. Externa</span>
          </div>

          <div>
            <h5>Cédula</h5>
            <input type="text" id="cedula" name="cedula" autofocus>
          </div>

          <div>
            <h5>1er. Apellido</h5>
            <input type="text" id="apellido1" name="apellido1">
          </div>

          <div>
            <h5>2do. Apellido</h5>
            <input type="text" id="apellido2" name="apellido2">
          </div>

          <div>
            <h5>1er. Nombre</h5>
            <input type="text" id="nombre1" name="nombre1">
          </div>

          <div>
            <h5>2do. Nombre</h5>
            <input type="text" id="nombre2" name="nombre2">
          </div>

          <div>
            <h5>Departamento</h5>
            <select id="departamento" name="departamento"></select>
          </div>

          <div>
            <h5>Municipio</h5>
            <select id="municipio" name="municipio"></select>
          </div>

          <div>
            <h5>Corregimiento</h5>
            <select id="corregimiento" name="corregimiento"></select>
          </div>

          <div>
            <h5>Barrio/vda</h5>
            <!--<select id="barrio" name="barrio"></select>-->
            <input type="text" id="barrio" name="barrio">
          </div>

          <div>
            <h5>Dirección</h5>
            <input type="text" id="direccion" name="direccion">
          </div>

          <div>
            <h5>Pto. Ref</h5>
            <input type="text" id="ptoref" name="ptoref">
          </div>

          <div>
            <h5>Teléfono(s)</h5>
            <input type="text" id="telefono" name="telefono">
          </div>

        </fieldset>
      </div>
    </aside>


    <aside>
      <div>
        <fieldset>
          <legend>Reporte</legend>

          <div>
            <h5>Fecha rep.</h5>

            <input type="date" id="fecha" name="fecha">
          </div>

          <div>
            <h5>Hora rep.</h5>
            <input type="time" id="hora" name="hora" value="12:30">
          </div>

          <div>
            <h5>T.O.S</h5>
            <select id="tos" name="tos"></select>
          </div>

          <div>
            <h5>Estado lum.</h5>
            <select id="estadolum" name="estadolum"></select>
          </div>

          <div>
            <h5>Serie lumin.</h5>
            <input type="text" id="serie" name="serie">
          </div>

          <div>
            <h5>Nro. Poste</h5>
            <input type="text" id="poste" name="poste">
          </div>

          <div id="divobs">
            <h5 id="labelobs">Obs.</h5>
            <textarea id="obs" name="obs"></textarea>
          </div>

          <div>
            <h5>NIC</h5>
            <input type="text" id="nic" name="nic">
          </div>

          <div>
            <h5>Nombre</h5>
            <input type="text" id="nombre" name="nombre">
          </div>

          <div>
            <h5>Dir.</h5>
            <input type="text" id="dir" name="dir">
          </div>
        </fieldset>
      </div>
    </aside>
  </form>
</section>

<footer><p><span class="ion-ios-information-outline"></span><em>Todos los campos del formulario son obligatorios.</em>
  </p></footer>
<script>

  let horaSet = document.getElementById('hora');
  let d = new Date(),
    h = d.getHours(),
    m = d.getMinutes();
  if (h < 10) h = '0' + h;
  if (m < 10) m = '0' + m;
  horaSet.value = h + ':' + m
</script>

</body>
</html>