<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");

	$usuario = $_SESSION['user'];
	
	//$consulta = "select DE_codigo as Codigo, DE_NOMBRE as Descripcion from DEPARTAMENTOS";
	$consulta = "SELECT  distinct  d.de_codigo cod_dpto, d.de_nombre nombre_depto
				 FROM usu_web_municipio U
 				 LEFT JOIN departamentos d ON d.de_codigo=u.uwm_dpto
				 WHERE U.uwm_usuario='$usuario'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		/*$row_array['codigo'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]);*/
		$row_array['codigoDpto'] = utf8_encode($fila[0]);
		$row_array['nombreDpto'] = utf8_encode($fila[1]);
		// $row_array['codigoMun'] = utf8_encode($fila[2]);
		// $row_array['nombreMun'] = utf8_encode($fila[3]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>