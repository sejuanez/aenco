<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");
	
	$consulta = "SELECT ts.to_codigo,  ts.to_descripcion, ts.to_tipo FROM tipo_orden_servicio ts";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		$row_array['codigo'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]);
        $row_array['to_tipo'] = utf8_encode($fila[2]);
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>