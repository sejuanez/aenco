<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	include("../../../init/gestion.php");
	
	$consulta = "SELECT CODIGO as Codigo, DESCRIPCION as Descripcion FROM TABLAS_AFOROMANTENIMIENTO WHERE TIPO='OBS LAMPARAS'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['codigo'] = utf8_encode($fila[0]);
		$row_array['nombre'] = utf8_encode($fila[1]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>