ELEMENT.locale(ELEMENT.lang.es);
  new Vue({
    el: '#app',
    data: () => ({
      ruleForm: {
        rh_cedula: '',
        rh_codexpciud: '',
        rh_expcedula: '',
        rh_apellido1: '',
        rh_apellido2: '',
        rh_nombre: '',
        rh_direccion: '',
        rh_codciudad: '',
        rh_ciudad: '',
        rh_codbarrio: '',
        rh_barrio: '',
        rh_telefono: '',
        rh_movil: '',
        rh_sexo: '',
        rh_dessexo: '',
        rh_facsangre: '',
        rh_rh: '',
        rh_rh1: '',
        rh_desrh: '',
        rh_estcivil: '',
        rh_fechanac: '',
        rh_codciunac: '',
        rh_ciudadnac: '',
        rh_coddptonac: '',
        rh_dptonac: '',
        rh_codnacional: '',
        rh_nacional: '',
        rh_estado: 'INACTIVO',
        rh_foto: '',
        rh_profesion: '',
        rh_explaboral: '',
        rh_nombre1: '',
        rh_nombre2: '',
        rh_edad: '',
        dl_codcentrab: '',
        dl_descentrab: '',
        dl_codcencosto: '',
        dl_descencosto: '',
        dl_salario: '',
        dl_nlibreta: '',
        dl_codclaselib: '',
        dl_desclaselib: '',
        dl_distritomil: '',
        dl_certjudicial: '',
        dl_fechaven: '',
        dl_hosrasdiarias: '',
        dp_codarp: '',
        dp_desarp: '',
        dp_codeps: '',
        dp_deseps: '',
        dp_afp: '',
        dp_desafp: '',
        dp_codcaja: '',
        dp_descaja: '',
        dp_codbanco: '',
        dp_desbanco: '',
        dp_ncuenta: '',
        dp_convencion: 'N',
        dp_adicionarp: '',
        em_cedula: '',
        em_codingeniero: '',
        em_nomingeniero: '',
        em_programa: '',
        em_desprograma: '',
        em_cuadrilla: '',
        em_codarea: '',
        em_desarea: '',
        em_codcargo: '',
        em_descargo: '',
        em_confianza: '',
        em_perfil: '',
        ta_camisa: '',
        ta_pantalon: '',
        ta_botas: '',
        ta_tipo: '',
        df_cedula: '',
        co_cedula: ''
      },
      ruleFormConte: {
        dc_codconte: '',
        dc_desconte: ''
      },
      ruleFormFAcademica: {
        df_codigo: '',
        df_formacademica: '',
        df_insteducativa: '',
        df_tituloobt: ''
      },
      ruleFormDatosContrato: {
        co_codempresa: '',
        co_empresa: '',
        co_fechaing: '',
        co_fechafin: '',
        co_tcontrato: '',
        co_codretiro: '',
        co_item: '',
        co_motivoretiro: '',
        co_codreal: '',
        co_motivoreal: '',
        co_fecharet: '',
        co_observacion: ''
      },
      ruleFormVencimientos: {
        codtipovence: '',
        descripcion: '',
        fechavence: ''
      },
      rules: {
        rh_cedula: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_codexpciud: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_apellido1: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_nombre1: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_direccion: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_codciudad: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_codbarrio: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_telefono: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_movil: [
          { required: true, message: 'Campo requerido', trigger: 'blur' }
        ],
        rh_sexo: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_estcivil: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_rh: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_fechanac: [
          { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_coddptonac: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_codciunac: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        rh_codnacional: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        dl_codcentrab: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ],
        dl_codcencosto: [
          { required: true, message: 'Campo requerido', trigger: 'change' }
        ]
      },
      imageUrl: '',
      tableData: [],
      tableDataContrato: [],
      tableDataInfoAcademinaca: [],
      tableDataInfoFamiliar: [],
      tableDataVenciomientos: [],
      tableDataBuscar: [],
      optionsDpto: [],
      optionsCiudades: [],
      optionsBarrio: [],
      optionsMunicipio: [],
      optionsNacionalidades: [],
      optionscentroTrabajo: [],
      optionscentroCosto: [],
      optionsBanco: [],
      optionsArp: [],
      optionsEps: [],
      optionsAfp: [],
      optionsCaja: [],
      optionsCampanas: [],
      optionsJefeInmediato: [],
      optionsAreas: [],
      optionsCargos: [],
      optionsConte: [],
      optionsTipoEstudio: [],
      optionsCategoriaDotacion: [],
      optionsEmpresa: [],
      optionsMotivoRetiro: [],
      optionsTipoVencimiento: [],
      optionsGenero: [
        {value: 'M', label: 'MASCULINO'},
        {value: 'F', label: 'FEMENINO'}
      ],
      optionsClaseLibreta: [
        {value: '0', label: 'NO APLICA'},
        {value: '1', label: 'PRIMERA CLASE'},
        {value: '2', label: 'SEGUNDA CLASE'}
      ],
      optionsTipoSanguineo: [
        {value: '1', label: 'NEGATIVO', fac_sangre: 'O', fac_sangre1: '-O', rhrh: '-'},
        {value: '2', label: 'POSITIVO', fac_sangre: 'O', fac_sangre1: '+O', rhrh: '+'},
        {value: '3', label: 'NEGATIVO', fac_sangre: 'A', fac_sangre1: '-A', rhrh: '-'},
        {value: '4', label: 'POSITIVO', fac_sangre: 'A', fac_sangre1: '+A', rhrh: '+'},
        {value: '5', label: 'NEGATIVO', fac_sangre: 'B', fac_sangre1: '-B', rhrh: '-'},
        {value: '6', label: 'POSITIVO', fac_sangre: 'B', fac_sangre1: '+B', rhrh: '+'},
        {value: '7', label: 'NEGATIVO', fac_sangre: 'AB', fac_sangre1: '-AB', rhrh: '-'},
        {value: '8', label: 'POSITIVO', fac_sangre: 'AB', fac_sangre1: '+AB', rhrh: '+'}
      ],
      optionsEstadoCivil: [
        {value: '0', label: 'Soltero(a)'},
        {value: '1', label: 'Casado(a)'},
        {value: '2', label: 'Viudo(a)'},
        {value: '3', label: 'Divorsiado(a)'},
        {value: '4', label: 'Union Libre'},
      ],
      optionsConfianza: [
        {value: '0', label: 'SI'},
        {value: '1', label: 'NO'}
      ],
      optionsPerfil: [
        {value: '0', label: 'Empleado Administrativo'},
        {value: '1', label: 'Jefe Administrativo'},
        {value: '2', label: 'Ingeniero de Proceso'},
        {value: '3', label: 'Supervisor'},
        {value: '4', label: 'Técnico Principal'},
        {value: '5', label: 'Técnico Auxiliar'},
        {value: '6', label: 'Capataz'},
        {value: '7', label: 'Gestor Comunitario'}
      ],
      optionsTipoContrato: [
        {value: '0', label: 'Termino Fijo (< 1 año)'},
        {value: '1', label: 'Termino Fijo (= 1 año)'},
        {value: '2', label: 'Termino Indefinido'},
        {value: '3', label: 'Labor Pactada (D.O.)'}
      ],
      fileList: [],
      sw: false,
      indexUpdate: null,
      modalVisible: false,
      dialogBuscar: false,
    }),
    methods: {
      addFuncionario: function (formName) {
        // this.$refs[formName].validate((valid) => {
        //   if (valid) {
        var fileValue = document.querySelector('.el-upload .el-upload__input');
        var data = new FormData();
        this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
        this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
        this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
        this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
        this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
        this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
        this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
        this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
        this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
        this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
        this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
        this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
        this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
        this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
        this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
        this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
        this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
        this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
        this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
        this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
        this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
        this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
        this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

        // var keys=Object.keys(this.ruleForm);
        // for(i=0;i<keys.length;i++){
        //   data.append(keys[i], this.ruleForm[keys[i]]);
        // }
        // console.log(this.ruleForm)
        // console.log(Object.keys(this.ruleForm));
        <!--region FormData DATOS_RRHH-->
        data.append('rh_cedula', this.ruleForm.rh_cedula);
        data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
        data.append('rh_expcedula', this.ruleForm.rh_expcedula);
        data.append('rh_apellido1', this.ruleForm.rh_apellido1);
        data.append('rh_apellido2', this.ruleForm.rh_apellido2);
        data.append('rh_nombre', this.ruleForm.rh_nombre);
        data.append('rh_direccion', this.ruleForm.rh_direccion);
        data.append('rh_codciudad', this.ruleForm.rh_codciudad);
        data.append('rh_ciudad', this.ruleForm.rh_ciudad);
        data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
        data.append('rh_barrio', this.ruleForm.rh_barrio);
        data.append('rh_telefono', this.ruleForm.rh_telefono);
        data.append('rh_movil', this.ruleForm.rh_movil);
        data.append('rh_sexo', this.ruleForm.rh_sexo);
        data.append('rh_dessexo', this.ruleForm.rh_dessexo);
        data.append('rh_facsangre', this.ruleForm.rh_facsangre);
        data.append('rh_rh', this.ruleForm.rh_rh1);
        data.append('rh_desrh', this.ruleForm.rh_desrh);
        data.append('rh_estcivil', this.ruleForm.rh_estcivil);
        data.append('rh_fechanac', this.ruleForm.rh_fechanac);
        data.append('rh_codciunac', this.ruleForm.rh_codciunac);
        data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
        data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
        data.append('rh_dptonac', this.ruleForm.rh_dptonac);
        data.append('rh_codnacional', this.ruleForm.rh_codnacional);
        data.append('rh_nacional', this.ruleForm.rh_nacional);
        data.append('rh_estado', this.ruleForm.rh_estado);
        data.append('rh_foto', fileValue.files[0]);
        data.append('rh_nombre1', this.ruleForm.rh_nombre1);
        data.append('rh_nombre2', this.ruleForm.rh_nombre2);
        <!--endregion-->

        <!--region FormData TECNICOS-->
        data.append('te_codigo', '');
        data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
        data.append('te_direccion', this.ruleForm.rh_direccion);
        data.append('te_telefono', this.ruleForm.rh_telefono);
        data.append('te_ciudad', this.ruleForm.rh_ciudad);
        data.append('te_cedula', this.ruleForm.rh_cedula);
        <!--endregion-->

        data.append('dl_cedula', this.ruleForm.rh_cedula);
        data.append('dl_conte', '');
        data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
        data.append('dl_descentrab', this.ruleForm.dl_descentrab);
        data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
        data.append('dl_descencosto', this.ruleForm.dl_descencosto);
        data.append('dl_salario', this.ruleForm.dl_salario);
        data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
        data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
        data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
        data.append('dl_distritomil', this.ruleForm.dl_distritomil);
        data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
        data.append('dl_fechaven', this.ruleForm.dl_fechaven);
        data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
        data.append('dp_cedula', this.ruleForm.rh_cedula);
        data.append('dp_codarp', this.ruleForm.dp_codarp);
        data.append('dp_desarp', this.ruleForm.dp_desarp);
        data.append('dp_codeps', this.ruleForm.dp_codeps);
        data.append('dp_deseps', this.ruleForm.dp_deseps);
        data.append('dp_afp', this.ruleForm.dp_afp);
        data.append('dp_desafp', this.ruleForm.dp_desafp);
        data.append('dp_codcaja', this.ruleForm.dp_codcaja);
        data.append('dp_descaja', this.ruleForm.dp_descaja);
        data.append('dp_codbanco', this.ruleForm.dp_codbanco);
        data.append('dp_desbanco', this.ruleForm.dp_desbanco);
        data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
        data.append('dp_convencion', this.ruleForm.dp_convencion);
        data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
        data.append('em_cedula', this.ruleForm.rh_cedula);
        data.append('em_codingeniero', this.ruleForm.em_codingeniero);
        data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
        data.append('em_programa', this.ruleForm.em_desprograma);
        data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
        data.append('em_codarea', this.ruleForm.em_codarea);
        data.append('em_desarea', this.ruleForm.em_desarea);
        data.append('em_codcargo', this.ruleForm.em_codcargo);
        data.append('em_descargo', this.ruleForm.em_descargo);
        data.append('em_confianza', this.ruleForm.em_confianza);
        data.append('em_perfil', this.ruleForm.em_perfil);
        data.append('ta_cedula', this.ruleForm.rh_cedula);
        data.append('ta_camisa', this.ruleForm.ta_camisa);
        data.append('ta_pantalon', this.ruleForm.ta_pantalon);
        data.append('ta_botas', this.ruleForm.ta_botas);
        data.append('ta_tipo', this.ruleForm.ta_tipo);
        data.append('dc_cedula', this.ruleForm.rh_cedula);
        data.append('conte', JSON.stringify(this.tableData));
        data.append('df_cedula', this.ruleForm.rh_cedula);
        data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));
        data.append('co_cedula', this.ruleForm.rh_cedula);
        data.append('contrato', JSON.stringify(this.tableDataContrato));
        data.append('cedula', this.ruleForm.rh_cedula);
        data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
        axios.post('request/insertFuncionario.php', data).then(response => {
          console.log(response.data.message);
          if (response.data.error === 0) {
            this.$notify({
              title: response.data.title,
              message: response.data.message,
              type: response.data.type
            });
          }
        }).catch(e => {
          console.log(e.response);
        });
        //   } else {
        //     console.log('error submit!!');
        //     return false;
        //   }
        // });
      },
      onUpdate () {
        var fileValue = document.querySelector('.el-upload .el-upload__input');
        var data = new FormData();

        this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
        this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
        this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
        this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
        this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
        this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
        this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
        this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
        this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
        this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
        this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
        this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
        this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
        this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
        this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
        this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
        this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
        this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
        this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
        this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
        this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
        this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
        this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

        //DATOS_RRHH
        data.append('rh_cedula', this.ruleForm.rh_cedula);
        data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
        data.append('rh_expcedula', this.ruleForm.rh_expcedula);
        data.append('rh_apellido1', this.ruleForm.rh_apellido1);
        data.append('rh_apellido2', this.ruleForm.rh_apellido2);
        data.append('rh_nombre', this.ruleForm.rh_nombre);
        data.append('rh_direccion', this.ruleForm.rh_direccion);
        data.append('rh_codciudad', this.ruleForm.rh_codciudad);
        data.append('rh_ciudad', this.ruleForm.rh_ciudad);
        data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
        data.append('rh_barrio', this.ruleForm.rh_barrio);
        data.append('rh_telefono', this.ruleForm.rh_telefono);
        data.append('rh_movil', this.ruleForm.rh_movil);
        data.append('rh_sexo', this.ruleForm.rh_sexo);
        data.append('rh_dessexo', this.ruleForm.rh_dessexo);
        data.append('rh_facsangre', this.ruleForm.rh_facsangre);
        data.append('rh_rh', this.ruleForm.rh_rh1);
        data.append('rh_desrh', this.ruleForm.rh_desrh);
        data.append('rh_estcivil', this.ruleForm.rh_estcivil);
        data.append('rh_fechanac', this.ruleForm.rh_fechanac);
        data.append('rh_codciunac', this.ruleForm.rh_codciunac);
        data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
        data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
        data.append('rh_dptonac', this.ruleForm.rh_dptonac);
        data.append('rh_codnacional', this.ruleForm.rh_codnacional);
        data.append('rh_nacional', this.ruleForm.rh_nacional);
        data.append('rh_estado', this.ruleForm.rh_estado);
        data.append('rh_foto', fileValue.files[0]);
        data.append('rh_nombre1', this.ruleForm.rh_nombre1);
        data.append('rh_nombre2', this.ruleForm.rh_nombre2);
        //
        axios.post('request/updateFuncionario.php', data).then(response => {
          console.log(response.data.message);
          if (response.data.error === 0) {
            this.$notify({
              title: response.data.title,
              message: response.data.message,
              type: response.data.type
            });
          }
        }).catch(e => {
          console.log(e.response);
        });
      },
      showDepartamentos () {
        axios.get('request/departamentos.php').then(response => {
          this.optionsDpto = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showMunicipios () {
        var mu_depto = new FormData();
        mu_depto.append('mu_depto', this.ruleForm.rh_coddptonac);
        axios.post('request/municipios.php', mu_depto).then(response => {
          this.optionsMunicipio = response.data;
        }).catch(error => {
          console.log(error.response);
        })
      },
      showCiudades () {
        axios.get('request/ciudades.php').then(response => {
          this.optionsCiudades = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showBarrios () {
        var ba_mpio = new FormData();
        ba_mpio.append('ba_mpio', this.ruleForm.rh_codciudad);
        axios.post('request/barrios.php', ba_mpio).then(response => {
          this.optionsBarrio = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showNacionalidades () {
        axios.get('request/nacionalidades.php').then(response => {
          this.optionsNacionalidades = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCentroTrabajo () {
        axios.get('request/centroTrabajo.php').then(response => {
          this.optionscentroTrabajo = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCentroCosto () {
        axios.get('request/centroCosto.php').then(response => {
          this.optionscentroCosto = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showARP () {
        axios.get('request/arp.php').then(response => {
          this.optionsArp = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showEPS () {
        axios.get('request/eps.php').then(response => {
          this.optionsEps = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showAFP () {
        axios.get('request/afp.php').then(response => {
          this.optionsAfp = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCaja () {
        axios.get('request/caja.php').then(response => {
          this.optionsCaja = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showBancos () {
        axios.get('request/bancos.php').then(response => {
          this.optionsBanco = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showJefeInmediato () {
        axios.get('request/datos_rrhh.php').then(response => {
          this.optionsJefeInmediato = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCampanas () {
        axios.get('request/campanas.php').then(response => {
          this.optionsCampanas = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showAreas () {
        axios.get('request/areas.php').then(response => {
          this.optionsAreas = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCargos () {
        axios.get('request/cargos.php').then(response => {
          this.optionsCargos = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showConte () {
        axios.get('request/conte.php').then(response => {
          this.optionsConte = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showTipoEstudio () {
        axios.get('request/tipo_estudio.php').then(response => {
          this.optionsTipoEstudio = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showCategoriaDotacion () {
        axios.get('request/categoria_dotacion.php').then(response => {
          this.optionsCategoriaDotacion = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showEmpresa () {
        axios.get('request/empresa.php').then(response => {
          this.optionsEmpresa = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showMotivoRetiro () {
        axios.get('request/motivo_retiro.php').then(response => {
          this.optionsMotivoRetiro = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showTipoVencimiento () {
        axios.get('request/tipo_vencimiento.php').then(response => {
          this.optionsTipoVencimiento = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showFuncionarios () {
        axios.get('request/getFuncionario.php').then(response => {
          console.log(response)
          this.tableDataBuscar = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      addDatosConte () {
        this.ruleFormConte.dc_desconte = this.valueSelect(this.optionsConte, 'co_codigo', 'co_nombre', this.ruleFormConte.dc_codconte);
        var data = this.ruleFormConte;
        this.tableData.push(data);
        this.clear();
      },
      deleteRow (row, index) {
        this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          type: 'warning'
        }).then(() => {
          this.$delete(this.tableData, index);
          this.$message({
            type: 'success',
            message: 'Registro eliminado con exito.'
          });
        }).catch(() => {
          this.$message({
            type: 'info',
            message: 'Operación cancelada por el usuario.'
          });
        });
      },
      addFormacionAcademica () {
        this.ruleFormFAcademica.df_formacademica = this.valueSelect(this.optionsTipoEstudio, 'es_codigo', 'es_nombre', this.ruleFormFAcademica.df_codigo);
        var data = this.ruleFormFAcademica;
        this.tableDataInfoAcademinaca.push(data);
        this.clearFAcademica();
      },
      addDatosContrato () {
        this.ruleFormDatosContrato.co_empresa = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
        this.ruleFormDatosContrato.co_motivoretiro = this.valueSelect(this.optionsMotivoRetiro, 'mr_codigo', 'mr_motivo', this.ruleFormDatosContrato.co_codretiro);
        // this.ruleFormDatosContrato.co_motivoreal = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
        this.ruleFormDatosContrato.co_item = this.tableDataContrato.length + 1;
        var data = this.ruleFormDatosContrato;
        if (!this.sw) {
          this.tableDataContrato.push(data)
        } else {
          this.tableDataContrato.splice(this.indexUpdate, 1, data);
          this.sw = false;
          this.clearDatosContrato();
        }
        this.clearDatosContrato();
      },
      editRowContrato (row, index) {
        console.log(row)
        this.ruleFormDatosContrato.co_codempresa = row.co_codempresa;
        this.ruleFormDatosContrato.co_empresa = row.co_empresa;
        this.ruleFormDatosContrato.co_fechaing = row.co_fechaing;
        this.ruleFormDatosContrato.co_fechafin = row.co_fechafin;
        this.ruleFormDatosContrato.co_codretiro = row.co_codretiro;
        this.ruleFormDatosContrato.co_motivoretiro = row.co_motivoretiro;
        this.ruleFormDatosContrato.co_tcontrato = row.co_tcontrato;
        var data = this.ruleFormDatosContrato;
        this.indexUpdate = index;
        this.modalVisible = true;
        this.sw = 1;
      },
      deleteRowContrato (row, index) {
        this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          type: 'warning'
        }).then(() => {
          this.$delete(this.tableDataContrato, index);
          this.sw = false;
          this.$message({
            type: 'success',
            message: 'Registro eliminado con exito.'
          });
        }).catch(() => {
          this.$message({
            type: 'info',
            message: 'Operación cancelada por el usuario.'
          });
        });
      },
      addVencimientos () {
        // this.ruleFormVencimientos.descripcion = this.valueSelect(this.optionsTipoVencimiento, 'tvcodigo', 'tvdescripcion', this.ruleFormVencimientos.codtipovence);
        var data = this.ruleFormVencimientos;
        this.tableDataVenciomientos.push(data);
        this.clearVencimientos();
      },
      deleteRowVencimiento (row, index) {
        this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          type: 'warning'
        }).then(() => {
          this.$delete(this.tableDataVenciomientos, index);
          this.$message({
            type: 'success',
            message: 'Registro eliminado con exito.'
          });
        }).catch(() => {
          this.$message({
            type: 'info',
            message: 'Operación cancelada por el usuario.'
          });
        });
      },
      handleChange (file, fileList1) {
        this.fileList = fileList1
      },
      handlePreview (file) {
        // this.fileList = file
        // console.log(file)
      },
      valueSelect (data, value, label, dato) {
        let labelReturn = '';
        data.forEach((item, index, arr) => {
          if (dato === item[value]) {
            labelReturn = item[label];
            // arr.length = 0
            return false;
          }
        });
        return labelReturn;
      },
      indexMethod (index) {
        return index;
      },
      indexMethod1 (index) {
        return index + 1;
      },
      indexMethod2 (index) {
        return index;
      },
      rowClick (row) {
        this.ruleForm.rh_cedula = row.rh_cedula
        this.ruleForm.rh_codexpciud = row.rh_codexpciud
        this.ruleForm.rh_expcedula = row.rh_expcedula
        this.ruleForm.rh_apellido1 = row.rh_apellido1
        this.ruleForm.rh_apellido2 = row.rh_apellido2
        this.ruleForm.rh_nombre1 = row.rh_nombre1
        this.ruleForm.rh_nombre2 = row.rh_nombre2
        this.ruleForm.rh_direccion = row.rh_direccion
        this.ruleForm.rh_codciudad = row.rh_codciudad
        this.ruleForm.rh_ciudad = row.rh_ciudad
        this.ruleForm.rh_codbarrio = row.rh_codbarrio
        this.ruleForm.rh_barrio = row.rh_barrio
        this.showBarrios();
        this.ruleForm.rh_telefono = row.rh_telefono
        this.ruleForm.rh_movil = row.rh_movil
        this.ruleForm.rh_sexo = row.rh_sexo
        this.ruleForm.rh_dessexo = row.rh_dessexo
        this.ruleForm.rh_facsangre = row.rh_facsangre
        this.ruleForm.rh_rh1 = row.rh_rh
        this.ruleForm.rh_desrh = row.rh_desrh
        this.ruleForm.rh_estcivil = row.rh_estcivil
        this.ruleForm.rh_fechanac = row.rh_fechanac
        this.ruleForm.rh_coddptonac = row.rh_coddptonac
        this.ruleForm.rh_dptonac = row.rh_dptonac
        this.ruleForm.rh_codciunac = row.rh_codciudnac
        this.ruleForm.rh_ciudadnac = row.rh_ciudadnac
        this.ruleForm.rh_codnacional = row.rh_codnacional
        this.ruleForm.rh_nacional = row.rh_nacional
        this.showMunicipios();
        this.dialogBuscar = false;
      },
      clear () {
        this.ruleFormConte = {
          dc_codconte: '',
          dc_desconte: ''
        }
      },
      clearFAcademica () {
        this.ruleFormFAcademica = {
          df_codigo: '',
          df_formacademica: '',
          df_insteducativa: '',
          df_tituloobt: ''
        }
      },
      clearDatosContrato () {
        this.ruleFormDatosContrato = {
          co_codempresa: '',
          co_empresa: '',
          co_fechaing: '',
          co_fechafin: '',
          co_tcontrato: '',
          co_codretiro: '',
          co_motivoretiro: '',
          co_codreal: '',
          co_motivoreal: '',
          co_fecharet: '',
          co_observacion: ''
        }
      },
      clearVencimientos () {
        this.ruleFormVencimientos = {
          codtipovence: '',
          descripcion: '',
          fechavence: ''
        }
      }
    },
    mounted () {
      this.showDepartamentos();
      this.showCiudades();
      this.showNacionalidades();
      this.showCentroTrabajo();
      this.showCentroCosto();
      this.showARP();
      this.showEPS();
      this.showAFP();
      this.showCaja();
      this.showBancos();
      this.showJefeInmediato();
      this.showCampanas();
      this.showAreas ();
      this.showCargos();
      this.showConte();
      this.showTipoEstudio();
      this.showCategoriaDotacion();
      this.showEmpresa();
      this.showMotivoRetiro();
      this.showTipoVencimiento();
      this.showFuncionarios();
    }
  });