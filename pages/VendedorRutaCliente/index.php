<?php
  session_start();
   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo"<script>window.location.href='../inicio/index.php';</script>";
    exit();
  } 
?>

<!DOCTYPE html>

<html>
<head>
  <meta charset="UTF-8">
  <title>Rutas por vendedor</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel="stylesheet" type="text/css" href="datatables/dataTables.bootstrap.css">
  <!-- <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' /> -->
  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' /> -->
  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <!-- <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' /> -->
  <!-- <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script> -->
  <!-- <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css"> -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <style>
    header {
      padding: 0px 4px 0px 0px;
      position: fixed;
      /* position: relative; */
      border-bottom: 1px solid #dedede;
      background-color: #ededed;
      overflow: hidden;
      left: 0;
      right: 0;
      z-index: 100;
    }
    header h3 {
      color: #555;
      font-weight: normal;
      font-size: 1rem;
      text-align: center;
      padding: 10px;
    }
    nav {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
    }
    .el-form-item--mini.el-form-item, .el-form-item--small.el-form-item {
      width: 100%;
    }
    .el-form-item--mini .el-form-item__content, .el-form-item--mini .el-form-item__label{
      line-height: 24px;
    }
    .el-select {
      width: 100%;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    .b-table th{
      background: rgb(0, 3, 74);
      font-weight: normal;
    }
    .table thead td, .table thead th{
      color: #FFFFFF !important;
    }
    .b-table .table th {
      font-weight: 200;
    }
    .b-table .table.is-bordered th.is-current-sort, .b-table .table.is-bordered th.is-sortable:hover {
      background: rgb(0, 3, 74) !important;
    }
    .table.is-bordered td, .table.is-bordered th {
      border-width: 0px !important;
    }
    .btn{
      background: none !important;
      border: .1px solid #DCDFE6 !important;
      height: 2.7rem !important;
    }
    .el-button:hover{
      background-color: #DDDDDD !important;
      color: #555 !important;
      border-color: #DDDDDD;
    }
    .el-button:focus {
      color: #555 !important;
      border-color: #DDDDDD !important;
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .buscar {
      padding-left: 0px !important;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-message-box{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-dialog{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .button, .file-cta, .file-name, .input, .pagination-ellipsis, .pagination-link, .pagination-next, .pagination-previous, .select select, .taginput .taginput-container.is-focusable, .textarea{
      border-radius: 0 !important;
      -webkit-border-radius: 0 !important;
      -moz-border-radius: 0 !important;
      -ms-border-radius: 0 !important;
      -o-border-radius: 0 !important;
    }
    .select:not(.is-multiple):not(.is-loading):after{
      font-size: 14px !important;
      border-color: #C0C4CC !important;
    }
    .navbar-link:after, .select:not(.is-multiple):not(.is-loading):after{
      border: 1px solid transparent;
      margin-top: -0.7em !important;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    .window-dialog, .el-dialog{
      width: 80%;
    }
    [class*=" el-icon-"], [class^=el-icon-]{
      /* margin-left: -.4rem; */
    }
    .img-thumbnail {
      display: inline-block;
      /* max-width: 100%; */
      height: auto;
      padding: 4px;
      line-height: 1em;
      background-color: #fff;
      border: 1px solid #ddd;
      /* border-radius: 4px; */
      border-radius: 50%;
      -webkit-transition: all .2s ease-in-out;
      -o-transition: all .2s ease-in-out;
      transition: all .2s ease-in-out;
    }
    .marco{
      padding: 1rem;
      border: 1px solid silver;
      margin: 1rem;
      background-color: #ededed;
    }
    .activo{
      color: #FF0000 !important;
    }
    select:focus{
      border-color: #409EFF;
      outline:0px;
    }
    .window-dialog, .el-dialog{

    }
    .el-dialog__wrapper{
      overflow: hidden;
    }
    @media only screen and (max-width: 768px) {
      .window-dialog, .el-dialog{
        width: 100%;
      }
      .el-dialog {
        width: 90%;
      }
      .el-message {
        width: 90%;
      }
      .el-table .cell{
        font-size: .8em;
      }
      #list-img img{
      	width: 296px;
    		height: 170px;
      }
      header h3{
        text-align: left;
        font-size: .8em;
      }
      .fileupload, .fileupload-new{
        text-align: center;
      }
      .btn{
        height: unset !important;
      }
      .marco-interno{
        margin: 0rem;
      }
    }
  </style>
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="js/select2/select2.full.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <link rel="stylesheet" href="https://unpkg.com/buefy/lib/buefy.min.css">

  <!-- Buefy JavaScript -->
  <script src="https://unpkg.com/buefy"></script>
  <script src="datatables/jquery.dataTables.min.js"></script>
  <script src="datatables/dataTables.bootstrap.min.js"></script>
  
</head>
<body>
  <div id="app">
    <header>
      <h3>Rutas por vendedor</h3>
      <nav>
      <!-- dialogBuscar = true -->
      <el-button v-if="sw" @click="addVendedorRutaCliente" icon="el-icon-check" class="btn">Guardar</el-button>
      <el-button v-if="!sw" @click="onUpdate" icon="el-icon-check" class="btn">Actualizar</el-button>
      <el-button v-if="!sw" @click="onDelete" icon="el-icon-close" class="btn">Eliminar</el-button>
      </nav>
    </header>
    
    <el-main>
      <el-form :model="ruleForm" size="mini" style="margin-top: 3rem;">
        <el-row :gutter="8">
          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Vendedor">
              <el-select v-model="ruleForm.codvende" @change="onChangeVendedor" filterable clearable placeholder="">
                <el-option
                  v-for="item in optionsVendedor"
                  :key="item.ve_cedula"
                  :label="item.ve_nombres"
                  :value="item.ve_cedula">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
            <el-form-item label="Ruta/día">
              <el-select v-model="ruleForm.ruta" @change="onChangeRutaDia" clearable placeholder="">
                <el-option
                  v-for="item in optionsDiasSemana"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
            <el-form-item label="Nit">
              <el-input v-model="ruleForm.cliente" @keyup.enter.native="showClientesBY" @blur="showClientesBY"></el-input>
            <!-- <b-select placeholder="" icon="account" isLoading>
              <option
                v-for="item in optionsCliente"
                :value="item.cl_nit"
                :key="item.cl_nit">
                {{ item.cl_noape }}
              </option>
            </b-select> -->
            <!-- <select v-model="ruleForm.cliente" class="el-select">
              <option v-for="item in optionsCliente" v-bind:value="item.cl_nit">
                {{ item.cl_noape }}
              </option>
            </select> -->
              <!-- <el-select v-model="ruleForm.cliente" placeholder="">
                <el-option
                  v-for="item in optionsCliente"
                  :key="item.cl_nit"
                  :label="item.cl_noape"
                  :value="item.cl_nit">
                </el-option>
              </el-select> -->
            </el-form-item>
          </el-col>

          <el-col :xs="1" :sm="1" :md="1" :lg="1" :xl="1" class="buscar">
            <el-form-item>
              <el-button @click="openDialog" :loading="isLoading" icon="el-icon-search" style="margin-top: 1.5rem;"></el-button>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Nombre">
              <el-input v-model="ruleForm.cl_noape" readOnly></el-input>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
            <el-form-item label="Orden">
              <el-input v-model="ruleForm.orden"></el-input>
            </el-form-item>
          </el-col>
        </el-row>
      </el-form>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableData"
            height="450"
            border
            size="mini"
            style="width: 100%">
            <el-table-column
              type="index"
              align="center"
              width="80"
              :index="indexMethod">
            </el-table-column>
            <el-table-column
              prop="codvende"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="ve_nombres"
              label="Nombre">
            </el-table-column>
            <el-table-column
              prop="ruta"
              label="Ruta/Día"
              width="100">
            </el-table-column>
            <el-table-column
              prop="cliente"
              label="Cod Cliente"
              width="100">
            </el-table-column>
            <el-table-column
              prop="cl_noape"
              label="Nombre">
            </el-table-column>
            <el-table-column
              prop="orden"
              label="Orden"
              width="80">
            </el-table-column>
            <el-table-column
              label="Acción"
              width="80">
              <template slot-scope="scope">
                <a href="javascript:void(0)" @click.stop.prevent="rowClick(scope.row, scope.$index)" style="margin-right: 1rem;">
                  <i class="el-icon-edit" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0)" @click.stop.prevent="onDelete(scope.row, scope.$index)">
                  <i class="el-icon-delete" aria-hidden="true"></i>
                </a>
              </template>
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
      
    </el-main>

    <el-dialog title="Busqueda de clientes" :visible.sync="dialogVisible" :close-on-click-modal="false" class="window-dialog">
      <el-row>
        <el-form @submit.native.prevent="filteredListClientes" size="mini">
          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item>
              <el-input v-model="inputSearch" @keyup.native="filteredListClientes">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>
        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <b-table @click="rowClickCliente" :bordered="true" paginated per-page="6" :data="tableDataClienteNew" :columns="columns"></b-table>
          <!-- <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>Cedula</th>
                <th>Nombre y Apellidos</th>
                <th>Dirección</th>
              </tr>
            </thead>
            <tbody id="datos1">
              <tr v-for="item in tableDataClienteNew">
                <td v-text="item.cl_nit"></td>
                <td v-text="item.cl_noape"></td>
                <td v-text="item.cl_direcci"></td>
              </tr>
            </tbody>
          </table> -->
          <!-- <el-table
            :data="tableDataClienteNew"
            border
            @row-click="rowClickCliente"
            size="mini"
            style="width: 100%">
            <el-table-column
              prop="cl_nit"
              header-align="left"
              align="right"
              width="90"
              label="Cedula">
            </el-table-column>
            <el-table-column
              prop="cl_noape"
              label="Nombre y Apellidos">
            </el-table-column>
            <el-table-column
              prop="cl_direcci"
              label="Dirección">
            </el-table-column>
          </el-table> -->
        </el-col>
      </el-row>
    </el-dialog>

    <el-dialog title="Actualizar ruta" :visible.sync="dialogVisibleRuta" :close-on-click-modal="false" class="window-dialog" width="40%">
      <el-row>
        <el-form size="mini">
          <el-row :gutter="8">

            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <el-form-item label="Ruta/día">
                <el-select v-model="ruleForm.ruta2" clearable placeholder="" style="width: 100%;">
                  <el-option
                    v-for="item in optionsDiasSemana"
                    :key="item.value"
                    :label="item.label"
                    :value="item.value">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <el-form-item label="Orden">
                <el-input v-model="ruleForm.orden2"></el-input>
              </el-form-item>
            </el-col>

          </el-row>


        </el-form>
      </el-row>

      <span slot="footer" class="dialog-footer">
        <el-button @click="onUpdate" type="primary">Actualizar</el-button>
      </span>

    </el-dialog>

  </div>

  <script>
  	ELEMENT.locale(ELEMENT.lang.es);
    Vue.use(Buefy.default);
    new Vue({
      el: '#app',
      data: () => ({
        ruleForm: {
          codvende: '',
          ruta: '',
          cliente: '',
          orden: '',
          cl_noape: '',
          codvende2: '',
          ruta2: '',
          orden2: ''
        },
        inputSearch: '',
        optionsVendedor: [],
        optionsCliente: [],
        optionsDiasSemana: [
          {value: 'LUNES', label: 'LUNES'},
          {value: 'MARTES', label: 'MARTES'},
          {value: 'MIERCOLES', label: 'MIERCOLES'},
          {value: 'JUEVES', label: 'JUEVES'},
          {value: 'VIERNES', label: 'VIERNES'},
          {value: 'SABADO', label: 'SABADO'},
          {value: 'DOMINGO', label: 'DOMINGO'}
        ],
        tableData: [],
        tableDataClientes:[],
        tableDataClienteNew: [],
        isLoading: false,
        sw: true,
        dialogVisible: false,
        dialogVisibleRuta: false,
        columns: [
          {field: 'cl_nit', label: 'Cedula'},
          {field: 'cl_noape', label: 'Nombre y Apellidos'},
          {field: 'cl_direcci', label: 'Dirección'},
          {field: 'cl_razonsocial', label: 'Razon social'}
        ]
      }),
      methods: {
        addVendedorRutaCliente () {
          if (this.ruleForm.orden === '' || this.ruleForm.codvende === '' || this.ruleForm.cliente === '' || this.ruleForm.ruta === '') {
            this.$alert('Faltan campos por diligenciar.', 'Validacion', {
              confirmButtonText: 'OK'
            });
          } else {
            var data = new FormData();
            data.append('orden', this.ruleForm.orden);
            data.append('codvende', this.ruleForm.codvende);
            data.append('ruta', this.ruleForm.ruta);
            data.append('cliente', this.ruleForm.cliente);
            axios.post('request/insertVendedorRutaCliente.php', data).then(response => {
              if (response.data.error === 0) {
                this.$notify({
                  title: response.data.title,
                  message: response.data.message,
                  type: response.data.type
                });
                this.showVendedorRutaClienteBY(this.ruleForm.codvende, this.ruleForm.ruta);
                this.ruleForm.cliente = '',
                this.ruleForm.cl_noape = ''
                this.ruleForm.orden = ''
              } else {
                this.$alert(response.data.message, response.data.title, {
                  confirmButtonText: 'OK'
                });
              }
            }).catch(e => {
              console.log(e.response);
            });
          }
        },
        onUpdate () {
          var data = new FormData();
          data.append('codvende', this.ruleForm.codvende2);
          data.append('ruta2', this.ruleForm.ruta2);
          data.append('ruta', this.ruleForm.ruta);
          data.append('cliente', this.ruleForm.cliente);
          data.append('orden', this.ruleForm.orden2);
          axios.post('request/updateVendedorRutaCliente.php', data).then(response => {
            if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
              this.showVendedorRutaClienteBY(this.ruleForm.codvende, this.ruleForm.ruta);
              this.ruleForm.cliente = '',
              this.ruleForm.cl_noape = ''
              this.ruleForm.orden = ''
              this.dialogVisibleRuta = false;
            } else {
              this.$alert(response.data.message, response.data.title, {
                confirmButtonText: 'OK'
              });
            }
          }).catch(e => {
            console.log(e.response);
          });
        },
        onDelete (row, index) {
          console.log(row)
          this.$confirm('Está seguro de eliminar el registro?', 'Confirmacion..!', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            var data = new FormData();
            data.append('codvende', row.codvende);
            data.append('ruta', row.ruta);
            data.append('cliente', row.cliente);
            axios.post('request/deleteVendedorRutaCliente.php', data).then(response => {
              if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
              this.showVendedorRutaCliente();
              this.limpiar();
            } else {
              this.$alert(response.data.message, response.data.title, {
                confirmButtonText: 'OK'
              });
            }
            }).catch(e => {
              console.log(e.response);
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        showVendedor () {
          axios.get('request/vendedor.php').then(response => {
            this.optionsVendedor = response.data;
          }).then(response => {
          }).catch(error => {
            console.log(e.response);
          })
        },
        showClientes () {
          this.isLoading = true
          axios.get('request/clientes.php').then(response => {
            // this.optionsCliente = response.data;
            this.tableDataClienteNew = response.data;
            this.tableDataClientes = response.data;
            this.dialogVisible = true;
            this.inputSearch = '';
            this.isLoading = false
          }).catch(error => {
            console.log(e.response);
            this.isLoading = false
          })
        },
        showClientesBY () {
          var data = new FormData();
          data.append('nit', this.ruleForm.cliente);
          axios.post('request/clientesBY.php', data).then(response => {
            this.ruleForm.cl_noape = response.data[0].cl_noape;
          }).catch(error => {
            console.log(e.response);
          })
        },
        showVendedorRutaCliente () {
          axios.get('request/getVendedorRutaCliente.php').then(response => {
            this.tableData = response.data;
          }).catch(error => {
            console.log(e.response);
          })
        },
        showVendedorRutaClienteBY (codvende, ruta) {
          var data = new FormData();
          data.append('codvende', codvende);
          data.append('ruta', ruta);
          axios.post('request/getVendedorRutaClienteBY.php', data).then(response => {
            this.tableData = response.data;
          }).catch(error => {
            console.log(e.response);
          })
        },
        rowClick (row) {
          this.ruleForm.codvende2 = row.codvende
          this.ruleForm.ruta = row.ruta
          this.ruleForm.ruta2 = row.ruta
          this.ruleForm.orden2 = row.orden
          this.ruleForm.cliente = row.cliente
          this.dialogVisibleRuta = true;
        },
        rowClickCliente (row) {
          this.ruleForm.cliente = row.cl_nit
          this.ruleForm.cl_noape = row.cl_noape
          this.dialogVisible = false;
        },
        openDialog () {
          this.showClientes();
        },
        filteredListClientes () {
          this.tableDataClienteNew = this.tableDataClientes.filter(item => {
            return item.cl_nit.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1 || item.cl_noape.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1 || item.cl_razonsocial.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1
          })
        },
        onChangeVendedor (value) {
          console.log(value)
          this.showVendedorRutaClienteBY(value, this.ruleForm.ruta);
        },
        onChangeRutaDia (value) {
          this.showVendedorRutaClienteBY(this.ruleForm.codvende, value);
        },
        indexMethod (index) {
          return index + 1;
        },
        limpiar () {
          this.ruleForm.codvende = '',
          this.ruleForm.ruta = '',
          this.ruleForm.cliente = '',
          this.ruleForm.cl_noape = ''
          this.ruleForm.orden = ''
        }
    	},
	    mounted () {
        this.showVendedor();
        // this.showVendedorRutaCliente();
	    }
    })
  </script>

</body>
</html>