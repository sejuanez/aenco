<?php
// header('Content-Type: charset=utf-8');
// ini_set('default_charset', 'utf-8');
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 11/07/2018
 * Time: 10:16 AM
 */

define('MAX_SEGMENT_SIZE', 65535);
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$codvende   = $_POST['codvende'];
$ruta       = $_POST['ruta'];
$ruta2      = $_POST['ruta2'];
$orden      = $_POST['orden'];
$cliente    = $_POST['cliente'];

$query = "UPDATE VEDEDORRUTACLIENTES SET RUTA = ?, ORDEN = ? WHERE CODVENDE = ? AND RUTA = ? AND CLIENTE = ?";
$data = ibase_prepare($conexion, $query);

ibase_execute($data,
    $ruta2, $orden, $codvende, $ruta, $cliente
);

$mensaje = array(
    'type'    => 'success',
    'message' => 'Registrado actualizado con exito..!',
    'title'   => 'Guardado..',
    'error'   => 0
);
echo json_encode($mensaje);