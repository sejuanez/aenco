<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 5/07/2018
 * Time: 5:08 PM
 */
define('MAX_SEGMENT_SIZE', 65535);
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$orden      = $_POST['orden'];
$codvende   = $_POST['codvende'];
$ruta       = $_POST['ruta'];
$cliente    = $_POST['cliente'];

$queryValidation = "SELECT COUNT(*) FROM vededorrutaclientes WHERE cliente = '$cliente'";
$return_arrValidation = array();
$dataValidation = ibase_query($conexion, $queryValidation);
$rowValidation = ibase_fetch_row($dataValidation);

if ($rowValidation[0] > 1) {
    $mensaje = array(
        'type'    => 'success',
        'message' => 'El cliente ya existe en la ruta seleccionada..!',
        'title'   => 'Validación..',
        'error'   => 1
    );
    echo json_encode($mensaje);
} else {
    $query = "INSERT INTO VEDEDORRUTACLIENTES (CODVENDE, RUTA, CLIENTE, ORDEN) VALUES (?, ?, ?, ?)";
    $data = ibase_prepare($conexion , $query);

    ibase_execute($data,
        $codvende, $ruta, $cliente, $orden
    );

    $mensaje = array(
        'type'    => 'success',
        'message' => 'Registrado guardado con exito..!',
        'title'   => 'Guardado..',
        'error'   => 0
    );
    echo json_encode($mensaje);
}