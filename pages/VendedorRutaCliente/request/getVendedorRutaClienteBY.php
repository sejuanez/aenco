<?php
    session_start();
    if (!$_SESSION['user']) {
      echo "<script>window.location.href='../../../index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $codvende = $_POST['codvende'];
    $ruta = $_POST['ruta'];

    $param = " ";
    if (!empty($codvende)) {
        $param = "WHERE vrc.codvende = $codvende ";
    }
    if (!empty($codvende) && !empty($ruta)) {
        $param = "WHERE vrc.codvende = '$codvende' AND vrc.ruta = '$ruta' ";
    }

    $query = "SELECT vrc.codvende, ve.ve_nombres, vrc.ruta, vrc.cliente, cl.cl_noape, vrc.orden FROM vededorrutaclientes vrc
    LEFT JOIN vendedor ve ON vrc.codvende = ve.ve_cedula
    left JOIN clientes cl ON vrc.cliente = cl.cl_nit
    $param ";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    
    
    while ($row = ibase_fetch_row($data)) {
        $row_array['codvende']   = utf8_encode($row[0]);
        $row_array['ve_nombres'] = utf8_encode($row[1]);
        $row_array['ruta']       = utf8_encode($row[2]);
        $row_array['cliente']    = utf8_encode($row[3]);
        $row_array['cl_noape']   = utf8_encode($row[4]);
        $row_array['orden']      = utf8_encode($row[5]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>