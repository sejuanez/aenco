<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 13/07/2018
 * Time: 8:24 AM
 */

session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$codvende = $_POST['codvende'];
$ruta     = $_POST['ruta'];
$cliente  = $_POST['cliente'];

$query = "DELETE FROM VEDEDORRUTACLIENTES WHERE CODVENDE = ? AND RUTA = ? AND CLIENTE = ?";
$data = ibase_prepare($conexion , $query);

ibase_execute($data, $codvende, $ruta, $cliente);

$mensaje = array(
    'type'    => 'success',
    'message' => 'Registrado eliminado con exito..!',
    'title'   => 'Guardado..',
    'error'   => 0
);
echo json_encode($mensaje);