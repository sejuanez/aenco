<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
  	$fechafin = $_POST['fechafin'];
  	$dpto = $_POST['dpto'];
	$proceso = $_POST['proceso'];


  $consulta = "SELECT o.ot_solicitud Sol_Interna, o.ot_solicitud_externa Sol_externa, o.ot_orden Orden_GOM, c.c_nombre proceso, o.ot_contratista Contratista, o.ot_fecha_asigna_contrata Fecha_asignacion, o.ot_fechaej Facha_atencion, (((coalesce(o.ot_fechaej, current_date) - o.ot_fecha_asigna_contrata) + 1) - (select CANT_DIAS_MUERTOS from calcular_dias2(o.ot_fecha_asigna_contrata, coalesce(o.ot_fechaej, current_date)))) Dias_Atencion, coalesce(oec.ote_fecha_envio, 'SIN ENVIAR') FECHA_ENVIO_CODENSA, case when o.ot_proceso_campana='FA' and o.ot_cod_dpto='11' then coalesce(((coalesce(oec.ote_fecha_envio, current_date) - o.ot_fecha_asigna_contrata)+1) - (select CANT_DIAS_MUERTOS from calcular_dias2(o.ot_fecha_asigna_contrata, oec.ote_fecha_envio)),'SIN ENVIAR') else coalesce((coalesce(oec.ote_fecha_envio, current_date) - o.ot_fechaej) - (select CANT_DIAS_MUERTOS from calcular_dias2(o.ot_fechaej, coalesce(oec.ote_fecha_envio, current_date))),'SIN ENVIAR') end Dias_envio_codensa, case WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25' then coalesce(oeg.ote_fecha_envio, 'SIN CARGAR') else 'NO APLICA' end FECHA_CARGUE_GOM, case WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25' then coalesce((oeg.ote_fecha_envio - o.ot_fechaej) - (select CANT_DIAS_MUERTOS from calcular_dias2(o.ot_fechaej, oeg.ote_fecha_envio)),'SIN CARGAR') ELSE 'NO APLICA' END Dias_cargue_gom, case WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25' then coalesce(oem.ote_fecha_envio, 'SIN CARGAR') else 'NO APLICA' end FECHA_CARGUE_MERCURIO, case WHEN o.ot_proceso_campana='RO' and o.ot_cod_dpto='25' then coalesce((oem.ote_fecha_envio - o.ot_fechaej) - (select CANT_DIAS_MUERTOS from calcular_dias2(o.ot_fechaej, oem.ote_fecha_envio)),'SIN CARGAR') ELSE 'NO APLICA' END Dias_cargue_mercurio, o.ot_cod_cliente COD_CLIENTE, o.ot_nombre_cliente NOMBRE_CLIENTE, o.ot_direccion_cliente DIRECCION_CLIENTE, d.de_nombre DEPARTAMENTO, m.mu_nombre MUNICIPIO, co.co_nombre LOCALIDAD, b.ba_nombarrio BARRIO, dur.ui_nombre RESULTADO, dua.ui_nombre ANOMALIA, t.te_nombres TECNICO, fv.fv_valor VALOR, fv.fv_ts TS, zfa.zf_zona ZONA from ot o left join campanas c on c.c_codigo=o.ot_proceso_campana left join ot_enviadas oec on oec.ote_solicitud=o.ot_solicitud and oec.ote_tipo_envio='01' left join ot_enviadas oeg on oeg.ote_solicitud=o.ot_solicitud and oeg.ote_tipo_envio='02' left join ot_enviadas oem on oem.ote_solicitud=o.ot_solicitud and oem.ote_tipo_envio='03' left join departamentos d on d.de_codigo=o.ot_cod_dpto left join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio left join corregimientos co on co.co_depto=o.ot_cod_dpto and co.co_municipio=o.ot_cod_mpio and co.co_codcorregimiento=o.ot_cod_localidad left join barrios b on b.ba_depto=o.ot_cod_dpto and b.ba_mpio=o.ot_cod_mpio and b.ba_sector=o.ot_cod_localidad and b.ba_codbarrio=o.ot_cod_barrio left join dato_unirre dur on dur.ui_acta=o.ot_solicitud and dur.ui_tipo='R' left join dato_unirre dua on dua.ui_acta=o.ot_solicitud and dua.ui_tipo='A' left join tecnicos t on t.te_codigo=o.ot_tecnico left join fac_valores fv on fv.fv_proceso=o.ot_proceso_campana and fv.fv_depto=o.ot_cod_dpto and fv.fv_mpio=o.ot_cod_mpio and fv.fv_corregimiento=o.ot_cod_localidad and fv.fv_codbarrio=o.ot_cod_barrio left join zonas_fac zfa on zfa.zf_dpto=o.ot_cod_dpto and zfa.zf_mpio=o.ot_cod_mpio and zfa.zf_corregi=o.ot_cod_localidad where o.ot_fecha_asigna_contrata between '".$fechaini."' and '".$fechafin."' and o.ot_cod_dpto='".$dpto."' and o.ot_proceso_campana='".$proceso."'";
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['solInterna'] = utf8_encode($fila[0]);
		$row_array['solExterna'] = utf8_encode($fila[1]);		
		$row_array['orden'] = utf8_encode($fila[2]);
		$row_array['proceso'] = utf8_encode($fila[3]);
		$row_array['contratista'] = utf8_encode($fila[4]);
		$row_array['fechaAsignacion'] = utf8_encode($fila[5]);
		$row_array['fechaAtencion'] = utf8_encode($fila[6]);
		$row_array['diasAtencion'] = utf8_encode($fila[7]);
		$row_array['fechaEnvioCodensa'] = utf8_encode($fila[8]);
		$row_array['diasEnvioCodensa'] = utf8_encode($fila[9]);
		$row_array['fechaCargueGom'] = utf8_encode($fila[10]);
		$row_array['diasCargueGom'] = utf8_encode($fila[11]);
		$row_array['fechaCargueMercurio'] = utf8_encode($fila[12]);
		$row_array['diasCargueMercurio'] = utf8_encode($fila[13]);
		$row_array['codCliente'] = utf8_encode($fila[14]);
		$row_array['cliente'] = utf8_encode($fila[15]);
		$row_array['direccion'] = utf8_encode($fila[16]);
		$row_array['departamento'] = utf8_encode($fila[17]);
		$row_array['municipio'] = utf8_encode($fila[18]);
		$row_array['localidad'] = utf8_encode($fila[19]);
		$row_array['barrio'] = utf8_encode($fila[20]);
		$row_array['resultado'] = utf8_encode($fila[21]);
		$row_array['anomalia'] = utf8_encode($fila[22]);
    $row_array['tecnico'] = utf8_encode($fila[23]);

    $row_array['valor'] = utf8_encode($fila[24]);
    $row_array['ts'] = utf8_encode($fila[25]);
		$row_array['zona'] = utf8_encode($fila[26]);

					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>