SET TERM ^ ;

create or alter procedure CENTRO_COSTO_BUSCA (
    EOPCION integer,
    ECOD varchar(15))
returns (
    CC_CODIGO varchar(5),
    CC_CENCOSTO varchar(80),
    CC_USUGRABA varchar(12),
    CC_TIPO varchar(5),
    CC_ESTADO varchar(1))
as
BEGIN
if (eopcion = 1) then
  begin
      FOR
        Select cc.cc_codigo, cc.cc_cencosto, cc.cc_usugraba, cc.cc_tipo, cc.cc_estado
        from centro_costo cc
        INTO :CC_CODIGO,
             :CC_CENCOSTO,
             :CC_USUGRABA,
             :cc_tipo,
             :CC_ESTADO
      DO
      BEGIN
        SUSPEND;
      END
  END

  --------------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        Select cc.cc_codigo, cc.cc_cencosto, cc.cc_usugraba, cc.cc_tipo, cc.cc_estado
        from centro_costo cc
        where cc.cc_codigo || cc.cc_cencosto like '%'||:ecod||'%'
        INTO :CC_CODIGO,
             :CC_CENCOSTO,
             :CC_USUGRABA,
             :cc_tipo,
             :CC_ESTADO

      DO
      BEGIN
        SUSPEND;
      END
  END

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON CENTRO_COSTO TO PROCEDURE CENTRO_COSTO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_COSTO_BUSCA TO SYSDBA;
