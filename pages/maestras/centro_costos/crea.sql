SET TERM ^ ;

create or alter procedure CENTRO_COSTO_CREA (
    ECOD varchar(5),
    ENOM varchar(80),
    EUSER varchar(12),
    ETIPO varchar(5),
    EESTADO varchar(1))
as
begin
    insert into centro_costo (
        cc_codigo, cc_cencosto, cc_usugraba, cc_tipo, cc_estado
        )
    values (
        :ecod,:enom,:euser,:etipo,:eestado
        );
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON CENTRO_COSTO TO PROCEDURE CENTRO_COSTO_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_COSTO_CREA TO SYSDBA;
