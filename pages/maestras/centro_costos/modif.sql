SET TERM ^ ;

create or alter procedure CENTRO_COSTO_MODIF (
    ECOD varchar(5),
    ENOM varchar(80),
    ETIPO varchar(5),
    EESTADO varchar(1))
as
begin
  update centro_costo c
  set
    c.cc_cencosto = :enom,
    c.cc_tipo = :etipo,
    c.cc_estado = :eestado
  where c.cc_codigo = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON CENTRO_COSTO TO PROCEDURE CENTRO_COSTO_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_COSTO_MODIF TO SYSDBA;
