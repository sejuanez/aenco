SET TERM ^ ;

create or alter procedure CENTRO_COSTO_BORRA (
    ECOD varchar(5))
as
begin
  delete from centro_costo c
  where c.cc_codigo = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON CENTRO_COSTO TO PROCEDURE CENTRO_COSTO_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_COSTO_BORRA TO SYSDBA;
