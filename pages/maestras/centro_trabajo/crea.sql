SET TERM ^ ;

create or alter procedure CENTRO_TRABAJO_CREA (
    ECOD varchar(5),
    ENOM varchar(80),
    EUSER varchar(12),
    EESTADO varchar(1))
as
begin
    insert into centro_trabajo (
        ct_codigo, CT_CENTRABAJO, ct_usugraba, ct_estado
        )
    values (
        :ecod,:enom,:euser,:eestado
        );
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON CENTRO_TRABAJO TO PROCEDURE CENTRO_TRABAJO_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_TRABAJO_CREA TO SYSDBA;
