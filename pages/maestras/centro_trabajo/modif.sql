SET TERM ^ ;

create or alter procedure CENTRO_TRABAJO_MODIF (
    ECOD varchar(5),
    ENOM varchar(80),
    EESTADO varchar(1))
as
begin
  update centro_trabajo c
  set
    c.ct_centrabajo = :enom,
    c.ct_estado = :eestado
  where c.ct_codigo = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON CENTRO_TRABAJO TO PROCEDURE CENTRO_TRABAJO_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_TRABAJO_MODIF TO SYSDBA;
