SET TERM ^ ;

create or alter procedure CENTRO_TRABAJO_BORRA (
    ECOD varchar(5))
as
begin
  delete from centro_trabajo c
  where c.ct_codigo = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON CENTRO_TRABAJO TO PROCEDURE CENTRO_TRABAJO_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_TRABAJO_BORRA TO SYSDBA;
