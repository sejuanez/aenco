SET TERM ^ ;

create or alter procedure CENTRO_TRABAJO_BUSCA (
    EOPCION integer,
    ECOD varchar(15))
returns (
    CC_CODIGO varchar(5),
    CC_CENCOSTO varchar(80),
    CC_USUGRABA varchar(12),
    CC_ESTADO varchar(1))
as
BEGIN
if (eopcion = 1) then
  begin
      FOR
        Select cc.ct_codigo, cc.ct_centrabajo, cc.ct_usugraba, cc.ct_estado
        from centro_trabajo cc
        INTO :CC_CODIGO,
             :CC_CENCOSTO,
             :CC_USUGRABA,
             :CC_ESTADO
      DO
      BEGIN
        SUSPEND;
      END
  END

  --------------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        Select cc.ct_codigo, cc.ct_centrabajo, cc.ct_usugraba, cc.ct_estado
        from centro_trabajo cc
        where cc.ct_codigo || cc.ct_centrabajo like '%'||:ecod||'%'
        INTO :CC_CODIGO,
             :CC_CENCOSTO,
             :CC_USUGRABA,
             :CC_ESTADO

      DO
      BEGIN
        SUSPEND;
      END
  END

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON CENTRO_TRABAJO TO PROCEDURE CENTRO_TRABAJO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE CENTRO_TRABAJO_BUSCA TO SYSDBA;
