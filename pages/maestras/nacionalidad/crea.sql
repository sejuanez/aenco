SET TERM ^ ;

create or alter procedure NACIONALIDAD_CREA (
    ECOD varchar(3),
    ENOM varchar(30),
    EUSER varchar(12),
    EESTADO varchar(1))
as
begin
    insert into nacionalidades (
        NA_CODIGO, NA_NACIONAL, NA_USUGRABA, NA_ESTADO
        )
    values (
        :ecod,:enom,:euser,:eestado
        );
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON NACIONALIDADES TO PROCEDURE NACIONALIDAD_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE NACIONALIDAD_CREA TO SYSDBA;
