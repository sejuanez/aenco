SET TERM ^ ;

create or alter procedure NACIONALIDADES_BORRA (
    ECOD varchar(20))
as
begin
  delete from nacionalidades
  where
        NA_CODIGO = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON NACIONALIDADES TO PROCEDURE NACIONALIDADES_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE NACIONALIDADES_BORRA TO SYSDBA;
