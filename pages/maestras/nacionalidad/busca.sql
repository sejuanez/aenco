SET TERM ^ ;

create or alter procedure NACIONALIDAD_BUSCA (
    EOPCION integer,
    ECOD varchar(20))
returns (
    CODIGO varchar(3),
    NOMBRE varchar(30),
    USU varchar(12),
    ESTADO varchar(1))
as
BEGIN
if (eopcion = 1) then
  begin
      FOR
        Select n.na_codigo, n.na_nacional, n.na_usugraba, n.na_estado
        from nacionalidades n
        INTO :codigo,
             :nombre,
             :usu,
             :estado
      DO
      BEGIN
        SUSPEND;
      END
  END

  --------------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        Select n.na_codigo, n.na_nacional, n.na_usugraba, n.na_estado
        from nacionalidades n
        where n.na_codigo || n.na_nacional like '%'||:ecod||'%'
        INTO :codigo,
             :nombre,
             :usu,
             :estado

      DO
      BEGIN
        SUSPEND;
      END
  END

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON NACIONALIDADES TO PROCEDURE NACIONALIDAD_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE NACIONALIDAD_BUSCA TO SYSDBA;
