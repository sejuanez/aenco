SET TERM ^ ;

create or alter procedure NACIONALIDADES_MODIF (
    ECOD varchar(3),
    ENOM varchar(30),
    EESTADO varchar(1))
as
begin
  update nacionalidades n
  set
    n.na_nacional = :enom,
    n.na_estado = :eestado
  where n.na_codigo = :ecod;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON NACIONALIDADES TO PROCEDURE NACIONALIDADES_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE NACIONALIDADES_MODIF TO SYSDBA;
