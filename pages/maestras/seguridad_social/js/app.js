// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        busqueda: "",
        codigo: "",
        nombre: "",
        tipo: "",
        activo: "",
        tablaDatos: [],

        codigoAux: "",
        nombreAux: "",
        tipoAux: "",

        btnModal: true,

    },


    methods: {

        nuevo: function () {
            let app = this;
            app.btnModal = true;
            app.resetCampos();
            $('#modal').modal('show');
            $("#codigo").prop('disabled', false);
        },

        loadDatos: function () {
            let app = this;
            $.post('./request/select.php', {
                mun: app.valorMunicipio,
                busqueda: app.busqueda.toUpperCase()
            }, function (data) {
                app.tablaDatos = JSON.parse(data);
            });
            app.busqueda = "";
        },

        verDatos: function (dato) {
            let app = this;

            app.btnModal = false;

            app.nombre = dato.nombre;
            app.nombreAux = dato.nombre;
            app.codigo = dato.codigo;
            app.codigoAux = dato.codigo;
            app.tipo = dato.tipo;
            app.tipoAux = dato.tipo;
            app.activo = dato.estado;

            $('#modal').modal('show');
            //$("#codigo").prop('disabled', true)

        },

        resetModal: function () {

            let app = this;

            $('#addFucionario').modal('hide');


        },

        guardar: function () {
            let app = this;

            if (app.codigo === "" ||
                app.nombre === "" ||
                app.tipo === "" ||
                app.activo === ""
            ) {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insert.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: app.codigo,
                        nombre: app.nombre,
                        tipo: app.tipo,
                        activo: app.activo,
                    },

                }).done(function (data) {

                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadDatos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },

        actualizar: function () {

            let app = this;


            if (app.codigo === "" ||
                app.nombre === "" ||
                app.tipo === "" ||
                app.estado === ""
            ) {

                alertify.error("Por favor ingresa todos los campos");

            } else {

                //hacemos la petición ajax
                $.ajax({
                    url: './request/update.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: app.codigo,
                        codigoAux: app.codigoAux,
                        nombre: app.nombre,
                        nombreAux: app.nombreAux,
                        tipo: app.tipo,
                        tipoAux: app.tipoAux,
                        activo: app.activo,
                    },

                }).done(function (data) {

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");
                        app.loadDatos();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {
                            alertify.warning("Ya existe un mes con este codigo");
                        } else {
                            alertify.error("Ha ocurrido un error");
                        }
                    }

                }).fail(function (data) {

                });

            }


        },

        eliminar: function (dato) {
            let app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " ?",
                function () {
                    $.post('./request/delete.php', {
                        codigo: dato.codigo,
                        nombre: dato.nombre,
                        tipo: dato.tipo,
                    }, function (data) {

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.loadDatos();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {
                                alertify.warning("Este item esta siendo utilizado");
                            } else {
                                alertify.error("Ha ocurrido un error");
                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        resetCampos: function () {

            let app = this;

            app.nombre = "";
            app.codigo = "";
            app.tipo = "";
            app.activo = "";

            $('#modal').modal('hide');


        },

    },


    watch: {},

    mounted() {
        this.loadDatos();
    },

});
