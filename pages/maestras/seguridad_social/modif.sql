SET TERM ^ ;

create or alter procedure SEGURIDAD_SOCIAL_MODIF (
    ECOD varchar(10),
    ENOM varchar(100),
    ETIPO varchar(5),
    EESTADO varchar(1),
    ECODAUX varchar(10),
    ENOMAUX varchar(100),
    ETIPOAUX varchar(5))
as
begin
  update seguridad_social s
  set
        s.ss_codigo = :ecod,
        s.ss_nombre = :enom,
        s.ss_tiposs = :etipo,
        s.ss_estado = :eestado
  where
        s.ss_codigo = :ecodAux and
        s.ss_nombre = :enomAux and
        s.ss_tiposs = :etipoAux;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON SEGURIDAD_SOCIAL TO PROCEDURE SEGURIDAD_SOCIAL_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SEGURIDAD_SOCIAL_MODIF TO SYSDBA;
