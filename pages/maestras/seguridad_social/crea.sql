SET TERM ^ ;

create or alter procedure SEGURIDAD_SOCIAL_CREA (
    ECOD varchar(10),
    ENOM varchar(100),
    ETIPO varchar(6),
    EUSER varchar(12),
    EESTADO varchar(1))
as
begin
    insert into seguridad_social (
            SS_CODIGO, SS_NOMBRE, SS_TIPOSS, SS_USUGRABA, SS_ESTADO
        )
    values (
        :ecod,:enom,:etipo,:euser,:eestado
        );
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON SEGURIDAD_SOCIAL TO PROCEDURE SEGURIDAD_SOCIAL_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SEGURIDAD_SOCIAL_CREA TO SYSDBA;
