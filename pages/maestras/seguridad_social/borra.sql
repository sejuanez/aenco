SET TERM ^ ;

create or alter procedure SEGURIDAD_SOCIAL_BORRA (
    ECOD varchar(10),
    ENOM varchar(100),
    ETIPO varchar(5))
as
begin
  delete from seguridad_social s
  where s.ss_codigo = :ecod
        and s.ss_nombre = :enom
        and s.ss_tiposs =:etipo;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON SEGURIDAD_SOCIAL TO PROCEDURE SEGURIDAD_SOCIAL_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SEGURIDAD_SOCIAL_BORRA TO SYSDBA;
