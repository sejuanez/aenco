SET TERM ^ ;

create or alter procedure SEGURIDAD_SOCIAL_BUSCA (
    EOPCION integer,
    ECOD varchar(20))
returns (
    COD varchar(10),
    NOM varchar(100),
    TIPO varchar(5),
    USU varchar(12),
    ESTADO varchar(1))
as
BEGIN
if (eopcion = 1) then
  begin
      FOR
        Select s.ss_codigo, s.ss_nombre, s.ss_tiposs, s.ss_usugraba, s.ss_estado
        from seguridad_social s
        order by s.ss_codigo
        INTO :cod,
             :nom,
             :tipo,
             :usu,
             :estado
      DO
      BEGIN
        SUSPEND;
      END
  END

  --------------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        Select s.ss_codigo, s.ss_nombre, s.ss_tiposs, s.ss_usugraba, s.ss_estado
        from seguridad_social s
        where upper(s.ss_codigo) || upper(s.ss_nombre) || s.ss_tiposs like '%'||upper(:ecod)||'%'
        order by s.ss_codigo
        INTO :cod,
             :nom,
             :tipo,
             :usu,
             :estado

      DO
      BEGIN
        SUSPEND;
      END
  END

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON SEGURIDAD_SOCIAL TO PROCEDURE SEGURIDAD_SOCIAL_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE SEGURIDAD_SOCIAL_BUSCA TO SYSDBA;
