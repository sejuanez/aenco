SET TERM ^ ;

create or alter procedure BANCO_BUSCA (
    EOPCION integer,
    ECOD varchar(20))
returns (
    COD varchar(3),
    NOM varchar(50),
    DIR varchar(50),
    TEL varchar(20),
    CODCIUDAD varchar(5),
    CIUDAD varchar(30),
    USU varchar(12),
    ESTADO varchar(1))
as
BEGIN
if (eopcion = 1) then
  begin
      FOR
        Select b.ba_codigo, b.ba_nombre, b.ba_direccion, b.ba_telefono, b.ba_codciudad, b.ba_ciudad, b.ba_usugraba, b.ba_estado
        from bancos b
        order by b.ba_codigo
        INTO :cod,
             :nom,
             :dir,
             :tel,
             :codCiudad,
             :ciudad,
             :usu,
             :estado
      DO
      BEGIN
        SUSPEND;
      END
  END

  --------------------------------------------

  if (eopcion = 2) then
  begin
      FOR
        Select b.ba_codigo, b.ba_nombre, b.ba_direccion, b.ba_telefono, b.ba_codciudad, b.ba_ciudad, b.ba_usugraba, b.ba_estado
        from bancos b
        where upper(b.ba_codigo) || upper(b.ba_nombre) like '%'||upper(:ecod)||'%'
        order by b.ba_codigo
        INTO :cod,
             :nom,
             :dir,
             :tel,
             :codCiudad,
             :ciudad,
             :usu,
             :estado

      DO
      BEGIN
        SUSPEND;
      END
  END

END^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON BANCOS TO PROCEDURE BANCO_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE BANCO_BUSCA TO SYSDBA;
