SET TERM ^ ;

create or alter procedure BANCO_MODIF (
    ENOM varchar(50),
    EDIR varchar(50),
    ETEL varchar(20),
    ECOD_CIUDAD varchar(5),
    ECIUDAD varchar(30),
    EESTADO varchar(1),
    ECODAUX varchar(3),
    ENOMAUX varchar(50))
as
begin
  update bancos b
  set
        b.ba_nombre = :enom,
        b.ba_direccion = :edir,
        b.ba_telefono = :etel,
        b.ba_codciudad = :ecod_ciudad,
        b.ba_ciudad = :eciudad,
        b.ba_estado = :eestado
  where
        b.ba_codigo = :ecodAux and
        b.ba_nombre = :enomAux;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON BANCOS TO PROCEDURE BANCO_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE BANCO_MODIF TO SYSDBA;
