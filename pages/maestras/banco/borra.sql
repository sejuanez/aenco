SET TERM ^ ;

create or alter procedure BANCO_BORRA (
    ECOD varchar(3),
    ENOM varchar(50))
as
begin
  delete from bancos b
  where
        b.ba_codigo = :ecod and
        b.ba_nombre = :enom;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,DELETE ON BANCOS TO PROCEDURE BANCO_BORRA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE BANCO_BORRA TO SYSDBA;
