SET TERM ^ ;

create or alter procedure BANCO_CREA (
    ECODIGO varchar(3),
    ENOMBRE varchar(50),
    EDIRECCION varchar(50),
    ETELEFONO varchar(20),
    ECOD_CIUDAD varchar(5),
    CIUDAD varchar(30),
    EUSU varchar(12),
    EESTADO varchar(1))
as
begin
    insert into bancos (
            BA_CODIGO, BA_NOMBRE, BA_DIRECCION, BA_TELEFONO, BA_CODCIUDAD, BA_CIUDAD, BA_USUGRABA, BA_ESTADO
        )
    values (
        :ecodigo,
        :enombre,
        :edireccion,
        :etelefono,
        :ecod_ciudad,
        :ciudad,
        :eusu,
        :eestado
        );
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT INSERT ON BANCOS TO PROCEDURE BANCO_CREA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE BANCO_CREA TO SYSDBA;
