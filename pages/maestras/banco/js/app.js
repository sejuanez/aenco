// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        busqueda: "",
        codigo: "",
        codigoAux: "",
        nombre: "",
        nombreAux: "",
        direccion: "",
        telefono: "",
        codCiudad: "",
        ciudad: "",
        estado: "",

        tablaDatos: [],

        btnModal: true,

    },


    methods: {

        nuevo: function () {
            let app = this;
            app.btnModal = true;
            app.resetCampos();
            $('#modal').modal('show');
            $("#codigo").prop('disabled', false);
        },

        loadDatos: function () {
            let app = this;
            $.post('./request/select.php', {
                mun: app.valorMunicipio,
                busqueda: app.busqueda.toUpperCase()
            }, function (data) {
                app.tablaDatos = JSON.parse(data);
            });
            app.busqueda = "";
        },

        verDatos: function (dato) {
            let app = this;

            app.btnModal = false;

            app.nombre = dato.nombre;
            app.nombreAux = dato.nombre;
            app.codigo = dato.codigo;
            app.codigoAux = dato.codigo;
            app.direccion = dato.dir;
            app.telefono = dato.tel;
            app.codCiudad = dato.codciudad;
            app.ciudad = dato.ciudad;
            app.estado = dato.estado;

            $('#modal').modal('show');
            $("#codigo").prop('disabled', true)

        },

        resetModal: function () {

            let app = this;

            $('#addFucionario').modal('hide');


        },

        guardar: function () {
            let app = this;

            if (app.codigo === "" ||
                app.nombre === ""
            ) {

                alertify.error("Por favor indica por lo menos codigo y nombre");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insert.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: app.codigo,
                        nombre: app.nombre,
                        direccion: app.direccion,
                        telefono: app.telefono,
                        codCiudad: app.codCiudad,
                        ciudad: app.ciudad,
                        estado: app.estado,
                    },

                }).done(function (data) {

                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadDatos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },

        actualizar: function () {

            let app = this;

            if (app.codigo === "" ||
                app.nombre === ""
            ) {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/update.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: app.codigo,
                        codigoAux: app.codigoAux,
                        nombre: app.nombre,
                        nombreAux: app.nombreAux,
                        direccion: app.direccion,
                        telefono: app.telefono,
                        codCiudad: app.codCiudad,
                        ciudad: app.ciudad,
                        estado: app.estado,
                    },

                }).done(function (data) {


                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.loadDatos();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {
                            alertify.warning("Ya existe un mes con este codigo");
                        } else {
                            alertify.error("Ha ocurrido un error");
                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {

                });

            }


        },

        eliminar: function (dato) {
            let app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " ?",
                function () {
                    $.post('./request/delete.php', {
                        codigo: dato.codigo,
                        nombre: dato.nombre,
                    }, function (data) {


                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.loadDatos();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {
                                alertify.warning("Este item esta siendo utilizado");
                            } else {
                                alertify.error("Ha ocurrido un error");
                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        resetCampos: function () {

            let app = this;

            app.nombre = "";
            app.codigo = "";
            app.direccion = "";
            app.telefono = "";
            app.codCiudad = "";
            app.ciudad = "";
            app.estado = "";

            $('#modal').modal('hide');


        },

    },


    watch: {},

    mounted() {
        this.loadDatos();
    },

});
