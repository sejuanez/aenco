Vue.use(Buefy.default)
new Vue({
  el: '#app',
  data: () => ({
    fecha: '',
    codvende: '',
  	optionsVendedor: [],
    tableData: []
  }),
  methods: {
  	showVendedor () {
      axios.get('request/vendedor.php').then(response => {
        this.optionsVendedor = response.data;
      }).catch(e => {
        console.log(e.response);
      });
    },
    showDetalle () {
      var data = new FormData();
      data.append('fecha', this.fecha);
      data.append('codvende', this.codvende);
      axios.post('request/detalles.php', data).then(response => {
        console.log(response.data);
        // this.optionsVendedor = response.data;
      }).catch(e => {
        console.log(e.response);
      });
    }
  },
  mounted () {
    this.showVendedor();
  }
})