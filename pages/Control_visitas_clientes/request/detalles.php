<?php
	session_start();
  	if(!$_SESSION['user']){
        echo "<script>window.location.href='../../inicio/index.php';</script>";
        exit();
    }
	include("../../../init/gestion.php");

	function saber_dia($nombredia) {
		list($m, $d, $Y) = explode("/", $nombredia);
		$dias = array('', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO', 'DOMINGO');
		$fecha = $dias[date('N', strtotime($nombredia))];
		return $fecha;
	}
	
	$fecha    = $_POST['fecha'];
	$codvende = $_POST['codvende'];
	$ruta     = saber_dia($fecha);
	
	$query = "SELECT vr.codvende, vr.cliente, c.cl_noape, c.cl_razonsocial, c.cl_direcci,
	COALESCE(gr.gp_estado, 'SIN VISITAR') estado,
	COALESCE(cg.cg_longitud,0) longitud,  COALESCE(cg.cg_latitud, 0) latitud,
	COALESCE(gr.gp_numroped, 'SIN PEDIDO') Nro_Pedido, A.nom_anomalia ANOMALIA, vr.ruta AS RUTA
	from vededorrutaclientes vr
	LEFT JOIN CLIENTES C ON C.cl_nit = vr.cliente
	LEFT JOIN georef_pedidos gr ON gr.gp_fecha = '$fecha' AND gr.gp_codcliente = vr.cliente AND gr.gp_vendedor = vr.codvende
	LEFT JOIN cli_georef CG ON cg.cg_nit = vr.cliente
	LEFT JOIN ANOMALIAS A ON A.cod_anomalia = GR.gp_codanomalia
	WHERE vr.codvende = '$codvende' AND vr.ruta = '$ruta'";
	$return_arr = array();
	

	$data = ibase_query($conexion, $query);

	while($rows = ibase_fetch_assoc($data)){
		// $row_array['CODVENDE']       = $rows[0];
		// $row_array['CLIENTE']        = $rows[1];
		// $row_array['CL_NOAPE']       = utf8_encode($rows[2]);
		// $row_array['CL_RAZONSOCIAL'] = utf8_encode($rows[3]);
		// $row_array['CL_DIRECCI']     = utf8_encode($rows[4]);
		// $row_array['ESTADO']         = utf8_encode($rows[5]);
		// $row_array['LONGITUD']       = $rows[6];
		// $row_array['LATITUD']        = $rows[7];
		// $row_array['NRO_PEDIDO']     = utf8_encode($rows[8]);
		// $row_array['ANOMALIA']       = $rows[9];
		// $row_array['RUTA']           = utf8_encode($rows[10]);
		// $rows['total_estado'] = array_count_values($rows);
		array_push($return_arr, $rows);
	}

	echo json_encode($return_arr);
	
?>