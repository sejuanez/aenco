<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    
    $query = "SELECT ve_cedula, ve_nombres FROM vendedor";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['ve_cedula'] = utf8_encode($row[0]);
		$row_array['ve_nombres'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>