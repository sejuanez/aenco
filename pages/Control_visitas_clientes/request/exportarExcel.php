<?php
	session_start();
  	if(!$_SESSION['user']){
        echo"<script>window.location.href='../../inicio/index.php';</script>";
        exit();
    }
    
    header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=gestion_".$_GET["fecha"]."-".$_GET["codvende"].".xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	include("../../../init/gestion.php");

	function saber_dia($nombredia) {
		list($m, $d, $Y) = explode("/", $nombredia);
		$dias = array('DOMINGO', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO');
		$fecha = $dias[date('N', strtotime($nombredia))];
		return $fecha;
	}

	$fecha    = $_GET['fecha'];
	$codvende = $_GET['codvende'];
	$ruta     = saber_dia($fecha);

	$query = "SELECT vr.codvende, vr.cliente, c.cl_noape, c.cl_razonsocial, c.cl_direcci,
	COALESCE(gr.gp_estado, 'SIN VISITAR') estado,
	COALESCE(cg.cg_longitud,0) longitud,  COALESCE(cg.cg_latitud, 0) latitud,
	COALESCE(gr.gp_numroped, 'SIN PEDIDO') Nro_Pedido, A.nom_anomalia ANOMALIA, vr.ruta AS RUTA
	from vededorrutaclientes vr
	LEFT JOIN CLIENTES C ON C.cl_nit = vr.cliente
	LEFT JOIN georef_pedidos gr ON gr.gp_fecha = '$fecha' AND gr.gp_codcliente = vr.cliente AND gr.gp_vendedor = vr.codvende
	LEFT JOIN cli_georef CG ON cg.cg_nit = vr.cliente
	LEFT JOIN ANOMALIAS A ON A.cod_anomalia = GR.gp_codanomalia
	WHERE vr.codvende = '$codvende' AND vr.ruta = '$ruta'";
	
	
	$data = ibase_query($conexion, $query);
	$return_arr = array();

	$table = "<table><tr class='cabecera'><td>Cod Vendedor</td><td>Cod Cliente</td><td>Cliente</td><td>".utf8_decode('Razón Social')."</td><td>".utf8_decode('Dirección')."</td><td>Estado</td><td>Latitud</td><td>Longitud</td><td>".utf8_decode('N° Peririodo')."</td><td>Anomalia</td><td>Ruta</td></tr>";

	while($row = ibase_fetch_row($data)) {
		$table.="<tr class='fila'>";
			$table.="<td>".$row_array['CODVENDE']       = $row[0]."</td>";
			$table.="<td>".$row_array['CLIENTE']        = $row[1]."</td>";
			$table.="<td>".$row_array['CL_NOAPE']       = utf8_encode($row[2])."</td>";
			$table.="<td>".$row_array['CL_RAZONSOCIAL'] = utf8_encode($row[3])."</td>";
			$table.="<td>".$row_array['CL_DIRECCI']     = utf8_encode($row[4])."</td>";
			$table.="<td>".$row_array['ESTADO']         = utf8_encode($row[5])."</td>";
			$table.="<td>".$row_array['LONGITUD']       = $row[6]."</td>";
			$table.="<td>".$row_array['LATITUD']        = $row[7]."</td>";
			$table.="<td>".$row_array['NRO_PEDIDO']     = utf8_encode($row[8])."</td>";
			$table.="<td>".$row_array['ANOMALIA']       = utf8_encode($row[9])."</td>";
			$table.="<td>".$row_array['RUTA']           = utf8_encode($row[10])."</td>";
		$table.="</tr>";
	}
	$table.="</table>";
	echo $table;
?>