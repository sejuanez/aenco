<?php
session_start();
if (!$_SESSION['user']) {
  echo
  "<script>window.location.href='../inicio/index.php';</script>";
  exit();
}
?>

<!DOCTYPE html5>
<html>
<head>
  <meta charset="utf-8">
  <title>Control vistas a clientes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>
  <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)'/>
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <style>
    .filtros{
      margin-left: 0rem;
      margin-right: 0rem;
      margin-top: 0rem;
    }
    .select select{
      border-radius: 0 !important;
      -webkit-border-radius: 0 !important;
      -moz-border-radius: 0 !important;
      -ms-border-radius: 0 !important;
      -o-border-radius: 0 !important;
    }
    .el-input__inner{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-table th {
      color: #FFFFFF !important;
      background-color: rgb(0, 3, 74) !important;
      font-weight: normal !important;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    .select:not(.is-multiple):not(.is-loading):after{
      border-color: #bfbcc6 !important;
    }
    .navbar-link:after, .select:not(.is-multiple):not(.is-loading):after{
      top: 41% !important;
    }
    .select select:focus{
      border-color: #4C9ED9 !important;
      outline: 0px;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
    .tg .tg-jl5r{font-weight:bold;background-color:#00034a;color:#ffffff;border-color:inherit;text-align:center}
    .tg .tg-5f41{background-color:#0008c5;color:#ffffff;border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top;text-align: center;}
    .tg .tg-dvpl{border-color:inherit;text-align:right;vertical-align:top}
    #subheader {
      padding: 50px 0px 0px 15px;
    }
  </style>
  <link rel="stylesheet" href="https://unpkg.com/buefy/lib/buefy.min.css">

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  <!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry"></script>
  <script type="text/javascript" src="js/gmaps.js"></script>-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
  <script type="text/javascript" src="../../js/gmaps.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/buefy"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>

<body>
  <div id="app">
    <header>
      <h3>Control vistas a clientes</h3>
      <nav>
        <ul id="menu">
          <li id="consultar">
            <a @click.stop.prevent="showDetalle" href="">
              <span class="ion-ios-search-strong"></span><h6>consultar</h6>
            </a>
          </li>
          <li id="excel" v-show="sw">
            <a id="exportar" href="">
              <span class="ion-ios-download-outline"></span><h6>exportar</h6>
            </a>
          </li>
        </ul>
      </nav>
    </header>
    
    <div id="subheader">
      <el-form size="mini">
        <el-row :gutter="4">
        <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
          <el-form-item label="Vendedor">
            <b-select v-model="codvende" placeholder="">
              <option
                v-for="item in optionsVendedor"
                :value="item.ve_cedula"
                :key="item.ve_cedula">
                {{ item.ve_nombres }}
              </option>
            </b-select>
          </el-form-item>
        </el-col>

        <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
          <el-form-item label="Fecha">
            <el-date-picker
              v-model="fecha"
              style="width: 100%"
              type="date"
              format="MM/dd/yyyy"
              value-format="MM/dd/yyyy">
            </el-date-picker>
          </el-form-item>
        </el-col>
      </el-row>
      </el-form>
    </div>

    <el-tabs type="card">
      <el-tab-pane label="Datos">
        <el-row >
          <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
            <el-table
              :data="tableData"
              border
              size="mini"
              style="width: 100%">
              <el-table-column
                prop="CODVENDE"
                label="Cod Vendedor"
                width="110">
              </el-table-column>
              <el-table-column
                prop="CLIENTE"
                label="Cod Cliente"
                width="100">
              </el-table-column>
              <el-table-column
                prop="CL_NOAPE"
                label="Cliente"
                width="200">
              </el-table-column>
              <el-table-column
                prop="CL_RAZONSOCIAL"
                label="Razón Social"
                width="200">
              </el-table-column>
              <el-table-column
                prop="CL_DIRECCI"
                label="Dirección"
                width="200">
              </el-table-column>
              <el-table-column
                prop="ESTADO"
                label="Estado"
                width="100">
              </el-table-column>
              <el-table-column
                prop="LONGITUD"
                label="Longitud"
                width="130">
              </el-table-column>
              <el-table-column
                prop="LATITUD"
                label="Latitud"
                width="130">
              </el-table-column>
              <el-table-column
                prop="NRO_PEDIDO"
                label="N° Periodo"
                width="100">
              </el-table-column>
              <el-table-column
                prop="ANOMALIA"
                label="Anomalia"
                width="130">
              </el-table-column>
              <el-table-column
                prop="RUTA"
                label="Ruta"
                width="130">
              </el-table-column>
            </el-table>
          </el-col>
        </el-row>
      </el-tab-pane>

      <el-tab-pane label="Mapa">
        <el-row :gutter="4">
          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
          <table class="tg" id="tblData">
            <thead>
              <tr>
                <th class="tg-jl5r" colspan="5">Resumen</th>
              </tr>
              <tr>
                <td class="tg-5f41">Conv.</td>
                <td class="tg-5f41" colspan="3">Estado</td>
                <td class="tg-5f41">Ver</td>
              </tr>
            </thead>
            <tbody id="datos">
              <tr>
                <th class="tg-0pky">Total</th>
                <th class="tg-dvpl" colspan="2" v-text="tableData.length">0</th>
                <th class="tg-0pky">Todas</th>
                <th class="tg-0pky">
                  <input type="checkbox" ref="ESTADOALL" :checked="selected" @click="selectAll">
                </th>
              </tr>
              <tr v-for="(items, index) in tableDataNew">
                <td class="tg-0pky"><img :src="getImgUrl(items.ESTADO)"  alt=""></td>
                <td class="tg-0pky dato" colspan="2" v-text="items.ESTADO"></td>
                <td class="tg-0pky" v-text="items.TOTAL"></td>
                <td class="tg-0pky">
                  <input type="checkbox" ref="ESTADO" @click="hideMakers(items.ESTADO, index)" :checked="selected">
                </td>
              </tr>
            </tbody>
          </table>
          </el-col>
          <el-col :xs="24" :sm="24" :md="18" :lg="18" :xl="18">
            <div id="map"></div>
          </el-col>
        </el-row>
      </el-tab-pane>
    </el-tabs>
  </div>
  <script>
    var map;
    var markers = [];
    var marker;
    var infowindow;
    ELEMENT.locale(ELEMENT.lang.es);
    Vue.use(Buefy.default)
    new Vue({
    el: '#app',
    data: () => ({
      fecha: '',
      codvende: '',
      optionsVendedor: [],
      tableData: [],
      tableDataNew: [],
      selected: false,
      sw: false,
      x: 0
    }),
    methods: {
      getImgUrl(pet) {
        return "img/"+ pet + ".jpg";
      },
      showVendedor () {
        axios.get('request/vendedor.php').then(response => {
          this.optionsVendedor = response.data;
        }).catch(e => {
          console.log(e.response);
        });
      },
      showDetalle () {
        if (this.codvende === '') {
          this.$alert('Diligenciar campo vendedor.', 'AVISO..!', {
            confirmButtonText: 'OK'
          })
        } else if (this.fecha === '') {
          this.$alert('Diligenciar campo fecha.', 'AVISO..!', {
            confirmButtonText: 'OK'
          })
        } else {
          var data = new FormData();
          data.append('fecha', this.fecha);
          data.append('codvende', this.codvende);
          axios.post('request/detalles.php', data).then(response => {
            // console.log(response.data[0].LATITUD);
            if (response.data.length !== 0) {
              var fecha = this.fecha;
              var codvende = this.codvende;
              this.tableData = response.data;

              var arrClaves = [];
              for (var i = 0; i < this.tableData.length; i++) {
                arrClaves.push(this.tableData[i].ESTADO);
              }
              var arrUnico = Array.from(new Set(arrClaves));         
              // console.log(arrUnico);
              
              var newTblData = [];
              arrUnico.forEach((element, index) => {
                if (typeof element !== 'undefined') {
                  var x = this.counts(element, this.tableData);
                  var data = {'ESTADO': element, 'TODAS': x};
                  newTblData.push(data);
                }
              });  

              var tempData = [];
              for (let i = 0; i < newTblData.length; i++) {
                var coor = [];
                for (var j = 0; j < this.tableData.length; j++) {
                  if (this.tableData[j].ESTADO === newTblData[i].ESTADO) {
                    coor.push(this.tableData[j]);
                  }
                }
                var temp = {'ESTADO': newTblData[i].ESTADO, 'TOTAL' : newTblData[i].TODAS, 'COOR' : coor };
                tempData.push(temp);
              }
            
              this.tableDataNew = tempData;
              this.tableDataNew.forEach(element => {
                element.COOR.forEach(element2 => {
                  if (element2.length !== 0) {
                    // Añadiendo los marcadores al mapa
                    if (this.x === 0) {
                      this.addMakers(element2);
                    } else {
                      this.x = 0;
                      this.deleteMarkers();
                      this.addMakers(element2);
                    }
                    // 
                  }
                });
              });
              this.selected = true;
              this.sw = true;
              // this.exportar(fecha, codvende);
              $('#exportar').attr('href', 'request/exportarExcel.php?fecha='+fecha+'&codvende='+codvende+'');
              // this.allSelected = true;
            } else {
              this.tableData = [];
              this.tableDataNew = [];
              this.sw = false;
              this.selected = false;
              this.deleteMarkers();
            }
            this.x = 1;
          }).catch(e => {
            console.log(e.response);
          });
        }
      },
      counts (element, array) {
			  var c = 0;
			  for (var i = 0; i < array.length; i++) {
				  if (array[i].ESTADO == element) {
				    c++;
				  }
			  }
			  return c;
      },
      selectAll () {
        if (this.$refs.ESTADOALL.checked === false) {
          for (var i = 0; i < markers.length; i++) {          
            markers[i].setMap(null);              
          }
          for (var j = 0; j < this.tableDataNew.length; j++) {
            this.$refs.ESTADO[j].checked = false
          }
        } else {
          for (var i = 0; i < markers.length; i++) {
           markers[i].setMap(map);
          }
          for (var j = 0; j < this.tableDataNew.length; j++) {
            this.$refs.ESTADO[j].checked = true
          }
        }        
      },
      select () {
        this.allSelected = false;
      },
      initMap () {
        // var myLatLng = new google.maps.LatLng(8.7483365, -75.8771448);
        map = new google.maps.Map(document.getElementById('map'), {
          center: {
            lat: 8.7660343, lng: -75.8688076
          },
          zoom: 10
        });
        // var marker = new google.maps.Marker({position: myLatLng, map: map});
      },
      addMakers (element) {
        var anomalia = element.ANOMALIA == null ? "NINGUNA" : element.ANOMALIA;
        // markers = [];
        var myLatLng = new google.maps.LatLng(element.LATITUD, element.LONGITUD);
        marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          icon: 'img/'+element.ESTADO+'.jpg'
        });
        
        this.clickMarker(element.LATITUD, element.LONGITUD);
        var contentString = '<div style="font-weight: bold;">'+element.CL_RAZONSOCIAL+'</div><div>'+element.CL_NOAPE+'</div><div>'+element.CL_DIRECCI+'</div><div>N° Pedido: '+element.NRO_PEDIDO+'</div> <div>Anomalia: : '+anomalia+'</div>';

        infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.setContent(contentString);
          infowindow.open(map, this);
          // infowindow.open(map, marker);
        });
        markers.push(marker);
      },
      hideMakers (state, index) {
        // markers[0].setMap(null);
        // markers[1].setMap(null);
        // markers[2].setMap(null);
        // markers[3].setMap(null);
        if(this.$refs.ESTADO[index].checked === false){

          for (var i = 0; i < markers.length; i++) {
            var str = markers[i].getIcon().split('/');
            var estado = str[1].split(".")[0];
            if(estado === state){
              markers[i].setMap(null);
            }
          }
        } else {

          for (var i = 0; i < markers.length; i++) {          
            var str = markers[i].getIcon().split('/');
            var estado = str[1].split(".")[0];
            if(estado === state){
              markers[i].setMap(map);
            }
          }
        }
        // console.log(this.$refs.ESTADO[index].checked);
      },
      diaSemana (fecha) {
        fecha = fecha.split('/');
        if (fecha.length != 3) {
          return null;
        } 
        // Vector para calcular día de la semana de un año regular.
        var regular = [0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5]; 
        // Vector para calcular día de la semana de un año bisiesto.
        var bisiesto = [0, 3, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6]; 
        // Vector para hacer la traducción de resultado en día de la semana.
        var semana = ['LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO', 'DOMINGO'];
        // Día especificado en la fecha recibida por parametro.
        var dia = fecha[0];
        // Módulo acumulado del mes especificado en la fecha recibida por parametro.
        var mes = fecha[1] - 1;
        // Año especificado por la fecha recibida por parametros.
        var anno = fecha[2];
        // Comparación para saber si el año recibido es bisiesto.
        if ((anno % 4 == 0) && !(anno % 100 == 0 && anno % 400 != 0)) {
          mes = bisiesto[mes];
        } else {
          mes = regular[mes];
        }
        // Se retorna el resultado del calculo del día de la semana.
        return semana[Math.ceil(Math.ceil(Math.ceil((anno - 1) % 7) + Math.ceil((Math.floor((anno - 1) / 4) - Math.floor((3 * (Math.floor((anno - 1) / 100) + 1)) / 4)) % 7) + mes + dia % 7) % 7)];
	    },
      clickMarker (lat, lon) {
        var coor = new google.maps.LatLng(lat, lon);
        map.setZoom(14);
        map.setCenter(marker.getPosition());
        // map.setCenter(coor);
      },
      setMapOnAll (map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      },
      clearMarkers () {
        this.setMapOnAll(null);
      },
      deleteMarkers() {
        this.clearMarkers();
        markers = [];
      }
    },
    mounted () {
      this.initMap();
      this.showVendedor();
      var f = new Date();
      this.fecha = (f.getMonth() +1) + '/' + f.getDate() + '/' + f.getFullYear();
    }
  })
  </script>
</body>
</html>