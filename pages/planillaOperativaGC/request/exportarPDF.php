<?php
setlocale(LC_ALL, "es_ES");
include('../../../init/gestion.php');
require('fpdf/fpdf.php');



$dias = array("domingo", "lunes", "martes", "mi&eacute;rcoles", "jueves", "viernes", "s&aacute;bado");

$fechap = $_GET['fechap'];
$gestor = $_GET['gestor'];
$param = "";

if (!empty($gestor)) {
    $param = "AND g.tecnico = '$gestor' ";
}
$query = "SELECT g.id ORDEN, g.nic NIC, g.corregimiento AS CORREGIMIENTO, g.municipio AS MUNICIPIO,
    g.barrio BARRIO, g.cliente AS CLIENTE, g.aol AS AOL,
    g.TIPO_VIA||' '||g.NOM_CALLE||' '||g.DUPLICADOR||' '|| g.NRO_PUERTA||' '||g.CGV||' '||g.REF_DIR||' '||g.BARRIO AS DIRECCION,
    g.tarifa AS TARIFA, g.estado_sum AS ESTADOSUM, g.medidor AS MEDIDOR, COALESCE(g.DEUDA_TERCEROS + g.DEUDA_ENERGIA, 0) AS DEUDA_TOTAL,
    g.importe_acordado AS IMPORTE_ACO, g.deuda_energia AS DEUDA_ENERGIA, g.fact_venc AS FV,
    g.fact_aco AS FA, g.fecha_programacion, t.te_nombres AS GESTOR, ITIN
    FROM gestion_cobro_entrega g
    LEFT JOIN tecnicos t ON t.te_codigo = g.tecnico
    WHERE g.fecha_programacion = '$fechap' $param AND g.acta IS NULL ORDER BY g.tecnico";
$data = ibase_query($conexion, $query);


class PDF extends FPDF {
    var $widths;
    var $aligns;

    private $dato;

    public function setDato($dato){
        $this->dato = $dato;
    }

    function Header(){
        $this->SetFont('helvetica', '', 9);
    }

    function Header_($addpage) {
       if($addpage){
            $this->AddPage();
       }
        $this->SetFont('helvetica', '', 9);
       // $this->SetXY(10, 15);
        $this->Cell(150, 5, utf8_decode('FECHA PROGRAMACION: '. strftime('%A') . ',' . date('d') . ' de ' . strftime('%B') . ' de ' . date('Y')), 0, 0, 'L');
       

        $this->SetFont('helvetica', '', 9);
       // $this->SetXY(110, 15);
        $this->Cell(200, 5, utf8_decode('AGENTE PROGRAMADO: '. $this->dato[17]), 0, 1, 'L');
        $this->Cell(40, 10, '', 0, 0, 'C');




      //  $this->Cell(10, 10, '', 1, 0, 'C');

        $this->SetFont('helvetica', 'B', 9);
        //$this->SetXY(50, 20);
        $this->Cell(150, 5, utf8_decode($this->dato[3]), 0, 0, 'L');
        $this->Cell(0, 10, '', 0, 1, 'C');

      
        $this->SetFont('helvetica', '', 8);
        $this->SetWidths(array(16, 16, 20, 20, 20, 10, 6, 19, 18, 18, 16, 6, 6, 22, 22, 6, 15, 10, 15, 10, 9, 20, 20));

        $titles = array(
            'ORDEN', 
            'NIC', 
            'CORREG', 
            'CLIENTE AOL',
            'DIRECCION',
            'TAR',
            'ES',
            'MEDIDOR',
            'DEUDA TOTAL',
            'IMPORTE ACORDA',
            'DEUDA ENERGI',
            'F.V',
            'F.A',
            'CEDULA',
            'CONTACTO',
            'TG',
            'TELEFONO',
            'COD',
            'RECAUDO',
            'P/A',
            'HORA',
            'OBSERVACIO',
            'LECTURA/FIRMA'
        );

        $this->Row($titles);

    }

    function saber_dia($nombredia) {
        list($m, $d, $Y) = explode("/", $nombredia);
        $dias = array('', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO', 'DOMINGO');
        $fecha = $dias[date('N', strtotime($nombredia))];
        return $fecha;
    }

    function SetWidths($w) {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a) {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    function Row($data) {
        //Calculate the height of the row
        $nb = 0;
        for($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            $this->Rect($x, $y, $w, $h);
            //Print the text
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h) {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY() + $h>$this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }
    }

    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw =&$this->CurrentFont['cw'];
        if($w == 0)
            $w = $this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}

$pdf = new PDF('Landscape', 'mm', 'Legal');
// $pdf->AliasNbPages();
//$pdf->SetFont('helvetica', '', 9);
 $pdf->AddPage();
 $ante = '';
 $desp = '';
 $a = 0;
while ($row = ibase_fetch_row($data)) {
    $desp = $row[17];
    $pdf->setDato($row);
    if($a == 0){
        $pdf->Header_(false);
        $ante = $row[17];
    }
    $a++;
    if(!($ante == $desp)){
      $pdf->Header_(true);
       $ante = $row[17];
    }
    
    $pdf->Row(array(
       utf8_decode($row[0]),
       utf8_decode($row[1]),
       utf8_decode($row[2]),
       utf8_decode($row[5] . ' ' . $row[6]),
       utf8_decode($row[7]),
       utf8_decode($row[8]),
       utf8_decode(''),
       utf8_decode($row[10]),
       utf8_decode($row[11]),
       utf8_decode($row[12]),
       utf8_decode($row[13]),
       utf8_decode($row[14]),
       utf8_decode($row[15]),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode(''),
       utf8_decode('')
    ));
}

$pdf->Output('D');
