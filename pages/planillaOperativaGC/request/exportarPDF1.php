<?php
setlocale(LC_ALL,"es_ES");
include('../../../init/gestion.php');
require('Muticell.php');


$pdf = new PDF_MC_Table('Landscape', 'mm', 'Legal');
$pdf->AddPage('Landscape','Legal');
$pdf->SetFont('helvetica', '', 8);

$dias = array("domingo","lunes","martes","mi&eacute;rcoles","jueves","viernes","s&aacute;bado");

$fechap = $_GET['fechap'];
$gestor = $_GET['gestor'];
$param = "";

if (!empty($gestor)) {
    $param = "AND g.tecnico = '$gestor' ";
}
$query = "SELECT g.id ORDEN, g.nic NIC, g.corregimiento AS CORREGIMIENTO, g.municipio AS MUNICIPIO,
    g.barrio BARRIO, g.cliente AS CLIENTE, g.aol AS AOL,
    g.TIPO_VIA||' '||g.NOM_CALLE||' '||g.DUPLICADOR||' '|| g.NRO_PUERTA||' '||g.CGV||' '||g.REF_DIR||' '||g.BARRIO AS DIRECCION,
    g.tarifa AS TARIFA, g.estado_sum AS ESTADOSUM, g.medidor AS MEDIDOR, COALESCE(g.DEUDA_TERCEROS + g.DEUDA_ENERGIA, 0) AS DEUDA_TOTAL,
    g.importe_acordado AS IMPORTE_ACO, g.deuda_energia AS DEUDA_ENERGIA, g.fact_venc AS FV,
    g.fact_aco AS FA, g.fecha_programacion, t.te_nombres AS GESTOR, ITIN
    FROM gestion_cobro_entrega g
    LEFT JOIN tecnicos t ON t.te_codigo = g.tecnico
    WHERE g.fecha_programacion = '$fechap' $param AND g.acta IS NULL ORDER BY g.tecnico";
$data = ibase_query($conexion, $query);


$pdf->SetXY(10, 15);
$pdf->Cell(150, 5, utf8_decode('FECHA PROGRAMACION: '.strftime('%A').','.date('d').' de '.strftime('%B').' de '.date('Y')), 0, 1, 'L');
$pdf->Cell(0, 10, '', 0, 1, 'C');


$return_arr = array();
$agnte;
while ($row = ibase_fetch_row($data)) {
    $return_arr[] = $row;
    $agnte = $row[17];
}

$pdf->SetXY(100, 15);
$pdf->Cell(188, 5, utf8_decode('AGENTE PROGRAMADO: '.$agnte), 0, 1, 'C');
$pdf->Cell(0, 10, '', 0, 1, 'C');
$pdf->SetWidths(array(16, 16, 20, 20, 20, 10, 6, 19, 18, 18, 16, 6, 6, 22, 22, 6, 15, 10, 15, 10, 9, 20, 20));

$titles = array(
    'ORDEN', 
    'NIC', 
    'CORREG', 
    'CLIENTE AOL',
    'DIRECCION',
    'TAR',
    'ES',
    'MEDIDOR',
    'DEUDA TOTAL',
    'IMPORTE ACORDA',
    'DEUDA ENERGI',
    'F.V',
    'F.A',
    'CEDULA',
    'CONTACTO',
    'TG',
    'TELEFONO',
    'COD',
    'RECAUDO',
    'P/A',
    'HORA',
    'OBSERVACIO',
    'LECTURA/FIRMA'
);
$pdf->SetFont('helvetica', '', 6);
$pdf->Row($titles);

foreach ($return_arr as $items) {
    $pdf->Row(array(
        utf8_decode($items[0]),
        utf8_decode($items[1]),
        utf8_decode($items[2]),
        utf8_decode($items[5].' '.$items[6]),
        utf8_decode($items[7]),
        utf8_decode($items[8]),
        utf8_decode(''),
        utf8_decode($items[10]),
        utf8_decode($items[11]),
        utf8_decode($items[12]),
        utf8_decode($items[13]),
        utf8_decode($items[14]),
        utf8_decode($items[15]),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode(''),
        utf8_decode('')
    ));
}


$pdf->Output('D');
