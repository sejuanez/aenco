<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $fechap = $_POST['fechap'];
    $gestor = $_POST['gestor'];

	$param = "";

	if (!empty($gestor)) {
		$param = "AND g.tecnico = '$gestor' ";
	}

    $query = "SELECT g.id ORDEN, g.nic NIC, g.corregimiento AS CORREGIMIENTO, g.municipio AS MUNICIPIO,
    g.barrio BARRIO, g.cliente AS CLIENTE, g.aol AS AOL,
    g.TIPO_VIA||' '||g.NOM_CALLE||' '||g.DUPLICADOR||' '|| g.NRO_PUERTA||' '||g.CGV||' '||g.REF_DIR||' '||g.BARRIO AS DIRECCION,
    g.tarifa AS TARIFA, g.estado_sum AS ESTADOSUM, g.medidor AS MEDIDOR, g.DEUDA_TERCEROS + g.DEUDA_ENERGIA AS DEUDA_TOTAL,
    g.importe_acordado AS IMPORTE_ACO, g.deuda_energia AS DEUDA_ENERGIA, g.fact_venc AS FV,
    g.fact_aco AS FA, g.fecha_programacion, t.te_nombres AS GESTOR, ITIN
    FROM gestion_cobro_entrega g
    LEFT JOIN tecnicos t ON t.te_codigo = g.tecnico
    WHERE g.fecha_programacion = '$fechap' $param AND g.acta IS NULL ORDER BY g.tecnico";
    $data = ibase_query($conexion, $query);

    $return_arr = array();
    // $return_arr2 = array();
    while ($row = ibase_fetch_row($data)) {
        $row_array['orden']              = utf8_encode($row[0]);
        $row_array['nic']                = utf8_encode($row[1]);
        $row_array['corregimiento']      = utf8_encode($row[2]);
        $row_array['municipio']          = utf8_encode($row[3]);
        $row_array['barrio']             = utf8_encode($row[4]);
        $row_array['cliente']            = utf8_encode($row[5]);
        $row_array['aol']                = utf8_encode($row[6]);
        $row_array['direccion']          = utf8_encode($row[7]);
        $row_array['tarifa']             = utf8_encode($row[8]);
        $row_array['estado_sum']         = utf8_encode($row[9]);
        $row_array['medidor']            = utf8_encode($row[10]);
        $row_array['deuda_total']        = utf8_encode($row[11]);
        $row_array['importe_aco']        = utf8_encode($row[12]);
        $row_array['deuda_energia']      = utf8_encode($row[13]);
        $row_array['fv']                 = utf8_encode($row[14]);
        $row_array['fa']                 = utf8_encode($row[15]);
        $row_array['fecha_programacion'] = utf8_encode($row[16]);
        $row_array['gestor']             = utf8_encode($row[17]);
        $row_array['itin']               = utf8_encode($row[18]);

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>