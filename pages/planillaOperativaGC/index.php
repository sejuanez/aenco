<?php
  session_start();
  if (!$_SESSION['user']) {
    echo "<script>window.location.href='../inicio/index.php';</script>";
    exit();
  }
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Legalización</title>
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    *{
      margin: 0;
      padding: 0;
    }
    .el-header{
      background-color: #EDEDED;
      border-bottom: 1px solid #DEDEDE;
      color: #333;
      /*text-align: center;*/
      /*line-height: 60px;*/
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .buscar {
      padding-left: 0px !important;
    }
    .btn-excel{
      text-decoration: none;
      height: 29px;
      margin-left: -1.5rem;
      margin-top: 1.75rem;
      font-size: 12px;
      /*padding: 10px 2px 1px;
      font-weight: 600;
      
      color: #000000;
      background-color: #FFFFFF;
      border-radius: 3px;
      padding: 0px 21px 11px 17px;
      border: 0px solid #ccc;
      margin-left: 1.5rem;*/
    }
    #header {
      background-color: #ededed;
      border-bottom: 1px solid #dedede;
      padding: 4px 10px;
      margin-bottom: 2px;
      -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 2;
    }
    #totales {
      border-collapse: collapse;
      font-family: "Segoe UI", verdana,Arial,sans-serif;
      color: #5f5f5f;
      text-shadow: 0px 1px #ffffff;
      font-size: 80%;
    }
    #totales td {
      padding: 0 8px;
      border-right: 1px solid #d3d3d3;
      text-align: center;
    }
    .el-table__footer-wrapper tbody td, .el-table__header-wrapper tbody td{
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    #modal {
      padding-top: 150px;
      width: 100%;
      height: 100%;
      position: fixed;
      right: 0%;
      background-color: #e7e7e7;
      z-index: 1;
    }
    #modal p {
      color: #5f5f5f;
      text-shadow: 0 1px #ffffff;
      font-size: 150%;
      text-align: center;
      font-family: verdana,Arial,sans-serif;
    }
    @media only screen and (max-width: 768px) {
      .btn-mobile{
        margin-left: 93px !important;
      }
      .el-date-editor.el-input, .el-date-editor.el-input__inner {
        width: 141px;
      }
    }
  </style>
  <script src="js/jquery/jquery-3.2.1.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<body>
  <div id="app">
    <el-container>
      <div id="header">
        <el-form size="mini">
          <el-row :gutter="4">
            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Fecha programación">
                <el-date-picker
                  style="width: 100%"
                  v-model="fechap"
                  type="date"
                  format="MM/dd/yyyy"
                  value-format="MM/dd/yyyy">
                </el-date-picker>
              </el-form-item>
            </el-col>

            <el-col :xs="4" :sm="4" :md="5" :lg="5" :xl="5">
              <el-form-item label="Gestor">
                <el-select v-model="gestor" filterable clearable placeholder="(TODOS)" style="width: 100%">
                  <el-option
                    v-for="item in optionsTecnico"
                    :key="item.te_codigo"
                    :label="item.te_nombres"
                    :value="item.te_codigo">
                  </el-option>
                </el-select>
              </el-form-item>
            </el-col>

            <el-col :xs="6" :sm="24" :md="3" :lg="3" :xl="3">
              <el-form-item>
                <el-button @click="onQuery" icon="el-icon-search" :loading="loadingTable" style="margin-top: 1.75rem;">Consultar</el-button>
              </el-form-item>
            </el-col>

            <el-col :xs="6" :sm="24" :md="1" :lg="1" :xl="1" v-show="sw">
              <!-- <el-form-item>
                <el-button @click="exportar" icon="el-icon-search" :loading="loadingTable" style="margin-top: 1.75rem;">Exportar</el-button>
              </el-form-item> -->
              <a id="exportar" href="" class="el-button btn-excel">
                <span><p style="margin-top: -.3rem;">Exportar</p></span>
              </a>
            </el-col>
          </el-row>
        </el-form>
      </div>
      
      <el-main style="margin-top: 0rem;">
        <el-row>
          <div id="modal">
            <p>Escoge una fechas de programación y seleccione un gestor o todos</p>
            <p>y pulsa consultar...</p>
          </div>
        </el-row>
      </el-main>
    </el-container>
  </div>
  <script>
    ELEMENT.locale(ELEMENT.lang.es);
    new Vue({
      el: '#app',
      data: () => ({
        fechap: '',
        gestor: '',
        tableData: [],
        optionsTecnico: [],
        loadingTable: false,
        sw: false
      }),
      methods: {
        onQuery () {
          this.loadingTable = true
          var data = new FormData();
          data.append('fechap', this.fechap);
          data.append('gestor', this.gestor);
          axios.post('request/getGestionCobroEntrega.php', data).then(response => {
            this.tableData = response.data;
            console.log(response.data);
            if (response.data.length !== 0) {
              this.sw = true;
              $('#exportar').attr('href', 'request/exportarPDF.php?fechap='+this.fechap+'&gestor='+this.gestor+'');
            } else {
              this.sw = false;
            }
            this.loadingTable = false
          }).catch(e => {
            console.log(e.response);
            this.loadingTable = false
          });
        },
        exportar () {
          var data = new FormData();
          data.append('fechap', this.fechap);
          // data.append('delegacion', this.delegacion);
          axios.post('request/exportarPDF.php', data).then(response => {
            
            console.log(response.data);
            if (response.data.length !== 0) {
              this.sw = true;
              var fecha = this.fecha;
              $('#exportar').attr('href', 'request/exportarExcel.php?fecha='+fecha+'&delegacion='+this.delegacion+'');
            } else {
              this.sw = false;
            }
            this.loadingTable = false
          }).catch(e => {
            console.log(e.response);
            this.loadingTable = false
          });
        },
        showTecnicos () {
          axios.get('request/tecnicos.php').then(response => {
            this.optionsTecnico = response.data;
          }).catch(error => {
            console.log(error.response);
          })
        }
      },
      mounted () {
        this.showTecnicos()
        var f = new Date();
        this.fechap = (f.getMonth() +1) + '/' + f.getDate() + '/' + f.getFullYear();
      }
    })
  </script>
</body>
</html>