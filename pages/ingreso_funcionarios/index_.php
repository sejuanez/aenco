<?php
  session_start();

  if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {
    echo"<script>window.location.href='../inicio/index_.php';</script>";
    exit();
  }
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <title>Ingreso funcionarios</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>
  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)'/> -->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css'/>
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <style>
    .el-form-item__label{
      text-align: left;
    }
    .el-form-item--mini .el-form-item__content, .el-form-item--mini .el-form-item__label{
      line-height: 1em;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .window-dialog, .el-dialog{
      width: 80%;
    }
    table{
      margin-top: 0px;
    }
    .el-table{
      color: #000000;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .95em;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    #subheader{
      padding: 17px 10px 10px 10px;
      background-color:#FFF;
    }
    .avatar-uploader .el-upload {
      border: 1px dashed #d9d9d9;
      border-radius: 6px;
      cursor: pointer;
      position: relative;
      overflow: hidden;
    }
    .avatar-uploader .el-upload:hover {
      border-color: #409EFF;
    }
    .avatar-uploader-icon {
      font-size: 28px;
      color: #8c939d;
      width: 178px;
      height: 178px;
      line-height: 178px;
      text-align: center;
    }
    .avatar {
      width: 178px;
      height: 178px;
      display: block;
    }
    #div_2 {
      margin-bottom: 0px;
    }
  </style>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>  

</head>
<body>
  <div id="app">
    <el-form :model="ruleForm" :rules="rules" ref="ruleForm" label-position="top" size="mini">
      <header>
        <h3>Ingreso funcionarios</h3>
        <nav>
          <ul id="menu">
            <li id="consultar">
              <a @click.stop.prevent="addFuncionario" href="">
                <span class="ion-android-done"></span><h6>Guardar</h6>
              </a>
            </li>

            <li>
              <a @click.stop.prevent="onUpdate" href="">
                <span class="ion-android-done"></span><h6>Actualizar</h6>
              </a>
            </li>
            <li id="consultando">
<!--              <el-form-item>-->
<!--                <el-button icon="el-icon-search"></el-button>-->
<!--              </el-form-item>-->
              <a @click.stop.prevent="dialogBuscar = true" href="">
                <span class="ion-load-d"></span><h6>Buscar</h6>
              </a>
            </li>
          </ul>
        </nav>
      </header>

      <div id="subheader">
      </div>

      <div id="contenido">
        <div id="div_2" style="padding: 15px;">
          <!-- <div class="wrapper"> -->
            <el-row :gutter="4">
              <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                <legend>Datos personales</legend>
                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Cedula">
                      <el-input v-model="ruleForm.rh_cedula"></el-input>
                    </el-form-item>
                  </el-col>

<!--                  <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">-->
<!--                    <el-form-item>-->
<!--                      <el-button icon="el-icon-search" style="margin-top: 1.45rem;"></el-button>-->
<!--                    </el-form-item>-->
<!--                  </el-col>-->

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Expedida en">
                      <el-select style="width: 100%" v-model="ruleForm.rh_codexpciud" clearable placeholder="Seleccione ciudad">
                        <el-option
                          v-for="item in optionsCiudades"
                          :key="item.mu_codigomun"
                          :label="item.mu_nombre"
                          :value="item.mu_codigomun">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Primer apellido">
                      <el-input v-model="ruleForm.rh_apellido1"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Segundo apellido">
                      <el-input v-model="ruleForm.rh_apellido2"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Primer nombre">
                      <el-input v-model="ruleForm.rh_nombre1"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Segundo nombre">
                      <el-input v-model="ruleForm.rh_nombre2"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Dirección">
                      <el-input v-model="ruleForm.rh_direccion"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Ciudad">
                      <el-select v-model="ruleForm.rh_codciudad" @change="showBarrios" clearable placeholder="Seleccione ciudad" style="width: 100%">
                        <el-option
                          v-for="item in optionsCiudades"
                          :key="item.mu_codigomun"
                          :label="item.mu_nombre"
                          :value="item.mu_codigomun">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Barrio">
                      <el-select v-model="ruleForm.rh_codbarrio" clearable placeholder="Seleccio barrio" style="width: 100%">
                        <el-option
                          v-for="item in optionsBarrio"
                          :key="item.cod"
                          :label="item.ba_nombarrio"
                          :value="item.ba_codbarrio">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Teléfono">
                      <el-input v-model="ruleForm.rh_telefono"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Celular">
                      <el-input v-model="ruleForm.rh_movil"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Sexo">
                      <el-select v-model="ruleForm.rh_sexo" clearable placeholder="Seleccione sexo" style="width: 100%">
                        <el-option
                          v-for="item in optionsGenero"
                          :key="item.value"
                          :label="item.label"
                          :value="item.value">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Estado civil">
                      <el-select v-model="ruleForm.rh_estcivil" clearable placeholder="Seleccione estado civil" style="width: 100%">
                        <el-option
                          v-for="item in optionsEstadoCivil"
                          :key="item.value"
                          :label="item.label"
                          :value="item.value">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-form-item label="Factor sangre">
                      <el-select v-model="ruleForm.rh_rh" clearable placeholder="Select" style="width: 100%">
                        <el-option
                          v-for="item in optionsTipoSanguineo"
                          :key="item.value"
                          :label="item.fac_sangre1"
                          :value="item.value">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>
                  
                  <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                    <el-upload
                      class="filesFormulario button-upload"
                      ref="upload"
                      action=""
                      accept=".jpg, .png"
                      :on-preview="handlePreview"
                      :auto-upload="false"
                      :on-progress="handleChange"
                      :http-request="handleChange"
                      :on-change="handleChange"
                      :on-remove="handleChange"
                      :file-list="fileList">
                      <el-button slot="trigger" size="mini">Clic para subir archivo</el-button>
                      <div slot="tip" class="el-upload__tip"></div>
                    </el-upload>
                  </el-col>

                  <!-- DATOS DE NACIMIENTO -->
                  <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                    <fieldset style="padding: 1rem;border: 1px solid silver;">
                      <legend>Datos nacimiento</legend>
                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Fecha de nacimiento">
                          <el-date-picker
                            v-model="ruleForm.rh_fechanac"
                            style="width: 100%"
                            type="date"
                            format="MM/dd/yyyy"
                            value-format="MM/dd/yyyy"
                            placeholder="Seleccione fecha">
                          </el-date-picker>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                        <el-form-item label="Edad">
                          <el-input v-model="ruleForm.rh_edad" readOnly></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Departamento">
                          <el-select @change="showMunicipios" v-model="ruleForm.rh_coddptonac" clearable placeholder="Seleccione departamento" style="width: 100%">
                            <el-option
                              v-for="item in optionsDpto"
                              :key="item.de_codigo"
                              :label="item.de_nombre"
                              :value="item.de_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                        <el-form-item label="Ciudad">
                          <el-select style="width: 100%" v-model="ruleForm.rh_codciunac" clearable placeholder="Select">
                            <el-option
                              v-for="item in optionsMunicipio"
                              :key="item.mu_codigomun"
                              :label="item.mu_nombre"
                              :value="item.mu_codigomun">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                        <el-form-item label="Nacionalidad">
                          <el-select style="width: 100%" v-model="ruleForm.rh_codnacional" clearable placeholder="Select">
                            <el-option
                              v-for="item in optionsNacionalidades"
                              :key="item.na_codigo"
                              :label="item.na_nacional"
                              :value="item.na_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>
                    </fieldset>
                  </el-col>

                
              </fieldset>
            </el-row>
          <!-- </div> -->
        </div>

        <div id="div_3">
          <div class="wrapper" style="padding: 15px;">
            <el-tabs type="card">
              <el-tab-pane label="1. Datos Legales">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                        <!-- <el-form label-position="top" size="mini"> -->
                          <el-row :gutter="4">
                            
                            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                              <el-form-item label="Cent. Trabajo">
                                <el-select v-model="ruleForm.dl_codcentrab" clearable placeholder="Seleccione cent. trabajo" style="width: 100%">
                                  <el-option
                                    v-for="item in optionscentroTrabajo"
                                    :key="item.ct_codigo"
                                    :label="item.ct_centrabajo"
                                    :value="item.ct_codigo">
                                  </el-option>
                                </el-select>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                              <el-form-item label="Cent. Costo">
                                <el-select v-model="ruleForm.dl_codcencosto" style="width: 100%" clearable placeholder="Seleccione cent. costo">
                                  <el-option
                                    v-for="item in optionscentroCosto"
                                    :key="item.cc_codigo"
                                    :label="item.cc_cencosto"
                                    :value="item.cc_codigo">
                                  </el-option>
                                </el-select>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                              <el-form-item label="Salario">
                                <el-input v-model="ruleForm.dl_salario"></el-input>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                              <el-form-item label="Horas Diarias">
                                <el-input v-model="ruleForm.dl_hosrasdiarias"></el-input>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                              <el-form-item label="Comp. Horas">
                                <el-input></el-input>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                              <el-form-item label="Salario en Letras">
                                <el-input type="textarea"></el-input>
                              </el-form-item>
                            </el-col>
                          </el-row>

                        <!-- </el-form> -->
                      </fieldset>
                    </el-col>
                  
                    <el-col ::xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                        <legend>Libreta Militar</legend>
                        <el-form label-position="top" size="mini">
                          
                          <el-row :gutter="4">
                            <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                              <el-form-item label="Número">
                                <el-input v-model="ruleForm.dl_nlibreta"></el-input>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                              <el-form-item label="Clase">
                                <el-select v-model="ruleForm.dl_codclaselib" style="width: 100%" clearable placeholder="Seleccione clase">
                                  <el-option
                                    v-for="item in optionsClaseLibreta"
                                    :key="item.value"
                                    :label="item.label"
                                    :value="item.value">
                                  </el-option>
                                </el-select>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                              <el-form-item label="Distrito">
                                <el-input v-model="ruleForm.dl_distritomil"></el-input>
                              </el-form-item>
                            </el-col>
                          </el-row>

                        </el-form>
                      </fieldset>

                      <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                        <legend>Certificado Judicial</legend>
                        <el-form label-position="top" size="mini">
                          <el-row :gutter="4">
                            
                            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                              <el-form-item label="Número">
                                <el-input v-model="ruleForm.dl_certjudicial"></el-input>
                              </el-form-item>
                            </el-col>

                            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                              <el-form-item label="Fecha">
                                <el-date-picker
                                  v-model="ruleForm.dl_fechaven"
                                  style="width: 100%"
                                  type="date"
                                  format="MM/dd/yyyy"
                                  value-format="MM/dd/yyyy"
                                  placeholder="Seleccione fecha">
                                </el-date-picker>
                              </el-form-item>
                            </el-col>

                          </el-row>
                        </el-form>
                      </fieldset>
                    </el-col>
                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="2. Datos Parafiscales">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <fieldset style="padding: 1rem;border: 1px solid silver;">
                      <legend>Seguridad social</legend>
                      <el-form label-position="top" size="mini">
                        
                        <el-row :gutter="4">
                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="ARP">
                              <el-select style="width: 100%" v-model="ruleForm.dp_codarp" clearable placeholder="Seleccione ARP">
                                <el-option
                                  v-for="item in optionsArp"
                                  :key="item.ss_codigo"
                                  :label="item.ss_nombre"
                                  :value="item.ss_codigo">
                                </el-option>
                              </el-select>
                            </el-form-item>
                          </el-col>

                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="EPS">
                              <el-select style="width: 100%" v-model="ruleForm.dp_codeps" clearable placeholder="Seleccione EPS">
                                <el-option
                                  v-for="item in optionsEps"
                                  :key="item.ss_codigo"
                                  :label="item.ss_nombre"
                                  :value="item.ss_codigo">
                                </el-option>
                              </el-select>
                            </el-form-item>
                          </el-col>

                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="AFP">
                              <el-select style="width: 100%" v-model="ruleForm.dp_afp" clearable placeholder="Seleccione AFP">
                                <el-option
                                  v-for="item in optionsAfp"
                                  :key="item.ss_codigo"
                                  :label="item.ss_nombre"
                                  :value="item.ss_codigo">
                                </el-option>
                              </el-select>
                            </el-form-item>
                          </el-col>

                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="Caja">
                              <el-select style="width: 100%" v-model="ruleForm.dp_codcaja" clearable placeholder="Seleccione caja">
                                <el-option
                                  v-for="item in optionsCaja"
                                  :key="item.ss_codigo"
                                  :label="item.ss_nombre"
                                  :value="item.ss_codigo">
                                </el-option>
                              </el-select>
                            </el-form-item>
                          </el-col>

                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="% Adicional ARP">
                              <el-input v-model="ruleForm.dp_adicionarp"></el-input>
                            </el-form-item>
                          </el-col>

                        </el-row>
                      </el-form>
                    </fieldset>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <fieldset style="padding: 1rem;border: 1px solid silver;margin-left: .8rem;">
                      <legend>Cuenta Bancaria</legend>
                      <el-form label-position="top" size="mini">
                        <el-row :gutter="4">
                          
                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="Banco">
                              <el-select style="width: 100%" v-model="ruleForm.dp_codbanco" clearable placeholder="Seleccione banco">
                                <el-option
                                  v-for="item in optionsBanco"
                                  :key="item.ba_codigo"
                                  :label="item.ba_nombre"
                                  :value="item.ba_codigo">
                                </el-option>
                              </el-select>
                            </el-form-item>
                          </el-col>

                          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                            <el-form-item label="N° Cuenta">
                              <el-input v-model="ruleForm.dp_ncuenta"></el-input>
                            </el-form-item>
                          </el-col>

                        </el-row>
                      </el-form>
                    </fieldset>
                  </el-col>
                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="3. Datos Empresa">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                  <el-form label-position="top" size="mini">
                    <el-row :gutter="4">
                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Jefe inmediato">
                          <el-select v-model="ruleForm.em_codingeniero" clearable placeholder="Seleccione jefe inmedianto" style="width: 100%">
                            <el-option
                              v-for="item in optionsJefeInmediato"
                              :key="item.rh_cedula"
                              :label="item.full_name"
                              :value="item.rh_cedula">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Programa">
                          <el-select placeholder="ss" style="width: 100%">
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="SubPrograma">
                          <el-select v-model="ruleForm.em_programa" clearable placeholder="Seleccione sub-programa" style="width: 100%">
                            <el-option
                              v-for="item in optionsCampanas"
                              :key="item.c_codigo"
                              :label="item.c_nombre"
                              :value="item.c_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Codigo">
                          <el-input readOnly></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Cuadrilla">
                          <el-input v-model="ruleForm.em_cuadrilla" readOnly></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Cel. Empresa N°">
                          <el-input readOnly></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Radio Empresa N°">
                          <el-input readOnly></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Manejo y confianza">
                          <el-select v-model="ruleForm.em_confianza" clearable placeholder="Seleccione" style="width: 100%">
                            <el-option
                              v-for="item in optionsConfianza"
                              :key="item.value"
                              :label="item.label"
                              :value="item.value">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Placa">
                          <el-select clearable placeholder="Seleccione placa" style="width: 100%" disabled>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Sector">
                          <el-select v-model="ruleForm.em_codarea" clearable placeholder="Seleccione sector" style="width: 100%">
                            <el-option
                              v-for="item in optionsAreas"
                              :key="item.ar_codigo"
                              :label="item.ar_area"
                              :value="item.ar_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Cargo">
                          <el-select v-model="ruleForm.ca_codigo" clearable placeholder="Seleccione cargo" style="width: 100%">
                            <el-option
                              v-for="item in optionsCargos"
                              :key="item.ca_codigo"
                              :label="item.ca_cargo"
                              :value="item.ca_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item label="Perfil">
                          <el-select v-model="ruleForm.em_perfil" clearable placeholder="Seleccione perfil">
                            <el-option
                              v-for="item in optionsPerfil"
                              :key="item.value"
                              :label="item.label"
                              :value="item.value">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <fieldset style="padding: 1rem;border: 1px solid silver;">
                        <legend>Tallas de dotación</legend>
                        <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                          <el-form-item label="categoria">
                            <el-select v-model="ruleForm.ta_tipo" placeholder="Seleccione categoria" style="width: 100%">
                              <el-option
                                v-for="item in optionsCategoriaDotacion"
                                :key="item.cd_codigo"
                                :label="item.cd_nombre"
                                :value="item.cd_codigo">
                              </el-option>
                            </el-select>
                          </el-form-item>
                        </el-col>

                        <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                          <el-form-item label="Camisa">
                            <el-input v-model="ruleForm.ta_camisa"></el-input>
                          </el-form-item>
                        </el-col>

                        <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                          <el-form-item label="Pantalon">
                            <el-input v-model="ruleForm.ta_pantalon"></el-input>
                          </el-form-item>
                        </el-col>
                        
                        <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                          <el-form-item label="Botas">
                            <el-input v-model="ruleForm.ta_botas"></el-input>
                          </el-form-item>
                        </el-col>

                        <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                          <el-form-item>
                            <el-button icon="el-icon-search">Consultar dotación asignada</el-button>
                          </el-form-item>
                        </el-col>
                      </fieldset>
                    </el-row>
                  </el-form>
                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="4. Datos Conte">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
<!--                  <el-form label-position="top" size="mini">-->
                    <el-row :gutter="4">
                      <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                        <el-form-item label="">
                          <el-select v-model="ruleFormConte.dc_codconte" placeholder="Seleccione" style="width: 100%">
                            <el-option
                              v-for="item in optionsConte"
                              :key="item.co_codigo"
                              :label="item.co_nombre"
                              :value="item.co_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                        <el-form-item>
                          <el-button @click="addDatosConte">Adicionar</el-button>
                        </el-form-item>
                      </el-col>
                    </el-row>

                    <el-row>
                      <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                        <el-table
                          :data="tableData"
                          border
                          :index="indexMethod"
                          size="mini"
                          style="width: 100%">
                          <el-table-column
                            prop="dc_codconte"
                            label="Codigo"
                            width="80">
                          </el-table-column>
                          <el-table-column
                            prop="dc_desconte"
                            label="Descripción">
                          </el-table-column>
                          <el-table-column
                            label="Acción"
                            width="80">
                            <template slot-scope="scope">
                              <a href="javascript:void(0)" @click.stop.prevent="deleteRow(scope.row, scope.$index)">
                                <i class="el-icon-delete" aria-hidden="true"></i>
                              </a>
                            </template>
                          </el-table-column>
                        </el-table>
                      </el-col>
                    </el-row>
                    
<!--                  </el-form>-->
                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="5. Datos Contrato">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                  <el-form label-position="top" size="mini">
                    <el-row>
                      <el-form-item label="">
                        <el-button-group>
                          <el-button @click="modalVisible = true" size="medium">Nuevo contrato</el-button>
                          <el-button size="medium">Terminar contrato</el-button>
                        </el-button-group>
                      </el-form-item>
                    </el-row>

                    <el-row>
                      <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                        <el-table
                          :data="tableDataContrato"
                          border
                          size="mini"
                          style="width: 100%">
                          <el-table-column
                            type="index"
                            align="center"
                            prop="co_item"
                            label="Item"
                            width="80"
                            :index="indexMethod1">
                          </el-table-column>
                          <el-table-column
                            prop="co_empresa"
                            label="Empresa">
                          </el-table-column>
                          <el-table-column
                            prop="co_fechaing"
                            label="Fecha Inicio">
                          </el-table-column>
                          <el-table-column
                            prop="co_fechafin"
                            label="Fecha Retiro">
                          </el-table-column>
                          <el-table-column
                            prop="co_motivoretiro"
                            label="Motivo del retiro">
                          </el-table-column>
                          <el-table-column
                            prop="name"
                            label="Motivo real del retro">
                          </el-table-column>
                          <el-table-column header-align="center" align="center" label="Acción" width="70">
                            <template slot-scope="scope">
                              <a href="#" class="delete" @click.stop.prevent="editRowContrato(scope.row, scope.$index)">
                                <i class="el-icon-edit" aria-hidden="true"></i>
                              </a>
                              <a href="#" class="delete" @click.stop.prevent="deleteRowContrato(scope.row, scope.$index)">
                                <i class="el-icon-delete" aria-hidden="true"></i>
                              </a>
                            </template>
                          </el-table-column>
                        </el-table>
                      </el-col>
                    </el-row>
                  </el-form>
                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="6. Formación Academica">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
<!--                  <el-form label-position="top" size="mini">-->
                    <el-row :gutter="4">
                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Tipo estudio">
                          <el-select v-model="ruleFormFAcademica.df_codigo" placeholder="Seleccione tipo estudio" style="width: 100%">
                            <el-option
                              v-for="item in optionsTipoEstudio"
                              :key="item.es_codigo"
                              :label="item.es_nombre"
                              :value="item.es_codigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                        <el-form-item label="Ins. Educativa">
                          <el-input v-model="ruleFormFAcademica.df_insteducativa"></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                        <el-form-item label="Titulo obtenido">
                          <el-input v-model="ruleFormFAcademica.df_tituloobt"></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                        <el-form-item>
                          <el-button @click="addFormacionAcademica" style="margin-top: 1.55rem;">Adicinar</el-button>
                        </el-form-item>
                      </el-col>
                    </el-row>
<!--                  </el-form>-->

                  <el-row>
                    <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                      <el-table
                        :data="tableDataInfoAcademinaca"
                        border
                        size="mini"
                        style="width: 100%">
                        <el-table-column
                          prop="df_formacademica"
                          label="Tipo estudio">
                        </el-table-column>
                        <el-table-column
                          prop="df_insteducativa"
                          label="Ins. Educativa">
                        </el-table-column>
                        <el-table-column
                          prop="df_tituloobt"
                          label="Titulo Obtenido">
                        </el-table-column>
                      </el-table>
                    </el-col>
                  </el-row>
                </fieldset>
              </el-tab-pane>
              
              <el-tab-pane label="7. Información Familiar">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                  <el-form label-position="top" size="mini">
                    <el-row :gutter="4">
                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Cedula conyugue">
                          <el-input></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                        <el-form-item label="Nombre conyugue">
                          <el-input></el-input>
                        </el-form-item>
                      </el-col>
                    </el-row>
                  </el-form>

                  <el-row>
                    <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                      <el-table
                        :data="tableDataInfoFamiliar"
                        border
                        size="mini"
                        style="width: 100%">
                        <el-table-column
                          prop="date"
                          label="Identificación">
                        </el-table-column>
                        <el-table-column
                          prop="name"
                          label="Nombres y Apellidos">
                        </el-table-column>
                        <el-table-column
                          prop="name"
                          label="Fec Nacimiento">
                        </el-table-column>
                        <el-table-column
                          prop="name"
                          label="Sexo">
                        </el-table-column>
                        <el-table-column
                          prop="name"
                          label="Edad(Años)">
                        </el-table-column>
                      </el-table>
                    </el-col>
                  </el-row>

                </fieldset>
              </el-tab-pane>

              <el-tab-pane label="8. Vencimientos">
                <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                  <el-form label-position="top" size="mini">
                    <el-row :gutter="4">
                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Tipo">
                          <el-select v-model="ruleFormVencimientos.codtipovence" placeholder="Seleccione tipo" style="width: 100%">
                            <el-option
                              v-for="item in optionsTipoVencimiento"
                              :key="item.tvcodigo"
                              :label="item.tvdescripcion"
                              :value="item.tvcodigo">
                            </el-option>
                          </el-select>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                        <el-form-item label="Descripcion">
                          <el-input v-model="ruleFormVencimientos.descripcion"></el-input>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                        <el-form-item label="Fecha">
                          <el-date-picker
                            v-model="ruleFormVencimientos.fechavence"
                            style="width: 100%"
                            type="date"
                            format="MM/dd/yyyy"
                            value-format="MM/dd/yyyy"
                            placeholder="Seleccione fecha">
                          </el-date-picker>
                        </el-form-item>
                      </el-col>

                      <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                        <el-form-item>
                          <el-button @click="addVencimientos" style="margin-top: 1.55rem;">Adicinar</el-button>
                        </el-form-item>
                      </el-col>
                    </el-row>
                  </el-form>

                  <el-row>
                    <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                      <el-table
                        :data="tableDataVenciomientos"
                        border
                        size="mini"
                        style="width: 100%"
                        :index="indexMethod2">
                        <el-table-column
                          prop="codtipovence"
                          label="Tipo">
                        </el-table-column>
                        <el-table-column
                          prop="descripcion"
                          label="Descripcion">
                        </el-table-column>
                        <el-table-column
                          prop="fechavence"
                          label="Fecha Vigencia">
                        </el-table-column>
                        <el-table-column header-align="center" align="center" label="Acción" width="70">
                          <template slot-scope="scope">
                            <a href="#" class="delete" @click.stop.prevent="deleteRowVencimiento(scope.row, scope.$index)">
                              <i class="el-icon-delete" aria-hidden="true"></i>
                            </a>
                          </template>
                        </el-table-column>
                      </el-table>
                    </el-col>
                  </el-row>

                </fieldset>
              </el-tab-pane>
            </el-tabs>
          </div>
        </div>
      </div>
    </el-form>

    <el-dialog title="Nuevo Contrato" :visible.sync="modalVisible" :close-on-click-modal="false" class="window-dialog">
      <el-row :gutter="6">
        <el-form size="mini">
          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Empresa">
              <el-select v-model="ruleFormDatosContrato.co_codempresa" style="width: 100%">
                <el-option
                  v-for="(item, index ) in optionsEmpresa"
                  :key="index"
                  :label="item.nombre"
                  :value="item.sw">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Fecha ingreso">
              <el-date-picker
                v-model="ruleFormDatosContrato.co_fechaing"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy"
                placeholder="Seleccione fecha">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Fecha fin">
              <el-date-picker
                v-model="ruleFormDatosContrato.co_fechafin"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy"
                placeholder="Seleccione fecha">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Tipo contrato">
              <el-select v-model="ruleFormDatosContrato.co_tcontrato" style="width: 100%">
                <el-option
                  v-for="item in optionsTipoContrato"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Fecha retiro">
              <el-date-picker
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy"
                placeholder="Seleccione fecha">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Motivo retiro">
              <el-select v-model="ruleFormDatosContrato.co_codretiro" style="width: 100%">
                <el-option
                  v-for="item in optionsMotivoRetiro"
                  :key="item.mr_codigo"
                  :label="item.mr_motivo"
                  :value="item.mr_codigo">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Motivo real">
              <el-select style="width: 100%"></el-select>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <span slot="footer" class="dialog-footer">
        <el-button @click="addDatosContrato">Adicionar</el-button>
      </span>
    </el-dialog>

    <!--DIALOG BUSCAR FUNCIONARIO-->
    <el-dialog title="Busqueda de funcionario" :visible.sync="dialogBuscar" :close-on-click-modal="false" class="window-dialog">
      <el-row>
        <el-form size="mini">
          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item>
              <el-input></el-input>
            </el-form-item>
          </el-col>
        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataBuscar"
            border
            height="250"
            @current-change="rowClick"
            size="mini"
            style="width: 100%">
            <el-table-column
              prop="rh_cedula"
              width="100"
              label="Cedula">
            </el-table-column>
            <el-table-column
              prop="full_name"
              label="Nombre y Apellidos">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>

    </el-dialog>

  </div>
  <script>
    ELEMENT.locale(ELEMENT.lang.es);
    new Vue({
      el: '#app',
      data: () => ({
        ruleForm: {
          rh_cedula: '',
          rh_codexpciud: '',
          rh_expcedula: '',
          rh_apellido1: '',
          rh_apellido2: '',
          rh_nombre: '',
          rh_direccion: '',
          rh_codciudad: '',
          rh_ciudad: '',
          rh_codbarrio: '',
          rh_barrio: '',
          rh_telefono: '',
          rh_movil: '',
          rh_sexo: '',
          rh_dessexo: '',
          rh_facsangre: '',
          rh_rh: '',
          rh_rh1: '',
          rh_desrh: '',
          rh_estcivil: '',
          rh_fechanac: '',
          rh_codciunac: '',
          rh_ciudadnac: '',
          rh_coddptonac: '',
          rh_dptonac: '',
          rh_codnacional: '',
          rh_nacional: '',
          rh_estado: 'INACTIVO',
          rh_foto: '',
          rh_profesion: '',
          rh_explaboral: '',
          rh_nombre1: '',
          rh_nombre2: '',
          rh_edad: '',
          dl_codcentrab: '',
          dl_descentrab: '',
          dl_codcencosto: '',
          dl_descencosto: '',
          dl_salario: '',
          dl_nlibreta: '',
          dl_codclaselib: '',
          dl_desclaselib: '',
          dl_distritomil: '',
          dl_certjudicial: '',
          dl_fechaven: '',
          dl_hosrasdiarias: '',
          dp_codarp: '',
          dp_desarp: '',
          dp_codeps: '',
          dp_deseps: '',
          dp_afp: '',
          dp_desafp: '',
          dp_codcaja: '',
          dp_descaja: '',
          dp_codbanco: '',
          dp_desbanco: '',
          dp_ncuenta: '',
          dp_convencion: 'N',
          dp_adicionarp: '',
          em_cedula: '',
          em_codingeniero: '',
          em_nomingeniero: '',
          em_programa: '',
          em_desprograma: '',
          em_cuadrilla: '',
          em_codarea: '',
          em_desarea: '',
          em_codcargo: '',
          em_descargo: '',
          em_confianza: '',
          em_perfil: '',
          ta_camisa: '',
          ta_pantalon: '',
          ta_botas: '',
          ta_tipo: '',
          df_cedula: '',
          co_cedula: '',
          te_codigo: ''
        },
        ruleFormConte: {
          dc_codconte: '',
          dc_desconte: ''
        },
        ruleFormFAcademica: {
          df_codigo: '',
          df_formacademica: '',
          df_insteducativa: '',
          df_tituloobt: ''
        },
        ruleFormDatosContrato: {
          co_codempresa: '',
          co_empresa: '',
          co_fechaing: '',
          co_fechafin: '',
          co_tcontrato: '',
          co_codretiro: '',
          co_item: '',
          co_motivoretiro: '',
          co_codreal: '',
          co_motivoreal: '',
          co_fecharet: '',
          co_observacion: ''
        },
        ruleFormVencimientos: {
          codtipovence: '',
          descripcion: '',
          fechavence: ''
        },
        rules: {
          rh_cedula: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_codexpciud: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_apellido1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_nombre1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_direccion: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_codciudad: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codbarrio: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_telefono: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_movil: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_sexo: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_estcivil: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_rh: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_fechanac: [
            { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_coddptonac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codciunac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codnacional: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcentrab: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcencosto: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ]
        },
        imageUrl: '',
        tableData: [],
        tableDataContrato: [],
        tableDataInfoAcademinaca: [],
        tableDataInfoFamiliar: [],
        tableDataVenciomientos: [],
        tableDataBuscar: [],
        optionsDpto: [],
        optionsCiudades: [],
        optionsBarrio: [],
        optionsMunicipio: [],
        optionsNacionalidades: [],
        optionscentroTrabajo: [],
        optionscentroCosto: [],
        optionsBanco: [],
        optionsArp: [],
        optionsEps: [],
        optionsAfp: [],
        optionsCaja: [],
        optionsCampanas: [],
        optionsJefeInmediato: [],
        optionsAreas: [],
        optionsCargos: [],
        optionsConte: [],
        optionsTipoEstudio: [],
        optionsCategoriaDotacion: [],
        optionsEmpresa: [],
        optionsMotivoRetiro: [],
        optionsTipoVencimiento: [],
        optionsGenero: [
          {value: 'M', label: 'MASCULINO'},
          {value: 'F', label: 'FEMENINO'}
        ],
        optionsClaseLibreta: [
          {value: '0', label: 'NO APLICA'},
          {value: '1', label: 'PRIMERA CLASE'},
          {value: '2', label: 'SEGUNDA CLASE'}
        ],
        optionsTipoSanguineo: [
          {value: '1', label: 'NEGATIVO', fac_sangre: 'O', fac_sangre1: '-O', rhrh: '-'},
          {value: '2', label: 'POSITIVO', fac_sangre: 'O', fac_sangre1: '+O', rhrh: '+'},
          {value: '3', label: 'NEGATIVO', fac_sangre: 'A', fac_sangre1: '-A', rhrh: '-'},
          {value: '4', label: 'POSITIVO', fac_sangre: 'A', fac_sangre1: '+A', rhrh: '+'},
          {value: '5', label: 'NEGATIVO', fac_sangre: 'B', fac_sangre1: '-B', rhrh: '-'},
          {value: '6', label: 'POSITIVO', fac_sangre: 'B', fac_sangre1: '+B', rhrh: '+'},
          {value: '7', label: 'NEGATIVO', fac_sangre: 'AB', fac_sangre1: '-AB', rhrh: '-'},
          {value: '8', label: 'POSITIVO', fac_sangre: 'AB', fac_sangre1: '+AB', rhrh: '+'}
        ],
        optionsEstadoCivil: [
          {value: '0', label: 'Soltero(a)'},
          {value: '1', label: 'Casado(a)'},
          {value: '2', label: 'Viudo(a)'},
          {value: '3', label: 'Divorsiado(a)'},
          {value: '4', label: 'Union Libre'},
        ],
        optionsConfianza: [
          {value: '0', label: 'SI'},
          {value: '1', label: 'NO'}
        ],
        optionsPerfil: [
          {value: '0', label: 'Empleado Administrativo'},
          {value: '1', label: 'Jefe Administrativo'},
          {value: '2', label: 'Ingeniero de Proceso'},
          {value: '3', label: 'Supervisor'},
          {value: '4', label: 'Técnico Principal'},
          {value: '5', label: 'Técnico Auxiliar'},
          {value: '6', label: 'Capataz'},
          {value: '7', label: 'Gestor Comunitario'}
        ],
        optionsTipoContrato: [
          {value: '0', label: 'Termino Fijo (< 1 año)'},
          {value: '1', label: 'Termino Fijo (= 1 año)'},
          {value: '2', label: 'Termino Indefinido'},
          {value: '3', label: 'Labor Pactada (D.O.)'}
        ],
        fileList: [],
        sw: false,
        indexUpdate: null,
        modalVisible: false,
        dialogBuscar: false,
      }),
      methods: {
        addFuncionario: function (formName) {
          // this.$refs[formName].validate((valid) => {
          //   if (valid) {
          var fileValue = document.querySelector('.el-upload .el-upload__input');
          var data = new FormData();
          this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
          this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
          this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
          this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
          this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
          this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
          this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
          this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
          this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
          this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
          this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
          this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
          this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
          this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
          this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
          this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
          this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
          this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
          this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
          this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
          this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
          this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
          this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

          // var keys=Object.keys(this.ruleForm);
          // for(i=0;i<keys.length;i++){
          //   data.append(keys[i], this.ruleForm[keys[i]]);
          // }
          // console.log(this.ruleForm)
          // console.log(Object.keys(this.ruleForm));
          <!--region FormData DATOS_RRHH-->
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
          data.append('rh_expcedula', this.ruleForm.rh_expcedula);
          data.append('rh_apellido1', this.ruleForm.rh_apellido1);
          data.append('rh_apellido2', this.ruleForm.rh_apellido2);
          data.append('rh_nombre', this.ruleForm.rh_nombre);
          data.append('rh_direccion', this.ruleForm.rh_direccion);
          data.append('rh_codciudad', this.ruleForm.rh_codciudad);
          data.append('rh_ciudad', this.ruleForm.rh_ciudad);
          data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
          data.append('rh_barrio', this.ruleForm.rh_barrio);
          data.append('rh_telefono', this.ruleForm.rh_telefono);
          data.append('rh_movil', this.ruleForm.rh_movil);
          data.append('rh_sexo', this.ruleForm.rh_sexo);
          data.append('rh_dessexo', this.ruleForm.rh_dessexo);
          data.append('rh_facsangre', this.ruleForm.rh_facsangre);
          data.append('rh_rh', this.ruleForm.rh_rh1);
          data.append('rh_desrh', this.ruleForm.rh_desrh);
          data.append('rh_estcivil', this.ruleForm.rh_estcivil);
          data.append('rh_fechanac', this.ruleForm.rh_fechanac);
          data.append('rh_codciunac', this.ruleForm.rh_codciunac);
          data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
          data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
          data.append('rh_dptonac', this.ruleForm.rh_dptonac);
          data.append('rh_codnacional', this.ruleForm.rh_codnacional);
          data.append('rh_nacional', this.ruleForm.rh_nacional);
          data.append('rh_estado', this.ruleForm.rh_estado);
          data.append('rh_foto', fileValue.files[0]);
          data.append('rh_nombre1', this.ruleForm.rh_nombre1);
          data.append('rh_nombre2', this.ruleForm.rh_nombre2);
          <!--endregion-->

          <!--region FormData TECNICOS-->
          data.append('te_codigo', '');
          data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
          data.append('te_direccion', this.ruleForm.rh_direccion);
          data.append('te_telefono', this.ruleForm.rh_telefono);
          data.append('te_ciudad', this.ruleForm.rh_ciudad);
          data.append('te_cedula', this.ruleForm.rh_cedula);
          <!--endregion-->

          data.append('dl_cedula', this.ruleForm.rh_cedula);
          data.append('dl_conte', '');
          data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
          data.append('dl_descentrab', this.ruleForm.dl_descentrab);
          data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
          data.append('dl_descencosto', this.ruleForm.dl_descencosto);
          data.append('dl_salario', this.ruleForm.dl_salario);
          data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
          data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
          data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
          data.append('dl_distritomil', this.ruleForm.dl_distritomil);
          data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
          data.append('dl_fechaven', this.ruleForm.dl_fechaven);
          data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
          data.append('dp_cedula', this.ruleForm.rh_cedula);
          data.append('dp_codarp', this.ruleForm.dp_codarp);
          data.append('dp_desarp', this.ruleForm.dp_desarp);
          data.append('dp_codeps', this.ruleForm.dp_codeps);
          data.append('dp_deseps', this.ruleForm.dp_deseps);
          data.append('dp_afp', this.ruleForm.dp_afp);
          data.append('dp_desafp', this.ruleForm.dp_desafp);
          data.append('dp_codcaja', this.ruleForm.dp_codcaja);
          data.append('dp_descaja', this.ruleForm.dp_descaja);
          data.append('dp_codbanco', this.ruleForm.dp_codbanco);
          data.append('dp_desbanco', this.ruleForm.dp_desbanco);
          data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
          data.append('dp_convencion', this.ruleForm.dp_convencion);
          data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
          data.append('em_cedula', this.ruleForm.rh_cedula);
          data.append('em_codingeniero', this.ruleForm.em_codingeniero);
          data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
          data.append('em_programa', this.ruleForm.em_desprograma);
          data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
          data.append('em_codarea', this.ruleForm.em_codarea);
          data.append('em_desarea', this.ruleForm.em_desarea);
          data.append('em_codcargo', this.ruleForm.em_codcargo);
          data.append('em_descargo', this.ruleForm.em_descargo);
          data.append('em_confianza', this.ruleForm.em_confianza);
          data.append('em_perfil', this.ruleForm.em_perfil);
          data.append('ta_cedula', this.ruleForm.rh_cedula);
          data.append('ta_camisa', this.ruleForm.ta_camisa);
          data.append('ta_pantalon', this.ruleForm.ta_pantalon);
          data.append('ta_botas', this.ruleForm.ta_botas);
          data.append('ta_tipo', this.ruleForm.ta_tipo);
          data.append('dc_cedula', this.ruleForm.rh_cedula);
          data.append('conte', JSON.stringify(this.tableData));
          data.append('df_cedula', this.ruleForm.rh_cedula);
          data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));
          data.append('co_cedula', this.ruleForm.rh_cedula);
          data.append('contrato', JSON.stringify(this.tableDataContrato));
          data.append('cedula', this.ruleForm.rh_cedula);
          data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
          axios.post('request/insertFuncionario.php', data).then(response => {
            console.log(response.data.message);
            if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
            }
          }).catch(e => {
            console.log(e.response);
          });
          //   } else {
          //     console.log('error submit!!');
          //     return false;
          //   }
          // });
        },
        onUpdate () {
          var fileValue = document.querySelector('.el-upload .el-upload__input');
          var data = new FormData();

          this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
          this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
          this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
          this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
          this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
          this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
          this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
          this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
          this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
          this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
          this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
          this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
          this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
          this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
          this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
          this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
          this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
          this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
          this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
          this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
          this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
          this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
          this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

          //DATOS_RRHH
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
          data.append('rh_expcedula', this.ruleForm.rh_expcedula);
          data.append('rh_apellido1', this.ruleForm.rh_apellido1);
          data.append('rh_apellido2', this.ruleForm.rh_apellido2);
          data.append('rh_nombre', this.ruleForm.rh_nombre);
          data.append('rh_direccion', this.ruleForm.rh_direccion);
          data.append('rh_codciudad', this.ruleForm.rh_codciudad);
          data.append('rh_ciudad', this.ruleForm.rh_ciudad);
          data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
          data.append('rh_barrio', this.ruleForm.rh_barrio);
          data.append('rh_telefono', this.ruleForm.rh_telefono);
          data.append('rh_movil', this.ruleForm.rh_movil);
          data.append('rh_sexo', this.ruleForm.rh_sexo);
          data.append('rh_dessexo', this.ruleForm.rh_dessexo);
          data.append('rh_facsangre', this.ruleForm.rh_facsangre);
          data.append('rh_rh', this.ruleForm.rh_rh1);
          data.append('rh_desrh', this.ruleForm.rh_desrh);
          data.append('rh_estcivil', this.ruleForm.rh_estcivil);
          data.append('rh_fechanac', this.ruleForm.rh_fechanac);
          data.append('rh_codciunac', this.ruleForm.rh_codciunac);
          data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
          data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
          data.append('rh_dptonac', this.ruleForm.rh_dptonac);
          data.append('rh_codnacional', this.ruleForm.rh_codnacional);
          data.append('rh_nacional', this.ruleForm.rh_nacional);
          data.append('rh_estado', this.ruleForm.rh_estado);
          data.append('rh_foto', fileValue.files[0]);
          data.append('rh_nombre1', this.ruleForm.rh_nombre1);
          data.append('rh_nombre2', this.ruleForm.rh_nombre2);
          //TECNICOS
          data.append('te_codigo', this.ruleForm.te_codigo);
          data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
          data.append('te_direccion', this.ruleForm.rh_direccion);
          data.append('te_telefono', this.ruleForm.rh_telefono);
          data.append('te_ciudad', this.ruleForm.rh_ciudad);
          data.append('te_cedula', this.ruleForm.rh_cedula);
          axios.post('request/updateFuncionario.php', data).then(response => {
            console.log(response.data.message);
            if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
            }
          }).catch(e => {
            console.log(e.response);
          });
        },
        showDepartamentos () {
          axios.get('request/departamentos.php').then(response => {
            this.optionsDpto = response.data;
          }).then(response => {
            this.showCentroTrabajo();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showMunicipios () {
          var mu_depto = new FormData();
          mu_depto.append('mu_depto', this.ruleForm.rh_coddptonac);
          axios.post('request/municipios.php', mu_depto).then(response => {
            this.optionsMunicipio = response.data;
          }).catch(error => {
            console.log(error.response);
          })
        },
        showCiudades () {
          axios.get('request/ciudades.php').then(response => {
            this.optionsCiudades = response.data;
          }).then(response => {
            this.showDepartamentos();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showBarrios () {
          var ba_mpio = new FormData();
          ba_mpio.append('ba_mpio', this.ruleForm.rh_codciudad);
          axios.post('request/barrios.php', ba_mpio).then(response => {
            this.optionsBarrio = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showNacionalidades () {
          axios.get('request/nacionalidades.php').then(response => {
            this.optionsNacionalidades = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCentroTrabajo () {
          axios.get('request/centroTrabajo.php').then(response => {
            this.optionscentroTrabajo = response.data;
          }).then(response => {
            this.showCentroCosto();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCentroCosto () {
          axios.get('request/centroCosto.php').then(response => {
            this.optionscentroCosto = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showARP () {
          axios.get('request/arp.php').then(response => {
            this.optionsArp = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showEPS () {
          axios.get('request/eps.php').then(response => {
            this.optionsEps = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showAFP () {
          axios.get('request/afp.php').then(response => {
            this.optionsAfp = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCaja () {
          axios.get('request/caja.php').then(response => {
            this.optionsCaja = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showBancos () {
          axios.get('request/bancos.php').then(response => {
            this.optionsBanco = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showJefeInmediato () {
          axios.get('request/datos_rrhh.php').then(response => {
            this.optionsJefeInmediato = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCampanas () {
          axios.get('request/campanas.php').then(response => {
            this.optionsCampanas = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showAreas () {
          axios.get('request/areas.php').then(response => {
            this.optionsAreas = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCargos () {
          axios.get('request/cargos.php').then(response => {
            this.optionsCargos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showConte () {
          axios.get('request/conte.php').then(response => {
            this.optionsConte = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showTipoEstudio () {
          axios.get('request/tipo_estudio.php').then(response => {
            this.optionsTipoEstudio = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCategoriaDotacion () {
          axios.get('request/categoria_dotacion.php').then(response => {
            this.optionsCategoriaDotacion = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showEmpresa () {
          axios.get('request/empresa.php').then(response => {
            this.optionsEmpresa = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showMotivoRetiro () {
          axios.get('request/motivo_retiro.php').then(response => {
            this.optionsMotivoRetiro = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showTipoVencimiento () {
          axios.get('request/tipo_vencimiento.php').then(response => {
            this.optionsTipoVencimiento = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showFuncionarios () {
          axios.get('request/getFuncionario.php').then(response => {
            console.log(response)
            this.tableDataBuscar = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        addDatosConte () {
          this.ruleFormConte.dc_desconte = this.valueSelect(this.optionsConte, 'co_codigo', 'co_nombre', this.ruleFormConte.dc_codconte);
          var data = this.ruleFormConte;
          this.tableData.push(data);
          this.clear();
        },
        deleteRow (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableData, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addFormacionAcademica () {
          this.ruleFormFAcademica.df_formacademica = this.valueSelect(this.optionsTipoEstudio, 'es_codigo', 'es_nombre', this.ruleFormFAcademica.df_codigo);
          var data = this.ruleFormFAcademica;
          this.tableDataInfoAcademinaca.push(data);
          this.clearFAcademica();
        },
        addDatosContrato () {
          this.ruleFormDatosContrato.co_empresa = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_motivoretiro = this.valueSelect(this.optionsMotivoRetiro, 'mr_codigo', 'mr_motivo', this.ruleFormDatosContrato.co_codretiro);
          // this.ruleFormDatosContrato.co_motivoreal = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_item = this.tableDataContrato.length + 1;
          var data = this.ruleFormDatosContrato;
          if (!this.sw) {
            this.tableDataContrato.push(data)
          } else {
            this.tableDataContrato.splice(this.indexUpdate, 1, data);
            this.sw = false;
            this.clearDatosContrato();
          }
          this.clearDatosContrato();
        },
        editRowContrato (row, index) {
          console.log(row)
          this.ruleFormDatosContrato.co_codempresa = row.co_codempresa;
          this.ruleFormDatosContrato.co_empresa = row.co_empresa;
          this.ruleFormDatosContrato.co_fechaing = row.co_fechaing;
          this.ruleFormDatosContrato.co_fechafin = row.co_fechafin;
          this.ruleFormDatosContrato.co_codretiro = row.co_codretiro;
          this.ruleFormDatosContrato.co_motivoretiro = row.co_motivoretiro;
          this.ruleFormDatosContrato.co_tcontrato = row.co_tcontrato;
          var data = this.ruleFormDatosContrato;
          this.indexUpdate = index;
          this.modalVisible = true;
          this.sw = 1;
        },
        deleteRowContrato (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataContrato, index);
            this.sw = false;
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addVencimientos () {
          // this.ruleFormVencimientos.descripcion = this.valueSelect(this.optionsTipoVencimiento, 'tvcodigo', 'tvdescripcion', this.ruleFormVencimientos.codtipovence);
          var data = this.ruleFormVencimientos;
          this.tableDataVenciomientos.push(data);
          this.clearVencimientos();
        },
        deleteRowVencimiento (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataVenciomientos, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        handleChange (file, fileList1) {
          this.fileList = fileList1
        },
        handlePreview (file) {
          // this.fileList = file
          // console.log(file)
        },
        valueSelect (data, value, label, dato) {
          let labelReturn = '';
          data.forEach((item, index, arr) => {
            if (dato === item[value]) {
              labelReturn = item[label];
              // arr.length = 0
              return false;
            }
          });
          return labelReturn;
        },
        indexMethod (index) {
          return index;
        },
        indexMethod1 (index) {
          return index + 1;
        },
        indexMethod2 (index) {
          return index;
        },
        rowClick (row) {
          this.ruleForm.rh_cedula = row.rh_cedula
          this.ruleForm.rh_codexpciud = row.rh_codexpciud
          this.ruleForm.rh_expcedula = row.rh_expcedula
          this.ruleForm.rh_apellido1 = row.rh_apellido1
          this.ruleForm.rh_apellido2 = row.rh_apellido2
          this.ruleForm.rh_nombre1 = row.rh_nombre1
          this.ruleForm.rh_nombre2 = row.rh_nombre2
          this.ruleForm.rh_direccion = row.rh_direccion
          this.ruleForm.rh_codciudad = row.rh_codciudad
          this.ruleForm.rh_ciudad = row.rh_ciudad
          this.ruleForm.rh_codbarrio = row.rh_codbarrio
          this.ruleForm.rh_barrio = row.rh_barrio
          this.showBarrios();
          this.ruleForm.rh_telefono = row.rh_telefono
          this.ruleForm.rh_movil = row.rh_movil
          this.ruleForm.rh_sexo = row.rh_sexo
          this.ruleForm.rh_dessexo = row.rh_dessexo
          this.ruleForm.rh_facsangre = row.rh_facsangre
          this.ruleForm.rh_rh1 = row.rh_rh
          this.ruleForm.rh_desrh = row.rh_desrh
          this.ruleForm.rh_estcivil = row.rh_estcivil
          this.ruleForm.rh_fechanac = row.rh_fechanac
          this.ruleForm.rh_coddptonac = row.rh_coddptonac
          this.ruleForm.rh_dptonac = row.rh_dptonac
          this.ruleForm.rh_codciunac = row.rh_codciudnac
          this.ruleForm.rh_ciudadnac = row.rh_ciudadnac
          this.ruleForm.rh_codnacional = row.rh_codnacional
          this.ruleForm.rh_nacional = row.rh_nacional
          this.showMunicipios();
          this.ruleForm.te_codigo = row.te_codigo
          this.ruleForm.dl_codcentrab = row.dl_codcentrab
          this.ruleForm.dl_descentrab = row.dl_descentrab
          console.log(row.dl_codcentrab)
          // this.ruleForm.dl_codcencosto = row.dl_codcencosto
          // this.ruleForm.dl_descencosto = row.dl_descencosto
          // this.ruleForm.dl_salario = row.dl_salario
          // this.ruleForm.dl_nlibreta = row.dl_nlibreta
          // this.ruleForm.dl_codclaselib = row.dl_codclaselib
          // this.ruleForm.dl_desclaselib = row.dl_desclaselib
          // this.ruleForm.dl_distritomil = row.dl_distritomil
          // this.ruleForm.dl_certjudicial = row.dl_certjudicial
          // this.ruleForm.dl_fechaven = row.dl_fechaven
          // this.ruleForm.dl_horasdiarias = row.dl_horasdiarias
          this.dialogBuscar = false;
        },
        clear () {
          this.ruleFormConte = {
            dc_codconte: '',
            dc_desconte: ''
          }
        },
        clearFAcademica () {
          this.ruleFormFAcademica = {
            df_codigo: '',
            df_formacademica: '',
            df_insteducativa: '',
            df_tituloobt: ''
          }
        },
        clearDatosContrato () {
          this.ruleFormDatosContrato = {
            co_codempresa: '',
            co_empresa: '',
            co_fechaing: '',
            co_fechafin: '',
            co_tcontrato: '',
            co_codretiro: '',
            co_motivoretiro: '',
            co_codreal: '',
            co_motivoreal: '',
            co_fecharet: '',
            co_observacion: ''
          }
        },
        clearVencimientos () {
          this.ruleFormVencimientos = {
            codtipovence: '',
            descripcion: '',
            fechavence: ''
          }
        }
      },
      mounted () {
        // this.showDepartamentos();
        this.showCiudades();
        this.showNacionalidades();
        // this.showCentroTrabajo();
        this.showCentroCosto();
        this.showARP();
        this.showEPS();
        this.showAFP();
        this.showCaja();
        this.showBancos();
        this.showJefeInmediato();
        this.showCampanas();
        this.showAreas ();
        this.showCargos();
        this.showConte();
        this.showTipoEstudio();
        this.showCategoriaDotacion();
        this.showEmpresa();
        this.showMotivoRetiro();
        this.showTipoVencimiento();
        this.showFuncionarios();
      }
    });
  </script>
</body>
</html>