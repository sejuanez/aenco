<?php
  session_start();
   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo"<script>window.location.href='../inicio/index.php';</script>";
    exit();
  } 
?>

<!DOCTYPE html>

<html>
<head>
  <meta charset="UTF-8">
  <title>Ingreso funcionarios</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <!-- <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' /> -->
  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' /> -->
  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />
  <!-- <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script> -->
  <!-- <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css"> -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <link rel="stylesheet" href="js/select2/select2.min.css">
  <link rel="stylesheet" href="css/select2.css">
  <style>
    .el-form-item--mini.el-form-item, .el-form-item--small.el-form-item {
      width: 100%;
    }
    .el-form-item--mini .el-form-item__content, .el-form-item--mini .el-form-item__label{
      line-height: 24px;
    }
    .el-select {
      width: 100%;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .buscar {
      padding-left: 0px !important;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-message-box{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-dialog{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    .window-dialog, .el-dialog{
      width: 80%;
    }
    [class*=" el-icon-"], [class^=el-icon-]{
      margin-left: -.4rem;
    }
    @media only screen and (max-width: 768px) {
      #list-img img{
      	width: 296px;
    		height: 170px;
      }
    }
  </style>
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="js/select2/select2.full.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/inLetters.js"></script>
 <!--  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script> -->
</head>
<body>
  <div id="app" v-loading="loading">
    <header>
      <h3>Ingreso funcionarios</h3>
      <nav>
        <ul id="menu">
          <li v-if="visibleCreate" id="consultar">
            <a @click.stop.prevent="addFuncionario" href="">
              <span class="ion-android-done"></span><h6>Guardar</h6>
            </a>
          </li>
          <li v-if="visibleUpdate">
            <a @click.stop.prevent="onUpdate" href="">
              <span class="ion-android-done"></span><h6>Actualizar</h6>
            </a>
          </li>
          <li id="consultar">
          <!-- dialogBuscar = true -->
          	<a @click.stop.prevent="calculo" href="">
          		<span class="ion-ios-search-strong"></span><h6>Consultar</h6></a>
          </li>  
        </ul>
      </nav>
    </header>

	  <section style="padding-top: 44px; height: calc(100% - 80px); height: -webkit-calc(100% - 80px); height: -moz-calc(100% - 80px)">
	    <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
	      <legend>Datos personales</legend>
	      <el-form size="mini">
	        <el-row :gutter="4">

            <el-col :xs="24" :sm="24" :md="18" :lg="18" :xl="18">

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Cedula">
                  <el-input v-model="ruleForm.rh_cedula" @keyup.enter.native="getFuncionarioBY" @blur="getFuncionarioBY"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codexpciud"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                <el-form-item label="Expedida en">
                  <el-input v-model="ruleForm.rh_expcedula" readOnly></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudadesExpedicion" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Primer apellido">
                  <el-input v-model="ruleForm.rh_apellido1"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Segundo apellido">
                  <el-input v-model="ruleForm.rh_apellido2"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                <el-form-item label="Primer nombre">
                  <el-input v-model="ruleForm.rh_nombre1"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                <el-form-item label="Segundo nombre">
                  <el-input v-model="ruleForm.rh_nombre2"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                <el-form-item label="Dirección">
                  <el-input v-model="ruleForm.rh_direccion"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codciudad"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="9" :lg="9" :xl="9">
                <el-form-item label="Ciudad">
                  <el-input v-model="ruleForm.rh_ciudad" readOnly></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudades" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codbarrio"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="7" :lg="7" :xl="7">
                <el-form-item label="Barrio">
                  <el-input v-model="ruleForm.rh_barrio" readOnly></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openBarrios" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item label="Teléfono">
                  <el-input v-model="ruleForm.rh_telefono"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item label="Celular">
                  <el-input v-model="ruleForm.rh_movil"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Sexo">
                  <el-select v-model="ruleForm.rh_sexo" clearable placeholder="Seleccione sexo" style="width: 100%">
                    <el-option
                      v-for="item in optionsGenero"
                      :key="item.value"
                      :label="item.label"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Estado civil">
                  <el-select v-model="ruleForm.rh_estcivil" clearable placeholder="Seleccione estado civil" style="width: 100%">
                    <el-option
                      v-for="item in optionsEstadoCivil"
                      :key="item.value"
                      :label="item.label"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Factor sangre">
                  <el-select v-model="ruleForm.rh_rh" filterable clearable placeholder="Factor sanguineo" style="width: 100%">
                    <el-option
                      v-for="item in optionsTipoSanguineo"
                      :key="item.value"
                      :label="item.fac_sangre1"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
              	<el-form-item label="Foto">
              		<input type ="file" class="form-control-file" id="foto" accept="image/*" name="foto">
              	</el-form-item>

                <!-- <el-upload
                  class="filesFormulario button-upload foto"
                  ref="upload"
                  action=""
                  accept=".jpg, .png"
                  :on-preview="handlePreview"
                  :auto-upload="false"
                  :on-progress="handleChange"
                  :http-request="handleChange"
                  :on-success="handleAvatarSuccess"
                  :on-change="handleChange"
                  :on-remove="handleChange"
                  :file-list="fileList">
                  <el-button slot="trigger" size="mini" style="margin-top: 1.75rem;">Clic para subir archivo</el-button>
                  <div slot="tip" class="el-upload__tip"></div>
                </el-upload> -->
              </el-col>

            </el-col>

            <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            	<!-- <el-upload
							  class="upload-demo"
							  action=""
							  :limit="1"
							  accept=".jpg"
							  :on-preview="handlePreview"
							  :auto-upload="false"
							  :file-list="fileList"
							  list-type="picture">
							  <el-button size="mini" style="margin-top: 1.5rem;">Seleccione archivo</el-button>
							</el-upload> -->
            	<img id="imgFuncionario" src="img/avatar.jpg" alt="" width="250" height="190" >
              <!-- <div class="fileupload fileupload-new" data-provides="fileupload">
                  <div id="getimagen_cobradores" class="fileupload-new img-thumbnail">
                      <img width="260" height="193" id="usuarios_imagen" src="">
                  </div>
                  <div style="visibility: hidden;">
                      <span class="btn btn-facebook btn-file" >
                          <span class="fileupload-new">Seleccionar imagen</span><br>
                            <span class="fileupload-exists">Change</span> 
                          <input type="file" id="archivo" class="file" name="archivo_cobradores" value="Seleccionar imagen">
                      </span>
                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a> 
                  </div>
              </div> -->

            </el-col>
            
            <!-- DATOS DE NACIMIENTO -->
            <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
              <fieldset style="padding: 1rem;border: 1px solid silver;">
                <legend>Datos nacimiento</legend>
                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Fecha de nacimiento">
                    <el-date-picker
                      v-model="ruleForm.rh_fechanac"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1">
                  <el-form-item label="Edad">
                    <el-input v-model="ruleForm.rh_edad" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="7" :lg="7" :xl="7">
                  <el-form-item label="Departamento">
                    <el-select @change="showMunicipios" v-model="ruleForm.rh_coddptonac" filterable clearable placeholder="Seleccione departamento" style="width: 100%">
                      <el-option
                        v-for="item in optionsDpto"
                        :key="item.de_codigo"
                        :label="item.de_nombre"
                        :value="item.de_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                  <el-form-item label="Ciudad">
                    <el-select style="width: 100%" v-model="ruleForm.rh_codciunac" filterable clearable placeholder="Seleccione ciudad">
                      <el-option
                        v-for="item in optionsMunicipio"
                        :key="item.mu_codigo"
                        :label="item.mu_municipio"
                        :value="item.mu_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                  <el-form-item label="Nacionalidad">
                    <el-select style="width: 100%" v-model="ruleForm.rh_codnacional" filterable clearable placeholder="Seleccionne nacionalidad">
                      <el-option
                        v-for="item in optionsNacionalidades"
                        :key="item.na_codigo"
                        :label="item.na_nacional"
                        :value="item.na_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>
              </fieldset>
            </el-col>

	        </el-row>
	      </el-form>
	    </fieldset>
		</section>
<!--    v-loading="loadingCard"-->
		<section id="seccion-tabla" style="margin: 1rem;">

      <el-tabs type="card">

        <el-tab-pane label="1. Datos Legales">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                <el-form size="mini">
                <el-row :gutter="4">

                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <el-form-item label="Cent. Trabajo">
                      <el-select v-model="ruleForm.dl_codcentrab" filterable clearable placeholder="Seleccione cent. trabajo" style="width: 100%">
                        <el-option
                          v-for="item in optionscentroTrabajo"
                          :key="item.ct_codigo"
                          :label="item.ct_centrabajo"
                          :value="item.ct_codigo">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <el-form-item label="Cent. Costo">
                      <el-select v-model="ruleForm.dl_codcencosto" style="width: 100%" filterable clearable placeholder="Seleccione cent. costo">
                        <el-option
                          v-for="item in optionscentroCosto"
                          :key="item.cc_codigo"
                          :label="item.cc_cencosto"
                          :value="item.cc_codigo">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Salario">
                      <el-input v-model="ruleForm.dl_salario" @keyup.enter.native="inLetter" @blur="inLetter"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Horas Diarias">
                      <el-input v-model="ruleForm.dl_hosrasdiarias"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Comp. Horas">
                      <el-input v-model="ruleForm.dl_comphoras" readOnly></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                    <el-form-item label="Salario en Letras">
                      <el-input v-model="ruleForm.dl_salario_letras" type="textarea" readOnly></el-input>
                    </el-form-item>
                  </el-col>
                </el-row>

               </el-form>
              </fieldset>
            </el-col>

            <el-col ::xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                <legend>Libreta Militar</legend>
                <el-form label-position="top" size="mini">

                  <el-row :gutter="4">
                    <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                      <el-form-item label="Número">
                        <el-input v-model="ruleForm.dl_nlibreta"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="10" :lg="10" :xl="10">
                      <el-form-item label="Clase">
                        <el-select v-model="ruleForm.dl_codclaselib" style="width: 100%" clearable placeholder="Seleccione clase">
                          <el-option
                            v-for="item in optionsClaseLibreta"
                            :key="item.value"
                            :label="item.label"
                            :value="item.value">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                      <el-form-item label="Distrito">
                        <el-input v-model="ruleForm.dl_distritomil"></el-input>
                      </el-form-item>
                    </el-col>
                  </el-row>

                </el-form>
              </fieldset>

              <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
                <legend>Certificado Judicial</legend>
                <el-form label-position="top" size="mini">
                  <el-row :gutter="4">

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Número">
                        <el-input v-model="ruleForm.dl_certjudicial"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Fecha">
                        <el-date-picker
                          v-model="ruleForm.dl_fechaven"
                          style="width: 100%"
                          type="date"
                          format="MM/dd/yyyy"
                          value-format="MM/dd/yyyy">
                        </el-date-picker>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="2. Parafiscales">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset style="padding: 1rem;border: 1px solid silver;">
                <legend>Seguridad social</legend>
                <el-form size="mini">

                  <el-row :gutter="4">
                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="ARP">
                        <el-select style="width: 100%" v-model="ruleForm.dp_codarp" filterable clearable placeholder="Seleccione ARP">
                          <el-option
                            v-for="item in optionsArp"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="EPS">
                        <el-select style="width: 100%" v-model="ruleForm.dp_codeps" filterable clearable placeholder="Seleccione EPS">
                          <el-option
                            v-for="item in optionsEps"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="AFP">
                        <el-select style="width: 100%" v-model="ruleForm.dp_afp" filterable clearable placeholder="Seleccione AFP">
                          <el-option
                            v-for="item in optionsAfp"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Caja">
                        <el-select style="width: 100%" v-model="ruleForm.dp_codcaja" filterable clearable placeholder="Seleccione caja">
                          <el-option
                            v-for="item in optionsCaja"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="7" :lg="7" :xl="7">
                      <el-form-item label="% Adicional ARP">
                        <el-input v-model="ruleForm.dp_adicionarp"></el-input>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>

            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset style="padding: 1rem;border: 1px solid silver;margin-left: .8rem;">
                <legend>Cuenta Bancaria</legend>
                <el-form label-position="top" size="mini">
                  <el-row :gutter="4">

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Banco">
                        <el-select style="width: 100%" v-model="ruleForm.dp_codbanco" filterable clearable placeholder="Seleccione banco">
                          <el-option
                            v-for="item in optionsBanco"
                            :key="item.ba_codigo"
                            :label="item.ba_nombre"
                            :value="item.ba_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="N° Cuenta">
                        <el-input v-model="ruleForm.dp_ncuenta"></el-input>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="3. Empresa">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Jefe inmediato">
                    <el-select v-model="ruleForm.em_codingeniero" filterable clearable placeholder="Seleccione jefe inmedianto" style="width: 100%">
                      <el-option
                        v-for="item in optionsJefeInmediato"
                        :key="item.rh_cedula"
                        :label="item.full_name"
                        :value="item.rh_cedula">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Programa">
                    <el-select v-model="ruleForm.em_programa" filterable clearable placeholder="Seleccione programa" style="width: 100%">
                      <el-option
                        v-for="item in optionsCampanas"
                        :key="item.c_codigo"
                        :label="item.c_nombre"
                        :value="item.c_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleForm.em_codigo" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cuadrilla">
                    <el-input v-model="ruleForm.em_cuadrilla" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cel. Empresa N°">
                    <el-input v-model="ruleForm.em_cel_empresa_num" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Radio Empresa N°">
                    <el-input v-model="ruleForm.em_radio_empresa_num" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Manejo y confianza">
                    <el-select v-model="ruleForm.em_confianza" clearable placeholder="Seleccione una opción" style="width: 100%">
                      <el-option
                        v-for="item in optionsConfianza"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Placa">
                    <el-select v-model="ruleForm.em_placa" clearable placeholder="Seleccione placa" style="width: 100%" disabled>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sector">
                    <el-select v-model="ruleForm.em_codarea" clearable placeholder="Seleccione sector" style="width: 100%">
                      <el-option
                        v-for="item in optionsAreas"
                        :key="item.ar_codigo"
                        :label="item.ar_area"
                        :value="item.ar_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                  <el-form-item label="Cargo">
                    <el-select v-model="ruleForm.em_codcargo" filterable clearable placeholder="Seleccione cargo" style="width: 100%">
                      <el-option
                        v-for="item in optionsCargos"
                        :key="item.ca_codigo"
                        :label="item.ca_cargo"
                        :value="item.ca_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Perfil">
                    <el-select v-model="ruleForm.em_perfil" clearable placeholder="Seleccione perfil">
                      <el-option
                        v-for="item in optionsPerfil"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>
              </el-row>

              <!-- TALLAS DE DOTACION -->
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                  <fieldset style="padding: 1rem;border: 1px solid silver;">
                    <legend>Tallas de dotación</legend>
                    <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                      <el-form-item label="categoria">
                        <el-select v-model="ruleForm.ta_tipo" clearable placeholder="Seleccione categoria" style="width: 100%">
                          <el-option
                            v-for="item in optionsCategoriaDotacion"
                            :key="item.cd_codigo"
                            :label="item.cd_nombre"
                            :value="item.cd_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Camisa">
                        <el-input v-model="ruleForm.ta_camisa"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Pantalon">
                        <el-input v-model="ruleForm.ta_pantalon"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Botas">
                        <el-input v-model="ruleForm.ta_botas"></el-input>
                      </el-form-item>
                    </el-col>

                    <!-- <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item>
                        <el-button icon="el-icon-search">Consultar dotación asignada</el-button>
                      </el-form-item>
                    </el-col> -->
                  </fieldset>
                </el-col>
              </el-row>
              
            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="4. Conte">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
            <el-row :gutter="4">
              <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                <el-form-item label="">
                  <el-select v-model="ruleFormConte.dc_codconte" filterable clearable placeholder="Seleccione" style="width: 100%">
                    <el-option
                      v-for="item in optionsConte"
                      :key="item.co_codigo"
                      :label="item.co_nombre"
                      :value="item.co_codigo">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                <el-form-item>
                  <el-button @click="addDatosConte">Adicionar</el-button>
                </el-form-item>
              </el-col>
            </el-row>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableData"
                  border
                  :index="indexMethod"
                  size="mini"
                  style="width: 100%">
                  <el-table-column
                    prop="dc_codconte"
                    label="Codigo"
                    width="80">
                  </el-table-column>
                  <el-table-column
                    prop="dc_desconte"
                    label="Descripción">
                  </el-table-column>
                  <el-table-column
                    label="Acción"
                    width="80">
                    <template slot-scope="scope">
                      <a href="javascript:void(0)" @click.stop.prevent="deleteRow(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>

            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="5. Contrato">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
              <el-row>
                <el-form-item label="">
                  <el-button-group>
                    <el-button @click="modalVisible = true" size="medium">Nuevo contrato</el-button>
                    <el-button size="medium">Terminar contrato</el-button>
                  </el-button-group>
                </el-form-item>
              </el-row>

              <el-row>
                <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                  <el-table
                    :data="tableDataContrato"
                    border
                    size="mini"
                    style="width: 100%">
                    <el-table-column
                      type="index"
                      align="center"
                      prop="co_item"
                      label="Item"
                      width="80"
                      :index="indexMethod1">
                    </el-table-column>
                    <el-table-column
                      prop="co_empresa"
                      label="Empresa">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      width="90"
                      prop="co_fechaing"
                      label="Fecha Inicio">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      prop="co_fechafin"
                      width="90"
                      label="Fecha fin">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      prop="co_fecharet"
                      width="90"
                      label="Fecha Retiro">
                    </el-table-column>
                    <el-table-column
                      prop="co_motivoretiro"
                      label="Motivo del retiro">
                    </el-table-column>
                    <el-table-column
                      prop="name"
                      label="Motivo real del retro">
                    </el-table-column>
                    <el-table-column header-align="center" align="center" label="Acción" width="70">
                      <template slot-scope="scope">
                        <a href="#" class="delete" @click.stop.prevent="editRowContrato(scope.row, scope.$index)">
                          <i class="el-icon-edit" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="delete" @click.stop.prevent="deleteRowContrato(scope.row, scope.$index)">
                          <i class="el-icon-delete" aria-hidden="true"></i>
                        </a>
                      </template>
                    </el-table-column>
                  </el-table>
                </el-col>
              </el-row>
            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="6. Formación Academica">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
            <el-row :gutter="4">
              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Tipo estudio">
                  <el-select v-model="ruleFormFAcademica.df_codigo" filterable clearable placeholder="Seleccione tipo estudio" style="width: 100%">
                    <el-option
                      v-for="item in optionsTipoEstudio"
                      :key="item.es_codigo"
                      :label="item.es_nombre"
                      :value="item.es_codigo">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                <el-form-item label="Ins. Educativa">
                  <el-input v-model="ruleFormFAcademica.df_insteducativa"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                <el-form-item label="Titulo obtenido">
                  <el-input v-model="ruleFormFAcademica.df_tituloobt"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item>
                  <el-button @click="addFormacionAcademica" style="margin-top: 1.55rem;">Adicinar</el-button>
                </el-form-item>
              </el-col>
            </el-row>
            </el-form>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableDataInfoAcademinaca"
                  border
                  :index="indexMethod5"
                  size="mini"
                  style="width: 100%">
                  <el-table-column
                    prop="df_formacademica"
                    label="Tipo estudio">
                  </el-table-column>
                  <el-table-column
                    prop="df_insteducativa"
                    label="Ins. Educativa">
                  </el-table-column>
                  <el-table-column
                    prop="df_tituloobt"
                    label="Titulo Obtenido">
                  </el-table-column>
                  <el-table-column header-align="center" align="center" label="Acción" width="70">
                    <template slot-scope="scope">
                      <a href="#" class="delete" @click.stop.prevent="deleteRowFormacademica(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="7. Inf. Familiar">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
              <el-row :gutter="6">
                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cedula conyugue">
                    <el-input v-model="ruleFormInfoFamiliar.if_cedulacony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleFormInfoFamiliar.if_ciudadexp"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                  <el-form-item label="Ciudad Exp">
                    <el-input v-model="ruleFormInfoFamiliar.if_desciudadexp" readOnly></el-input>
                  </el-form-item>
                </el-col>

              <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudadInfoFamiliar1" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col>

                <el-col :xs="24" :sm="24" :md="11" :lg="11" :xl="11">
                  <el-form-item label="Nombre conyugue">
                    <el-input v-model="ruleFormInfoFamiliar.if_nombrecony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Fecha Nac">
                    <el-date-picker
                      v-model="ruleFormInfoFamiliar.if_fechanaccony"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleFormInfoFamiliar.if_ciudad"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Ciudad">
                    <el-input v-model="ruleFormInfoFamiliar.if_desciudad" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                  <el-form-item>
                    <el-button @click="openCiudadInfoFamiliar2" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sexo">
                    <el-select v-model="ruleFormInfoFamiliar.if_sexocony" clearable placeholder="Seleccione sexo" style="width: 100%">
                      <el-option
                        v-for="item in optionsGenero"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="10" :lg="10" :xl="10">
                  <el-form-item label="Ocupacion">
                    <el-input v-model="ruleFormInfoFamiliar.if_profecony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="11" :lg="11" :xl="11">
                  <el-form-item label="Nombre del hijo">
                    <el-input v-model="ruleFormInfoHijos.ih_nombrehijo"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Fecha Nac">
                    <el-date-picker
                      v-model="ruleFormInfoHijos.ih_fechanac"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                  <el-form-item label="D.I. / Reg Civil">
                    <el-input v-model="ruleFormInfoHijos.ih_identifica"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sexo">
                    <el-select v-model="ruleFormInfoHijos.ih_sexo" clearable placeholder="Seleccione sexo" style="width: 100%">
                      <el-option
                        v-for="item in optionsGenero"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                  <el-form-item>
                    <el-button @click="addInfoFamiliar" style="margin-top: 1.5rem;">Adicionar</el-button>
                  </el-form-item>
                </el-col>

              </el-row>
              
            </el-form>

            <!-- <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24"> -->
              <el-table
                :data="tableDataInfoHijos"
                border
                size="mini"
                style="width: 100%">
                <el-table-column
                  type="index"
                  align="center"
                  label="#"
                  width="80"
                  :index="indexMethod4">
                </el-table-column>
                <el-table-column
                  prop="ih_identifica"
                  label="Identificación"
                  width="100">
                </el-table-column>
                <el-table-column
                  prop="ih_nombrehijo"
                  label="Nombre y Apellidos">
                </el-table-column>
                <el-table-column
                  prop="ih_fechanac"
                  label="Fec Nacimiento"
                  width="110">
                </el-table-column>
                <el-table-column
                  prop="if_desciudad"
                  label="Ciudad">
                </el-table-column>
                <el-table-column
                  prop="ih_sexo"
                  label="Sexo"
                  width="80">
                </el-table-column>
                <el-table-column header-align="center" align="center" label="Acción" width="70">
                  <template slot-scope="scope">
                    <a href="#" class="delete" @click.stop.prevent="deleteRowInfoHijos(scope.row, scope.$index)">
                      <i class="el-icon-delete" aria-hidden="true"></i>
                    </a>
                  </template>
                </el-table-column>
              </el-table>
            <!-- </el-col> -->

          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="8. Vencimientos">
          <fieldset style="padding: 1rem;border: 1px solid silver;margin: 1rem;">
            <el-form size="mini">
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Tipo">
                    <el-select v-model="ruleFormVencimientos.codtipovence" clearable placeholder="Seleccione tipo" style="width: 100%">
                      <el-option
                        v-for="item in optionsTipoVencimiento"
                        :key="item.tvcodigo"
                        :label="item.tvdescripcion"
                        :value="item.tvcodigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                  <el-form-item label="Descripcion">
                    <el-input v-model="ruleFormVencimientos.descripcion"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Fecha">
                    <el-date-picker
                      v-model="ruleFormVencimientos.fechavence"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                  <el-form-item>
                    <el-button @click="addVencimientos" style="margin-top: 1.5rem;">Adicinar</el-button>
                  </el-form-item>
                </el-col>
              </el-row>
            </el-form>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableDataVenciomientos"
                  border
                  size="mini"
                  style="width: 100%"
                  :index="indexMethod2">
                  <el-table-column
                    prop="codtipovence"
                    label="Tipo">
                  </el-table-column>
                  <el-table-column
                    prop="descripcion"
                    label="Descripcion">
                  </el-table-column>
                  <el-table-column
                    prop="fechavence"
                    label="Fecha Vigencia">
                  </el-table-column>
                  <el-table-column header-align="center" align="center" label="Acción" width="70">
                    <template slot-scope="scope">
                      <a href="#" class="delete" @click.stop.prevent="deleteRowVencimiento(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>

          </fieldset>
        </el-tab-pane>

      </el-tabs>

	  </section>

    <!--DIALOGO CONTRATO-->
    <el-dialog title="Nuevo Contrato" :visible.sync="modalVisible" :close-on-click-modal="false" class="window-dialog">
      <el-row :gutter="6">
        <el-form size="mini">
          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Empresa">
              <el-select v-model="ruleFormDatosContrato.co_codempresa" filterable clearable placeholder="Seleccione empresa" style="width: 100%">
                <el-option
                  v-for="(item, index ) in optionsEmpresa"
                  :key="index"
                  :label="item.nombre"
                  :value="item.sw">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
            <el-form-item label="Fecha ingreso">
              <el-date-picker
                v-model="ruleFormDatosContrato.co_fechaing"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
            <el-form-item label="Fecha fin">
              <el-date-picker
                v-model="ruleFormDatosContrato.co_fechafin"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Tipo contrato">
              <el-select v-model="ruleFormDatosContrato.co_tcontrato" clearable placeholder="Seleccione tipo" style="width: 100%">
                <el-option
                  v-for="item in optionsTipoContrato"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Fecha retiro">
              <el-date-picker
                v-model="ruleFormDatosContrato.co_fecharet"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="9" :lg="9" :xl="9">
            <el-form-item label="Motivo retiro">
              <el-select v-model="ruleFormDatosContrato.co_codretiro" filterable clearable placeholder="Seleccione motivo retiro" style="width: 100%">
                <el-option
                  v-for="item in optionsMotivoRetiro"
                  :key="item.mr_codigo"
                  :label="item.mr_motivo"
                  :value="item.mr_codigo">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="9" :lg="9" :xl="9">
            <el-form-item label="Motivo real">
              <el-select filterable clearable placeholder="Seleccione motivo real" style="width: 100%"></el-select>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <span slot="footer" class="dialog-footer">
        <el-button @click="addDatosContrato">Adicionar</el-button>
      </span>
    </el-dialog>

    <!--DIALOG BUSCAR FUNCIONARIO-->
    <el-dialog title="Busqueda de funcionario" :visible.sync="dialogBuscar" :close-on-click-modal="false" class="window-dialog">
      <el-row>
        <el-form @submit.native.prevent="filteredListFuncionario" size="mini">
          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item>
              <el-input v-model ="inputSearch" @keyup.enter.native="filteredListFuncionario">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

          <!-- <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
            <el-form-item>
              <el-button @click="showFuncionarios">Buscar</el-button>
            </el-form-item>
          </el-col> -->
        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataFuncionario"
            border
            height="250"
            @current-change="rowClick"
            size="mini"
            style="width: 100%">
            <el-table-column
              prop="rh_cedula"
              width="100"
              label="Cedula">
            </el-table-column>
            <el-table-column
              prop="full_name"
              label="Nombre y Apellidos">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>

    </el-dialog>

    <!-- DIALOG CIUDADES EXPEDICION -->
    <el-dialog title="Ciudades expedición" :visible.sync="dialogVisibleCiudadesExpedicion" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudExp" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesExpedicion" @keyup.enter.native="filteredListCiudExp" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            refs="materiales"
            @current-change="rowClickCiudadesExpedicion"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG CIUDADES -->
    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudades" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudades" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudades" @keyup.enter.native="filteredListCiudades" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @current-change="rowClickCiudades"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG CIUDADES INFO FAMILIAR1 -->
    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudadesInfoFamiliar1" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudadesInfoFamiliar1" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesInfoFamiliar1" @keyup.enter.native="filteredListCiudadesInfoFamiliar1" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @current-change="rowClickCiudadesInfoFamiliar1"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudadesInfoFamiliar2" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudadesInfoFamiliar2" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesInfoFamiliar2" @keyup.enter.native="filteredListCiudadesInfoFamiliar2" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @current-change="rowClickCiudadesInfoFamiliar2"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG BARRIOS -->
    <el-dialog title="Barrios" :visible.sync="dialogVisibleBarrios" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListBarrios" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchBarrios" @keyup.enter.native="filteredListBarrios" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataBarriosNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @current-change="rowClickBarrios"
            style="width: 100%">
            <el-table-column
              prop="ba_codbarrio"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="ba_nombarrio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

  </div>

  <script>

		jQuery(document).ready(function($) {
			$("#foto").change(function(){
	    	readURL(this);
			});
		});

		function readURL(input) {
	    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgFuncionario').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
	    }
		}

  	ELEMENT.locale(ELEMENT.lang.es);
    new Vue({
      el: '#app',
      data: () => ({
      	ruleForm: {
          rh_cedula: '',
          rh_codexpciud: '',
          rh_expcedula: '',
          rh_apellido1: '',
          rh_apellido2: '',
          rh_nombre: '',
          rh_direccion: '',
          rh_codciudad: '',
          rh_ciudad: '',
          rh_codbarrio: '',
          rh_barrio: '',
          rh_telefono: '',
          rh_movil: '',
          rh_sexo: '',
          rh_dessexo: '',
          rh_facsangre: '',
          rh_rh: '',
          rh_rh1: '',
          rh_desrh: '',
          rh_estcivil: '',
          rh_fechanac: '',
          rh_codciunac: '',
          rh_ciudadnac: '',
          rh_coddptonac: '',
          rh_dptonac: '',
          rh_codnacional: '',
          rh_nacional: '',
          rh_estado: 'INACTIVO',
          rh_foto: '',
          rh_profesion: '',
          rh_explaboral: '',
          rh_nombre1: '',
          rh_nombre2: '',
          rh_edad: '',
          dl_codcentrab: '',
          dl_descentrab: '',
          dl_codcencosto: '',
          dl_descencosto: '',
          dl_salario: '',
          dl_comphoras: '',
          dl_nlibreta: '',
          dl_codclaselib: '',
          dl_desclaselib: '',
          dl_distritomil: '',
          dl_certjudicial: '',
          dl_fechaven: '',
          dl_hosrasdiarias: '',
          dl_salario_letras: '',
          dp_codarp: '',
          dp_desarp: '',
          dp_codeps: '',
          dp_deseps: '',
          dp_afp: '',
          dp_desafp: '',
          dp_codcaja: '',
          dp_descaja: '',
          dp_codbanco: '',
          dp_desbanco: '',
          dp_ncuenta: '',
          dp_convencion: 'N',
          dp_adicionarp: '',
          em_cedula: '',
          em_codingeniero: '',
          em_nomingeniero: '',
          em_programa: '',
          em_desprograma: '',
          em_cuadrilla: '',
          em_codarea: '',
          em_desarea: '',
          em_codcargo: '',
          em_descargo: '',
          em_confianza: '',
          em_codigo: '',
          em_perfil: '',
          em_cel_empresa_num: '',
          em_radio_empresa_num: '',
          em_placa: '',
          ta_camisa: '',
          ta_pantalon: '',
          ta_botas: '',
          ta_tipo: '',
          df_cedula: '',
          co_cedula: '',
          te_codigo: ''
	      },
        ruleFormConte: {
          dc_codconte: '',
          dc_desconte: ''
        },
        ruleFormFAcademica: {
          df_codigo: '',
          df_formacademica: '',
          df_insteducativa: '',
          df_tituloobt: ''
        },
        ruleFormDatosContrato: {
          co_codempresa: '',
          co_empresa: '',
          co_fechaing: '',
          co_fechafin: '',
          co_fecharet: '',
          co_tcontrato: '',
          co_codretiro: '',
          co_item: '',
          co_motivoretiro: '',
          co_codreal: '',
          co_motivoreal: '',
          co_fecharet: '',
          co_observacion: ''
        },
        ruleFormVencimientos: {
          codtipovence: '',
          destipovence: '',
          descripcion: '',
          fechavence: ''
        },
        ruleFormInfoFamiliar: {
      	  if_cedula: '',
      	  if_nombrecony: '',
          if_fechanaccony: '',
          if_ciudad: '',
          if_desciudad: '',
          if_cedulacony: '',
          if_ciudadexp: '',
          if_desciudadexp: '',
          if_profecony: '',
          if_canhijo: '',
          if_sexocony: '',
          if_dessexocony: ''
        },
        ruleFormInfoHijos: {
      	  ih_nombrehijo: '',
          ih_fechanac: '',
          ih_identifica: '',
          ih_sexo: '',
          cantidad: ''
        },
        rules: {
          rh_cedula: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_codexpciud: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_apellido1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_nombre1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_direccion: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_codciudad: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codbarrio: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_telefono: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_movil: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_sexo: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_estcivil: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_rh: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_fechanac: [
            { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_coddptonac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codciunac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codnacional: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcentrab: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcencosto: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ]
        },
        imageUrl: '',
        selected: 0,
        tableData: [],
        tableDataContrato: [],
        tableDataInfoAcademinaca: [],
        tableDataInfoHijos: [],
        tableDataVenciomientos: [],
        tableDataBuscar: [],
        tableDataFuncionario: [],
        tableDataCiudadesExpedicion: [],
        tableDataCiudadesExpedicionNew: [],
        tableDataBarrios: [],
        tableDataBarriosNew: [],
        optionsDpto: [],
        optionsCiudades: [],
        optionsBarrio: [],
        optionsMunicipio: [],
        optionsNacionalidades: [],
        optionscentroTrabajo: [],
        optionscentroCosto: [],
        optionsBanco: [],
        optionsArp: [],
        optionsEps: [],
        optionsAfp: [],
        optionsCaja: [],
        optionsCampanas: [],
        optionsJefeInmediato: [],
        optionsAreas: [],
        optionsCargos: [],
        optionsConte: [],
        optionsTipoEstudio: [],
        optionsCategoriaDotacion: [],
        optionsEmpresa: [],
        optionsMotivoRetiro: [],
        optionsTipoVencimiento: [],
        optionsGenero: [
          {value: 'M', label: 'MASCULINO'},
          {value: 'F', label: 'FEMENINO'}
        ],
        optionsClaseLibreta: [
          {value: '0', label: 'NO APLICA'},
          {value: '1', label: 'PRIMERA CLASE'},
          {value: '2', label: 'SEGUNDA CLASE'}
        ],
        optionsTipoSanguineo: [
          {value: 'O-', label: 'NEGATIVO', fac_sangre: 'O', fac_sangre1: 'O-', rhrh: '-'},
          {value: 'O+', label: 'POSITIVO', fac_sangre: 'O', fac_sangre1: 'O+', rhrh: '+'},
          {value: 'A-', label: 'NEGATIVO', fac_sangre: 'A', fac_sangre1: 'A-', rhrh: '-'},
          {value: 'A+', label: 'POSITIVO', fac_sangre: 'A', fac_sangre1: 'A+', rhrh: '+'},
          {value: 'B-', label: 'NEGATIVO', fac_sangre: 'B', fac_sangre1: 'B-', rhrh: '-'},
          {value: 'B+', label: 'POSITIVO', fac_sangre: 'B', fac_sangre1: 'B+', rhrh: '+'},
          {value: 'AB-', label: 'NEGATIVO', fac_sangre: 'AB', fac_sangre1: 'AB-', rhrh: '-'},
          {value: 'AB+', label: 'POSITIVO', fac_sangre: 'AB', fac_sangre1: 'AB+', rhrh: '+'}
        ],
        optionsEstadoCivil: [
          {value: '0', label: 'Soltero(a)'},
          {value: '1', label: 'Casado(a)'},
          {value: '2', label: 'Viudo(a)'},
          {value: '3', label: 'Divorsiado(a)'},
          {value: '4', label: 'Union Libre'},
        ],
        optionsConfianza: [
          {value: '0', label: 'SI'},
          {value: '1', label: 'NO'}
        ],
        optionsPerfil: [
          {value: '0', label: 'Administrativo'},
          {value: '1', label: 'Operativo'}
        ],
        optionsTipoContrato: [
          {value: '0', label: 'Termino Fijo (< 1 año)'},
          {value: '1', label: 'Termino Fijo (= 1 año)'},
          {value: '2', label: 'Termino Indefinido'},
          {value: '3', label: 'Labor Pactada (D.O.)'}
        ],
        fileList: [],
        sw: false,
        loading: true,
        indexUpdate: null,
        rowDelete: 0,
        modalVisible: false,
        dialogBuscar: false,
        dialogInfoFamiliar: false,
        visibleCreate: true,
        visibleUpdate: false,
        visibleDelete: false,
        dialogVisibleCiudadesExpedicion: false,
        dialogVisibleCiudades: false,
        dialogVisibleBarrios: false,
        dialogVisibleCiudadesInfoFamiliar1: false,
        dialogVisibleCiudadesInfoFamiliar2: false,
        inputSearch: '',
        inputSearchCiudadesExpedicion: '',
        inputSearchCiudades: '',
        inputSearchBarrios: '',
        inputSearchCiudadesInfoFamiliar1: '',
        inputSearchCiudadesInfoFamiliar2: ''
      }),
      methods: {
        addFuncionario () {
          this.$refs[formName].validate((valid) => {
            if (valid) {
              alert('submit!');
            } else {
              console.log('error submit!!');
              return false;
            }
          });
          // this.ruleForm.rh_codciunac === '' && 
          if (this.ruleForm.rh_cedula === '' && this.ruleForm.rh_codexpciud === '' && this.ruleForm.rh_apellido1 === '' && this.ruleForm.rh_nombre1 === '' && this.ruleForm.rh_direccion === '' && this.ruleForm.rh_codciudad === '' && this.ruleForm.rh_codbarrio === '' && this.ruleForm.rh_telefono === '' && this.ruleForm.rh_movil === '' && this.ruleForm.rh_sexo === '' && this.ruleForm.rh_estcivil === '' && this.ruleForm.rh_rh === '' && this.ruleForm.rh_fechanac === '' && this.ruleForm.rh_coddptonac === '' && this.ruleForm.rh_codnacional === '' && this.ruleForm.dl_codcentrab === '' && this.ruleForm.dl_codcencosto === '') {
            this.$alert('Faltan campos por diligenciar.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            // var fileValue = document.querySelector('.el-upload .el-upload__input');
            var fileValue = document.querySelector('.form-control-file');
            var data = new FormData();
            // this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
            // this.ruleForm.rh_expcedula = $('#expedida-en option:selected').text();
            // this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
            // this.ruleForm.rh_ciudad = $('#ciudad option:selected').text();
            // this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
            this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
            this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
            this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
            this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
            this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
            this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
            this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
            this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
            this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
            this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
            this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
            this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
            this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
            this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
            this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
            this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
            this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
            this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
            this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
            this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

            var foto = fileValue.files[0] === undefined ? null : fileValue.files[0];
            // var keys=Object.keys(this.ruleForm);
            // for(i=0;i<keys.length;i++){
            //   data.append(keys[i], this.ruleForm[keys[i]]);
            // }
            // console.log(this.ruleForm)
            // console.log(Object.keys(this.ruleForm));
            //DATOS_RRHH
            data.append('rh_cedula', this.ruleForm.rh_cedula);
            data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
            data.append('rh_expcedula', this.ruleForm.rh_expcedula);
            data.append('rh_apellido1', this.ruleForm.rh_apellido1);
            data.append('rh_apellido2', this.ruleForm.rh_apellido2);
            data.append('rh_nombre', this.ruleForm.rh_nombre);
            data.append('rh_direccion', this.ruleForm.rh_direccion);
            data.append('rh_codciudad', this.ruleForm.rh_codciudad);
            data.append('rh_ciudad', this.ruleForm.rh_ciudad);
            data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
            data.append('rh_barrio', this.ruleForm.rh_barrio);
            data.append('rh_telefono', this.ruleForm.rh_telefono);
            data.append('rh_movil', this.ruleForm.rh_movil);
            data.append('rh_sexo', this.ruleForm.rh_sexo);
            data.append('rh_dessexo', this.ruleForm.rh_dessexo);
            data.append('rh_facsangre', this.ruleForm.rh_facsangre);
            data.append('rh_rh', this.ruleForm.rh_rh1);
            data.append('rh_desrh', this.ruleForm.rh_desrh);
            data.append('rh_estcivil', this.ruleForm.rh_estcivil);
            data.append('rh_fechanac', this.ruleForm.rh_fechanac);
            data.append('rh_codciunac', this.ruleForm.rh_codciunac);
            data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
            data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
            data.append('rh_dptonac', this.ruleForm.rh_dptonac);
            data.append('rh_codnacional', this.ruleForm.rh_codnacional);
            data.append('rh_nacional', this.ruleForm.rh_nacional);
            data.append('rh_estado', this.ruleForm.rh_estado);
            data.append('rh_foto', foto);
            data.append('rh_nombre1', this.ruleForm.rh_nombre1);
            data.append('rh_nombre2', this.ruleForm.rh_nombre2);
            //TECNICOS
            data.append('te_codigo', '');
            data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
            data.append('te_direccion', this.ruleForm.rh_direccion);
            data.append('te_telefono', this.ruleForm.rh_telefono);
            data.append('te_ciudad', this.ruleForm.rh_ciudad);
            data.append('te_cedula', this.ruleForm.rh_cedula);

            data.append('dl_cedula', this.ruleForm.rh_cedula);
            data.append('dl_conte', '');
            data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
            data.append('dl_descentrab', this.ruleForm.dl_descentrab);
            data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
            data.append('dl_descencosto', this.ruleForm.dl_descencosto);
            data.append('dl_salario', this.ruleForm.dl_salario);
            data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
            data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
            data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
            data.append('dl_distritomil', this.ruleForm.dl_distritomil);
            data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
            data.append('dl_fechaven', this.ruleForm.dl_fechaven);
            data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
            data.append('dp_cedula', this.ruleForm.rh_cedula);
            data.append('dp_codarp', this.ruleForm.dp_codarp);
            data.append('dp_desarp', this.ruleForm.dp_desarp);
            data.append('dp_codeps', this.ruleForm.dp_codeps);
            data.append('dp_deseps', this.ruleForm.dp_deseps);
            data.append('dp_afp', this.ruleForm.dp_afp);
            data.append('dp_desafp', this.ruleForm.dp_desafp);
            data.append('dp_codcaja', this.ruleForm.dp_codcaja);
            data.append('dp_descaja', this.ruleForm.dp_descaja);
            data.append('dp_codbanco', this.ruleForm.dp_codbanco);
            data.append('dp_desbanco', this.ruleForm.dp_desbanco);
            data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
            data.append('dp_convencion', this.ruleForm.dp_convencion);
            data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
            //DATOS_EMPRESA
            data.append('em_cedula', this.ruleForm.rh_cedula);
            data.append('em_codingeniero', this.ruleForm.em_codingeniero);
            data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
            data.append('em_programa', this.ruleForm.em_desprograma);
            data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
            data.append('em_codarea', this.ruleForm.em_codarea);
            data.append('em_desarea', this.ruleForm.em_desarea);
            data.append('em_codcargo', this.ruleForm.em_codcargo);
            data.append('em_descargo', this.ruleForm.em_descargo);
            data.append('em_confianza', this.ruleForm.em_confianza);
            data.append('em_perfil', this.ruleForm.em_perfil);
            //DATOS_TALLAS
            data.append('ta_cedula', this.ruleForm.rh_cedula);
            data.append('ta_camisa', this.ruleForm.ta_camisa);
            data.append('ta_pantalon', this.ruleForm.ta_pantalon);
            data.append('ta_botas', this.ruleForm.ta_botas);
            data.append('ta_tipo', this.ruleForm.ta_tipo);

            data.append('dc_cedula', this.ruleForm.rh_cedula);
            data.append('conte', JSON.stringify(this.tableData));
            //DATOS_FORMACADEMICA
            data.append('df_cedula', this.ruleForm.rh_cedula);
            data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));

            data.append('co_cedula', this.ruleForm.rh_cedula);
            data.append('contrato', JSON.stringify(this.tableDataContrato));
            //VENCIMIENTOS
            data.append('cedula', this.ruleForm.rh_cedula);
            data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
            //INFORME_FAMILIAR
            data.append('if_cedula', this.ruleForm.rh_cedula);
            data.append('if_nombrecony', this.ruleFormInfoFamiliar.if_nombrecony);
            data.append('if_fechanaccony', this.ruleFormInfoFamiliar.if_fechanaccony);
            data.append('if_ciudad', this.ruleFormInfoFamiliar.if_ciudad);
            data.append('if_cedulacony', this.ruleFormInfoFamiliar.if_cedulacony);
            data.append('if_ciudadexp', this.ruleFormInfoFamiliar.if_ciudadexp);
            data.append('if_profecony', this.ruleFormInfoFamiliar.if_profecony);
            data.append('if_canhijo', this.ruleFormInfoFamiliar.if_canhijo);
            data.append('if_sexocony', this.ruleFormInfoFamiliar.if_sexocony);
            //INFORME_HIJOS
            data.append('ih_cedula', this.ruleForm.rh_cedula);
            data.append('hijos', JSON.stringify(this.tableDataInfoHijos));
            axios.post('request/insertFuncionario.php', data).then(response => {
              if (response.data.error === 0) {
                this.$notify({
                  title: response.data.title,
                  message: response.data.message,
                  type: response.data.type
                });
                this.clearRH();
                this.clear();
                this.clearFAcademica();
                this.clearDatosContrato();
                this.clearVencimientos();
                this.clearInfoFamiliar();
                this.clearInfoHijos();
                this.tableData = [];
                this.tableDataContrato = [];
                this.tableDataInfoAcademinaca = [];
                this.tableDataInfoHijos = [];
                this.tableDataVenciomientos = [];
              }
            }).catch(e => {
              console.log(e.response);
            });
          }
        },
        onUpdate () {
          var fileValue = document.querySelector('.el-upload .el-upload__input');
          var data = new FormData();

          this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
          this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
          this.ruleForm.rh_barrio = this.valueSelect(this.optionsBarrio, 'ba_codbarrio', 'ba_nombarrio', this.ruleForm.rh_codbarrio);
          this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
          this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
          this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
          this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
          this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
          this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
          this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
          this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
          this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
          this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
          this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
          this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
          this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
          this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
          this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
          this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
          this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
          this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
          this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
          this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

          var foto = fileValue.files[0] === undefined ? null : fileValue.files[0];

          //DATOS_RRHH
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
          data.append('rh_expcedula', this.ruleForm.rh_expcedula);
          data.append('rh_apellido1', this.ruleForm.rh_apellido1);
          data.append('rh_apellido2', this.ruleForm.rh_apellido2);
          data.append('rh_nombre', this.ruleForm.rh_nombre);
          data.append('rh_direccion', this.ruleForm.rh_direccion);
          data.append('rh_codciudad', this.ruleForm.rh_codciudad);
          data.append('rh_ciudad', this.ruleForm.rh_ciudad);
          data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
          data.append('rh_barrio', this.ruleForm.rh_barrio);
          data.append('rh_telefono', this.ruleForm.rh_telefono);
          data.append('rh_movil', this.ruleForm.rh_movil);
          data.append('rh_sexo', this.ruleForm.rh_sexo);
          data.append('rh_dessexo', this.ruleForm.rh_dessexo);
          data.append('rh_facsangre', this.ruleForm.rh_facsangre);
          data.append('rh_rh', this.ruleForm.rh_rh1);
          data.append('rh_desrh', this.ruleForm.rh_desrh);
          data.append('rh_estcivil', this.ruleForm.rh_estcivil);
          data.append('rh_fechanac', this.ruleForm.rh_fechanac);
          data.append('rh_codciunac', this.ruleForm.rh_codciunac);
          data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
          data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
          data.append('rh_dptonac', this.ruleForm.rh_dptonac);
          data.append('rh_codnacional', this.ruleForm.rh_codnacional);
          data.append('rh_nacional', this.ruleForm.rh_nacional);
          data.append('rh_estado', this.ruleForm.rh_estado);
          data.append('rh_foto', foto);
          data.append('rh_nombre1', this.ruleForm.rh_nombre1);
          data.append('rh_nombre2', this.ruleForm.rh_nombre2);
          //TECNICOS
          data.append('te_codigo', this.ruleForm.te_codigo);
          data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
          data.append('te_direccion', this.ruleForm.rh_direccion);
          data.append('te_telefono', this.ruleForm.rh_telefono);
          data.append('te_ciudad', this.ruleForm.rh_ciudad);
          data.append('te_cedula', this.ruleForm.rh_cedula);
          //DATOS_LEGALES
          data.append('dl_cedula', this.ruleForm.rh_cedula);
          data.append('dl_conte', '');
          data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
          data.append('dl_descentrab', this.ruleForm.dl_descentrab);
          data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
          data.append('dl_descencosto', this.ruleForm.dl_descencosto);
          data.append('dl_salario', this.ruleForm.dl_salario);
          data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
          data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
          data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
          data.append('dl_distritomil', this.ruleForm.dl_distritomil);
          data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
          data.append('dl_fechaven', this.ruleForm.dl_fechaven);
          data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
          //DATOS_PARAFISCALES
          data.append('dp_cedula', this.ruleForm.rh_cedula);
          data.append('dp_codarp', this.ruleForm.dp_codarp);
          data.append('dp_desarp', this.ruleForm.dp_desarp);
          data.append('dp_codeps', this.ruleForm.dp_codeps);
          data.append('dp_deseps', this.ruleForm.dp_deseps);
          data.append('dp_afp', this.ruleForm.dp_afp);
          data.append('dp_desafp', this.ruleForm.dp_desafp);
          data.append('dp_codcaja', this.ruleForm.dp_codcaja);
          data.append('dp_descaja', this.ruleForm.dp_descaja);
          data.append('dp_codbanco', this.ruleForm.dp_codbanco);
          data.append('dp_desbanco', this.ruleForm.dp_desbanco);
          data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
          data.append('dp_convencion', this.ruleForm.dp_convencion);
          data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
          //DATOS_EMPRESA
          data.append('em_cedula', this.ruleForm.rh_cedula);
          data.append('em_codingeniero', this.ruleForm.em_codingeniero);
          data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
          data.append('em_programa', this.ruleForm.em_desprograma);
          data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
          data.append('em_codarea', this.ruleForm.em_codarea);
          data.append('em_desarea', this.ruleForm.em_desarea);
          data.append('em_codcargo', this.ruleForm.em_codcargo);
          data.append('em_descargo', this.ruleForm.em_descargo);
          data.append('em_confianza', this.ruleForm.em_confianza);
          data.append('em_perfil', this.ruleForm.em_perfil);
          //DATOS_TALLAS
          data.append('ta_cedula', this.ruleForm.rh_cedula);
          data.append('ta_camisa', this.ruleForm.ta_camisa);
          data.append('ta_pantalon', this.ruleForm.ta_pantalon);
          data.append('ta_botas', this.ruleForm.ta_botas);
          data.append('ta_tipo', this.ruleForm.ta_tipo);
          //DATOS_CONTE
          data.append('dc_cedula', this.ruleForm.rh_cedula);
          data.append('conte', JSON.stringify(this.tableData));
          //DATOS_FORMACADEMICA
          data.append('df_cedula', this.ruleForm.rh_cedula);
          data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));
          //INFORME_FAMILIAR
          data.append('if_cedula', this.ruleForm.rh_cedula);
          data.append('if_nombrecony', this.ruleFormInfoFamiliar.if_nombrecony);
          data.append('if_fechanaccony', this.ruleFormInfoFamiliar.if_fechanaccony);
          data.append('if_ciudad', this.ruleFormInfoFamiliar.if_ciudad);
          data.append('if_cedulacony', this.ruleFormInfoFamiliar.if_cedulacony);
          data.append('if_ciudadexp', this.ruleFormInfoFamiliar.if_ciudadexp);
          data.append('if_profecony', this.ruleFormInfoFamiliar.if_profecony);
          // data.append('if_canhijo', this.ruleFormInfoFamiliar.if_canhijo);
          data.append('if_sexocony', this.ruleFormInfoFamiliar.if_sexocony);
          //INFORME_HIJOS
          data.append('ih_cedula', this.ruleForm.rh_cedula);
          data.append('hijos', JSON.stringify(this.tableDataInfoHijos));
          //VENCIMIENTOS
          data.append('cedula', this.ruleForm.rh_cedula);
          data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
          axios.post('request/updateFuncionario.php', data).then(response => {
            console.log(response.data.message);
            if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
              this.visibleCreate = true;
              this.visibleUpdate = false;
              this.clearRH();
              this.clear();
              this.clearFAcademica();
              this.clearDatosContrato();
              this.clearVencimientos();
              this.clearInfoFamiliar();
              this.clearInfoHijos();
              this.tableData = [];
              this.tableDataContrato = [];
              this.tableDataInfoAcademinaca = [];
              this.tableDataInfoHijos = [];
              this.tableDataVenciomientos = [];
            }
          }).catch(e => {
            console.log(e.response);
          });
        },
        onDelete () {
          // this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigo', 'mu_municipio', this.ruleForm.rh_codexpciud);
          console.log(this.ruleForm.rh_codexpciud);
          // var data = new FormData();
          // data.append('dc_codconte', this.ruleFormConte.dc_codconte);
          // axios.post('request/deleteFuncionario.php', data).then(response => {
          //   console.log(response.data.message);
          //   // if (response.data.error === 0) {
          //   //   this.$notify({
          //   //     title: response.data.title,
          //   //     message: response.data.message,
          //   //     type: response.data.type
          //   //   });
          //   // }
          // }).catch(e => {
          //   console.log(e.response);
          // });
        },
        showCiudades () {
          this.loading = true
          axios.get('request/ciudades.php').then(response => {
            this.tableDataCiudadesExpedicionNew = response.data;
            this.tableDataCiudadesExpedicion = response.data;
          }).then(response => {
            this.showDepartamentos();
            this.loading = false
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showDepartamentos () {
          this.loading = true
          axios.get('request/departamentos.php').then(response => {
            this.optionsDpto = response.data;
            this.loading = false
          }).then(response => {
            this.showNacionalidades();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showMunicipios () {
          var mu_depto = new FormData();
          mu_depto.append('mu_depto', this.ruleForm.rh_coddptonac);
          axios.post('request/municipios.php', mu_depto).then(response => {
            this.tableDataBarrios = response.data;
          }).catch(error => {
            console.log(error.response);
          })
        },
        showBarrios (id) {
          var ba_mpio = new FormData();
          // this.ruleForm.rh_codciudad = $('#ciudad').val();.substring(2, 5)
          ba_mpio.append('ba_mpio', id);
          axios.post('request/barrios.php', ba_mpio).then(response => {
            this.tableDataBarriosNew = response.data;
            this.tableDataBarrios = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showNacionalidades () {
          this.loading = true
          axios.get('request/nacionalidades.php').then(response => {
            this.optionsNacionalidades = response.data;
            this.loading = false
          }).then(response => {
            this.showCentroTrabajo();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCentroTrabajo () {
          this.loading = true
          axios.get('request/centroTrabajo.php').then(response => {
            this.optionscentroTrabajo = response.data;
            this.loading = false
          }).then(response => {
            this.showCentroCosto();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCentroCosto () {
          this.loading = true
          axios.get('request/centroCosto.php').then(response => {
            this.optionscentroCosto = response.data;
            this.loading = false
          }).then(response => {
            this.showARP();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showARP () {
          this.loading = true
          axios.get('request/arp.php').then(response => {
            this.optionsArp = response.data;
            this.loading = false
          }).then(response => {
            this.showEPS();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showEPS () {
          this.loading = true
          axios.get('request/eps.php').then(response => {
            this.optionsEps = response.data;
            this.loading = false
          }).then(response => {
            this.showAFP();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showAFP () {
          this.loading = true
          axios.get('request/afp.php').then(response => {
            this.optionsAfp = response.data;
            this.loading = false
          }).then(response => {
            this.showCaja();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCaja () {
          this.loading = true
          axios.get('request/caja.php').then(response => {
            this.optionsCaja = response.data;
          }).then(response => {
            this.loading = false
            this.showBancos();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showBancos () {
          this.loading = true
          axios.get('request/bancos.php').then(response => {
            this.optionsBanco = response.data;
          }).then(response => {
            this.loading = false
            this.showJefeInmediato();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showJefeInmediato () {
          this.loading = true
          axios.get('request/datos_rrhh.php').then(response => {
            this.optionsJefeInmediato = response.data;
          }).then(response => {
            this.loading = false
            this.showCampanas();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCampanas () {
          this.loading = true
          axios.get('request/campanas.php').then(response => {
            this.optionsCampanas = response.data;
          }).then(response => {
            this.loading = false
            this.showAreas();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showAreas () {
          this.loading = true
          axios.get('request/areas.php').then(response => {
            this.optionsAreas = response.data;
          }).then(response => {
            this.loading = false
            this.showCargos();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCargos () {
          this.loading = true
          axios.get('request/cargos.php').then(response => {
            this.optionsCargos = response.data;
          }).then(response => {
            this.loading = false
            this.showConte();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showConte () {
          this.loading = true
          axios.get('request/conte.php').then(response => {
            this.optionsConte = response.data;
          }).then(response => {
            this.loading = false
            this.showTipoEstudio();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showTipoEstudio () {
          this.loading = true
          axios.get('request/tipo_estudio.php').then(response => {
            this.optionsTipoEstudio = response.data;
          }).then(response => {
            this.loading = false
            this.showCategoriaDotacion();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showCategoriaDotacion () {
          this.loading = true
          axios.get('request/categoria_dotacion.php').then(response => {
            this.optionsCategoriaDotacion = response.data;
          }).then(response => {
            this.loading = false
            this.showEmpresa();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showEmpresa () {
          this.loading = true
          axios.get('request/empresa.php').then(response => {
            this.optionsEmpresa = response.data;
          }).then(response => {
            this.loading = false
            this.showMotivoRetiro();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showMotivoRetiro () {
          this.loading = true
          axios.get('request/motivo_retiro.php').then(response => {
            this.optionsMotivoRetiro = response.data;
          }).then(response => {
            this.loading = false
            this.showTipoVencimiento();
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showTipoVencimiento () {
          this.loading = true
          axios.get('request/tipo_vencimiento.php').then(response => {
            this.optionsTipoVencimiento = response.data;
            this.loading = false
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        showFuncionarios () {
          this.loading = true
          // var data = new FormData();
          // data.append('funcionario', this.inputSearch);
          axios.get('request/getFuncionario.php').then(response => {
            this.tableDataFuncionario = response.data;
            this.tableDataBuscar = response.data;
            this.loading = false
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        addDatosConte () {
          this.ruleFormConte.dc_desconte = this.valueSelect(this.optionsConte, 'co_codigo', 'co_nombre', this.ruleFormConte.dc_codconte);
          var data = this.ruleFormConte;
          if (this.ruleFormConte.dc_codconte === '') {
            this.$alert('Por favor diligenciar campo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableData.push(data);
            this.clear();
          }
        },
        deleteRow (row, index) {
          console.log(row.dc_codconte)
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            if (this.rowDelete === 0) {
              this.$delete(this.tableData, index);
              this.$message({
                type: 'success',
                message: 'Registro eliminado con exito.'
              });
            } else {
              var data = new FormData();
              data.append('dc_codconte', row.dc_codconte);
              axios.post('request/deleteDatosConte.php', data).then(response => {
                this.$message({
                  type: 'success',
                  message: 'Registro eliminado con exito..!'
                });
              }).catch(e => {
                console.log(e.response);
              });
              this.$delete(this.tableData, index);
            }
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addFormacionAcademica () {
          this.ruleFormFAcademica.df_formacademica = this.valueSelect(this.optionsTipoEstudio, 'es_codigo', 'es_nombre', this.ruleFormFAcademica.df_codigo);
          var data = this.ruleFormFAcademica;
          if (this.ruleFormFAcademica.df_codigo === '') {
            this.$alert('Por favor diligenciar campo tipo estudio.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormFAcademica.df_insteducativa === '') {
            this.$alert('Por favor diligenciar campo Ins. Educativa.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormFAcademica.df_tituloobt === '') {
            this.$alert('Por favor diligenciar campo titulo obtenido.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableDataInfoAcademinaca.push(data);
            this.clearFAcademica();
          }
        },
        deleteRowFormacademica (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataInfoAcademinaca, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addDatosContrato () {
          this.ruleFormDatosContrato.co_empresa = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_motivoretiro = this.valueSelect(this.optionsMotivoRetiro, 'mr_codigo', 'mr_motivo', this.ruleFormDatosContrato.co_codretiro);
          // this.ruleFormDatosContrato.co_motivoreal = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_item = this.tableDataContrato.length + 1;
          var data = this.ruleFormDatosContrato;
          if (!this.sw) {
            if (this.ruleFormDatosContrato.co_codempresa === '') {
              this.$alert('Por favor diligenciar campo empresa.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else if (this.ruleFormDatosContrato.co_fechaing === '') {
              this.$alert('Por favor diligenciar campo fecha ingreso.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else if (this.ruleFormDatosContrato.co_fechafin === '') {
              this.$alert('Por favor diligenciar campo fecha fin.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else if (this.ruleFormDatosContrato.co_tcontrato === '') {
              this.$alert('Por favor diligenciar campo tipo contrato.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else {
              this.tableDataContrato.push(data);
              this.clearDatosContrato();
            }
          } else {
            this.tableDataContrato.splice(this.indexUpdate, 1, data);
            this.sw = false;
            this.clearDatosContrato();
          }
        },
        editRowContrato (row, index) {
          console.log(row)
          this.ruleFormDatosContrato.co_codempresa = row.co_codempresa;
          this.ruleFormDatosContrato.co_empresa = row.co_empresa;
          this.ruleFormDatosContrato.co_fechaing = row.co_fechaing;
          this.ruleFormDatosContrato.co_fechafin = row.co_fechafin;
          this.ruleFormDatosContrato.co_codretiro = row.co_codretiro;
          this.ruleFormDatosContrato.co_motivoretiro = row.co_motivoretiro;
          this.ruleFormDatosContrato.co_tcontrato = row.co_tcontrato;
          var data = this.ruleFormDatosContrato;
          this.indexUpdate = index;
          this.modalVisible = true;
          this.sw = 1;
        },
        deleteRowContrato (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataContrato, index);
            this.sw = false;
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addVencimientos () {
          // this.ruleFormVencimientos.destipovence = this.valueSelect(this.optionsTipoVencimiento, 'tvcodigo', 'tvdescripcion', this.ruleFormVencimientos.codtipovence);
          var data = this.ruleFormVencimientos;
          if (this.ruleFormVencimientos.codtipovence === '') {
            this.$alert('Por favor diligenciar campo tipo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormVencimientos.descripcion === '') {
            this.$alert('Por favor diligenciar campo descripción.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormVencimientos.fechavence === '') {
            this.$alert('Por favor diligenciar campo fecha.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableDataVenciomientos.push(data);
            this.clearVencimientos();
          }
        },
        deleteRowVencimiento (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataVenciomientos, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addInfoFamiliar () {
          var dataHijos = this.ruleFormInfoHijos;
          if (this.ruleFormInfoHijos.ih_nombrehijo === '') {
            this.$alert('Por favor diligenciar campo nombre del hijo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_fechanac === '') {
            this.$alert('Por favor diligenciar campo fecha nac.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_identifica === '') {
            this.$alert('Por favor diligenciar campo d.i / reg civil.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_sexo === '') {
            this.$alert('Por favor diligenciar campo sexo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableDataInfoHijos.push(dataHijos);
            this.clearInfoHijos();
          }
        },
        deleteRowInfoHijos (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataInfoHijos, index);
            var cant = this.ruleFormInfoHijos.cantidad;
            cant = this.tableDataInfoHijos.length - 1;
            this.ruleFormInfoFamiliar.if_canhijo = cant;
            console.log(cant)
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        rowClick (row) {
        	console.log(row)
          this.ruleForm.rh_cedula = row.rh_cedula
          this.ruleForm.rh_codexpciud = row.rh_codexpciud
          this.ruleForm.rh_expcedula = row.rh_expcedula
          this.ruleForm.rh_apellido1 = row.rh_apellido1
          this.ruleForm.rh_apellido2 = row.rh_apellido2
          this.ruleForm.rh_nombre1 = row.rh_nombre1
          this.ruleForm.rh_nombre2 = row.rh_nombre2
          this.ruleForm.rh_direccion = row.rh_direccion
          this.ruleForm.rh_codciudad = row.rh_codciudad
          this.ruleForm.rh_ciudad = row.rh_ciudad
          this.ruleForm.rh_codbarrio = row.rh_codbarrio
          this.ruleForm.rh_barrio = row.rh_barrio
          this.showBarrios();
          this.ruleForm.rh_telefono = row.rh_telefono
          this.ruleForm.rh_movil = row.rh_movil
          this.ruleForm.rh_sexo = row.rh_sexo
          this.ruleForm.rh_dessexo = row.rh_dessexo
          this.ruleForm.rh_facsangre = row.rh_facsangre
          this.ruleForm.rh_rh = row.tipo_sangre
          this.ruleForm.rh_desrh = row.rh_desrh
          this.ruleForm.rh_estcivil = row.rh_estcivil
          this.ruleForm.rh_fechanac = row.rh_fechanac
          this.ruleForm.rh_coddptonac = row.rh_coddptonac
          this.ruleForm.rh_dptonac = row.rh_dptonac
          this.ruleForm.rh_codciunac = row.rh_codciudnac
          this.ruleForm.rh_ciudadnac = row.rh_ciudadnac
          this.ruleForm.rh_codnacional = row.rh_codnacional
          this.ruleForm.rh_nacional = row.rh_nacional
          this.showMunicipios();
          this.ruleForm.te_codigo = row.te_codigo
          this.ruleForm.dl_codcentrab = row.dl_codcentrab
          this.ruleForm.dl_descentrab = row.dl_descentrab
          this.ruleForm.dl_codcencosto = row.dl_codcencosto
          this.ruleForm.dl_descencosto = row.dl_descencosto
          this.ruleForm.dl_salario = row.dl_salario
          this.ruleForm.dl_nlibreta = row.dl_nlibreta
          this.ruleForm.dl_codclaselib = row.dl_codclaselib
          this.ruleForm.dl_desclaselib = row.dl_desclaselib
          this.ruleForm.dl_distritomil = row.dl_distritomil
          this.ruleForm.dl_certjudicial = row.dl_certjudicial
          this.ruleForm.dl_fechaven = row.dl_fechaven
          this.ruleForm.dl_hosrasdiarias = row.dl_horasdiarias
          this.ruleForm.dp_codarp = row.dp_codarp
          this.ruleForm.dp_desarp = row.dp_desarp
          this.ruleForm.dp_codeps = row.dp_codeps
          this.ruleForm.dp_deseps = row.dp_deseps
          this.ruleForm.dp_afp = row.dp_codafp
          this.ruleForm.dp_desafp = row.dp_desafp
          this.ruleForm.dp_codcaja = row.dp_codcaja
          this.ruleForm.dp_descaja = row.dp_descaja
          this.ruleForm.dp_codbanco = row.dp_codbanco
          this.ruleForm.dp_desbanco = row.dp_desbanco
          this.ruleForm.dp_ncuenta = row.dp_ncuenta
          this.ruleForm.dp_adicionarp = row.dp_adicionarp
          this.ruleForm.em_codingeniero = row.em_codingeniero
          this.ruleForm.em_nomingeniero = row.em_nomingeniero
          this.ruleForm.em_programa = row.em_programa
          this.ruleForm.em_codarea = row.em_codarea
          this.ruleForm.em_desarea = row.em_desarea
          this.ruleForm.em_codcargo = row.em_codcargo
          this.ruleForm.em_descargo = row.em_descargo
          this.ruleForm.em_codigo = row.em_codigo
          this.ruleForm.em_confianza = row.em_confianza
          this.ruleForm.em_perfil = row.em_perfil
          this.ruleForm.ta_camisa = row.ta_camisa
          this.ruleForm.ta_pantalon = row.ta_pantalon
          this.ruleForm.ta_botas = row.ta_botas
          this.ruleForm.ta_tipo = row.ta_tipo
          this.ruleFormInfoFamiliar.if_cedula = row.if_cedula
          this.ruleFormInfoFamiliar.if_nombrecony = row.if_nombrecony
          this.ruleFormInfoFamiliar.if_fechanaccony = row.if_fechanaccony
          this.ruleFormInfoFamiliar.if_ciudad = row.if_ciudad
          this.ruleFormInfoFamiliar.if_desciudad = row.if_desciudad
          this.ruleFormInfoFamiliar.if_cedulacony = row.if_cedulacony
          this.ruleFormInfoFamiliar.if_ciudadexp = row.if_ciudadexp
          this.ruleFormInfoFamiliar.if_desciudadexp = row.if_desciudadexp
          this.ruleFormInfoFamiliar.if_profecony = row.if_profecony
          this.ruleFormInfoFamiliar.if_canhijo = row.if_canhijo
          this.ruleFormInfoFamiliar.if_sexocony = row.if_sexocony
          this.ruleFormInfoFamiliar.if_dessexocony = row.if_dessexocony
          this.getDatosConte(row.rh_cedula);
          this.getDatosContrato(row.rh_cedula);
          this.getDatosFormacademica(row.rh_cedula);
          // this.getInfoFamiliar(row.rh_cedula);
          this.getInfoHijos(row.rh_cedula);
          this.getVencimientos(row.rh_cedula);
          if (row.rh_foto !== '') {
            $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+row.rh_cedula);
          } else {
            $('#imgFuncionario').attr('src', "img/avatar.jpg");
          }
          this.rowDelete = 1;
          this.dialogBuscar = false;
          this.visibleUpdate = true;
          this.visibleCreate = false;
        },
        rowClickCiudadesExpedicion (row) {
          this.ruleForm.rh_codexpciud = row.mu_codigo
          this.ruleForm.rh_expcedula = row.mu_municipio
          this.dialogVisibleCiudadesExpedicion = false;
        },
        rowClickCiudades (row) {
          this.ruleForm.rh_codciudad = row.mu_codigo
          this.ruleForm.rh_ciudad = row.mu_municipio
          this.showBarrios(row.cod);
          this.dialogVisibleCiudades = false;
        },
        rowClickCiudadesInfoFamiliar1 (row) {
          this.ruleFormInfoFamiliar.if_ciudadexp = row.mu_codigo
          this.ruleFormInfoFamiliar.if_desciudadexp = row.mu_municipio
          this.dialogVisibleCiudadesInfoFamiliar1 = false;
        },
        rowClickCiudadesInfoFamiliar2 (row) {
          this.ruleFormInfoFamiliar.if_ciudad = row.mu_codigo
          this.ruleFormInfoFamiliar.if_desciudad = row.mu_municipio
          this.dialogVisibleCiudadesInfoFamiliar2 = false;
        },
        rowClickBarrios (row) {
          this.ruleForm.rh_codbarrio = row.ba_codbarrio
          this.ruleForm.rh_barrio = row.ba_nombarrio
          this.dialogVisibleBarrios = false;
        },
        getFuncionarioBY () {
          var data = new FormData();
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          axios.post('request/getFuncionarioBY.php', data).then(response => {
            console.log(response.data[0])
            this.ruleForm.rh_cedula = response.data[0].rh_cedula
            this.ruleForm.rh_codexpciud = response.data[0].rh_codexpciud
            this.ruleForm.rh_expcedula = response.data[0].rh_expcedula
            this.ruleForm.rh_apellido1 = response.data[0].rh_apellido1
            this.ruleForm.rh_apellido2 = response.data[0].rh_apellido2
            this.ruleForm.rh_nombre1 = response.data[0].rh_nombre1
            this.ruleForm.rh_nombre2 = response.data[0].rh_nombre2
            this.ruleForm.rh_direccion = response.data[0].rh_direccion
            this.ruleForm.rh_codciudad = response.data[0].rh_codciudad
            this.ruleForm.rh_ciudad = response.data[0].rh_ciudad
            this.ruleForm.rh_codbarrio = response.data[0].rh_codbarrio
            this.ruleForm.rh_barrio = response.data[0].rh_barrio
            this.showBarrios();
            this.ruleForm.rh_telefono = response.data[0].rh_telefono
            this.ruleForm.rh_movil = response.data[0].rh_movil
            this.ruleForm.rh_sexo = response.data[0].rh_sexo
            this.ruleForm.rh_dessexo = response.data[0].rh_dessexo
            this.ruleForm.rh_facsangre = response.data[0].rh_facsangre
            this.ruleForm.rh_rh = response.data[0].tipo_sangre
            this.ruleForm.rh_desrh = response.data[0].rh_desrh
            this.ruleForm.rh_estcivil = response.data[0].rh_estcivil
            this.ruleForm.rh_fechanac = response.data[0].rh_fechanac
            this.ruleForm.rh_coddptonac = response.data[0].rh_coddptonac
            this.ruleForm.rh_dptonac = response.data[0].rh_dptonac
            this.ruleForm.rh_codciunac = response.data[0].rh_codciudnac
            this.ruleForm.rh_ciudadnac = response.data[0].rh_ciudadnac
            this.ruleForm.rh_codnacional = response.data[0].rh_codnacional
            this.ruleForm.rh_nacional = response.data[0].rh_nacional
            this.showMunicipios();
            this.ruleForm.te_codigo = response.data[0].te_codigo
            this.ruleForm.dl_codcentrab = response.data[0].dl_codcentrab
            this.ruleForm.dl_descentrab = response.data[0].dl_descentrab
            this.ruleForm.dl_codcencosto = response.data[0].dl_codcencosto
            this.ruleForm.dl_descencosto = response.data[0].dl_descencosto
            this.ruleForm.dl_salario = response.data[0].dl_salario
            this.ruleForm.dl_nlibreta = response.data[0].dl_nlibreta
            this.ruleForm.dl_codclaselib = response.data[0].dl_codclaselib
            this.ruleForm.dl_desclaselib = response.data[0].dl_desclaselib
            this.ruleForm.dl_distritomil = response.data[0].dl_distritomil
            this.ruleForm.dl_certjudicial = response.data[0].dl_certjudicial
            this.ruleForm.dl_fechaven = response.data[0].dl_fechaven
            this.ruleForm.dl_hosrasdiarias = response.data[0].dl_horasdiarias
            this.ruleForm.dp_codarp = response.data[0].dp_codarp
            this.ruleForm.dp_desarp = response.data[0].dp_desarp
            this.ruleForm.dp_codeps = response.data[0].dp_codeps
            this.ruleForm.dp_deseps = response.data[0].dp_deseps
            this.ruleForm.dp_afp = response.data[0].dp_codafp
            this.ruleForm.dp_desafp = response.data[0].dp_desafp
            this.ruleForm.dp_codcaja = response.data[0].dp_codcaja
            this.ruleForm.dp_descaja = response.data[0].dp_descaja
            this.ruleForm.dp_codbanco = response.data[0].dp_codbanco
            this.ruleForm.dp_desbanco = response.data[0].dp_desbanco
            this.ruleForm.dp_ncuenta = response.data[0].dp_ncuenta
            this.ruleForm.dp_adicionarp = response.data[0].dp_adicionarp
            this.ruleForm.em_codingeniero = response.data[0].em_codingeniero
            this.ruleForm.em_nomingeniero = response.data[0].em_nomingeniero
            this.ruleForm.em_programa = response.data[0].em_programa
            this.ruleForm.em_codarea = response.data[0].em_codarea
            this.ruleForm.em_desarea = response.data[0].em_desarea
            this.ruleForm.em_codcargo = response.data[0].em_codcargo
            this.ruleForm.em_descargo = response.data[0].em_descargo
            this.ruleForm.em_codigo = response.data[0].em_codigo
            this.ruleForm.em_confianza = response.data[0].em_confianza
            this.ruleForm.em_perfil = response.data[0].em_perfil
            this.ruleForm.ta_camisa = response.data[0].ta_camisa
            this.ruleForm.ta_pantalon = response.data[0].ta_pantalon
            this.ruleForm.ta_botas = response.data[0].ta_botas
            this.ruleForm.ta_tipo = response.data[0].ta_tipo
            this.ruleFormInfoFamiliar.if_cedula = response.data[0].if_cedula
            this.ruleFormInfoFamiliar.if_nombrecony = response.data[0].if_nombrecony
            this.ruleFormInfoFamiliar.if_fechanaccony = response.data[0].if_fechanaccony
            this.ruleFormInfoFamiliar.if_ciudad = response.data[0].if_ciudad
            this.ruleFormInfoFamiliar.if_desciudad = response.data[0].if_desciudad
            this.ruleFormInfoFamiliar.if_cedulacony = response.data[0].if_cedulacony
            this.ruleFormInfoFamiliar.if_ciudadexp = response.data[0].if_ciudadexp
            this.ruleFormInfoFamiliar.if_desciudadexp = response.data[0].if_desciudadexp
            this.ruleFormInfoFamiliar.if_profecony = response.data[0].if_profecony
            this.ruleFormInfoFamiliar.if_canhijo = response.data[0].if_canhijo
            this.ruleFormInfoFamiliar.if_sexocony = response.data[0].if_sexocony
            this.ruleFormInfoFamiliar.if_dessexocony = response.data[0].if_dessexocony
            this.getDatosConte(this.ruleForm.rh_cedula);
            this.getDatosContrato(this.ruleForm.rh_cedula);
            this.getDatosFormacademica(this.ruleForm.rh_cedula);
            // this.getInfoFamiliar(row.rh_cedula);
            this.getInfoHijos(this.ruleForm.rh_cedula);
            this.getVencimientos(this.ruleForm.rh_cedula);
            if (response.data[0].rh_foto !== '') {
              $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+this.ruleForm.rh_cedula);
            } else {
              $('#imgFuncionario').attr('src', "img/avatar.jpg");
            }
            // $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+this.ruleForm.rh_cedula);
            this.rowDelete = 1;
            this.visibleUpdate = true;
            this.visibleCreate = false;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosConte (dc_cedula) {
          var data = new FormData();
          data.append('dc_cedula', dc_cedula);
          axios.post('request/getDatosConte.php', data).then(response => {
            this.tableData = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosContrato (co_cedula) {
          var data = new FormData();
          data.append('co_cedula', co_cedula);
          axios.post('request/getDatosContrato.php', data).then(response => {
            this.tableDataContrato = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosFormacademica (df_cedula) {
          var data = new FormData();
          data.append('df_cedula', df_cedula);
          axios.post('request/getDatosFormacademica.php', data).then(response => {
            this.tableDataInfoAcademinaca = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getInfoHijos (ih_cedula) {
          var data = new FormData();
          data.append('ih_cedula', ih_cedula);
          axios.post('request/getInfoHijos.php', data).then(response => {
            this.tableDataInfoHijos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getVencimientos (cedula) {
          var data = new FormData();
          data.append('cedula', cedula);
          axios.post('request/getVencimientos.php', data).then(response => {
            this.tableDataVenciomientos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        handleChange (file, fileList1) {
          this.fileList = fileList1
        },
        handlePreview (file) {
          // this.fileList = file
          // console.log(file)
        },
        valueSelect (data, value, label, dato) {
          let labelReturn = '';
          data.forEach((item, index, arr) => {
            if (dato === item[value]) {
              labelReturn = item[label];
              // arr.length = 0
              return false;
            }
          });
          return labelReturn;
        },
        indexMethod (index) {
          return index;
        },
        indexMethod1 (index) {
          return index + 1;
        },
        indexMethod2 (index) {
          return index;
        },
        indexMethod3 (index) {
          return index;
        },
        indexMethod4 (index) {
          return index + 1;
        },
        indexMethod5 (index) {
          return index;
        },
        clear () {
          this.ruleFormConte = {
            dc_codconte: '',
            dc_desconte: ''
          }
        },
        clearFAcademica () {
          this.ruleFormFAcademica = {
            df_codigo: '',
            df_formacademica: '',
            df_insteducativa: '',
            df_tituloobt: ''
          }
        },
        clearDatosContrato () {
          this.ruleFormDatosContrato = {
            co_codempresa: '',
            co_empresa: '',
            co_fechaing: '',
            co_fechafin: '',
            co_tcontrato: '',
            co_codretiro: '',
            co_motivoretiro: '',
            co_codreal: '',
            co_motivoreal: '',
            co_fecharet: '',
            co_observacion: ''
          }
        },
        clearVencimientos () {
          this.ruleFormVencimientos = {
            codtipovence: '',
            descripcion: '',
            fechavence: ''
          }
        },
        clearInfoFamiliar () {
          this.ruleFormInfoFamiliar = {
            if_cedula: '',
            if_nombrecony: '',
            if_fechanaccony: '',
            if_ciudad: '',
            if_desciudad: '',
            if_cedulacony: '',
            if_ciudadexp: '',
            if_desciudadexp: '',
            if_profecony: '',
            if_canhijo: '',
            if_sexocony: '',
            if_dessexocony: ''
          }
        },
        clearInfoHijos () {
          this.ruleFormInfoHijos = {
            ih_nombrehijo: '',
            ih_fechanac: '',
            ih_identifica: '',
            ih_sexo: '',
            cantidad: ''
          }
        },
        clearRH () {
          this.ruleForm = {
            rh_cedula: '',
            rh_codexpciud: '',
            rh_expcedula: '',
            rh_apellido1: '',
            rh_apellido2: '',
            rh_nombre: '',
            rh_direccion: '',
            rh_codciudad: '',
            rh_ciudad: '',
            rh_codbarrio: '',
            rh_barrio: '',
            rh_telefono: '',
            rh_movil: '',
            rh_sexo: '',
            rh_dessexo: '',
            rh_facsangre: '',
            rh_rh: '',
            rh_rh1: '',
            rh_desrh: '',
            rh_estcivil: '',
            rh_fechanac: '',
            rh_codciunac: '',
            rh_ciudadnac: '',
            rh_coddptonac: '',
            rh_dptonac: '',
            rh_codnacional: '',
            rh_nacional: '',
            rh_estado: 'INACTIVO',
            rh_foto: '',
            rh_profesion: '',
            rh_explaboral: '',
            rh_nombre1: '',
            rh_nombre2: '',
            rh_edad: '',
            dl_codcentrab: '',
            dl_descentrab: '',
            dl_codcencosto: '',
            dl_descencosto: '',
            dl_salario: '',
            dl_nlibreta: '',
            dl_codclaselib: '',
            dl_desclaselib: '',
            dl_distritomil: '',
            dl_certjudicial: '',
            dl_fechaven: '',
            dl_hosrasdiarias: '',
            dp_codarp: '',
            dp_desarp: '',
            dp_codeps: '',
            dp_deseps: '',
            dp_afp: '',
            dp_desafp: '',
            dp_codcaja: '',
            dp_descaja: '',
            dp_codbanco: '',
            dp_desbanco: '',
            dp_ncuenta: '',
            dp_convencion: 'N',
            dp_adicionarp: '',
            em_cedula: '',
            em_codingeniero: '',
            em_nomingeniero: '',
            em_programa: '',
            em_desprograma: '',
            em_cuadrilla: '',
            em_codarea: '',
            em_desarea: '',
            em_codcargo: '',
            em_descargo: '',
            em_confianza: '',
            em_codigo: '',
            em_perfil: '',
            ta_camisa: '',
            ta_pantalon: '',
            ta_botas: '',
            ta_tipo: '',
            df_cedula: '',
            co_cedula: '',
            te_codigo: ''
          }
        },
        inLetter () {
          this.ruleForm.dl_salario_letras = numeroALetras(this.ruleForm.dl_salario);
        },
        isEmpty (obj) {
          for (var key in obj) {
            console.log(key)
            if (obj.hasOwnProperty(key)) {
              return false;
            }
          }
          return true;
        },
        openCiudadesExpedicion () {
          this.dialogVisibleCiudadesExpedicion = true;
          this.inputSearchCiudadesExpedicion = '';
        },
        openCiudades () {
          this.dialogVisibleCiudades = true;
          this.inputSearchCiudades = '';
        },
        openBarrios () {
          this.dialogVisibleBarrios = true;
          this.inputSearchBarrios = '';
        },
        openCiudadInfoFamiliar1 () {
          this.dialogVisibleCiudadesInfoFamiliar1 = true;
          this.inputSearchCiudadesInfoFamiliar1 = '';
        },
        openCiudadInfoFamiliar2 () {
          this.dialogVisibleCiudadesInfoFamiliar2 = true;
          this.inputSearchCiudadesInfoFamiliar2 = '';
        },
        filteredListCiudExp () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesExpedicion.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesExpedicion.toLowerCase()) > -1
          })
        },
        filteredListCiudades () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudades.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudades.toLowerCase()) > -1
          })
        },
        filteredListBarrios () {
          this.tableDataBarriosNew = this.tableDataBarrios.filter(item => {
            return item.ba_codbarrio.toLowerCase().indexOf(this.inputSearchBarrios.toLowerCase()) > -1 || item.ba_nombarrio.toLowerCase().indexOf(this.inputSearchBarrios.toLowerCase()) > -1
          })
        },
        filteredListFuncionario () {
          this.tableDataFuncionario = this.tableDataBuscar.filter(item => {
            return item.rh_cedula.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1 || item.full_name.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1
          })
        },
        filteredListCiudadesInfoFamiliar1 () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar1.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar1.toLowerCase()) > -1
          })
        },
        filteredListCiudadesInfoFamiliar2 () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar2.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar2.toLowerCase()) > -1
          })
        },
        andlePreview (file) {
	        console.log(file);
	      },
        calculo () {
          this.ruleForm.dl_comphoras = Math.round(this.ruleForm.dl_salario / 30 / this.ruleForm.dl_hosrasdiarias);
        }
    	},
	    mounted () {
	      this.showCiudades();
	      this.showFuncionarios();
	    }
    })
  </script>

</body>
</html>