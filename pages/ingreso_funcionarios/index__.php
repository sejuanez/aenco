<?php
  session_start();
   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
    echo"<script>window.location.href='../inicio/index.php';</script>";
    exit();
  } 
?>

<!DOCTYPE html>

<html>
<head>
  <meta charset="UTF-8">
  <title>Funcionarios</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/estilo.css">
  <!-- <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' /> -->
  <!-- <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' /> -->
  <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
  <link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />
  <!-- <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script> -->
  <!-- <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css"> -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <link rel="stylesheet" href="https://unpkg.com/buefy/lib/buefy.min.css">
  <style>
    .el-form-item--mini.el-form-item, .el-form-item--small.el-form-item {
      width: 100%;
    }
    .el-form-item--mini .el-form-item__content, .el-form-item--mini .el-form-item__label{
      line-height: 24px;
    }
    .el-select {
      width: 100%;
    }
    .el-table th {
      color: #FFFFFF;
      background-color: rgb(0, 3, 74);
      font-weight: normal;
    }
    .btn{
      background: none !important;
      border: .1px solid #DCDFE6 !important;
      height: 2.6rem !important;
    }
    .el-button:hover{
      background-color: #DDDDDD !important;
      color: #555 !important;
      border-color: #DDDDDD;
    }
    .el-button:focus {
      color: #555 !important;
      border-color: #DDDDDD !important;
    }
    table{
      margin-top: 0px;
      /*font-size: .95em;*/
    }
    .buscar {
      padding-left: 0px !important;
    }
    .el-table--mini, .el-table--small, .el-table__expand-icon {
      font-size: .9em;
      font-weight: normal;
      font-family: 'Segoe UI',Arial,Helvetica,sans-serif;
    }
    .el-input__inner {
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-message-box{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-dialog{
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      -ms-border-radius: 0;
      -o-border-radius: 0;
    }
    .el-table--border td, .el-table--border th, .el-table__body-wrapper .el-table--border.is-scrolling-left~.el-table__fixed {
      border-right: 0px solid #ebeef5;
    }
    ::-webkit-input-placeholder {
      color: black !important;
    }
    ::-moz-placeholder {
      color: black !important;
    }
    :-ms-input-placeholder {
      color: black !important;
    }
    :-moz-placeholder {
      color: black !important;
    }
    .window-dialog, .el-dialog{
      width: 80%;
    }
    [class*=" el-icon-"], [class^=el-icon-]{
      /* margin-left: -.4rem; */
    }
    .img-thumbnail {
      display: inline-block;
      /* max-width: 100%; */
      height: auto;
      padding: 4px;
      line-height: 1em;
      background-color: #fff;
      border: 1px solid #ddd;
      /* border-radius: 4px; */
      border-radius: 50%;
      -webkit-transition: all .2s ease-in-out;
      -o-transition: all .2s ease-in-out;
      transition: all .2s ease-in-out;
    }
    .marco{
      padding: 1rem;
      border: 1px solid silver;
      margin: 1rem;
      background-color: #ededed;
    }
    .activo{
      color: #FF0000 !important;
    }
    .b-table th{
      background: rgb(0, 3, 74);
      font-weight: normal;
    }
    .table thead td, .table thead th{
      color: #FFFFFF !important;
    }
    .b-table .table th {
      font-weight: 200;
    }
    .b-table .table.is-bordered th.is-current-sort, .b-table .table.is-bordered th.is-sortable:hover {
      background: rgb(0, 3, 74) !important;
    }
    .table.is-bordered td, .table.is-bordered th {
      border-width: 0px !important;
    }
    .el-dialog__wrapper{
      overflow: hidden;
    }
    @media only screen and (max-width: 768px) {
      .window-dialog, .el-dialog{
        width: 100%;
      }
      .el-dialog {
        width: 90%;
      }
      .el-message {
        width: 90%;
      }
      .el-table .cell{
        font-size: .8em;
      }
      #list-img img{
        width: 296px;
        height: 170px;
      }
      header h3{
        text-align: left;
        font-size: .8em;
      }
      .fileupload, .fileupload-new{
        text-align: center;
      }
      .btn{
        height: unset !important;
      }
      .marco-interno{
        margin: 0rem;
      }
    }
  </style>
  <script src="js/jquery-2.2.3.min.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <script src="https://unpkg.com/element-ui/lib/umd/locale/es.js"></script>
  <script src="https://unpkg.com/buefy"></script>
  <script src="js/select2/select2.full.min.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="js/inLetters.js"></script>
  <script src="js/funciones.js"></script>
 <!--  dialogBuscar = true <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script> -->
</head>
<body>
  <!-- v-loading="loading" -->
  <div id="app">
    <header>
      <h3>Funcionarios</h3>
      <nav>
      <!-- dialogBuscar = true -->
      <el-button v-if="visibleCreate" @click.stop.prevent="addFuncionario" :loading="loadingSave" icon="el-icon-check" class="btn">Guardar</el-button>
      <el-button v-if="visibleUpdate" @click.stop.prevent="onUpdate" :loading="loadingSave" icon="el-icon-check" class="btn">Actualizar</el-button>
      <el-button @click.stop.prevent="openFuncionario" icon="el-icon-search" class="btn">Buscar</el-button>
      <el-button @click.stop.prevent="onCancel" icon="el-icon-close" class="btn">Cancelar</el-button>
      </nav>
    </header>

    <section style="padding-top: 44px; height: calc(100% - 80px); height: -webkit-calc(100% - 80px); height: -moz-calc(100% - 80px)">
      <fieldset class="marco">
        <legend>Datos personales</legend>
        <el-form :model="ruleForm" :rules="rules" ref="ruleForm" size="mini">
          <el-row :gutter="4">

            <el-col :xs="24" :sm="24" :md="18" :lg="18" :xl="18">

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Cedula">
                  <el-input v-model="ruleForm.rh_cedula" @keyup.enter.native="getFuncionarioBY" @blur="getFuncionarioBY"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codexpciud"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="21" :sm="21" :md="8" :lg="8" :xl="8">
                <el-form-item label="Expedida en">
                  <el-input v-model="ruleForm.rh_expcedula" readOnly>
                    <el-button slot="append" icon="el-icon-search" @click="openCiudadesExpedicion"></el-button>
                  </el-input>
                </el-form-item>
                <!-- <el-form-item label="Expedida en"> -->
                  <!-- <el-input v-model="ruleForm.rh_expcedula" readOnly>
                    <el-button @click="openCiudadesExpedicion" icon="el-icon-search"></el-button>
                  </el-input> -->
                <!-- </el-form-item> -->
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Primer apellido">
                  <el-input v-model="ruleForm.rh_apellido1"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Segundo apellido">
                  <el-input v-model="ruleForm.rh_apellido2"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                <el-form-item label="Primer nombre">
                  <el-input v-model="ruleForm.rh_nombre1"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                <el-form-item label="Segundo nombre">
                  <el-input v-model="ruleForm.rh_nombre2"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                <el-form-item label="Dirección">
                  <el-input v-model="ruleForm.rh_direccion"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codciudad"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="21" :sm="21" :md="9" :lg="9" :xl="9">
                <el-form-item label="Ciudad">
                  <el-input v-model="ruleForm.rh_ciudad" readOnly>
                    <el-button slot="append" icon="el-icon-search" @click="openCiudades"></el-button>
                  </el-input>
                  <!-- <el-input v-model="ruleForm.rh_ciudad" readOnly></el-input> -->
                </el-form-item>
              </el-col>

              <!-- <el-col :xs="1" :sm="1" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudades" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col> -->

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codbarrio"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="21" :sm="21" :md="7" :lg="7" :xl="7">
                <el-form-item label="Barrio">
                  <el-input v-model="ruleForm.rh_barrio" readOnly>
                    <el-button slot="append" icon="el-icon-search" @click="openBarrios" :disabled="stateButton"></el-button>
                  </el-input>
                  <!-- <el-input v-model="ruleForm.rh_barrio" readOnly></el-input> -->
                </el-form-item>
              </el-col>

              <!-- <el-col :xs="1" :sm="1" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openBarrios" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;" :disabled="stateButton"></el-button>
                </el-form-item>
              </el-col> -->

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item label="Teléfono">
                  <el-input v-model="ruleForm.rh_telefono"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item label="Celular">
                  <el-input v-model="ruleForm.rh_movil"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Sexo">
                  <el-select v-model="ruleForm.rh_sexo" clearable placeholder="" style="width: 100%">
                    <el-option
                      v-for="item in optionsGenero"
                      :key="item.value"
                      :label="item.label"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Estado civil">
                  <el-select v-model="ruleForm.rh_estcivil" clearable placeholder="" style="width: 100%">
                    <el-option
                      v-for="item in optionsEstadoCivil"
                      :key="item.value"
                      :label="item.label"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Factor sangre">
                  <el-select v-model="ruleForm.rh_rh" filterable clearable placeholder="" style="width: 100%">
                    <el-option
                      v-for="item in optionsTipoSanguineo"
                      :key="item.value"
                      :label="item.fac_sangre1"
                      :value="item.value">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="20" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Fecha de nacimiento">
                  <el-date-picker
                    @change="edad"
                    v-model="ruleForm.rh_fechanac"
                    style="width: 100%"
                    type="date"
                    format="MM/dd/yyyy"
                    value-format="MM/dd/yyyy">
                  </el-date-picker>
                </el-form-item>
              </el-col>

              <el-col :xs="4" :sm="24" :md="1" :lg="1" :xl="1">
                <el-form-item label="Edad">
                  <el-input v-model="ruleForm.rh_edad" readOnly></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_coddptonac"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="21" :sm="21" :md="6" :lg="6" :xl="6">
                <el-form-item label="Departamento">
                  <el-input v-model="ruleForm.rh_dptonac" readOnly>
                    <el-button slot="append" icon="el-icon-search" @click="openCiudadDpto"></el-button>
                  </el-input>
                  <!-- <el-input v-model="ruleForm.rh_dptonac" readOnly></el-input> -->
                </el-form-item>
              </el-col>

              <!-- <el-col :xs="1" :sm="1" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudadDpto" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col> -->

              <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                <el-form-item label="Codigo">
                  <el-input v-model="ruleForm.rh_codciunac"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="21" :sm="21" :md="6" :lg="6" :xl="6">
                <el-form-item label="Ciudad">
                  <el-input v-model="ruleForm.rh_ciudadnac" readOnly>
                    <el-button slot="append" icon="el-icon-search" @click="openMpio" :disabled="stateButton2"></el-button>
                  </el-input>
                  <!-- <el-input v-model="ruleForm.rh_ciudadnac" readOnly></el-input> -->
                </el-form-item>
              </el-col>

              <!-- <el-col :xs="1" :sm="1" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openMpio" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col> -->

            </el-col>

            <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
              <!-- <span><p  style="margin-left: 11rem;" "></p></span> -->
              <div class="fileupload fileupload-new" data-provides="fileupload" style="height: 211px !important;text-align: center;">
                <div id="getimagen_cobradores" class="fileupload-new img-thumbnail">
                  <img width="200" height="185" id="imgFuncionario" src="img/avatar.jpg" style="cursor: pointer;border-radius: 50%;">
                </div>
                <p v-text="ruleForm.rh_estado" :class="active ? 'activo' : ''" style="text-align: center;"></p>
                <div style="visibility: hidden;">
                  <span class="btn btn-facebook btn-file" >
                    <span class="fileupload-new">Seleccionar imagen</span><br>
                    <span class="fileupload-exists">Change</span> 
                    <input type="file" id="foto" class="form-control-file" name="foto" value="Seleccionar imagen">
                  </span>
                  <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a> 
                </div>
              </div>

              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-form-item label="Nacionalidad">
                  <el-select style="width: 100%" v-model="ruleForm.rh_codnacional" filterable clearable placeholder="">
                    <el-option
                      v-for="item in optionsNacionalidades"
                      :key="item.na_codigo"
                      :label="item.na_nacional"
                      :value="item.na_codigo">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

            </el-col>

          </el-row>
        </el-form>
      </fieldset>
    </section>
<!--    v-loading="loadingCard"-->
    <section id="seccion-tabla" style="margin: 1rem;">

      <el-tabs type="card">

        <el-tab-pane label="1. Datos Legales">
          <fieldset class="marco">
            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset class="marco marco-interno">
                <el-form size="mini">
                <el-row :gutter="4">

                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <el-form-item label="Cent. Trabajo">
                      <el-select v-model="ruleForm.dl_codcentrab" filterable clearable placeholder="" style="width: 100%">
                        <el-option
                          v-for="item in optionscentroTrabajo"
                          :key="item.ct_codigo"
                          :label="item.ct_centrabajo"
                          :value="item.ct_codigo">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                    <el-form-item label="Cent. Costo">
                      <el-select v-model="ruleForm.dl_codcencosto" filterable clearable placeholder="" style="width: 100%">
                        <el-option
                          v-for="item in optionscentroCosto"
                          :key="item.cc_codigo"
                          :label="item.cc_cencosto"
                          :value="item.cc_codigo">
                        </el-option>
                      </el-select>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Salario">
                      <el-input v-model="ruleForm.dl_salario" @keyup.enter.native="inLetter" @blur="inLetter"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Horas Diarias">
                      <el-input v-model="ruleForm.dl_hosrasdiarias" @keyup.enter.native="calculo" @blur="calculo"></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                    <el-form-item label="Comp. Horas">
                      <el-input v-model="ruleForm.dl_comphoras" readOnly></el-input>
                    </el-form-item>
                  </el-col>

                  <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                    <el-form-item label="Salario en Letras">
                      <el-input v-model="ruleForm.dl_salario_letras" type="textarea" readOnly></el-input>
                    </el-form-item>
                  </el-col>
                </el-row>

               </el-form>
              </fieldset>
            </el-col>

            <el-col ::xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset class="marco marco-interno">
                <legend>Libreta Militar</legend>
                <el-form label-position="top" size="mini">

                  <el-row :gutter="4">
                    <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                      <el-form-item label="Número">
                        <el-input v-model="ruleForm.dl_nlibreta"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="10" :lg="10" :xl="10">
                      <el-form-item label="Clase">
                        <el-select v-model="ruleForm.dl_codclaselib" clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsClaseLibreta"
                            :key="item.value"
                            :label="item.label"
                            :value="item.value">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                      <el-form-item label="Distrito">
                        <el-input v-model="ruleForm.dl_distritomil"></el-input>
                      </el-form-item>
                    </el-col>
                  </el-row>

                </el-form>
              </fieldset>

              <fieldset class="marco marco-interno">
                <legend>Certificado Judicial</legend>
                <el-form label-position="top" size="mini">
                  <el-row :gutter="4">

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Número">
                        <el-input v-model="ruleForm.dl_certjudicial"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Fecha">
                        <el-date-picker
                          v-model="ruleForm.dl_fechaven"
                          style="width: 100%"
                          type="date"
                          format="MM/dd/yyyy"
                          value-format="MM/dd/yyyy">
                        </el-date-picker>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="2. Parafiscales">
          <fieldset class="marco">
            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset class="marco marco-interno">
                <legend>Seguridad social</legend>
                <el-form size="mini">

                  <el-row :gutter="4">
                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="ARL">
                        <el-select v-model="ruleForm.dp_codarp" filterable clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsArp"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="EPS">
                        <el-select v-model="ruleForm.dp_codeps" filterable clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsEps"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="AFP">
                        <el-select v-model="ruleForm.dp_afp" filterable clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsAfp"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Caja">
                        <el-select v-model="ruleForm.dp_codcaja" filterable clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsCaja"
                            :key="item.ss_codigo"
                            :label="item.ss_nombre"
                            :value="item.ss_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="7" :lg="7" :xl="7">
                      <el-form-item label="% Adicional ARL">
                        <el-input v-model="ruleForm.dp_adicionarp"></el-input>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>

            <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
              <fieldset class="marco marco-interno">
                <legend>Cuenta Bancaria</legend>
                <el-form label-position="top" size="mini">
                  <el-row :gutter="4">

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="Banco">
                        <el-select v-model="ruleForm.dp_codbanco" filterable clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsBanco"
                            :key="item.ba_codigo"
                            :label="item.ba_nombre"
                            :value="item.ba_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                      <el-form-item label="N° Cuenta">
                        <el-input v-model="ruleForm.dp_ncuenta"></el-input>
                      </el-form-item>
                    </el-col>

                  </el-row>
                </el-form>
              </fieldset>
            </el-col>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="3. Empresa">
          <fieldset class="marco">
            <el-form size="mini">
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Jefe inmediato">
                    <el-select v-model="ruleForm.em_codingeniero" filterable clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsJefeInmediato"
                        :key="item.rh_cedula"
                        :label="item.full_name"
                        :value="item.rh_cedula">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Programa">
                    <el-select v-model="ruleForm.em_programa" filterable clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsCampanas"
                        :key="item.c_codigo"
                        :label="item.c_nombre"
                        :value="item.c_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleForm.em_codigo" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cuadrilla">
                    <el-input v-model="ruleForm.em_cuadrilla" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cel. Empresa N°">
                    <el-input v-model="ruleForm.em_cel_empresa_num" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Radio Empresa N°">
                    <el-input v-model="ruleForm.em_radio_empresa_num" readOnly></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Manejo y confianza">
                    <el-select v-model="ruleForm.em_confianza" clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsConfianza"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Placa">
                    <el-select v-model="ruleForm.em_placa" clearable placeholder="" style="width: 100%" disabled>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sector">
                    <el-select v-model="ruleForm.em_codarea" clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsAreas"
                        :key="item.ar_codigo"
                        :label="item.ar_area"
                        :value="item.ar_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                  <el-form-item label="Cargo">
                    <el-select v-model="ruleForm.em_codcargo" filterable clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsCargos"
                        :key="item.ca_codigo"
                        :label="item.ca_cargo"
                        :value="item.ca_codigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Perfil">
                    <el-select v-model="ruleForm.em_perfil" clearable placeholder="">
                      <el-option
                        v-for="item in optionsPerfil"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>
              </el-row>

              <!-- TALLAS DE DOTACION -->
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                  <fieldset class="marco marco-interno">
                    <legend>Tallas de dotación</legend>
                    <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                      <el-form-item label="Categoria">
                        <el-select v-model="ruleForm.ta_tipo" clearable placeholder="" style="width: 100%">
                          <el-option
                            v-for="item in optionsCategoriaDotacion"
                            :key="item.cd_codigo"
                            :label="item.cd_nombre"
                            :value="item.cd_codigo">
                          </el-option>
                        </el-select>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Camisa">
                        <el-input v-model="ruleForm.ta_camisa" maxlength="2"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Pantalon">
                        <el-input v-model="ruleForm.ta_pantalon" maxlength="2"></el-input>
                      </el-form-item>
                    </el-col>

                    <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item label="Botas">
                        <el-input v-model="ruleForm.ta_botas" maxlength="2"></el-input>
                      </el-form-item>
                    </el-col>

                    <!-- <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                      <el-form-item>
                        <el-button icon="el-icon-search">Consultar dotación asignada</el-button>
                      </el-form-item>
                    </el-col> -->
                  </fieldset>
                </el-col>
              </el-row>
              
            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="4. Conte">
          <fieldset class="marco">
            <el-form size="mini">
            <el-row :gutter="4">
              <el-col :xs="16" :sm="16" :md="12" :lg="12" :xl="12">
                <el-form-item label="">
                  <el-select v-model="ruleFormConte.dc_codconte" filterable clearable placeholder="" style="width: 100%">
                    <el-option
                      v-for="item in optionsConte"
                      :key="item.co_codigo"
                      :label="item.co_nombre"
                      :value="item.co_codigo">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="4" :sm="4" :md="4" :lg="4" :xl="4">
                <el-form-item>
                  <el-button @click="addDatosConte">Adicionar</el-button>
                </el-form-item>
              </el-col>
            </el-row>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableData"
                  border
                  :index="indexMethod"
                  size="mini"
                  style="width: 100%">
                  <el-table-column
                    prop="dc_codconte"
                    label="Codigo"
                    width="80">
                  </el-table-column>
                  <el-table-column
                    prop="dc_desconte"
                    label="Descripción">
                  </el-table-column>
                  <el-table-column
                    label="Acción"
                    width="80">
                    <template slot-scope="scope">
                      <a href="javascript:void(0)" @click.stop.prevent="deleteRow(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>

            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="5. Contrato">
          <fieldset class="marco">
            <el-form size="mini">
              <el-row>
                <el-form-item label="">
                  <el-button-group>
                    <el-button @click="openNuevoContrato" size="medium">Nuevo contrato</el-button>
                    <el-button @click="openTerminarContrato" size="medium" :disabled="stateButtonContrato">Terminar contrato</el-button>
                  </el-button-group>
                </el-form-item>
              </el-row>

              <el-row>
                <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                  <el-table
                    :data="tableDataContrato"
                    border
                    size="mini"
                    style="width: 100%">
                    <el-table-column
                      type="index"
                      align="center"
                      prop="co_item"
                      label="Item"
                      width="80"
                      :index="indexMethod1">
                    </el-table-column>
                    <el-table-column
                      prop="co_empresa"
                      label="Empresa">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      width="90"
                      prop="co_fechaing"
                      label="Fecha Inicio">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      prop="co_fechafin"
                      width="90"
                      label="Fecha fin">
                    </el-table-column>
                    <el-table-column
                      align="center"
                      prop="co_fecharet"
                      width="90"
                      label="Fecha Retiro">
                    </el-table-column>
                    <el-table-column
                      prop="co_motivoretiro"
                      label="Motivo del retiro">
                    </el-table-column>
                    <el-table-column
                      prop="name"
                      label="Motivo real del retro">
                    </el-table-column>
                    <el-table-column header-align="center" align="center" label="Acción" width="70">
                      <template slot-scope="scope">
                        <a href="#" class="delete" @click.stop.prevent="editRowContrato(scope.row, scope.$index)">
                          <i class="el-icon-edit" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="delete" @click.stop.prevent="deleteRowContrato(scope.row, scope.$index)">
                          <i class="el-icon-delete" aria-hidden="true"></i>
                        </a>
                      </template>
                    </el-table-column>
                  </el-table>
                </el-col>
              </el-row>
            </el-form>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="6. Formación Academica">
          <fieldset class="marco">
            <el-form size="mini">
            <el-row :gutter="4">
              <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                <el-form-item label="Tipo estudio">
                  <el-select v-model="ruleFormFAcademica.df_codigo" filterable clearable placeholder="" style="width: 100%">
                    <el-option
                      v-for="item in optionsTipoEstudio"
                      :key="item.es_codigo"
                      :label="item.es_nombre"
                      :value="item.es_codigo">
                    </el-option>
                  </el-select>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                <el-form-item label="Ins. Educativa">
                  <el-input v-model="ruleFormFAcademica.df_insteducativa" onKeyUp="this.value=this.value.toUpperCase();"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
                <el-form-item label="Titulo obtenido">
                  <el-input v-model="ruleFormFAcademica.df_tituloobt" onKeyUp="this.value=this.value.toUpperCase();"></el-input>
                </el-form-item>
              </el-col>

              <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                <el-form-item>
                  <el-button @click="addFormacionAcademica" style="margin-top: 1.55rem;">Adicinar</el-button>
                </el-form-item>
              </el-col>
            </el-row>
            </el-form>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableDataInfoAcademinaca"
                  border
                  :index="indexMethod5"
                  size="mini"
                  style="width: 100%">
                  <el-table-column
                    prop="df_formacademica"
                    label="Tipo estudio">
                  </el-table-column>
                  <el-table-column
                    prop="df_insteducativa"
                    label="Ins. Educativa">
                  </el-table-column>
                  <el-table-column
                    prop="df_tituloobt"
                    label="Titulo Obtenido">
                  </el-table-column>
                  <el-table-column header-align="center" align="center" label="Acción" width="70">
                    <template slot-scope="scope">
                      <a href="#" class="delete" @click.stop.prevent="deleteRowFormacademica(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>
          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="7. Inf. Familiar">
          <fieldset class="marco">
            <el-form size="mini">
              <el-row :gutter="6">
                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Cedula conyugue">
                    <el-input v-model="ruleFormInfoFamiliar.if_cedulacony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleFormInfoFamiliar.if_ciudadexp"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
                  <el-form-item label="Ciudad Exp">
                    <el-input v-model="ruleFormInfoFamiliar.if_desciudadexp" readOnly>
                      <el-button slot="append" icon="el-icon-search" @click="openCiudadInfoFamiliar1"></el-button>
                    </el-input>
                    <!-- <el-input v-model="ruleFormInfoFamiliar.if_desciudadexp" readOnly></el-input> -->
                  </el-form-item>
                </el-col>

              <!-- <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                <el-form-item>
                  <el-button @click="openCiudadInfoFamiliar1" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                </el-form-item>
              </el-col> -->

                <el-col :xs="24" :sm="24" :md="10" :lg="10" :xl="10">
                  <el-form-item label="Nombre conyugue">
                    <el-input v-model="ruleFormInfoFamiliar.if_nombrecony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Fecha Nac">
                    <el-date-picker
                      v-model="ruleFormInfoFamiliar.if_fechanaccony"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2" style="display: none;">
                  <el-form-item label="Codigo">
                    <el-input v-model="ruleFormInfoFamiliar.if_ciudad"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Ciudad">
                    <el-input v-model="ruleFormInfoFamiliar.if_desciudad" readOnly>
                      <el-button slot="append" icon="el-icon-search" @click="openCiudadInfoFamiliar2"></el-button>
                    </el-input>
                    <!-- <el-input v-model="ruleFormInfoFamiliar.if_desciudad" readOnly></el-input> -->
                  </el-form-item>
                </el-col>

                <!-- <el-col :xs="24" :sm="24" :md="1" :lg="1" :xl="1" class="buscar">
                  <el-form-item>
                    <el-button @click="openCiudadInfoFamiliar2" icon="el-icon-search" style="margin-top: 1.5rem;width: 1rem;"></el-button>
                  </el-form-item>
                </el-col> -->

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sexo">
                    <el-select v-model="ruleFormInfoFamiliar.if_sexocony" clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsGenero"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="15" :lg="15" :xl="15">
                  <el-form-item label="Ocupacion">
                    <el-input v-model="ruleFormInfoFamiliar.if_profecony"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="11" :lg="11" :xl="11">
                  <el-form-item label="Nombre del hijo">
                    <el-input v-model="ruleFormInfoHijos.ih_nombrehijo" onKeyUp="this.value=this.value.toUpperCase();"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Fecha Nac">
                    <el-date-picker
                      v-model="ruleFormInfoHijos.ih_fechanac"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="3" :lg="3" :xl="3">
                  <el-form-item label="D.I. / Reg Civil">
                    <el-input v-model="ruleFormInfoHijos.ih_identifica"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="4" :lg="4" :xl="4">
                  <el-form-item label="Sexo">
                    <el-select v-model="ruleFormInfoHijos.ih_sexo" clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsGenero"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                  <el-form-item>
                    <el-button @click="addInfoFamiliar" style="margin-top: 1.5rem;">Adicionar</el-button>
                  </el-form-item>
                </el-col>

              </el-row>
              
            </el-form>

            <!-- <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24"> -->
              <el-table
                :data="tableDataInfoHijos"
                border
                size="mini"
                style="width: 100%">
                <el-table-column
                  type="index"
                  align="center"
                  label="#"
                  width="80"
                  :index="indexMethod4">
                </el-table-column>
                <el-table-column
                  prop="ih_identifica"
                  label="Identificación"
                  width="100">
                </el-table-column>
                <el-table-column
                  prop="ih_nombrehijo"
                  label="Nombre y Apellidos">
                </el-table-column>
                <el-table-column
                  prop="ih_fechanac"
                  label="Fec Nacimiento"
                  width="110">
                </el-table-column>
                <el-table-column
                  prop="if_desciudad"
                  label="Ciudad">
                </el-table-column>
                <el-table-column
                  prop="ih_sexo"
                  label="Sexo"
                  width="80">
                </el-table-column>
                <el-table-column header-align="center" align="center" label="Acción" width="70">
                  <template slot-scope="scope">
                    <a href="#" class="delete" @click.stop.prevent="deleteRowInfoHijos(scope.row, scope.$index)">
                      <i class="el-icon-delete" aria-hidden="true"></i>
                    </a>
                  </template>
                </el-table-column>
              </el-table>
            <!-- </el-col> -->

          </fieldset>
        </el-tab-pane>

        <el-tab-pane label="8. Vencimientos">
          <fieldset class="marco">
            <el-form size="mini">
              <el-row :gutter="4">
                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Tipo">
                    <el-select v-model="ruleFormVencimientos.codtipovence" clearable placeholder="" style="width: 100%">
                      <el-option
                        v-for="item in optionsTipoVencimiento"
                        :key="item.tvcodigo"
                        :label="item.tvdescripcion"
                        :value="item.tvcodigo">
                      </el-option>
                    </el-select>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
                  <el-form-item label="Descripcion">
                    <el-input v-model="ruleFormVencimientos.descripcion" onKeyUp="this.value=this.value.toUpperCase();"></el-input>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
                  <el-form-item label="Fecha">
                    <el-date-picker
                      v-model="ruleFormVencimientos.fechavence"
                      style="width: 100%"
                      type="date"
                      format="MM/dd/yyyy"
                      value-format="MM/dd/yyyy">
                    </el-date-picker>
                  </el-form-item>
                </el-col>

                <el-col :xs="24" :sm="24" :md="2" :lg="2" :xl="2">
                  <el-form-item>
                    <el-button @click="addVencimientos" style="margin-top: 1.5rem;">Adicinar</el-button>
                  </el-form-item>
                </el-col>
              </el-row>
            </el-form>

            <el-row>
              <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
                <el-table
                  :data="tableDataVenciomientos"
                  border
                  size="mini"
                  style="width: 100%"
                  :index="indexMethod2">
                  <el-table-column
                    prop="codtipovence"
                    label="Tipo">
                  </el-table-column>
                  <el-table-column
                    prop="descripcion"
                    label="Descripcion">
                  </el-table-column>
                  <el-table-column
                    prop="fechavence"
                    label="Fecha Vigencia">
                  </el-table-column>
                  <el-table-column header-align="center" align="center" label="Acción" width="70">
                    <template slot-scope="scope">
                      <a href="#" class="delete" @click.stop.prevent="deleteRowVencimiento(scope.row, scope.$index)">
                        <i class="el-icon-delete" aria-hidden="true"></i>
                      </a>
                    </template>
                  </el-table-column>
                </el-table>
              </el-col>
            </el-row>

          </fieldset>
        </el-tab-pane>

      </el-tabs>

    </section>

    <!--DIALOGO CONTRATO-->
        <el-dialog title="Nuevo Contrato" :visible.sync="modalVisible" :close-on-click-modal="false" class="window-dialog">
      <el-row :gutter="6">
        <el-form size="mini">

          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item label="Empresa">
              <el-select v-model="ruleFormDatosContrato.co_codempresa" filterable clearable placeholder="" style="width: 100%" :disabled="on">
                <el-option
                  v-for="item in optionsEmpresa"
                  :key="item.sw"
                  :label="item.nombre"
                  :value="item.sw">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
            <el-form-item label="Fecha ingreso">
              <el-date-picker
                :disabled="on"
                v-model="ruleFormDatosContrato.co_fechaing"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Tipo contrato">
              <el-select v-model="ruleFormDatosContrato.co_tcontrato" @change="onChange" clearable placeholder="" style="width: 100%" :disabled="on">
                <el-option
                  v-for="item in optionsTipoContrato"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="5" :lg="5" :xl="5">
            <el-form-item label="Fecha fin">
              <el-date-picker
                :disabled="stateDate"
                v-model="ruleFormDatosContrato.co_fechafin"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="6" :lg="6" :xl="6">
            <el-form-item label="Fecha retiro">
              <el-date-picker
                :disabled="off"
                v-model="ruleFormDatosContrato.co_fecharet"
                style="width: 100%"
                type="date"
                format="MM/dd/yyyy"
                value-format="MM/dd/yyyy">
              </el-date-picker>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="9" :lg="9" :xl="9">
            <el-form-item label="Motivo retiro">
              <el-select v-model="ruleFormDatosContrato.co_codretiro" filterable clearable placeholder="" style="width: 100%" :disabled="off">
                <el-option
                  v-for="item in optionsMotivoRetiro"
                  :key="item.mr_codigo"
                  :label="item.mr_motivo"
                  :value="item.mr_codigo">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>

          <el-col :xs="24" :sm="24" :md="9" :lg="9" :xl="9">
            <el-form-item label="Motivo real">
              <el-select :disabled="off" v-model="ruleFormDatosContrato.co_codreal" filterable clearable placeholder="" style="width: 100%"></el-select>
            </el-form-item>
          </el-col>
          
        </el-form>
      </el-row>

      <span slot="footer" class="dialog-footer">
        <el-button @click="addDatosContrato">Adicionar</el-button>
      </span>
    </el-dialog>

    <!--DIALOG BUSCAR FUNCIONARIO-->
    <el-dialog title="Busqueda de funcionario" :visible.sync="dialogBuscar" :close-on-click-modal="false" class="window-dialog">
      <el-row>
        <el-form @submit.native.prevent="filteredListFuncionario" size="mini">
          <el-col :xs="24" :sm="24" :md="8" :lg="8" :xl="8">
            <el-form-item>
              <el-input v-model ="inputSearch" @keyup.native="filteredListFuncionario">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>
        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClick" :bordered="true" paginated per-page="6" :data="tableDataFuncionario" :columns="columnsRRHH"></b-table> -->
          <el-table
            :data="tableDataFuncionario"
            border
            height="250"
            @row-click="rowClick"
            size="mini"
            style="width: 100%">
            <el-table-column
              prop="rh_cedula"
              header-align="left"
              align="right"
              width="90"
              label="Cedula">
            </el-table-column>
            <el-table-column
              prop="full_name"
              label="Nombre y Apellidos">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>

    </el-dialog>

    <!-- DIALOG CIUDADES EXPEDICION -->
    <el-dialog title="Ciudades expedición" :visible.sync="dialogVisibleCiudadesExpedicion" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudExp" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesExpedicion" @keyup.native="filteredListCiudExp" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClickCiudadesExpedicion" :bordered="true" paginated per-page="6" :data="tableDataCiudadesExpedicionNew" :columns="columns"></b-table> -->
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            refs="materiales"
            @row-click="rowClickCiudadesExpedicion"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG CIUDADES -->
    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudades" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudades" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudades" @keyup.native="filteredListCiudades" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClickCiudades" :bordered="true" paginated per-page="6" :data="tableDataCiudadesExpedicionNew" :columns="columns"></b-table> -->
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickCiudades"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG CIUDADES INFO FAMILIAR1 -->
    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudadesInfoFamiliar1" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudadesInfoFamiliar1" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesInfoFamiliar1" @keyup.native="filteredListCiudadesInfoFamiliar1" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickCiudadesInfoFamiliar1"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>
    
    <!-- DIALOG CIUDADES INFO FAMILIAR2 -->
    <el-dialog title="Ciudades" :visible.sync="dialogVisibleCiudadesInfoFamiliar2" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListCiudadesInfoFamiliar2" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchCiudadesInfoFamiliar2" @keyup.native="filteredListCiudadesInfoFamiliar2" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <el-table
            :data="tableDataCiudadesExpedicionNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickCiudadesInfoFamiliar2"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG BARRIOS -->
    <el-dialog title="Barrios" :visible.sync="dialogVisibleBarrios" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListBarrios" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchBarrios" @keyup.native="filteredListBarrios" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClickBarrios" :bordered="true" paginated per-page="6" :data="tableDataBarriosNew" :columns="columnsBarrio"></b-table> -->
          <el-table
            :data="tableDataBarriosNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickBarrios"
            style="width: 100%">
            <el-table-column
              prop="ba_codbarrio"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="ba_nombarrio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG DEPARTAMENTOS -->
    <el-dialog title="Departamento" :visible.sync="dialogVisibleDpto" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListDpto" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchDpto" @keyup.native="filteredListDpto" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClickDpto" :bordered="true" paginated per-page="6" :data="tableDataDptoNew" :columns="columnsDpto"></b-table> -->
          <el-table
            :data="tableDataDptoNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickDpto"
            style="width: 100%">
            <el-table-column
              prop="mu_coddpto"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_departamento"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

    <!-- DIALOG CIUDAD -->
    <el-dialog title="Ciudad" :visible.sync="dialogVisibleMpio" :close-on-click-modal="false">
      <el-row :gutter="12">
        <el-form @submit.native.prevent="filteredListMpio" size="mini">

          <el-col :xs="24" :sm="24" :md="12" :lg="12" :xl="12">
            <el-form-item >
              <el-input v-model="inputSearchMpio" @keyup.native="filteredListMpio" :autofocus="true">
                <i slot="prefix" class="el-input__icon el-icon-search"></i>
              </el-input>
            </el-form-item>
          </el-col>

        </el-form>
      </el-row>

      <el-row>
        <el-col :xs="24" :sm="24" :md="24" :lg="24" :xl="24">
          <!-- <b-table @click="rowClickMpio" :bordered="true" paginated per-page="6" :data="tableDataMpioNew" :columns="columnsMpio"></b-table> -->
          <el-table
            :data="tableDataMpioNew"
            border
            size="mini"
            height="250"
            highlight-current-row
            @row-click="rowClickMpio"
            style="width: 100%">
            <el-table-column
              prop="mu_codigo"
              label="Codigo"
              width="100">
            </el-table-column>
            <el-table-column
              prop="mu_municipio"
              label="Descripción">
            </el-table-column>
          </el-table>
        </el-col>
      </el-row>
    </el-dialog>

  </div>

  <script>

    jQuery(document).ready(function($) {
      $('#foto').on('change', function () {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) {
          return; // no file selected, or no FileReader support
        }

        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file
        console.log(files[0]);
        reader.onloadend = function (e) { // set image data as background of div
          console.log(e.target);
          $('#imgFuncionario').attr('src', e.target.result);
        };
      });

      $("#imgFuncionario").click(function (event) {
        $("#foto").click();
      });
      // $("#foto").change(function(){
      //  readURL(this);
      // });
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgFuncionario').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    ELEMENT.locale(ELEMENT.lang.es);
    Vue.use(Buefy.default);
    new Vue({
      el: '#app',
      data: () => ({
        active: false,
        ruleForm: {
          rh_cedula: '',
          rh_codexpciud: '',
          rh_expcedula: '',
          rh_apellido1: '',
          rh_apellido2: '',
          rh_nombre: '',
          rh_direccion: '',
          rh_codciudad: '',
          rh_ciudad: '',
          rh_codbarrio: '',
          rh_barrio: '',
          rh_telefono: '',
          rh_movil: '',
          rh_sexo: '',
          rh_dessexo: '',
          rh_facsangre: '',
          rh_rh: '',
          rh_rh1: '',
          rh_desrh: '',
          rh_estcivil: '',
          rh_fechanac: '',
          rh_codciunac: '',
          rh_ciudadnac: '',
          rh_coddptonac: '',
          rh_dptonac: '',
          rh_codnacional: '',
          rh_nacional: '',
          rh_estado: '',
          rh_foto: '',
          rh_profesion: '',
          rh_explaboral: '',
          rh_nombre1: '',
          rh_nombre2: '',
          rh_edad: '',
          dl_codcentrab: '',
          dl_descentrab: '',
          dl_codcencosto: '',
          dl_descencosto: '',
          dl_salario: '',
          dl_comphoras: '',
          dl_nlibreta: '',
          dl_codclaselib: '',
          dl_desclaselib: '',
          dl_distritomil: '',
          dl_certjudicial: '',
          dl_fechaven: '',
          dl_hosrasdiarias: '',
          dl_salario_letras: '',
          dp_codarp: '',
          dp_desarp: '',
          dp_codeps: '',
          dp_deseps: '',
          dp_afp: '',
          dp_desafp: '',
          dp_codcaja: '',
          dp_descaja: '',
          dp_codbanco: '',
          dp_desbanco: '',
          dp_ncuenta: '',
          dp_convencion: 'N',
          dp_adicionarp: '',
          em_cedula: '',
          em_codingeniero: '',
          em_nomingeniero: '',
          em_programa: '',
          em_desprograma: '',
          em_cuadrilla: '',
          em_codarea: '',
          em_desarea: '',
          em_codcargo: '',
          em_descargo: '',
          em_confianza: '',
          em_codigo: '',
          em_perfil: '',
          em_cel_empresa_num: '',
          em_radio_empresa_num: '',
          em_placa: '',
          ta_camisa: '',
          ta_pantalon: '',
          ta_botas: '',
          ta_tipo: '',
          df_cedula: '',
          co_cedula: '',
          te_codigo: ''
        },
        ruleFormConte: {
          dc_codconte: '',
          dc_desconte: ''
        },
        ruleFormFAcademica: {
          df_codigo: '',
          df_formacademica: '',
          df_insteducativa: '',
          df_tituloobt: ''
        },
        ruleFormDatosContrato: {
          co_codempresa: '',
          co_empresa: '',
          co_fechaing: '',
          co_fechafin: '',
          co_fecharet: '',
          co_tcontrato: '',
          co_codretiro: '',
          co_item: '',
          co_motivoretiro: '',
          co_codreal: '',
          co_motivoreal: '',
          co_fecharet: '',
          co_observacion: ''
        },
        ruleFormVencimientos: {
          codtipovence: '',
          destipovence: '',
          descripcion: '',
          fechavence: ''
        },
        ruleFormInfoFamiliar: {
          if_cedula: '',
          if_nombrecony: '',
          if_fechanaccony: '',
          if_ciudad: '',
          if_desciudad: '',
          if_cedulacony: '',
          if_ciudadexp: '',
          if_desciudadexp: '',
          if_profecony: '',
          if_canhijo: '',
          if_sexocony: '',
          if_dessexocony: ''
        },
        ruleFormInfoHijos: {
          ih_nombrehijo: '',
          ih_fechanac: '',
          ih_identifica: '',
          ih_sexo: '',
          cantidad: ''
        },
        rules: {
          rh_cedula: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_expcedula: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_apellido1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_nombre1: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_direccion: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_ciudad: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_barrio: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_telefono: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_movil: [
            { required: true, message: 'Campo requerido', trigger: 'blur' }
          ],
          rh_sexo: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_estcivil: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_rh: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_fechanac: [
            { type: 'date', required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_coddptonac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codciunac: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          rh_codnacional: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcentrab: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ],
          dl_codcencosto: [
            { required: true, message: 'Campo requerido', trigger: 'change' }
          ]
        },
        imageUrl: '',
        selected: 0,
        tableData: [],
        tableDataContrato: [],
        tableDataInfoAcademinaca: [],
        tableDataInfoHijos: [],
        tableDataVenciomientos: [],
        tableDataBuscar: [],
        tableDataFuncionario: [],
        tableDataCiudadesExpedicion: [],
        tableDataCiudadesExpedicionNew: [],
        tableDataBarrios: [],
        tableDataBarriosNew: [],
        tableDataDpto: [],
        tableDataDptoNew: [],
        tableDataMpio: [],
        tableDataMpioNew: [],
        optionsDpto: [],
        optionsCiudades: [],
        optionsMunicipio: [],
        optionsNacionalidades: [],
        optionscentroTrabajo: [],
        optionscentroCosto: [],
        optionsBanco: [],
        optionsArp: [],
        optionsEps: [],
        optionsAfp: [],
        optionsCaja: [],
        optionsCampanas: [],
        optionsJefeInmediato: [],
        optionsAreas: [],
        optionsCargos: [],
        optionsConte: [],
        optionsTipoEstudio: [],
        optionsCategoriaDotacion: [],
        optionsEmpresa: [],
        optionsMotivoRetiro: [],
        optionsTipoVencimiento: [],
        optionsGenero: [
          {value: 'M', label: 'MASCULINO'},
          {value: 'F', label: 'FEMENINO'}
        ],
        optionsClaseLibreta: [
          {value: '0', label: 'NO APLICA'},
          {value: '1', label: 'PRIMERA CLASE'},
          {value: '2', label: 'SEGUNDA CLASE'}
        ],
        optionsTipoSanguineo: [
          {value: 'O-', label: 'NEGATIVO', fac_sangre: 'O', fac_sangre1: 'O-', rhrh: '-'},
          {value: 'O+', label: 'POSITIVO', fac_sangre: 'O', fac_sangre1: 'O+', rhrh: '+'},
          {value: 'A-', label: 'NEGATIVO', fac_sangre: 'A', fac_sangre1: 'A-', rhrh: '-'},
          {value: 'A+', label: 'POSITIVO', fac_sangre: 'A', fac_sangre1: 'A+', rhrh: '+'},
          {value: 'B-', label: 'NEGATIVO', fac_sangre: 'B', fac_sangre1: 'B-', rhrh: '-'},
          {value: 'B+', label: 'POSITIVO', fac_sangre: 'B', fac_sangre1: 'B+', rhrh: '+'},
          {value: 'AB-', label: 'NEGATIVO', fac_sangre: 'AB', fac_sangre1: 'AB-', rhrh: '-'},
          {value: 'AB+', label: 'POSITIVO', fac_sangre: 'AB', fac_sangre1: 'AB+', rhrh: '+'}
        ],
        optionsEstadoCivil: [
          {value: '0', label: 'Soltero(a)'},
          {value: '1', label: 'Casado(a)'},
          {value: '2', label: 'Viudo(a)'},
          {value: '3', label: 'Divorsiado(a)'},
          {value: '4', label: 'Union Libre'},
        ],
        optionsConfianza: [
          {value: '0', label: 'SI'},
          {value: '1', label: 'NO'}
        ],
        optionsPerfil: [
          {value: '0', label: 'Administrativo'},
          {value: '1', label: 'Operativo'}
        ],
        optionsTipoContrato: [
          {value: '0', label: 'Termino Fijo (< 1 año)'},
          {value: '1', label: 'Termino Fijo (= 1 año)'},
          {value: '2', label: 'Termino Indefinido'},
          {value: '3', label: 'Labor Pactada (D.O.)'}
        ],
        fileList: [],
        sw: false,
        loading: true,
        loadingSave: false,
        indexUpdate: null,
        rowDelete: 0,
        visibleCreate: true,
        visibleUpdate: false,
        visibleDelete: false,
        stateDate: true,
        modalVisible: false,
        dialogBuscar: false,
        dialogInfoFamiliar: false,
        dialogVisibleCiudadesExpedicion: false,
        dialogVisibleCiudades: false,
        dialogVisibleBarrios: false,
        dialogVisibleCiudadesInfoFamiliar1: false,
        dialogVisibleCiudadesInfoFamiliar2: false,
        dialogVisibleDpto: false,
        dialogVisibleMpio: false,
        stateButton: true,
        stateButton2: true,
        inputSearch: '',
        inputSearchCiudadesExpedicion: '',
        inputSearchCiudades: '',
        inputSearchBarrios: '',
        inputSearchCiudadesInfoFamiliar1: '',
        inputSearchCiudadesInfoFamiliar2: '',
        inputSearchDpto: '',
        inputSearchMpio: '',
        columns: [
          {field: 'mu_codigo', label: 'Código', width: '40'},
          {field: 'mu_municipio', label: 'Municipio'}
        ],
        columnsBarrio: [
          {field: 'ba_codbarrio', label: 'Código'},
          {field: 'ba_nombarrio', label: 'Barrio'}
        ],
        columnsDpto: [
          {field: 'mu_coddpto', label: 'Código'},
          {field: 'mu_departamento', label: 'Departamento'}
        ],
        columnsMpio: [
          {field: 'mu_codigo', label: 'Código', width: '40'},
          {field: 'mu_municipio', label: 'Municipio'}
        ],
        columnsRRHH: [
          {field: 'rh_cedula', label: 'Cedula', width: '60'},
          {field: 'full_name', label: 'Nombre y Apellidos'}
        ],
        fecha: '',
        contrato: 0,
        tituloContrato: '',
        off: true,
        on: true,
        stateButtonContrato: true,
        criterio: 0
      }),
      methods: {
        addFuncionario () {
          if (this.ruleForm.rh_cedula === '' && this.ruleForm.rh_codexpciud === '' && this.ruleForm.rh_apellido1 === '' && this.ruleForm.rh_nombre1 === '' && this.ruleForm.rh_direccion === '' && this.ruleForm.rh_codciudad === '' && this.ruleForm.rh_codbarrio === '' && this.ruleForm.rh_telefono === '' && this.ruleForm.rh_movil === '' && this.ruleForm.rh_sexo === '' && this.ruleForm.rh_estcivil === '' && this.ruleForm.rh_rh === '' && this.ruleForm.rh_fechanac === '' && this.ruleForm.rh_coddptonac === '' && this.ruleForm.rh_codnacional === '' && this.ruleForm.dl_codcentrab === '' && this.ruleForm.dl_codcencosto === '') {
            this.$alert('Faltan campos por diligenciar.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.loadingSave = true
            // var fileValue = document.querySelector('.el-upload .el-upload__input');
            var fileValue = document.querySelector('.form-control-file');
            var data = new FormData();
            // this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
            // this.ruleForm.rh_expcedula = $('#expedida-en option:selected').text();
            // this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
            // this.ruleForm.rh_ciudad = $('#ciudad option:selected').text();
            // this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
            // this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
            this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
            this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
            this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
            this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
            this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
            this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
            this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
            this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
            this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
            this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
            this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
            this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
            this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
            this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
            this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
            this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
            this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
            this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

            var foto = fileValue.files[0] === undefined ? null : fileValue.files[0];
            // validamos si el funcionario tiene contrato
            if (this.tableDataContrato.length !== 0) {
              this.ruleForm.rh_estado = 'ACTIVO';
            } else {
              this.ruleForm.rh_estado = 'INACTIVO';
            }
            // var keys=Object.keys(this.ruleForm);
            // for(i=0;i<keys.length;i++){
            //   data.append(keys[i], this.ruleForm[keys[i]]);
            // }
            // console.log(this.ruleForm)
            // console.log(Object.keys(this.ruleForm));
            //DATOS_RRHH
            data.append('rh_cedula', this.ruleForm.rh_cedula);
            data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
            data.append('rh_expcedula', this.ruleForm.rh_expcedula);
            data.append('rh_apellido1', this.ruleForm.rh_apellido1);
            data.append('rh_apellido2', this.ruleForm.rh_apellido2);
            data.append('rh_nombre', this.ruleForm.rh_nombre);
            data.append('rh_direccion', this.ruleForm.rh_direccion);
            data.append('rh_codciudad', this.ruleForm.rh_codciudad);
            data.append('rh_ciudad', this.ruleForm.rh_ciudad);
            data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
            data.append('rh_barrio', this.ruleForm.rh_barrio);
            data.append('rh_telefono', this.ruleForm.rh_telefono);
            data.append('rh_movil', this.ruleForm.rh_movil);
            data.append('rh_sexo', this.ruleForm.rh_sexo);
            data.append('rh_dessexo', this.ruleForm.rh_dessexo);
            data.append('rh_facsangre', this.ruleForm.rh_facsangre);
            data.append('rh_rh', this.ruleForm.rh_rh1);
            data.append('rh_desrh', this.ruleForm.rh_desrh);
            data.append('rh_estcivil', this.ruleForm.rh_estcivil);
            data.append('rh_fechanac', this.ruleForm.rh_fechanac);
            data.append('rh_codciunac', this.ruleForm.rh_codciunac);
            data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
            data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
            data.append('rh_dptonac', this.ruleForm.rh_dptonac);
            data.append('rh_codnacional', this.ruleForm.rh_codnacional);
            data.append('rh_nacional', this.ruleForm.rh_nacional);
            data.append('rh_estado', this.ruleForm.rh_estado);
            data.append('rh_foto', foto);
            data.append('rh_nombre1', this.ruleForm.rh_nombre1);
            data.append('rh_nombre2', this.ruleForm.rh_nombre2);
            //TECNICOS
            data.append('te_codigo', '');
            data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
            data.append('te_direccion', this.ruleForm.rh_direccion);
            data.append('te_telefono', this.ruleForm.rh_telefono);
            data.append('te_ciudad', this.ruleForm.rh_ciudad);
            data.append('te_cedula', this.ruleForm.rh_cedula);

            data.append('dl_cedula', this.ruleForm.rh_cedula);
            data.append('dl_conte', '');
            data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
            data.append('dl_descentrab', this.ruleForm.dl_descentrab);
            data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
            data.append('dl_descencosto', this.ruleForm.dl_descencosto);
            data.append('dl_salario', this.ruleForm.dl_salario);
            data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
            data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
            data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
            data.append('dl_distritomil', this.ruleForm.dl_distritomil);
            data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
            data.append('dl_fechaven', this.ruleForm.dl_fechaven);
            data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
            data.append('dp_cedula', this.ruleForm.rh_cedula);
            data.append('dp_codarp', this.ruleForm.dp_codarp);
            data.append('dp_desarp', this.ruleForm.dp_desarp);
            data.append('dp_codeps', this.ruleForm.dp_codeps);
            data.append('dp_deseps', this.ruleForm.dp_deseps);
            data.append('dp_afp', this.ruleForm.dp_afp);
            data.append('dp_desafp', this.ruleForm.dp_desafp);
            data.append('dp_codcaja', this.ruleForm.dp_codcaja);
            data.append('dp_descaja', this.ruleForm.dp_descaja);
            data.append('dp_codbanco', this.ruleForm.dp_codbanco);
            data.append('dp_desbanco', this.ruleForm.dp_desbanco);
            data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
            data.append('dp_convencion', this.ruleForm.dp_convencion);
            data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
            //DATOS_EMPRESA
            data.append('em_cedula', this.ruleForm.rh_cedula);
            data.append('em_codingeniero', this.ruleForm.em_codingeniero);
            data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
            data.append('em_programa', this.ruleForm.em_desprograma);
            data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
            data.append('em_codarea', this.ruleForm.em_codarea);
            data.append('em_desarea', this.ruleForm.em_desarea);
            data.append('em_codcargo', this.ruleForm.em_codcargo);
            data.append('em_descargo', this.ruleForm.em_descargo);
            data.append('em_confianza', this.ruleForm.em_confianza);
            data.append('em_perfil', this.ruleForm.em_perfil);
            //DATOS_TALLAS
            data.append('ta_cedula', this.ruleForm.rh_cedula);
            data.append('ta_camisa', this.ruleForm.ta_camisa);
            data.append('ta_pantalon', this.ruleForm.ta_pantalon);
            data.append('ta_botas', this.ruleForm.ta_botas);
            data.append('ta_tipo', this.ruleForm.ta_tipo);

            data.append('dc_cedula', this.ruleForm.rh_cedula);
            data.append('conte', JSON.stringify(this.tableData));
            //DATOS_FORMACADEMICA
            data.append('df_cedula', this.ruleForm.rh_cedula);
            data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));

            data.append('co_cedula', this.ruleForm.rh_cedula);
            data.append('contrato', JSON.stringify(this.tableDataContrato));
            //VENCIMIENTOS
            data.append('cedula', this.ruleForm.rh_cedula);
            data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
            //INFORME_FAMILIAR
            data.append('if_cedula', this.ruleForm.rh_cedula);
            data.append('if_nombrecony', this.ruleFormInfoFamiliar.if_nombrecony);
            data.append('if_fechanaccony', this.ruleFormInfoFamiliar.if_fechanaccony);
            data.append('if_ciudad', this.ruleFormInfoFamiliar.if_ciudad);
            data.append('if_cedulacony', this.ruleFormInfoFamiliar.if_cedulacony);
            data.append('if_ciudadexp', this.ruleFormInfoFamiliar.if_ciudadexp);
            data.append('if_profecony', this.ruleFormInfoFamiliar.if_profecony);
            data.append('if_canhijo', this.ruleFormInfoFamiliar.if_canhijo);
            data.append('if_sexocony', this.ruleFormInfoFamiliar.if_sexocony);
            data.append('if_desciudadexp', this.ruleFormInfoFamiliar.if_desciudadexp);
            data.append('if_desciudad', this.ruleFormInfoFamiliar.if_desciudad);
            //INFORME_HIJOS
            data.append('ih_cedula', this.ruleForm.rh_cedula);
            data.append('hijos', JSON.stringify(this.tableDataInfoHijos));
            
            axios.post('request/insertFuncionario.php', data).then(response => {
              this.loadingSave = false
              if (response.data.error === 0) {
                this.$notify({
                  title: response.data.title,
                  message: response.data.message,
                  type: response.data.type
                });
                this.showFuncionarios();
                this.clearRH();
                this.clear();
                this.clearFAcademica();
                this.clearDatosContrato();
                this.clearVencimientos();
                this.clearInfoFamiliar();
                this.clearInfoHijos();
                document.getElementById('foto').value = '';
                $('#imgFuncionario').attr('src', "img/avatar.jpg");
                this.tableData = [];
                this.tableDataContrato = [];
                this.tableDataInfoAcademinaca = [];
                this.tableDataInfoHijos = [];
                this.tableDataVenciomientos = [];
              } else {
                this.$alert(response.data.message, response.data.title, {
                  confirmButtonText: 'OK'
                });
              }
            }).catch(e => {
              this.loadingSave = false
              console.log(e.response);
            });
          }
        },
        onUpdate () {
          this.loadingSave = true
          var fileValue = document.querySelector('.form-control-file');
          var data = new FormData();

          // this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codexpciud);
          // this.ruleForm.rh_ciudad = this.valueSelect(this.optionsCiudades, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciudad);
          // this.ruleForm.rh_dptonac = this.valueSelect(this.optionsDpto, 'de_codigo', 'de_nombre', this.ruleForm.rh_coddptonac);
          // this.ruleForm.rh_ciudadnac = this.valueSelect(this.optionsMunicipio, 'mu_codigomun', 'mu_nombre', this.ruleForm.rh_codciunac);
          this.ruleForm.rh_dessexo = this.valueSelect(this.optionsGenero, 'value', 'label', this.ruleForm.rh_sexo);
          this.ruleForm.rh_facsangre = this.valueSelect(this.optionsTipoSanguineo, 'value', 'fac_sangre', this.ruleForm.rh_rh);
          this.ruleForm.rh_rh1 = this.valueSelect(this.optionsTipoSanguineo, 'value', 'rhrh', this.ruleForm.rh_rh);
          this.ruleForm.rh_desrh = this.valueSelect(this.optionsTipoSanguineo, 'value', 'label', this.ruleForm.rh_rh);
          this.ruleForm.rh_nacional = this.valueSelect(this.optionsNacionalidades, 'na_codigo', 'na_nacional', this.ruleForm.rh_codnacional);
          this.ruleForm.rh_nombre = this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2;
          this.ruleForm.dl_descentrab = this.valueSelect(this.optionscentroTrabajo, 'ct_codigo', 'ct_centrabajo', this.ruleForm.dl_codcentrab);
          this.ruleForm.dl_descencosto = this.valueSelect(this.optionscentroCosto, 'cc_codigo', 'cc_cencosto', this.ruleForm.dl_codcencosto);
          this.ruleForm.dl_desclaselib = this.valueSelect(this.optionsClaseLibreta, 'value', 'label', this.ruleForm.dl_codclaselib);
          this.ruleForm.dp_desbanco = this.valueSelect(this.optionsBanco, 'ba_codigo', 'ba_nombre', this.ruleForm.dp_codbanco);
          this.ruleForm.dp_desarp = this.valueSelect(this.optionsArp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codarp);
          this.ruleForm.dp_deseps = this.valueSelect(this.optionsEps, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codeps);
          this.ruleForm.dp_desafp = this.valueSelect(this.optionsAfp, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_afp);
          this.ruleForm.dp_descaja = this.valueSelect(this.optionsCaja, 'ss_codigo', 'ss_nombre', this.ruleForm.dp_codcaja);
          this.ruleForm.em_desprograma = this.valueSelect(this.optionsCampanas, 'c_codigo', 'c_nombre', this.ruleForm.em_programa);
          this.ruleForm.em_nomingeniero = this.valueSelect(this.optionsJefeInmediato, 'rh_cedula', 'full_name', this.ruleForm.em_codingeniero);
          this.ruleForm.em_desarea = this.valueSelect(this.optionsAreas, 'ar_codigo', 'ar_area', this.ruleForm.em_codarea);
          this.ruleForm.em_descargo = this.valueSelect(this.optionsCargos, 'ca_codigo', 'ca_cargo', this.ruleForm.em_codcargo);

          var foto = fileValue.files[0] === undefined ? null : fileValue.files[0];
          if (this.tableDataContrato.length !== 0) {
            this.ruleForm.rh_estado = 'ACTIVO';
          } else {
            this.ruleForm.rh_estado = 'INACTIVO';
          }

          if (this.tableDataContrato[0].co_fechafin || this.fecha.split('/')[2] > this.fecha.split('/')[2]) {
            this.ruleForm.rh_estado = 'INACTIVO'
          }

          if (this.tableDataContrato[0].co_fecharet) {
            this.ruleForm.rh_estado = 'INACTIVO'
          }

          //DATOS_RRHH
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          data.append('rh_codexpciud', this.ruleForm.rh_codexpciud);
          data.append('rh_expcedula', this.ruleForm.rh_expcedula);
          data.append('rh_apellido1', this.ruleForm.rh_apellido1);
          data.append('rh_apellido2', this.ruleForm.rh_apellido2);
          data.append('rh_nombre', this.ruleForm.rh_nombre);
          data.append('rh_direccion', this.ruleForm.rh_direccion);
          data.append('rh_codciudad', this.ruleForm.rh_codciudad);
          data.append('rh_ciudad', this.ruleForm.rh_ciudad);
          data.append('rh_codbarrio', this.ruleForm.rh_codbarrio);
          data.append('rh_barrio', this.ruleForm.rh_barrio);
          data.append('rh_telefono', this.ruleForm.rh_telefono);
          data.append('rh_movil', this.ruleForm.rh_movil);
          data.append('rh_sexo', this.ruleForm.rh_sexo);
          data.append('rh_dessexo', this.ruleForm.rh_dessexo);
          data.append('rh_facsangre', this.ruleForm.rh_facsangre);
          data.append('rh_rh', this.ruleForm.rh_rh1);
          data.append('rh_desrh', this.ruleForm.rh_desrh);
          data.append('rh_estcivil', this.ruleForm.rh_estcivil);
          data.append('rh_fechanac', this.ruleForm.rh_fechanac);
          data.append('rh_codciunac', this.ruleForm.rh_codciunac);
          data.append('rh_ciudadnac', this.ruleForm.rh_ciudadnac);
          data.append('rh_coddptonac', this.ruleForm.rh_coddptonac);
          data.append('rh_dptonac', this.ruleForm.rh_dptonac);
          data.append('rh_codnacional', this.ruleForm.rh_codnacional);
          data.append('rh_nacional', this.ruleForm.rh_nacional);
          data.append('rh_estado', this.ruleForm.rh_estado);
          data.append('rh_foto', foto);
          data.append('rh_nombre1', this.ruleForm.rh_nombre1);
          data.append('rh_nombre2', this.ruleForm.rh_nombre2);
          //TECNICOS
          data.append('te_codigo', this.ruleForm.te_codigo);
          data.append('te_nombres', this.ruleForm.rh_nombre1 + ' ' + this.ruleForm.rh_nombre2 + ' ' + this.ruleForm.rh_apellido1 + ' ' + this.ruleForm.rh_apellido2);
          data.append('te_direccion', this.ruleForm.rh_direccion);
          data.append('te_telefono', this.ruleForm.rh_telefono);
          data.append('te_ciudad', this.ruleForm.rh_ciudad);
          data.append('te_cedula', this.ruleForm.rh_cedula);
          //DATOS_LEGALES
          data.append('dl_cedula', this.ruleForm.rh_cedula);
          data.append('dl_conte', '');
          data.append('dl_codcentrab', this.ruleForm.dl_codcentrab);
          data.append('dl_descentrab', this.ruleForm.dl_descentrab);
          data.append('dl_codcencosto', this.ruleForm.dl_codcencosto);
          data.append('dl_descencosto', this.ruleForm.dl_descencosto);
          data.append('dl_salario', this.ruleForm.dl_salario);
          data.append('dl_nlibreta', this.ruleForm.dl_nlibreta);
          data.append('dl_codclaselib', this.ruleForm.dl_codclaselib);
          data.append('dl_desclaselib', this.ruleForm.dl_desclaselib);
          data.append('dl_distritomil', this.ruleForm.dl_distritomil);
          data.append('dl_certjudicial', this.ruleForm.dl_certjudicial);
          data.append('dl_fechaven', this.ruleForm.dl_fechaven);
          data.append('dl_hosrasdiarias', this.ruleForm.dl_hosrasdiarias);
          //DATOS_PARAFISCALES
          data.append('dp_cedula', this.ruleForm.rh_cedula);
          data.append('dp_codarp', this.ruleForm.dp_codarp);
          data.append('dp_desarp', this.ruleForm.dp_desarp);
          data.append('dp_codeps', this.ruleForm.dp_codeps);
          data.append('dp_deseps', this.ruleForm.dp_deseps);
          data.append('dp_afp', this.ruleForm.dp_afp);
          data.append('dp_desafp', this.ruleForm.dp_desafp);
          data.append('dp_codcaja', this.ruleForm.dp_codcaja);
          data.append('dp_descaja', this.ruleForm.dp_descaja);
          data.append('dp_codbanco', this.ruleForm.dp_codbanco);
          data.append('dp_desbanco', this.ruleForm.dp_desbanco);
          data.append('dp_ncuenta', this.ruleForm.dp_ncuenta);
          data.append('dp_convencion', this.ruleForm.dp_convencion);
          data.append('dp_adicionarp', this.ruleForm.dp_adicionarp);
          //DATOS_EMPRESA
          data.append('em_cedula', this.ruleForm.rh_cedula);
          data.append('em_codingeniero', this.ruleForm.em_codingeniero);
          data.append('em_nomingeniero', this.ruleForm.em_nomingeniero);
          data.append('em_programa', this.ruleForm.em_desprograma);
          data.append('em_cuadrilla', this.ruleForm.em_cuadrilla);
          data.append('em_codarea', this.ruleForm.em_codarea);
          data.append('em_desarea', this.ruleForm.em_desarea);
          data.append('em_codcargo', this.ruleForm.em_codcargo);
          data.append('em_descargo', this.ruleForm.em_descargo);
          data.append('em_confianza', this.ruleForm.em_confianza);
          data.append('em_perfil', this.ruleForm.em_perfil);
          //DATOS_TALLAS
          data.append('ta_cedula', this.ruleForm.rh_cedula);
          data.append('ta_camisa', this.ruleForm.ta_camisa);
          data.append('ta_pantalon', this.ruleForm.ta_pantalon);
          data.append('ta_botas', this.ruleForm.ta_botas);
          data.append('ta_tipo', this.ruleForm.ta_tipo);
          //DATOS_CONTE
          data.append('dc_cedula', this.ruleForm.rh_cedula);
          data.append('conte', JSON.stringify(this.tableData));

          //DATOS_CONTRATOS
          data.append('co_cedula', this.ruleForm.rh_cedula);
          data.append('contrato', JSON.stringify(this.tableDataContrato));

          //DATOS_FORMACADEMICA
          data.append('df_cedula', this.ruleForm.rh_cedula);
          data.append('facademica', JSON.stringify(this.tableDataInfoAcademinaca));

          //INFORME_FAMILIAR
          data.append('if_cedula', this.ruleForm.rh_cedula);
          data.append('if_nombrecony', this.ruleFormInfoFamiliar.if_nombrecony);
          data.append('if_fechanaccony', this.ruleFormInfoFamiliar.if_fechanaccony);
          data.append('if_ciudad', this.ruleFormInfoFamiliar.if_ciudad);
          data.append('if_cedulacony', this.ruleFormInfoFamiliar.if_cedulacony);
          data.append('if_ciudadexp', this.ruleFormInfoFamiliar.if_ciudadexp);
          data.append('if_profecony', this.ruleFormInfoFamiliar.if_profecony);
          // data.append('if_canhijo', this.ruleFormInfoFamiliar.if_canhijo);
          data.append('if_sexocony', this.ruleFormInfoFamiliar.if_sexocony);
          data.append('if_desciudadexp', this.ruleFormInfoFamiliar.if_desciudadexp);
          data.append('if_desciudad', this.ruleFormInfoFamiliar.if_desciudad);

          //INFORME_HIJOS
          data.append('ih_cedula', this.ruleForm.rh_cedula);
          data.append('hijos', JSON.stringify(this.tableDataInfoHijos));

          //VENCIMIENTOS
          data.append('cedula', this.ruleForm.rh_cedula);
          data.append('vencimientos', JSON.stringify(this.tableDataVenciomientos));
          axios.post('request/updateFuncionario.php', data).then(response => {
            console.log(response.data.message);
            this.loadingSave = false
            if (response.data.error === 0) {
              this.$notify({
                title: response.data.title,
                message: response.data.message,
                type: response.data.type
              });
              this.showFuncionarios();
              this.visibleCreate = true;
              this.visibleUpdate = false;
              this.clearRH();
              this.clear();
              this.clearFAcademica();
              this.clearDatosContrato();
              this.clearVencimientos();
              this.clearInfoFamiliar();
              this.clearInfoHijos();
              document.getElementById('foto').value = '';
              $('#imgFuncionario').attr('src', "img/avatar.jpg");
              this.tableData = [];
              this.tableDataContrato = [];
              this.tableDataInfoAcademinaca = [];
              this.tableDataInfoHijos = [];
              this.tableDataVenciomientos = [];
            } else {
              this.$alert(response.data.message, response.data.title, {
                confirmButtonText: 'OK'
              });
            }
          }).catch(e => {
            this.loadingSave = false
            console.log(e.response);
          });
        },
        onCancel () {
          this.visibleCreate = true;
          this.visibleUpdate = false;
          this.clearRH();
          this.clear();
          this.clearFAcademica();
          this.clearDatosContrato();
          this.clearVencimientos();
          this.clearInfoFamiliar();
          this.clearInfoHijos();
          document.getElementById('foto').value = '';
          $('#imgFuncionario').attr('src', "img/avatar.jpg");
          this.tableData = [];
          this.tableDataContrato = [];
          this.tableDataInfoAcademinaca = [];
          this.tableDataInfoHijos = [];
          this.tableDataVenciomientos = [];             
        },
        onDelete () {
          // this.ruleForm.rh_expcedula = this.valueSelect(this.optionsCiudades, 'mu_codigo', 'mu_municipio', this.ruleForm.rh_codexpciud);
          // console.log(this.ruleForm.rh_dptonac);
          console.log(JSON.stringify(this.tableData));
          // var data = new FormData();
          // data.append('dc_codconte', this.ruleFormConte.dc_codconte);
          // axios.post('request/deleteFuncionario.php', data).then(response => {
          //   console.log(response.data.message);
          //   // if (response.data.error === 0) {
          //   //   this.$notify({
          //   //     title: response.data.title,
          //   //     message: response.data.message,
          //   //     type: response.data.type
          //   //   });
          //   // }
          // }).catch(e => {
          //   console.log(e.response);
          // });
        },
        showCiudades () {
          axios.get('request/ciudades.php').then(response => {
            this.tableDataCiudadesExpedicionNew = response.data;
            this.tableDataCiudadesExpedicion = response.data;
          }).then(response => {
            this.showDepartamentos();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showDepartamentos () {
          axios.get('request/departamentos.php').then(response => {
            this.tableDataDptoNew = response.data;
            this.tableDataDpto = response.data;
          }).then(response => {
            this.showNacionalidades();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showMunicipios (id) {
          //this.ruleForm.rh_coddptonac
          var mu_depto = new FormData();
          mu_depto.append('mu_depto', id);
          axios.post('request/municipios.php', mu_depto).then(response => {
            console.log(response.data);
            if (response.data.length !== 0) {
              this.stateButton2 = false;
            } else {
              this.stateButton2 = true;
            }
            this.tableDataMpioNew = response.data;
            this.tableDataMpio = response.data;
          }).catch(error => {
            console.log(error.response);
          })
        },
        showBarrios (ba_depto, ba_mpio) {
          var data = new FormData();
          // this.ruleForm.rh_codciudad = $('#ciudad').val();.substring(2, 5)
          data.append('ba_depto', ba_depto);
          data.append('ba_mpio', ba_mpio);
          axios.post('request/barrios.php', data).then(response => {
            console.log(response.data)
            if (response.data.length !== 0) {
              this.stateButton = false;
            } else {
              this.stateButton = true;
            }
            this.tableDataBarriosNew = response.data;
            this.tableDataBarrios = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showNacionalidades () {
          this.loading = true
          axios.get('request/nacionalidades.php').then(response => {
            this.optionsNacionalidades = response.data;
          }).then(response => {
            this.showCentroTrabajo();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCentroTrabajo () {
          axios.get('request/centroTrabajo.php').then(response => {
            this.optionscentroTrabajo = response.data;
          }).then(response => {
            this.showCentroCosto();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCentroCosto () {
          axios.get('request/centroCosto.php').then(response => {
            this.optionscentroCosto = response.data;
          }).then(response => {
            this.showARP();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showARP () {
          axios.get('request/arp.php').then(response => {
            this.optionsArp = response.data;
          }).then(response => {
            this.showEPS();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showEPS () {
          axios.get('request/eps.php').then(response => {
            this.optionsEps = response.data;
          }).then(response => {
            this.showAFP();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showAFP () {
          axios.get('request/afp.php').then(response => {
            this.optionsAfp = response.data;
          }).then(response => {
            this.showCaja();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCaja () {
          axios.get('request/caja.php').then(response => {
            this.optionsCaja = response.data;
          }).then(response => {
            this.showBancos();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showBancos () {
          axios.get('request/bancos.php').then(response => {
            this.optionsBanco = response.data;
          }).then(response => {
            this.showJefeInmediato();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showJefeInmediato () {
          axios.get('request/datos_rrhh.php').then(response => {
            this.optionsJefeInmediato = response.data;
          }).then(response => {
            this.showCampanas();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCampanas () {
          axios.get('request/campanas.php').then(response => {
            this.optionsCampanas = response.data;
          }).then(response => {
            this.showAreas();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showAreas () {
          axios.get('request/areas.php').then(response => {
            this.optionsAreas = response.data;
          }).then(response => {
            this.showCargos();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCargos () {
          axios.get('request/cargos.php').then(response => {
            this.optionsCargos = response.data;
          }).then(response => {
            this.showConte();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showConte () {
          axios.get('request/conte.php').then(response => {
            this.optionsConte = response.data;
          }).then(response => {
            this.showTipoEstudio();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showTipoEstudio () {
          axios.get('request/tipo_estudio.php').then(response => {
            this.optionsTipoEstudio = response.data;
          }).then(response => {
            this.showCategoriaDotacion();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showCategoriaDotacion () {
          axios.get('request/categoria_dotacion.php').then(response => {
            this.optionsCategoriaDotacion = response.data;
          }).then(response => {
            this.showEmpresa();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showEmpresa () {
          axios.get('request/empresa.php').then(response => {
            this.optionsEmpresa = response.data;
          }).then(response => {
            this.showMotivoRetiro();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showMotivoRetiro () {
          axios.get('request/motivo_retiro.php').then(response => {
            this.optionsMotivoRetiro = response.data;
          }).then(response => {
            this.showTipoVencimiento();
          }).catch(e => {
            console.log(e.response);
          });
        },
        showTipoVencimiento () {
          axios.get('request/tipo_vencimiento.php').then(response => {
            this.optionsTipoVencimiento = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        showFuncionarios () {
          this.loading = true
          // var data = new FormData();
          // data.append('funcionario', this.inputSearch);
          axios.get('request/getFuncionario.php').then(response => {
            this.tableDataFuncionario = response.data;
            this.tableDataBuscar = response.data;
            this.loading = false
          }).catch(e => {
            console.log(e.response);
            this.loading = false
          });
        },
        addDatosConte () {
          this.ruleFormConte.dc_desconte = this.valueSelect(this.optionsConte, 'co_codigo', 'co_nombre', this.ruleFormConte.dc_codconte);
          var data = this.ruleFormConte;
          if (this.ruleFormConte.dc_codconte === '') {
            this.$alert('Por favor diligenciar campo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            var arrClaves = [];
            for (var i = 0; i < this.tableData.length; i++) {
              arrClaves.push(this.tableData[i].dc_codconte);
            }
            var arrUnico = Array.from(new Set(arrClaves));
            if (!arrUnico.includes(this.ruleFormConte.dc_codconte)) {
              this.tableData.push(data);
              this.clear();
            } else {
              this.$alert('Codigo de conte ya ingresado', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            }
          }
        },
        deleteRow (row, index) {
          console.log(row.dc_codconte)
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            if (this.rowDelete === 0) {
              this.$delete(this.tableData, index);
              this.$message({
                type: 'success',
                message: 'Registro eliminado con exito.'
              });
            } else {
              var data = new FormData();
              data.append('dc_codconte', row.dc_codconte);
              axios.post('request/deleteDatosConte.php', data).then(response => {
                this.$message({
                  type: 'success',
                  message: 'Registro eliminado con exito..!'
                });
              }).catch(e => {
                console.log(e.response);
              });
              this.$delete(this.tableData, index);
            }
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addFormacionAcademica () {
          this.ruleFormFAcademica.df_formacademica = this.valueSelect(this.optionsTipoEstudio, 'es_codigo', 'es_nombre', this.ruleFormFAcademica.df_codigo);
          var data = this.ruleFormFAcademica;
          if (this.ruleFormFAcademica.df_codigo === '') {
            this.$alert('Por favor diligenciar campo tipo estudio.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormFAcademica.df_insteducativa === '') {
            this.$alert('Por favor diligenciar campo Ins. Educativa.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormFAcademica.df_tituloobt === '') {
            this.$alert('Por favor diligenciar campo titulo obtenido.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableDataInfoAcademinaca.push(data);
            this.clearFAcademica();
          }
        },
        deleteRowFormacademica (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataInfoAcademinaca, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addDatosContrato () {
          this.ruleFormDatosContrato.co_empresa = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_motivoretiro = this.valueSelect(this.optionsMotivoRetiro, 'mr_codigo', 'mr_motivo', this.ruleFormDatosContrato.co_codretiro);
          // this.ruleFormDatosContrato.co_motivoreal = this.valueSelect(this.optionsEmpresa, 'sw', 'nombre', this.ruleFormDatosContrato.co_codempresa);
          this.ruleFormDatosContrato.co_item = this.tableDataContrato.length + 1;
          var data = this.ruleFormDatosContrato;
          if (!this.sw) {
            switch (this.criterio) {
              case 0:
                if (this.ruleFormDatosContrato.co_empresa === '' || this.ruleFormDatosContrato.co_fechaing === '' || this.ruleFormDatosContrato.co_tcontrato === '') {
                  this.$alert('Por favor diligenciar campos.', 'Validación..!', {
                    confirmButtonText: 'OK'
                  });
                } else {
                  if (this.ruleFormDatosContrato.co_tcontrato === '0') {
                    if (this.ruleFormDatosContrato.co_codempresa === '') {
                      this.$alert('Por favor diligenciar campo empresa.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechaing === '') {
                      this.$alert('Por favor diligenciar campo fecha ingreso.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechafin === '') {
                      this.$alert('Por favor diligenciar campo fecha fin.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_tcontrato === '') {
                      this.$alert('Por favor diligenciar campo tipo contrato.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else {
                      this.tableDataContrato.push(data);
                      this.modalVisible = false;
                      this.clearDatosContrato();
                    }
                  } else if (this.ruleFormDatosContrato.co_tcontrato === '1') {
                    if (this.ruleFormDatosContrato.co_codempresa === '') {
                      this.$alert('Por favor diligenciar campo empresa.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechaing === '') {
                      this.$alert('Por favor diligenciar campo fecha ingreso.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    }  else if (this.ruleFormDatosContrato.co_tcontrato === '') {
                      this.$alert('Por favor diligenciar campo tipo contrato.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else {
                      this.tableDataContrato.push(data);
                      this.modalVisible = false;
                      this.clearDatosContrato();
                    }
                  } else if (this.ruleFormDatosContrato.co_tcontrato === '2') {
                    if (this.ruleFormDatosContrato.co_codempresa === '') {
                      this.$alert('Por favor diligenciar campo empresa.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechaing === '') {
                      this.$alert('Por favor diligenciar campo fecha ingreso.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_tcontrato === '') {
                      this.$alert('Por favor diligenciar campo tipo contrato.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else {
                      this.tableDataContrato.push(data);
                      this.modalVisible = false;
                      this.clearDatosContrato();
                    }
                  } else if (this.ruleFormDatosContrato.co_tcontrato === '3') {
                    if (this.ruleFormDatosContrato.co_codempresa === '') {
                      this.$alert('Por favor diligenciar campo empresa.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechaing === '') {
                      this.$alert('Por favor diligenciar campo fecha ingreso.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_fechafin === '') {
                      this.$alert('Por favor diligenciar campo fecha fin.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else if (this.ruleFormDatosContrato.co_tcontrato === '') {
                      this.$alert('Por favor diligenciar campo tipo contrato.', 'Validación..!', {
                        confirmButtonText: 'OK'
                      });
                    } else {
                      // this.tableDataContrato.push(data);
                      // this.modalVisible = false;
                      // this.clearDatosContrato();
                      this.tableDataContrato.splice(this.indexUpdate, 1, data);
                      // this.sw = false;
                      this.sw = 1;
                      this.modalVisible = false;
                      this.clearDatosContrato();
                    }
                  }
                }
                break;
              case 1:
                break;
            }
          } else {
            if (this.ruleFormDatosContrato.co_fecharet === '') {
              this.$alert('Por favor diligenciar campos fecha retiro.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else if (this.ruleFormDatosContrato.co_motivoretiro === '') {
              this.$alert('Por favor diligenciar campos motivo retiro.', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            } else {
              this.tableDataContrato.splice(this.indexUpdate, 1, data);
              // this.sw = false;
              this.sw = 1;
              this.modalVisible = false;
              this.clearDatosContrato();
            }
            
          }
        },
        editRowContrato (row, index) {
          if (row.co_tcontrato === '0') {
            this.stateDate = false;
          } else if (row.co_tcontrato === '1') {
            this.stateDate = true;
            var data = new FormData();
            data.append('fecha', row.co_fechaing);
            axios.post('request/contratacionAllo.php', data).then(response => {
              row.co_fechafin = response.data.allo;
              // this.ruleForm.rh_edad = response.data.edad;
            }).catch(e => {
              console.log(e.response);
            });
          } else if (row.co_tcontrato === '2') {
            this.stateDate = true;
            row.co_fechafin = '';
          } else if (row.co_tcontrato === '3') {
            this.stateDate = false;
          }
          console.log(row)
          this.ruleFormDatosContrato.co_codempresa = row.co_codempresa;
          this.ruleFormDatosContrato.co_empresa = row.co_empresa;
          this.ruleFormDatosContrato.co_fechaing = row.co_fechaing;
          this.ruleFormDatosContrato.co_fechafin = row.co_fechafin;
          this.ruleFormDatosContrato.co_codretiro = row.co_codretiro;
          this.ruleFormDatosContrato.co_motivoretiro = row.co_motivoretiro;
          this.ruleFormDatosContrato.co_tcontrato = row.co_tcontrato;
          var data = this.ruleFormDatosContrato;
          this.indexUpdate = index;
          this.modalVisible = true;
          this.sw = 1;
          this.criterio = 1;
          this.off = false;
          this.stateButtonContrato = false;
        },
        deleteRowContrato (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataContrato, index);
            this.sw = false;
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addVencimientos () {
          // this.ruleFormVencimientos.destipovence = this.valueSelect(this.optionsTipoVencimiento, 'tvcodigo', 'tvdescripcion', this.ruleFormVencimientos.codtipovence);
          var data = this.ruleFormVencimientos;
          if (this.ruleFormVencimientos.codtipovence === '') {
            this.$alert('Por favor diligenciar campo tipo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormVencimientos.descripcion === '') {
            this.$alert('Por favor diligenciar campo descripción.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormVencimientos.fechavence === '') {
            this.$alert('Por favor diligenciar campo fecha.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            var arrClaves = [];
            for (var i = 0; i < this.tableDataVenciomientos.length; i++) {
              arrClaves.push(this.tableDataVenciomientos[i].codtipovence);
            }
            var arrUnico = Array.from(new Set(arrClaves));
            if (!arrUnico.includes(this.ruleFormVencimientos.codtipovence)) {
              this.tableDataVenciomientos.push(data);
              this.clearVencimientos();
            } else {
              this.$alert('Codigo ya ingresado', 'Validación..!', {
                confirmButtonText: 'OK'
              });
            }
            // this.tableDataVenciomientos.push(data);
            // this.clearVencimientos();
          }
        },
        deleteRowVencimiento (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataVenciomientos, index);
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        addInfoFamiliar () {
          var dataHijos = this.ruleFormInfoHijos;
          if (this.ruleFormInfoHijos.ih_nombrehijo === '') {
            this.$alert('Por favor diligenciar campo nombre del hijo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_fechanac === '') {
            this.$alert('Por favor diligenciar campo fecha nac.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_identifica === '') {
            this.$alert('Por favor diligenciar campo d.i / reg civil.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else if (this.ruleFormInfoHijos.ih_sexo === '') {
            this.$alert('Por favor diligenciar campo sexo.', 'Validación..!', {
              confirmButtonText: 'OK'
            });
          } else {
            this.tableDataInfoHijos.push(dataHijos);
            this.clearInfoHijos();
          }
        },
        deleteRowInfoHijos (row, index) {
          this.$confirm('Está seguro de eliminar el registro?', 'Warning', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.$delete(this.tableDataInfoHijos, index);
            var cant = this.ruleFormInfoHijos.cantidad;
            cant = this.tableDataInfoHijos.length - 1;
            this.ruleFormInfoFamiliar.if_canhijo = cant;
            console.log(cant)
            this.$message({
              type: 'success',
              message: 'Registro eliminado con exito.'
            });
          }).catch(() => {
            this.$message({
              type: 'info',
              message: 'Operación cancelada por el usuario.'
            });
          });
        },
        rowClick (row) {
          console.log(row)
          this.ruleForm.rh_cedula = row.rh_cedula
          this.ruleForm.rh_codexpciud = row.rh_codexpciud
          this.ruleForm.rh_expcedula = row.rh_expcedula
          this.ruleForm.rh_apellido1 = row.rh_apellido1
          this.ruleForm.rh_apellido2 = row.rh_apeelido2
          this.ruleForm.rh_nombre1 = row.rh_nombre1
          this.ruleForm.rh_nombre2 = row.rh_nombre2
          this.ruleForm.rh_direccion = row.rh_direccion
          this.ruleForm.rh_codciudad = row.rh_codciudad
          this.ruleForm.rh_ciudad = row.rh_ciudad
          this.showBarrios(row.mu_coddpto, row.codigo);
          this.ruleForm.rh_codbarrio = row.rh_codbarrio
          this.ruleForm.rh_barrio = row.rh_barrio
          this.ruleForm.rh_telefono = row.rh_telefono
          this.ruleForm.rh_movil = row.rh_movil
          this.ruleForm.rh_sexo = row.rh_sexo
          this.ruleForm.rh_dessexo = row.rh_dessexo
          this.ruleForm.rh_facsangre = row.rh_facsangre
          this.ruleForm.rh_rh = row.tipo_sangre
          this.ruleForm.rh_desrh = row.rh_desrh
          this.ruleForm.rh_estcivil = row.rh_estcivil
          this.ruleForm.rh_fechanac = row.rh_fechanac
          this.ruleForm.rh_coddptonac = row.rh_coddptonac
          this.ruleForm.rh_dptonac = row.rh_dptonac
          this.ruleForm.rh_codciunac = row.rh_codciudnac
          this.ruleForm.rh_ciudadnac = row.rh_ciudadnac
          this.ruleForm.rh_codnacional = row.rh_codnacional
          this.ruleForm.rh_nacional = row.rh_nacional
          this.ruleForm.rh_estado = row.rh_estado
          // this.ruleForm.rh_edad = row.edad
          this.showMunicipios(row.rh_coddptonac);
          this.ruleForm.te_codigo = row.te_codigo
          this.ruleForm.dl_codcentrab = row.dl_codcentrab
          this.ruleForm.dl_descentrab = row.dl_descentrab
          this.ruleForm.dl_codcencosto = row.dl_codcencosto
          this.ruleForm.dl_descencosto = row.dl_descencosto
          this.ruleForm.dl_salario = row.dl_salario
          this.ruleForm.dl_nlibreta = row.dl_nlibreta
          this.ruleForm.dl_codclaselib = row.dl_codclaselib
          this.ruleForm.dl_desclaselib = row.dl_desclaselib
          this.ruleForm.dl_distritomil = row.dl_distritomil
          this.ruleForm.dl_certjudicial = row.dl_certjudicial
          this.ruleForm.dl_fechaven = row.dl_fechaven
          this.ruleForm.dl_hosrasdiarias = row.dl_horasdiarias
          this.ruleForm.dl_comphoras = Math.round(row.dl_salario / 30 / row.dl_horasdiarias);
          this.ruleForm.dl_salario_letras = numeroALetras(row.dl_salario);
          this.ruleForm.dp_codarp = row.dp_codarp
          this.ruleForm.dp_desarp = row.dp_desarp
          this.ruleForm.dp_codeps = row.dp_codeps
          this.ruleForm.dp_deseps = row.dp_deseps
          this.ruleForm.dp_afp = row.dp_codafp
          this.ruleForm.dp_desafp = row.dp_desafp
          this.ruleForm.dp_codcaja = row.dp_codcaja
          this.ruleForm.dp_descaja = row.dp_descaja
          this.ruleForm.dp_codbanco = row.dp_codbanco
          this.ruleForm.dp_desbanco = row.dp_desbanco
          this.ruleForm.dp_ncuenta = row.dp_ncuenta
          this.ruleForm.dp_adicionarp = row.dp_adicionarp
          this.ruleForm.em_codingeniero = row.em_codingeniero
          this.ruleForm.em_nomingeniero = row.em_nomingeniero
          this.ruleForm.em_programa = row.em_programa
          this.ruleForm.em_codarea = row.em_codarea
          this.ruleForm.em_desarea = row.em_desarea
          this.ruleForm.em_codcargo = row.em_codcargo
          this.ruleForm.em_descargo = row.em_descargo
          this.ruleForm.em_codigo = row.em_codigo
          this.ruleForm.em_confianza = row.em_confianza
          this.ruleForm.em_perfil = row.em_perfil
          this.ruleForm.ta_camisa = row.ta_camisa
          this.ruleForm.ta_pantalon = row.ta_pantalon
          this.ruleForm.ta_botas = row.ta_botas
          this.ruleForm.ta_tipo = row.ta_tipo
          this.ruleFormInfoFamiliar.if_cedula = row.if_cedula
          this.ruleFormInfoFamiliar.if_nombrecony = row.if_nombrecony
          this.ruleFormInfoFamiliar.if_fechanaccony = row.if_fechanaccony
          this.ruleFormInfoFamiliar.if_ciudad = row.if_ciudad
          this.ruleFormInfoFamiliar.if_desciudad = row.if_desciudad
          this.ruleFormInfoFamiliar.if_cedulacony = row.if_cedulacony
          this.ruleFormInfoFamiliar.if_ciudadexp = row.if_ciudadexp
          this.ruleFormInfoFamiliar.if_desciudadexp = row.if_desciudadexp
          this.ruleFormInfoFamiliar.if_profecony = row.if_profecony
          this.ruleFormInfoFamiliar.if_canhijo = row.if_canhijo
          this.ruleFormInfoFamiliar.if_sexocony = row.if_sexocony
          this.ruleFormInfoFamiliar.if_dessexocony = row.if_dessexocony
          var data = new FormData();
          data.append('fecha_nac', row.rh_fechanac);
          axios.post('request/calculateAge.php', data).then(response => {
            this.ruleForm.rh_edad = response.data.edad;
          }).catch(e => {
            console.log(e.response);
          });
          this.getDatosConte(row.rh_cedula);
          this.getDatosContrato(row.rh_cedula);
          this.getDatosFormacademica(row.rh_cedula);
          // this.getInfoFamiliar(row.rh_cedula);
          this.getInfoHijos(row.rh_cedula);
          this.getVencimientos(row.rh_cedula);
          if (row.rh_foto !== '') {
            $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+row.rh_cedula);
          } else {
            $('#imgFuncionario').attr('src', "img/avatar.jpg");
          }
          if (this.tableDataContrato[0].co_fecharet) {
            this.stateButtonContrato = false;
          }
          if (row.rh_estado === 'INACTIVO') {
            this.active = true
          } else {
            this.active = false
          }
          this.rowDelete = 1;
          this.dialogBuscar = false;
          this.visibleUpdate = true;
          this.visibleCreate = false;
          // this.stateButton = true;
          // this.stateButton2 = true;
        },
        rowClickCiudadesExpedicion (row) {
          this.ruleForm.rh_codexpciud = row.mu_codigo
          this.ruleForm.rh_expcedula = row.mu_municipio
          this.dialogVisibleCiudadesExpedicion = false;
        },
        rowClickCiudades (row) {
          console.log(row);
          this.ruleForm.rh_codciudad = row.mu_codigo
          this.ruleForm.rh_ciudad = row.mu_municipio
          this.showBarrios(row.mu_coddpto, row.cod);
          this.dialogVisibleCiudades = false;
        },
        rowClickCiudadesInfoFamiliar1 (row) {
          this.ruleFormInfoFamiliar.if_ciudadexp = row.mu_codigo
          this.ruleFormInfoFamiliar.if_desciudadexp = row.mu_municipio
          this.dialogVisibleCiudadesInfoFamiliar1 = false;
        },
        rowClickCiudadesInfoFamiliar2 (row) {
          this.ruleFormInfoFamiliar.if_ciudad = row.mu_codigo
          this.ruleFormInfoFamiliar.if_desciudad = row.mu_municipio
          this.dialogVisibleCiudadesInfoFamiliar2 = false;
        },
        rowClickBarrios (row) {
          this.ruleForm.rh_codbarrio = row.ba_codbarrio
          this.ruleForm.rh_barrio = row.ba_nombarrio
          this.dialogVisibleBarrios = false;
        },
        rowClickDpto (row) {
          this.ruleForm.rh_coddptonac = row.mu_coddpto
          this.ruleForm.rh_dptonac = row.mu_departamento
          this.ruleForm.rh_codciunac = '';
          this.ruleForm.rh_ciudadnac = '';
          // this.showDepartamentos();
          this.showMunicipios(row.mu_coddpto);
          this.dialogVisibleDpto = false;
        },
        rowClickMpio (row) {
          this.ruleForm.rh_codciunac = row.mu_codigo
          this.ruleForm.rh_ciudadnac = row.mu_municipio
          this.dialogVisibleMpio = false;
        },
        getFuncionarioBY () {
          var data = new FormData();
          data.append('rh_cedula', this.ruleForm.rh_cedula);
          axios.post('request/getFuncionarioBY.php', data).then(response => {
            console.log(response.data[0])
            if (response.data[0] === undefined) {
              this.visibleCreate = true;
              this.visibleUpdate = false;
              this.ruleForm.rh_codexpciud = '';
              this.ruleForm.rh_expcedula = '';
              this.ruleForm.rh_apellido1 = '';
              this.ruleForm.rh_apellido2 = '';
              this.ruleForm.rh_nombre = '';
              this.ruleForm.rh_direccion = '';
              this.ruleForm.rh_codciudad = '';
              this.ruleForm.rh_ciudad = '';
              this.ruleForm.rh_codbarrio = '';
              this.ruleForm.rh_barrio = '';
              this.ruleForm.rh_telefono = '';
              this.ruleForm.rh_movil = '';
              this.ruleForm.rh_sexo = '';
              this.ruleForm.rh_dessexo = '';
              this.ruleForm.rh_facsangre = '';
              this.ruleForm.rh_rh = '';
              this.ruleForm.rh_rh1 = '';
              this.ruleForm.rh_desrh = '';
              this.ruleForm.rh_estcivil = '';
              this.ruleForm.rh_estcivil = '';
              this.ruleForm.rh_fechanac = '';
              this.ruleForm.rh_codciunac = '';
              this.ruleForm.rh_ciudadnac = '';
              this.ruleForm.rh_coddptonac = '';
              this.ruleForm.rh_dptonac = '';
              this.ruleForm.rh_codnacional = '';
              this.ruleForm.rh_nacional = '';
              this.ruleForm.rh_estado = '';
              this.ruleForm.rh_foto = '';
              this.ruleForm.rh_profesion = '';
              this.ruleForm.rh_explaboral = '';
              this.ruleForm.rh_nombre1 = '';
              this.ruleForm.rh_nombre2 = '';
              this.ruleForm.rh_edad = '';
              this.ruleForm.dl_codcentrab = '';
              this.ruleForm.dl_descentrab = '';
              this.ruleForm.dl_codcencosto = '';
              this.ruleForm.dl_descencosto = '';
              this.ruleForm.dl_salario = '';
              this.ruleForm.dl_nlibreta = '';
              this.ruleForm.dl_codclaselib = '';
              this.ruleForm.dl_desclaselib = '';
              this.ruleForm.dl_distritomil = '';
              this.ruleForm.dl_certjudicial = '';
              this.ruleForm.dl_fechaven = '';
              this.ruleForm.dl_hosrasdiarias = '';
              this.ruleForm.dp_codarp = '';
              this.ruleForm.dp_desarp = '';
              this.ruleForm.dp_codeps = '';
              this.ruleForm.dp_deseps = '';
              this.ruleForm.dp_afp = '';
              this.ruleForm.dp_desafp = '';
              this.ruleForm.dp_codcaja = '';
              this.ruleForm.dp_descaja = '';
              this.ruleForm.dp_codbanco = '';
              this.ruleForm.dp_desbanco = '';
              this.ruleForm.dp_ncuenta = '';
              this.ruleForm.dp_convencion = 'N';
              this.ruleForm.dp_adicionarp = '';
              this.ruleForm.em_cedula = '';
              this.ruleForm.em_codingeniero = '';
              this.ruleForm.em_nomingeniero = '';
              this.ruleForm.em_programa = '';
              this.ruleForm.em_desprograma = '';
              this.ruleForm.em_cuadrilla = '';
              this.ruleForm.em_codarea = '';
              this.ruleForm.em_desarea = '';
              this.ruleForm.em_codcargo = '';
              this.ruleForm.em_descargo = '';
              this.ruleForm.em_confianza = '';
              this.ruleForm.em_codigo = '';
              this.ruleForm.em_perfil = '';
              this.ruleForm.ta_camisa = '';
              this.ruleForm.ta_pantalon = '';
              this.ruleForm.ta_botas = '';
              this.ruleForm.ta_tipo = '';
              this.ruleForm.df_cedula = '';
              this.ruleForm.co_cedula = '';
              this.ruleForm.te_codigo = '';
              this.clear();
              this.clearFAcademica();
              this.clearDatosContrato();
              this.clearVencimientos();
              this.clearInfoFamiliar();
              this.clearInfoHijos();
              document.getElementById('foto').value = '';
              $('#imgFuncionario').attr('src', "img/avatar.jpg");
              this.tableData = [];
              this.tableDataContrato = [];
              this.tableDataInfoAcademinaca = [];
              this.tableDataInfoHijos = [];
              this.tableDataVenciomientos = []; 
            } else {
              this.ruleForm.rh_cedula = response.data[0].rh_cedula
              this.ruleForm.rh_codexpciud = response.data[0].rh_codexpciud
              this.ruleForm.rh_expcedula = response.data[0].rh_expcedula
              this.ruleForm.rh_apellido1 = response.data[0].rh_apellido1
              this.ruleForm.rh_apellido2 = response.data[0].rh_apeelido2
              this.ruleForm.rh_nombre1 = response.data[0].rh_nombre1
              this.ruleForm.rh_nombre2 = response.data[0].rh_nombre2
              this.ruleForm.rh_direccion = response.data[0].rh_direccion
              this.ruleForm.rh_codciudad = response.data[0].rh_codciudad
              this.ruleForm.rh_ciudad = response.data[0].rh_ciudad
              this.ruleForm.rh_codbarrio = response.data[0].rh_codbarrio
              this.ruleForm.rh_barrio = response.data[0].rh_barrio
              this.showBarrios(response.data[0].mu_coddpto, response.data[0].codigo);
              this.ruleForm.rh_telefono = response.data[0].rh_telefono
              this.ruleForm.rh_movil = response.data[0].rh_movil
              this.ruleForm.rh_sexo = response.data[0].rh_sexo
              this.ruleForm.rh_dessexo = response.data[0].rh_dessexo
              this.ruleForm.rh_facsangre = response.data[0].rh_facsangre
              this.ruleForm.rh_rh = response.data[0].tipo_sangre
              this.ruleForm.rh_desrh = response.data[0].rh_desrh
              this.ruleForm.rh_estcivil = response.data[0].rh_estcivil
              this.ruleForm.rh_fechanac = response.data[0].rh_fechanac
              this.ruleForm.rh_coddptonac = response.data[0].rh_coddptonac
              this.ruleForm.rh_dptonac = response.data[0].rh_dptonac
              this.ruleForm.rh_codciunac = response.data[0].rh_codciudnac
              this.ruleForm.rh_estado = response.data[0].rh_estado
              this.ruleForm.rh_ciudadnac = response.data[0].rh_ciudadnac
              this.ruleForm.rh_codnacional = response.data[0].rh_codnacional
              this.ruleForm.rh_nacional = response.data[0].rh_nacional
              this.showMunicipios(response.data[0].rh_coddptonac);
              this.ruleForm.te_codigo = response.data[0].te_codigo
              this.ruleForm.dl_codcentrab = response.data[0].dl_codcentrab
              this.ruleForm.dl_descentrab = response.data[0].dl_descentrab
              this.ruleForm.dl_codcencosto = response.data[0].dl_codcencosto
              this.ruleForm.dl_descencosto = response.data[0].dl_descencosto
              this.ruleForm.dl_salario = response.data[0].dl_salario
              this.ruleForm.dl_nlibreta = response.data[0].dl_nlibreta
              this.ruleForm.dl_codclaselib = response.data[0].dl_codclaselib
              this.ruleForm.dl_desclaselib = response.data[0].dl_desclaselib
              this.ruleForm.dl_distritomil = response.data[0].dl_distritomil
              this.ruleForm.dl_certjudicial = response.data[0].dl_certjudicial
              this.ruleForm.dl_fechaven = response.data[0].dl_fechaven
              this.ruleForm.dl_hosrasdiarias = response.data[0].dl_horasdiarias
              this.ruleForm.dl_comphoras = Math.round(response.data[0].dl_salario / 30 / response.data[0].dl_horasdiarias);
              this.ruleForm.dl_salario_letras = numeroALetras(response.data[0].dl_salario);
              this.ruleForm.dp_codarp = response.data[0].dp_codarp
              this.ruleForm.dp_desarp = response.data[0].dp_desarp
              this.ruleForm.dp_codeps = response.data[0].dp_codeps
              this.ruleForm.dp_deseps = response.data[0].dp_deseps
              this.ruleForm.dp_afp = response.data[0].dp_codafp
              this.ruleForm.dp_desafp = response.data[0].dp_desafp
              this.ruleForm.dp_codcaja = response.data[0].dp_codcaja
              this.ruleForm.dp_descaja = response.data[0].dp_descaja
              this.ruleForm.dp_codbanco = response.data[0].dp_codbanco
              this.ruleForm.dp_desbanco = response.data[0].dp_desbanco
              this.ruleForm.dp_ncuenta = response.data[0].dp_ncuenta
              this.ruleForm.dp_adicionarp = response.data[0].dp_adicionarp
              this.ruleForm.em_codingeniero = response.data[0].em_codingeniero
              this.ruleForm.em_nomingeniero = response.data[0].em_nomingeniero
              this.ruleForm.em_programa = response.data[0].em_programa
              this.ruleForm.em_codarea = response.data[0].em_codarea
              this.ruleForm.em_desarea = response.data[0].em_desarea
              this.ruleForm.em_codcargo = response.data[0].em_codcargo
              this.ruleForm.em_descargo = response.data[0].em_descargo
              this.ruleForm.em_codigo = response.data[0].em_codigo
              this.ruleForm.em_confianza = response.data[0].em_confianza
              this.ruleForm.em_perfil = response.data[0].em_perfil
              this.ruleForm.ta_camisa = response.data[0].ta_camisa
              this.ruleForm.ta_pantalon = response.data[0].ta_pantalon
              this.ruleForm.ta_botas = response.data[0].ta_botas
              this.ruleForm.ta_tipo = response.data[0].ta_tipo
              this.ruleFormInfoFamiliar.if_cedula = response.data[0].if_cedula
              this.ruleFormInfoFamiliar.if_nombrecony = response.data[0].if_nombrecony
              this.ruleFormInfoFamiliar.if_fechanaccony = response.data[0].if_fechanaccony
              this.ruleFormInfoFamiliar.if_ciudad = response.data[0].if_ciudad
              this.ruleFormInfoFamiliar.if_desciudad = response.data[0].if_desciudad
              this.ruleFormInfoFamiliar.if_cedulacony = response.data[0].if_cedulacony
              this.ruleFormInfoFamiliar.if_ciudadexp = response.data[0].if_ciudadexp
              this.ruleFormInfoFamiliar.if_desciudadexp = response.data[0].if_desciudadexp
              this.ruleFormInfoFamiliar.if_profecony = response.data[0].if_profecony
              this.ruleFormInfoFamiliar.if_canhijo = response.data[0].if_canhijo
              this.ruleFormInfoFamiliar.if_sexocony = response.data[0].if_sexocony
              this.ruleFormInfoFamiliar.if_dessexocony = response.data[0].if_dessexocony
              var data = new FormData();
              data.append('fecha_nac', response.data[0].rh_fechanac);
              axios.post('request/calculateAge.php', data).then(response => {
                this.ruleForm.rh_edad = response.data.edad;
              }).catch(e => {
                console.log(e.response);
              });
              this.getDatosConte(this.ruleForm.rh_cedula);
              this.getDatosContrato(this.ruleForm.rh_cedula);
              this.getDatosFormacademica(this.ruleForm.rh_cedula);
              // this.getInfoFamiliar(row.rh_cedula);
              this.getInfoHijos(this.ruleForm.rh_cedula);
              this.getVencimientos(this.ruleForm.rh_cedula);
              if (response.data[0].rh_foto !== '') {
                $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+this.ruleForm.rh_cedula);
              } else {
                $('#imgFuncionario').attr('src', "img/avatar.jpg");
              }
              if (this.tableDataContrato[0].co_fecharet) {
                this.stateButtonContrato = false;
              }
              if (response.data[0].rh_estado === 'INACTIVO') {
                this.active = true
              } else {
                this.active = false
              }
              // $('#imgFuncionario').attr('src', "./request/getFoto.php?cedula="+this.ruleForm.rh_cedula);
              this.rowDelete = 1;
              this.visibleUpdate = true;
              this.visibleCreate = false;
            }
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosConte (dc_cedula) {
          var data = new FormData();
          data.append('dc_cedula', dc_cedula);
          axios.post('request/getDatosConte.php', data).then(response => {
            this.tableData = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosContrato (co_cedula) {
          var data = new FormData();
          data.append('co_cedula', co_cedula);
          axios.post('request/getDatosContrato.php', data).then(response => {
            this.tableDataContrato = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getDatosFormacademica (df_cedula) {
          var data = new FormData();
          data.append('df_cedula', df_cedula);
          axios.post('request/getDatosFormacademica.php', data).then(response => {
            this.tableDataInfoAcademinaca = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getInfoHijos (ih_cedula) {
          var data = new FormData();
          data.append('ih_cedula', ih_cedula);
          axios.post('request/getInfoHijos.php', data).then(response => {
            this.tableDataInfoHijos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        getVencimientos (cedula) {
          var data = new FormData();
          data.append('cedula', cedula);
          axios.post('request/getVencimientos.php', data).then(response => {
            this.tableDataVenciomientos = response.data;
          }).catch(e => {
            console.log(e.response);
          });
        },
        handleChange (file, fileList1) {
          this.fileList = fileList1
        },
        handlePreview (file) {
          // this.fileList = file
          // console.log(file)
        },
        valueSelect (data, value, label, dato) {
          let labelReturn = '';
          data.forEach((item, index, arr) => {
            if (dato === item[value]) {
              labelReturn = item[label];
              // arr.length = 0
              return false;
            }
          });
          return labelReturn;
        },
        indexMethod (index) {
          return index;
        },
        indexMethod1 (index) {
          return index + 1;
        },
        indexMethod2 (index) {
          return index;
        },
        indexMethod3 (index) {
          return index;
        },
        indexMethod4 (index) {
          return index + 1;
        },
        indexMethod5 (index) {
          return index;
        },
        clear () {
          this.ruleFormConte = {
            dc_codconte: '',
            dc_desconte: ''
          }
        },
        clearFAcademica () {
          this.ruleFormFAcademica = {
            df_codigo: '',
            df_formacademica: '',
            df_insteducativa: '',
            df_tituloobt: ''
          }
        },
        clearDatosContrato () {
          this.ruleFormDatosContrato = {
            co_codempresa: '',
            co_empresa: '',
            co_fechaing: '',
            co_fechafin: '',
            co_tcontrato: '',
            co_codretiro: '',
            co_motivoretiro: '',
            co_codreal: '',
            co_motivoreal: '',
            co_fecharet: '',
            co_observacion: ''
          }
          this.stateDate = true;
        },
        clearVencimientos () {
          this.ruleFormVencimientos = {
            codtipovence: '',
            descripcion: '',
            fechavence: ''
          }
        },
        clearInfoFamiliar () {
          this.ruleFormInfoFamiliar = {
            if_cedula: '',
            if_nombrecony: '',
            if_fechanaccony: '',
            if_ciudad: '',
            if_desciudad: '',
            if_cedulacony: '',
            if_ciudadexp: '',
            if_desciudadexp: '',
            if_profecony: '',
            if_canhijo: '',
            if_sexocony: '',
            if_dessexocony: ''
          }
        },
        clearInfoHijos () {
          this.ruleFormInfoHijos = {
            ih_nombrehijo: '',
            ih_fechanac: '',
            ih_identifica: '',
            ih_sexo: '',
            cantidad: ''
          }
        },
        clearRH () {
          this.ruleForm = {
            rh_cedula: '',
            rh_codexpciud: '',
            rh_expcedula: '',
            rh_apellido1: '',
            rh_apellido2: '',
            rh_nombre: '',
            rh_direccion: '',
            rh_codciudad: '',
            rh_ciudad: '',
            rh_codbarrio: '',
            rh_barrio: '',
            rh_telefono: '',
            rh_movil: '',
            rh_sexo: '',
            rh_dessexo: '',
            rh_facsangre: '',
            rh_rh: '',
            rh_rh1: '',
            rh_desrh: '',
            rh_estcivil: '',
            rh_fechanac: '',
            rh_codciunac: '',
            rh_ciudadnac: '',
            rh_coddptonac: '',
            rh_dptonac: '',
            rh_codnacional: '',
            rh_nacional: '',
            rh_estado: '',
            rh_foto: '',
            rh_profesion: '',
            rh_explaboral: '',
            rh_nombre1: '',
            rh_nombre2: '',
            rh_edad: '',
            dl_codcentrab: '',
            dl_descentrab: '',
            dl_codcencosto: '',
            dl_descencosto: '',
            dl_salario: '',
            dl_nlibreta: '',
            dl_codclaselib: '',
            dl_desclaselib: '',
            dl_distritomil: '',
            dl_certjudicial: '',
            dl_fechaven: '',
            dl_hosrasdiarias: '',
            dp_codarp: '',
            dp_desarp: '',
            dp_codeps: '',
            dp_deseps: '',
            dp_afp: '',
            dp_desafp: '',
            dp_codcaja: '',
            dp_descaja: '',
            dp_codbanco: '',
            dp_desbanco: '',
            dp_ncuenta: '',
            dp_convencion: 'N',
            dp_adicionarp: '',
            em_cedula: '',
            em_codingeniero: '',
            em_nomingeniero: '',
            em_programa: '',
            em_desprograma: '',
            em_cuadrilla: '',
            em_codarea: '',
            em_desarea: '',
            em_codcargo: '',
            em_descargo: '',
            em_confianza: '',
            em_codigo: '',
            em_perfil: '',
            ta_camisa: '',
            ta_pantalon: '',
            ta_botas: '',
            ta_tipo: '',
            df_cedula: '',
            co_cedula: '',
            te_codigo: ''
          }
        },
        inLetter () {
          if (this.ruleForm.dl_salario !== '') {
            this.ruleForm.dl_salario_letras = numeroALetras(this.ruleForm.dl_salario);
          } else {
            this.ruleForm.dl_salario = '';
          }
        },
        isEmpty (obj) {
          for (var key in obj) {
            console.log(key)
            if (obj.hasOwnProperty(key)) {
              return false;
            }
          }
          return true;
        },
        openCiudadesExpedicion () {
          this.dialogVisibleCiudadesExpedicion = true;
          this.inputSearchCiudadesExpedicion = '';
        },
        openCiudades () {
          this.dialogVisibleCiudades = true;
          this.inputSearchCiudades = '';
        },
        openBarrios () {
          this.dialogVisibleBarrios = true;
          this.inputSearchBarrios = '';
        },
        openCiudadInfoFamiliar1 () {
          this.dialogVisibleCiudadesInfoFamiliar1 = true;
          this.inputSearchCiudadesInfoFamiliar1 = '';
        },
        openCiudadInfoFamiliar2 () {
          this.dialogVisibleCiudadesInfoFamiliar2 = true;
          this.inputSearchCiudadesInfoFamiliar2 = '';
        },
        openCiudadDpto () {
          this.dialogVisibleDpto = true;
          this.inputSearchDpto = '';
        },
        openMpio () {
          this.dialogVisibleMpio = true;
          this.inputSearchMpio = '';
        },
        openFuncionario () {
          this.dialogBuscar = true;
          this.inputSearch = '';
        },
        openNuevoContrato () {
          this.modalVisible = true;
          this.contrato = 0;
          this.on = false;
          this.off = true;
          this.stateButtonContrato = true;
          this.criterio = 0;
          this.ruleFormDatosContrato.co_empresa === '';
          this.ruleFormDatosContrato.co_fechaing === '';
          this.ruleFormDatosContrato.co_tcontrato === '';
          this.ruleFormDatosContrato.co_fecharet === '';
          this.ruleFormDatosContrato.co_motivoretiro === '';
        },
        openTerminarContrato () {
          this.$confirm('Está seguro que desea terminar el contrato seleccionado?', 'Alerta..!', {
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
            type: 'warning'
          }).then(() => {
            this.ruleForm.rh_estado = 'INACTIVO';
            this.stateButtonContrato = true;
          }).catch(() => {
            // this.$message({
            //   type: 'info',
            //   message: 'Operación cancelada por el usuario.'
            // });
          });
          // this.modalVisible = true;
          // this.contrato = 1;
        },
        filteredListCiudExp () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesExpedicion.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesExpedicion.toLowerCase()) > -1
          })
        },
        filteredListCiudades () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudades.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudades.toLowerCase()) > -1
          })
        },
        filteredListBarrios () {
          this.tableDataBarriosNew = this.tableDataBarrios.filter(item => {
            return item.ba_codbarrio.toLowerCase().indexOf(this.inputSearchBarrios.toLowerCase()) > -1 || item.ba_nombarrio.toLowerCase().indexOf(this.inputSearchBarrios.toLowerCase()) > -1
          })
        },
        filteredListFuncionario () {
          this.tableDataFuncionario = this.tableDataBuscar.filter(item => {
            return item.rh_cedula.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1 || item.full_name.toLowerCase().indexOf(this.inputSearch.toLowerCase()) > -1
          })
        },
        filteredListCiudadesInfoFamiliar1 () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar1.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar1.toLowerCase()) > -1
          })
        },
        filteredListCiudadesInfoFamiliar2 () {
          this.tableDataCiudadesExpedicionNew = this.tableDataCiudadesExpedicion.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar2.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchCiudadesInfoFamiliar2.toLowerCase()) > -1
          })
        },
        filteredListDpto () {
          this.tableDataDptoNew = this.tableDataDpto.filter(item => {
            return item.mu_coddpto.toLowerCase().indexOf(this.inputSearchDpto.toLowerCase()) > -1 || item.mu_departamento.toLowerCase().indexOf(this.inputSearchDpto.toLowerCase()) > -1
          })
        },
        filteredListMpio () {
          this.tableDataMpioNew = this.tableDataMpio.filter(item => {
            return item.mu_codigo.toLowerCase().indexOf(this.inputSearchMpio.toLowerCase()) > -1 || item.mu_municipio.toLowerCase().indexOf(this.inputSearchMpio.toLowerCase()) > -1
          })
        },
        andlePreview (file) {
          console.log(file);
        },
        onChange () {
          if (this.ruleFormDatosContrato.co_tcontrato === '0') {
            this.stateDate = false;
          } else if (this.ruleFormDatosContrato.co_tcontrato === '1') {
            this.stateDate = true;
            var data = new FormData();
            data.append('fecha', this.ruleFormDatosContrato.co_fechaing);
            axios.post('request/contratacionAllo.php', data).then(response => {
              this.ruleFormDatosContrato.co_fechafin = response.data.allo;
              // this.ruleForm.rh_edad = response.data.edad;
            }).catch(e => {
              console.log(e.response);
            });
          } else if (this.ruleFormDatosContrato.co_tcontrato === '2') {
            this.stateDate = true;
            this.ruleFormDatosContrato.co_fechafin = '';
          } else if (this.ruleFormDatosContrato.co_tcontrato === '3') {
            this.stateDate = false;
          }
        },
        calculo () {
          if (this.ruleForm.dl_hosrasdiarias !== '') {
            this.ruleForm.dl_comphoras = Math.round(this.ruleForm.dl_salario / 30 / this.ruleForm.dl_hosrasdiarias);
          } else {
            this.ruleForm.dl_hosrasdiarias = '';
          }
        },
        edad (value) {
          // console.log(value)
          var data = new FormData();
          data.append('fecha_nac', this.ruleForm.rh_fechanac);
          axios.post('request/calculateAge.php', data).then(response => {
            this.ruleForm.rh_edad = response.data.edad;
          }).catch(e => {
            console.log(e.response);
          });
        },
        cantNombres () {
          nombres.reduce((conNombres, nombre) => {
            conNombres[nombre.dc_codconte] = (conNombres[nombre.dc_codconte] || 0) + 1;
            if (conNombres[nombre.dc_codconte] > 1) {
              alert('Codigo de conte ya ingresado')
            } else {
              return conNombres;
            }
            // return conNombres;
          }, {})
        },
        forceUppercase(e, o, prop) {
          const start = e.target.selectionStart;
          e.target.value = e.target.value.toUpperCase();
          this.$set(o, prop, e.target.value);
          e.target.setSelectionRange(start, start);
        }
      },
      mounted () {
        this.showCiudades();
        this.showFuncionarios();
        var f = new Date();
        this.fecha = (f.getMonth() +1) + '/' + f.getDate() + '/' + f.getFullYear();
      }
    })
  </script>

</body>
</html>