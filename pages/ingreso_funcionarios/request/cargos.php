<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT ca_codigo, ca_cargo FROM cargos ORDER BY ca_cargo ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['ca_codigo'] = utf8_encode($row[0]);
		$row_array['ca_cargo'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>