<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT DISTINCT mu_coddpto, UPPER(mu_departamento) mu_departamento FROM municipios_col ORDER BY mu_coddpto ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['mu_coddpto']      = utf8_encode($row[0]);
        $row_array['mu_departamento'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>