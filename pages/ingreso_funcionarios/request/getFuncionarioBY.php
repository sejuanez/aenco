<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 11/07/2018
 * Time: 11:06 AM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$rh_cedula = $_POST['rh_cedula'];

$query = "SELECT dr.rh_cedula, dr.rh_codexpciud, dr.rh_expcedula, dr.rh_apellido1, dr.rh_apeelido2, dr.rh_nombre, dr.rh_direccion, dr.rh_codciudad, dr.rh_ciudad, dr.rh_codbarrio,
dr.rh_barrio, dr.rh_telefono, dr.rh_movil, dr.rh_sexo, dr.rh_dessexo, dr.rh_facsangre, dr.rh_rh, dr.rh_facsangre||dr.rh_rh tipo_sangre, dr.rh_desrh, dr.rh_estcivil, EXTRACT(MONTH FROM dr.rh_fechanac)||'/'||EXTRACT(DAY FROM dr.rh_fechanac)||'/'||EXTRACT(YEAR FROM dr.rh_fechanac) rh_fechanac, dr.rh_codciudnac, dr.rh_ciudadnac,
dr.rh_coddptonac, dr.rh_dptonac, dr.rh_codnacional, dr.rh_nacional, dr.rh_estado, dr.rh_foto, dr.rh_nombre1, dr.rh_nombre2, dr.rh_nombre1||' '||dr.rh_nombre2||' '||dr.rh_apellido1||' '||dr.rh_apeelido2 full_name,
te.te_codigo, te.te_nombres, te.te_direccion, te.te_telefono, te.te_ciudad, te.te_cedula, te.te_estado, dl.dl_cedula, dl.dl_codcentrab, dl.dl_descentrab, dl.dl_codcencosto,
dl.dl_descencosto, dl.dl_salario, dl.dl_nlibreta, dl.dl_codclaselib, dl.dl_desclaselib, dl.dl_distritomil, dl.dl_certjudicial, EXTRACT(MONTH FROM dl.dl_fechaven)||'/'||EXTRACT(DAY FROM dl.dl_fechaven)||'/'||EXTRACT(YEAR FROM dl.dl_fechaven) dl_fechaven, dl.dl_horasdiarias,
dp.dp_cedula, dp.dp_codarp, dp.dp_desarp, dp.dp_codeps, dp.dp_deseps, dp.dp_codafp, dp.dp_desafp, dp.dp_codcaja, dp.dp_descaja, dp.dp_codbanco, dp.dp_desbanco, dp.dp_ncuenta, dp.dp_adicionarp,
em.em_codingeniero, em.em_nomingeniero, em.em_programa, em.em_codarea, em.em_desarea, em.em_codcargo, em.em_descargo, em.em_codigo, em.em_confianza, em.em_perfil,
dt.ta_camisa, dt.ta_pantalon, dt.ta_botas, dt.ta_tipo, inf.if_nombrecony, EXTRACT(MONTH FROM inf.if_fechanaccony)||'/'||EXTRACT(DAY FROM inf.if_fechanaccony)||'/'||EXTRACT(YEAR FROM inf.if_fechanaccony) if_fechanaccony, inf.if_ciudad, inf.if_cedulacony, inf.if_ciudadexp, inf.if_profecony, inf.if_canhijos, inf.if_sexocony, mc.mu_municipio AS if_desciudad, mc2.mu_municipio AS if_desciudadexp, SUBSTRING(dr.rh_codciudad FROM 3 FOR 5) codigo, SUBSTRING(dr.rh_codciudad FROM 1 FOR 2) mu_coddpto
FROM datos_rrhh dr
LEFT JOIN tecnicos te ON te.te_cedula = dr.rh_cedula
LEFT JOIN datos_legales dl ON dl.dl_cedula = dr.rh_cedula
LEFT JOIN datos_parafiscales dp ON dp.dp_cedula = dr.rh_cedula
LEFT JOIN datos_empresa em ON em.em_cedula = dr.rh_cedula
LEFT JOIN datos_tallas dt ON dt.ta_cedula = dr.rh_cedula
LEFT JOIN informe_familiar inf ON inf.if_cedula = dr.rh_cedula
LEFT JOIN municipios_col mc ON inf.if_ciudad = mc.mu_codigo
LEFT JOIN municipios_col mc2 ON inf.if_ciudadexp = mc2.mu_codigo
WHERE dr.rh_cedula = '$rh_cedula' ORDER BY dr.rh_cedula ASC";

$return_arr = array();
$data = ibase_query($conexion, $query);

while($fila = ibase_fetch_row($data)){
    $row_array['rh_cedula']       = utf8_encode($fila[0]);
    $row_array['rh_codexpciud']   = utf8_encode($fila[1]);
    $row_array['rh_expcedula']    = utf8_encode($fila[2]);
    $row_array['rh_apellido1']    = utf8_encode($fila[3]);
    $row_array['rh_apeelido2']    = utf8_encode($fila[4]);
    $row_array['rh_nombre']       = utf8_encode($fila[5]);
    $row_array['rh_direccion']    = utf8_encode($fila[6]);
    $row_array['rh_codciudad']    = utf8_encode($fila[7]);
    $row_array['rh_ciudad']       = utf8_encode($fila[8]);
    $row_array['rh_codbarrio']    = utf8_encode($fila[9]);
    $row_array['rh_barrio']       = utf8_encode($fila[10]);
    $row_array['rh_telefono']     = utf8_encode($fila[11]);
    $row_array['rh_movil']        = utf8_encode($fila[12]);
    $row_array['rh_sexo']         = utf8_encode($fila[13]);
    $row_array['rh_dessexo']      = utf8_encode($fila[14]);
    $row_array['rh_facsangre']    = utf8_encode($fila[15]);
    $row_array['rh_rh']           = utf8_encode($fila[16]);
    $row_array['tipo_sangre']     = utf8_encode($fila[17]);
    $row_array['rh_desrh']        = utf8_encode($fila[18]);
    $row_array['rh_estcivil']     = utf8_encode($fila[19]);
    $row_array['rh_fechanac']     = utf8_encode($fila[20]);
    $row_array['rh_codciudnac']   = utf8_encode($fila[21]);
    $row_array['rh_ciudadnac']    = utf8_encode($fila[22]);
    $row_array['rh_coddptonac']   = utf8_encode($fila[23]);
    $row_array['rh_dptonac']      = utf8_encode($fila[24]);
    $row_array['rh_codnacional']  = utf8_encode($fila[25]);
    $row_array['rh_nacional']     = utf8_encode($fila[26]);
    $row_array['rh_estado']       = utf8_encode($fila[27]);
    $row_array['rh_foto']         = utf8_encode($fila[28]);
    $row_array['rh_nombre1']      = utf8_encode($fila[29]);
    $row_array['rh_nombre2']      = utf8_encode($fila[30]);
    $row_array['full_name']       = utf8_encode($fila[31]);
    $row_array['te_codigo']       = utf8_encode($fila[32]);
    $row_array['te_nombres']      = utf8_encode($fila[33]);
    $row_array['te_direccion']    = utf8_encode($fila[34]);
    $row_array['te_telefono']     = utf8_encode($fila[35]);
    $row_array['te_ciudad']       = utf8_encode($fila[36]);
    $row_array['te_cedula']       = utf8_encode($fila[37]);
    $row_array['te_estado']       = utf8_encode($fila[38]);
    $row_array['dl_cedula']       = utf8_encode($fila[39]);
    $row_array['dl_codcentrab']   = utf8_encode($fila[40]);
    $row_array['dl_descentrab']   = utf8_encode($fila[41]);
    $row_array['dl_codcencosto']  = utf8_encode($fila[42]);
    $row_array['dl_descencosto']  = utf8_encode($fila[43]);
    $row_array['dl_salario']      = utf8_encode($fila[44]);
    $row_array['dl_nlibreta']     = utf8_encode($fila[45]);
    $row_array['dl_codclaselib']  = utf8_encode($fila[46]);
    $row_array['dl_desclaselib']  = utf8_encode($fila[47]);
    $row_array['dl_distritomil']  = utf8_encode($fila[48]);
    $row_array['dl_certjudicial'] = utf8_encode($fila[49]);
    $row_array['dl_fechaven']     = utf8_encode($fila[50]);
    $row_array['dl_horasdiarias'] = utf8_encode($fila[51]);
    $row_array['dp_cedula']       = utf8_encode($fila[52]);
    $row_array['dp_codarp']       = utf8_encode($fila[53]);
    $row_array['dp_desarp']       = utf8_encode($fila[54]);
    $row_array['dp_codeps']       = utf8_encode($fila[55]);
    $row_array['dp_deseps']       = utf8_encode($fila[56]);
    $row_array['dp_codafp']       = utf8_encode($fila[57]);
    $row_array['dp_desafp']       = utf8_encode($fila[58]);
    $row_array['dp_codcaja']      = utf8_encode($fila[59]);
    $row_array['dp_descaja']      = utf8_encode($fila[60]);
    $row_array['dp_codbanco']     = utf8_encode($fila[61]);
    $row_array['dp_desbanco']     = utf8_encode($fila[62]);
    $row_array['dp_ncuenta']      = utf8_encode($fila[63]);
    $row_array['dp_adicionarp']   = utf8_encode($fila[64]);
    $row_array['em_codingeniero'] = utf8_encode($fila[65]);
    $row_array['em_nomingeniero'] = utf8_encode($fila[66]);
    $row_array['em_programa']     = utf8_encode($fila[67]);
    $row_array['em_codarea']      = utf8_encode($fila[68]);
    $row_array['em_desarea']      = utf8_encode($fila[69]);
    $row_array['em_codcargo']     = utf8_encode($fila[70]);
    $row_array['em_descargo']     = utf8_encode($fila[71]);
    $row_array['em_codigo']       = utf8_encode($fila[72]);
    $row_array['em_confianza']    = utf8_encode($fila[73]);
    $row_array['em_perfil']       = utf8_encode($fila[74]);
    $row_array['ta_camisa']       = utf8_encode($fila[75]);
    $row_array['ta_pantalon']     = utf8_encode($fila[76]);
    $row_array['ta_botas']        = utf8_encode($fila[77]);
    $row_array['ta_tipo']         = utf8_encode($fila[78]);
    $row_array['if_nombrecony']   = utf8_encode($fila[79]);
    $row_array['if_fechanaccony'] = utf8_encode($fila[80]);
    $row_array['if_ciudad']       = utf8_encode($fila[81]);
    $row_array['if_cedulacony']   = utf8_encode($fila[82]);
    $row_array['if_ciudadexp']    = utf8_encode($fila[83]);
    $row_array['if_profecony']    = utf8_encode($fila[84]);
    $row_array['if_canhijos']     = utf8_encode($fila[85]);
    $row_array['if_sexocony']     = utf8_encode($fila[86]);
    $row_array['if_desciudad']    = utf8_encode($fila[87]);
    $row_array['if_desciudadexp'] = utf8_encode($fila[88]);
    $row_array['codigo']          = utf8_encode($fila[89]);
    $row_array['mu_coddpto']      = utf8_encode($fila[90]);
    array_push($return_arr, $row_array);
}

echo json_encode($return_arr);