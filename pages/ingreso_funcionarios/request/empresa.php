<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT EM_CODIGO, EM_NOMBRE FROM EMPRESA_TRAB";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['sw'] = utf8_encode($row[0]);
		$row_array['nombre'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>