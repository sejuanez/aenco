<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 5/07/2018
 * Time: 5:08 PM
 */
define('MAX_SEGMENT_SIZE', 65535);
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

function blob_create($data) {
    if (strlen($data) == 0)
        return false;
    $handle = ibase_blob_create();
    $len = strlen($data);
    for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
        $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
        $buf = substr($data, $pos, $buflen);
        ibase_blob_add($handle, $buf);
    }
    return ibase_blob_close($handle);
}

function normaliza ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

try {
    if (array_key_exists('rh_foto', $_FILES)) {
        $foto = blob_create(file_get_contents($_FILES['rh_foto']['tmp_name']));
    } else {
        $foto = null;
    }

    # VARIABLES DATOS_RRHH
    $rh_cedula      = $_POST['rh_cedula'];
    $rh_codexpciud  = $_POST['rh_codexpciud'];
    $rh_expcedula   = $_POST['rh_expcedula'];
    $rh_apellido1   = $_POST['rh_apellido1'];
    $rh_apellido2   = $_POST['rh_apellido2'];
    $rh_nombre      = $_POST['rh_nombre'];
    $rh_direccion   = $_POST['rh_direccion'];
    $rh_codciudad   = $_POST['rh_codciudad'];
    $rh_ciudad      = $_POST['rh_ciudad'];
    $rh_codbarrio   = $_POST['rh_codbarrio'];
    $rh_barrio      = $_POST['rh_barrio'];
    $rh_telefono    = $_POST['rh_telefono'];
    $rh_movil       = $_POST['rh_movil'];
    $rh_sexo        = $_POST['rh_sexo'];
    $rh_dessexo     = $_POST['rh_dessexo'];
    $rh_facsangre   = $_POST['rh_facsangre'];
    $rh_rh          = $_POST['rh_rh'];
    $rh_desrh       = $_POST['rh_desrh'];
    $rh_estcivil    = $_POST['rh_estcivil'];
    $rh_fechanac    = $_POST['rh_fechanac'];
    $rh_codciunac   = $_POST['rh_codciunac'];
    $rh_ciudadnac   = $_POST['rh_ciudadnac'];
    $rh_coddptonac  = $_POST['rh_coddptonac'];
    $rh_dptonac     = $_POST['rh_dptonac'];
    $rh_codnacional = $_POST['rh_codnacional'];
    $rh_nacional    = $_POST['rh_nacional'];
    $rh_estado      = $_POST['rh_estado'];
    $rh_nombre1     = $_POST['rh_nombre1'];
    $rh_nombre2     = $_POST['rh_nombre2'];

    # VARIABLES TECNICOS
    $te_nombres   = $_POST['te_nombres'];
    $te_direccion = $_POST['te_direccion'];
    $te_telefono  = $_POST['te_telefono'];
    $te_ciudad    = $_POST['te_ciudad'];
    $te_cedula    = $_POST['te_cedula'];
    
   # VARIABLES DATOS_LEGALES
   $dl_cedula         = $_POST['dl_cedula'];
   $dl_conte          = $_POST['dl_conte'];
   $dl_codcentrab     = $_POST['dl_codcentrab'];
   $dl_descentrab     = $_POST['dl_descentrab'];
   $dl_codcencosto    = $_POST['dl_codcencosto'];
   $dl_descencosto    = $_POST['dl_descencosto'];
   $dl_salario        = $_POST['dl_salario'];
   $dl_nlibreta       = $_POST['dl_nlibreta'];
   $dl_codclaselib    = $_POST['dl_codclaselib'];
   $dl_desclaselib    = $_POST['dl_desclaselib'];
   $dl_distritomil    = $_POST['dl_distritomil'];
   $dl_certjudicial   = $_POST['dl_certjudicial'];
   $dl_fechaven       = $_POST['dl_fechaven'];
   //$dl_salario_base = $_POST['dl_salario_base'];
   $dl_hosrasdiarias  = $_POST['dl_hosrasdiarias'];

   # VARIABLES DATOS_PARAFICALES
   $dp_cedula           = $_POST['dp_cedula'];
   $dp_codarp           = $_POST['dp_codarp'];
   $dp_desarp           = $_POST['dp_desarp'];
   $dp_codeps           = $_POST['dp_codeps'];
   $dp_deseps           = $_POST['dp_deseps'];
   $dp_afp              = $_POST['dp_afp'];
   $dp_desafp           = $_POST['dp_desafp'];
   $dp_codcaja          = $_POST['dp_codcaja'];
   $dp_descaja          = $_POST['dp_descaja'];
   $dp_codbanco         = $_POST['dp_codbanco'];
   $dp_desbanco         = $_POST['dp_desbanco'];
   $dp_ncuenta          = $_POST['dp_ncuenta'];
   $dp_convencion       = $_POST['dp_convencion'];
   $dp_adicionarp       = $_POST['dp_adicionarp'];

   # VARIABLES DATOS_EMPRESA
   $em_cedula       = $_POST['em_cedula'];
   $em_codingeniero = $_POST['em_codingeniero'];
   $em_nomingeniero = $_POST['em_nomingeniero'];
   $em_programa     = $_POST['em_programa'];
   $em_cuadrilla    = $_POST['em_cuadrilla'];
   $em_codarea      = $_POST['em_codarea'];
   $em_desarea      = $_POST['em_desarea'];
   $em_codcargo     = $_POST['em_codcargo'];
   $em_descargo     = $_POST['em_descargo'];
   $em_confianza    = $_POST['em_confianza'];
   $em_perfil       = $_POST['em_perfil'];

    # VARIABLES DATOS_TALLAS
    $ta_cedula   = $_POST['ta_cedula'];
    $ta_camisa   = $_POST['ta_camisa'];
    $ta_pantalon = $_POST['ta_pantalon'];
    $ta_botas    = $_POST['ta_botas'];
    $ta_tipo     = $_POST['ta_tipo'];

    # VARIABLES DATOS_CONTE
    $dc_cedula = $_POST['dc_cedula'];
    $conte     = $_POST['conte'];

    # VARIABLES DATOS_CONTRATOS
    $co_cedula = $_POST['co_cedula'];
    $contrato = $_POST['contrato'];

    # VARIABLES DATOS_FORMACADEMICA
    $df_cedula  = $_POST['df_cedula'];
    $facademica = $_POST['facademica'];

    # VARIABLES VENCIMIENTOS
    $cedula       = $_POST['cedula'];
    $vencimientos = $_POST['vencimientos'];

    # VARIABLES INFORMACION_FAMILIAR
    $if_cedula       = $_POST['if_cedula'];
    $if_nombrecony   = $_POST['if_nombrecony'];
    $if_fechanaccony = $_POST['if_fechanaccony'];
    $if_ciudad       = $_POST['if_ciudad'];
    $if_cedulacony   = $_POST['if_cedulacony'];
    $if_ciudadexp    = $_POST['if_ciudadexp'];
    $if_profecony    = $_POST['if_profecony'];
    $if_canhijo      = $_POST['if_canhijo'];
    $if_sexocony     = $_POST['if_sexocony'];

    # VARIABLES INFORME_HIJOS
    $ih_cedula = $_POST['ih_cedula'];
    $hijos     = $_POST['hijos'];

    $tr = ibase_trans();
    if ($rh_cedula === '' || $rh_codexpciud === '' || $rh_expcedula === 'null' || $rh_apellido1 === 'null' || $rh_direccion === 'null' || $rh_codciudad === 'null' || $rh_ciudad === 'null' || $rh_codbarrio === 'null' || $rh_barrio === 'null' || $rh_telefono === 'null' || $rh_movil === 'null' || $rh_sexo === 'null' || $rh_facsangre === 'null' || $rh_estcivil === 'null' || $rh_fechanac === 'null' || $rh_codciunac === 'null' || $rh_ciudadnac === 'null' || $rh_coddptonac === 'null' || $rh_dptonac === 'null' || $rh_codnacional === 'null' || $rh_nombre1 === 'null' || $rh_nombre2 === 'null') {
        $mensaje = array(
            'type'    => 'success',
            'message' => 'Faltan datos por diligenciar..!',
            'title'   => 'Alerta..',
            'error'   => 1
        );
        echo json_encode($mensaje);
    } else {
        $query = "INSERT INTO DATOS_RRHH (RH_CEDULA, RH_CODEXPCIUD, RH_EXPCEDULA, RH_APELLIDO1, RH_APEELIDO2, RH_NOMBRE, RH_DIRECCION, RH_CODCIUDAD, RH_CIUDAD, RH_CODBARRIO, RH_BARRIO, RH_TELEFONO, RH_MOVIL, RH_SEXO, RH_DESSEXO, RH_FACSANGRE, RH_RH, RH_DESRH, RH_ESTCIVIL, RH_FECHANAC, RH_CODCIUDNAC, RH_CIUDADNAC, RH_CODDPTONAC, RH_DPTONAC, RH_CODNACIONAL, RH_NACIONAL, RH_ESTADO, RH_USUGRABA, RH_FOTO, RH_NOMBRE1, RH_NOMBRE2)
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $data = ibase_prepare($conexion , $query);
        $result = ibase_execute($data,
            $rh_cedula, $rh_codexpciud, strtoupper(utf8_decode($rh_expcedula)), strtoupper(utf8_decode($rh_apellido1)), strtoupper(utf8_decode($rh_apellido2)), strtoupper(utf8_decode($rh_nombre)),
            strtoupper(utf8_decode($rh_direccion)), $rh_codciudad, strtoupper(utf8_decode($rh_ciudad)), $rh_codbarrio, strtoupper(utf8_decode($rh_barrio)), $rh_telefono,
            $rh_movil, $rh_sexo, strtoupper(utf8_decode($rh_dessexo)), $rh_facsangre, $rh_rh, strtoupper(utf8_decode($rh_desrh)), strtoupper(utf8_decode($rh_estcivil)),
            $rh_fechanac, $rh_codciunac, strtoupper(utf8_decode($rh_ciudadnac)), $rh_coddptonac, strtoupper(utf8_decode($rh_dptonac)), $rh_codnacional,
            strtoupper(utf8_decode($rh_nacional)), $rh_estado, $_SESSION['user'], $foto, strtoupper(utf8_decode($rh_nombre1)), strtoupper(utf8_decode($rh_nombre2))
        );

        $cod = "SELECT GEN_ID(TECNICO, 1) te_codigo FROM empresa";
        $return_arr = array();
        $dataCod = ibase_query($conexion, $cod);
        while($x = ibase_fetch_row($dataCod)){
            $row_array['te_codigo'] = utf8_encode($x[0]);
            array_push($return_arr, $row_array);
        }   
        $genId = json_encode($return_arr[0]['te_codigo']);

        # TECNICOS
        $queryTecnicos = "INSERT INTO TECNICOS (TE_CODIGO, TE_NOMBRES, TE_DIRECCION, TE_TELEFONO, TE_CIUDAD, TE_USU_SISTEMA, TE_CEDULA, TE_ESTADO)
                          VALUES (".str_replace('"','',$genId).", ?, ?, ?, ?, ?, ?, ?)";
        $dataTecnicos = ibase_prepare($conexion, $queryTecnicos);
        ibase_execute($dataTecnicos,
            strtoupper(utf8_decode($te_nombres)), strtoupper(utf8_decode($te_direccion)), $te_telefono, strtoupper(utf8_decode($te_ciudad)), $_SESSION['user'], $te_cedula, 'N'
        );

        # DATOS_LEGALES
        $queryDatosLegales = "INSERT INTO DATOS_LEGALES (DL_CEDULA, DL_CONTE, DL_CODCENTRAB, DL_DESCENTRAB, DL_CODCENCOSTO, DL_DESCENCOSTO, DL_SALARIO, DL_NLIBRETA, DL_CODCLASELIB, DL_DESCLASELIB, DL_DISTRITOMIL, DL_CERTJUDICIAL, DL_USUGRABA, DL_FECHAVEN, DL_SALARIO_BASE, DL_HORASDIARIAS)
                              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $dataDatosLegales = ibase_prepare($conexion, $queryDatosLegales);
        ibase_execute($dataDatosLegales,
            $dl_cedula, $dl_conte, $dl_codcentrab, strtoupper(utf8_decode($dl_descentrab)), $dl_codcencosto,
            strtoupper(utf8_decode($dl_descencosto)), $dl_salario, $dl_nlibreta, $dl_codclaselib, strtoupper(utf8_decode($dl_desclaselib)),
            strtoupper(utf8_decode($dl_distritomil)), $dl_certjudicial, $_SESSION['user'], $dl_fechaven, 0, $dl_hosrasdiarias
        );

        # DATOS_PARAFISCALES
        $queryDatosParafiscales = "INSERT INTO DATOS_PARAFISCALES (DP_CEDULA, DP_CODARP, DP_DESARP, DP_CODEPS, DP_DESEPS, DP_CODAFP, DP_DESAFP, DP_CODCAJA, DP_DESCAJA, DP_CODBANCO, DP_DESBANCO, DP_NCUENTA, DP_USUGRABA, DP_CONVENCION, DP_ADICIONPENSION, DP_ADICIONARP)
                                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $dataDatosParafiscales = ibase_prepare($conexion, $queryDatosParafiscales);
        ibase_execute($dataDatosParafiscales,
            $dp_cedula, $dp_codarp, strtoupper(utf8_decode($dp_desarp)), $dp_codeps, strtoupper(utf8_decode($dp_deseps)), $dp_afp, strtoupper(utf8_decode($dp_desafp)),
            $dp_codcaja, strtoupper(utf8_decode($dp_descaja)), $dp_codbanco, strtoupper(utf8_decode($dp_desbanco)), $dp_ncuenta, $_SESSION['user'],
            $dp_convencion, 0, $dp_adicionarp
        );

        # DATOS_EMPRESA
        $queryDatosEmpresa = "INSERT INTO DATOS_EMPRESA (EM_CEDULA, EM_CODINGENIERO, EM_NOMINGENIERO, EM_PROGRAMA, EM_CODCARGO, EM_DESCARGO, EM_USUGRABA, EM_CODIGO, EM_CONFIANZA, EM_PERFIL)
                              VALUES (?, ?, ?, ?, ?, ?, ?, ".str_replace('"','',$genId).", ?, ?)";
        $dataDatosEmpresa = ibase_prepare($conexion, $queryDatosEmpresa);
        ibase_execute($dataDatosEmpresa,
            $em_cedula, $em_codingeniero, strtoupper(utf8_decode($em_nomingeniero)), $em_programa, $em_codcargo,
            strtoupper(utf8_decode($em_descargo)), $_SESSION['user'], strtoupper(utf8_decode($em_confianza)), strtoupper(utf8_decode($em_perfil))
        );

        # DATOS_TALLAS
        $queryDatosTalla = "INSERT INTO DATOS_TALLAS (TA_CEDULA, TA_CAMISA, TA_PANTALON, TA_BOTAS, TA_USUGRABA, TA_TIPO)
                            VALUES (?, ?, ?, ?, ?, ?)";
        $dataDatosTalla = ibase_prepare($conexion, $queryDatosTalla);
        ibase_execute($dataDatosTalla,
            $ta_cedula, strtoupper(utf8_decode($ta_camisa)), $ta_pantalon, $ta_botas, $_SESSION['user'], $ta_tipo
        );

        # DATOS_CONTE
        $queryDatosConte = "INSERT INTO DATOS_CONTE (DC_CEDULA, DC_CODCONTE, DC_DESCONTE)
                            VALUES (?, ?, ?)";
        $dataDatosConte = ibase_prepare($conexion, $queryDatosConte);
        $conte = json_decode($conte);
        foreach ($conte as $item) {
            // echo gettype($conte);
            ibase_execute($dataDatosConte,
                $dc_cedula, $item->dc_codconte, strtoupper(utf8_decode($item->dc_desconte))
            );
        }

        # DATOS_CONTRATOS
        $queryDatosContrato = "INSERT INTO DATOS_CONTRATOS (CO_CEDULA, CO_ITEMCONT, CO_CODEMPRESA, CO_EMPRESA, CO_FECHA_ING, CO_FECHA_FIN, CO_FECHA_RET, CO_CODRETIRO, CO_OBSERVACION, CO_USUGRABA, CO_CODREAL, CO_MOTREAL, CO_TCONTRATO)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $dataDatosContrato = ibase_prepare($conexion, $queryDatosContrato);
        $contrato = json_decode($contrato);
        foreach ($contrato as $item) {
            ibase_execute($dataDatosContrato,
                $co_cedula, $item->co_item, $item->co_codempresa, strtoupper(utf8_decode($item->co_empresa)), $item->co_fechaing,
                $item->co_fechafin, $item->co_fecharet, $item->co_codretiro, strtoupper(utf8_decode($item->co_observacion)),
                $_SESSION['user'], $item->co_codreal, '', $item->co_tcontrato
            );
        }

        # DATOS_FORMACADEMICA
        $queryFormacionAcademica = "INSERT INTO DATOS_FORMACADEMICA (DF_CEDULA, DF_CODIGO, DF_FORMACADEMICA, DF_INSTEDUCATIVA, DF_TITULOOBT, DF_USUGRABA)
                                    VALUES (?, ?, ?, ?, ?, ?)";
        $dataFormacionAcademica = ibase_prepare($conexion, $queryFormacionAcademica);
        $facademica = json_decode($facademica);
        foreach ($facademica as $item) {
            ibase_execute($dataFormacionAcademica,
                $df_cedula, $item->df_codigo, strtoupper(utf8_decode($item->df_formacademica)), strtoupper(utf8_decode($item->df_insteducativa)), strtoupper(utf8_decode($item->df_tituloobt)), $_SESSION['user']
            );
        }

        # VENCIMIENTOS
        $queryVencimientos = "INSERT INTO VENCIMIENTOS (MODULO, CEDULA, CODTIPOVENCE, DESCRIPCION, FECHAVENCE)
                              VALUES ('RH', ?, ?, ?, ?)";
        $dataVencimientos = ibase_prepare($conexion, $queryVencimientos);
        $vencimientos = json_decode($vencimientos);
        foreach ($vencimientos as $item) {
            ibase_execute($dataVencimientos,
                $cedula, $item->codtipovence, strtoupper(utf8_decode($item->descripcion)), $item->fechavence
            );
        }

        # INFORME_FAMILIAR
        $queryInfoFamiliar = "INSERT INTO INFORME_FAMILIAR (IF_CEDULA, IF_NOMBRECONY, IF_FECHANACCONY, IF_CIUDAD, IF_CEDULACONY, IF_CIUDADEXP, IF_PROFECONY, IF_CANHIJOS, IF_SEXOCONY)
                              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $dataInfoFamiliar = ibase_prepare($conexion, $queryInfoFamiliar);
        ibase_execute($dataInfoFamiliar,
            $if_cedula, strtoupper(utf8_decode($if_nombrecony)), $if_fechanaccony, $if_ciudad, $if_cedulacony,
            $if_ciudadexp, $if_profecony, $if_canhijo, strtoupper(utf8_decode($if_sexocony))
        );

        # INFORME_HIJOS
        $queryInfoHijos = "INSERT INTO INFORME_HIJOS (IH_CEDULA, IH_NOMBREHIJO, IH_FECHANAC, IH_IDENTIFICA, IH_SEXO)
                           VALUES (?, ?, ?, ?, ?)";
        $dataInfoHijos = ibase_prepare($conexion, $queryInfoHijos);
        $hijos = json_decode($hijos);
        foreach ($hijos as $item) {
            ibase_execute($dataInfoHijos,
                $ih_cedula, strtoupper(utf8_decode($item->ih_nombrehijo)), $item->ih_fechanac, $item->ih_identifica, strtoupper(utf8_decode($item->ih_sexo))
            );
        }
    }
//    $cod = "GEN_ID(TECNICO, 1)";
    ibase_commit($tr);
} catch (Exception $e) {
    ibase_rollback($tr);
    return $e->getMessage();
}

// if ($rh_foto != 'null') {
//     unlink($_SERVER['DOCUMENT_ROOT'].$rh_foto);//acá le damos la direccion exacta del archivo
// }
$mensaje = array(
    'type'    => 'success',
    'message' => 'Registrado guardado con exito..!',
    'title'   => 'Guardado..',
    'error'   => 0
);
echo json_encode($mensaje);