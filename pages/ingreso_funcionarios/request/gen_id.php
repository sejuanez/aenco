<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 16/07/2018
 * Time: 7:08 AM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$query = "SELECT GEN_ID(TECNICO, 1) te_codigo FROM empresa";
$return_arr = array();
$data = ibase_query($conexion, $query);
while($fila = ibase_fetch_row($data)){
    $row_array['te_codigo'] = utf8_encode($fila[0]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);