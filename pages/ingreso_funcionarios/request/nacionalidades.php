<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Fecha: 5/07/2018
 * Time: 8:31 AM
 */

    session_start();
    if(!$_SESSION['user']){
        echo"<script>window.location.href='../../inicio/index_.php';</script>";
        exit();
    }
    include('../../../init/gestion.php');

    $query = "SELECT na_codigo, na_nacional FROM nacionalidades";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['na_codigo'] = utf8_encode($row[0]);
        $row_array['na_nacional'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);