<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 13/07/2018
 * Time: 9:55 AM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$dc_cedula = $_POST['dc_cedula'];

$query = "SELECT dc_codconte, dc_desconte FROM datos_conte WHERE dc_cedula = '".$dc_cedula."'";

$return_arr = array();
$data = ibase_query($conexion, $query);

while($fila = ibase_fetch_row($data)){
    $row_array['dc_codconte'] = utf8_encode($fila[0]);
    $row_array['dc_desconte'] = utf8_encode($fila[1]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);