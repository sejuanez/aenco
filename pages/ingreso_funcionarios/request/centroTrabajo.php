<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT ct_codigo, ct_centrabajo FROM centro_trabajo ORDER BY ct_centrabajo ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['ct_codigo'] = utf8_encode($row[0]);
		$row_array['ct_centrabajo'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>