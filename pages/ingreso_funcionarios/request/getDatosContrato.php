<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 13/07/2018
 * Time: 9:55 AM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$co_cedula = $_POST['co_cedula'];

$query = "SELECT dc.co_itemcont, dc.co_codempresa, dc.co_empresa, EXTRACT(MONTH FROM dc.co_fecha_ing)||'/'||EXTRACT(DAY FROM dc.co_fecha_ing)||'/'||EXTRACT(YEAR FROM dc.co_fecha_ing) co_fecha_ing, EXTRACT(MONTH FROM dc.co_fecha_fin)||'/'||EXTRACT(DAY FROM dc.co_fecha_fin)||'/'||EXTRACT(YEAR FROM dc.co_fecha_fin) co_fecha_fin, EXTRACT(MONTH FROM dc.co_fecha_ret)||'/'||EXTRACT(DAY FROM dc.co_fecha_ret)||'/'||EXTRACT(YEAR FROM dc.co_fecha_ret) co_fecha_ret, dc.co_codretiro, mr.mr_motivo, dc.co_codreal, dc.co_motreal, dc.co_tcontrato
          FROM datos_contratos dc
          LEFT JOIN motivo_retiro mr ON mr.mr_codigo = dc.co_codretiro
          WHERE dc.co_cedula = '".$co_cedula."'";

$return_arr = array();
$data = ibase_query($conexion, $query);

while($fila = ibase_fetch_row($data)){
    $row_array['co_item']         = utf8_encode($fila[0]);
    $row_array['co_codempresa']   = utf8_encode($fila[1]);
    $row_array['co_empresa']      = utf8_encode($fila[2]);
    $row_array['co_fechaing']     = utf8_encode($fila[3]);
    $row_array['co_fechafin']     = utf8_encode($fila[4]);
    $row_array['co_fecharet']    = utf8_encode($fila[5]);
    $row_array['co_codretiro']    = utf8_encode($fila[6]);
    $row_array['co_motivoretiro'] = utf8_encode($fila[7]);
    $row_array['co_codreal']      = utf8_encode($fila[8]);
    $row_array['co_motreal']      = utf8_encode($fila[9]);
    $row_array['co_tcontrato']    = utf8_encode($fila[10]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);