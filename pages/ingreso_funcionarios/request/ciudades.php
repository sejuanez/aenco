<?php
	session_start();
	if(!$_SESSION['user']){
	  echo"<script>window.location.href='../../inicio/index.php';</script>";
	  exit();
	}
    include('../../../init/gestion.php');

	$queryAll = "SELECT DISTINCT MU_CODIGO, SUBSTRING(MU_CODIGO FROM 3 FOR 5) COD, MU_MUNICIPIO, MU_CODDPTO FROM municipios_col";
	$return_arrAll = array();

	$data = ibase_query($conexion, $queryAll);
    while ($row = ibase_fetch_row($data)) {
		$row_array['mu_codigo']    = utf8_encode($row[0]);
		$row_array['cod']          = utf8_encode($row[1]);
		$row_array['mu_municipio'] = utf8_encode($row[2]);
		$row_array['mu_coddpto']   = utf8_encode($row[3]);
        array_push($return_arrAll, $row_array);
    }
    echo json_encode($return_arrAll);