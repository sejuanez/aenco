<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 16/07/2018
 * Time: 4:23 PM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$if_cedula = $_POST['if_cedula'];
$query = "SELECT if_nombrecony, EXTRACT(MONTH FROM if_fechanaccony)||'/'||EXTRACT(DAY FROM if_fechanaccony)||'/'||EXTRACT(YEAR FROM if_fechanaccony) if_fechanaccony, if_ciudad, if_cedulacony, if_ciudadexp, if_profecony, if_canhijos, if_sexocony FROM informe_familiar WHERE if_cedula = '".$if_cedula."'";
$return_arr = array();
$data = ibase_query($conexion, $query);

while($fila = ibase_fetch_row($data)){
    $row_array['if_nombrecony']   = utf8_encode($fila[0]);
    $row_array['if_fechanaccony'] = utf8_encode($fila[1]);
    $row_array['if_ciudad']       = utf8_encode($fila[2]);
    $row_array['if_cedulacony']   = utf8_encode($fila[3]);
    $row_array['if_ciudadexp']    = utf8_encode($fila[4]);
    $row_array['if_profecony']    = utf8_encode($fila[5]);
    $row_array['if_canhijos']     = utf8_encode($fila[6]);
    $row_array['if_sexocony']     = utf8_encode($fila[7]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);