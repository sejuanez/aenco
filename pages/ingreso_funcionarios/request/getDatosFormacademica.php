<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 16/07/2018
 * Time: 3:52 PM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$df_cedula = $_POST['df_cedula'];
$query = "SELECT df_codigo, df_formacademica, df_insteducativa, df_tituloobt FROM datos_formacademica WHERE df_cedula = '".$df_cedula."'";

$return_arr = array();
$data = ibase_query($conexion, $query);
while($fila = ibase_fetch_row($data)){
    $row_array['df_codigo']        = utf8_encode($fila[0]);
    $row_array['df_formacademica'] = utf8_encode($fila[1]);
    $row_array['df_insteducativa'] = utf8_encode($fila[2]);
    $row_array['df_tituloobt']     = utf8_encode($fila[3]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);