<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT ba_codigo, ba_nombre FROM bancos ORDER BY ba_nombre ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['ba_codigo'] = utf8_encode($row[0]);
		$row_array['ba_nombre'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>