<?php
    session_start();
    if(!$_SESSION['user']){
      echo"<script>window.location.href='../../inicio/index.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');

    $mu_depto = $_POST['mu_depto'];

    $query = "SELECT mu_codigo, mu_municipio FROM municipios_col WHERE mu_coddpto = '$mu_depto' ORDER BY mu_municipio ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['mu_codigo'] = utf8_encode($row[0]);
        $row_array['mu_municipio'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>