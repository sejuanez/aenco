<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 17/07/2018
 * Time: 7:55 AM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$cedula = $_POST['cedula'];
$query = "SELECT cedula, codtipovence, descripcion, EXTRACT(MONTH FROM fechavence)||'/'||EXTRACT(DAY FROM fechavence)||'/'||EXTRACT(YEAR FROM fechavence) fechavence FROM vencimientos WHERE cedula = '".$cedula."'";

$return_arr = array();
$data = ibase_query($conexion, $query);
while($fila = ibase_fetch_row($data)){
    $row_array['cedula']       = utf8_encode($fila[0]);
    $row_array['codtipovence'] = utf8_encode($fila[1]);
    $row_array['descripcion']  = utf8_encode($fila[2]);
    $row_array['fechavence']   = utf8_encode($fila[3]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);