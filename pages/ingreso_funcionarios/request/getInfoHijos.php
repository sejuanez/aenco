<?php
/**
 * User: LUIS LOPEZ LOPEZ
 * Date: 16/07/2018
 * Time: 4:33 PM
 */
session_start();
if (!$_SESSION['user']) {
    echo "<script>window.location.href='../../../index_.php';</script>";
    exit();
}
include("../../../init/gestion.php");

$ih_cedula = $_POST['ih_cedula'];
$query = "SELECT ih_nombrehijo, EXTRACT(MONTH FROM ih_fechanac)||'/'||EXTRACT(DAY FROM ih_fechanac)||'/'||EXTRACT(YEAR FROM ih_fechanac) ih_fechanac, ih_identifica, ih_sexo FROM informe_hijos WHERE ih_cedula = '".$ih_cedula."'";
$return_arr = array();
$data = ibase_query($conexion, $query);

while($fila = ibase_fetch_row($data)){
    $row_array['ih_nombrehijo'] = utf8_encode($fila[0]);
    $row_array['ih_fechanac']   = utf8_encode($fila[1]);
    $row_array['ih_identifica'] = utf8_encode($fila[2]);
    $row_array['ih_sexo']       = utf8_encode($fila[3]);
    array_push($return_arr, $row_array);
}
echo json_encode($return_arr);