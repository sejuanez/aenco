<?php
    session_start();
    if(!$_SESSION['user']){
      echo "<script>window.location.href='../../../index_.php';</script>";
      exit();
    }
    include('../../../init/gestion.php');
    $query = "SELECT rh_cedula, rh_nombre1||' '||rh_nombre2||' '||rh_apellido1||' '||rh_apeelido2 AS full_name FROM datos_rrhh ORDER BY rh_nombre1 ASC";
    $return_arr = array();

    $data = ibase_query($conexion, $query);
    while ($row = ibase_fetch_row($data)) {
        $row_array['rh_cedula'] = utf8_encode($row[0]);
		$row_array['full_name'] = utf8_encode($row[1]);
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
?>