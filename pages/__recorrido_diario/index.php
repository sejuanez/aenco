<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
  } 
?>

<!DOCTYPE html5>

<html>

<head>
	
  <meta charset="utf-8">
	
  <title>Recorrido diario</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  
	<link rel="stylesheet" type="text/css" href="css/estilo.css">

  <link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  <link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />
  
  <link rel='stylesheet' href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />
  
  <link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
  
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="../../js/gmaps.js"></script>


  	<script type="text/javascript">
    
           
      
      var hoy= new Date();
      var dias=["domingo","lunes","martes","miércoles","jueves","viernes","sábado"];
      var meses=["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"];
      var dia=hoy.getDate();
      var mes=hoy.getMonth()+1;
      var anio=hoy.getFullYear();
      
      var nodoActivo=null;
      var map;
      var limits;

      var arrayMarker=[];
      var infowindow;

     function animarMarker(val){
          
          infowindow.setContent("<h6>"+arrayMarker[val].title+"</h6>");
          infowindow.open(map.map,map.markers[val]);
    };

    function play(){
      var cont=0;
        var timeout = setInterval(function(){
          if(cont < arrayMarker.length){
           
            map.addMarker(arrayMarker[cont]);
            infowindow.setContent("<h6>"+arrayMarker[cont].title+"</h6>");
            infowindow.open(map.map,map.markers[cont]);
          }            
          else{
            
            clearInterval(timeout);
            infowindow.open(map.map,map.markers[0]);
            document.getElementById("recorrido").style.display="inline";
            document.getElementById("slider").focus();
          }
            
          cont++;
        },300,cont)
    };
      
      
  		$(function(){

        $("#modal").hide();

                
        function cargarTecnicos(){

          $('#tecnico').html("<option>Cargando...</option>");
          $('#tecnico').focus();
          $("#buscar").button({disabled:true});
          $.ajax({
                      url:'request/getTecnicos.php',
                      type:'POST',
                      dataType:'json',
                      data:{fecha:$('#fecha').datepicker('getDate').getFullYear()+"-"+($('#fecha').datepicker('getDate').getMonth() + 1)+"-"+$('#fecha').datepicker('getDate').getDate()}
                  }).done(function (repuesta){
                      if(repuesta.length>0){
                        var tecnicos="";
                        for(var i=0; i<repuesta.length;i++){
                          
                          tecnicos+="<option value='"+repuesta[i].codigo+"' data-nombres='"+repuesta[i].nombres+"' data-cuadrilla='"+repuesta[i].cuadrilla+"'>"+repuesta[i].codigo+" - "+repuesta[i].nombres+"</option>";
                        }
                        $('#tecnico').html(tecnicos);
                        $("#buscar").button({disabled:false});
                        //$("#buscar").focus();
                      }
                      else{
                        $('#tecnico').html("<option>Vacío</option>");
                      }
            });

        };

        
        function iniciarComponentes(){

          google.maps.event.addDomListener(window, "resize", function() {
             
             var center_lat = parseFloat(map.getCenter().lat()).toPrecision(4);
             var center_lng = parseFloat(map.getCenter().lng()).toPrecision(5);
             $("#container-map").css({width:'100%', height:"100%"});
             $("#div-map").css({width:'100%', height:"100%"});             
             google.maps.event.trigger(map, "resize");
             map.setCenter(center_lat, center_lng);
             
            });

            $("#buscar").button();
            $("#buscar").button({disabled:true});

            $('#fecha').datepicker(
              {   dateFormat: 'dd/mm/yy',//'yy/M/d',
                  minDate: '-30Y',
                  maxDate: '+30Y',
                  changeMonth: true,
                  changeYear: true,
                  numberOfMonths: 1,
                  dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                      'Junio', 'Julio', 'Agosto', 'Septiembre',
                      'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
                      'May', 'Jun', 'Jul', 'Ago',
                      'Sep', 'Oct', 'Nov', 'Dic']
                
               });

            $('#fecha').datepicker( "setDate", new Date());
            
            $('#fecha').change(function(){
              cargarTecnicos();
              
            });
           
            
          cargarTecnicos();


                    
          $("#buscar").button().click(function(e){
            e.preventDefault();
            document.getElementById("recorrido").style.display="none";
            document.getElementById("slider").max=0;
            $("#buscar").button({disabled:true});
            $("#modal h1").html("Buscando...");
            $("#modal p").html("Por favor espere mientras realizamos la consulta.");
            $("#modal").animate({right:'0%'},500);

            map.removeMarkers();
            
             
             var tecnicoSeleccionado = document.getElementById("tecnico").options[document.getElementById("tecnico").selectedIndex];

              $.ajax({
                      url:'request/getRecorrido.php',
                      type:'POST',
                      dataType:'json',
                      data:{
                            anio:$('#fecha').datepicker('getDate').getFullYear(),
                            mes:$('#fecha').datepicker('getDate').getMonth() + 1,
                            dia:$('#fecha').datepicker('getDate').getDate(),
                            codtecnico:document.getElementById("tecnico").value,
                            nombretecnico:tecnicoSeleccionado.dataset['nombres']//,
                            //cuadrilla:tecnicoSeleccionado.dataset['cuadrilla']
                          }
                  }).done(function (repuesta){

                      if(repuesta.length>0){
                          
                          document.getElementById("slider").max=repuesta.length-1;
                          
                          arrayMarker=[];
                                                                              
                          limits=new google.maps.LatLngBounds();
                          
                          map.setCenter(repuesta[0].lat,repuesta[0].lon);
                          
                          infowindow = new google.maps.InfoWindow({content: "<h6>"+repuesta[0].hora+"</h6>"});

           
                          for(var i=0;i<repuesta.length;i++){
                              arrayMarker[i]={position:{lat:+repuesta[i].lat,lng:+repuesta[i].lon},title:repuesta[i].hora,icon:'../imagenes/'+tecnicoSeleccionado.dataset['cuadrilla']+'.png'};
                              //arrayMarker[i]={position:{lat:+repuesta[i].lat,lng:+repuesta[i].lon},title:repuesta[i].hora,icon:'img/'+document.getElementById("tecnico").value+'.png'};
                              
                              //map.addMarker({lat:repuesta[i].lat, lng:repuesta[i].lon, icon:'img/'+document.getElementById("tecnico").value+'.png', title:repuesta[i].hora,infoWindow:{content:"<h6>"+repuesta[i].hora+"</h6>"}});

                              limits.extend(new google.maps.LatLng(repuesta[i].lat,repuesta[i].lon));
                          }
                          map.fitBounds(limits);
                          $("#modal").animate({right:'-100%'},500);
                          $("#buscar").button({disabled:false});

                          play();
                      }
                      else{
                        $("#modal h1").html("Lo sentimos!");
                        $("#modal p").html("No se encontraron resultados para esta consulta. Vuelve a intentar.");
                      }
                     $("#buscar").button({disabled:false});
                  });
          });
               

        };//fin de iniciarComponentes()

        //===================================================================================

        function cargarMapa(){
              //GMaps.geolocate({
                //  success: function(position){
                        
                         map = new GMaps({  // muestra mapa centrado en coords [lat, lng]
                            el: '#div-map',
                            lat: 8.749349, /*position.coords.latitude,*/
                            lng: -75.879568,/*position.coords.longitude,*/
                            zoom:10
                          });

                         
                                                
                         
                         //$("#header").html("<ul><li><a href='../index.php' id='atras' class='ion-ios-arrow-thin-left' title='Regresar'></a></li><li><h3>Recorrido diario</h3><h5>"+dias[hoy.getDay()]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></li><li><h5>Fecha</h5><input type='text' id='fecha' readOnly /></li><li><h5>Técnico</h5><select id='tecnico' ></select></li><li><h5>&nbsp;</h5><a href='#' id='buscar'>Buscar</a></li><li id='recorrido' style='display:none'><h5>Recorrido</h5><input type='range' id='slider' min=0 max=100 value=0  oninput='animarMarker(this.value)' onchange='animarMarker(this.value)' /></li></ul>");
                         $("#header").html("<ul><li><h3>Recorrido diario</h3><h5>"+dias[hoy.getDay()]+", "+dia+" de "+meses[mes-1]+" de "+anio+"</h5></li><li><h5>Fecha</h5><input type='text' id='fecha' readOnly /></li><li><h5>Técnico</h5><select id='tecnico' ></select></li><li><h5>&nbsp;</h5><a href='#' id='buscar'>Buscar</a></li><li id='recorrido' style='display:none'><h5>Recorrido</h5><input type='range' id='slider' min=0 max=100 value=0  oninput='animarMarker(this.value)' onchange='animarMarker(this.value)' /></li></ul>");
                         
                         

                         $("#modal").show();
                         //limits = new google.maps.LatLngBounds();
                         limits = new google.maps.LatLngBounds();
                         iniciarComponentes();                                      
                  /*},
                  error: function(error){
                     alert('Fallo en la geolocalizacion:'+error.message);
                     return false;
                  },
                  not_supported:function(){
                    alert('Geolocalizacion no soportada');
                    return false;
                  }
              });*/
        };
        //======================================================================================

        
        
        cargarMapa();
        
       
  		});//fin del onready
  	</script>
</head>
  <body>
    <div id="header"></div>

    <div id='modal'><h1>Bienvenido!</h1><p>Escoge una fecha y selecciona un tecnico, luego pulsa Buscar...</p></div>


    
    <div id="contenido">

      <!--<div id="div-wrap-tecnicos">
          <div id="div-tecnicos">
          </div>
      </div>-->



      <div id="container-map">
        <div id="div-map">

        </div>
      </div>

       <!--<div id="detalles" ></div>-->
      

      

    </div>

  </body>
</html>