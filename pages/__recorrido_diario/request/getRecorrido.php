<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }


    
	include("../../../init/gestion.php");

	$dia = $_POST["dia"];
	if($dia<10)
		$dia = "0".$dia;

	$mes = $_POST["mes"];
	if($mes<10)
		$mes = "0".$mes;

	$anio = $_POST["anio"];

	$fecha_format = $anio."-".$mes."-".$dia;
	//$fecha_format = '2015-08-21';

	$codtecnico = $_POST["codtecnico"];
	$nombretecnico = $_POST["nombretecnico"];
	//$cuadrilla = $_POST["cuadrilla"];
	
	
	$consulta = "SELECT dgr_lat, dgr_long, dgr_hora, dgr_tecnico, dgr_fecha FROM h_datos_geo_referenciacion  DG WHERE DG.dgr_fecha='".$fecha_format."' AND DG.dgr_tecnico='".$nombretecnico."' ORDER BY DG.dgr_hora";
	

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['lat'] = utf8_encode($fila[0]);
		$row_array['lon'] = utf8_encode($fila[1]);
		$row_array['hora'] = utf8_encode($fila[2]);
		$row_array['tecnico'] = utf8_encode($fila[3]);
		$row_array['fecha'] = utf8_encode($fila[4]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>