<?php
	session_start();
	
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	include("../../../init/gestion.php");
	$fecha = $_POST['fecha'];
	
	//$consulta = "SELECT distinct(dg.dgr_tecnico), t.te_nombres, t.te_codigo,t.te_cuadrilla from h_datos_geo_referenciacion dg left join tecnicos t on t.te_codigo=dg.dgr_tecnico order by t.te_codigo asc";
	
	$consulta = "SELECT distinct t.te_codigo, t.te_nombres, t.te_cuadrilla from tecnicos t inner join h_datos_geo_referenciacion DG on dg.dgr_tecnico=t.te_codigo and dg.dgr_fecha='".$fecha."'";

	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		
		$row_array['codigo'] = utf8_encode($fila[1]);
		$row_array['nombres'] = utf8_encode($fila[0]);
		$row_array['cuadrilla'] = utf8_encode($fila[2]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>