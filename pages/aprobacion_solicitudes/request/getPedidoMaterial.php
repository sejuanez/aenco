<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");
// include("gestion.php");


$fechaInicial = utf8_decode($_POST['fechaInicial']);
$fechaFinal = utf8_decode($_POST['fechaFinal']);
$tipoOrden = utf8_decode($_POST['tipoOrden']);
$orden = utf8_decode($_POST['orden']);
$tecnico = utf8_decode($_POST['tecnico']);

//echo $fechaInicial . $fechaFinal . $tipoOrden . $orden . $tecnico;

$whereFecha = '';
if ($fechaFinal != "") {
    $whereFecha = " AND  (pm.pm_fecha_solicita between '" . $fechaInicial . "' and '" . $fechaFinal . "')";
}

$whereOrden = '';
if ($orden != "") {
    if ($tipoOrden == 1) {
        $whereOrden = " AND pm.orden_externa = '" . $orden . "'";
    } else {
        $whereOrden = " AND pm.pm_numero = '" . $orden . "'";
    }
}

$whereTecnico = '';
if ($tecnico != "") {
    $whereTecnico = " AND pm.pm_tecnico_lider = " . $tecnico;
}


//echo $whereFecha . $whereOrden . $whereTecnico;


$sql = "select
                pm.pm_numero,
                pm.orden_externa, 
                pm.pm_proyecto, 
                pm.pm_consecutivo, 
                pm.pm_reserva,                
                pm.pm_instalacion, 
                pm.pm_ingeniero, 
                pm.pm_circuito, 
                pm.pm_nodo, 
                pm.pm_direccion,                              
                pm.pm_departamento,
                pm.pm_municipio, 
                pm.pm_trabajo_realizar, 
                pm.pm_supervisor_responable,                
                t.te_nombres, 
                pm.pm_placa_movil, 
                pm.pm_fecha_solicita, 
                pm.pm_hora_solicita,
                pm.pm_usuario_crea, 
                pm.pm_estado_pedido
      from      pedido_materiales pm
      inner join tecnicos t on t . te_codigo = pm . pm_tecnico_lider
      where pm . pm_estado_pedido = 'SOLICITADO' " . $whereFecha . $whereOrden . $whereTecnico . "
      order by pm . pm_numero desc
            
                                ";

//echo $sql;


$result = ibase_query($conexion, $sql);

$return_arr = array();


while ($fila = ibase_fetch_row($result)) {
    $row_array['numero'] = utf8_encode($fila[0]);
    $row_array['gom'] = utf8_encode($fila[1]);
    $row_array['proyecto'] = utf8_encode($fila[2]);
    $row_array['consecutivo'] = utf8_encode($fila[3]);
    $row_array['reserva'] = utf8_encode($fila[4]);
    $row_array['instalacion'] = utf8_encode($fila[5]);
    $row_array['ingeniero'] = utf8_encode($fila[6]);
    $row_array['circuito'] = utf8_encode($fila[7]);
    $row_array['nodo'] = utf8_encode($fila[8]);
    $row_array['direccion'] = utf8_encode($fila[9]);
    $row_array['departamento'] = utf8_encode($fila[10]);
    $row_array['municipio'] = utf8_encode($fila[11]);
    $row_array['trabajo'] = utf8_encode($fila[12]);
    $row_array['supervisor'] = utf8_encode($fila[13]);
    $row_array['tecnico'] = utf8_encode($fila[14]);
    $row_array['placa'] = utf8_encode($fila[15]);
    $row_array['fecha'] = utf8_encode($fila[16]);
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);



