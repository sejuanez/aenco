<?php

define('MAX_SEGMENT_SIZE', 65535);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");

$data = $_POST['data'];
$opcion = $_POST['opcion'];
$consecutivo = $_POST['consecutivo'];
$reserva = $_POST['reserva'];
$return_arr = array();

$n = count($data);


if ($opcion == 'APROBAR') {

    $stmt = "EXECUTE PROCEDURE PEDIDO_MATERIAL_MODIF(1,
                                                     '" . $data[0]['numero'] . "',                                                    
                                                     '" . $consecutivo . "',                                                    
                                                     '" . $reserva . "'                                                    
                                                     )";
    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);

    $row_array['array'] = array(
        "opcion" => $opcion,
        "pedido" => $result,
        "detalle" => null,
    );
    array_push($return_arr, $row_array);

    for ($i = 0; $i < count($data); $i++) {

        $stmt = "EXECUTE PROCEDURE PEDIDO_MATERIAL_DETALLE_MODIF('" . $data[$i]['numero'] . " ',
                                                             '" . $data[$i]['codMateerial'] . "',
                                                             '" . $data[$i]['cantidad_aprobada'] . "'
                                                              )";
        $query = ibase_prepare($stmt);
        $result = ibase_execute($query);

        $row_array['array'] = array(
            "opcion" => null,
            "pedido" => null,
            "detalle" => $result
        );


        array_push($return_arr, $row_array);

    }


} else {

    $stmt = "EXECUTE PROCEDURE PEDIDO_MATERIAL_MODIF(2,
                                                     '" . $data[0]['numero'] . "',
                                                     null,
                                                     null                                                     
                                                     )";
    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);

    $row_array['array'] = array(
        "opcion" => $opcion,
        "pedido" => $result,
        "detalle" => null,
    );
    array_push($return_arr, $row_array);

    for ($i = 0; $i < count($data); $i++) {

        $stmt = "EXECUTE PROCEDURE PEDIDO_MATERIAL_DETALLE_MODIF('" . $data[$i]['numero'] . " ',
                                                             '" . $data[$i]['codMateerial'] . "',
                                                             0
                                                              )";
        $query = ibase_prepare($stmt);
        $result = ibase_execute($query);

        $row_array['array'] = array(
            "opcion" => null,
            "pedido" => null,
            "detalle" => $result
        );


        array_push($return_arr, $row_array);

    }


}


echo json_encode($return_arr);



