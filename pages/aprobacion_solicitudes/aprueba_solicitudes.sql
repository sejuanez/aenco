SET TERM ^ ;

create or alter procedure PEDIDO_MATERIAL_MODIF (
    EOPCION integer,
    ENUMERO integer,
    CONSECUTIVO integer,
    RESERVA integer)
as
begin
------------------------------------------------
------------ APRUEBA PEDIDO --------------------
------------------------------------------------
  if (eopcion = 1) then
    begin
      update PEDIDO_MATERIALES pm
      set   pm.pm_estado_pedido = 'APROBADO',
            pm.pm_fecha_estado = current_date,
            pm.pm_consecutivo = :consecutivo,
            pm.pm_reserva = :reserva
      where pm.pm_numero = :enumero;
    end
------------------------------------------------/
------------ RECHAZA PEDIDO --------------------/
------------------------------------------------/

  if (eopcion = 2) then
    begin
      update PEDIDO_MATERIALES pm
      set pm.pm_estado_pedido = 'RECHAZADO', pm.pm_fecha_estado = current_date
      where pm.pm_numero = :enumero;
    end


end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON PEDIDO_MATERIALES TO PROCEDURE PEDIDO_MATERIAL_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PEDIDO_MATERIAL_MODIF TO SYSDBA;
