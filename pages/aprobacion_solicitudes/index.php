<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">

        input[type='number'] {
            -moz-appearance: textfield;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }


        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 58vh;
            overflow-y: scroll;
        }

        #tbody_detalle {
            display: block;
            height: 38vh;
            overflow-y: scroll
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Aprobaciòn Solicitudes


            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="buscar();">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-4">
                <fieldset>
                    <legend>Fecha Solicitud</legend>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group"><label for="">Fecha Inicio</label>
                                <div class="input-group input-group-sm"><input type="date" id="" aria-label=""
                                                                               v-model="fechaInicial"
                                                                               class="form-control "></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group"><label for="">Fecha Fin:</label>
                                <div class="input-group input-group-sm"><input type="date" id="" aria-label=""
                                                                               v-model="fechaFinal"
                                                                               class="form-control "></div>
                            </div>
                        </div>

                    </div>
                    <br></fieldset>
            </div>

            <div class="col-4">
                <fieldset>
                    <legend>Tipo de orden</legend>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group"><label for="">Tipo</label>
                                <div class="input-group input-group-sm">
                                    <select name="" id="" v-model="tipoOrden">
                                        <option value="1">GOM</option>
                                        <option value="2">Interna</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group"><label for="">Orden:</label>
                                <div class="input-group input-group-sm">
                                    <input type="text" id="" aria-label=""
                                           v-model="orden"
                                           class="form-control"></div>
                            </div>
                        </div>

                    </div>
                    <br></fieldset>
            </div>

            <div class="col-4">
                <fieldset>
                    <legend>Tecnico</legend>
                    <div class="row">

                        <div class="col-12">
                            <div class="form-group"><label for="">Seleccionar</label>
                                <div class="input-group input-group-sm">

                                    <select class="selectTecnico" id="selectTecnico" name="selectTecnico"
                                            v-model="valorTEC">
                                        <option value="">Tecnico lider...</option>
                                        <option v-for="item in selectTecnico"
                                                :value="item.codigo">
                                            {{item.nombre}}
                                        </option>
                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>
                    <br></fieldset>
            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%; margin-top: 5px">

        <div class="row">
            <div class="col-12 my-tbody">


                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="100">Orden</th>
                        <th width="100">GOM</th>
                        <th>Tecnico</th>
                        <th>Placa</th>
                        <th>Fecha</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaPedidos.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaPedidos">
                        <td v-text="dato.numero" width="100"></td>
                        <td v-text="dato.gom" width="100"></td>
                        <td v-text="dato.tecnico"></td>
                        <td v-text="dato.placa"></td>
                        <td v-text="dato.fecha"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verDetalle(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="modalDetalle" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Detalle del pedido
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <div style="padding-bottom: 5px" class="row">

                        <div class="col-12 col-sm-3"></div>

                        <div class="col-12 col-sm-3">
                            <div class="form-label-group">
                                <input type="text" id="consecutivo" class="form-control" placeholder="Email address"
                                       required=""
                                       autofocus=""
                                       autocomplete="nope"
                                       v-model="consecutivo">
                                <label for="consecutivo">Consecutivo</label>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3">
                            <div class="form-label-group">
                                <input type="text" id="reserva" class="form-control" placeholder="Email address"
                                       required=""
                                       autofocus=""
                                       autocomplete="nope"
                                       v-model="reserva">
                                <label for="reserva">Reserva</label>
                            </div>
                        </div>

                    </div styl>

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th>Material</th>
                                    <th width="80">C Pedida</th>
                                    <th width="100">C Aprobada</th>
                                    <th style="text-align: center;" width="70">Editar</th>
                                    <th style="text-align: center;margin: 5px" width="80">
                                        <i class="fa fa-check-square" title=""
                                           style="cursor:pointer; margin:5px;"
                                           @click="aprobarCantidad('', '', 'apruebaTodo')"></i>
                                        <i class="fa fa-trash-o" title=""
                                           style="cursor:pointer; margin:5px;"
                                           @click="aprobarCantidad('', '', 'desapruebaTodo')"></i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbody_detalle">
                                <tr v-if="tablaPedidos.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaDetalle">
                                    <td v-text="dato.material"></td>
                                    <td width="80" v-text="dato.cantidad"></td>
                                    <td width="100" v-text="dato.cantidad_aprobada"></td>
                                    <td width="70" style="text-align: center;">
                                        <i class="fa fa-pencil" title=""
                                           style="cursor:pointer; margin: 0 0 10px 10px;"
                                           @click="editarDetalle(dato, index)"></i>
                                    </td>
                                    <td width="80" style="text-align: center;">

                                        <i class="fa fa-check-square-o" title=""
                                           style="cursor:pointer; margin:5px;"
                                           @click="aprobarCantidad(dato, index, 'aprueba')"></i>
                                        <i class="fa fa-trash-o" title=""
                                           style="cursor:pointer; margin:5px;"
                                           @click="aprobarCantidad(dato, index, 'desaprueba')"></i>

                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModalDetalle()">
                        Cancelar
                    </button>

                    <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="aprobar()">
                        Aprobar
                    </button>

                    <button type="button" v-if="true" class="btn btn-danger btn-sm" @click="rechazar()">
                        Rechazar
                    </button>


                </div>


            </div>


        </div>
    </div>

    <div id="modalEditaDetalle" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1"
         role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Detalle del pedido
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">


                            <div class="col-6">
                                <div class="form-group input-group-sm">
                                    <label for="descripcion_modal">Material: </label>
                                    <input type="text" id="descripcion" name="descripcion_modal" readonly
                                           class="form-control" required aria-label="materal"
                                           v-model="material">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Cantidad </label>
                                    <input type="text" id="descripcion" name="nombre_modal" readonly
                                           class="form-control" required aria-label="nombre_modal"
                                           v-model="cantidad">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Cantidad aprobada </label>
                                    <input type="number" id="descripcion"
                                           class="form-control noSigno"
                                           v-model="cantidad_aprobada">
                                </div>
                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm"
                                @click="resetModalCantidad()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizarDetalle()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
