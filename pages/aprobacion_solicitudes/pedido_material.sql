SET TERM ^ ;

create or alter procedure PEDIDO_MATERIAL_DETALLE_BUSCA (
    ECOD integer)
returns (
    NUMERO integer,
    CODMATER varchar(15),
    MATERIAL varchar(100),
    CANTIDADPEDIDA numeric(15,2),
    CANTIDADAPROBADA numeric(15,2))
as
begin
    --Mostrar todos los datos.
    for
        select PMD_NUMERO, PMD_CODMATER, m.ma_descripcion, PMD_CANTIDAD_PEDIDA, PMD_CANT_APROBADA
        from PEDIDO_MATERIALES_DETALLE
        left join materiales m on m.ma_codigo = PMD_CODMATER
        where pmd_numero = :ecod
        into :numero, :codmater, :material, :cantidadpedida, :cantidadaprobada
    do
    begin
      suspend;
    end
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT ON PEDIDO_MATERIALES_DETALLE TO PROCEDURE PEDIDO_MATERIAL_DETALLE_BUSCA;
GRANT SELECT ON MATERIALES TO PROCEDURE PEDIDO_MATERIAL_DETALLE_BUSCA;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PEDIDO_MATERIAL_DETALLE_BUSCA TO SYSDBA;
