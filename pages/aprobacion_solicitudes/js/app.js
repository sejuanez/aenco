// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectTecnico").select2().change(function (e) {
        var usuario = $(".selectTecnico").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeTecnico(usuario, nombreUsuario);
    });

    $('.noSigno').keydown(function (e) {
        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        codigo: "",
        nombre: "",

        fechaInicial: "",
        fechaFinal: "",

        orden: "",
        tipoOrden: 1,

        selectTecnico: [],
        valorTEC: "",

        valorActividad: "",
        selectActividades: [],

        consecutivo: "",
        reserva: "",

        busqueda: "",

        material: "",
        cantidad: "",
        cantidad_aprobada: "",


        tablaPedidos: [],

        tablaDetalle: [],

        itemSeleccionado: "",

        noResultados: false,

    },


    methods: {

        loadTecnico: function () {
            var app = this;
            $.get('./request/getTecnico.php', function (data) {
                app.selectTecnico = JSON.parse(data);
            });
        },

        onchangeTecnico: function (cod, nombre) {
            var app = this;
            //console.log(cod)
            app.valorTEC = cod;
        },

        verDetalle: function (dato) {


            $.post('./request/getDetalle.php', {
                numero: dato.numero
            }, function (data) {
                console.log(data)
                app.tablaDetalle = JSON.parse(data);
                $('#modalDetalle').modal('show');

            });


        },

        aprobarCantidad: function (dato, index, opcion) {

            var app = this;

            if (opcion == 'aprueba') {
                app.tablaDetalle[index]['cantidad_aprobada'] = app.tablaDetalle[index]['cantidad'];
            } else if (opcion == 'desaprueba') {
                app.tablaDetalle[index]['cantidad_aprobada'] = 0;

            } else if (opcion == 'apruebaTodo') {

                for (detalle in app.tablaDetalle) {
                    app.tablaDetalle[detalle]['cantidad_aprobada'] = app.tablaDetalle[detalle]['cantidad'];
                }

            } else if (opcion == 'desapruebaTodo') {

                for (detalle in app.tablaDetalle) {
                    app.tablaDetalle[detalle]['cantidad_aprobada'] = 0;
                }

            }

        },

        editarDetalle: function (dato, index) {

            var app = this;

            app.material = dato.material;
            app.cantidad = dato.cantidad;
            app.cantidad_aprobada = dato.cantidad_aprobada;
            app.itemSeleccionado = index;


            $('#modalEditaDetalle').modal('show');


        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        buscar: function () {
            var app = this;

            if ((app.fechaInicial != "" &&
                app.fechaFinal != "") ||
                app.orden != "" ||
                app.valorTEC != ""

            ) {
                $.post('./request/getPedidoMaterial.php', {
                    fechaInicial: app.fechaInicial,
                    fechaFinal: app.fechaFinal,
                    tipoOrden: app.tipoOrden,
                    orden: app.orden.toUpperCase(),
                    tecnico: app.valorTEC
                }, function (data) {


                    try {
                        var datos = jQuery.parseJSON(data);
                        app.tablaPedidos = datos;

                    } catch (e) {
                        console.log(data)
                        app.tablaPedidos = [];
                    }

                });
            } else {

                alertify.warning('Por favor selecciona rango de fechas, ingresa una orden o selecciona un tecnico')

            }

        },

        actualizarDetalle: function () {

            var app = this;

            if (app.cantidad_aprobada < 0) {

                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                app.tablaDetalle[app.itemSeleccionado]['cantidad_aprobada'] = app.cantidad_aprobada;

                app.resetModalCantidad();

            }


        },

        resetCampos: function () {

            app = this;

            app.codigo = "";
            app.nombre = "";
            app.valorActividad = "";
            $("#selectActividad").val("").trigger('change')

        },

        resetModalDetalle: function () {
            var app = this;

            app.consecutivo = "";
            app.reserva = "";
            $('#modalDetalle').modal('hide');
        },

        resetModalCantidad: function () {
            $('#modalEditaDetalle').modal('hide');
        },

        rechazar: function () {

            app.modificar('RECHAZADO')

        },

        aprobar: function () {

            var app = this;

            if (app.consecutivo == "" ||
                app.reserva == "") {

                alertify.error('Por favor in gresa elconsecutivo y la reserva');

            } else {

                var contador = 0;
                for (item in app.tablaDetalle) {
                    if (app.tablaDetalle[item]['cantidad_aprobada'] > 0) {
                        contador++;
                    }
                }

                if (contador <= 0) {
                    alertify.warning('Para aprobar un pedido se necesitan materiales con cantidad aprobada mayor a 0')
                } else {

                    app.modificar('APROBAR')

                }
            }
        },

        modificar: function (opcion) {

            $.ajax({
                url: './request/modificarPedido.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: {
                    data: app.tablaDetalle,
                    opcion: opcion,
                    consecutivo: app.consecutivo,
                    reserva: app.reserva,
                },

            }).done(function (data) {

                var respuesta = JSON.parse(data);

                console.log(respuesta)

                if (respuesta[0]['array']['opcion'] == 'APROBAR') {
                    alertify.success('Pedido Aprobado')
                    app.resetModalDetalle();
                    app.buscar()
                } else {
                    alertify.error('Pedido Rechazado')
                    app.resetModalDetalle();
                    app.buscar()
                }


            }).fail(function (data) {
                //console.log(data)
            });

        }

    },


    watch: {
        fechaFinal: function () {

            var f1 = this.fechaInicial.split("-");
            var fi = new Date(parseInt(f1[0]), parseInt(f1[1] - 1), parseInt(f1[2]));
            fi.setHours(0, 0, 0, 0);

            var f2 = this.fechaFinal.split("-");
            var ff = new Date(parseInt(f2[0]), parseInt(f2[1] - 1), parseInt(f2[2]));
            ff.setHours(0, 0, 0, 0);


            if (fi.getTime() > ff.getTime()) {
                alertify.error("No puede ingresar una fecha mayor a la inicial");
                this.fechaFinal = "";
            }

        }
    },

    mounted() {

        this.loadTecnico();

    },

});
