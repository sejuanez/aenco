SET TERM ^ ;

create or alter procedure PEDIDO_MATERIAL_DETALLE_MODIF (
    ENUMERO integer,
    EMATERIAL varchar(15),
    ECANTIDAD integer)
as
begin
  update pedido_materiales_detalle pm
  set pm.PMD_CANT_APROBADA = :ecantidad
  where pm.PMD_NUMERO = :enumero and
        pm.PMD_CODMATER = :ematerial;
end^

SET TERM ; ^

/* Following GRANT statetements are generated automatically */

GRANT SELECT,UPDATE ON PEDIDO_MATERIALES_DETALLE TO PROCEDURE PEDIDO_MATERIAL_DETALLE_MODIF;

/* Existing privileges on this procedure */

GRANT EXECUTE ON PROCEDURE PEDIDO_MATERIAL_DETALLE_MODIF TO SYSDBA;
