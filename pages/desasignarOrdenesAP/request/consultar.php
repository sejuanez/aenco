<?php
	session_start();
  	if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

		
	
	include("../../../init/gestion.php");
	
	$dpto = $_POST["dpto"];
	$mun = $_POST["mun"];
	$cgto = $_POST["cgto"];
	$barrio = $_POST["barrio"];

		
		

	$consulta = "SELECT CAST('N' AS VARCHAR(2)) SELECCIONADO, OA_NUMERO OT_NUMERO, OA_CEDULA CEDULA, OA_APELLIDO1 APELLIDO1, OA_APELLIDO2 APELLIDO2, OA_NOMBRE1 NOMBRE1, OA_NOMBRE2 NOMBRE2, d.de_nombre DPTO, m.mu_nombre MUNICIPIO, C.co_nombre CTO, b.ba_nombarrio BARRIO, OA_DIRECCION DIRECCION, OA_PTO_REF REFERENCIA, OA_TELEFONOS TELEFONOS, OA_FECHA_REPORTE FECHA_REPORTE, OA_HORA_REPORTE HORA_REPORTE, t.to_descripcion TIPO_ORDEN, OA_ESTADO_LUM ESTADO_LUM, OA_SERIE SERIE, OA_POSTE POSTE, OA_OBS OBSERVACION, OA_NIC NIC, OA_FECHA_GENERA FECHA_GENERA, OA_HORA_GENERA HORA_GENERA, OA_USUSIS_GENERA USUARIO_SISEMA, OA_TECNICO TECNICO, OA_SOL_EXTERNA EXTERNA, O.oa_fecha_asigna FECHA_ASIGNA,  O.oa_ususis_asigna USUASIG, tec.te_nombres NOMBRESTEC  FROM ot_ap o left join departamentos d on d.de_codigo=o.oa_dpto left join municipios m on m.mu_depto=o.oa_dpto and m.mu_codigomun=o.oa_mpio left join corregimientos c on c.co_depto=o.oa_dpto and c.co_municipio = o.oa_mpio and c.co_codcorregimiento=o.oa_corregimiento left join barrios b on b.ba_codbarrio=o.oa_barrio and b.ba_depto=o.oa_dpto and b.ba_mpio=o.oa_mpio and b.ba_sector=o.oa_corregimiento left join tipo_orden_servicio t on t.to_codigo=o.oa_tos left join tecnicos tec on tec.te_codigo=o.oa_tecnico where ((O.oa_tecnico is not null or O.oa_tecnico!='') and (O.oa_acta is null or (O.oa_acta='')))";

	if($dpto!='null'&&$dpto!=''){
		$consulta.=" and  o.oa_dpto='".$dpto."'";
		
	}

	if($mun!='null'&&$mun!=''){
		$consulta.=" and  o.oa_mpio='".$mun."'";
		
	}
	if($cgto!='null'&&$cgto!=''){
		$consulta.=" and  o.oa_corregimiento='".$cgto."'";
		
	}
	if($barrio!='null'&&$barrio!=''){
		$consulta.=" and o.oa_barrio='".$barrio."'";
		
	}

	
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

		
	while($fila = ibase_fetch_row($result)){
		
		$row_array['seleccionado'] = utf8_encode($fila[0]);
		$row_array['ot_numero'] = utf8_encode($fila[1]);
		$row_array['cedula'] = utf8_encode($fila[2]);
		$row_array['apellido1'] = utf8_encode($fila[3]);
		$row_array['apellido2'] = utf8_encode($fila[4]);
		$row_array['nombre1'] = utf8_encode($fila[5]);
		$row_array['nombre2'] = utf8_encode($fila[6]);
		$row_array['dpto'] = utf8_encode($fila[7]);
		$row_array['mun'] = utf8_encode($fila[8]);
		$row_array['cgto'] = utf8_encode($fila[9]);
		$row_array['barrio'] = utf8_encode($fila[10]);
		$row_array['direccion'] = utf8_encode($fila[11]);
		$row_array['ptoref'] = utf8_encode($fila[12]);
		$row_array['tels'] = utf8_encode($fila[13]);
		$row_array['fechavisita'] = utf8_encode($fila[14]);
		$row_array['horavisita'] = utf8_encode($fila[15]);
		$row_array['tipoorden'] = utf8_encode($fila[16]);

		$row_array['estadoluminaria'] = utf8_encode($fila[17]);
		$row_array['serie'] = utf8_encode($fila[18]);
		$row_array['poste'] = utf8_encode($fila[19]);

		$row_array['obs'] = utf8_encode($fila[20]);

		$row_array['nic'] = utf8_encode($fila[21]);
		
		$row_array['fechagen'] = utf8_encode($fila[22]);
		$row_array['horagen'] = utf8_encode($fila[23]);
		$row_array['usuario'] = utf8_encode($fila[24]);
		$row_array['tecnico'] = utf8_encode($fila[25]);
		$row_array['externa'] = utf8_encode($fila[26]);
		$row_array['fechaasig'] = utf8_encode($fila[27]);
		$row_array['usuasig'] = utf8_encode($fila[28]);
		$row_array['nombrestec'] = utf8_encode($fila[29]);
				
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>