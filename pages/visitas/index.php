<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Visitas</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->

	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	
	<script type="text/javascript">
		$(function(){
			
			//var externa = "SI";
			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});
			
			var date= new Date();

			var h= date.getHours();
			var m= date.getMinutes();
			var s= date.getSeconds();

			document.getElementById('hora').value = h+':'+m+':'+s;
			
			$("#guardando").hide();


			function limpiarFormulario(){
				
				$("input[type=text]").val("");
				//$("#obs").val("");
			};
			
			function validarFormulario(){
				if($("#cedula").val()!="" && $("#apellido1").val()!="" && $("#nombre1").val()!="" && $("#departamento").val()!="" && $("#municipio").val()!="" && $("#corregimiento").val()!="" && $("#barrio").val()!="" && $("#direccion").val()!="" && $("#ptoref").val()!="" && $("#fecha").val()!="" && $("#hora").val()!="" && $("#sexo").val()!="" && $("#cel1").val()!=""){
					return true;
				}else{
					return false;
				}
			};//fin de la funcion validar formulario

			function setBarrio(Dpto, Mun, Cgto){
				//console.log("funcion setBarrio invocada");
					$.ajax({
						url:'request/getBarrios.php',
		                type:'POST',
		                dataType:'json',
		                data:{dpto:Dpto, mun:Mun, cgto:Cgto}
		                
					}).done(function(repuesta){
						if(repuesta.length>0){
							var options="";
							
							for(var i=0;i<repuesta.length;i++){
								options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
							}
							
							$("#barrio").html(options);
						}
						
					});


			};// fin de la funcion setBarrio

			function setCorregimiento(Dpto, Mun){
					$.ajax({
						url:'request/getCorregimientos.php',
		                type:'POST',
		                dataType:'json',
		                data:{dpto:Dpto, mun:Mun}
		                
					}).done(function(repuesta){
						if(repuesta.length>0){
							var options="";
							
							for(var i=0;i<repuesta.length;i++){
								options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
							}
							
							$("#corregimiento").html(options);
							setBarrio(Dpto, Mun, $("#corregimiento").val());
						}
						
					});


			};// fin de la funcion setCorregimiento

			function setMunicipio(Dpto){
					$.ajax({
						url:'request/getMunicipios.php',
		                type:'POST',
		                dataType:'json',
		                data:{dpto:Dpto}
		                
					}).done(function(repuesta){
						if(repuesta.length>0){
							var options="";
							
							for(var i=0;i<repuesta.length;i++){
								options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
							}
							
							$("#municipio").html(options);
							setCorregimiento(Dpto,$("#municipio").val());
						}
						
					});


			};// fin de la funcion setMunicipio


			//ajax que carga la liista de departamentos
			$.ajax({
					url:'request/getDepartamentos.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					/*if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#departamento").html(options);
						setMunicipio($("#departamento").val());
					}*/
					if(repuesta.length>0){
						var optionsDpto="";
						var optionsMun="";
						
						for(var i=0;i<repuesta.length;i++){
							optionsDpto+="<option value='"+repuesta[i].codigoDpto+"'>"+repuesta[i].nombreDpto+"</option>";
							optionsMun+="<option value='"+repuesta[i].codigoMun+"'>"+repuesta[i].nombreMun+"</option>";
						}
						
						$("#departamento").html(optionsDpto);
						//$("#municipio").html(optionsMun);
						setMunicipio($("#departamento").val());
						//setCorregimiento($("#departamento").val(),$("#municipio").val());
					}
					
				});// fin del ajax departamentos

				//listener del evento Change del checkbox externa

				/*$("#sol_externa").on("change",function(){
					if($("#sol_externa").prop("checked"))
							externa = "SI";
					else
							externa = "NO";

				});*/

			//Listeners de el evento Change de las listas departamentos
			$("#departamento").on("change",function(){
					$("#municipio").html("");
					$("#corregimiento").html("");
					$("#barrio").html("");
					setMunicipio($("#departamento").val());
				});	
			//fin de Listeners de el evento Change de la lista departamentos

			//Listeners de el evento Change de las listas municipios
			$("#municipio").on("change",function(){
					$("#corregimiento").html("");
					$("#barrio").html("");
					setCorregimiento($("#departamento").val(),$("#municipio").val());
				});	
			//fin de Listeners de el evento Change de la lista municipio

			//Listeners de el evento Change de las listas corregimiento
			$("#corregimiento").on("change",function(){
					$("#barrio").html("");
					setBarrio($("#departamento").val(),$("#municipio").val(),$("#corregimiento").val());
				});	
			//fin de Listeners de el evento Change de la lista corregimiento


		//ajax que carga la lista de tipo de ordenes
			$.ajax({
					url:'request/getTipoOrdenes.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#tos").html(options);
						
					}
					
				});// fin del ajax tipo ordenes


		//ajax que carga la lista de estado luminarias
			/*$.ajax({
					url:'php/getEstadoLuminaria.php',
	                type:'POST',
	                dataType:'json'
	                
				}).done(function(repuesta){
					if(repuesta.length>0){
						var options="";
						
						for(var i=0;i<repuesta.length;i++){
							options+="<option value='"+repuesta[i].codigo+"'>"+repuesta[i].nombre+"</option>";
						}
						
						$("#estadolum").html(options);
						
					}
					
				});*/// fin del ajax estado luminarias

			
			//listener del evento focusout del ccuadro de texto NIC
			
			/*$("#nic").on("focusout",function(){
					
					//ajax que carga Nombre y Direccion a partir del NIC digitado
					$.ajax({
							url:'php/getNombreDir.php',
			                type:'POST',
			                dataType:'json',
			                data:{codigo:$("#nic").val()}
			                
						}).done(function(repuesta){
							if(repuesta.length>0){
								$("#nombre").val(repuesta[0].nombre);
								$("#dir").val(repuesta[0].direccion);
							}else{
								$("#nombre").val("");
								$("#dir").val("");
							}
							
						});// fin del ajax getNombreDir
			});*/// fin listener del evento focusout del ccuadro de texto NIC

			//listener del evento click del boton GUARDAR

			$("#guardar").click(function(e){
				e.preventDefault();
				$("#guardar").hide();
				$("#cancelar").hide();

				if(validarFormulario()){
					$("#guardando").show();
					$.ajax({//ajax guardar
							url:'request/guardar.php',
			                type:'POST',
			                dataType:'json',
			                data:{
			                	cedula:+$("#cedula").val(),
			                	apellido1:$("#apellido1").val().toUpperCase(),
			                	apellido2:$("#apellido2").val().toUpperCase(),
			                	nombre1:$("#nombre1").val().toUpperCase(),
			                	nombre2:$("#nombre2").val().toUpperCase(),
			                	departamento:$("#departamento").val(),
			                	municipio:$("#municipio").val(),
			                	corregimiento:$("#corregimiento").val(),
			                	barrio:$("#barrio").val(),
			                	direccion:$("#direccion").val().toUpperCase(),
			                	ptoref:$("#ptoref").val().toUpperCase(),
			                	telefono:$("#telefono").val(),
			                	fecha:$("#fecha").val(),
			                	hora:$("#hora").val(),
			                	tos:$("#tos").val().toUpperCase(),
			                	sexo:$("#sexo").val(),
			                	email:$("#email").val(),
			                	cel1:$("#cel1").val().toUpperCase(),
			                	cel2:$("#cel2").val().toUpperCase(),
			                	obs:$("#obs").val().toUpperCase()
			                }
			                
						}).done(function(repuesta){
							if(repuesta.length>0){
								
								//alert("La orden  "+repuesta[0].consecutivo+" se guardó correctamente");
								$("#alert p").html("La orden  "+repuesta[0].consecutivo+" se guardó correctamente.");
								$( "#alert" ).dialog( "open" );
								limpiarFormulario();
							}else{
								//alert("Error al guardar");
								$("#alert p").html("Error al guardar.");
								$( "#alert" ).dialog( "open" );
								
							}

							$("#guardar").show();
							$("#cancelar").show();
							$("#guardando").hide();
						});

				}else{
					//alert("Debes diligenciar todos los campos marcados con asterisco (*)");
					$("#alert p").html("Debes diligenciar todos los campos marcados con asterisco (*).");
					$( "#alert" ).dialog( "open" );
					
					$("#guardar").show();
					$("#cancelar").show();
				}
				
			});// fin del evento clic de guardar
			
			//listener del evento clic del boton cancelar
			$("#cancelar").click(function(e){
				e.preventDefault();
				limpiarFormulario();
			});//fin del evento clic del boton cancelar

			$("#guardando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<header>
		<h3>Generación de visitas a clientes</h3>
		<nav>
			<ul id="menu">
				
				<li id="guardar"><a href="" ><span class="ion-android-done"></span><h6>Guardar</h6></a></li>
				<li id="guardando"><a href="" ><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>
				<li id="cancelar"><a href="" ><span class="ion-android-close"></span><h6>Cancelar</h6></a></li>
					
			</ul>
		</nav>
		
	</header>
	

	<section>
		<form>
			<aside>
				<div>
					<fieldset>
						<!--<legend>Solicitante</legend>-->

						<!--<div>
							<input type="checkbox" id="sol_externa" name="sol_externa" checked ><span>Sol. Externa</span>
						</div>-->

						<div>
							<h5>Cédula *</h5>
							<input type="text" id="cedula" name="cedula" autofocus>
						</div>

						<div>
							<h5>1er. Apellido *</h5>
							<input type="text" id="apellido1" name="apellido1">
						</div>

						<div>
							<h5>2do. Apellido</h5>
							<input type="text" id="apellido2" name="apellido2">
						</div>

						<div>
							<h5>1er. Nombre *</h5>
							<input type="text" id="nombre1" name="nombre1">
						</div>

						<div>
							<h5>2do. Nombre</h5>
							<input type="text" id="nombre2" name="nombre2">
						</div>

						<div>
							<h5>Departamento *</h5>
							<select id="departamento" name="departamento" ></select>
						</div>

						<div>
							<h5>Municipio *</h5>
							<select id="municipio" name="municipio" ></select>
						</div>

						<div>
							<h5>Corregimiento *</h5>
							<select id="corregimiento" name="corregimiento"></select>
						</div>

						<div>
							<h5>Barrio/vda *</h5>
							<select id="barrio" name="barrio"></select>
						</div>

						<div>
							<h5>Dirección *</h5>
							<input type="text" id="direccion" name="direccion">
						</div>

						<div>
							<h5>Pto. Ref *</h5>
							<input type="text" id="ptoref" name="ptoref">
						</div>

						<div>
							<h5>Teléfono(s)</h5>
							<input type="text" id="telefono" name="telefono">
						</div>

					</fieldset>
				</div>
			</aside>



		
			<aside>
				<div>
					<fieldset>
						<!--<legend>Reporte</legend>-->
						
						<div>
							<h5>Fecha visita *</h5>
							<input type="date" id="fecha" name="fecha" value="<?php echo date("Y-m-d"); ?>">
						</div>

						<div>
							<h5>Hora visita *</h5>
							<input type="time" id="hora" name="hora" value="12:30">
						</div>

						<div>
							<h5>T.O.S</h5>
							<select id="tos" name="tos"></select>
						</div>

						<div>
							<h5>Sexo *</h5>
							<select id="sexo" name="sexo">
								<option value="M">Hombre</option>
								<option value="F">Mujer</option>
							</select>
						</div>

						<!--<div>
							<h5>Estado lum.</h5>
							<select id="estadolum" name="estadolum"></select>
						</div>-->

						<div>
							<h5>E-mail</h5>
							<input type="text" id="email" name="email">
						</div>

						<!--<div>
							<h5>Serie lumin.</h5>
							<input type="text" id="serie" name="serie">
						</div>-->

						<div>
							<h5>Celular 1 *</h5>
							<input type="text" id="cel1" name="cel1">
						</div>

						<div>
							<h5>Celular 2</h5>
							<input type="text" id="cel2" name="cel2">
						</div>

						<div id="divobs">
							<h5 id="labelobs">Obs.</h5>
							<textarea id="obs" name="obs" rows='4'></textarea>
						</div>

						<!--<div>
							<h5>Nro. Poste</h5>
							<input type="text" id="poste" name="poste">
						</div>


						<div id="divobs">
							<h5 id="labelobs">Obs.</h5>
							<textarea id="obs" name="obs"></textarea>
						</div>

						<div>
							<h5>NIC</h5>
							<input type="text" id="nic" name="nic">
						</div>
						
						<div>
							<h5>Nombre</h5>
							<input type="text" id="nombre" name="nombre">
						</div>

						<div>
							<h5>Dir.</h5>
							<input type="text" id="dir" name="dir">
						</div>-->
					</fieldset>
				</div>
			</aside>
		</form>	
	</section>

	<footer><p><span class="ion-ios-information-outline"></span><em>Los campos del formulario marcados con * son obligatorios.</em></p></footer>

</body>
</html>