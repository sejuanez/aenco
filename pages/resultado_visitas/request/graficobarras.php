<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	
	
	$consulta = "SELECT distinct coalesce(rh.rh_apellido1,'')||' '||coalesce(rh.rh_nombre1, '') Funcionario, count(o.ot_tecnico) Asignadas, count(ot.ot_tecnico) Ejecutadas from ot_creditos o  left join tecnicos t on t.te_codigo=o.ot_tecnico  left join datos_rrhh rh on rh.rh_cedula=t.te_cedula  left join ot_creditos ot on ot.ot_numero=o.ot_numero and ot.ot_ejecutada='S' and ot.ot_fechaej between '".$fechaini."' and '".$fechafin."' where o.ot_fecha_asigna between '".$fechaini."' and '".$fechafin."' group by coalesce(rh.rh_apellido1,'')||' '||coalesce(rh.rh_nombre1, '')";
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['funcionario'] = utf8_encode($fila[0]);
		$row_array['asignadas'] = utf8_encode($fila[1]);		
		$row_array['ejecutadas'] = utf8_encode($fila[2]);

					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>