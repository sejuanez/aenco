<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Resultado visitas</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});




		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Visitas'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            valueSuffix: ' visitas'
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};






			$("#consultando").hide();
			$("#contenido").hide();


			$("#div-asignadas").hide();
			$("#div-ejecutadas").hide();
			$("#div-pendientes").hide();




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#contenido").hide();
				$("#consultando").show();
				
				$("#div-asignadas").fadeOut(500);
				$("#div-ejecutadas").fadeOut(500);
				$("#div-pendientes").fadeOut(500);

				//$("#div-tabla").html("<h4 style='text-align:center; color:#999; font-weight:normal'>Cargando...</h4>");


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



				$.ajax({
					url:'request/graficobarras.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						//var hc_column=[];
						var funcionarios=[];
						var asignadas=[];
						var ejecutadas=[];
						var totalAsignadas=0;
						var totalEjecutadas=0;

						var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];
						var filas="";

						for (var i=0; i<repuesta.length; i++) {
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].funcionario+"</td><td>"+repuesta[i].asignadas+"</td>"+"<td>"+repuesta[i].ejecutadas+"</td></tr>";
                               
                               funcionarios[i]=repuesta[i].funcionario;
                               asignadas[i]=+repuesta[i].asignadas;
                               ejecutadas[i]=+repuesta[i].ejecutadas;
                               totalAsignadas=totalAsignadas + (+repuesta[i].asignadas);
                               totalEjecutadas=totalEjecutadas + (+repuesta[i].ejecutadas);
                               
                               //hc_column.push([repuesta[i].funcionario,+repuesta[i].asignadas]);
						}
						
						$("table tbody").html(filas);

						var series=[
										{
											name:'Asignadas', 
											data:asignadas,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				}
	                          			},
										{
											name:'Ejecutadas', 
											data:ejecutadas,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				}
										}
									];
						
						$("#total-asignadas").html(totalAsignadas);
						$("#div-asignadas").fadeIn(500);
						
						$("#total-ejecutadas").html(totalEjecutadas);
						$("#div-ejecutadas").fadeIn(500);

						$("#total-pendientes").html(totalAsignadas - totalEjecutadas);
						$("#div-pendientes").fadeIn(500);

						$("#contenido").show();

						dibujarGraficoBarras("grafico-barras",funcionarios,series,colors);

						//drawColumnChart(hc_column,"grafico-barras",colors);//DIBUJAR EL GRAFICO
                        //$("g.highcharts-data-labels>g[opacity=0]").attr("opacity",1);

					}
					else{
												
						$("#total-asignadas").html("");						
						
						$("#total-ejecutadas").html("");						

						$("#total-pendientes").html("");

						$("table tbody").html("");

						$("#alert").dialog("open");
								
					}



					$("#consultando").hide();
					$("#consultar").show();
				});
			});
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});

			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay visitas para mostrar.</p>
	</div>

	<header>
		<h3>Resultado de visitas</h3>
		<nav>
			<ul id="menu">
				
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				
			</form>
		
		</div>
		
		<div id="div-subtotales">

			<div id="div-asignadas">				
				<h4 id="total-asignadas">1</h4>
				<h5>Total órdenes</h5>
			</div>

			<div id="div-ejecutadas">				
				<h4 id="total-ejecutadas">2</h4>
				<h5>Total ejecutadas</h5>
			</div>

			<div id="div-pendientes">				
				<h4 id="total-pendientes">3</h4>
				<h5>Total pendientes</h5>
			</div>
		</div>
		
	</div>



	<div id='contenido'>

		<div id='div-funcionarios'>
			<table>
				<thead>
					<tr class='cabecera'><td>Funcionario</td><td>Asig.</td><td>Ejec.</td></tr>
				</thead>

				<tbody>
					
				</tbody>
			</table>
		</div>


		<div id='div-graficos' >

			<div id="grafico-barras" ></div>

		</div>

	</div>

	

</body>
</html>