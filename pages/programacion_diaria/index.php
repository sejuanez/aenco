<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Programación diaria</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	

	<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>

  	<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>	

	
	<script type="text/javascript">
		
		$(function(){

			


			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						//$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});


			$("#tabs").tabs({
				show: { effect: "slide", duration: 200 },
				active:0,
				activate: function( event, ui ) {

					var ancho = +$("#div-grafico-tecnico").width();
					var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);
				}
			});
			

			$(window).resize(function() {
			    	var ancho = +$("#div-grafico-tecnico").width();
			    	var alto = +$("#div-grafico-tecnico").height();
					$("#grafico-tecnico").highcharts().setSize(ancho,alto, false);

					ancho = +$("#div-grafico-municipio").width();
					alto = +$("#div-grafico-municipio").height();
					$("#grafico-municipio").highcharts().setSize(ancho,alto, false);
			});
		
			
			function dibujarGraficoBarras(element, categorias, series, colores){
				$('#'+element).highcharts({
			        
			        chart: {
			            type: 'column',
			            backgroundColor: "#fafafa"
			        },

			        colors:colores,

			        title: {
			            text:null /*'Historic World Population by Region'*/
			        },
			        /*subtitle: {
			            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			        },*/
			        xAxis: {
			            categories: categorias/*['Africa', 'America', 'Asia', 'Europe', 'Oceania']*/,
			            title: {
			                text: null
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Programaciones'/*,
			                align: 'high'*/
			            },
			            labels: {
			                overflow: 'justify'
			            }
			        },
			        tooltip: {
			            valueSuffix: ' programaciones'
			        },
			        plotOptions: {
			            /*bar: {
			                dataLabels:{
								enabled: true,
								rotation: 90,
								x:5,
								y:-6
							}
			            }*/
						column:{
							dataLabels:{
								enabled: true,
								x:0,
								y:-12,
								//borderRadius: 5,
			                    //backgroundColor: 'rgba(252, 255, 197, 0.7)',
			                    /*borderWidth: 1,
			                    borderColor: '#AAA',*/
			                    padding:20
							}
						}
			        },
			        legend: {
			            layout: 'horizontal',
			            align: 'right',
			            verticalAlign: 'top',
			            x: 0,
			            y: 0,
			            floating: false,
			            borderWidth: 0,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: false
			        },
			        credits: {
			            enabled: false
			        },
			        series: series/*[{
			            name: 'Year 1800',
			            data: [107, 31, 635, 203, 2]
			        }, {
			            name: 'Year 1900',
			            data: [133, 156, 947, 408, 6]
			        }, {
			            name: 'Year 2012',
			            data: [1052, 954, 4250, 740, 38]
			        }]*/
			    });
			};






			$("#exportar").hide();
			$("#consultando").hide();
			$("#contenido").hide();


			/*$("#div-asignadas").hide();
			$("#div-ejecutadas").hide();
			$("#div-pendientes").hide();*/




			$("#txtFechaFin").datepicker({
				minDate: new Date(),
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaFin").datepicker("setDate", new Date());



			
			$("#txtFechaIni").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat:"dd/mm/yy",
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			});

			$("#txtFechaIni").datepicker("setDate", new Date());




			$("#txtFechaIni").change(function(){
				
				$("#txtFechaFin").datepicker("option", "minDate",$("#txtFechaIni").datepicker("getDate"));
			});

			



			//listener del evento clic del boton consultar
			$('#consultar').click(function(e){

				e.preventDefault();
				$(this).hide();
				$("#exportar").hide();
				$("#contenido").hide();
				$("#consultando").show();
				
				/*$("#div-asignadas").fadeOut(500);
				$("#div-ejecutadas").fadeOut(500);
				$("#div-pendientes").fadeOut(500);*/

				//$("#div-tabla").html("<h4 style='text-align:center; color:#999; font-weight:normal'>Cargando...</h4>");


				var diaIni=+($('#txtFechaIni').datepicker('getDate').getDate());
				if(diaIni<10)
					diaIni="0"+diaIni;

				var mesIni= +($('#txtFechaIni').datepicker('getDate').getMonth() + 1);
				if(mesIni<10)
					mesIni="0"+mesIni;

				var diaFin=+($('#txtFechaFin').datepicker('getDate').getDate());
				if(diaFin<10)
					diaFin="0"+diaFin;

				var mesFin= +($('#txtFechaFin').datepicker('getDate').getMonth() + 1);
				if(mesFin<10)
					mesFin="0"+mesFin;



				//var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+($('#txtFechaIni').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaIni').datepicker('getDate').getDate();
				var fechaIni=$('#txtFechaIni').datepicker('getDate').getFullYear()+"-"+mesIni+"-"+diaIni;
		        //var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+($('#txtFechaFin').datepicker('getDate').getMonth() + 1)+"-"+$('#txtFechaFin').datepicker('getDate').getDate();
		        var fechaFin=$('#txtFechaFin').datepicker('getDate').getFullYear()+"-"+mesFin+"-"+diaFin;



		        // ajax GENERAL

				$.ajax({
					url:'request/general.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

												
						var filas="";

						for (var i=0; i<repuesta.length; i++) {
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].solicitud+"</td><td>"+repuesta[i].orden+"</td>"+"<td>"+repuesta[i].proceso+"</td><td>"+repuesta[i].fechaprogramada+"</td><td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].departamento+"</td><td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].localidad+"</td><td>"+repuesta[i].barrio+"</td></tr>";
						}
						
						$("#tabla-general tbody").html(filas);

						
						$("#contenido").show();

						$("#exportar a").attr("href","request/exportarExcel.php?fechaIni="+fechaIni+"&fechaFin="+fechaFin);
						$("#exportar").show();


						
					}
					else{												
						
						$("#tabla-general tbody").html("");
						$("#exportar a").attr("href","");								
					}

					$("#consultando").hide();
					$("#consultar").show();
				});






				//ajax POR TECNICOS
				$.ajax({
					url:'request/porTecnico.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var tecnicos=[];						
						var cantidad=[];
						var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						var filas="";

						for (var i=0; i<repuesta.length; i++) {

							tecnicos[i]=repuesta[i].tecnico;
							cantidad[i]=+repuesta[i].cantidad;
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].tecnico+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						$("#tabla-tecnico tbody").html(filas);

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				}
	                          			}
									];

						
						$("#contenido").show();
						dibujarGraficoBarras("grafico-tecnico",tecnicos,series,colors);

						
					}
					else{												
						
						$("#tabla-tecnico tbody").html("");
						$("#grafico-tecnico").html("");
					}

					$("#consultando").hide();
					$("#consultar").show();
				});
				

				

				//ajax POR MUNICIPIOS
				$.ajax({
					url:'request/porMunicipio.php',
	                type:'POST',
	                dataType:'json',
	                data:{fechaini:fechaIni , fechafin:fechaFin}

				}).done(function(repuesta){

					
					if(repuesta.length>0){

						var municipios=[];						
						var cantidad=[];
						var colors=['#00034A','#0008C5','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000','#800080','#00FF7F','#FF6347','#FFFF00','#8A2BE2','#9932CC','#FF8C00','#FF1493','#ADFF2F','#FFD700','#008000','800000','#6B8E23','#FF4500','#FF0000'];

						var filas="";

						for (var i=0; i<repuesta.length; i++) {

							municipios[i]=repuesta[i].municipio;
							cantidad[i]=+repuesta[i].cantidad;
							
							filas+="<tr class='fila'>"+
                              "<td>"+repuesta[i].municipio+"</td><td>"+repuesta[i].cantidad+"</td></tr>";
						}
						
						$("#tabla-municipio tbody").html(filas);
						

						var series=[
										{
											name:'Cantidad', 
											data:cantidad,
											dataLabels: {
												enabled: true,
	                              				//rotation: -90,
	                              				color: '#000',
	                              				align: 'center',
	                             				// format: '${point.y}', // one decimal
				                              	/*formatter:function(){
				                                  return accounting.formatMoney(this.y, {symbol : '$', precision : 0,thousand : ','});
				                              	},*/
	                              				y: 10, // 10 pixels down from the top
				                                style: {
				                                  fontSize: '10px',
				                                  fontFamily: 'Verdana, sans-serif'
				                                }
	                          				}
	                          			}
									];

						
						$("#contenido").show();
						dibujarGraficoBarras("grafico-municipio",municipios,series,colors);
						

						
					}
					else{												
						
						$("#tabla-municipio tbody").html("");
						$("#grafico-municipio").html("");
					}

					$("#consultando").hide();
					$("#consultar").show();
				});


			});
			
			

			$("#consultando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
		<p>No hay programaciones para mostrar.</p>
	</div>
	

	<header>
		<h3>Programación diaria</h3>
		<nav>
			<ul id="menu">
				
				<li id="exportar"><a href="" ><span class="ion-ios-download-outline"></span><h6>exportar</h6></a></li>
				<li id="consultar"><a href="" ><span class="ion-ios-search-strong"></span><h6>consultar</h6></a></li>
				<li id="consultando"><a href="" ><span class="ion-load-d"></span><h6>consultando...</h6></a></li>				
					
			</ul>
		</nav>
		
	</header>
	
	


	<div id='subheader'>

		<div id="div-form">
			<form id="form">

				<div><label>Fecha inicial</label><input type="text" id="txtFechaIni" readOnly></div>
				<div><label>Fecha final</label><input type="text" id="txtFechaFin" readOnly></div>
				
			</form>
		
		</div>
		
	</div>



	<div id='contenido'>

		<div id='tabs'>

			<ul>
				<li><a href="#tab1">1. <span class='titulo-tab'>General</span></a></li>
				<li><a href="#tab2">2. <span class='titulo-tab'>Resumen por técnico</span></a></li>
				<li><a href="#tab3">3. <span class='titulo-tab'>Resumen por municipio</span></a></li>
			
			</ul>


			<div id='tab1'>
				<table id='tabla-general'>
					<thead>
						<tr class='cabecera'><td>Solicitud</td><td>Orden</td><td>Proceso</td><td>Fecha programada</td><td>Técnico</td><td>Departamento</td><td>Municipio</td><td>localidad</td><td>Barrio</td></tr>
					</thead>	

					<tbody>
						
					</tbody>
				</table>
			</div>


			<div id='tab2'>
				<div>
					<table id='tabla-tecnico'>
						<thead>
							<tr class='cabecera'><td>Técnico</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>

				<div id='div-grafico-tecnico'>
					<div id='grafico-tecnico'></div>
				</div>
			</div>


			<div id='tab3'>
				<div>
					<table id='tabla-municipio'>
						<thead>
							<tr class='cabecera'><td>Municipio</td><td>Cantidad</td></tr>
						</thead>	

						<tbody>
							
						</tbody>
					</table>
				</div>

				<div id='div-grafico-municipio'>
					<div id='grafico-municipio'></div>
				</div>
			</div>



		</div>

		<!--<div id='div-funcionarios'>
			<table>
				<thead>
					<tr class='cabecera'><td>Funcionario</td><td>Asig.</td><td>Ejec.</td></tr>
				</thead>

				<tbody>
					
				</tbody>
			</table>
		</div>


		<div id='div-graficos' >

			<div id="grafico-barras" ></div>

		</div>-->

	</div>

	

</body>
</html>