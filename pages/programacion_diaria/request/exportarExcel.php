<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
 	header("Content-Disposition: filename=programaciones_desde_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	include("../../../init/gestion.php");

	$fechaini = $_GET['fechaIni'];
	$fechafin = $_GET['fechaFin'];
		
	$consulta = "SELECT o.ot_solicitud Solicitud,
                               o.ot_orden Orden,
                               ca.c_nombre Proceso,
                               o.ot_fechaprogramada FechaProgramada,
                               T.te_nombres Tecnico,
                               D.de_nombre  Departamento,
                               m.mu_nombre  Municipio,
                               c.co_nombre  Localidad,
                               b.ba_nombarrio Barrio
                        from ot o
                            INNER join departamentos d on d.de_codigo=o.ot_cod_dpto
                            INNER join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio
                            INNER join corregimientos c on c.co_depto=o.ot_cod_dpto and c.co_municipio=o.ot_cod_mpio and c.co_codcorregimiento=o.ot_cod_localidad
                            INNER  JOIN BARRIOS b on b.ba_depto=o.ot_cod_dpto and b.ba_mpio=o.ot_cod_mpio and b.ba_sector=o.ot_cod_localidad and b.ba_codbarrio=o.ot_cod_barrio
                            INNER JOIN CAMPANAS CA ON CA.c_codigo=O.ot_proceso_campana
                            INNER JOIN TECNICOS T ON T.te_codigo=O.ot_tecnico
                        where o.ot_fechaprogramada between '".$fechaini."' AND '".$fechafin."'";

	//$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'>".
						"<td>Solicitud</td>".
						"<td>Orden</td>".
						"<td>Proceso</td>".
						"<td>Fecha programada</td>".
						"<td>Tecnico</td>".
						"<td>Departamento</td>".
						"<td>Municipio</td>".
						"<td>Localidad</td>".
						"<td>Barrio</td>".
					"</tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td>".
					"<td>".utf8_encode($fila[1])."</td>".
					"<td>".utf8_encode($fila[2])."</td>".
					"<td>".utf8_encode($fila[3])."</td>".
					"<td>".utf8_encode($fila[4])."</td>".
					"<td>".utf8_encode($fila[5])."</td>".
					"<td>".utf8_encode($fila[6])."</td>".
					"<td>".utf8_encode($fila[7])."</td>".
					"<td>".utf8_encode($fila[8])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>