<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }



	include("../../../init/gestion.php");

	$fechaini = $_POST['fechaini'];
	$fechafin = $_POST['fechafin'];
	
	
	$consulta = "SELECT o.ot_solicitud Solicitud,
                               o.ot_orden Orden,
                               ca.c_nombre Proceso,
                               o.ot_fechaprogramada FechaProgramada,
                               T.te_nombres Tecnico,
                               D.de_nombre  Departamento,
                               m.mu_nombre  Municipio,
                               c.co_nombre  Localidad,
                               b.ba_nombarrio Barrio
                        from ot o
                            INNER join departamentos d on d.de_codigo=o.ot_cod_dpto
                            INNER join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio
                            INNER join corregimientos c on c.co_depto=o.ot_cod_dpto and c.co_municipio=o.ot_cod_mpio and c.co_codcorregimiento=o.ot_cod_localidad
                            INNER  JOIN BARRIOS b on b.ba_depto=o.ot_cod_dpto and b.ba_mpio=o.ot_cod_mpio and b.ba_sector=o.ot_cod_localidad and b.ba_codbarrio=o.ot_cod_barrio
                            INNER JOIN CAMPANAS CA ON CA.c_codigo=O.ot_proceso_campana
                            INNER JOIN TECNICOS T ON T.te_codigo=O.ot_tecnico
                        where o.ot_fechaprogramada between '".$fechaini."' AND '".$fechafin."'";
	//echo $consulta;
	$return_arr = array();

	$result = ibase_query($conexion,$consulta);

	while($fila = ibase_fetch_row($result)){
		
		$row_array['solicitud'] = utf8_encode($fila[0]);
		$row_array['orden'] = utf8_encode($fila[1]);		
		$row_array['proceso'] = utf8_encode($fila[2]);
		$row_array['fechaprogramada'] = utf8_encode($fila[3]);
		$row_array['tecnico'] = utf8_encode($fila[4]);
		$row_array['departamento'] = utf8_encode($fila[5]);
		$row_array['municipio'] = utf8_encode($fila[6]);
		$row_array['localidad'] = utf8_encode($fila[7]);
		$row_array['barrio'] = utf8_encode($fila[8]);

					
		array_push($return_arr, $row_array);
	}

	echo json_encode($return_arr);

?>