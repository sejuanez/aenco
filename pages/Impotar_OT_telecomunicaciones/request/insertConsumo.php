<?php

define('MAX_SEGMENT_SIZE', 65535);
ini_set('max_execution_time', 0);
error_reporting(0);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../init/gestion.php");


$array = $_POST['array'];


$arrayTotales = array();
$arrayErrores = array();

$total = count($array) - 1;
$errores = 0;
$insertados = 0;

$trans = ibase_trans(IBASE_DEFAULT);
$flang = true;


for ($i = 1; $i < count($array); $i++) {


    $sql = "EXECUTE PROCEDURE OT_TELECOMUNICACIONES_CREA (
                                    '" . (($array[$i][0] == "") ? "" : $array[$i][0]) . "',
                                    '" . (($array[$i][1] == "") ? "" : $array[$i][1]) . "',
                                    '" . (($array[$i][2] == "") ? "" : $array[$i][2]) . "',
                                    '" . (($array[$i][3] == "") ? "" : utf8_decode($array[$i][3])) . "',
                                    '" . (($array[$i][4] == "") ? "" : utf8_decode($array[$i][4])) . "',
                                    '" . (($array[$i][5] == "") ? "" : utf8_decode($array[$i][5])) . "',
                                    '" . (($array[$i][6] == "") ? "" : utf8_decode($array[$i][6])) . "',
                                    '" . (($array[$i][7] == "") ? "" : $array[$i][7]) . "',
                                    '" . (($array[$i][8] == "") ? "" : utf8_decode($array[$i][8])) . "',
                                    '" . (($array[$i][9] == "") ? "" : $array[$i][9]) . "',
                                    '" . (($array[$i][10] == "") ? "" : $array[$i][10]) . "',
                                    '" . (($array[$i][11] == "") ? "" : $array[$i][11]) . "',
                                    '" . (($array[$i][12] == "") ? "" : utf8_decode($array[$i][12])) . "',
                                    '" . (($array[$i][13] == "") ? "" : utf8_decode($array[$i][13])) . "',
                                    '" . (($array[$i][14] == "") ? "" : utf8_decode($array[$i][14])) . "',
                                    '" . (($array[$i][15] == "") ? "" : utf8_decode($array[$i][15])) . "',
                                    '" . (($array[$i][16] == "") ? "" : utf8_decode($array[$i][16])) . "',
                                    '" . (($array[$i][17] == "") ? "" : utf8_decode($array[$i][17])) . "',
                                    '" . (($array[$i][18] == "") ? "" : utf8_decode($array[$i][18])) . "',
                                    '" . (($array[$i][19] == "") ? "" : utf8_decode($array[$i][19])) . "',
                                    '" . (($array[$i][20] == "") ? "" : utf8_decode($array[$i][20])) . "',
                                    '" . (($array[$i][21] == "") ? "" : utf8_decode($array[$i][21])) . "',
                                    '" . (($array[$i][22] == "") ? "" : utf8_decode($array[$i][22])) . "',
                                    '" . (($array[$i][23] == "") ? "" : utf8_decode($array[$i][23])) . "',
                                    '" . (($array[$i][24] == "") ? "" : utf8_decode($array[$i][24])) . "',
                                    '" . (($array[$i][25] == "") ? "" : utf8_decode($array[$i][25])) . "',
                                    '" . (($array[$i][26] == "") ? "" : utf8_decode($array[$i][26])) . "',
                                    '" . (($array[$i][27] == "") ? "" : utf8_decode($array[$i][27])) . "',
                                    '" . (($array[$i][28] == "") ? "" : utf8_decode($array[$i][28])) . "',
                                    '" . (($array[$i][29] == "") ? "" : utf8_decode($array[$i][29])) . "',
                                    '" . (($array[$i][30] == "") ? "" : utf8_decode($array[$i][30])) . "',
                                    '" . (($array[$i][31] == "") ? "" : utf8_decode($array[$i][31])) . "',
                                    '" . (($array[$i][32] == "") ? "" : utf8_decode($array[$i][32])) . "',
                                    '" . (($array[$i][33] == "") ? "" : utf8_decode($array[$i][33])) . "',
                                    '" . (($array[$i][34] == "") ? "" : utf8_decode($array[$i][34])) . "',
                                    '" . (($array[$i][35] == "") ? "" : utf8_decode($array[$i][35])) . "',
                                    '" . (($array[$i][36] == "") ? "" : utf8_decode($array[$i][36])) . "',
                                    '" . (($array[$i][37] == "") ? "" : utf8_decode($array[$i][37])) . "',
                                    '" . (($array[$i][38] == "") ? "" : utf8_decode($array[$i][38])) . "',
                                    '" . $_SESSION['user'] . "'
                                    )
                                   ";

    //echo $sql . '<br>';

    $result = ibase_query($trans, $sql);

    if ($result == 1) {
        $insertados++;
    } else {
        $flang = false;
        $errores++;
        $row_array['cuenta'] = $array[$i][0];
        $row_array['titular'] = $array[$i][3];
        $row_array['error'] = "" . str_replace('"', '', ibase_errmsg()) . "";
        array_push($arrayErrores, $row_array);

    }

}

$row_array2['total'] = $total;
$row_array2['errores'] = $errores;
$row_array2['insertados'] = $insertados;
array_push($arrayTotales, $row_array2);

$return = [
    "totales" => $arrayTotales,
    "errores" => $arrayErrores,
];

if (!$flang) {
    ibase_rollback($trans);
    echo json_encode($return);
} else {

    ibase_commit($trans);
    echo json_encode($return);

}






