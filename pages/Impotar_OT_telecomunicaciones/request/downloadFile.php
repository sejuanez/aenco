<?php
/**
 * Created by PhpStorm.
 * User: ServerConelec
 * Date: 05/04/2019
 * Time: 5:59
 */

$fileName = basename('modelo_cargue_visitas.xlsx');
$filePath = '../model/' . $fileName;
if (!empty($fileName) && file_exists($filePath)) {
    // Define headers
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$fileName");
    header("Content-Type: application/vnd.ms-excel");
    header("Content-Transfer-Encoding: binary");

    // Read the file
    readfile($filePath);
    exit;
} else {
    echo 'The file does not exist.';
}