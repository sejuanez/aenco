<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 59vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Importar Visitas


            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="download();">
					<i class="fa fa-download" aria-hidden="true"></i> Modelo
		    	</span>

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 75px;" @click="cargar();">
					<i class="fa fa-upload" aria-hidden="true"></i> Importar
		    	</span>


        </p>
    </header>

    <div v-if="ajax" class="loading">Loading&#8230;</div>
    <div class="container" style="padding-bottom: 5px; max-width: 95%;">
        <form enctype="multipart/form-data" class="formGuardar" v-show="false">
            <div class="col">
                <div class="form-group" id="inputFileContainer">
                </div>

                <div class="form-group" id="aDownloadContainer">
                    <a id='download' download href='request/downloadFile.php'></a>
                </div>
            </div>
        </form>

    </div>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                   aria-selected="true">Resumen</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false" v-text="textErrores">Errores</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th>Importados Correctamente</th>
                                    <th>Errores</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaConsumos.length == 0">
                                    <td class="text-center fondoGris" colspan="3"></td>
                                </tr>
                                <tr v-for="(dato, index) in tablaConsumos">
                                    <td v-text="dato.insertados"></td>
                                    <td v-text="dato.errores"></td>
                                    <td v-text="dato.total"></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>


            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th width="100">Valor</th>
                                    <th width="300">Titular</th>
                                    <th>Error</th>
                                </tr>
                                </thead>
                                <tbody id="errores">

                                <tr v-for="(dato, index) in arrayErrores">
                                    <td v-text="dato.cuenta" width="100"></td>
                                    <td v-text="dato.titular" width="300"></td>
                                    <td v-text="dato.error"></td>


                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>

        </div>

    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
