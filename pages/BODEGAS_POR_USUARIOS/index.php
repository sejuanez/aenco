<?php
    session_start();

    if (!$_SESSION['user']/* && !$_SESSION['permiso']*/) {//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
    }
?>

<!DOCTYPE html>
<html>

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <style type="text/css">


        .table-bordered td, .table-bordered th {
            border: 1px solid #b1b1b1;
        }

        .table thead th {
            vertical-align: bottom;

        }

        .success {
            background-color: #dff0d8;
        }

        .warning {
            background-color: #fcf8e3;
        }

        label {
            font-size: 0.8em;
        }

        .imgUsuario {
            max-height: 70px;
            min-height: 70px;
            max-width: 100%;
            margin-bottom: 10px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }

        .form-control:disabled,
        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        table {
            font-size: .8em;
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Bodega por Usuarios


        </p>
    </header>


    <div class="container">


        <div class="row">

            <div class="col-12 col-sm-12">

                <br>
                <div class="row">

                    <div class="col-12 col-sm-5">
                        <div class="form-group input-group-sm">
                            <label for="nomProveedor">Usuarios </label>

                            <select class="selectBDUsuario" id="selectBDUsuario" name="selectBDUsuario"
                                    v-model="valorBDUsuario">
                                <option value="">Seleccione...</option>
                                <option v-for="selectBDUsuario in selectBDUsuarios"
                                        :value="selectBDUsuario.USUARIO">
                                    {{selectBDUsuario.NOMBRE}}
                                </option>
                            </select>

                        </div>
                    </div>


                    <div class="col-12 col-sm-5">
                        <div class="form-group input-group-sm">
                            <label for="nomProveedor">Bodegas </label>

                            <select class="selectBodega" id="selectBodega" name="selectBodega"
                                    v-model="valorBodega">
                                <option value="">Seleccione...</option>
                                <option v-for="selectBodega in selectBodegas"
                                        :value="selectBodega.USUARIO">
                                    {{selectBodega.NOMBRE}}
                                </option>
                            </select>

                        </div>
                    </div>

                    <div class="col-12 col-sm-2">
                        <div class="form-group input-group-sm">
                            <label for="nomProveedor">Bodegas </label>

                            <button class="btn btn-block" style="cursor: pointer;"
                                    @click="agregarBodega()">
                                <i title="" class="fa fa-arrow-down"></i>
                                &nbsp; Agregar
                            </button>

                        </div>
                    </div>


                </div>

                <!--
                 <div class="row">
                     <div class="col-12 col-sm-2">
                         <div class="text-center">
                             <label for="btnAgregarTabla">Agregar</label>
                             <br>
                             <span id="btnExportar" class="float-right"
                                   style="font-size: 1.2em; cursor: pointer; width: 100%; height: 100%"
                                   @click="cargarMunicipioTabla()">
                                     <i class="fa fa-plus-square-o" aria-hidden="true"></i>

                             </span>
                         </div>
                     </div>
                 </div>

                 -->

            </div>


        </div>


        <hr>

        <div class="row">


            <div class="col-12">


                <table class="table table-sm ">
                    <thead class="fondoGris">
                    <tr>
                        <th>Bodega</th>
                        <th>Descripcion</th>
                        <th width="100" style="text-align: center;">Opcion</th>

                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-for="(dato, index) in tablaPermisosTiene" :id="index" class="">
                        <td v-text="dato.codBodega"></td>
                        <td v-text="dato.noombreBodega"></td>

                        <td width="100" style="text-align: center;">
                            <i class="fa fa-trash-o" title=""
                               style="cursor:pointer; margin: 0 0 10px 10px; text-align: center;"
                               @click="QuitarPermisos(dato.usuario, dato.codBodega)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>


        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js "></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts.js"></script>
<script type="text/javascript" src="../../js/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript" src="../../js/accounting.js"></script>
<script lang="javascript" src="js/xlsx.full.min.js"></script>
<script lang="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>


</body>

</html>
