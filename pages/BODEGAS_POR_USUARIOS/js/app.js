// ----------------------------------------------
// On Load page
// ----------------------------------------------

jQuery(document).ready(function ($) {


    $(".selectBDUsuario").select2().change(function (e) {

        let usuario = $(this).val();
        let nombreUsuario = this.options[this.selectedIndex].text;

        app.valorBDUsuario = usuario;

        app.onChangeTipoUsuario(usuario, nombreUsuario);

    });

    $(".selectBodega").select2().change(function (e) {

        let bodega = $(this).val();
        app.valorBodega = bodega;

    });


    $("#foto").change(function () {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUsuario').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

});
// ----------------------------------------------
// ----------------------------------------------


let app = new Vue({
        el: '#app',
        data: {


            selectBDUsuarios: [],
            selectBodegas: [],

            valorBDUsuario: "",
            valorBodega: "",

            idDepartamento: "",
            selectDepartamentos: [],

            idMunicipio: "",
            selectMunicipios: [],

            tablaDepartamentos: [],

            tablaPermisosTiene: [],
            tablaPermisosNoTiene: [],
            tablaTodos: [],


            usuario: "",
            nombreUsuario:
                "",

            password1:
                "",
            password2:
                "",


        },

        methods: {


            loadSelectUsuariosRegistrados: function () {
                var app = this;
                $.get('./request/getSelectUsuariosRegistrados.php', function (data) {

                    app.selectBDUsuarios = JSON.parse(data);

                });


            },

            loadSelectBodega: function () {
                var app = this;
                $.get('./request/getSelectBodega.php', function (data) {

                    app.selectBodegas = JSON.parse(data);

                });


            },

            onChangeTipoUsuario: function (usuario, nombreUsuario) {

                var app = this;


                app.usuario = usuario;
                app.nombreUsuario = usuario;

                app.tablaPermisosTiene = [];

                $.get('./request/getPermisos.php?usuario=' + usuario, function (data) {

                    var datos = JSON.parse(data);

                    console.log(data)

                    app.tablaPermisosTiene = datos;


                    //app.tablaPermisos = JSON.parse(data);

                });


            },

            agregarBodega: function () {

                var app = this;


                if (app.valorBDUsuario == "" || app.valorBodega == "") {

                    alertify.warning("Seleccione un usuario y una bodega")

                } else {

                    var formData = new FormData;
                    formData.append('usuario', app.valorBDUsuario);
                    formData.append('bodega', app.valorBodega);


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertPermisos.php',
                        type: 'POST',
                        data: formData,
                        //necesario para subir archivos via ajax
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                        },
                        success: function (data) {

                            console.log(data)

                            if (data == 1) {
                                alertify.success("Agregado correctamente")
                                app.onChangeTipoUsuario(app.valorBDUsuario)
                            }

                            if (data.includes("violation of PRIMARY or UNIQUE KEY")) {
                                alertify.warning("Este usuario ya tiene esta bodega")
                            }

                        },
                        //si ha ocurrido un error
                        error: function (FormData) {
                            console.log(data)
                        }
                    });

                }


            },

            QuitarPermisos: function (usuario, codBodega) {

                var formData = new FormData;
                formData.append('usuario', usuario);
                formData.append('codBodega', codBodega);


                //hacemos la petición ajax
                $.ajax({
                    url: './request/eliminarPermisos.php',
                    type: 'POST',
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log(data)

                        if (data == 1) {
                            alertify.success("Eliminado correctamente")
                            app.onChangeTipoUsuario(app.valorBDUsuario)
                        } else {

                            alertify.error("Error")
                        }

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });

            }

        },

        watch:
            {}
        ,
        mounted() {

            this.loadSelectUsuariosRegistrados();
            this.loadSelectBodega();

        }
        ,

    })
;
