var oFileIn, evento;

function guardarEvento(evt) {
  evento = evt;
}

$("#exportar_excel").click(function () {
  var elt = document.getElementById('Exportar_a_Excel');
  var wb = XLSX.utils.table_to_book(elt, { sheet: "Sheet JS" });
  return XLSX.writeFile(wb, 'test.xlsx');
});


function doit(type, fn, dl) {
  var elt = document.getElementById('data-table');
  var wb = XLSX.utils.table_to_book(elt, { sheet: "Sheet JS" });
  return dl ?
    XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
    XLSX.writeFile(wb, 'test.xlsx');
}

$("#btn_consultar").click(function () {
  $("#cuerpo_tabla").html('');
  filePicked(evento);
});

$(function () {
  oFileIn = document.getElementById('archivo');
  if (oFileIn.addEventListener) {
    oFileIn.addEventListener('change', guardarEvento, false);
  }
});

function filePicked(oEvent) {

  var oFile = oEvent.target.files[0];
  var sFilename = oFile.name;

  var reader = new FileReader();


  reader.onload = function (e) {
    var data = e.target.result;
    var cfb = XLS.CFB.read(data, { type: 'binary' });
    var wb = XLS.parse_xlscfb(cfb);
    var resultados = [];
    wb.SheetNames.forEach(async function (sheetName) {
      function InsertDataBase(rowsInsert) {
        return new Promise(function(resolve, reject){
          fetch("request/insert.php", {
            method: 'POST',
            mode: "same-origin",
            credentials: "same-origin",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({ data: rowsInsert })
          })
            .then(res => {
              // console.info(res)
              return res.json()
            })
            .catch(error => {reject(error)})
            .then(json => { resolve(json) })
        })
      }
      var data = XLS.utils.sheet_to_json(wb.Sheets[sheetName]);
      let partAmount = 200
      let parts = Math.ceil(data.length / partAmount)
      // console.log("Partes: ", parts)
      
      for(let i = 0; i < parts ; i++){
        let sendData = data.slice(i*partAmount, ((i+1)*partAmount));
        var m = await InsertDataBase(sendData)
        resultados = resultados.concat(m)
        console.log(resultados)
        let errores = m.filter(m1 => m1.success === false)
        errores.forEach((value, index) => {
          console.log(value.data)
          $("#cuerpo_tabla").append(`<tr>
          <td>${value.data['ID'] || ''}</td>
          <td>${value.data['NIC'] || ''}</td>
          <td>${value.data['CAMPANA'] || ''}</td>
          <td>${value.data['DELEGACION'] || ''}</td>
          <td>${value.data['CODTEC'] || ''}</td>
          <td>${value.data['MES_PROGRAMA'] | ''}</td>
          <td>${value.data['DIA_PROGRAMA'] || ''}</td>
          <td>${value.data['MES_PROGRAMA'] || ''}</td>
          <td>${value.data['ANO_PROGRAMA'] || 'No existe'}</td>
          <td>${value.result}</td>
          </tr>`);
        })
        
        $("#correctos").html(resultados.filter(m => m.success === true).length);
        $("#errores").html(resultados.filter(m => m.success === false).length);
        $("#total").html(resultados.length);
        let porcentaje = Math.floor((resultados.length / data.length * 100))
        $("#barra").html(porcentaje + "%");
        $("#barra").css("width", porcentaje + "%");
      }
      
    });
  };

  // Tell JS To Start Reading The File.. You could delay this if desired
  reader.readAsBinaryString(oFile);
}
