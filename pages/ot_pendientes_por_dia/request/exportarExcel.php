<?php
	session_start();
  	if(!$_SESSION['user']){
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }

	header("Content-type: application/vnd.ms-excel; name='excel'");
  //header("Content-Disposition: filename=ot_pendientes_por_dia_".str_replace('-',"",$_GET["fechaIni"])."_hasta_".str_replace('-',"",$_GET["fechaFin"]).".xls");
 	header("Content-Disposition: filename=ot_pendientes_por_dia_".$_GET['dpto']."_".$_GET['proceso'].".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	echo "\xEF\xBB\xBF"; // UTF-8 BOM

	include("../../../init/gestion.php");

	//$fechaini = $_GET['fechaIni'];
  //$fechafin = $_GET['fechaFin'];
  $dpto = $_GET['dpto'];
	$proceso = $_GET['proceso'];
		
	$consulta = "SELECT distinct
                   o.ot_solicitud,
                   o.ot_fecha_asigna_contrata,
                   d.de_nombre Dpto,
                   m.mu_nombre Mpio,
                  ((current_date -  o.ot_fecha_asigna_contrata)+1 - (select CANT_DIAS_MUERTOS from  calcular_dias2(o.ot_fecha_asigna_contrata, current_date))) Numero_dias FROM ot o left join departamentos d on d.de_codigo = o.ot_cod_dpto left join municipios m on m.mu_depto=o.ot_cod_dpto and m.mu_codigomun=o.ot_cod_mpio where o.ot_proceso_campana='".$proceso."' and o.ot_cod_dpto='".$dpto."' and o.ot_ejecutada <> 'S'  or (o.ot_ejecutada is null)";

	$result = ibase_query($conexion,$consulta);

	$tabla = "<table>".
					"<tr class='cabecera'><td>Solicitud</td><td>Fecha asig.</td><td>Departamento</td><td>Municipio</td><td>Dias</td></tr>";

	while($fila = ibase_fetch_row($result)){
		
		$tabla.="<tr class='fila'>".
					"<td>".utf8_encode($fila[0])."</td><td>".utf8_encode($fila[1])."</td>"."<td>".utf8_encode($fila[2])."</td><td>".utf8_encode($fila[3])."</td><td>".utf8_encode($fila[4])."</td>".
				"</tr>";						
		
	}

	$tabla.="</table>";
	echo $tabla;
	//$row_array['tabla'] = utf8_encode($tabla);
	//array_push($return_arr, $row_array);

	//echo json_encode($return_arr);
?>